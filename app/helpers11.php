<?php 

Use App\Models\KycCategory;
use Spatie\Permission\Models\Role;
use App\User;
use Spatie\Permission\Traits\HasRoles;
use App\Models\CreditWallet;

 function changeDateFormate($date,$date_format){
        return \Carbon\Carbon::createFromFormat('Y-m-d', $date)->format($date_format);    
    }
   
    function productImagePath($image_name)
    {
        return public_path('images/products/'.$image_name);
    }

if(!function_exists('getKycCat')){
	function getKycCat(){
		$kycCategory = KycCategory::orderBy('id', 'DESC')->get();
	
			return $kycCategory;
		
	}
}
 if(!function_exists('getCountRetailer')){
   
     function getCountRetailer(){
       // $user = User::get();
        $retailers = User::with('roles')->role('retailer')->count(); 
        if(!empty($retailers)){
            return $retailers;
        }else{
            return false;
        }


     } 
 } 
if(!function_exists('getAuthReferral')){
    function getAuthReferral(){
       $referral = User::select('referral_id')->where('id', Auth::user()->id)->first();
       if($referral){
           return $referral;
       }
       else{
           return false;
       }
    }
}
//Divya Helper

if (!function_exists('creditInvoiceNumber')) {

    function creditInvoiceNumber()
    {
        $latest = App\Models\CreditWalletTransaction::latest()->first();
    
        if (! $latest) {
            return 'TXN-CP0001';
        }
    
        return 'TXN-CP'.sprintf('%04d', $latest->id+1); // sprintf will add leading zeros to the invoice number
    }
    }
	
	if (!function_exists('cashInvoiceNumber')) {

    function cashInvoiceNumber()
    {
        $latest = App\Models\CashWalletTransaction::latest()->first();
    
        if (! $latest) {
            return 'TXN0001';
        }
    
        return 'TXN'.sprintf('%04d', $latest->id+1); // sprintf will add leading zeros to the invoice number
    }
    }
	
	if (!function_exists('monthlyRechargeInvoiceNumber')) {

    function monthlyRechargeInvoiceNumber()
    {
        $latest = App\Models\AccountRecharge::latest()->first();
    
        if (! $latest) {
            return 'TXN-MR0001';
        }
    
        return 'TXN-MR'.sprintf('%04d', $latest->id+1); // sprintf will add leading zeros to the invoice number
    }
    }
	
	if(!function_exists('getCreditPoints')){
		function getCreditPoints(){
		$userId = Auth::user()->id;		
        // $userId;
		$credit = CreditWallet::where('user_id', $userId)->first();
		if(!empty($credit)){
			return $credit;
		}
		else{
			return false;
		}

		}
	}

