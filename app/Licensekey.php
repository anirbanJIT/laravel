<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Licensekey extends Model
{
    protected $fillable = ['licensekey', 'is_active', 'used_by'];
}
