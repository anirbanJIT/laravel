<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;
use App\User;
use App\Models\UserAddonPlans;
use \Carbon\Carbon;
use \App\FCM;

class PlanExpire extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'plan:daily';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send plan expiry alert.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle() {
        $users = User::where('parent_user_id', 0)
                ->get()
                ->toArray();


        if (!empty($users)) {

            foreach ($users as $key => $user) {

                $basic = UserAddonPlans::select('*')
                        ->where('is_addon', 0)
                        ->where('user_id', $user['id'])
                        ->orderBy('id', 'desc')
                        ->first();

                if (!empty($basic)) {
                    $start_date = \Carbon\Carbon::parse(Carbon::now());
                    $end_date = \Carbon\Carbon::parse($basic->expire_date);
                    if ($end_date > $start_date) {
                        if ($user['fcm_token']) {
                            $different_days = $start_date->diffInDays($end_date);
                            if ($different_days <= 7) {
                                $msg = "Your plan is expiring in $different_days days.";
                                if ($different_days == 0) {
                                    $msg = "Your plan is expiring today.";
                                }
                                $fcm = new FCM();
                                $fcm->token = $user['fcm_token'];
                                $fcm->notification['title'] = 'Plan Expiring Alert!';
                                $fcm->notification['body'] = $msg;
                                $fcm->send();
                            }
                        }
                    }
                }
            }
        }

        $this->info('Cron run successfully!');
    }

}
