<?php
namespace App\Http\Controllers;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Models\CreditWallet;
use DB;
use Carbon\carbon;
use Charts;
use Response;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$totalCreditPoints = DB::table('credit_wallet')->sum('credit_wallet.amount');
        $totalRechargeAmount = DB::table('account_recharges')->sum('account_recharges.recharge_amount');
        $recharges = DB::table('account_recharges')->where(DB::raw("(DATE_FORMAT(created_at,'%Y'))"),date('Y'))
                    ->get();
		$totalUser =DB::table('users')->count();
		$activeUser =DB::table('users')->where("status",1)->count();
		$inactiveUser =DB::table('users')->where("status",0)->count();
       /* $chart=DB::table('users')->select( DB::raw('count(status) as y, status as label') )
                    ->groupBy('status')
                    ->get(); */
              //  return $chart;
        $totalRechargeAmountMonth = DB::table('account_recharges')                                  
                                    ->whereYear('created_at', Carbon::now()->year)
                                    ->whereMonth('created_at', Carbon::now()->month)
                                    ->sum('account_recharges.recharge_amount');
        $recentRecharges = DB::table('account_recharges')                                  
                                   ->join('users', 'account_recharges.user_id', '=', 'users.id')
                                   ->select('account_recharges.*', 'users.name as retailerName', 'users.email as retailerEmail')
                                    ->take(5)->orderBy('id', 'DESC')->get();
        // dd($recentRecharge); 
        $kyc_user = DB::table('user_kyc_details')->where('status', 0)->count();

        // complaint feedback        
        $complaint_feedback_total = DB::table('complaint_feedback')->count();
        $complaint_feedback_solved_total = DB::table('complaint_feedback')->where('status', 'Solved')->count();
        $complaint_feedback_unsolved_total = DB::table('complaint_feedback')->where('status', 'UnSolved')->count(); 
        $feedback_total = DB::table('complaint_feedback')->where('type', 'feedback')->count();
        $complaint_total = DB::table('complaint_feedback')->where('type', 'complaint')->count();

        // like unlike        
        $like_total = DB::table('user_reactions')->where('status', '1')->count();
        $unlike_total = DB::table('user_reactions')->where('status', '2')->count();
                                                                           
        return view('home', compact('totalCreditPoints','totalRechargeAmount', 'totalRechargeAmountMonth','recentRecharges','totalUser','activeUser','inactiveUser','kyc_user','complaint_feedback_total','complaint_feedback_solved_total','complaint_feedback_unsolved_total','feedback_total','complaint_total','like_total','unlike_total'));
    }
    public function getchatdata(Request $request)
    {
        $chart = DB::table('users')->where('type', 1)->select( DB::raw('count(status) as y, status as label') )
        ->groupBy('status')->get();

        $result= DB::table('account_recharges')->select( DB::raw('sum(recharge_amount) as y'), DB::raw("DATE_FORMAT(created_at,'%M %Y') as months") )
        ->groupBy('months')
        ->get();
    
        return Response::json(array('chart'=>$chart, 'chart1'=>$result));  
       // return Response::json($chart);  
    }

    public function likeList(Request $request) {

        if($request->type == 'like') {
            $like_title = "Like List";
            $data_list = DB::table('user_reactions')->select('users.name','user_reactions.rating','user_reactions.comment')->leftJoin('users', 'user_reactions.user_id', '=', 'users.id')->where('user_reactions.status', '1')->get();
        } else {
            $like_title = "Dislike List";
            $data_list = DB::table('user_reactions')->select('users.name','user_reactions.rating','user_reactions.comment')->leftJoin('users', 'user_reactions.user_id', '=', 'users.id')->where('user_reactions.status', '2')->get();
        }
        
        return view('admin.dashboard.like_list', compact('data_list','like_title'));
    }

    

}
