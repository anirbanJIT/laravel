<?php

namespace App\Http\Controllers\Api\V1;

use JWTAuth;
use App\User;
use App\Models\RechargePlan;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Str;

class RechargePlanController extends Controller {

    /**
     * @var
     */
    protected $user;

    /**
     * TaskController constructor.
     */
    public function __construct() {
        $this->user = JWTAuth::parseToken()->authenticate();
        return $this->user;
    }

    public function getPlanList() {

        if ($this->user) {

            $user = User::findOrFail($this->user->id);

            $user_current_plan = $user->current_plan ?? 'light_plan';

            $recharge_plan = RechargePlan::where('recharge_plan_type', $user_current_plan)->where('recharge_plan_status', '=', 0)->orderBy('id', 'DESC')->get();

            $data['user_current_plan'] = $user_current_plan;
            $data['recharge_plan'] = $recharge_plan;

            if ($recharge_plan) {

                $recharge_plans['responseCode'] = "200";
                $recharge_plans['responseMessage'] = "Success";
                $recharge_plans['responseDescription'] = "Success";
                $recharge_plans['data'] = $data;
            } else {

                $recharge_plans['responseCode'] = "100";
                $recharge_plans['responseMessage'] = "Success";
                $recharge_plans['responseDescription'] = "No data found";
            }
        } else {

            $recharge_plans['responseCode'] = "401";
            $recharge_plans['responseMessage'] = "Error";
            $recharge_plans['responseDescription'] = "Sorry, You are not authorized";
        }

        return $recharge_plans;
    }

}
