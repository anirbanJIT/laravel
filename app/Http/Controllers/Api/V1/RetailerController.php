<?php
namespace App\Http\Controllers\Api\V1;

use JWTAuth;
use App\User;
use App\Models\Customer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Str;

class RetailerController extends Controller
{
    /**
     * @var
     */
    protected $user;

    /**
     * TaskController constructor.
     */
    public function __construct()
    {
        $this->user = JWTAuth::parseToken()->authenticate();
		return $this->user;
    }

    public function getBuyerList()
    {

	 	if($this->user){
		
	        $referred_by = (!empty($_GET["referred_by"])) ? ($_GET["referred_by"]) : ('1');

	        if($referred_by){
				$usersReferral = User::where('id',$referred_by)->orWhere('email',$referred_by)->select('id')->first();
			} else {
				$usersReferral = User::where('id',$referred_by)->orWhere('email',$referred_by)->select('id')->first();
			}

			if(empty($usersReferral)) {
				$buyer_list['responseCode'] = "100";
				$buyer_list['responseMessage'] = "Success";
				$buyer_list['responseDescription'] = "No data found";
				return $buyer_list;
			}

	        if($usersReferral->id){
			 
		        $referred_by = $usersReferral->id;
				 
		        $usersQuery = Customer::query();
		          
		        $usersQuery->where('retailer_id',$referred_by);
	        
	        }

	        $users = $usersQuery->select('*')->orderBy('id','desc');

		    if(!empty($users)) {

				$buyer_list['responseCode'] = "200";
				$buyer_list['responseMessage'] = "Success";
				$buyer_list['responseDescription'] = "Success";
				$buyer_list['data'] = $users;

			} else {

				$buyer_list['responseCode'] = "100";
				$buyer_list['responseMessage'] = "Success";
				$buyer_list['responseDescription'] = "No data found";
			}

		} else {

			$buyer_list['responseCode'] = "401";
			$buyer_list['responseMessage'] = "Error";
			$buyer_list['responseDescription'] = "Sorry, You are not authorized";
		}

		return $buyer_list;

	}
	
}