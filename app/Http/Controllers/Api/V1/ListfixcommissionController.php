<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Customer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Str;

class ListfixcommissionController extends Controller {

    public function listfixcommission() {

        $data = DB::table('fix_commission')->first();

        if (!empty($data)) {

            $fix_commission_list['responseCode'] = "200";
            $fix_commission_list['responseMessage'] = "Success";
            $fix_commission_list['responseDescription'] = "Success";
            $fix_commission_list['data'] = $data;
        } else {

            $fix_commission_list['responseCode'] = "100";
            $fix_commission_list['responseMessage'] = "Success";
            $fix_commission_list['responseDescription'] = "No data found";
        }


        return $fix_commission_list;
    }

}
