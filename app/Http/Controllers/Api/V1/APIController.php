<?php



namespace App\Http\Controllers\Api\V1;



use DB;

use JWTAuth;

use App\User;

use App\Models\Banner;

use App\Models\Customer;

use App\Models\UserLevel;

use App\Models\CashWallet;

use App\Models\CreditWallet;

use App\Models\PasswordReset;

use App\Models\UserContactDetail;

use App\Models\ShopDetail;

use App\Models\UserImeiChangeRequest;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Validator;

use Illuminate\Http\Request;

use Tymon\JWTAuth\Exceptions\JWTException;

use App\Http\Requests\Api\RegistrationFormRequest;

use Illuminate\Support\Facades\Redirect;

use Hash,

    Mail;

use Illuminate\Support\Facades\Password;

use Illuminate\Mail\Message;

use Illuminate\Support\Str;

use Illuminate\Foundation\Auth\ResetsPasswords;

use App\Notifications\LoginOTP;

use App\Notifications\RegistrationOTP;

use App\Notifications\PasswordResetOTP;

use Auth;



class APIController extends Controller
{



    use ResetsPasswords;



    /**

     * @var bool

     */

    public $loginAfterSignUp = true;


    public function checkFun()
    {
        return auth()->user();
    }


    /**

     * @param Request $request

     * @return \Illuminate\Http\JsonResponse

     */

    public function login(Request $request)
    {

        //return $request['fcmtoken'];

        $token = null;

        $otp = null;

        $validator = Validator::make($request->all(), [

            'email' => 'required'

        ]);



        if ($validator->fails()) {

            $data['responseCode'] = "101";

            $data['responseMessage'] = "Error";

            $data['responseDescription'] = implode("\n", $validator->errors()->all());

            return $data;
        }



        $user = User::Where('phone', $request->email)->orWhere('email', $request->email)->orWhere('username', $request->email)->first();



        if (empty($user)) {

            $data['responseCode'] = "101";

            $data['responseMessage'] = "Failure";

            $data['responseDescription'] = "Invalid Email or Username or Phone";

            return $data;
        } else {

            if ($user->parent_user_id == 0 && $user->device_id != $request->device_id) {

                //                $imei = UserImeiChangeRequest::firstOrNew(['user_id' => $user->id]);

                $imei = new UserImeiChangeRequest();

                $imei->user_id = $user->id;

                $imei->device_id = $request->device_id;

                $imei->save();

                $data['responseCode'] = "102";

                $data['responseMessage'] = "Failure";

                $data['responseDescription'] = "Invalid device id, You can try to do this again or contact Modernmart admin";

                return $data;
            }
        }



        if ($request->has('login_with_otp')) {



            if (!$token = JWTAuth::fromUser($user)) {

                $data['responseCode'] = "100";

                $data['responseMessage'] = "Failure";

                $data['responseDescription'] = "JWTAuth token issue";

                return $data;
            }



            $otp = null;

            $otp = mt_rand(100000, 999999);

            if (is_numeric($request->get('email'))) {

                try {

                    $message = "Your OTP code is " . $otp;

                    $sms = json_decode($this->send_sms($request->email, $message), true);
                } catch (Exception $e) {

                    $data['responseCode'] = "100";

                    $data['responseMessage'] = "Failure";

                    $data['responseDescription'] = 'SMS Gateway Error';

                    return $data;
                }
            }



            if (!is_numeric($request->get('email')) && !empty($user->email)) {

                $user->notify(new LoginOTP($user, $otp));
            }

            Auth::login($user);

            User::where('id', auth()->user()->id)->update(['otp' => $otp, 'api_token' => $token]);
        } else {



            if (is_numeric($request->get('email'))) {



                $input = ['phone' => $request->get('email'), 'password' => $request->get('password')];
            } elseif (filter_var($request->get('email'), FILTER_VALIDATE_EMAIL)) {



                $input = ['email' => $request->get('email'), 'password' => $request->get('password')];
            } else {



                $input = ['username' => $request->get('email'), 'password' => $request->get('password')];
            }



            $token = JWTAuth::attempt($input);



            if (!$token) {

                $data['responseCode'] = "100";

                $data['responseMessage'] = "Failure";

                //$data['responseDescription'] = "JWT token not generated";	

                //$data['responseDescription'] = "Invalid Email or Username or Phone or Password : ". implode(', ', $request->all());

                $data['responseDescription'] = "Invalid Email or Username or Phone or Password";

                return $data;
            }
        }



        if (!empty($request['fcmtoken'])) {

            User::where('id', auth()->user()->id)->update(['fcm_token' => $request['fcmtoken']]);



            $user = DB::table('users')

                ->join('user_contact_details as c', 'users.id', '=', 'c.user_id', 'left')

                ->join('user_kyc_details as k', 'users.id', '=', 'k.user_id', 'left')

                ->select('users.*', 'c.address', 'c.city', 'c.state', 'c.country', 'c.pin_code', DB::raw('IFNULL(k.status,0) as kyc_status'))

                ->where('users.id', auth()->user()->id)

                ->first();



            if (isset($user->profile_pic)) {

                $user->profile_pic = url('/images/profile/' . $user->profile_pic);
            }



            $data['responseCode'] = "200";

            $data['responseMessage'] = "Success";

            $data['responseDescription'] = "Success";

            $data['data'] = $user;

            $data['token'] = $token;

            $data['otp'] = $otp;

            return $data;
        } else {

            $data['responseCode'] = "102";

            $data['responseMessage'] = "Failure";

            $data['responseDescription'] = "Empty fcmtoken";

            return $data;
        }
    }



    public function resendotp(Request $request)
    {

        $validator = Validator::make($request->all(), [

            'phone' => 'required'

        ]);



        if ($validator->fails()) {

            $data['responseCode'] = "101";

            $data['responseMessage'] = "Error";

            $data['responseDescription'] = implode("\n", $validator->errors()->all());

            return $data;
        }



        $user = User::Where('phone', $request->phone)->first();



        if (empty($user)) {

            $data['responseCode'] = "101";

            $data['responseMessage'] = "Failure";

            $data['responseDescription'] = "Invalid Email or Username or Phone";

            return $data;
        } else {

            $otp = null;

            $otp = mt_rand(100000, 999999);

            if (is_numeric($request->get('phone'))) {

                try {

                    $message = "Your OTP code is " . $otp;

                    $sms = json_decode($this->send_sms($request->phone, $message), true);

                    $data['otp_number'] = $otp;

                    $data['responseCode'] = "200";

                    $data['responseMessage'] = "Success";

                    User::where('id', $user->id)->update(['otp' => $otp]);

                    return $data;
                } catch (Exception $e) {

                    $data['responseCode'] = "100";

                    $data['responseMessage'] = "Failure";

                    $data['responseDescription'] = 'SMS Gateway Error';

                    return $data;
                }
            }
        }
    }



    public function LoginOtpverify(Request $request)
    {



        $validator = Validator::make($request->all(), [

            'user_id' => 'required',

            'otp_number' => 'required',

        ]);



        $temp = $validator->errors()->all();



        if ($validator->fails()) {

            $error['responseCode'] = "101";

            $user_reg['responseMessage'] = "Error";

            $user_reg['responseDescription'] = $temp;

            return $user_reg;
        }



        $user_id = $request->user_id;

        $user = User::find($user_id);

        if ($user->otp == $request->otp_number) {

            //User::where('id',$user->id)->update(['otp' => null]);

            $user = DB::table('users')

                ->join('user_contact_details', 'users.id', '=', 'user_contact_details.user_id')

                ->where('users.id', $user_id)

                ->first();

            if (isset($user->profile_pic)) {

                $user->profile_pic = url('/images/profile/' . $user->profile_pic);
            } else {

                $user->profile_pic = '';
            }

            $data['responseCode'] = "200";

            $data['responseMessage'] = "Success";

            $data['responseDescription'] = "Success";

            $data['data'] = $user;

            $data['token'] = $user->api_token;

            return $data;
        } else {

            $data['responseCode'] = "101";

            $data['responseMessage'] = "Error";

            $data['responseDescription'] = "Wrong OTP number";
        }

        return $data;
    }



    public function logout(Request $request)
    {





        try {

            // JWTAuth::invalidate($request->token);

            JWTAuth::invalidate(JWTAuth::getToken());

            //return response()->json([

            //  'success' => true,

            //  'message' => 'User logged out successfully'

            // ]);



            $data['responseCode'] = "200";

            $data['responseMessage'] = "Success";

            $data['responseDescription'] = "User logged out successfully";

            return $data;
        } catch (JWTException $exception) {

            //return response()->json([

            // 'success' => false,

            // 'message' => 'Sorry, the user cannot be logged out'

            // ], 500);



            $data['responseCode'] = "500";

            $data['responseMessage'] = "Error";

            $data['responseDescription'] = "Sorry, the user cannot be logged out";



            return $data;
        }
    }



    //new

    // public function resendOTP(Request $request) {

    //     $user = new User();

    //     $userget = DB::table('users')

    //             ->select('*')

    //             ->where('users.id', $request->user_id)

    //             ->first();

    //     $user->name = $userget->name;

    //     $user->email = $userget->email;

    //     $user->phone = $userget->phone;

    //     $user->password = bcrypt($request->password);

    //     $otp = null;

    //     $otp = mt_rand(100000, 999999);

    //     // $credit = json_decode($this->check_sms_credit(),true);

    //     // if($credit['status']=='success') {

    //     // //return $credit;

    //     // $sms_credit=$credit['balance']['sms'];

    //     // if($sms_credit>0)

    //     // {

    //     $message = "Thank you for registering with modern mart. Your OTP code is " . $otp;

    //     $sms = json_decode($this->send_sms($request->phone, $message), true);

    //     //}}

    //     if (!empty($user->email)) {

    //         $user->notify(new RegistrationOTP($user, $otp));

    //     }

    //     User::where('id', $userget->id)->update(['otp' => $otp]);

    //     //else{

    //     //return "Not balance available in SMS Api";

    //     //}

    //     $user_reg['responseCode'] = "200";

    //     $user_reg['responseMessage'] = "Success";

    //     $user_reg['responseDescription'] = "Success";

    //     $user_reg['data'] = $user;

    //     $user_reg['otp_number'] = $otp;

    //     return $user_reg;

    // }



    public function checkEmail(Request $request)
    {

        if ($request->has('email')) {

            $email = User::where('email', $request->email)->select('email')->first();

            if ($email) {





                $user_reg['responseCode'] = "101";

                $user_reg['responseMessage'] = "Error";

                $user_reg['responseDescription'] = "Email Id already available, Please enter another email id.";

                return $user_reg;
            } else {

                $user_reg['responseCode'] = "200";

                $user_reg['responseMessage'] = "Success";

                $user_reg['responseDescription'] = "Email Id available.";

                return $user_reg;
            }
        }



        if ($request->has('phone')) {

            $phone = User::where('phone', $request->phone)->select('phone')->first();

            if ($phone) {

                $user_reg['responseCode'] = "101";

                $user_reg['responseMessage'] = "Error";

                $user_reg['responseDescription'] = "Phone number already available, Please enter another Phone number.";

                return $user_reg;
            } else {

                $user_reg['responseCode'] = "200";

                $user_reg['responseMessage'] = "Success";

                $user_reg['responseDescription'] = "Phone number available.";

                return $user_reg;
            }
        }
    }



    /**

     * @param RegistrationFormRequest $request

     * @return \Illuminate\Http\JsonResponse

     */

    public function reg1(Request $request)
    {

        $referred_by = '';

        $validator = Validator::make($request->all(), [

            'name' => 'required|max:255',

            'phone' => 'required|max:15|unique:users',

            'email' => 'nullable|string|email|max:255',

            'password' => 'required|min:6|required_with:password_confirmation|same:password_confirmation',

            'password_confirmation' => 'min:6',

            'shop_name' => 'required|max:255',

            'address' => 'required',

            'country' => 'required',

            'state' => 'required',

            'city' => 'required',

            'pin_code' => 'required',

        ]);

        $temp = $validator->errors()->all();

        if ($validator->fails()) {

            $user_reg['responseCode'] = "101";

            $user_reg['responseMessage'] = "Error";

            $user_reg['responseDescription'] = implode("\n", $temp);

            return $user_reg;
        } else {



            if ($request->country != 101 && empty($request->email)) {

                $user_reg['responseCode'] = "101";

                $user_reg['responseMessage'] = "Error";

                $user_reg['responseDescription'] = "Email id is required";

                return $user_reg;
            }



            if (!empty($request->email)) {

                $user = User::Where('email', $request->email)->first();

                if ($user) {

                    $data['responseCode'] = "100";

                    $data['responseMessage'] = "Failure";

                    $data['responseDescription'] = "The email address has already been taken.";

                    return $data;
                }
            }



            $referred_by = '1234567';

            if ($request->referred_by != '' || $request->referred_by != NULL) {

                if ($this->checkuser($request->referred_by)) {

                    $referred_by = $request->referred_by;
                }

                /* else{

                  $user_reg['responseCode'] = "101";

                  $user_reg['responseMessage'] = "Error";

                  $user_reg['responseDescription'] = "Referral not found";

                  } */
            }



            $referral_id = $this->generateReferralNumber();

            if ($referred_by) {

                //$token = JWTAuth::fromUser($user);

                $user = new User();

                $user->referred_by = $referred_by;

                $user->shop_name = $request->shop_name;

                $user->name = $request->name;

                $user->email = $request->email;

                $user->phone = $request->phone;

                $user->other_phone = $request->other_phone;

                $user->referral_id = $referral_id;

                $user->type = 1;

                $user->password = bcrypt($request->password);

                $user->device_id = $request->device_id ?? null;

                $user->save();

                $user->assignRole('retailer');



                //$country = DB::table('countries')->select('id')->where('phonecode',$request->country)->first();



                $contact_details = [
                    'user_id' => $user->id,

                    'address' => $request->address,

                    'city' => $request->city,

                    'state' => $request->state,

                    'country' => $request->country,

                    'pin_code' => $request->pin_code

                ];



                UserContactDetail::create($contact_details);



                $Shop_details = [

                    'user_id' => $user->id,

                    'shop_name' => $request->shop_name,

                    'shop_photo' => '',

                    'shop_gallery' => json_encode([], JSON_FORCE_OBJECT),

                    'address' => $request->address,

                    'state' => $request->state,

                    'city' => $request->city,

                    'country_id' => $request->country,

                    'pin_code' => $request->pin_code

                ];



                ShopDetail::create($Shop_details);



                // if ($this->loginAfterSignUp) {

                //  return $this->login($request);

                // }

                //$input = $request->only('email', 'password');

                if ($request->has('email') && !empty($request->get('email'))) {

                    $input = $request->only('email', 'password');
                } else {

                    $input = ['phone' => $request->get('phone'), 'password' => $request->get('password')];
                }

                $token = null;

                $token = JWTAuth::attempt($input);



                $r = $referred_by;

                $l = 1;



                while ($r != '1') {

                    if ($r != '1') {

                        UserLevel::create(['user_id' => $user->referral_id, 'upline_id' => $r, 'level' => $l]);
                    }

                    $userData = User::select('referred_by')->where('referral_id', $r)->first();

                    $r = $userData->referred_by;

                    $l++;
                }



                CashWallet::create(['user_id' => $user->id, 'amount' => '0']);

                CreditWallet::create(['user_id' => $user->id, 'amount' => '0']);



                $otp = null;

                $otp = mt_rand(100000, 999999);

                // $credit = json_decode($this->check_sms_credit(),true);

                // if($credit['status']=='success') {

                // //return $credit;

                // $sms_credit=$credit['balance']['sms'];

                // if($sms_credit>0)

                // {

                /* 	$message = "Thank you for registering with modern mart. Your OTP code is ".$otp; */



                $message = "Welcome to modernmart. Your otp is " . $otp;





                $sms = json_decode($this->send_sms($request->phone, $message), true);



                //}}



                if (!empty($user->email)) {

                    $user->notify(new RegistrationOTP($user, $otp));
                }



                User::where('id', $user->id)->update(['otp' => $otp, 'api_token' => $token]);

                //else{

                //return "Not balance available in SMS Api";

                //}



                $user_reg['responseCode'] = "200";

                $user_reg['responseMessage'] = "Success";

                $user_reg['responseDescription'] = "Success";

                $user_reg['data'] = $user;

                $user_reg['otp_number'] = $otp;

                $user_reg['token'] = $token;
            } else {

                $user_reg['responseCode'] = "101";

                $user_reg['responseMessage'] = "Error";

                $user_reg['responseDescription'] = "Referral not found";
            }

            return $user_reg;
        }
    }



    public function verifyOtp(Request $request)
    {



        $user_id = $request->user_id;

        $user = User::find($user_id);

        if ($user->otp == $request->otp_number) {

            User::where('id', $user->id)->update(['otp_status' => 1]);

            $user_reg['responseCode'] = "200";

            $user_reg['responseMessage'] = "Success";

            $user_reg['responseDescription'] = "Your mobile number has been verified successfully";
        } else {

            $user_reg['responseCode'] = "101";

            $user_reg['responseMessage'] = "Error";

            $user_reg['responseDescription'] = "Wrong OTP number";
        }

        return $user_reg;
    }



    public function recoverPassord(Request $request)
    {

        if (is_numeric($request->get('email'))) {

            $user = User::where('phone', $request->email)->first();
        } elseif (filter_var($request->get('email'), FILTER_VALIDATE_EMAIL)) {

            $user = User::where('email', $request->email)->first();
        }

        if (!$user) {

            $forget['responseCode'] = "101";

            $forget['responseMessage'] = "Error";

            $forget['responseDescription'] = "Your phone number \ email address was not found.";

            return $forget;
        }



        try {

            if ($user->parent_user_id != 0) {

                $Mainuser = User::where('id', $user->parent_user_id)->first();



                $phoneno = $Mainuser->phone;

                $date = date("Y-m-d H:i:s");

                $token = Str::random(64);



                $otp = null;

                $otp = mt_rand(100000, 999999);

                if (!empty($Mainuser->phone)) {

                    $message = $Mainuser->name . ", Your OTP code is " . $otp;

                    $sms = json_decode($this->send_sms($phoneno, $message), true);
                }

                //                $credit = json_decode($this->check_sms_credit(), true);

                //                if ($credit['status'] == 'success') {

                //                    $sms_credit = $credit['balance']['sms'];

                //                    if ($sms_credit > 0) {

                //                        $message = "Your OTP code is " . $otp;

                //                        $sms = json_decode($this->send_sms($phoneno, $message), true);

                //                    }

                //                }

            } else {

                $date = date("Y-m-d H:i:s");

                $token = Str::random(64);



                $otp = null;

                $otp = mt_rand(100000, 999999);

                if (!empty($user->phone)) {

                    $message = $user->name . ", Your OTP code is " . $otp;

                    $sms = json_decode($this->send_sms($user->phone, $message), true);

                    //                    $credit = json_decode($this->check_sms_credit(), true);

                    //                    if ($credit['status'] == 'success') {

                    //                        $sms_credit = $credit['balance']['sms'];

                    //                        if ($sms_credit > 0) {

                    //                            $message = "Your OTP code is " . $otp;

                    //                            $sms = json_decode($this->send_sms($user->phone, $message), true);

                    //                        }

                    //                    }

                }
            }

            PasswordReset::insert([

                'email' => empty($user->email) ? $user->phone : $user->email,

                'token' => $token,

                'otp' => $otp,

                'created_at' => $date

            ]);

            if (!empty($user->email)) {

                $user->notify(new PasswordResetOTP($user, $otp));
            }

            $user['token'] = $token;

            $user['password_otp'] = $otp;

            $forget['otp_number'] = $otp;

            $forget['responseCode'] = "200";

            $forget['responseMessage'] = "Success";

            $forget['data'] = $user;
        } catch (\Exception $e) {

            $error_message = $e->getMessage();

            $forget['responseCode'] = "401";

            $forget['responseMessage'] = "Error";

            $forget['responseDescription'] = $error_message;
        }

        return $forget;
    }



    public function resetPassword(Request $request)
    {

        $validator = Validator::make($request->all(), [

            'token' => 'required',

            'otp' => 'required',

            'email' => 'required',

            //'phone' => 'nullable|exists:users,phone',

            'password' => 'required|confirmed|min:6'

        ]);

        $temp = $validator->errors()->all();

        //check if input is valid before moving on

        if ($validator->fails()) {

            // return redirect()->back()->withErrors(['email' => 'Please complete the form']);

            $reset['responseCode'] = "401";

            $reset['responseMessage'] = "Error";

            $reset['responseDescription'] = $temp;

            return $reset;
        }



        $password = $request->password; // Validate the token

        $tokenData = DB::table('password_resets')->where([['token', $request->token], ['otp', $request->otp]])->first(); // Redirect the user back to the password reset request form if the token is invalid

        //return $tokenData;

        if (!$tokenData) {

            //return view('auth.passwords.email');

            $reset['responseCode'] = "401";

            $reset['responseMessage'] = "Error";

            $reset['responseDescription'] = "Your token is invalid";

            return $reset;
        }







        $tokenemail = substr($tokenData->email, 0, 10);

        /* $user = User::Where('phone', $tokenemail)->first();

          if (!$user) {

          $reset['responseCode'] = "401";

          $reset['responseMessage'] = "Error";

          $reset['responseDescription'] = "Phone not found";

          return $reset;

          } */



        $user = User::where('email', $tokenData->email)->first();

        if (!$user) {

            $reset['responseCode'] = "401";

            $reset['responseMessage'] = "Error";

            $reset['responseDescription'] = "Email not found";

            return $reset;
        }



        $user->password = bcrypt($password);

        $user->update(); //or $user->save();

        //login the user immediately they change password successfully

        //Auth::login($user);

        //Delete the token

        PasswordReset::where('email', $user->email)->orWhere('email', $user->phone)->delete();



        $reset['responseCode'] = "200";

        $reset['responseMessage'] = "Sucess";

        $reset['responseDescription'] = "Your new password has been changed.";

        return $reset;



        //Send Email Reset Success Email

        //if ($this->sendSuccessEmail($tokenData->email)) {

        // return view('index');

        //} else {

        //  return redirect()->back()->withErrors(['email' => trans('A Network Error occurred. Please try again.')]);

        //}

    }



    public function customerRegistration(Request $request)
    {

        $validator = Validator::make($request->all(), [

            'name' => 'required|max:255',

            'phone' => 'required|max:15',

            'model_id' => 'required',

            'amount' => 'required',

            'bulk_cut' => 'required'
        ]);



        $temp = $validator->errors()->all();

        if ($validator->fails()) {

            $user_reg['responseCode'] = "101";

            $user_reg['responseMessage'] = "Error";

            $user_reg['responseDescription'] = $temp;

            return $user_reg;

            //return Redirect::back()->withErrors($validator);

        } else {



            //$token = JWTAuth::fromUser($user);

            $retailer = DB::table('users')->select('id', 'shop_name', 'name')->where('id', auth()->user()->id)->get();

            //print_r($retailer[0]->shop_name);exit;

            $user = new Customer();

            if ($request->bulk_cut == '1') {

                $user->qty = $request->qty;

                $user->bulk_quantity = $request->qty;
            }

            $user->retailer_id = auth()->user()->id;

            $user->name = $request->name;

            $user->phone = $request->phone;

            $user->model_id = $request->model_id;

            $user->image_id = $request->image_id ?? NULL;



            $user->amount = $request->amount;

            $create = $user->save();

            $message = urlencode("Hi, " . $request->name . ", Welcome to Modernmart. please give review to shop : " . $retailer[0]->shop_name . " on Website - https://modernmart.xyz/store/?serach_shop=" . $retailer[0]->shop_name);

            //$api_url = urlencode("http://msg.pwasms.com/app/smsapi/index.php?key=4617005EE5A04B&campaign=0&routeid=69&type=text&contacts=".$request->phone."&senderid=SELCT&msg=".$message);

            //$response = file_get_contents($api_url);

            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, "http://msg.pwasms.com/app/smsapi/index.php");

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

            curl_setopt($ch, CURLOPT_POST, 1);

            curl_setopt($ch, CURLOPT_POSTFIELDS, "key=4617005EE5A04B&campaign=0&routeid=69&type=text&contacts=" . $request->phone . "&senderid=SELECT&msg=" . $message);

            $response = curl_exec($ch);

            curl_close($ch);



            if ($create) {

                $user_reg['responseCode'] = "200";

                $user_reg['responseMessage'] = "Success";

                $user_reg['responseDescription'] = "Customer created successfully.";
            } else {



                $user_reg['responseCode'] = "101";

                $user_reg['responseMessage'] = "Error";

                $user_reg['responseDescription'] = "Customer not created.";
            }
        }

        return $user_reg;
    }



    /**

     * @param Request $request

     * @return \Illuminate\Http\JsonResponse

     */

    public function getBanner(Request $request)
    {

        $banner_location = $request->banner_location ?? 'home_banner';

        $banner_data = Banner::where('banner_location', $banner_location)->where('status', 1)->get();

        if ($banner_data) {

            $banner['responseCode'] = "200";

            $banner['responseMessage'] = "Success";

            foreach ($banner_data as $key => $value) {

                $banner['data'][$key]['image'] = $value['image'];

                $banner['data'][$key]['url'] = $value['ex_link'];
            }

            $banner['url'] = "images/banners/";
        } else {

            $banner['responseCode'] = "100";

            $banner['responseMessage'] = "Success";

            $banner['responseDescription'] = "No data found";
        }

        return $banner;
    }



    //Country List Api

    public function getCountryList()
    {

        $countryList = DB::table('countries')->select('id', 'phonecode', 'name')->get()->toArray();

        if ($countryList) {

            $countries['responseCode'] = "200";

            $countries['responseMessage'] = "Success";

            $countries['data'] = $countryList;
        } else {

            $countryList['responseCode'] = "100";

            $countryList['responseMessage'] = "Success";

            $countryList['responseDescription'] = "No data found";
        }

        return $countries;
    }



    /*     * *****  State List   *********** */



    //Country List Api

    public function getStateList(Request $request)
    {



        $stateList = DB::table('states')->select('id', 'name', 'country_id')->where([['country_id', '=', $request->country_id]])->get()->toArray();

        if ($stateList) {

            $states['responseCode'] = "200";

            $states['responseMessage'] = "Success";

            $states['data'] = $stateList;
        } else {

            $states['responseCode'] = "100";

            $states['responseMessage'] = "Success";

            $states['responseDescription'] = "No data found";
        }

        return $states;
    }



    //Country List Api

    public function getAppVersion()
    {

        $app_version = DB::table('appversion')->select('app_current_version')->Where('id', 1)->first();

        if ($app_version) {

            $data['responseCode'] = "200";

            $data['responseMessage'] = "Success";

            $data['data'] = $app_version;
        } else {

            $data['responseCode'] = "100";

            $data['responseMessage'] = "Success";

            $data['responseDescription'] = "No data found";
        }

        return $data;
    }



    //paper Size List Api

    public function getPaperSizeList()
    {



        $paperSizesList = DB::table('paper_sizes')->select('id', 'title', 'paper_width', 'paper_height')->get()->toArray();

        if ($paperSizesList) {

            $paperSizeList['responseCode'] = "200";

            $paperSizeList['responseMessage'] = "Success";

            $paperSizeList['data'] = $paperSizesList;
        } else {

            $paperSizeList['responseCode'] = "100";

            $paperSizeList['responseMessage'] = "Success";

            $paperSizeList['responseDescription'] = "No data found";
        }

        return $paperSizeList;
    }



    /*     * *****  paper Size List Api   *********** */



    public function checkBulkStatus()
    {



        if (auth()->user()) {



            $customer = DB::table('customers')->select('*')->where('retailer_id', auth()->user()->id)->orderBy('id', 'desc')->first();



            if (isset($customer->bulk_quantity) && $customer->bulk_quantity > 0) {

                $responce['responseCode'] = "200";

                $responce['responseMessage'] = "Success";

                $responce['responseDescription'] = "ON";
            } else {

                $responce['responseCode'] = "200";

                $responce['responseMessage'] = "Success";

                $responce['responseDescription'] = "OFF";
            }
        } else {

            $responce['responseCode'] = "401";

            $responce['responseMessage'] = "Error";

            $responce['responseDescription'] = "Sorry, You are not authorized";
        }

        return $responce;
    }



    public function updateBulkQuantity(Request $request)
    {



        if (auth()->user()) {



            $validator = Validator::make($request->all(), [

                'quantity' => 'required|numeric|min:0|not_in:0',

            ]);



            $temp = $validator->errors()->all();

            if ($validator->fails()) {

                $responce['responseCode'] = "101";

                $responce['responseMessage'] = "Error";

                $responce['responseDescription'] = $temp;

                return $responce;
            } else {



                $customer = DB::table('customers')->select('*')->where('retailer_id', auth()->user()->id)->orderBy('id', 'desc')->first();



                if ($request->quantity > $customer->bulk_quantity) {

                    $responce['responseCode'] = "100";

                    $responce['responseMessage'] = "Error";

                    $responce['responseDescription'] = "You don't have sufficient balance.";
                } else {



                    $result = DB::table('customers')->where('id', $customer->id)->decrement('bulk_quantity', $request->quantity);



                    $responce['responseCode'] = "200";

                    $responce['responseMessage'] = "Success";

                    $responce['responseDescription'] = "Success";
                }
            }
        } else {

            $responce['responseCode'] = "401";

            $responce['responseMessage'] = "Error";

            $responce['responseDescription'] = "Sorry, You are not authorized";
        }



        return $responce;
    }
}
