<?php

namespace App\Http\Controllers\Api\V1;

use DB;

use JWTAuth;

use App\User;

use App\Models\Banner;

use App\Models\Customer;

use App\Models\UserLevel;

use App\Models\CashWallet;

use App\Models\CreditWallet;

use App\Models\PasswordReset;

use App\Models\UserContactDetail;

use App\Models\ShopDetail;

use App\Models\UserImeiChangeRequest;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Validator;

use Illuminate\Http\Request;

use Tymon\JWTAuth\Exceptions\JWTException;

use App\Http\Requests\Api\RegistrationFormRequest;

use Illuminate\Support\Facades\Redirect;

use Hash,

    Mail;

use Illuminate\Support\Facades\Password;
//use Illuminate\Support\Facades\DB;

use Illuminate\Mail\Message;

use Illuminate\Support\Str;

use Illuminate\Foundation\Auth\ResetsPasswords;

use App\Notifications\LoginOTP;

use App\Notifications\RegistrationOTP;

use App\Notifications\PasswordResetOTP;

use Auth;



class MainController extends Controller
{

    public function login(Request $request)
    {
        //$user = DB::table('users')->get();
        //return $user;


        //$user = DB::table('users')->Where('phone', $request->email)->orWhere('email', $request->email)->orWhere('username', $request->email)->first();

        $validator = Validator::make($request->all(), [

            'email' => 'required'

        ]);

        if ($validator->fails()) {

            $data['responseCode'] = "101";

            $data['responseMessage'] = "Error";

            $data['responseDescription'] = implode("\n", $validator->errors()->all());

            return $data;
        }

        $userList = DB::table('users')
            ->select('shop_name', 'name', 'username', 'email', 'phone', 'api_token', 'fcm_token')
            ->orWhere('phone', $request->email)
            ->orWhere('email', $request->email)
            ->orWhere('username', $request->email)
            ->get()->toArray();


        if ($userList) {

            $users['responseCode'] = "200";

            $users['responseMessage'] = "Success";

            $users['data'] = $userList;
        } else {

            $users['responseCode'] = "100";

            $users['responseMessage'] = "Failed";

            $users['responseDescription'] = "No data found";
        }

        return $users;
    }

    // email check is here

    public function checkEmail(Request $request)
    {

        if ($request->has('email')) {

            $email = User::where('email', $request->email)->select('email')->first();

            if ($email) {





                $user_reg['responseCode'] = "101";

                $user_reg['responseMessage'] = "Error";

                $user_reg['responseDescription'] = "Email Id already available, Please enter another email id.";

                return $user_reg;
            } else {

                $user_reg['responseCode'] = "200";

                $user_reg['responseMessage'] = "Success";

                $user_reg['responseDescription'] = "Email Id available.";

                return $user_reg;
            }
        }
    }
}
