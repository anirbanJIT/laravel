<?php

namespace App\Http\Controllers\Api\V1;
use JWTAuth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UserContactDetail;
use App\Models\UserBankDetail;
use App\Models\ShopDetail;
use App\User;
use App\Models\UserKycDetail;
use App\Models\UserProfileChangeRequest;
use App\Models\KycCategory;
use DB;
use Validator;
use App\Models\RechargeCommission;
use App\Models\UserLevel;
use App\Models\CreditWallet;
use App\Models\CashWallet;
use App\Models\Customer;
use App\Models\ComplaintFeedback;
use App\Models\GeneralNotification;
use App\Models\PayoutRequest;
use Exception;
use \Illuminate\Support\Carbon;
use PDF;
class UserController extends Controller
{
   /**
     * @var
     */
    protected $user;

    /**
     * TaskController constructor.
     */
   public function __construct()
    {
        $this->user = JWTAuth::parseToken()->authenticate();
        return $this->user;
    }

  public function getUserContactDetail(Request $request)
  {
    //return $this->user->id;
    if($this->user){
    $userContactDetails = UserContactDetail::select('id', 'user_id', 'address', 'city','state', 'country', 'pin_code')->where('user_id', $this->user->id)->get()->toArray();

    $userShopDetail = $this->user->getShopDetail()->first();

     if ($userShopDetail) {
      $userShopDetail->shop_gallery = isset($userShopDetail->shop_gallery) ? json_decode($userShopDetail->shop_gallery,true) : [];
      $userShopDetail->shop_gallery = implode(',', $userShopDetail->shop_gallery);
    }else{
      $userShopDetail = [   
          'user_id' =>  $this->user->id,
          'shop_name' => $this->user->shop_name,
          'shop_photo'  => '',
          'shop_gallery' =>  json_encode([],JSON_FORCE_OBJECT),
          'address' => null,
          'state' => null,
          'city' =>  null,
          'country_id' =>  null,
          'pin_code' => null
      ]; 
      ShopDetail::create($userShopDetail);
      $userShopDetail = $this->user->getShopDetail()->first();
    } 

    if($userContactDetails) {
      $contactDetails['responseCode'] = "200";
      $contactDetails['responseMessage'] = "Success";
      $contactDetails['data'] = $userContactDetails;
      $contactDetails['shop_data'] = !empty($userShopDetail) ? $userShopDetail : [];
      $contactDetails['shop_base_url'] = url('/images/shop/');
    }
    else{
      $contactDetails['responseCode'] = "100";
      $contactDetails['responseMessage'] = "Success";
      $contactDetails['responseDescription'] = "No data found";
    }
    }
    else
      {
      $contactDetails['responseCode'] = "401";
      $contactDetails['responseMessage'] = "Error";
      $contactDetails['responseDescription'] = "Sorry, You are not authorized";
      }
      return $contactDetails;
    }
    /****** Update user contact detail *****/
    public function UpdateUserContactDetail(Request $request){
     
    if($this->user){
        $validator = Validator::make($request->all(),[
            'address' => 'required',
            'country' => 'required',
            'state' => 'required',
            'city' => 'required',
            'pin_code' => 'min:6'
        ]);
    $temp = $validator->errors()->all();
    if($validator->fails()) 
      {
          $error['responseCode'] = "101";
          $user_reg['responseMessage'] = "Error";
          $user_reg['responseDescription'] = $temp;
          return $user_reg;
        //return Redirect::back()->withErrors($validator);
      }
      else {

          $user = User::findOrFail($request->user_id);
        
          $userContDetail = $user->getContactDetail()->first();
          if(!empty($userContDetail)){
            $userContactupdate = $userContDetail->fill($request->all())->save();
          }else{
            $userContactupdate = UserContactDetail::create($request->all()); 
          }
          
          if($userContactupdate) {
          $contactupdate['responseCode'] = "200";
          $contactupdate['responseMessage'] = "Success";
          $contactupdate['data'] = $userContactupdate;
          

          }
          else{
            $contactupdate['responseCode'] = "100";
            $contactupdate['responseMessage'] = "Success";
            $contactupdate['responseDescription'] = "No data found";
          }


            }

      }
      else
      {
      $contactupdate['responseCode'] = "401";
      $contactupdate['responseMessage'] = "Error";
      $contactupdate['responseDescription'] = "Sorry, You are not authorized";
      }
      
      return $contactupdate;

    }

    /****** send User Profile Change Request *****/
    public function sendUserProfileChangeRequest(Request $request){

        if($this->user){
            $validator = Validator::make($request->all(),[
                'user_id' => 'required',            
                'country' => 'required',
                'email' => 'required',
                'state' => 'required',
                'phone' => 'required',
            ]);
            $temp = $validator->errors()->all();

            if($validator->fails()) {

              $error['responseCode'] = "101";
              $user_reg['responseMessage'] = "Error";
              $user_reg['responseDescription'] = $temp;
              return $user_reg;

            } else {

                $userProfileChangeRequest = UserProfileChangeRequest::updateOrCreate($request->all());
                
                if($userProfileChangeRequest) {
                  $data = UserProfileChangeRequest::find($userProfileChangeRequest->id);
                  $contactupdate['responseCode'] = "200";
                  $contactupdate['responseMessage'] = "Success";
                  $contactupdate['data'] = $data;

                } else{

                  $error['responseCode'] = "101";
                  $user_reg['responseMessage'] = "Error";
                  $contactupdate['responseDescription'] = "No data found";

                }

            }
        } else {
          $contactupdate['responseCode'] = "401";
          $contactupdate['responseMessage'] = "Error";
          $contactupdate['responseDescription'] = "Sorry, You are not authorized";
        } 

        return $contactupdate;
    }

  public function getUserProfileChangeRequestStatus($user_id)
  {
      if($this->user){
         
         $data = UserProfileChangeRequest::where('user_id',$user_id)->first();
        
        if($data) {
          
          $contactupdate['responseCode'] = "200";
          $contactupdate['responseMessage'] = "Success";
          $contactupdate['data'] = $data;

        } else{

          $error['responseCode'] = "101";
          $user_reg['responseMessage'] = "Error";
          $contactupdate['responseDescription'] = "No data found";

        }

    } else {
      $contactupdate['responseCode'] = "401";
      $contactupdate['responseMessage'] = "Error";
      $contactupdate['responseDescription'] = "Sorry, You are not authorized";
    } 

    return $contactupdate;
  }

   //Banks details 
   public function getUserBankDetail(Request $request)
   {
     //return $this->user->id;
     if($this->user){
     $userBankDetails = UserBankDetail::select('id', 'user_id', 'bank_name', 'account_no','branch_name', 'account_name', 'ifsc_code', 'account_type', 'mobile_number')->where('user_id', $this->user->id)->get()->toArray();
      //return $brands_data;
     if($userBankDetails) {
     $bankDetails['responseCode'] = "200";
     $bankDetails['responseMessage'] = "Success";
     $bankDetails['data'] = $userBankDetails;
     
 
     }
     else{
       $bankDetails['responseCode'] = "100";
       $bankDetails['responseMessage'] = "Success";
       $bankDetails['responseDescription'] = "No data found";
     }
     }
     else
       {
       $bankDetails['responseCode'] = "401";
       $bankDetails['responseMessage'] = "Error";
       $bankDetails['responseDescription'] = "Sorry, You are not authorized";
       }
       return $bankDetails;
     }
     
   /****** Update user bank detail *****/
    public function UpdateUserBanktDetail(Request $request){

      if($this->user){
          $validator = Validator::make($request->all(),[
              'bank_name' => 'required',
              'account_no' => 'required',
              'branch_name' => 'required',
              'account_name' => 'required',
              'ifsc_code' => 'min:11',
              'account_type' => 'required'              
          ]);
          $temp = $validator->errors()->all();
          if($validator->fails()) 
          {
              $bankUpdate['responseCode'] = "101";
              $bankUpdate['responseMessage'] = "Error";
              $bankUpdate['responseDescription'] = impl