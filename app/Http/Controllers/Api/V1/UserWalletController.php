<?php

namespace App\Http\Controllers\Api\V1;

use JWTAuth;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\AccountRecharge;
use App\Models\CreditWalletTransaction;
use App\Models\CashWalletTransaction;
use App\Models\CreditWallet;
use App\Models\CashWallet;
use App\Models\RechargePlan;
use App\Models\Rewards;
use App\Models\RechargeCommission;
use App\Models\Log;
use App\Models\UserAddonPlans;
use App\Models\ModelScreenImage;
Use App\helper;
Use DB;
use DateTime;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class UserWalletController extends Controller {

    /**

     * @var

     */
    protected $user;
    protected $cashWallet;
    protected $creditWallet;

    /**

     * TaskController constructor.

     */
    public function __construct() {

        $this->creditWallet = new CreditWallet();

        $this->cashWallet = new CashWallet();

        $this->user = JWTAuth::parseToken()->authenticate();

        return $this->user;
    }

    //Credit Wallet

    public function getCreditWallet(Request $request) {

        //return $this->user->id;

        if ($this->user) {

            $getCreditWallet = CreditWalletTransaction::select('id', 'receiver_id', 'credit_amt', 'total_credit_pur_amt', 'transaction_description', 'created_at', 'status')->where('receiver_id', $this->user->id)->where('credit_amt', '>', '0')->where('status', '1')->get()->toArray();

            //return $creditWallet;

            if ($getCreditWallet) {

                $creditWallet['responseCode'] = "200";

                $creditWallet['responseMessage'] = "Success";



                $creditWallet['data'] = $getCreditWallet;
            } else {

                $creditWallet['responseCode'] = "100";

                $creditWallet['responseMessage'] = "Success";

                $creditWallet['responseDescription'] = "No data found";
            }
        } else {

            $creditWallet['responseCode'] = "401";

            $creditWallet['responseMessage'] = "Error";

            $creditWallet['responseDescription'] = "Sorry, You are not authorized";
        }

        return $creditWallet;
    }

    //Credit Wallet

    public function getDebitWallet(Request $request) {

        //return $this->user->id;

        if ($this->user) {

            $getDebitWallet = CreditWalletTransaction::select('id', 'receiver_id', 'debit_amt', 'total_credit_pur_amt', 'transaction_description', 'created_at', 'status')->where('receiver_id', $this->user->id)->where('debit_amt', '>', '0')->get()->toArray();

            //return $creditWallet;

            if ($getDebitWallet) {

                $debitWallet['responseCode'] = "200";

                $debitWallet['responseMessage'] = "Success";

                $debitWallet['data'] = $getDebitWallet;
            } else {

                $debitWallet['responseCode'] = "100";

                $debitWallet['responseMessage'] = "Success";

                $debitWallet['responseDescription'] = "No data found";
            }
        } else {

            $debitWallet['responseCode'] = "401";

            $debitWallet['responseMessage'] = "Error";

            $debitWallet['responseDescription'] = "Sorry, You are not authorized";
        }

        return $debitWallet;
    }

    public function getWalletDetail() {

        if ($this->user) {

            $credit_setting = $this->creditWallet->creditSetting();

            $credit = $this->creditWallet->retailerCreditBalance();

            $cash = $this->cashWallet->retailerCashBalance();

            $expired = $this->user->expire_date;
            if (empty($expired)) {
                $expired = 0;
            }
            $userplan = $this->user->current_plan_id;
            $validity = RechargePlan :: select('*')->where('id', $userplan)->get()->toArray();

            $wallet_detail['user_id'] = $credit->user_id;
            $wallet_detail['expired_at'] = $expired;
            $wallet_detail['current_plan'] = $validity;
            $wallet_detail['credit_wallet_balance'] = $credit->amount;

            $wallet_detail['cash_wallet_balance'] = $cash->amount;

            $wallet_detail['credit_point_amount_conversion'] = $credit_setting->amount;

            $wallet_detail['currency_symbol'] = env('CURRENCY_SYMBOL');



            if ($wallet_detail) {

                $data['responseCode'] = "200";

                $data['responseMessage'] = "Success";

                $data['data'] = $wallet_detail;
            } else {

                $data['responseCode'] = "100";

                $data['responseMessage'] = "Success";

                $data['responseDescription'] = "No data found";
            }
        } else {

            $data['responseCode'] = "401";

            $data['responseMessage'] = "Error";

            $data['responseDescription'] = "Sorry, You are not authorized";
        }

        return $data;
    }

    public function getpurchaseCreditsPointToken(Request $request) {

        $invoice_no = creditInvoiceNumber();
        $api_response = json_decode($this->getCreditPurchaseToken($request->total_credit_conversion_amt, $invoice_no), true);
        $balance = $this->creditWallet->retailerCreditBalance();
        if ($balance->amount > 0) {
            $balance_amount = $balance->amount + $request->credit_point;
        } else {
            $balance_amount = $request->credit_point;
        }
        $data = new CreditWalletTransaction;
        $data->invoice_no = $invoice_no;
        $data->receiver_id = $this->user->id;
        $data->sender_id = $this->user->id;
        $data->credit_amt = $request->credit_point;
        $data->debit_amt = 0;
        $data->balance_amt = ($balance->amount + $request->credit_point);
        $data->singal_credit_pur_amt = $request->credit_conversion_amt;
        $data->total_credit_pur_amt = $request->total_credit_conversion_amt;
        $data->type = 'Credit Purchase';
        $data->payment_mode = 'Cash Free';
        $data->transaction_description = 'You purchased ' . $request->credit_point . ' credit points and pay for this ' . env('CURRENCY_SYMBOL') . ' ' . $request->total_credit_conversion_amt;
        if ($data->save()) {
            $token_data['responseCode'] = "200";
            $token_data['responseMessage'] = "Success";
            $token_data['cashfree_app_id'] = env('CASHFREE_APP_ID');
            $token_data['orderId'] = $invoice_no;
            $token_data['data'] = $api_response;
        } else {
            $token_data['responseCode'] = "100";
            $token_data['responseMessage'] = "Error";
            $token_data['responseDescription'] = "Your Oredr is not completed, Please try again.";
        }
        return $token_data;
    }

    public function creditPurchaseSuccess(Request $request) {
        $secretkey = env('CASHFREE_SECRET_KEY');
        $orderId = $request->orderId;
        $orderAmount = $request->orderAmount;
        $referenceId = $request->referenceId;
        $txStatus = $request->txStatus;
        $paymentMode = $request->paymentMode;
        $txMsg = $request->txMsg;
        $txTime = $request->txTime;
        $signature = $request->signature;
        $data = $orderId . $orderAmount . $referenceId . $txStatus . $paymentMode . $txMsg . $txTime;
        $hash_hmac = hash_hmac('sha256', $data, $secretkey, true);
        $computedSignature = base64_encode($hash_hmac);
        if ($request->txStatus == 'SUCCESS' && ($signature == $computedSignature)) {
            $credit = CreditWalletTransaction::where('invoice_no', $request->orderId)->select('balance_amt')->first();
            // change status of order after payment confirmation
            CreditWalletTransaction::where('invoice_no', $request->orderId)->update(['status' => 1, 'transaction_id' => $request->referenceId]);
            CreditWallet::where('user_id', $this->user->id)->update(['amount' => $credit->balance_amt]);
            $purchase['responseCode'] = "200";
            $purchase['responseMessage'] = "Success";
            $purchase['responseDescription'] = "Your credit purchase has been done successfully of rs" . $credit->balance_amt . ". Please check your credit wallet.";
            $user = DB::table('users')
                    ->select('*')
                    ->where('users.id', $this->user_id)
                    ->first();
            $sms = json_decode($this->send_sms($request->phone, $purchase['responseDescription']), true);
        } else {
            $purchase['responseCode'] = "100";
            $purchase['responseMessage'] = "Error";
            $purchase['responseDescription'] = "Your Oredr is not completed, Please try again.";
        }
        return $purchase;
    }

    public function daysInterval($from, $to) {

        $datetime1 = new DateTime($from);

        $datetime2 = new DateTime($to);

        $interval = $datetime1->diff($datetime2);

        $days = $interval->format('%R%a'); //now do whatever you like with $days

        return $days;
    }

    public function getMonthlyRechargeInformation() {

        if ($this->user) {

            $data = array();

            $commission = DB::table('commission-setting')->where('user_id', $this->user->id)->first();
            if (!$commission) {
                $commission = DB::table('commission-setting')->whereNull('user_id')->first();
            }

            $date = date("Y-m-d H:i:s");

            $expire_date = auth()->user()->expire_date;

            $days = $this->daysInterval($date, $expire_date);



            if ($days > 0 && ($expire_date != NULL && $expire_date != '0000-00-00 00:00:00')) {

                $expire_date = $expire_date;
            } else {

                $expire_date = $date;
            }

            $creditConversion = DB::table('credit-point-setting')->first();

            $creditConversionAmount = $creditConversion->amount;



            $data['minimum_recharge_credit_point'] = $commission->minimum_recharge;

            $data['expire_date'] = $expire_date;

            $data['credit_point_amount'] = $creditConversionAmount;

            if ($data) {

                $purchase['responseCode'] = "200";

                $purchase['responseMessage'] = "Success";

                $purchase['data'] = $data;
            } else {



                $purchase['responseCode'] = "100";

                $purchase['responseMessage'] = "Error";

                $purchase['responseDescription'] = "Data not available.";
            }
        } else {

            $purchase['responseCode'] = "401";

            $purchase['responseMessage'] = "Error";

            $purchase['responseDescription'] = "Sorry, You are not authorized";
        }



        return $purchase;
    }

    public function monthlyRechargePayment(Request $request) {

        if ($this->user) {

            $validator = Validator::make($request->all(), [
                        'payment_method' => 'required',
            ]);

            if ($validator->fails()) {
                $purchase['responseCode'] = "100";
                $purchase['responseMessage'] = "Error";
                $purchase['responseDescription'] = "Please select payment methode";
            } else {

                if ($request->payment_method == 'credit_wallet') {
                    try {
                        $exception = DB::transaction(function () use($request) {

                                    $invoice_no = monthlyRechargeInvoiceNumber();
                                    $credit = new CreditWallet();
                                    $credit = $credit->retailerCreditBalance();

                                    if ($credit->amount >= $request->recharge_amount) {
                                        $balance_amount = $credit->amount - $request->recharge_amount;

                                        if ($request->recharge_duration > 0) {

                                            $data = new CreditWalletTransaction;
                                            $data->invoice_no = $invoice_no;
                                            $data->receiver_id = $this->user->id;
                                            $data->sender_id = $this->user->id;
                                            $data->credit_amt = 0;
                                            $data->debit_amt = $request->recharge_amount;
                                            $data->balance_amt = $balance_amount;
                                            $data->singal_credit_pur_amt = ($request->recharge_amount / $request->recharge_duration);
                                            $data->total_credit_pur_amt = $request->recharge_amount;
                                            $data->type = 'Account Recharge Payment';
                                            $data->status = 1;
                                            $data->payment_mode = 'Credit Wallet';
                                            $data->transaction_description = 'You pay ' . $request->recharge_amount . 'credit points.';

                                            if ($data->save()) {

                                                CreditWallet::where('user_id', $this->user->id)->update(['amount' => $balance_amount]);

                                                $accRecharge = new AccountRecharge;
                                                $accRecharge->user_id = $this->user->id;
                                                $accRecharge->recharge_duration = $request->recharge_duration;
                                                $accRecharge->recharge_amount = $request->recharge_amount;
                                                $accRecharge->expire_date = $request->expire_date;
                                                $accRecharge->payment_mode = 1;
                                                $accRecharge->invoice_no = $invoice_no;
                                                $accRecharge->status = 1;

                                                if ($accRecharge->save()) {

                                                    $com = $this->rechargeCommission($request, $invoice_no);
                                                    if ($com) {
                                                        User::where('id', $this->user->id)->update(['status' => 1, 'expire_date' => $request->expire_date]);
                                                    }
                                                }
                                            }
                                        } else {
                                            $purchase['responseCode'] = "100";
                                            $purchase['responseMessage'] = "Error";
                                            $purchase['responseDescription'] = "Please select recharge duration.";
                                            return $purchase;
                                        }
                                    } else {
                                        $purchase['responseCode'] = "100";
                                        $purchase['responseMessage'] = "Error";
                                        $purchase['responseDescription'] = "You have not sufficient balance in credit wallet.";
                                        return $purchase;
                                    }

                                    $purchase['responseCode'] = "200";
                                    $purchase['responseMessage'] = "Success";
                                    $purchase['responseDescription'] = "Your monthly recharge has been done successfully";
                                    $user = DB::table('users')
                                            ->select('*')
                                            ->where('users.id', $this->user_id)
                                            ->first();
                                    $message = "Thank you! Your monthly recharge of " + $request->recharge_amount + ".rs has been done successfully. Invoice No." + $invoice_no;
                                    $sms = json_decode($this->send_sms($user->phone, $message), true);
                                    return $purchase;
                                });
                        return is_null($exception) ? true : $exception;
                    } catch (Exception $e) {
                        //return false;
                        $purchase['responseCode'] = "100";
                        $purchase['responseMessage'] = "Error";
                        $purchase['responseDescription'] = "Sorry your request is not completed.";
                        return $purchase;
                    }
                } else if ($request->payment_method == 'cash_free') {

                    $invoice_no = monthlyRechargeInvoiceNumber();
                    $api_response = json_decode($this->getCreditPurchaseToken($request->recharge_amount_after_conversion, $invoice_no), true);
                    $purchase['responseCode'] = "200";
                    $purchase['responseMessage'] = "Success";
                    $purchase['cashfree_app_id'] = env('CASHFREE_APP_ID');
                    $purchase['orderId'] = $invoice_no;
                    $purchase['api_data'] = $api_response;
                    $purchase['request_data'] = $request->all();
                }
                return $purchase;
            }
        } else {

            $purchase['responseCode'] = "401";
            $purchase['responseMessage'] = "Error";
            $purchase['responseDescription'] = "Sorry, You are not authorized";
            return $purchase;
        }
    }

    public function monthlyRechargeSuccess(Request $request) {
        if ($this->user) {
            $secretkey = env('CASHFREE_SECRET_KEY');
            $orderId = $request->orderId;
            $orderAmount = $request->orderAmount;
            $referenceId = $request->referenceId;
            $txStatus = $request->txStatus;
            $paymentMode = $request->paymentMode;
            $txMsg = $request->txMsg;
            $txTime = $request->txTime;
            $signature = $request->signature;
            $data = $orderId . $orderAmount . $referenceId . $txStatus . $paymentMode . $txMsg . $txTime;
            $hash_hmac = hash_hmac('sha256', $data, $secretkey, true);
            $computedSignature = base64_encode($hash_hmac);
            try {

                $exception = DB::transaction(function () use($request, $txStatus, $signature, $computedSignature) {
                            if ($request->txStatus == 'SUCCESS') {

                                $allres = 'txStatus ' . $txStatus . 'signature ' . $signature . 'computedSignature ' . $computedSignature;
                                $data = new Log;
                                $data->user_id = $this->user->id;
                                $data->type = 'CASHFREE_SUCCESS' . $request->txStatus;
                                $data->description = implode(',', $request->all()) . $allres;
                                $data->save();

                                $accRecharge = new AccountRecharge;
                                $accRecharge->user_id = $this->user->id;
                                $accRecharge->plan_id = $request->plan_id ?? NULL;
                                $accRecharge->recharge_duration = $request->recharge_duration;
                                $accRecharge->recharge_amount = $request->recharge_amount;
                                $accRecharge->expire_date = $request->expire_date;
                                $accRecharge->payment_mode = 2;
                                $accRecharge->invoice_no = $request->orderId;
                                $accRecharge->status = 1;
                                if ($accRecharge->save()) {

                                    $balance = $this->creditWallet->retailerCreditBalance();
                                    if ($balance->amount > 0) {
                                        $balance_amount = $balance->amount + $request->credit_point;
                                    } else {
                                        $balance_amount = $request->credit_point;
                                    }
                                    $data = new CreditWalletTransaction;
                                    $data->invoice_no = $request->orderId;
                                    $data->receiver_id = $this->user->id;
                                    $data->sender_id = $this->user->id;
                                    $data->credit_amt = $request->credit_point;
                                    $data->debit_amt = 0;
                                    $data->balance_amt = ($balance->amount + $request->credit_point);
                                    $data->singal_credit_pur_amt = ($request->recharge_amount / $request->recharge_duration);
                                    $data->total_credit_pur_amt = $request->recharge_amount;
                                    $data->type = 'Account Recharge Payment';
                                    $data->status = 1;
                                    $data->payment_mode = 'Credit Wallet';
                                    $data->transaction_description = 'You pay ' . $request->recharge_amount . 'credit points.';
                                    $data->save();

                                    CreditWallet::where('user_id', $this->user->id)->update(['amount' => $balance_amount]);

                                    $this->rechargeCommission($request, $request->orderId);

                                    if ($request->has('plan_id') && $request->has('reward')) {
                                        /* 26-11-2020:1300 Tusar ne add karavaya reward to reward table */
                                        $reward = new Rewards;
                                        $reward->plan_id = $request->plan_id;
                                        $reward->user_id = $this->user->id;
                                        $reward->reward = $request->reward;
                                        $reward->action = 'Credit';
                                        $reward->save();
                                    }


                                    User::where('id', $this->user->id)->update(['status' => 1, 'expire_date' => $request->expire_date]);

                                    $re_plan = RechargePlan::select('*')->where('id', $request->plan_id)->first();
                                    $addonData['user_id'] = $this->user->id;
                                    $addonData['plan_id'] = $re_plan->recharge_plan_type;
                                    $addonData['recharge_plan_id'] = $request->plan_id;
                                    $addonData['expire_date'] = $request->expire_date;
                                    $addonData['is_addon'] = 1;
                                    $planvalidity = $re_plan->recharge_plan_validity;
                                    if ($re_plan->recharge_plan_type == config('constants.plans.basic')) {

                                        $existing_plan = UserAddonPlans::select('*')
                                                ->where('user_id', $this->user->id)
                                                ->where('is_addon', 0)
                                                ->orderBy('id', 'desc')
                                                ->first();
                                        if (!empty($existing_plan)) {
                                            $expiry_date = Carbon::parse($existing_plan->expire_date)->addDays($planvalidity);
                                            $addonData['start_date'] = $request->start_date;
                                            $addonData['expire_date'] = $request->expire_date;
                                            $addonData['is_addon'] = 0;
                                        } else {
                                            $addonData['start_date'] = $request->start_date;
                                            $addonData['expire_date'] = $request->expire_date;
                                            $addonData['is_addon'] = 0;
                                        }
                                    } else {
                                        $addonData['start_date'] = Carbon::now();
                                        $addonData['expire_date'] = Carbon::now()->addDays($planvalidity);
                                    }

                                    UserAddonPlans::Create($addonData);
//                                    $re_plan = RechargePlan::select('*')->where('id', $request->plan_id)->first();
//
//                                    $addonData['user_id'] = $this->user->id;
//                                    $addonData['plan_id'] = $re_plan->recharge_plan_type;
//                                    $addonData['recharge_plan_id'] = $request->plan_id;
//                                    $addonData['expire_date'] = $request->expire_date;
//                                    $addonData['is_addon'] = 1;
//                                    UserAddonPlans::Create($addonData);
                                }
                                $purchase['responseCode'] = "200";
                                $purchase['responseMessage'] = "Success";
                                $purchase['responseDescription'] = "Your monthly recharge has been done successfully";
                                $user = DB::table('users')
                                        ->select('*')
                                        ->where('users.id', $this->user->id)
                                        ->first();
                                $invoice_no = $request->orderId;
                                $message = "Thank you! Your monthly recharge of " . $request->recharge_amount . ".rs has been done successfully. Invoice No." . $invoice_no;
                                $sms = json_decode($this->send_sms($this->user->phone, $message), true);
                                return $purchase;
                            } else {
                                $purchase['responseCode'] = "100";
                                $purchase['responseMessage'] = "Error";
                                $purchase['responseDescription'] = "Sorry your payment is not completed. Fail";
                                return $purchase;
                            }
                        });
                return is_null($exception) ? true : $exception;
            } catch (Exception $e) {
                $purchase['responseCode'] = "100";
                $purchase['responseMessage'] = "Error";
                $purchase['responseDescription'] = "Sorry your request is not completed. catch Error";
                return $purchase;
            }
        } else {
            $purchase['responseCode'] = "401";
            $purchase['responseMessage'] = "Error";
            $purchase['responseDescription'] = "Sorry, You are not authorized";
            return $purchase;
        }
    }

    public function rechargeCommission($data, $invoice_no) {
        $sponsor_id = $this->user->referred_by;
        $sponsor_data = User::where('referral_id', $sponsor_id)->first();

        $plan = RechargePlan::where('id', $data->plan_id)->first();
        $plan_id = $plan->recharge_plan_type ?? 0;
        $comData = DB::table('commission-setting')
                ->where([['user_id', $sponsor_data->id],
                    ['status', 1],
                    ['commission_plan_id_from', $sponsor_data->current_plan],
                    ['commission_plan_id_to', $plan_id]
                ])
                ->first();
        $commission_type = $comData->commission_type ?? 0;
        $status = $comData->status ?? 0;
        $commission = $comData->commission ?? 0;
        if (!$comData) {
            $comData = DB::table('plans_commission')
                    ->where([
                        ['commission_plan_id_from', $sponsor_data->current_plan],
                        ['commission_plan_id_to', $plan_id]
                    ])
                    ->first();
            $commission_type = $comData->discount_type ?? 0;
            $status = 1;
            $commission = $comData->discount_value ?? 0;
        }

        if (!$comData) {
            //$comData = DB::table('commission-setting')->where('user_id',NULL)->first();
            $comData = DB::table('commission-setting')->first();
            $commission_type = $comData->commission_type ?? 0;
            $status = $comData->status ?? 0;
            $commission = $comData->commission ?? 0;
        }

        if ($status == 1) {
            $date = date("Y-m-d H:i:s");
            $sponsor_joining_date = $sponsor_data->created_at;
            $commission_duration = $comData->recharge_month ?? 0;
            $commission_date = date("Y-m-d H:i:s", strtotime("+" . $commission_duration . " years", strtotime($sponsor_joining_date)));
            $comDays = $this->daysInterval($date, $commission_date);
            $recharge_amount = isset($data->recharge_amount) ? $data->recharge_amount : $data->recharge_plan_amount;
            $invoice_credit = $recharge_amount;
            if ($commission_type == 2) {
                $commission_credit = ($recharge_amount * $commission) / 100; //percentage
            } else {
                $commission_credit = $commission; //fixed
            }
            $creditConversion = DB::table('credit-point-setting')->first();
            $creditConversionAmount = $creditConversion->amount;
            $invoice_amt = $invoice_credit * $creditConversionAmount;
            $commission_amt = $commission_credit * $creditConversionAmount;
//            if ($comDays > 0) {
            if ($sponsor_data->status == 1) {

                $cash = new CashWallet();
                $balance = $cash->retailerCashBalance();
                if ($balance->amount > 0) {
                    $balance_amount = $balance->amount + $commission_amt;
                } else {
                    $balance_amount = $commission_amt;
                }

                $data = new CashWalletTransaction;
                $data->invoice_no = $invoice_no;
                $data->receiver_id = $sponsor_data->id;
                $data->sender_id = $this->user->id;
                $data->credit_amt = $commission_amt;
                $data->debit_amt = 0;
                $data->balance_amt = $balance_amount;
                $data->type = 'Recharge Commission';
                $data->payment_mode = 'Credit Wallet';
                $data->transaction_description = 'You get recharge commission ' . env('CURRENCY_SYMBOL') . ' ' . $commission_amt;

                if ($data->save()) {
                    CashWallet::where('user_id', $this->user->id)->update(['amount' => $balance_amount]);

                    $commissionData = new RechargeCommission;
                    $commissionData->user_id = $sponsor_data->id;
                    $commissionData->sender_id = $this->user->id;
                    $commissionData->invoice_no = $invoice_no;
                    $commissionData->commission_per = $commission;
                    $commissionData->invoice_amt = $invoice_amt;
                    $commissionData->commission_amt = $commission_amt;
                    $commissionData->invoice_credit = $invoice_credit;
                    $commissionData->commission_credit = $commission_credit;
                    $commissionData->type = 'Recharge Commission';
                    $commissionData->description = 'You get recharge commission ' . env('CURRENCY_SYMBOL') . ' ' . $commission_amt;
                    $commissionData->status = 1;
                    $commissionData->save();
                    //dd($commissionData);
                }
                //dd($data);
            } else {

                $commissionData = new RechargeCommission;
                $commissionData->user_id = $sponsor_data->id;
                $commissionData->sender_id = $this->user->id;
                $commissionData->invoice_no = $invoice_no;
                $commissionData->commission_per = $comData->commission;
                $commissionData->invoice_amt = $invoice_amt;
                $commissionData->commission_amt = $commission_amt;
                $commissionData->invoice_credit = $invoice_credit;
                $commissionData->commission_credit = $commission_credit;
                $commissionData->type = 'Recharge Commission';
                $commissionData->description = 'You get recharge commission ' . env('CURRENCY_SYMBOL') . ' ' . $commission_amt;
                $commissionData->status = 0;
                $commissionData->save();

                $data = new Log;
                $data->user_id = $sponsor_data->id;
                $data->type = 'commission';
                $data->description = "account status is not active";
                $data->save();
            }
//            } else {
//                $data = new Log;
//                $data->user_id = $sponsor_data->id;
//                $data->type = 'commission';
//                $data->description = "Commission year expire now, thats why this user not get commission";
//                $data->save();
//            }
        } else {
            $data = new Log;
            $data->user_id = $sponsor_data->id;
            $data->type = 'commission';
            $data->description = "Commission status is not active";
            $data->save();
        }
        return $commissionData;
    }

    /* naren */

    public function monthlyRechargePaymentNew(Request $request) {

        if ($this->user) {

            $validator = Validator::make($request->all(), [
                        'payment_method' => 'required',
                        'recharge_plan_id' => 'required'
            ]);

            $temp = $validator->errors()->all();

            if ($validator->fails()) {
                $purchase['responseCode'] = "100";
                $purchase['responseMessage'] = "Error";
                $purchase['responseDescription'] = $temp;
                return $purchase;
            }

            $recharge_plan = RechargePlan::where('id', $request->recharge_plan_id)->where('recharge_plan_status', 1)->orderBy('id', 'DESC')->first();

            if (empty($recharge_plan)) {
                $purchase['responseCode'] = "100";
                $purchase['responseMessage'] = "Error";
                $purchase['responseDescription'] = "Sorry your request plan is not found.";
                return $purchase;
            }

            if ($request->payment_method == 'credit_wallet') {
                try {

                    $exception = DB::transaction(function () use($request, $recharge_plan) {

                                $invoice_no = monthlyRechargeInvoiceNumber();
                                $credit = new CreditWallet();
                                $credit = $credit->retailerCreditBalance();

                                if ($credit->amount >= $recharge_plan->recharge_plan_amount) {
                                    $balance_amount = $credit->amount - $recharge_plan->recharge_plan_amount;

                                    if ($recharge_plan->recharge_plan_validity > 0) {

                                        $data = new CreditWalletTransaction;
                                        $data->invoice_no = $invoice_no;
                                        $data->receiver_id = $this->user->id;
                                        $data->sender_id = $this->user->id;
                                        $data->credit_amt = 0;
                                        $data->debit_amt = $recharge_plan->recharge_plan_amount;
                                        $data->balance_amt = $balance_amount;
                                        $data->singal_credit_pur_amt = ($recharge_plan->recharge_plan_amount / $recharge_plan->recharge_plan_validity);
                                        $data->total_credit_pur_amt = $recharge_plan->recharge_plan_amount;
                                        $data->type = 'Account Recharge Payment';
                                        $data->status = 1;
                                        $data->payment_mode = 'Credit Wallet';
                                        $data->transaction_description = 'You pay ' . $recharge_plan->recharge_plan_amount . 'credit points.';

                                        if ($data->save()) {

                                            CreditWallet::where('user_id', $this->user->id)->update(['amount' => $balance_amount]);

                                            $current = \Carbon\Carbon::now();
                                            $expire_date = $current->addDays($recharge_plan->recharge_plan_validity);

                                            $accRecharge = new AccountRecharge;
                                            $accRecharge->user_id = $this->user->id;
                                            $accRecharge->recharge_duration = $recharge_plan->recharge_plan_validity;
                                            $accRecharge->recharge_amount = $recharge_plan->recharge_plan_amount;
                                            $accRecharge->expire_date = $expire_date;
                                            $accRecharge->payment_mode = 1;
                                            $accRecharge->invoice_no = $invoice_no;
                                            $accRecharge->status = 1;

                                            if ($accRecharge->save()) {

                                                $com = $this->rechargeCommission($recharge_plan, $invoice_no);
                                                if ($com) {
                                                    User::where('id', $this->user->id)->update(['status' => 1, 'expire_date' => $expire_date]);
                                                }
                                            }
                                        }
                                    } else {
                                        $purchase['responseCode'] = "100";
                                        $purchase['responseMessage'] = "Error";
                                        $purchase['responseDescription'] = "Please select recharge duration.";
                                        return $purchase;
                                    }
                                } else {
                                    $purchase['responseCode'] = "100";
                                    $purchase['responseMessage'] = "Error";
                                    $purchase['responseDescription'] = "You have not sufficient balance in credit wallet.";
                                    return $purchase;
                                }

                                $purchase['responseCode'] = "200";
                                $purchase['responseMessage'] = "Success";
                                $purchase['responseDescription'] = "Your monthly recharge has been done successfully";
                                $user = DB::table('users')
                                        ->select('*')
                                        ->where('users.id', $this->user_id)
                                        ->first();
                                $message = "Thank you! Your monthly recharge of " + $request->recharge_amount + ".rs has been done successfully. Invoice No." + $invoice_no;
                                $sms = json_decode($this->send_sms($user->phone, $message), true);
                                return $purchase;
                            });

                    return is_null($exception) ? true : $exception;
                } catch (Exception $e) {
                    //return false;
                    $purchase['responseCode'] = "100";
                    $purchase['responseMessage'] = "Error";
                    $purchase['responseDescription'] = "Sorry your request is not completed.";
                    return $purchase;
                }
            } else if ($request->payment_method == 'cash_free') {

                $invoice_no = monthlyRechargeInvoiceNumber();

                $recharge_amount_after_conversion = $recharge_plan->recharge_plan_amount;

                $api_response = json_decode($this->getCreditPurchaseToken($recharge_amount_after_conversion, $invoice_no), true);

                if ($api_response['status'] == "OK") {

                    $current = \Carbon\Carbon::now();
                    $expire_date = $current->addDays($recharge_plan->recharge_plan_validity);

                    $accRecharge = new AccountRecharge;
                    $accRecharge->user_id = $this->user->id;
                    $accRecharge->recharge_duration = $recharge_plan->recharge_plan_validity;
                    $accRecharge->recharge_amount = $recharge_plan->recharge_plan_amount;
                    $accRecharge->expire_date = $expire_date;
                    $accRecharge->payment_mode = 1;
                    $accRecharge->invoice_no = $invoice_no;
                    $accRecharge->status = 1;

                    if ($accRecharge->save()) {

                        $com = $this->rechargeCommission($recharge_plan, $invoice_no);
                        if ($com) {
                            User::where('id', $this->user->id)->update(['status' => 1, 'expire_date' => $expire_date]);
                        }
                    }
                }
                $purchase['responseCode'] = "200";
                $purchase['responseMessage'] = "Success";
                $purchase['cashfree_app_id'] = env('CASHFREE_APP_ID');
                $purchase['orderId'] = $invoice_no;
                $purchase['api_data'] = $api_response;
                $purchase['request_data'] = $request->all();

                return $purchase;
            }

            $purchase['responseCode'] = "100";
            $purchase['responseMessage'] = "Error";
            $purchase['responseDescription'] = "Sorry your request is not completed.";
            return $purchase;
        } else {

            $purchase['responseCode'] = "401";
            $purchase['responseMessage'] = "Error";
            $purchase['responseDescription'] = "Sorry, You are not authorized";
            return $purchase;
        }
    }

    public function getRechargeCommissionReport() {
        if ($this->user) {

            $data = RechargeCommission::with('incomeReceiver', 'creditPurchaser')->where('user_id', $this->user->id)->orderBy('id', 'desc')->get();
            if ($data) {
                $commission_report['responseCode'] = "200";
                $commission_report['responseMessage'] = "Success";
                $commission_report['responseDescription'] = "Success";
                $commission_report['data'] = $data;
            } else {
                $commission_report['responseCode'] = "100";
                $commission_report['responseMessage'] = "Success";
                $commission_report['responseDescription'] = "No data found";
            }
        } else {
            $commission_report['responseCode'] = "401";
            $commission_report['responseMessage'] = "Error";
            $commission_report['responseDescription'] = "Sorry, You are not authorized";
        }
        return $commission_report;
    }

    public function getActivePlan() {

        if ($this->user) {

            $user_id = $this->user->id;
            if ($this->user->parent_user_id > 0) {
                $user_id = $this->user->parent_user_id;
            }
            $addonrecharges = UserAddonPlans::select('*')
//                    ->where('is_addon', 1)
                    ->where('user_id', $user_id)
                    ->where('expire_date', '>', Carbon::now())
                    ->orderBy('plan_id')
                    ->get();

            $addonplans = [];
            if (!empty($addonrecharges)) {
                foreach ($addonrecharges as $key => $value) {
                    $addon = RechargePlan::select('recharge_plans.*', DB::raw("IFNULL(plans.name,'') as plan_name"))
                            ->leftjoin('plans', 'plans.id', '=', 'recharge_plans.recharge_plan_type')
                            ->where('recharge_plans.id', $value['recharge_plan_id'])
                            ->first();
                    if (!empty($addon)) {
                        $addon = $addon->toArray();
                        $addon['start_date'] = $value['start_date'];
                        $addon['expire_date'] = $value['expire_date'];
                        $addon['is_addon'] = $value['is_addon'];
                        $addonplans[] = $addon;
                    }
                }
            }

//            $wallet_detail['current_plan'] = $validity;
            $wallet_detail['current_plans'] = $addonplans;

            if ($wallet_detail) {
                $data['responseCode'] = "200";
                $data['responseMessage'] = "Success";
                $data['data'] = $wallet_detail;
            } else {
                $data['responseCode'] = "100";
                $data['responseMessage'] = "Success";
                $data['responseDescription'] = "No data found";
            }
        } else {
            $data['responseCode'] = "401";
            $data['responseMessage'] = "Error";
            $data['responseDescription'] = "Sorry, You are not authorized";
        }

        return $data;
    }

    public function getExpiredPlan() {

        if ($this->user) {

            $user_id = $this->user->id;
            if ($this->user->parent_user_id > 0) {
                $user_id = $this->user->parent_user_id;
            }

            $addonrecharges = UserAddonPlans::select('*')
                    ->where('user_id', $user_id)
                    ->where('expire_date', '<', Carbon::now())
                    ->orderBy('plan_id')
                    ->get();

            $expiredplans = [];
            if (!empty($addonrecharges)) {
                foreach ($addonrecharges as $key => $value) {
//                    $addon = RechargePlan::select('*')->where('id', $value['recharge_plan_id'])->first()->toArray();
                    $addon = RechargePlan::select('recharge_plans.*', DB::raw("IFNULL(plans.name,'') as plan_name"))
                                    ->leftjoin('plans', 'plans.id', '=', 'recharge_plans.recharge_plan_type')
                                    ->where('recharge_plans.id', $value['recharge_plan_id'])
                                    ->first();
                    if (!empty($addon)) {
                        $addon = $addon->toArray();
                        $addon['start_date'] = $value['start_date'];
                        $addon['expire_date'] = $value['expire_date'];
                        $addon['is_addon'] = $value['is_addon'];
                        $expiredplans[] = $addon;
                    }
                }
            }

            $wallet_detail['expired_plans'] = $expiredplans;

            if ($wallet_detail) {
                $data['responseCode'] = "200";
                $data['responseMessage'] = "Success";
                $data['data'] = $wallet_detail;
            } else {
                $data['responseCode'] = "100";
                $data['responseMessage'] = "Success";
                $data['responseDescription'] = "No data found";
            }
        } else {
            $data['responseCode'] = "401";
            $data['responseMessage'] = "Error";
            $data['responseDescription'] = "Sorry, You are not authorized";
        }

        return $data;
    }

    public function getUpcomingPlan() {

        if ($this->user) {

            $upcoming_plans = RechargePlan::select('recharge_plans.*', DB::raw("IFNULL(plans.name,'') as plan_name"))
                            ->leftjoin('plans', 'plans.id', '=', 'recharge_plans.recharge_plan_type')
                            ->where('recharge_plans.recharge_plan_status',1)
                            ->orderBy('plans.id')
                            ->get()->toArray();

            $wallet_detail['upcoming_plans'] = $upcoming_plans;

            if ($wallet_detail) {
                $data['responseCode'] = "200";
                $data['responseMessage'] = "Success";
                $data['data'] = $wallet_detail;
            } else {
                $data['responseCode'] = "100";
                $data['responseMessage'] = "Success";
                $data['responseDescription'] = "No data found";
            }
        } else {
            $data['responseCode'] = "401";
            $data['responseMessage'] = "Error";
            $data['responseDescription'] = "Sorry, You are not authorized";
        }

        return $data;
    }

    public function getPlanPrice(Request $request) {

        if ($this->user) {
            $validator = Validator::make($request->all(), [
                        'plan_id' => 'required',
            ]);

            if ($validator->fails()) {
                $purchase['responseCode'] = "100";
                $purchase['responseMessage'] = "Error";
                $purchase['responseDescription'] = "Please select plan";
            } else {
                $post = $request->all();
                $user_id = $this->user->id;
                if ($this->user->parent_user_id > 0) {
                    $user_id = $this->user->parent_user_id;
                }

                $plan = RechargePlan::select('*')
                                ->where('id', $post['plan_id'])
                                ->first()->toArray();


                $basic = UserAddonPlans::select('*')
//                        ->where('is_addon', 0)
                        ->where('user_id', $user_id)
                        ->where('plan_id', $plan['recharge_plan_type'])
                        ->orderBy('id', 'desc')
                        ->first();


                $plan_price = $plan['recharge_plan_amount'];
                $plan_per_day_price = $plan['recharge_plan_per_day_cost'];
                if (!empty($basic)) {
                    $expire_date = \Carbon\Carbon::parse($basic->expire_date);
                    $start_date = \Carbon\Carbon::parse(Carbon::now());
                    $expire_year = \Carbon\Carbon::parse($basic->expire_date)->format('Y');
                    $next_year = $expire_year + 1;
                    $end_date = \Carbon\Carbon::parse('31-12-' . $expire_year . ' 23:59:59');
                    if (\Carbon\Carbon::parse($basic->expire_date)->format('m-d') == '12-31') {
                        $start_date = \Carbon\Carbon::parse('1-1-' . $next_year . '00:00:00');
                        $end_date = \Carbon\Carbon::parse('31-12-' . $next_year . ' 23:59:59');
                    } else {
                        $start_date = $expire_date;
                        $different_days = $start_date->diffInDays($end_date);
                        if ($plan['recharge_plan_validity'] > $different_days) {
                            $plan_price = $different_days * $plan_per_day_price;
                        }
                        $start_date = $expire_date->addDays(1);
                    }
                } else {
                    $start_date = \Carbon\Carbon::parse(Carbon::now());
                    $year = Carbon::now()->format('Y');
                    $end_date = \Carbon\Carbon::parse('31-12-' . $year . ' 23:59:59');

                    $different_days = $start_date->startOfDay()->diffInDays($end_date);
                    if ($plan['recharge_plan_validity'] > $different_days) {
                        $plan_price = $different_days * $plan_per_day_price;
                    }
                }

                $price = number_format($plan_price, 2);
                $wallet_detail['amount'] = str_replace(',', '', $price);
                $wallet_detail['start_date'] = \Carbon\Carbon::parse($start_date)->format('Y-m-d 00:00:00');
                $wallet_detail['expire_date'] = \Carbon\Carbon::parse($end_date)->format('Y-m-d H:i:s');

                $data['responseCode'] = "200";
                $data['responseMessage'] = "Success";
                $data['data'] = $wallet_detail;
            }
        } else {
            $data['responseCode'] = "401";
            $data['responseMessage'] = "Error";
            $data['responseDescription'] = "Sorry, You are not authorized";
        }

        return $data;
    }

    public function checkExpiryDate(Request $request) {

        if ($this->user) {

            $validator = Validator::make($request->all(), [
                        'image_id' => 'required',
            ]);

            if ($validator->fails()) {
                $purchase['responseCode'] = "100";
                $purchase['responseMessage'] = "Error";
                $purchase['responseDescription'] = "Please select plan";
            } else {

                $user_id = $this->user->id;
                if ($this->user->parent_user_id > 0) {
                    $user_id = $this->user->parent_user_id;
                }

                $post = $request->all();
                $image_id = $post['image_id'];
                $image_data = ModelScreenImage::select('msi.*', 'pbt.plan_id')
                                ->from('model_screen_images as msi')
                                ->leftJoin('brand_model_screen_images as bmsi', 'bmsi.id', '=', 'msi.brand_model_screen_image_id')
                                ->leftJoin('plan_brandtypes as pbt', 'pbt.brand_type_id', '=', 'bmsi.brand_type_id')
                                ->where('msi.id', $image_id)
                                ->first()->toArray();


                $lastRecharge = UserAddonPlans::select('*')
                        ->where('user_id', $user_id)
                        ->where('plan_id', $image_data['plan_id'])
                        ->orderBy('id', 'desc')
                        ->first();

                $valid = 0;
                if (!empty($lastRecharge)) {
                    $start_date = \Carbon\Carbon::parse(Carbon::now());
                    $end_date = \Carbon\Carbon::parse($lastRecharge->expire_date);

                    if ($end_date < $start_date) {
                        $valid = 2;
                    } else {
                        $different_days = $start_date->diffInDays($end_date);
                        if ($different_days <= 3) {
                            $valid = 1;
                        }
                    }
                }

                $data['responseCode'] = "200";
                $data['responseMessage'] = "Success";
                $data['status'] = $valid;
                $data['expire_date'] = $lastRecharge->expire_date ?? "";
            }
        } else {
            $data['responseCode'] = "401";
            $data['responseMessage'] = "Error";
            $data['responseDescription'] = "Sorry, You are not authorized";
        }

        return $data;
    }

}
