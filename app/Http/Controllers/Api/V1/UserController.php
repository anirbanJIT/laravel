<?php



namespace App\Http\Controllers\Api\V1;



use JWTAuth;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Models\UserContactDetail;

use App\Models\UserBankDetail;

use App\Models\ShopDetail;

use App\User;

use App\Models\UserKycDetail;

use App\Models\UserProfileChangeRequest;

use App\Models\KycCategory;

use DB;

use Validator;

use App\Models\RechargeCommission;

use App\Models\UserLevel;

use App\Models\CreditWallet;

use App\Models\CashWallet;

use App\Models\Customer;

use App\Models\ComplaintFeedback;

use App\Models\GeneralNotification;

use App\Models\PayoutRequest;

use Exception;

use \Illuminate\Support\Carbon;

use PDF;



class UserController extends Controller
{



    /**

     * @var

     */

    protected $user;



    /**

     * TaskController constructor.

     */

    // public function __construct()
    // {
    //     $this->user = JWTAuth::parseToken()->authenticate();
    //     return $this->user;
    // }



    public function getUserContactDetail(Request $request)
    {

        //return $this->user->id;

        if ($this->user) {

            $userContactDetails = UserContactDetail::select('id', 'user_id', 'address', 'city', 'state', 'country', 'pin_code')->where('user_id', $this->user->id)->get()->toArray();



            $userShopDetail = $this->user->getShopDetail()->first();



            if ($userShopDetail) {

                $userShopDetail->shop_gallery = isset($userShopDetail->shop_gallery) ? json_decode($userShopDetail->shop_gallery, true) : [];

                $userShopDetail->shop_gallery = implode(',', $userShopDetail->shop_gallery);
            } else {

                $userShopDetail = [

                    'user_id' => $this->user->id,

                    'shop_name' => $this->user->shop_name,

                    'shop_photo' => '',

                    'shop_gallery' => json_encode([], JSON_FORCE_OBJECT),

                    'address' => null,

                    'state' => null,

                    'city' => null,

                    'country_id' => null,

                    'pin_code' => null

                ];

                ShopDetail::create($userShopDetail);

                $userShopDetail = $this->user->getShopDetail()->first();
            }



            if ($userContactDetails) {

                $contactDetails['responseCode'] = "200";

                $contactDetails['responseMessage'] = "Success";

                $contactDetails['data'] = $userContactDetails;

                $contactDetails['shop_data'] = !empty($userShopDetail) ? $userShopDetail : [];

                $contactDetails['shop_base_url'] = url('/images/shop/');
            } else {

                $contactDetails['responseCode'] = "100";

                $contactDetails['responseMessage'] = "Success";

                $contactDetails['responseDescription'] = "No data found";
            }
        } else {

            $contactDetails['responseCode'] = "401";

            $contactDetails['responseMessage'] = "Error";

            $contactDetails['responseDescription'] = "Sorry, You are not authorized";
        }

        return $contactDetails;
    }



    /*     * **** Update user contact detail **** */



    public function UpdateUserContactDetail(Request $request)
    {



        if ($this->user) {

            $validator = Validator::make($request->all(), [

                'address' => 'required',

                'country' => 'required',

                'state' => 'required',

                'city' => 'required',

                'pin_code' => 'min:6'

            ]);

            $temp = $validator->errors()->all();

            if ($validator->fails()) {

                $error['responseCode'] = "101";

                $user_reg['responseMessage'] = "Error";

                $user_reg['responseDescription'] = $temp;

                return $user_reg;

                //return Redirect::back()->withErrors($validator);

            } else {



                $user = User::findOrFail($request->user_id);



                $userContDetail = $user->getContactDetail()->first();

                if (!empty($userContDetail)) {

                    $userContactupdate = $userContDetail->fill($request->all())->save();
                } else {

                    $userContactupdate = UserContactDetail::create($request->all());
                }



                if ($userContactupdate) {

                    $contactupdate['responseCode'] = "200";

                    $contactupdate['responseMessage'] = "Success";

                    $contactupdate['data'] = $userContactupdate;
                } else {

                    $contactupdate['responseCode'] = "100";

                    $contactupdate['responseMessage'] = "Success";

                    $contactupdate['responseDescription'] = "No data found";
                }
            }
        } else {

            $contactupdate['responseCode'] = "401";

            $contactupdate['responseMessage'] = "Error";

            $contactupdate['responseDescription'] = "Sorry, You are not authorized";
        }



        return $contactupdate;
    }



    /*     * **** send User Profile Change Request **** */



    public function sendUserProfileChangeRequest(Request $request)
    {



        if ($this->user) {

            $validator = Validator::make($request->all(), [

                'user_id' => 'required',

                'country' => 'required',

                'email' => 'required',

                'state' => 'required',

                'phone' => 'required',

            ]);

            $temp = $validator->errors()->all();



            if ($validator->fails()) {



                $error['responseCode'] = "101";

                $user_reg['responseMessage'] = "Error";

                $user_reg['responseDescription'] = $temp;

                return $user_reg;
            } else {



                $userProfileChangeRequest = UserProfileChangeRequest::updateOrCreate($request->all());



                if ($userProfileChangeRequest) {

                    $data = UserProfileChangeRequest::find($userProfileChangeRequest->id);

                    $contactupdate['responseCode'] = "200";

                    $contactupdate['responseMessage'] = "Success";

                    $contactupdate['data'] = $data;
                } else {



                    $error['responseCode'] = "101";

                    $user_reg['responseMessage'] = "Error";

                    $contactupdate['responseDescription'] = "No data found";
                }
            }
        } else {

            $contactupdate['responseCode'] = "401";

            $contactupdate['responseMessage'] = "Error";

            $contactupdate['responseDescription'] = "Sorry, You are not authorized";
        }



        return $contactupdate;
    }



    public function getUserProfileChangeRequestStatus($user_id)
    {

        if ($this->user) {



            $data = UserProfileChangeRequest::where('user_id', $user_id)->first();



            if ($data) {



                $contactupdate['responseCode'] = "200";

                $contactupdate['responseMessage'] = "Success";

                $contactupdate['data'] = $data;
            } else {



                $error['responseCode'] = "101";

                $user_reg['responseMessage'] = "Error";

                $contactupdate['responseDescription'] = "No data found";
            }
        } else {

            $contactupdate['responseCode'] = "401";

            $contactupdate['responseMessage'] = "Error";

            $contactupdate['responseDescription'] = "Sorry, You are not authorized";
        }



        return $contactupdate;
    }



    //Banks details

    public function getUserBankDetail(Request $request)
    {

        //return $this->user->id;

        if ($this->user) {

            $userBankDetails = UserBankDetail::select('id', 'user_id', 'bank_name', 'account_no', 'branch_name', 'account_name', 'ifsc_code', 'account_type', 'mobile_number')->where('user_id', $this->user->id)->get()->toArray();

            //return $brands_data;

            if ($userBankDetails) {

                $bankDetails['responseCode'] = "200";

                $bankDetails['responseMessage'] = "Success";

                $bankDetails['data'] = $userBankDetails;
            } else {

                $bankDetails['responseCode'] = "100";

                $bankDetails['responseMessage'] = "Success";

                $bankDetails['responseDescription'] = "No data found";
            }
        } else {

            $bankDetails['responseCode'] = "401";

            $bankDetails['responseMessage'] = "Error";

            $bankDetails['responseDescription'] = "Sorry, You are not authorized";
        }

        return $bankDetails;
    }



    /*     * **** Update user bank detail **** */



    public function UpdateUserBanktDetail(Request $request)
    {



        if ($this->user) {

            $validator = Validator::make($request->all(), [

                'bank_name' => 'required',

                'account_no' => 'required',

                'branch_name' => 'required',

                'account_name' => 'required',

                'ifsc_code' => 'min:11',

                'account_type' => 'required'

            ]);

            $temp = $validator->errors()->all();

            if ($validator->fails()) {

                $bankUpdate['responseCode'] = "101";

                $bankUpdate['responseMessage'] = "Error";

                $bankUpdate['responseDescription'] = implode("\n", $temp);

                return $bankUpdate;
            }



            $userBankDetail = $this->user->getBankDetail()->first();

            $all_inputs = $request->all();

            $all_inputs['user_id'] = $this->user->id;



            /* Start add beneficiary cash free */

            $contact_detail = $this->user->getContactDetail;

            if (!$contact_detail) {

                $bankUpdate['responseCode'] = "100";

                $bankUpdate['responseMessage'] = "Success";

                $bankUpdate['responseDescription'] = "Please first update your contact details";

                return $bankUpdate;
            }



            $tokenData = $this->getToken();

            if ($tokenData['status'] == 'SUCCESS' && $tokenData['subCode'] == 200) {

                $token = $tokenData['data']['token'];

                /* only add beneficiary */

                $stateName = DB::table('states')->where('id', $contact_detail->state)->select('name')->first();

                $name = strtok($this->user->name, ' ');

                $beneficiary = array(

                    'beneId' => $name . $this->user->id,

                    'name' => $request->account_name,

                    'email' => $this->user->email ?? $request->mobile_number . '@modernmart.in',

                    'phone' => $request->mobile_number,

                    'bankAccount' => $request->account_no,

                    'ifsc' => $request->ifsc_code,

                    'address1' => $contact_detail->address,

                    'city' => $contact_detail->city,

                    'state' => $stateName->name,

                    'pincode' => $contact_detail->pin_code,

                );

                /*   $isValidateBankDetails = $this->addBeneficiary($token,$beneficiary);

                  only validate account information open this comment */

                $account_details = 'name=' . urlencode($request->account_name) . '&phone=' . $request->mobile_number . '&bankAccount=' . $request->account_no . '&ifsc=' . $request->ifsc_code;

                $isValidateBankDetails = $this->validateBankDetails($token, $account_details);

                if (empty($isValidateBankDetails)) {

                    $bankUpdate['responseCode'] = "100";

                    $bankUpdate['responseMessage'] = "ERROR";

                    $bankUpdate['responseDescription'] = 'Something went wrong. Try again!';

                    return $bankUpdate;
                }



                if ($isValidateBankDetails['status'] == 'SUCCESS' && $isValidateBankDetails['subCode'] == 200) {

                    if (!empty($userBankDetail)) {

                        $userBankUpdate = $userBankDetail->fill($all_inputs)->save();
                    } else {

                        $userBankUpdate = UserBankDetail::create($all_inputs);
                    }

                    $bankUpdate['responseCode'] = "200";

                    $bankUpdate['responseMessage'] = $isValidateBankDetails['message']; //'Account Verified and beneficiary added successfully';

                    $bankUpdate['data'] = $userBankUpdate;

                    return $bankUpdate;
                } else if ($isValidateBankDetails['status'] == 'ERROR' && $isValidateBankDetails['subCode'] == 409) {



                    if (!empty($userBankDetail)) {

                        $userBankUpdate = $userBankDetail->fill($all_inputs)->save();
                    } else {

                        $userBankUpdate = UserBankDetail::create($all_inputs);
                    }

                    $bankUpdate['responseCode'] = "200";

                    $bankUpdate['responseMessage'] = "Success";

                    $bankUpdate['responseMessage'] = "Entered bank Account is already registered";

                    $bankUpdate['data'] = $userBankUpdate;

                    return $bankUpdate;
                } else {

                    $bankUpdate['responseCode'] = "100";

                    $bankUpdate['responseMessage'] = $isValidateBankDetails['message'];

                    return $bankUpdate;
                }
            }



            $bankUpdate['responseCode'] = "100";

            $bankUpdate['responseMessage'] = "ERROR";

            $bankUpdate['responseDescription'] = $tokenData['message'];

            return $bankUpdate;
        } else {

            $bankUpdate['responseCode'] = "401";

            $bankUpdate['responseMessage'] = "Error";

            $bankUpdate['responseDescription'] = "Sorry, You are not authorized";

            return $bankUpdate;
        }



        //return $bankUpdate;

    }



    //  //KYC Type Api

    public function getKyctypeList()
    {



        if ($this->user) {

            $kycTypeList = DB::table('kyc_categories')->select('id', 'name')->get()->toArray();

            if ($kycTypeList) {

                $kycTypes['responseCode'] = "200";

                $kycTypes['responseMessage'] = "Success";

                $kycTypes['data'] = $kycTypeList;
            } else {

                $kycTypes['responseCode'] = "100";

                $kycTypes['responseMessage'] = "Success";

                $kycTypes['responseDescription'] = "No data found";
            }
        } else {

            $kycTypes['responseCode'] = "401";

            $kycTypes['reponseMessage'] = "Error";

            $kycTypes['responseDescription'] = "Sorry, You are not authrized";
        }



        return $kycTypes;
    }



    //KYC Api



    public function getUserKycDetail(Request $request)
    {

        //return $this->user->id;

        if ($this->user) {

            $userKycDetails = UserKycDetail::select('id', 'user_id', 'kyc_document', 'kyc_number', 'customer_name', 'relation', 'dob', 'phone', 'emailKyc', 'address', 'city', 'state_id', 'country_id', 'pin_code', 'attachment', 'kyc_expiry_date', 'status', 'notes')->where('user_id', $this->user->id)->get()->toArray();

            //return $brands_data;

            if ($userKycDetails) {

                $kycDetails['responseCode'] = "200";

                $kycDetails['responseMessage'] = "Success";

                $kycDetails['data'] = $userKycDetails;

                $kycDetails['url'] = "images/kyc/";
            } else {

                $kycDetails['responseCode'] = "100";

                $kycDetails['responseMessage'] = "Success";

                $kycDetails['responseDescription'] = "No data found";
            }
        } else {

            $kycDetails['responseCode'] = "401";

            $kycDetails['responseMessage'] = "Error";

            $kycDetails['responseDescription'] = "Sorry, You are not authorized";
        }

        return $kycDetails;
    }



    /*     * **** Update user KYC detail **** */



    public function UpdateUserKycDetail(Request $request)
    {

        // return $request->userId;

        if ($this->user) {

            $validator = Validator::make($request->all(), [

                'kyc_document' => 'required',

                'kyc_number' => 'required',

                //'kyc_expiry_date' => 'required',

                //'dob' => 'required',

                //'customer_name' => 'required',

                //'phone' => 'required',

                // 'pin_code' => 'required'

            ]);

            $temp = $validator->errors()->all();

            if ($validator->fails()) {

                $error['responseCode'] = "101";

                $user_reg['responseMessage'] = "Error";

                $user_reg['responseDescription'] = implode(',', $temp);

                return $user_reg;

                //return Redirect::back()->withErrors($validator);

            } else {



                $user = User::findOrFail($this->user->id);



                $userKycDetail = $user->getKycDetail()->first();



                if (!empty($userKycDetail)) {



                    if (!empty($request->attachment)) {

                        $attachment = time() . '.' . $request->attachment->getClientOriginalExtension();

                        $request->attachment->move(public_path('images/kyc'), $attachment);

                        $userKycDetail->attachment = $attachment;
                    }



                    $userKycDetail->kyc_document = $request->kyc_document;

                    $userKycDetail->kyc_number = $request->kyc_number;

                    $userKycDetail->kyc_expiry_date = !empty($request->kyc_expiry_date) ? $request->kyc_expiry_date : null;

                    $KycCreateUpdate = $userKycDetail->save();
                } else {

                    if (!empty($request->attachment)) {

                        $attachment = time() . '.' . $request->attachment->getClientOriginalExtension();

                        $request->attachment->move(public_path('images/kyc'), $attachment);
                    } else {

                        $attachment = '';
                    }



                    $userKyc = new UserKycDetail();

                    $userKyc->attachment = $attachment;

                    $userKyc->user_id = $this->user->id;

                    $userKyc->kyc_document = $request->kyc_document;

                    $userKyc->kyc_number = $request->kyc_number;

                    $userKyc->kyc_expiry_date = $request->kyc_expiry_date ?? null;

                    $userKyc->status = 0;

                    /*    $userKyc->dob = $request->dob;

                      $userKyc->customer_name = $request->customer_name;

                      $userKyc->phone = $request->phone;

                      $userKyc->emailKyc = $request->emailKyc;

                      $userKyc->address = $request->address;

                      $userKyc->country_id = $request->country_id;

                      $userKyc->state_id = $request->state_id;

                      $userKyc->city = $request->city;

                      $userKyc->pin_code = $request->pin_code; */

                    $KycCreateUpdate = $userKyc->save();
                }





                if ($KycCreateUpdate) {

                    $userKycDetail = $user->getKycDetail()->first();

                    $kycUrl = url('/images/kyc/' . $userKycDetail->attachment);

                    $kycUpdate['responseCode'] = "200";

                    $kycUpdate['responseMessage'] = "Success";

                    $kycUpdate['data'] = $userKycDetail;

                    //$kycUpdate['shop_data'] = $userShopDetail;

                    $kycUpdate['url'] = $kycUrl;

                    //$kycUpdate['shop_base_url'] = url('/images/shop/');

                } else {

                    $kycUpdate['responseCode'] = "100";

                    $kycUpdate['responseMessage'] = "Success";

                    $kycUpdate['responseDescription'] = "No data found";
                }
            }
        } else {

            $kycUpdate['responseCode'] = "401";

            $kycUpdate['responseMessage'] = "Error";

            $kycUpdate['responseDescription'] = "Sorry, You are not authorized";
        }



        return $kycUpdate;
    }



    /*     * **** Update user contact detail **** */



    public function UpdateUserShopDetail(Request $request)
    {

        if ($this->user) {



            $validator = Validator::make($request->all(), [

                'shop_name' => 'required',

                'address' => 'required',

                'country' => 'required',

                'state' => 'required',

                'city' => 'required',

                'pin_code' => 'min:6'

            ]);



            $temp = $validator->errors()->all();



            if ($validator->fails()) {

                $error['responseCode'] = "101";

                $user_reg['responseMessage'] = "Error";

                $user_reg['responseDescription'] = $temp;

                return $user_reg;
            } else {



                $user = $this->user;

                $user->shop_name = $request->shop_name;

                $user->save();



                $userShopDetail = $user->getShopDetail()->first();



                $all_inputs = $request->all();



                $all_inputs['country_id'] = $request->country;

                $all_inputs['user_id'] = $this->user->id;



                if (!empty($request->file('shop_photo'))) {

                    $file = $request->file('shop_photo');

                    $destinationPath = public_path('images/shop'); // upload path

                    $photo = time() . 'shop.' . $file->getClientOriginalExtension();

                    $file->move($destinationPath, $photo);

                    $all_inputs['shop_photo'] = $photo;
                } else {

                    $all_inputs['shop_photo'] = $userShopDetail->shop_photo;
                }

                if (!empty($request->file('machine_photo'))) {

                    $file = $request->file('machine_photo');

                    $destinationPath = public_path('images/shop'); // upload path

                    $photo = time() . 'machine.' . $file->getClientOriginalExtension();

                    $file->move($destinationPath, $photo);



                    $all_inputs['machine_photo'] = $photo;
                } else {

                    $all_inputs['machine_photo'] = $userShopDetail->machine_photo;
                }



                if ($request->file('shop_gallery')) {

                    $k = 1;

                    $dataImage = [];

                    foreach ($request->file('shop_gallery') as $file) {

                        $destinationPath = public_path('images/shop'); // upload path

                        $profileImage = time() . $k . '.' . $file->getClientOriginalExtension();

                        $file->move($destinationPath, $profileImage);

                        $dataImage[] = $profileImage;

                        $k++;
                    }

                    $all_inputs['shop_gallery'] = json_encode($dataImage, JSON_FORCE_OBJECT);
                } elseif (!empty($userShopDetail)) {

                    $all_inputs['shop_gallery'] = $userShopDetail->shop_gallery;
                }

                if (!empty($userShopDetail)) {

                    $ShopDetail = $userShopDetail->update($all_inputs);
                } else {

                    $ShopDetail = ShopDetail::create($all_inputs);
                }



                $userKycDetail = $user->getKycDetail()->first();

                if (empty($userKycDetail)) {

                    $userKyc = new UserKycDetail();

                    $userKyc->kyc_number = '';

                    $userKyc->user_id = $this->user->id;

                    $KycCreateUpdate = $userKyc->save();
                }



                if ($ShopDetail) {

                    $contactupdate['responseCode'] = "200";

                    $contactupdate['responseMessage'] = "Success";

                    $contactupdate['data'] = $ShopDetail;
                } else {

                    $contactupdate['responseCode'] = "100";

                    $contactupdate['responseMessage'] = "Success";

                    $contactupdate['responseDescription'] = "No data found";
                }
            }
        } else {

            $contactupdate['responseCode'] = "401";

            $contactupdate['responseMessage'] = "Error";

            $contactupdate['responseDescription'] = "Sorry, You are not authorized";
        }



        return $contactupdate;
    }



    //Country List Api

    public function getCountryList()
    {



        if ($this->user) {

            $countryList = DB::table('countries')->select('id', 'name')->get()->toArray();

            if ($countryList) {

                $countries['responseCode'] = "200";

                $countries['responseMessage'] = "Success";

                $countries['data'] = $countryList;
            } else {

                $countryList['responseCode'] = "100";

                $countryList['responseMessage'] = "Success";

                $countryList['responseDescription'] = "No data found";
            }
        } else {

            $countries['responseCode'] = "401";

            $countries['reponseMessage'] = "Error";

            $countries['responseDescription'] = "Sorry, You are not authrized";
        }



        return $countries;
    }



    /*     * *****  State List   *********** */



    //Country List Api

    public function getStateList(Request $request)
    {



        if ($this->user) {

            $stateList = DB::table('states')->select('id', 'name', 'country_id')->where([['country_id', '=', $request->country_id]])->get()->toArray();

            if ($stateList) {

                $states['responseCode'] = "200";

                $states['responseMessage'] = "Success";

                $states['data'] = $stateList;
            } else {

                $states['responseCode'] = "100";

                $states['responseMessage'] = "Success";

                $states['responseDescription'] = "No data found";
            }
        } else {

            $states['responseCode'] = "401";

            $states['reponseMessage'] = "Error";

            $states['responseDescription'] = "Sorry, You are not authrized";
        }



        return $states;
    }



    public function kycFileUpload(Request $request)
    {



        if ($this->user) {

            $user = User::findOrFail($this->user->id);

            $dataKyc = $user->getKycDetail()->first();

            if ($request->file) {

                $kycFile = time() . '.' . $request->file->getClientOriginalExtension();

                $request->file->move(public_path('images/kyc'), $kycFile);

                //$dataKyc->attachment = $kycFile;

                $kycUrl = url('/images/kyc/' . $kycFile);
            }



            if ($kycUrl) {

                $files['responseCode'] = "200";

                $files['responseMessage'] = "Success";

                $files['data'] = $kycUrl;
            } else {

                $files['responseCode'] = "100";

                $files['responseMessage'] = "Success";

                $files['responseDescription'] = "No data found";
            }
        } else {

            $files['responseCode'] = "401";

            $files['reponseMessage'] = "Error";

            $files['responseDescription'] = "Sorry, You are not authrized";
        }

        return $files;
    }



    public function userDashboardDetail()
    {
        $this->user = auth()->user()->id;

        // dd($this->user);
        // dd(auth()->user()->id);

        $kycStatus = "Not Submitted";

        $keyNotes = '';

        $data = array();

        if ($this->user) {

            // dd($this->user);

            $user = User::find($this->user);

            // dd($user);
            // dd($user->getKycDetail);

            $paidCommission = $user->getTotalPaidCommission()->sum('commission_amt');

            $unpaidCommission = $user->getTotalUnpaidCommission()->sum('commission_amt');

            $creditBalance = $user->getCreditWalletBalance->amount;

            $cashBalance = $user->getCashWalletBalance->amount;

            if ($user->getKycDetail) {

                $kycStatus = $user->getKycDetail->status == 0 ? 'Unapproved' : 'Approved';

                $keyNotes = !empty($user->getKycDetail->notes) ? $user->getKycDetail->notes : '';
            }

            $data['referral_id'] = $user->referral_id;

            $data['expire_date'] = $user->expire_date;

            $data['user_staus'] = $user->status == 0 ? 'Inactive' : 'Active';

            $data['paid_commission'] = $paidCommission;

            $data['unpaid_commission'] = $unpaidCommission;

            $data['total_commission'] = $paidCommission + $unpaidCommission;

            $data['credit_balance'] = $creditBalance;

            $data['cash_balance'] = $cashBalance;

            $data['kyc_status'] = $kycStatus;

            $data['kyc_notes'] = $keyNotes;

            $data['currency_symbol'] = env('CURRENCY_SYMBOL');



            $dashboardData['responseCode'] = "200";

            $dashboardData['responseMessage'] = "Success";

            $dashboardData['data'] = $data;

            return $dashboardData;
        } else {

            $dashboardData['responseCode'] = "401";

            $dashboardData['responseMessage'] = "Error";

            $dashboardData['responseDescription'] = "Sorry, You are not authorized";

            return $dashboardData;
        }
    }



    public function getCustomerDetail()
    {

        if ($this->user) {

            $user = Customer::where('retailer_id', $this->user->id)->join('brand_models', 'brand_models.id', '=', 'customers.model_id')

                ->select('customers.*', 'brand_models.name as brand_model_name', 'brand_models.cut_point', 'brand_models.image')->orderBy('customers.id', 'desc')->get();



            if ($user) {

                $customerData['responseCode'] = "200";

                $customerData['responseMessage'] = "Success";

                $customerData['data'] = $user;

                $customerData['url'] = "images/models";
            } else {

                $customerData['responseCode'] = "100";

                $customerData['responseMessage'] = "Success";

                $customerData['responseDescription'] = "No data found";
            }
        } else {

            $customerData['responseCode'] = "401";

            $customerData['responseMessage'] = "Error";

            $customerData['responseDescription'] = "Sorry, You are not authorized";
        }

        return $customerData;
    }



    public function showDirectMemberTree(Request $request)
    {

        /*

          Date:26-11-2020:12:24 tusar ne commission_amt change karava ke commission_credit

         */

        if ($this->user) {



            $referred_by = intval($this->user->referral_id);

            //DB::enableQueryLog();

            $directMembers = User::query()->Join('recharge_commissions', 'recharge_commissions.sender_id', '=', 'users.id')

                ->select('users.id', 'users.name', 'users.email')

                ->selectRaw('FORMAT(sum(recharge_commissions.commission_credit),2) as commission')

                ->where('users.referred_by', $referred_by)

                ->groupby('users.id', 'users.name', 'email')

                ->get()->toArray();



            //DB::enableQueryLog();

            $commissionData = User::query()->Join('recharge_commissions', 'recharge_commissions.sender_id', '=', 'users.id')

                ->select(

                    \DB::raw(
                        'users.id, users.name,

                                  (SELECT count(users.referred_by) FROM users WHERE users.referred_by = "' . $referred_by . '") AS chain,

                                  (SELECT FORMAT(sum(recharge_commissions.commission_credit),2) FROM recharge_commissions WHERE recharge_commissions.status=0 AND recharge_commissions.user_id = "' . $this->user->id . '") AS recevible,

                                  (SELECT FORMAT(sum(recharge_commissions.commission_credit),2) FROM recharge_commissions WHERE recharge_commissions.status=1 AND recharge_commissions.user_id = "' . $this->user->id . '") AS received,

                                  (SELECT FORMAT(sum(recharge_commissions.commission_credit),2) FROM recharge_commissions WHERE recharge_commissions.user_id = "' . $this->user->id . '") AS total'

                    )

                )

                ->where('users.referred_by', $referred_by)

                ->groupby('users.id', 'users.name')

                ->get()->toArray();

            // dd(DB::getQueryLog());

            //$commissionData = RechargeCommission::where('user_id', $this->user->id)->get();

            $commissionPaid = RechargeCommission::where('user_id', $this->user->id)->where('status', '1')->sum('recharge_commissions.commission_credit');

            $commissionUnpaid = RechargeCommission::where('user_id', $this->user->id)->where('status', '0')->sum('recharge_commissions.commission_credit');

            $commission = DB::select('SELECT * FROM fix_commission');



            if ($directMembers) {

                $directMember['responseCode'] = "200";

                $directMember['responseMessage'] = "Success";

                $directMember['fixpayoutcommission'] = $commission[0]->commission;

                $directMember['data'] = $directMembers;

                $directMember['commissionData'] = $commissionData;

                $directMember['commissionPaid'] = number_format($commissionPaid, 2);

                $directMember['commissionUnpaid'] = number_format($commissionUnpaid, 2);

                $directMember['totalCommision'] = number_format($commissionPaid + $commissionUnpaid, 2);
            } else {

                $directMember['responseCode'] = "100";

                $directMember['fixpayoutcommission'] = $commission[0]->commission;

                $directMember['responseMessage'] = "Success";

                $directMember['responseDescription'] = "No data found";
            }
        } else {

            $directMember['responseCode'] = "401";

            $directMember['reponseMessage'] = "Error";

            $directMember['responseDescription'] = "Sorry, You are not authrized";
        }



        return $directMember;
    }



    /*     * **** start  request payout **** */



    public function getRequestPayout(Request $request)
    {



        if ($this->user) {



            $requestPayout = PayoutRequest::where('user_id', $this->user->id)->get();



            if ($requestPayout) {



                $dataPayout['responseCode'] = "200";

                $dataPayout['responseMessage'] = "Success";

                $dataPayout['data'] = $requestPayout;
            } else {

                $dataPayout['responseCode'] = "100";

                $dataPayout['responseMessage'] = "Success";

                $dataPayout['responseDescription'] = "No data found";
            }
        } else {

            $dataPayout['responseCode'] = "401";

            $dataPayout['responseMessage'] = "Error";

            $dataPayout['responseDescription'] = "Sorry, You are not authorized";
        }

        return $dataPayout;
    }



    public function requestPayout(Request $request)
    {



        if ($this->user) {



            $validator = Validator::make($request->all(), [

                'amount' => 'required',

                'type' => 'required',

            ]);



            $temp = $validator->errors()->all();



            if ($validator->fails()) {



                $dataPayout['responseCode'] = "101";

                $dataPayout['responseMessage'] = "Error";

                $dataPayout['responseDescription'] = $temp;

                return $dataPayout;
            } else {



                $commissionUnpaid = RechargeCommission::where('user_id', $this->user->id)->where('status', '0')->sum('recharge_commissions.commission_amt');



                if ($request->amount > $commissionUnpaid) {

                    $dataPayout['responseCode'] = "101";

                    $dataPayout['responseMessage'] = "Error";

                    $dataPayout['responseDescription'] = 'Payout Request amount less than or equal to the unpaid amount';

                    return $dataPayout;
                }



                $requestPayout = new PayoutRequest();

                $requestPayout->user_id = $this->user->id;

                $requestPayout->amount = $request->amount;

                $requestPayout->type = $request->type;

                $requestPayout->save();



                $dataPayout['responseCode'] = "200";

                $dataPayout['responseMessage'] = "Success";

                $dataPayout['data'] = $requestPayout;
            }
        } else {

            $dataPayout['responseCode'] = "401";

            $dataPayout['reponseMessage'] = "Error";

            $dataPayout['responseDescription'] = "Sorry, You are not authrized";
        }



        return $dataPayout;
    }



    /*     * **** end request payout **** */





    /*     * **** get user Profile **** */

    public function getUserProfile(Request $request)
    {

        dd(auth()->user());

        if ($this->user) {



            $user = USER::find($this->user->id);

            $countries = DB::table("countries")->pluck("name", "id");

            $kycCategory = KycCategory::orderBy('id', 'DESC')->get();

            $userContactDetail = $user->getContactDetail()->first();



            if ($user) {

                if (isset($user->profile_pic)) {

                    $user->profile_pic = url('/images/profile/' . $user->profile_pic);
                } else {

                    $user->profile_pic = '';
                }

                $data['user'] = $user;

                $data['userContactDetail'] = !empty($userContactDetail) ? $userContactDetail : [];

                $data['countries'] = $countries;

                $data['kycCategory'] = $kycCategory;

                $userUpdate['responseCode'] = "200";

                $userUpdate['responseMessage'] = "Success";

                $userUpdate['data'] = $data;
            } else {



                $userUpdate['responseCode'] = "100";

                $userUpdate['responseMessage'] = "Success";

                $userUpdate['responseDescription'] = "No data found";
            }
        } else {

            $userUpdate['responseCode'] = "401";

            $userUpdate['responseMessage'] = "Error";

            $userUpdate['responseDescription'] = "Sorry, You are not authorized";
        }

        return $userUpdate;
    }



    /*     * **** update user Profile **** */



    public function updateProfile(Request $request)
    {



        if ($this->user) {



            $validator = Validator::make($request->all(), [

                'name' => 'required|max:255',

                'email' => 'nullable|string|email|max:255',

                'shop_name' => 'required|max:255',

                'address' => 'required',

            ]);



            $temp = $validator->errors()->all();



            if ($validator->fails()) {



                $userUpdate['responseCode'] = "101";

                $userUpdate['responseMessage'] = "Error";

                $userUpdate['responseDescription'] = $temp;

                return $userUpdate;
            } else {



                if ($request->country != 101 && empty($request->email)) {

                    $userUpdate['responseCode'] = "101";

                    $userUpdate['responseMessage'] = "Error";

                    $userUpdate['responseDescription'] = "Email id is required";

                    return $userUpdate;
                }



                $user = USER::find($this->user->id);

                $user->shop_name = $request->shop_name ?? NULL;

                $user->name = $request->name;

                $user->email = $request->email;

                $user->phone = !empty($request->phone) ? $request->phone : $user->phone;

                $user->other_phone = $request->other_phone;



                if ($request->profile_pic) {

                    $profile_pic = time() . '.' . $request->profile_pic->getClientOriginalExtension();

                    $request->profile_pic->move(public_path('images/profile'), $profile_pic);

                    $profile_pic_url = url('/images/profile/' . $profile_pic);

                    $user->profile_pic = $profile_pic;
                }



                $user->save();



                $userContactDetail = $user->getContactDetail()->first();



                if (!empty($userContactDetail)) {

                    $contact_details = [

                        'user_id' => $user->id,

                        'address' => !empty($request->address) ? $request->address : $userContactDetail->address,

                        'city' => !empty($request->city) ? $request->city : $userContactDetail->city,

                        'state' => !empty($request->state) ? $request->state : $userContactDetail->state,

                        'country' => !empty($request->country) ? $request->country : $userContactDetail->country,

                        'pin_code' => !empty($request->pin_code) ? $request->pin_code : $userContactDetail->pin_code,

                    ];

                    $userContactData = $userContactDetail->update($contact_details);
                } else {

                    $contact_details = [

                        'user_id' => $user->id,

                        'address' => $request->address,

                        'city' => $request->city,

                        'state' => $request->state,

                        'country' => $request->country,

                        'pin_code' => $request->pin_code

                    ];

                    $userContactData = UserContactDetail::create($contact_details);
                }

                $userContactDetail = $user->getContactDetail()->first();



                $users = collect($user);

                $userContactDetail = collect($userContactDetail);



                $userAndContact = $userContactDetail->merge($users);



                $userUpdate['responseCode'] = "200";

                $userUpdate['responseMessage'] = "Success";

                $userUpdate['responseDescription'] = "Success";

                $userUpdate['data'] = $userAndContact;

                $userUpdate['url'] = '/images/profile/' . $user->profile_pic;
            }
        } else {

            $userUpdate['responseCode'] = "401";

            $userUpdate['responseMessage'] = "Error";

            $userUpdate['responseDescription'] = "Sorry, You are not authorized";
        }

        return $userUpdate;
    }



    public function profilePicUpload(Request $request)
    {



        /* if($this->user){

          $user = User::findOrFail($this->user->id); */

        $token = $request->token;

        if ($token) {



            $user = JWTAuth::toUser($token);



            if ($request->profile_pic) {

                $profile_pic = time() . '.' . $request->profile_pic->getClientOriginalExtension();

                $request->profile_pic->move(public_path('images/profile'), $profile_pic);

                $profile_pic_url = url('/images/profile/' . $profile_pic);

                $user->profile_pic = $profile_pic;

                $user->save();
            }



            if ($profile_pic_url) {

                $files['responseCode'] = "200";

                $files['responseMessage'] = "Success";

                $files['data'] = $profile_pic_url;
            } else {

                $files['responseCode'] = "100";

                $files['responseMessage'] = "Success";

                $files['responseDescription'] = "No data found";
            }
        } else {

            $files['responseCode'] = "401";

            $files['reponseMessage'] = "Error";

            $files['responseDescription'] = "Sorry, You are not authrized";
        }

        return $files;
    }



    public function complaintFeedback(Request $request)
    {



        if ($this->user) {



            $validator = Validator::make($request->all(), [

                'title' => 'required|max:255',

                'description' => 'required',

                'type' => 'required',

            ]);



            $temp = $validator->errors()->all();



            if ($validator->fails()) {



                $complaintFeedback['responseCode'] = "101";

                $complaintFeedback['responseMessage'] = "Error";

                $complaintFeedback['responseDescription'] = $temp;

                return $complaintFeedback;
            }



            $data = $request->all();



            $data['user_id'] = $this->user->id;



            $datasave = ComplaintFeedback::create($data);



            if ($datasave) {

                $complaintFeedback['responseCode'] = "200";

                $complaintFeedback['responseMessage'] = "Success";

                $complaintFeedback['responseDescription'] = $request->type . " send successfully.";

                $complaintFeedback['data'] = $datasave;
            } else {

                $complaintFeedback['responseCode'] = "100";

                $complaintFeedback['responseMessage'] = "Success";

                $complaintFeedback['responseDescription'] = "No data added";
            }
        } else {

            $complaintFeedback['responseCode'] = "401";

            $complaintFeedback['reponseMessage'] = "Error";

            $complaintFeedback['responseDescription'] = "Sorry, You are not authrized";
        }

        return $complaintFeedback;
    }



    public function getNotification(Request $request)
    {



        if ($this->user) {



            $data = GeneralNotification::whereNull('user_id')->orWhere('user_id', $this->user->id)->orderBy('id', 'desc')->get();



            if ($data) {



                foreach ($data as $key => $value) {

                    if (isset($value->image) && !empty($value->image)) {

                        // $images = json_decode($value->image, true);

                        if (is_array($images)) {

                            foreach ($images as $i => $image) {

                                $images[$i] = url('images/notifications', $image);
                            }

                            $value->image = $images;
                        } else {

                            $value->image = [0 => url('images/notifications', $value->image)];
                        }
                    }
                }



                $notification['responseCode'] = "200";

                $notification['responseMessage'] = "Success";

                $notification['data'] = $data;
            } else {

                $notification['responseCode'] = "100";

                $notification['responseMessage'] = "Success";

                $notification['responseDescription'] = "No data found";
            }
        } else {



            $notification['responseCode'] = "401";

            $notification['reponseMessage'] = "Error";

            $notification['responseDescription'] = "Sorry, You are not authrized";
        }

        return $notification;
    }



    public function getRewardList(Request $request)
    {



        if ($this->user) {



            $data = DB::table('rewards')

                ->join('recharge_plans', 'recharge_plans.id', '=', 'rewards.plan_id')

                ->select('rewards.*', 'recharge_plans.recharge_plan_name', 'recharge_plans.recharge_plan_rewards')

                ->where('rewards.user_id', $this->user->id)

                ->get();

            $commission = DB::select('SELECT * FROM fix_reward');

            if ($data) {



                $notification['responseCode'] = "200";

                $notification['responseMessage'] = "Success";

                $notification['fixpayoutcommission'] = $commission[0]->reward;

                $notification['data'] = $data;
            } else {

                $notification['responseCode'] = "100";

                $notification['responseMessage'] = "Success";

                $notification['fixpayoutcommission'] = $commission[0]->reward;

                $notification['responseDescription'] = "No data found";
            }
        } else {



            $notification['responseCode'] = "401";

            $notification['reponseMessage'] = "Error";

            $notification['responseDescription'] = "Sorry, You are not authrized";
        }

        return $notification;
    }



    public function getUserCurrentPlanDetails(Request $request)
    {

        if ($this->user) {



            $data = DB::table('users')->where('id', $this->user->id)->select('current_plan_id', 'current_plan')->first();



            if ($data) {

                $sub_user_list['responseCode'] = "200";

                $sub_user_list['responseMessage'] = "Success";

                $sub_user_list['data'] = $data;
            } else {

                $sub_user_list['responseCode'] = "100";

                $sub_user_list['responseMessage'] = "Success";

                $sub_user_list['responseDescription'] = "No data found";
            }
        } else {

            $sub_user_list['responseCode'] = "401";

            $sub_user_list['responseMessage'] = "Error";

            $sub_user_list['responseDescription'] = "Sorry, You are not authorized";
        }

        return $sub_user_list;
    }



    /* sub user create */



    public function subUserCreate(Request $request)
    {



        if ($this->user) {



            $validator = Validator::make($request->all(), [

                'name' => 'required|max:255',

                'phone' => 'required|max:15|unique:users',

                'app_role' => 'required',

            ]);



            $temp = $validator->errors()->all();



            if ($validator->fails()) {

                $sub_user_reg['responseCode'] = "101";

                $sub_user_reg['responseMessage'] = "Error";

                $sub_user_reg['responseDescription'] = implode(", ", $temp);

                return $sub_user_reg;
            }

            try {



                $referred_by = $this->user->referred_by;

                $referral_id = $this->generateReferralNumber();

                $user = new User();

                $user->app_role = $request->app_role;

                $user->parent_user_id = $this->user->id;

                $user->referred_by = $referred_by;

                $user->shop_name = $this->user->shop_name;

                $user->name = $request->name;

                $user->username = 'u' . $request->phone;

                $user->email = $request->phone . '@modernmart.in';

                $user->phone = $request->phone;

                $user->other_phone = 0;

                $user->referral_id = $referral_id;

                $user->otp = 124421;

                $user->otp_status = 1;

                $user->status = 1;

                $user->type = 2; //staff

                $user->password = bcrypt($request->phone);

                $user->current_plan_id = $this->user->current_plan_id;

                $user->current_plan = $this->user->current_plan;

                $user->current_blukcut_plan = $this->user->current_blukcut_plan;

                $user->current_blukcut_credit = $this->user->current_blukcut_credit;

                $user->save();

                $user->assignRole('staff member');



                $userContactDetail = $this->user->getContactDetail()->first();



                $contact_details = [
                    'user_id' => $user->id,

                    'address' => $userContactDetail->address ?? '',

                    'city' => $userContactDetail->city ?? '',

                    'state' => $userContactDetail->state ?? 0,

                    'country' => $userContactDetail->country ?? 0,

                    'pin_code' => $userContactDetail->pin_code ?? 0

                ];



                UserContactDetail::create($contact_details);

                UserKycDetail::create(['user_id' => $user->id, 'kyc_number' => 'N/A', 'status' => '0']);



                $r = $referred_by;

                $l = 1;



                while ($r != '1') {



                    if ($r != '1') {

                        UserLevel::create(['user_id' => $user->referral_id, 'upline_id' => $r, 'level' => $l]);
                    }

                    $userData = User::select('referred_by')->where('referral_id', $r)->first();

                    $r = $userData->referred_by;

                    $l++;
                }



                CashWallet::create(['user_id' => $user->id, 'amount' => '0']);

                CreditWallet::create(['user_id' => $user->id, 'amount' => '0']);



                $otp = mt_rand(100000, 999999);

                // $credit = json_decode($this->check_sms_credit(),true);

                // if($credit['status']=='success') {

                //   //return $credit;

                //   $sms_credit=$credit['balance']['sms'];

                //   if($sms_credit>0)

                //   {

                ini_set("allow_url_fopen", 1);

                $message = urlencode("Hi, " . $request->name . " You have been added in Modern Mart by " . $user->shop_name . ", here is login id and password Shop name - " . $user->shop_name . ",Login id - u" . $request->phone . ",Password - " . $request->phone);

                //$api_url = urlencode("http://msg.pwasms.com/app/smsapi/index.php?key=4617005EE5A04B&campaign=0&routeid=69&type=text&contacts=".$request->phone."&senderid=SELCT&msg=".$message);

                //$response = file_get_contents($api_url);

                $ch = curl_init();

                curl_setopt($ch, CURLOPT_URL, "http://msg.pwasms.com/app/smsapi/index.php");

                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

                curl_setopt($ch, CURLOPT_POST, 1);

                curl_setopt($ch, CURLOPT_POSTFIELDS, "key=4617005EE5A04B&campaign=0&routeid=69&type=text&contacts=" . $request->phone . "&senderid=SELECT&msg=" . $message);

                $response = curl_exec($ch);

                curl_close($ch);



                $sub_user_reg['responseCode'] = "200";

                $sub_user_reg['responseMessage'] = "Success";

                $sub_user_reg['responseDescription'] = "User successfully created";
            } catch (\Exception $e) {

                $sub_user_reg['responseCode'] = "401";

                $sub_user_reg['responseMessage'] = "Error";

                $sub_user_reg['responseDescription'] = $e->getMessage();
            }
        } else {

            $sub_user_reg['responseCode'] = "401";

            $sub_user_reg['responseMessage'] = "Error";

            $sub_user_reg['responseDescription'] = "Sorry, You are not authorized";
        }

        return $sub_user_reg;
    }



    /* start get sub user list 180920201100 */



    public function getSubUser(Request $request)
    {



        if ($this->user) {



            $data = DB::table('users')->where('parent_user_id', $this->user->id)->get();



            if ($data) {

                $sub_user_list['responseCode'] = "200";

                $sub_user_list['responseMessage'] = "Success";

                $sub_user_list['data'] = $data;
            } else {

                $sub_user_list['responseCode'] = "100";

                $sub_user_list['responseMessage'] = "Success";

                $sub_user_list['responseDescription'] = "No data found";
            }
        } else {



            $sub_user_list['responseCode'] = "401";

            $sub_user_list['responseMessage'] = "Error";

            $sub_user_list['responseDescription'] = "Sorry, You are not authorized";
        }



        return $sub_user_list;
    }



    /* end get sub user list */

    /* subUserStatusChangeRequest 06082021 */



    public function subUserStatusChangeRequest(Request $request)
    {





        if ($this->user) {



            $validator = Validator::make($request->all(), [

                'user_id' => 'required|max:255',

                'status_change_to' => 'required',

            ]);



            $temp = $validator->errors()->all();



            if ($validator->fails()) {

                $sub_user_reg['responseCode'] = "101";

                $sub_user_reg['responseMessage'] = "Error";

                $sub_user_reg['responseDescription'] = implode(", ", $temp);

                return $sub_user_reg;
            }

            try {



                UserKycDetail::where('user_id', $request->user_id)->update(['status_change_to' => $request->status_change_to]);



                $sub_user_reg['responseCode'] = "200";

                $sub_user_reg['responseMessage'] = "Success";

                $sub_user_reg['responseDescription'] = "Send Request";
            } catch (\Exception $e) {

                $sub_user_reg['responseCode'] = "401";

                $sub_user_reg['responseMessage'] = "Error";

                $sub_user_reg['responseDescription'] = $e->getMessage();
            }
        } else {

            $sub_user_reg['responseCode'] = "401";

            $sub_user_reg['responseMessage'] = "Error";

            $sub_user_reg['responseDescription'] = "Sorry, You are not authorized";
        }

        return $sub_user_reg;
    }



    /* end subUserStatusChangeRequest 06082021 */



    function getImage(Request $request)
    {

        $image = $request->file;

        $filename = time() . '.' . $image->getClientOriginalExtension();

        $path = public_path('images/' . $filename);

        $image->move(public_path('images'), $filename);

        $data['getpdf'] = array(

            'image' => $filename,

            'padding' => $request->padding

        );

        $file = rand(time(), 10) . 'picture.pdf';

        $pdf = PDF::loadView('getImage', $data)->save(public_path('images/' . $file))->stream($file);

        return response(['url' => asset('images/' . $file)], 200);
    }



    /* cash free */

    #get auth token



    public function getToken()
    {

        try {

            $auth = env('CASHFREE_PAYOUT_AUTHURL');

            $response = $this->post_helper($auth, null, null);

            return $response;
        } catch (Exception $ex) {

            error_log('error in getting token');

            error_log($ex->getMessage());

            die();
        }
    }



    public function create_header($token)
    {

        $header = array(

            'X-Client-Id: ' . env('CASHFREE_PAYOUT_APP_ID'),

            'X-Client-Secret: ' . env('CASHFREE_PAYOUT_SECRET_KEY'),

            'Content-Type: application/json',

        );

        $headers = $header;

        if (!is_null($token)) {

            array_push($headers, 'Authorization: Bearer ' . $token);
        }

        return $headers;
    }



    public function post_helper($action, $data, $token)
    {

        $baseurl = env('CASHFREE_PAYOUT_BASEURL');

        $finalUrl = $baseurl . $action;

        $headers = $this->create_header($token);

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_POST, 1);

        curl_setopt($ch, CURLOPT_URL, $finalUrl);

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        if (!is_null($data))

            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));

        $r = curl_exec($ch);

        if (curl_errno($ch)) {

            print('error in posting');

            print(curl_error($ch));

            die();
        }

        curl_close($ch);

        $rObj = json_decode($r, true);

        if ($rObj['status'] == 'SUCCESS' || $rObj['subCode'] == '200') {

            return $rObj;
        }

        return $rObj;
    }



    public function get_helper($finalUrl, $token)
    {

        $headers = $this->create_header($token);

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $finalUrl);

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $r = curl_exec($ch);

        if (curl_errno($ch)) {

            \Log::error('error in posting');

            \Log::error(curl_error($ch));

            die();
        }

        curl_close($ch);

        $rObj = json_decode($r, true);

        if ($rObj['status'] == 'SUCCESS' || $rObj['subCode'] == '200') {

            return $rObj;
        } else {

            return $rObj;
        }
    }



    #ValidateBankDetails



    public function validateBankDetails($token, $account_details)
    {

        try {

            $baseurl = env('CASHFREE_PAYOUT_BASEURL');

            $getBene = env('CASHFREE_BULK_VALIDATION');

            $finalUrl = $baseurl . $getBene . $account_details;

            $response = $this->get_helper($finalUrl, $token);

            return $response;
        } catch (Exception $ex) {

            $msg = $ex->getMessage();

            if (strstr($msg, 'Beneficiary does not exist')) {

                return false;
            }

            \Log::error('error in getting beneficiary details');

            \Log::error($msg);

            die();
        }
    }



    #add beneficiary



    public function addBeneficiary($token, $beneficiary)
    {

        try {

            $addBene = env('CASHFREE_PAYOUT_ADDBENEURL');

            $response = $this->post_helper($addBene, $beneficiary, $token);

            return $response;
        } catch (Exception $ex) {

            $msg = $ex->getMessage();

            return $msg;
        }
    }



    /* end cashfree */



    /* @narenthaker 06072021 16:17 getcomplaintFeedback start */



    public function getcomplaintFeedback(Request $request)
    {

        if ($this->user) {



            $data = ComplaintFeedback::Where('user_id', $this->user->id)->select('id', 'title', 'description', 'type', 'status', 'created_at')->orderBy('id', 'desc')->get()->toArray();



            if ($data) {



                $complaintFeedback['responseCode'] = "200";

                $complaintFeedback['responseMessage'] = "Success";

                $complaintFeedback['data'] = $data;
            } else {

                $complaintFeedback['responseCode'] = "100";

                $complaintFeedback['responseMessage'] = "Success";

                $complaintFeedback['responseDescription'] = "No data found";
            }
        } else {



            $complaintFeedback['responseCode'] = "401";

            $complaintFeedback['reponseMessage'] = "Error";

            $complaintFeedback['responseDescription'] = "Sorry, You are not authrized";
        }

        return $complaintFeedback;
    }



    /* @narenthaker 06072021 16:17 getcomplaintFeedback end */





    /* @narenthaker 06072021 16:17 staffKycUpload start */



    public function staffKycUpload(Request $request)
    {



        if ($this->user) {



            $validator = Validator::make($request->all(), [

                'shop_visiting_card' => 'required',

                'shop_pic_with_selfie' => 'required',

                'current_location' => 'required',

                'lat_lan' => 'required',

            ]);



            if ($validator->fails()) {

                $data['responseCode'] = "101";

                $data['responseMessage'] = "Error1";

                $data['responseDescription'] = implode(", ", $validator->errors()->all());

                return $data;
            }



            try {



                $all_inputs = $request->all();

                $kyc_detail = $this->user->getKycDetail()->first();



                $shop_visiting_card = null;

                if ($request->has('shop_visiting_card')) {

                    $kycFile = time() . '1.' . $request->shop_visiting_card->getClientOriginalExtension();

                    $request->shop_visiting_card->move(public_path('images/kyc'), $kycFile);

                    $shop_visiting_card = $kycFile;

                    $all_inputs['shop_visiting_card'] = url('/images/kyc/' . $kycFile);
                }



                $shop_pic_with_selfie = null;

                if ($request->has('shop_pic_with_selfie')) {

                    $kycFile = time() . '2.' . $request->shop_pic_with_selfie->getClientOriginalExtension();

                    $request->shop_pic_with_selfie->move(public_path('images/kyc'), $kycFile);

                    $shop_pic_with_selfie = $kycFile;

                    $all_inputs['shop_pic_with_selfie'] = url('/images/kyc/' . $kycFile);
                }



                $current_location = $request->current_location;

                $lat_lan = $request->lat_lan;



                $kyc_detail = UserKycDetail::updateOrCreate(

                    ['user_id' => $this->user->id],
                    [

                        'shop_visiting_card' => $shop_visiting_card,

                        'shop_pic_with_selfie' => $shop_pic_with_selfie,

                        'current_location' => $current_location,

                        'lat_lan' => $lat_lan,

                        'kyc_number' => 'Staff Kyc',

                        'status' => 0

                    ]

                );



                $data['responseCode'] = "200";

                $data['responseMessage'] = "Success";

                $data['responseDescription'] = "Kyc uplode successfully";

                $data['data'] = $all_inputs;
            } catch (\Exception $e) {

                \Log::info($e->getMessage());

                $data['responseCode'] = "401";

                $data['responseMessage'] = "Error2";

                $data['responseDescription'] = $e->getMessage();
            }
        } else {

            \Log::info("Sorry, You are not authorized");

            $data['responseCode'] = "401";

            $data['responseMessage'] = "Error3";

            $data['responseDescription'] = "Sorry, You are not authorized";
        }

        return $data;
    }



    /* @narenthaker 06072021 16:17 staffKycUpload end */



    public function acceptPrivacyPolicy()
    {

        if ($this->user) {

            $data = User::where('id', $this->user->id)

                ->update(['privacy_policy' => 1]);



            if ($data) {



                $update['responseCode'] = "200";

                $update['responseMessage'] = "Success";

                $update['data'] = $data;
            } else {



                $update['responseCode'] = "101";

                $update['responseMessage'] = "Error";

                $update['responseDescription'] = "No data found";
            }
        } else {

            $update['responseCode'] = "401";

            $update['responseMessage'] = "Error";

            $update['responseDescription'] = "Sorry, You are not authorized";
        }



        return $update;
    }



    public function updatenotificationstatus(Request $request)
    {



        if ($this->user) {





            $data = GeneralNotification::where('id', $request->id)->update(['read' => 1]);



            if ($data) {



                $notification['responseCode'] = "200";

                $notification['responseMessage'] = "Success";

                $notification['data'] = $data;
            } else {

                $notification['responseCode'] = "100";

                $notification['responseMessage'] = "Success";

                $notification['responseDescription'] = "No data found";
            }
        } else {



            $notification['responseCode'] = "401";

            $notification['reponseMessage'] = "Error";

            $notification['responseDescription'] = "Sorry, You are not authrized";
        }

        return $notification;
    }



    public function subUserCount(Request $request)
    {



        if ($this->user) {



            $data = DB::table('users')->where('parent_user_id', $this->user->id)->get()->toArray();



            $total = count($data);

            $active = 0;

            $inactive = 0;

            if (!empty($data)) {

                foreach ($data as $key => $value) {

                    if ($value->status == 1) {

                        $active++;
                    } else {

                        $inactive++;
                    }
                }
            }



            $count['total'] = $total;

            $count['active'] = $active;

            $count['inactive'] = $inactive;



            $sub_user_list['responseCode'] = "200";

            $sub_user_list['responseMessage'] = "Success";

            $sub_user_list['data'] = $count;
        } else {



            $sub_user_list['responseCode'] = "401";

            $sub_user_list['responseMessage'] = "Error";

            $sub_user_list['responseDescription'] = "Sorry, You are not authorized";
        }



        return $sub_user_list;
    }
}
