<?php

namespace App\Http\Controllers\Api\V1;

use JWTAuth;
use App\Models\BrandType;
use App\Models\Brand;
use App\Models\BrandModel;
use App\Models\BrandSubModel;
use App\Models\BrandModelScreenImage;
use App\Models\BrandModelScreenRetailer;
use App\Models\UserReactions;
use App\Models\Design;
use App\User;
use App\Models\UserLastCut;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use DB;
use Validator;
use App\Notifications\UserReaction;
use Notification;

class DesignController extends Controller {

    /**
     * @var
     */
    protected $user;

    /**
     * TaskController constructor.
     */
    public function __construct() {
        $this->user = JWTAuth::parseToken()->authenticate();
        return $this->user;
    }

    public function add(Request $request) {
        if ($this->user) {
            $validator = Validator::make($request->all(), [
                        'brand_id' => 'required',
                        'model_id' => 'required',
                        'model_image' => 'required|mimes:jpeg,jpg,png,gif',
                        'plt_file' => 'required',
            ]);
            $temp = $validator->errors()->all();
            if ($validator->fails()) {
                $responceData['responseCode'] = "101";
                $responceData['responseMessage'] = "Error";
                $responceData['responseDescription'] = $temp;
                return $responceData;
            } else {
//                $total = Design::where('user_id', $this->user->id)->count();
//                if ($total == 8) {
//                    $responceData['responseCode'] = "101";
//                    $responceData['responseMessage'] = "Error";
//                    $responceData['responseDescription'] = "You cant't add more than 8 models";
//                    return $responceData;
//                }
                $model_images = [];
                $plt_files = [];

                $design = new BrandModelScreenRetailer();
                if ($request->hasFile('model_image')) {
                    $model_image = time() . '_model.' . $request->model_image->getClientOriginalExtension();
                    $path = $request->model_image->move(public_path('images/models-screen-image'), $model_image);
                    array_push($model_images, $model_image);

                    //$upload_file = $request->file('model_image');
                    $imagesize = getimagesize($path);
                }
                $design->image = json_encode($model_images, JSON_FORCE_OBJECT);
//                if ($request->hasFile('plt_file')) {
//                    $plt_file = time() . '.' . $request->plt_file->getClientOriginalExtension();
//                    $request->plt_file->move(public_path('images/designs'), $plt_file);
//                    $design->plt_file = $plt_file;
//                }

                $pltdata = base64_decode($request->plt_file);
                $plt_file = time() . '.plt';
                file_put_contents(public_path('images/models-screen-image/' . $plt_file), $pltdata);
                array_push($plt_files, $plt_file);
                $names = array();
                array_push($names, $request->name);

                $design->blt_file = json_encode($plt_files, JSON_FORCE_OBJECT);

                $design->brand_id = $request->brand_id;
                $design->model_id = $request->model_id;

                $design->upload_by = $this->user->id;
                $design->height = round($imagesize[1] / 2.02, 2);
                $design->width = round($imagesize[0] / 2.02, 2);

                $design->save();

                $responceData['responseCode'] = "200";
                $responceData['responseMessage'] = "Success";
                $responceData['responseDescription'] = "Model added successfully";
            }
        } else {
            $responceData['responseCode'] = "401";
            $responceData['responseMessage'] = "Error";
            $responceData['responseDescription'] = "Sorry, You are not authorized";
        }
        return $responceData;
    }

    public function update(Request $request) {
        if ($this->user) {
            $rules = [
                'id' => 'required',
                'brand_id' => 'required',
                'model_id' => 'required',
            ];
            if ($request->hasFile('model_image')) {
                $rules['model_image'] = 'required|mimes:jpeg,jpg,png,gif';
            }
//            if ($request->hasFile('plt_file')) {
//                $rules['plt_file'] = 'required';
//            }
            $validator = Validator::make($request->all(), $rules);
            $temp = $validator->errors()->all();
            if ($validator->fails()) {
                $responceData['responseCode'] = "101";
                $responceData['responseMessage'] = "Error";
                $responceData['responseDescription'] = $temp;
                return $responceData;
            } else {
                $design = BrandModelScreenRetailer::find($request->id);

                if (!$design) {
                    $responceData['responseCode'] = "404";
                    $responceData['responseMessage'] = "Error";
                    $responceData['responseDescription'] = "Record Not Found.";
                    return $responceData;
                }
                $model_images = [];
                $plt_files = [];
                if ($request->hasFile('model_image')) {
//                    $model_image = time() . '.' . $request->model_image->getClientOriginalExtension();
                    $model_image = time() . '_model.' . $request->model_image->getClientOriginalExtension();
                    $path = $request->model_image->move(public_path('images/models-screen-image/'), $model_image);
                    array_push($model_images, $model_image);

                    //$upload_file = $request->file('model_image');
                    $imagesize = getimagesize($path);
                }
//                if ($request->hasFile('plt_file')) {
//                    $plt_file = time() . '.' . $request->plt_file->getClientOriginalExtension();
//                    $request->plt_file->move(public_path('images/designs'), $plt_file);
//                    $design->plt_file = $plt_file;
//                }

                if (base64_encode(base64_decode($request->plt_file, true)) === $request->plt_file) {
                    $pltdata = base64_decode($request->plt_file);
                    $plt_file = time() . '.plt';
                    file_put_contents(public_path('images/models-screen-image/' . $plt_file), $pltdata);
                    array_push($plt_files, $plt_file);
                }

                if ($request->hasFile('model_image')) {
                    $design->image = json_encode($model_images, JSON_FORCE_OBJECT);
                    $design->height = round($imagesize[1] / 2.02, 2);
                    $design->width = round($imagesize[0] / 2.02, 2);
                }
                if ($request->plt_file) {
                    $design->blt_file = json_encode($plt_files, JSON_FORCE_OBJECT);
                }

                $design->brand_id = $request->brand_id;
                $design->model_id = $request->model_id;

                $design->upload_by = $this->user->id;


                $design->save();

                $responceData['responseCode'] = "200";
                $responceData['responseMessage'] = "Success";
                $responceData['responseDescription'] = "Model updated successfully";
            }
        } else {
            $responceData['responseCode'] = "401";
            $responceData['responseMessage'] = "Error";
            $responceData['responseDescription'] = "Sorry, You are not authorized";
        }
        return $responceData;
    }

    public function delete(Request $request) {
        if ($this->user) {
            $rules = [
                'id' => 'required',
            ];
            $validator = Validator::make($request->all(), $rules);
            $temp = $validator->errors()->all();
            if ($validator->fails()) {
                $responceData['responseCode'] = "101";
                $responceData['responseMessage'] = "Error";
                $responceData['responseDescription'] = $temp;
                return $responceData;
            } else {

                $delete = BrandModelScreenRetailer::where('id', $request->id)->delete();

                $responceData['responseCode'] = "200";
                $responceData['responseMessage'] = "Success";
                $responceData['responseDescription'] = "Model deleted successfully";
            }
        } else {
            $responceData['responseCode'] = "401";
            $responceData['responseMessage'] = "Error";
            $responceData['responseDescription'] = "Sorry, You are not authorized";
        }
        return $responceData;
    }

    public function designList(Request $request) {
        if ($this->user) {
            $design_data = BrandModelScreenRetailer::where('upload_by', $this->user->id)->orderBy('id', 'DESC')->get()->toArray();
            if ($design_data) {
                foreach ($design_data as $key => $value) {
                    $design_data[$key] = $value;
                    $design_data[$key]['image'] = json_decode($value['image'], JSON_FORCE_OBJECT)[0];
                    $design_data[$key]['blt_file'] = json_decode($value['blt_file'], JSON_FORCE_OBJECT)[0];
                    $design_data[$key]['model_image_url'] = url('public/images/models-screen-image/' . json_decode($value['image'], JSON_FORCE_OBJECT)[0]);
                    $design_data[$key]['plt_file_url'] = url('public/images/models-screen-image/' . json_decode($value['blt_file'], JSON_FORCE_OBJECT)[0]);
//                    $design_data[$key]['model_image_url'] = url('public/images/designs/' . $value['image']);
//                    $design_data[$key]['plt_file_url'] = url('public/images/designs/' . $value['blt_file']);
                }
                $responceData['responseCode'] = "200";
                $responceData['responseMessage'] = "Success";
                $responceData['responseDescription'] = "Success";
                $responceData['data'] = $design_data;
                $responceData['url'] = "images/designs/";
            } else {
                $responceData['responseCode'] = "100";
                $responceData['responseMessage'] = "Success";
                $responceData['responseDescription'] = "No data found";
            }
        } else {
            $responceData['responseCode'] = "401";
            $responceData['responseMessage'] = "Error";
            $responceData['responseDescription'] = "Sorry, You are not authorized";
        }
        return $responceData;
    }

    public function userReaction(Request $request) {
        if ($this->user) {
            $rules = [
                'image_id' => 'required',
                'status' => 'required',
                'rating' => 'required',
            ];
            $validator = Validator::make($request->all(), $rules);
            $temp = $validator->errors()->all();
            if ($validator->fails()) {
                $responceData['responseCode'] = "101";
                $responceData['responseMessage'] = "Error";
                $responceData['responseDescription'] = $temp;
                return $responceData;
            } else {

                $post = $request->post();

                $data['user_id'] = $this->user->id;
                $data['model_screen_image_id'] = $post['image_id'];

                $exist = UserReactions::where($data)->first();
                $data['status'] = $post['status'];
                $data['rating'] = $post['rating'];
                $data['comment'] = isset($post['user_comment']) ? $post['user_comment'] : "";
                if (!empty($exist)) {
                    $save = UserReactions::where('id', $exist->id)->update($data);
                } else {
                    $save = UserReactions::create($data);
                }

                $permission = DB::table('permissions')->where('name', 'user_reactions')->first();
                if (!empty($permission)) {
                    $roles = DB::table('role_has_permissions')->where('permission_id', $permission->id)->get()->toArray();

                    if (!empty($roles)) {
                        $users = User::whereHas('roles', function($q) use ($roles) {
                                    $q->whereIn('id', array_column($roles, 'role_id'));
                                })->get();

                        if (!empty($users)) {
                            if ($post['status'] == 2) {
                                foreach ($users as $key => $value) {
                                    $message = "A user has disliked a design please check.";
                                    if(isset($value['phone']) && $value['phone'] != ""){
                                        $this->send_sms($value['phone'], $message);
                                    }
                                }
                                Notification::send($users, new UserReaction());
//                                $resut = $users->notify(new UserReaction($users));
                            }
                        }
                    }
                }

                $responceData['responseCode'] = "200";
                $responceData['responseMessage'] = "Success";
            }
        } else {
            $responceData['responseCode'] = "401";
            $responceData['responseMessage'] = "Error";
            $responceData['responseDescription'] = "Sorry, You are not authorized";
        }
        return $responceData;
    }

    public function validateUser(Request $request) {
        if ($this->user) {
            $rules = [
                'image_id' => 'required',
            ];
            $validator = Validator::make($request->all(), $rules);
            $temp = $validator->errors()->all();
            if ($validator->fails()) {
                $responceData['responseCode'] = "101";
                $responceData['responseMessage'] = "Error";
                $responceData['responseDescription'] = $temp;
                return $responceData;
            } else {

                $post = $request->post();

                $data['user_id'] = $this->user->id;
                $data['image_id'] = $post['image_id'];

                $valid = false;
                $exist = UserLastCut::where($data)->first();
                if (!empty($exist)) {
                    $date = Carbon::parse($exist->updated_at);
                    $now = Carbon::now();
                    $diff = $date->diffInMinutes($now);

                    if ($diff <= 30) {
                        $valid = true;
                    }
                }
                $responceData['responseCode'] = "200";
                $responceData['responseMessage'] = "Success";
                $responceData['valid'] = $valid;
            }
        } else {
            $responceData['responseCode'] = "401";
            $responceData['responseMessage'] = "Error";
            $responceData['responseDescription'] = "Sorry, You are not authorized";
        }
        return $responceData;
    }

}
