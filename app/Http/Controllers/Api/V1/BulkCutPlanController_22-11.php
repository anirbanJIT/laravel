<?php
namespace App\Http\Controllers\Api\V1;

use JWTAuth;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Models\BulkCutPlan;
use App\Models\CutWallet;
use App\Models\OffLineCut;
use App\Models\AccountRecharge;
use App\Models\CreditWalletTransaction;
use App\Models\CashWalletTransaction;
use App\Models\CreditWallet;
use App\Models\CashWallet;
use App\Models\RechargePlan;
use App\Models\RechargeCommission;
use App\Models\BulkCutWallet;
use App\Models\Log;
Use App\helper;
Use DB;
use DateTime;
use Validator;
use Illuminate\Support\Str;

class BulkCutPlanController extends Controller
{
    /**
     * @var
     */
    protected $user;

    /**
     * TaskController constructor.
     */
    public function __construct()
    {
        $this->user = JWTAuth::parseToken()->authenticate();
    return $this->user;
    }

    public function getBulkcutList()
    {

    if($this->user){

      $user = User::findOrFail($this->user->id);

      $user_current_blukcut_plan_id = $user->current_blukcut_plan ?? null;
      $user_current_blukcut_credit = $user->user_current_blukcut_credit ?? 0;
    
      $plans = BulkCutPlan::where('status', 1 )->orderBy('id','DESC')->get();

      $data['user_current_blukcut_credit'] = $user_current_blukcut_credit;
      $data['user_current_blukcut_plan_id'] = $user_current_blukcut_plan_id;
      $data['plans'] = $plans;

      if($plans) {

        $recharge_plans['responseCode'] = "200";
        $recharge_plans['responseMessage'] = "Success";
        $recharge_plans['responseDescription'] = "Success";
        $recharge_plans['data'] = $data;

      } else {

        $recharge_plans['responseCode'] = "100";
        $recharge_plans['responseMessage'] = "Success";
        $recharge_plans['responseDescription'] = "No data found";
      }

    } else {

      $recharge_plans['responseCode'] = "401";
      $recharge_plans['responseMessage'] = "Error";
      $recharge_plans['responseDescription'] = "Sorry, You are not authorized";
    }

    return $recharge_plans;

  }

  /* start /api/v1/recharge-bulk-plan */
  public function rechargeBulkplanPayment(Request $request){

    if($this->user){ 

      $validator = Validator::make($request->all(),[
        'payment_method' => 'required', 
        'bulk_plan_id' => 'required'    
      ]);

      $temp = $validator->errors()->all();

      if($validator->fails()) 
      {
        $purchase['responseCode'] = "100";
        $purchase['responseMessage'] = "Error";
        $purchase['responseDescription'] = $temp;
        return $purchase;
      }

      $recharge_plan = BulkCutPlan::where('id', $request->bulk_plan_id )->where('status', 1 )->first();

      if (empty($recharge_plan)) {
        $purchase['responseCode'] = "100";
        $purchase['responseMessage'] = "Error";
        $purchase['responseDescription'] = "Sorry your request plan is not found.";
        return $purchase;
      } 

      if($request->payment_method == 'credit_wallet') {
        try {

          $exception = DB::transaction(function () use($request, $recharge_plan) {

            $invoice_no = monthlyRechargeInvoiceNumber(); 
            $credit = new CreditWallet();
            $credit = $credit->retailerCreditBalance();

            if($credit->amount>=$recharge_plan->price)
            {
              $balance_amount = $credit->amount-$recharge_plan->price;

              $data = new CreditWalletTransaction;
              $data->invoice_no = $invoice_no;
              $data->receiver_id = $this->user->id;
              $data->sender_id = $this->user->id;
              $data->credit_amt = 0;
              $data->debit_amt = $recharge_plan->price;
              $data->balance_amt = $balance_amount;
              $data->singal_credit_pur_amt = 0;
              $data->total_credit_pur_amt = 0;
              $data->type = 'BulkPlan Recharge Payment';
              $data->status = 1;
              $data->payment_mode = 'Credit Wallet';
              $data->transaction_description = 'You pay '.$request->price. 'credit points.';

              if($data->save()){

                CreditWallet::where('user_id',$this->user->id)->update(['amount' => $balance_amount]);

                $current = \Carbon\Carbon::now();
                $expire_date = $current->addDays($recharge_plan->validity);

                $accRecharge = new AccountRecharge;
                $accRecharge->user_id = $this->user->id;
                $accRecharge->recharge_duration = $recharge_plan->validity;
                $accRecharge->recharge_amount = $recharge_plan->price;
                $accRecharge->expire_date = $expire_date;
                $accRecharge->payment_mode = 1;
                $accRecharge->invoice_no = $invoice_no;
                $accRecharge->status = 1;

                if($accRecharge->save()){
                  //update current bulkcut plan and credit
                  User::where('id',$this->user->id)->update(['current_blukcut_plan'=>$request->bulk_plan_id, 'current_blukcut_credit'=>$recharge_plan->credit]);

                  // add credit in BulkCutWallet
                  $all_inputs['user_id'] = $user->id;
                  $all_inputs['blukcut_plan_id'] = $request->bulk_plan_id;
                  $all_inputs['cuts'] = $recharge_plan->credit;
                  $all_inputs['actions'] = 'credit';
                  $all_inputs['expires_at'] = $expire_date;
                  $data = BulkCutWallet::create($all_inputs);
                }

              }

            }
            else{
              $purchase['responseCode'] = "100";
              $purchase['responseMessage'] = "Error";
              $purchase['responseDescription'] = "You have not sufficient balance in credit wallet.";
              return $purchase;
            } 

            $purchase['responseCode'] = "200";
            $purchase['responseMessage'] = "Success";
            $purchase['responseDescription'] = "Your bulk plan recharge has been done successfully";
            return $purchase;

          }); 

          return is_null($exception) ? true : $exception;

        } catch(Exception $e) {
          dd($e);
          //return false;
          $purchase['responseCode'] = "100";
          $purchase['responseMessage'] = "Error";
          $purchase['responseDescription'] = "Sorry your request is not completed.";
          return $purchase;
        }
        
        
      } else if($request->payment_method == 'cash_free'){

        /*$invoice_no = monthlyRechargeInvoiceNumber();

        $recharge_amount_after_conversion = $recharge_plan->price;
        
        $api_response = json_decode($this->getCreditPurchaseToken($recharge_amount_after_conversion,$invoice_no),true);
        
        if($api_response['status'] == "OK"){

          $current = \Carbon\Carbon::now();
          $expire_date = $current->addDays($recharge_plan->validity);

          $accRecharge = new AccountRecharge;
          $accRecharge->user_id = $this->user->id;
          $accRecharge->recharge_duration = $recharge_plan->validity;
          $accRecharge->recharge_amount = $recharge_plan->price;
          $accRecharge->expire_date = $expire_date;
          $accRecharge->payment_mode = 1;
          $accRecharge->invoice_no = $invoice_no;
          $accRecharge->status = 1;

          if($accRecharge->save()){
            User::where('id',$this->user->id)->update(['current_blukcut_plan'=>$request->bulk_plan_id, 'current_blukcut_credit'=>$recharge_plan->price]);
          } 
        }*/

        $secretkey = env('CASHFREE_SECRET_KEY');

        $orderId = $request->orderId;

        $orderAmount = $request->orderAmount;

        $txStatus = $request->txStatus;

        $paymentMode = $request->paymentMode;

        $txMsg = $request->txMsg;

        $txTime = $request->txTime;

        $signature = $request->signature;

        $data = $orderId.$orderAmount.$txStatus.$paymentMode.$txMsg.$txTime;

        $hash_hmac = hash_hmac('sha256', $data, $secretkey, true) ;

        $computedSignature = base64_encode($hash_hmac);

        try{

          $exception = DB::transaction(function () use($request,$txStatus,$signature,$computedSignature,$recharge_plan) {

            //if($txStatus=='SUCCESS' && ($signature == $computedSignature)) { 

          	if($txStatus=='SUCCESS') { 

              $current = \Carbon\Carbon::now();

              $expire_date = $current->addDays($recharge_plan->validity);

              $accRecharge = new AccountRecharge;

              $accRecharge->user_id = $this->user->id;

              $accRecharge->recharge_duration = $recharge_plan->validity;

              $accRecharge->recharge_amount = $request->orderAmount;

              $accRecharge->expire_date = $expire_date;

              $accRecharge->payment_mode = 2;

              $accRecharge->invoice_no = $request->orderId;

              $accRecharge->status = 1;

              if($accRecharge->save()){
                User::where('id',$this->user->id)->update(['current_blukcut_plan'=>$request->bulk_plan_id, 'current_blukcut_credit'=>$recharge_plan->credit]);

                // add credit in BulkCutWallet
                $all_inputs['user_id'] = $this->user->id;
                $all_inputs['blukcut_plan_id'] = $request->bulk_plan_id;
                $all_inputs['cuts'] = $recharge_plan->credit;
                $all_inputs['actions'] = 'credit';
                $all_inputs['expires_at'] = $expire_date;
                $data = BulkCutWallet::create($all_inputs);
              } 

              $purchase['responseCode'] = "200";

              $purchase['responseMessage'] = "Success";

              $purchase['responseDescription'] = "Your recharge has been done successfully";

              return $purchase;

            } else {

            	$purchase['responseCode'] = "100";

				$purchase['responseMessage'] = "Error";

				$purchase['responseDescription'] = "Sorry your payment is not completed.";

				return $purchase; 
            } 
          });

          return is_null($exception) ? true : $exception; 

        } catch(Exception $e) { 

          $purchase['responseCode'] = "100";

          $purchase['responseMessage'] = "Error";

          $purchase['responseDescription'] = "Sorry your request is not completed.";

          return $purchase;

        }

      } else {

        $purchase['responseCode'] = "401";

        $purchase['responseMessage'] = "Error";

        $purchase['responseDescription'] = "Sorry, You are not authorized";

        return $purchase;

      }
    }

  }
  /* end /api/v1/recharge-bulk-plan */

  /* start api/v1/update-bulkcut-wallet */
  public function updateBulkCutWallet(Request $request){

    if($this->user){

        $validator = Validator::make($request->all(),[
            'cuts' => 'required'
        ]);

        $temp = $validator->errors()->all();

        if($validator->fails()) 
        {
            $cat_wallet['responseCode'] = "101";
            $cat_wallet['responseMessage'] = "Error";
            $cat_wallet['responseDescription'] = $temp;
            return $cat_wallet;
        }
       
        if ($this->user->parent_user_id == 0) {
          $user = $this->user;
        }else{
          $user = User::where('id',$this->user->parent_user_id)->first();
        }

       	if($user->current_blukcut_credit>=$request->cuts){

       		$plan_is_expires = BulkCutWallet::where('user_id', $user->id)
                              ->where('blukcut_plan_id', $user->current_blukcut_plan)
                              ->where('actions','credit')
                              ->where('expires_at','<', Carbon::now())
                              ->orderBy('id','DESC')
                              ->count();
            
        if ($plan_is_expires>0) {
            $cat_wallet['responseCode'] = "100";
            $cat_wallet['responseMessage'] = "Error";
            $cat_wallet['responseDescription'] = "You plan was expires.";
				    return $cat_wallet;
        }

       		$current_blukcut_credit_after_conversion = intval($user->current_blukcut_credit) - intval($request->cuts);

       		User::where('id',$user->id)->update(['current_blukcut_credit'=>$current_blukcut_credit_after_conversion]);
	        
	        $all_inputs = $request->all(); 
	        $all_inputs['user_id'] = $user->id;

	        $data = BulkCutWallet::create($all_inputs);

	        if($data) {
	          $cat_wallet['responseCode'] = "200";
	          $cat_wallet['responseMessage'] = "Success";
	          $cat_wallet['data'] = $data;
	      	} 

	    }else{
          $cat_wallet['responseCode'] = "100";
          $cat_wallet['responseMessage'] = "Error";
          $cat_wallet['responseDescription'] = "You have not sufficient blukcut credit.";
          return $cat_wallet;
        } 

    } else {

      $cat_wallet['responseCode'] = "401";
      $cat_wallet['responseMessage'] = "Error";
      $cat_wallet['responseDescription'] = "Sorry, You are not authorized";
    }

    return $cat_wallet;

  }
  /* end api/v1/update-bulkcut-wallet */

  /* end api/v1/get-bulkcut-wallet */
  public function getBulkCutWalletHistory(){

    if($this->user){

      if ($this->user->parent_user_id == 0) {
          $user = $this->user;
      }else{
        $user = User::where('id',$this->user->parent_user_id)->first();
      }

      $data = BulkCutWallet::where('user_id', $user->id)->orderBy('id','DESC')->get(['id','user_id','blukcut_plan_id','cuts','actions','created_at','expires_at']);

      $current_plan = BulkCutPlan::where('id', $user->current_blukcut_plan)->first();

      $last_bulkcut_recharge = BulkCutWallet::where('user_id', $user->id)
                              ->where('blukcut_plan_id', $user->current_blukcut_plan)
                              ->where('actions','credit')
                              ->orderBy('id','DESC')
                              ->first();
      $bulk_limit = 0;
      $diff = 0;
      if ($last_bulkcut_recharge) {
         $bulk_limit = BulkCutWallet::where('user_id', $user->id)
                                  ->where('blukcut_plan_id', $user->current_blukcut_plan)
                                  ->where('id','>',$last_bulkcut_recharge->id)->count('id');

        $date = Carbon::parse($last_bulkcut_recharge->expires_at);
        $now = Carbon::now();
        $diff = $date->diffInDays($now);
      }

      $data_remaining['bulk_balance'] = ($this->user->current_blukcut_credit ?? 0) . ' Balance';
      $data_remaining['validity'] = $diff. ' Days';
      $data_remaining['limit'] = ($current_plan->cut_limit - $bulk_limit) . ' Limit';

      if($data) {
        $cat_wallet['responseCode'] = "200";
        $cat_wallet['responseMessage'] = "Success";
        $cat_wallet['current_plan'] = $current_plan;
        $cat_wallet['last_recharge'] = $last_bulkcut_recharge;
        $cat_wallet['remaining_data'] = $data_remaining;
        $cat_wallet['cut_history'] = $data;
      } else {

        $cat_wallet['responseCode'] = "100";
        $cat_wallet['responseMessage'] = "Success";
        $cat_wallet['responseDescription'] = "No data found";
      }

    } else {

      $cat_wallet['responseCode'] = "401";
      $cat_wallet['responseMessage'] = "Error";
      $cat_wallet['responseDescription'] = "Sorry, You are not authorized";
    }

    return $cat_wallet;

  }
  /* end api/v1/get-bulkcut-wallet */

  public function updateCutWalletList(Request $request){

    if($this->user){

        $validator = Validator::make($request->all(),[
            'model_id' => 'required',
            'model_name' => 'required',
            'amount' => 'required',
            'actions' => 'required'
        ]);
        $temp = $validator->errors()->all();
        if($validator->fails()) 
        {
            $error['responseCode'] = "101";
            $cat_wallet['responseMessage'] = "Error";
            $cat_wallet['responseDescription'] = $temp;
            return $cat_wallet;
        }

        if ($this->user->parent_user_id == 0) {
          $user = $this->user;
        }else{
          $user = User::where('id',$this->user->parent_user_id)->first();
        }

        $all_inputs = $request->all(); 
        $all_inputs['user_id'] = $user->id;

        $retailerCreditBalance = CreditWallet::where('user_id','=',$user->id)->first();
        //dd($retailerCreditBalance->amount);
        if ($retailerCreditBalance) {

            if ($retailerCreditBalance->amount > $all_inputs['amount']) {
                $retailerCreditBalance->amount = intval($retailerCreditBalance->amount) - intval($all_inputs['amount']);
                $retailerCreditBalance->updated_at = now()->toDateString();
                $retailerCreditBalance->save();

                $data = CutWallet::create($all_inputs);
                $cat_wallet['responseCode'] = "200";
                $cat_wallet['responseMessage'] = "Success";
                $cat_wallet['data'] = $data;

            }else{
                $cat_wallet['responseCode'] = "100";
                $cat_wallet['responseMessage'] = "Error";
                $cat_wallet['responseDescription'] = "Insufficient amount in your Wallet.";
            }

        } else{
          $cat_wallet['responseCode'] = "100";
          $cat_wallet['responseMessage'] = "Error";
          $cat_wallet['responseDescription'] = "No data found";
        }

    } else {

      $cat_wallet['responseCode'] = "401";
      $cat_wallet['responseMessage'] = "Error";
      $cat_wallet['responseDescription'] = "Sorry, You are not authorized";
    }

    return $cat_wallet;

  }

  public function getCutWalletList(){

    if($this->user){

      if ($this->user->parent_user_id == 0) {
        $user = $this->user;
      }else{
        $user = User::where('id',$this->user->parent_user_id)->first();
      }

      $data = CutWallet::where('user_id', $user->id)->orderBy('id','DESC')->get();

      if($data) {

        $cat_wallet['responseCode'] = "200";
        $cat_wallet['responseMessage'] = "Success";
        $cat_wallet['data'] = $data;

      } else {

        $cat_wallet['responseCode'] = "100";
        $cat_wallet['responseMessage'] = "Success";
        $cat_wallet['responseDescription'] = "No data found";
      }

    } else {

      $cat_wallet['responseCode'] = "401";
      $cat_wallet['responseMessage'] = "Error";
      $cat_wallet['responseDescription'] = "Sorry, You are not authorized";
    }

    return $cat_wallet;

  }

  public function creditOffLineCut(Request $request){

    if($this->user){

        $validator = Validator::make($request->all(),[
            'amount' => 'required',
        ]);
        $temp = $validator->errors()->all();
        if($validator->fails()) 
        {
            $error['responseCode'] = "101";
            $off_line_cut['responseMessage'] = "Error";
            $off_line_cut['responseDescription'] = $temp;
            return $off_line_cut;
        }

        if ($this->user->parent_user_id == 0) {
          $user = $this->user;
        }else{
          $user = User::where('id',$this->user->parent_user_id)->first();
        }

        $all_inputs = $request->all(); 
        $all_inputs['user_id'] = $user->id;

        $data = OffLineCut::create($all_inputs);

        if($data) {

          $off_line_cut['responseCode'] = "200";
          $off_line_cut['responseMessage'] = "Success";
          $off_line_cut['data'] = $data;

        } else {

          $off_line_cut['responseCode'] = "100";
          $off_line_cut['responseMessage'] = "Success";
          $off_line_cut['responseDescription'] = "No data found";
        }

    } else {

      $off_line_cut['responseCode'] = "401";
      $off_line_cut['responseMessage'] = "Error";
      $off_line_cut['responseDescription'] = "Sorry, You are not authorized";
    }

    return $off_line_cut;

  }



  /* start api/v1/set-bulkcut-on 10/10/2020 tusar */
  public function setBulkCutOn(Request $request){
    if($this->user){

        $validator = Validator::make($request->all(),[
            'user_id' => 'required'
        ]);
        $temp = $validator->errors()->all();

        if ($this->user->parent_user_id == 0) {
          $user = $this->user;
        }else{
          $user = User::where('id',$this->user->parent_user_id)->first();
        }

        if($user->current_blukcut_credit>=$request->bulkcut_qty){

          $current_plan = BulkCutPlan::where('id', $user->current_blukcut_plan)->first();

          $current_blukcut_credit_after_conversion = $user->current_blukcut_credit - 0;

          User::where('id',$user->id)->update(['current_blukcut_credit'=>$current_blukcut_credit_after_conversion]);

          $date = Carbon::parse(now())->addHour($current_plan->duration);

          $all_inputs['user_id'] = $user->id;
          $all_inputs['blukcut_plan_id'] = $user->current_blukcut_plan;
          $all_inputs['cuts'] = $user->current_blukcut_credit;
          $all_inputs['actions'] = 'debit';
          $all_inputs['expires_at'] = $date;
          $data = BulkCutWallet::create($all_inputs);

          if($data) {
            $cat_wallet['responseCode'] = "200";
            $cat_wallet['responseMessage'] = "Success";
            $cat_wallet['data'] = $data;
          } 

      }else{
          $cat_wallet['responseCode'] = "100";
          $cat_wallet['responseMessage'] = "Error";
          $cat_wallet['responseDescription'] = "You have not sufficient blukcut credit.";
          return $cat_wallet;
        } 

    } else {

      $cat_wallet['responseCode'] = "401";
      $cat_wallet['responseMessage'] = "Error";
      $cat_wallet['responseDescription'] = "Sorry, You are not authorized";
    }

    return $cat_wallet;

  }
  /* end api/v1/set-bulkcut-on 10/10/2020 tusar */

  /* start api/v1/set-bulkcut-off 10/10/2020 tusar */
  public function getBulkCutOff(){

    if($this->user){

        if ($this->user->parent_user_id == 0) {
          $user = $this->user;
        } else {
          $user = User::where('id',$this->user->parent_user_id)->first();
        }

        if (empty($user->current_blukcut_plan) || empty($user->current_blukcut_credit)) {
//        if (empty($user->current_blukcut_plan)) {
          $cat_wallet['responseCode'] = "100";
          $cat_wallet['responseMessage'] = "Error";
          $cat_wallet['responseDescription'] = "OFF";
          return $cat_wallet;
        }

        $data = BulkCutWallet::where('user_id', $user->id)->where('blukcut_plan_id', $user->current_blukcut_plan)->orderBy('id','DESC')->first();

        if (Carbon::parse($data->expires_at) < Carbon::now()) {
          $cat_wallet['responseCode'] = "100";
          $cat_wallet['responseMessage'] = "Error";
          $cat_wallet['responseDescription'] = "OFF";
          return $cat_wallet;
        } else {
          $cat_wallet['responseCode'] = "200";
          $cat_wallet['responseMessage'] = "Success";
          $cat_wallet['data'] = 'ON';
          return $cat_wallet;
        }

    } else {

      $cat_wallet['responseCode'] = "401";
      $cat_wallet['responseMessage'] = "Error";
      $cat_wallet['responseDescription'] = "Sorry, You are not authorized";
    }

    return $cat_wallet;

  }
  /* end api/v1/set-bulkcut-on 10/10/2020 tusar */
  
}