<?php
namespace App\Http\Controllers\Api\V1;

use JWTAuth;
use App\Models\Video;
use App\Models\VideoCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class VideoController extends Controller
{
    /**
     * @var
     */
    protected $user;

    /**
     * TaskController constructor.
     */
    public function __construct()
    {
        $this->user = JWTAuth::parseToken()->authenticate();
		return $this->user;
    }

    public function getVideoCategory()
	{
		if($this->user){
			$tutorial_data = VideoCategory::select('id','title','image')->where('status','1')->orderBy('title')->get();
			if($tutorial_data) {
				$tutorial['responseCode'] = "200";
				$tutorial['responseMessage'] = "Success";
				$tutorial['responseDescription'] = "Success";
				$tutorial['data'] = $tutorial_data;
				$tutorial['video_image_url'] = "images/video-category/";
			}
			else{
				$tutorial['responseCode'] = "100";
				$tutorial['responseMessage'] = "Success";
				$tutorial['responseDescription'] = "No data found";
			}
		}
		else
		{
			$tutorial['responseCode'] = "401";
			$tutorial['responseMessage'] = "Error";
			$tutorial['responseDescription'] = "Sorry, You are not authorized";
		}
		return $tutorial;
	}

    public function getVideos(Request $request)
	{
		if($this->user){
			$tutorial_data = Video::select('video_title','video_link','video_image','video_description','video_status','video_type')
								->where('video_category_id',$request->id)
								->where('video_status','1')
								->orderBy('id','DESC')
								->get();
			if($tutorial_data) {
				$tutorial['responseCode'] = "200";
				$tutorial['responseMessage'] = "Success";
				$tutorial['responseDescription'] = "Success";
				$tutorial['data'] = $tutorial_data;
				$tutorial['video_image_url'] = "images/videos/";
			}
			else{
				$tutorial['responseCode'] = "100";
				$tutorial['responseMessage'] = "Success";
				$tutorial['responseDescription'] = "No data found";
			}
		}
		else
		{
			$tutorial['responseCode'] = "401";
			$tutorial['responseMessage'] = "Error";
			$tutorial['responseDescription'] = "Sorry, You are not authorized";
		}
		return $tutorial;
	}
	
	public function getTutorial()
	{
		if($this->user){
			$tutorial_data = Video::select('video_title','video_link','video_image','video_description','video_category','video_status')->orderBy('id','DESC')->where('video_category','Tutorial')->where('video_status','1')->get();
			if($tutorial_data) {
				$tutorial['responseCode'] = "200";
				$tutorial['responseMessage'] = "Success";
				$tutorial['responseDescription'] = "Success";
				$tutorial['data'] = $tutorial_data;
				$tutorial['video_image_url'] = "images/videos/";
			}
			else{
				$tutorial['responseCode'] = "100";
				$tutorial['responseMessage'] = "Success";
				$tutorial['responseDescription'] = "No data found";
			}
		}
		else
		{
			$tutorial['responseCode'] = "401";
			$tutorial['responseMessage'] = "Error";
			$tutorial['responseDescription'] = "Sorry, You are not authorized";
		}
		return $tutorial;
	}

	public function getVideoTutorial()
	{
		if($this->user){
			$video_tutorial_data = Video::select('video_title','video_link','video_image','video_description','video_category','video_status')->orderBy('id','DESC')->where('video_category','Other')->where('video_status','1')->get();
			if($video_tutorial_data) {
				$video_tutorial['responseCode'] = "200";
				$video_tutorial['responseMessage'] = "Success";
				$video_tutorial['responseDescription'] = "Success";
				$video_tutorial['data'] = $video_tutorial_data;
				$video_tutorial['video_image_url'] = "images/videos/";
			}
			else{
				$video_tutorial['responseCode'] = "100";
				$video_tutorial['responseMessage'] = "Success";
				$video_tutorial['responseDescription'] = "No data found";
			}
		}
		else
		{
			$video_tutorial['responseCode'] = "401";
			$video_tutorial['responseMessage'] = "Error";
			$video_tutorial['responseDescription'] = "Sorry, You are not authorized";
		}
		return $video_tutorial;
	}

	
	
}