<?php
namespace App\Http\Controllers\Api\V1;

use App\Models\Customer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Str;

class PrivacypolicyController extends Controller
{
   


      public function listprivacypolicy()
  {
      
      $data = DB::table('privacypolicy')->first();
      
         if(!empty($data)) {

        $privacy_policy_list['responseCode'] = "200";
        $privacy_policy_list['responseMessage'] = "Success";
        $privacy_policy_list['responseDescription'] = "Success";
        $privacy_policy_list['data'] = $data;

      } else {

        $privacy_policy_list['responseCode'] = "100";
        $privacy_policy_list['responseMessage'] = "Success";
        $privacy_policy_list['responseDescription'] = "No data found";
      }

      
      return $privacy_policy_list;

   
  }
public function settingpdf()
  {
      
      $data = DB::table('settingpdf')->first();
      
         if(!empty($data)) {

        $privacy_policy_list['responseCode'] = "200";
        $privacy_policy_list['responseMessage'] = "Success";
        $privacy_policy_list['path'] = url('/settingpdf/');
        $privacy_policy_list['data'] = $data;

      } else {

        $privacy_policy_list['responseCode'] = "100";
        $privacy_policy_list['responseMessage'] = "Success";
        $privacy_policy_list['responseDescription'] = "No data found";
      }

      
      return $privacy_policy_list;

   
  }

	
}