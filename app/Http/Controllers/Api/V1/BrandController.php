<?php

namespace App\Http\Controllers\Api\V1;

use JWTAuth;
use App\Models\BrandType;
use App\Models\Brand;
use App\Models\BrandModel;
use App\Models\BrandSubModel;
use App\Models\BrandModelScreenImage;
use App\Models\UserReactions;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use DB;

class BrandController extends Controller {

    /**
     * @var
     */
    protected $user;

    /**
     * TaskController constructor.
     */
    public function __construct() {
        $this->user = JWTAuth::parseToken()->authenticate();
        return $this->user;
    }

    public function Serach(Request $request) {
        if ($this->user) {
            $q = $request->q;

            $data['brand_type'] = BrandType::where('status', '1')->where('name', 'LIKE', '%' . $q . '%')->get(['id', 'name', 'image'])->toArray();

            $data['brand'] = Brand::where('status', '1')->where('name', 'LIKE', '%' . $q . '%')->get(['id', 'name', 'image'])->toArray();

            $data['brand_model'] = BrandModel::where('status', '1')->where('name', 'LIKE', '%' . $q . '%')->get(['id', 'name', 'image'])->toArray();

            /* $data['screen_image'] = BrandModelScreenImage::where('status','1')->where('name','LIKE','%'.$q.'%')->get(['id', 'name','image'])->toArray(); */

            if ($data) {
                $serach_data['responseCode'] = "200";
                $serach_data['responseMessage'] = "Success";
                $serach_data['data'] = $data;
                $serach_data['url'] = "images/brand-types/";
            } else {
                $serach_data['responseCode'] = "100";
                $serach_data['responseMessage'] = "Success";
                $serach_data['responseDescription'] = "No data found";
            }
        } else {
            $serach_data['responseCode'] = "401";
            $serach_data['responseMessage'] = "Error";
            $serach_data['responseDescription'] = "Sorry, You are not authorized";
        }
        return $serach_data;
    }

    public function getBrandTypeList() {
        if ($this->user) {
            $user_id = $this->user->id;
            if($this->user->parent_user_id > 0){
                $user_id = $this->user->parent_user_id;
            }
            $user_addon_plan_ids = [];
            $user_current_plan = User::where('id', $user_id)->value('current_plan');
            
            $user_addon_plan = DB::table('user_addon_plans')->where('user_id', $user_id)->get()->toArray();
            if(!empty($user_addon_plan)){
                $user_addon_plan_ids = array_column($user_addon_plan,'plan_id');
            }
            if(!empty($user_current_plan)){
                array_push($user_addon_plan_ids, $user_current_plan);
            }
            
            $h9 = 0;
            if(in_array(config('constants.plans.9h'), $user_addon_plan_ids)){
                $h9 = "1";
            }
                                
            $brand_type_data = BrandType::where('status', '1')
                    ->leftJoin('plan_brandtypes', 'plan_brandtypes.brand_type_id', '=', 'brand_types.id')
                    ->whereIn('plan_brandtypes.plan_id', $user_addon_plan_ids)
                    ->groupBy('brand_types.id')
                    ->orderBy('order_number', 'DESC')
                    ->orderBy('brand_types.id', 'DESC')
                    ->get(['brand_types.id', 'name', 'image',DB::raw("$h9 as 9H")])->toArray();
            if ($brand_type_data) {
                $brand_type['responseCode'] = "200";
                $brand_type['responseMessage'] = "Success";
                $brand_type['responseDescription'] = "Success";
                $brand_type['data'] = $brand_type_data;
                $brand_type['url'] = "images/brand-types/";
            } else {
                $brand_type['responseCode'] = "100";
                $brand_type['responseMessage'] = "Success";
                $brand_type['responseDescription'] = "No data found";
            }
        } else {
            $brand_type['responseCode'] = "401";
            $brand_type['responseMessage'] = "Error";
            $brand_type['responseDescription'] = "Sorry, You are not authorized";
        }
        return $brand_type;
    }

    public function getBrandList(Request $request) {
        if ($this->user) {
            $user_id = $this->user->id;
            if($this->user->parent_user_id > 0){
                $user_id = $this->user->parent_user_id;
            }
            $user_addon_plan_ids = [];
            $user_current_plan = User::where('id', $user_id)->value('current_plan');
            
            $user_addon_plan = DB::table('user_addon_plans')->where('user_id', $user_id)->get()->toArray();
            if(!empty($user_addon_plan)){
                $user_addon_plan_ids = array_column($user_addon_plan,'plan_id');
            }
            if(!empty($user_current_plan)){
                array_push($user_addon_plan_ids, $user_current_plan);
            }


            if (isset($request->brand_type)) {
                $brands_data = Brand::select('brands.id', 'name', 'image')
                        ->leftJoin('plan_brandtypes', 'plan_brandtypes.brand_type_id', '=', 'brands.brand_type_id')
                        ->withCount('model')
                        ->whereIn('plan_brandtypes.plan_id', $user_addon_plan_ids)
                        ->where([['brands.brand_type_id', '=', $request->brand_type], ['status', '=', 1]])
                        ->groupBy('brands.id')
                        ->orderBy('order_number', 'DESC')
                        ->orderBy('id', 'DESC')
                        ->get()->toArray();
            } else {
                $brands_data = Brand::select('brands.id', 'name', 'image')
                        ->leftJoin('plan_brandtypes', 'plan_brandtypes.brand_type_id', '=', 'brands.brand_type_id')
                        ->withCount('model')
                        ->whereIn('plan_brandtypes.plan_id', $user_addon_plan_ids)
                        ->where('status', '=', 1)
                        ->groupBy('brands.id')
                        ->orderBy('order_number', 'DESC')
                        ->orderBy('id', 'DESC')
                        ->get()->toArray();
            }

            $brands_data;
            if ($brands_data) {
                $brands['responseCode'] = "200";
                $brands['responseMessage'] = "Success";
                $brands['data'] = $brands_data;
                $brands['url'] = "images/brands/";
            } else {
                $brands['responseCode'] = "100";
                $brands['responseMessage'] = "Success";
                $brands['responseDescription'] = "No data found";
            }
        } else {
            $brands['responseCode'] = "401";
            $brands['responseMessage'] = "Error";
            $brands['responseDescription'] = "Sorry, You are not authorized";
        }
        return $brands;
    }

    /* @narenthaker July 05, 2021 18:14 getAllBrandList start */

    public function getAllBrandList(Request $request) {
        if ($this->user) {

            $user_id = $this->user->id;
            if($this->user->parent_user_id > 0){
                $user_id = $this->user->parent_user_id;
            }
            $user_addon_plan_ids = [];
            $user_current_plan = User::where('id', $user_id)->value('current_plan');
            
            $user_addon_plan = DB::table('user_addon_plans')->where('user_id', $user_id)->get()->toArray();
            if(!empty($user_addon_plan)){
                $user_addon_plan_ids = array_column($user_addon_plan,'plan_id');
            }
            if(!empty($user_current_plan)){
                array_push($user_addon_plan_ids, $user_current_plan);
            }
            
            $q = $request->brand_type ?? '';

            if (!empty($q)) {
                
                $brands_data = Brand::select('brands.id', 'name', 'image')
                        ->leftJoin('plan_brandtypes', 'plan_brandtypes.brand_type_id', '=', 'brands.brand_type_id')
                        ->withCount('model')
                        ->whereIn('plan_brandtypes.plan_id', $user_addon_plan_ids)
                        ->where('status', '=', 1)
                        ->where('name', 'LIKE', '%' . $q . '%')
                        ->groupBy('brands.id')
                        ->orderBy('name')
                        ->get()->toArray();
                
//                $data = Brand::select('id', 'name', 'image')->withCount('model')->where('name', 'LIKE', '%' . $q . '%')->where('status', '1')->orderBy('name')->get()->toArray();
                $data = $brands_data;

                $data1 = BrandModel::where('status', '1')->where('name', 'LIKE', '%' . $q . '%')->get(['id', 'name', 'image'])->toArray();
            } else {
                $brands_data = Brand::select('brands.id', 'name', 'image')
                        ->leftJoin('plan_brandtypes', 'plan_brandtypes.brand_type_id', '=', 'brands.brand_type_id')
                        ->withCount('model')
                        ->whereIn('plan_brandtypes.plan_id', $user_addon_plan_ids)
                        ->where('status', '=', 1)
                        ->groupBy('brands.id')
                        ->orderBy('name')
                        ->get()->toArray();
                
//                $data = Brand::select('id', 'name', 'image')->withCount('model')->where('status', '1')->orderBy('name')->get()->toArray();
                $data = $brands_data;
                $data1 = BrandModel::where('status', '1')->where('name', 'LIKE', '%' . $q . '%')->get(['id', 'name', 'image'])->toArray();
            }

            $data2 = array_merge($data, $data1);

            if ($data2) {
                $numbers = array();
                $letters = array();
                foreach ($data2 as $key => $value) {
                    if (is_numeric(substr($value['name'], 0, 1))) {
                        $numbers[] = $value;
                    } else {
                        $letters[] = $value;
                    }
                }
                array_multisort(array_column($letters, 'name'), SORT_ASC, $letters);
                array_multisort(array_column($numbers, 'name'), SORT_ASC, $numbers);
                $data3 = array_merge($letters, $numbers);

                // for($i=0;$i<sizeof($data1);$i++){
                //   $modals = BrandModel::where('status', '1')->where('name', 'LIKE', '%' . $q . '%')->where('brand_id','=', $data1[$i]['id'])->orderBy('name')->get(['id', 'name', 'image'])->toArray();
                //   $data2[$i] = array('id'=>$data1[$i]['id'],'name'=>$data1[$i]['name'],'image'=>$data1[$i]['image'],'model_count'=>$data1[$i]['model_count'],'models' =>$modals);
                // }
                $brands['responseCode'] = "200";
                $brands['responseMessage'] = "Success";
                $brands['brand_url'] = "images/brands/";
                $brands['brand_model_url'] = "images/brand-types/";
                $brands['data'] = $data3;
            } else {
                $brands['responseCode'] = "100";
                $brands['responseMessage'] = "Success";
                $brands['responseDescription'] = "No data found";
            }
        } else {
            $brands['responseCode'] = "401";
            $brands['responseMessage'] = "Error";
            $brands['responseDescription'] = "Sorry, You are not authorized";
        }
        return $brands;
    }

    public function cmp($a, $b) {
        return strcmp($a["name"], $b["name"]);
    }

    /* @narenthaker July 05, 2021 18:14 getAllBrandList end */

    public function getTodayBrandModelList(Request $request) {

        if ($this->user) {

            $brand_model_data = BrandModel::select('id', 'name', 'image')->whereDate('created_at', Carbon::today())->where('status', '=', '1')->orderBy('order_number', 'DESC')->orderBy('id', 'DESC')->get(['id', 'name', 'image'])->toArray();


            //return $brand_model['data'][$key]['total_screen_image'];
            if ($brand_model_data) {
                $brand_model['responseCode'] = "200";
                $brand_model['responseMessage'] = "Success";
                foreach ($brand_model_data as $key => $value) {

                    //return $value['id'];
                    $totalImage = $this->countModelScreenImage($value['id']);

                    $brand_model['data'][$key]['screen_shape_count'] = $totalImage;
                    $brand_model['data'][$key]['id'] = $value['id'];
                    $brand_model['data'][$key]['name'] = $value['name'];
                    $brand_model['data'][$key]['image'] = $value['image'];
                }
                $brand_model['url'] = "images/models/";
            } else {
                $brand_model['responseCode'] = "100";
                $brand_model['responseMessage'] = "Success";
                $brand_model['responseDescription'] = "No data found";
            }
        } else {
            $brand_model['responseCode'] = "401";
            $brand_model['responseMessage'] = "Error";
            $brand_model['responseDescription'] = "Sorry, You are not authorized";
        }
        return $brand_model;
    }

    public function countModelScreenImage($model_id) {
        $totalImage = 0;
        $brandModelScreenData = BrandModelScreenImage::where([['model_id', '=', $model_id], ['status', '=', '1']])->get(['image'])->toArray();
        //return "vvvv";
        if ($brandModelScreenData) {
            foreach ($brandModelScreenData as $key => $value)
                $totalImage = $totalImage + count(json_decode($value['image'], TRUE));
            return $totalImage;
        }
    }

    public function getBrandModelList(Request $request) {

        if ($this->user) {
            $brand_model_data = BrandModel::select('id', 'name', 'image', 'brand_type_id')->where([['brand_id', '=', $request->brand_id], ['status', '=', '1']])->orderBy('order_number', 'DESC')->orderBy('id', 'DESC')->get(['id', 'name', 'image'])->toArray();


            //return $brand_model['data'][$key]['total_screen_image'];
            if ($brand_model_data) {
                $brand_model['responseCode'] = "200";
                $brand_model['responseMessage'] = "Success";
                foreach ($brand_model_data as $key => $value) {
                    $download = DB::select('SELECT * FROM brand_types where id=' . $value['brand_type_id'])[0]->download;
                    //return $value['id'];
                    //$totalImage = $this->countModelScreenImage($value['id']);
                    //$brand_model['data'][$key]['screen_shape_count'] = $totalImage;
                    $brand_model['data'][$key]['id'] = $value['id'];
                    $brand_model['data'][$key]['name'] = $value['name'];
                    $brand_model['data'][$key]['image'] = $value['image'];
                    $brand_model['data'][$key]['download_access'] = $download;
                }
                $brand_model['url'] = "images/models/";
            } else {
                $brand_model['responseCode'] = "100";
                $brand_model['responseMessage'] = "Success";
                $brand_model['responseDescription'] = "No data found";
            }
        } else {
            $brand_model['responseCode'] = "401";
            $brand_model['responseMessage'] = "Error";
            $brand_model['responseDescription'] = "Sorry, You are not authorized";
        }
        return $brand_model;
    }

    public function getBrandSubModelList(Request $request) {

        if ($this->user) {
            $brand_model_data = BrandSubModel::select('id', 'name', 'image', 'cut_point', 'brand_type_id')->where([['brand_models_id', '=', $request->model_id], ['status', '=', '1']])->orderBy('order_number', 'DESC')->orderBy('id', 'DESC')->get(['id', 'name', 'image', 'cut_point'])->toArray();

            if ($brand_model_data) {
                $brand_model['responseCode'] = "200";
                $brand_model['responseMessage'] = "Success";
                foreach ($brand_model_data as $key => $value) {
                    $download = DB::select('SELECT * FROM brand_types where id=' . $value['brand_type_id'])[0]->download;

                    $totalImage = $this->countModelScreenImage($value['id']);

                    $brand_model['data'][$key]['screen_shape_count'] = $totalImage;
                    $brand_model['data'][$key]['id'] = $value['id'];
                    $brand_model['data'][$key]['name'] = $value['name'];
                    $brand_model['data'][$key]['image'] = $value['image'];
                    $brand_model['data'][$key]['cut_point'] = $value['cut_point'];
                    $brand_model['data'][$key]['download_access'] = $download;
                }
                $brand_model['url'] = "images/models/";
            } else {
                $brand_model['responseCode'] = "100";
                $brand_model['responseMessage'] = "Success";
                $brand_model['responseDescription'] = "No data found";
            }
        } else {
            $brand_model['responseCode'] = "401";
            $brand_model['responseMessage'] = "Error";
            $brand_model['responseDescription'] = "Sorry, You are not authorized";
        }
        return $brand_model;
    }

    public function getBrandModelScreenList(Request $request) {
        $brand_model_screen_data1 = array();
        if ($this->user) {
            $brand_model_screen_data = BrandModelScreenImage::where([['model_id', '=', $request->model_id], ['status', '=', '1']])->orderBy('order_number', 'DESC')->orderBy('id', 'DESC')->get(['id', 'image'])->toArray();
            //return $brand_model_screen_data;
            //return $brand_model_screen_data1;
            if ($brand_model_screen_data) {
                $brand_model_screen['responseCode'] = "200";
                $brand_model_screen['responseMessage'] = "Success";

                foreach ($brand_model_screen_data as $keys => $v) {
                    //return $v['image'];
                    $download = DB::select('SELECT * FROM brand_types where id=' . $v['brand_type_id'])[0]->download;
                    $brand_model_screen_data1['id'] = $v['id'];
                    $images = json_decode($v['image'], TRUE);
                    $k = 0;
                    //$images = ($images);
                    foreach ($images as $key => $value) {
                        $brand_model_screen_data1['images'][$k]['image_id'] = $key;
                        $brand_model_screen_data1['images'][$k]['image_name'] = $value;
                        $brand_model_screen_data1['images'][$k]['download_access'] = $download;
                        $k++;
                    }
                    $brand_model_screen['data'][$keys] = $brand_model_screen_data1;
                }

                $brand_model_screen['url'] = "images/models-screen-image/";
            } else {
                $brand_model_screen['responseCode'] = "100";
                $brand_model_screen['responseMessage'] = "Success";
                $brand_model_screen['responseDescription'] = "No data found";
            }
        } else {
            $brand_model_screen['responseCode'] = "401";
            $brand_model_screen['responseMessage'] = "Error";
            $brand_model_screen['responseDescription'] = "Sorry, You are not authorized";
        }
        return $brand_model_screen;
    }

    public function getBrandModelScreenListWithBlt(Request $request) {
        $brand_model_screen_data1 = array();
        if ($this->user) {

            $brand_model_screen_data = BrandModelScreenImage::
                            where('sub_model_id', $request->sub_model_id)
                            ->where('status', 1)
                            ->orderBy('id', 'DESC')
                            ->with(['brandsImages' => function ($query) {
                                    $query->withCount('userlikes');
                                    $query->withCount('userunlikes');
                                }])
                            ->get()->toArray();

//            $brand_model_screen_data = DB::select('select * from brand_model_screen_images where sub_model_id = '.$request->sub_model_id.' and status = 1 order by id');

            $brand_model_screen_data = json_decode(json_encode($brand_model_screen_data), true);
            //print_r($brand_model_screen_data);exit;
            if ($brand_model_screen_data) {
                $brand_model_screen['responseCode'] = "200";
                $brand_model_screen['responseMessage'] = "Success";

                foreach ($brand_model_screen_data as $keys => $v) {
                    //return $v['image'];
                    $download = DB::select('SELECT * FROM brand_types where id=' . $v['brand_type_id'])[0]->download;
                    $brand_model_screen_data1['id'] = $v['id'];
                    $name = json_decode($v['name'], TRUE);
                    $name = $name === NULL ? [0 => ''] : $name;
                    $images = json_decode($v['image'], TRUE);
                    $blt_files = json_decode($v['blt_file'], TRUE);

                    $json_width = json_decode($v['width'], TRUE);
                    $width = $json_width === NULL ? [0 => ''] : $json_width;

                    $json_height = json_decode($v['height'], TRUE);
                    $height = $json_height === NULL ? [0 => ''] : $json_height;
//                    $k = 0;
                    //$images = ($images);
//                    foreach ($images as $key => $value) {
//                        $brand_model_screen_data1['images'][$k]['image_id'] = $key;
//                        $brand_model_screen_data1['images'][$k]['name'] = (isset($name[$k]) && !empty($name[$k])) ? $name[$k] : strtok($value, '.') ?? '';
//                        $brand_model_screen_data1['images'][$k]['image_name'] = $value;
//                        $brand_model_screen_data1['images'][$k]['blt_file'] = isset($blt_files[$k]) ? $blt_files[$k] : "";
//                        $brand_model_screen_data1['images'][$k]['width'] = (isset($width[$k]) && !empty($width[$k])) ? $width[$k] : '';
//                        $brand_model_screen_data1['images'][$k]['height'] = (isset($height[$k]) && !empty($height[$k])) ? $height[$k] : '';
//                        $brand_model_screen_data1['images'][$k]['download_access'] = $download;
//                        $k++;
//                    }
                    $brand_model_screen_data1['images'] = [];
                    if (!empty($v['brands_images'])) {
                        foreach ($v['brands_images'] as $k => $value) {
                            $brand_model_screen_data1['images'][$k]['image_id'] = $value['id'];
                            $brand_model_screen_data1['images'][$k]['name'] = $value['screen_name'];
                            $brand_model_screen_data1['images'][$k]['image_name'] = $value['image_file'];
                            $brand_model_screen_data1['images'][$k]['blt_file'] = $value['blt_file'];
                            $brand_model_screen_data1['images'][$k]['width'] = $value['width'];
                            $brand_model_screen_data1['images'][$k]['height'] = $value['height'];
                            $brand_model_screen_data1['images'][$k]['download_access'] = $download;
                            $brand_model_screen_data1['images'][$k]['likes'] = $value['userlikes_count'];
                            $brand_model_screen_data1['images'][$k]['unlikes'] = $value['userunlikes_count'];
                            $reaction = UserReactions::where('model_screen_image_id',$value['id'])->where('user_id',$this->user->id)->first();
                            $brand_model_screen_data1['images'][$k]['user_reaction'] = $reaction->status ?? 0;
                            $brand_model_screen_data1['images'][$k]['user_comment'] = $reaction->comment ?? '';
//                            $k++;
                        }
                    }
                    /* @$myArray = json_decode($v['blt_file']);  
                      if( $myArray === NULL ){
                      $brand_model_screen_data1['blt_file'] = $v['blt_file'];
                      }else{
                      $brand_model_screen_data1['blt_file'] = json_encode($v['blt_file']);
                      } */
                    $brand_model_screen['data'][$keys] = $brand_model_screen_data1;
                }

                $brand_model_screen['url'] = "images/models-screen-image/";
            } else {
                $brand_model_screen['responseCode'] = "100";
                $brand_model_screen['responseMessage'] = "Success";
                $brand_model_screen['responseDescription'] = "No data found";
            }
        } else {
            $brand_model_screen['responseCode'] = "401";
            $brand_model_screen['responseMessage'] = "Error";
            $brand_model_screen['responseDescription'] = "Sorry, You are not authorized";
        }
        return $brand_model_screen;
    }

    public function getTodayBrandModelListReleasewise(Request $request) {
        if ($this->user) {

            $brands_datas = DB::table('brands')
                            ->join('brand_models', 'brands.id', '=', 'brand_models.brand_id')
                            ->join('brand_model_screen_images', 'brand_models.id', '=', 'brand_model_screen_images.model_id')
                            ->select('brands.id', 'brands.name as brand', 'brand_models.name as model', 'brand_models.id as model_id', 'brand_models.image', 'brand_models.created_at as date', 'brand_model_screen_images.image as images', 'brand_model_screen_images.blt_file')
                            ->orderBy('brand_models.created_at', 'DESC')
                            ->get()->groupBy(function($item) {
                return Carbon::parse($item->date)->format('d-m-Y');
            });

            $data = [];

            $k = 0;

            foreach ($brands_datas as $k1 => $brands_data) {

                $data[$k]['date'] = $k1;

                if ($brands_data) {

                    foreach ($brands_data as $key => $value) {

                        if (isset($value->images)) {
                            $value->images = json_decode($value->images, TRUE);
                            foreach ($value->images as $index => $image_name) {
                                $brand_model_screen_images[$index]['image_id'] = $index;
                                $brand_model_screen_images[$index]['image_name'] = $image_name;
                            }
                            $value->images = $brand_model_screen_images;
                        }
                        if (isset($value->date)) {
                            $value->date = Carbon::parse($value->date)->format('d-m-Y');
                        }
                    }
                    $data[$k]['data'] = $brands_data;
                }

                $k++;
            }

            if ($data) {

                $brands['responseCode'] = "200";
                $brands['responseMessage'] = "Success";
                $brands['alldata'] = $data;
                $brands['image_url'] = "images/brands/";
                $brands['images_url'] = "images/models-screen-image/";
            } else {
                $brands['responseCode'] = "100";
                $brands['responseMessage'] = "Success";
                $brands['responseDescription'] = "No data found";
            }
        } else {
            $brands['responseCode'] = "401";
            $brands['responseMessage'] = "Error";
            $brands['responseDescription'] = "Sorry, You are not authorized";
        }
        return $brands;
    }

}
