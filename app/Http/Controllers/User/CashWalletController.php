<?php

namespace App\Http\Controllers\User;
use App\User;
use App\Models\CreditWallet;
use App\Models\CashWallet;
use App\Models\CreditWalletTransaction;
use App\Models\CashWalletTransaction;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Redirect;
Use App\helper;
class CashWalletController extends Controller
{
     protected $cashWallet;
   
   public function __construct(Request $request)
  {
	 
     $this->cashWallet = new CashWallet();
  }
  
  public function cashTransactionReport()
{
	 if (!Gate::allows('manage_cash_wallet')) {
            return abort(401);
        }
	$cash = $this->cashWallet->retailerCashBalance();
	$cashTransaction = CashWalletTransaction::where('receiver_id',auth()->user()->id)->orderBy('id','desc')->get();
	return view('user.cash-wallet.cash-transaction',compact('cashTransaction','cash'));
}

}
