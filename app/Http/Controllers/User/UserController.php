<?php

namespace App\Http\Controllers\User;

use App\User;
use Spatie\Permission\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use App\Models\KycCategory;
use App\Models\UserContactDetail;
use App\Models\UserBankDetail;
use App\Models\UserKycDetail;
use App\Models\ShopDetail;
use App\Models\CreditWallet;
use App\Models\CashWallet;
use App\Models\AccountRecharge;
use App\Models\CreditWalletTransaction;
use App\Models\CashWalletTransaction;
use App\Models\Customer;
use Illuminate\Support\Facades\Redirect;
use App\Models\Log;
use App\Models\RechargeCommission;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreUsersRequest;
use App\Http\Requests\Admin\UpdateUsersRequest;
use Illuminate\Support\Facades\Validator;
use DB;
use DateTime;

class UserController extends Controller
{

    /**
     * Display a listing of User.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!Gate::allows('users_manage')) {
            return abort(401);
        }

        $users = User::all();

        return view('admin.users.index', compact('users'));
    }

    /**
     * Show the form for creating new User.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!Gate::allows('users_manage')) {
            return abort(401);
        }
        $roles = Role::get()->pluck('name', 'name');

        return view('admin.users.create', compact('roles'));
    }

    /**
     * Store a newly created User in storage.
     *
     * @param  \App\Http\Requests\StoreUsersRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUsersRequest $request)
    {
        if (!Gate::allows('users_manage')) {
            return abort(401);
        }
        $user = User::create($request->all());
        $roles = $request->input('roles') ? $request->input('roles') : [];
        $user->assignRole($roles);

        return redirect()->route('admin.users.index');
    }

    /**
     * Show the form for editing User.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        if (!Gate::allows('users_manage')) {
            return abort(401);
        }
        $roles = Role::get()->pluck('name', 'name');

        return view('admin.users.edit', compact('user', 'roles'));
    }

    /**
     * Update User in storage.
     *
     * @param  \App\Http\Requests\UpdateUsersRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUsersRequest $request, User $user)
    {
        if (!Gate::allows('users_manage')) {
            return abort(401);
        }

        $user->update($request->all());
        $roles = $request->input('roles') ? $request->input('roles') : [];
        $user->syncRoles($roles);

        return redirect()->route('admin.users.index');
    }

    public function show(User $user)
    {
        if (!Gate::allows('users_manage')) {
            return abort(401);
        }

        $user->load('roles');

        return view('admin.users.show', compact('user'));
    }

    /**
     * Remove User from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        if (!Gate::allows('users_manage')) {
            return abort(401);
        }

        $user->delete();

        return redirect()->route('admin.users.index');
    }

    /**
     * Delete all selected User at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (!Gate::allows('users_manage')) {
            return abort(401);
        }
        User::whereIn('id', request('ids'))->delete();

        return response()->noContent();
    }

    public function getStateList(Request $request)
    {
        $states = DB::table("states")
            ->where("country_id", $request->country_id)
            ->pluck("name", "id");
        return response()->json($states);
    }

    public function getPlanList(Request $request)
    {
        $plans = DB::table("recharge_plans")
            ->where("recharge_plan_type", $request->current_plan_type)
            ->pluck("recharge_plan_name", "id");
        return response()->json($plans);
    }

    public function getSerialNumberList(Request $request)
    {

        $plans = DB::table("planserial_numbers")
            ->whereIn("plan_id", explode(',', $request->current_plan_type))
            ->where("user_id", NULL)
            ->pluck("serial_number", "id");
        return response()->json($plans);
    }

    public function updateProfile()
    {
        $userId = auth()->user()->id;

        $user = User::find($userId);
        $countries = DB::table("countries")->pluck("name", "id");
        $kycCategory = KycCategory::orderBy('id', 'DESC')->get();

        return view('user.account.edit', compact('user', 'countries', 'kycCategory'));
    }

    public function updateGeneralInformation(Request $request, $userId)
    {


        $user = User::findOrFail($userId);
        $user = $user->update($request->all());
        if ($user) {
            return redirect()->back()->with('message', 'General Information updated successfully');
        } else {
            return redirect()->back() - with('message', 'Something webt wrong');
        }
    }

    public function updateContactDetail(Request $request, $userId)
    {

        request()->validate([
            'address' => 'required',
            'country' => 'required',
            'state' => 'required',
            'city' => 'required',
            'pin_code' => 'required',
        ], [
            'address.required' => 'Address is required',
            'country.required' => 'Country is required',
            'State.required' => 'State is required',
            'city.required' => 'City is required',
            'pin_code.required' => 'Pin Code is required',
        ]);


        $user = User::findOrFail($userId);

        if ($request->country != 101 && empty($user->email)) {
            return redirect()->back()->with('message', 'Email id is required');
        }

        $userContactDetail = $user->getContactDetail()->first();

        if (!empty($userContactDetail)) {
            $user = $userContactDetail->update($request->all());
        } else {

            $user = UserContactDetail::create($request->all());
        }

        if ($user) {
            return redirect()->back()->with('message', 'Contact Information updated successfully');
        } else {
            return redirect()->back() - with('message', 'Something went wrong');
        }
    }

    //Update user bank detail
    public function updateBankDetail(Request $request, $userId)
    {
        request()->validate([
            'bank_name' => 'required',
            'account_no' => 'required',
            'branch_name' => 'required',
            'account_name' => 'required',
            'ifsc_code' => 'required',
            'account_type' => 'required',
            'mobile_number' => 'required',
        ], [
            'bank_name.required' => 'Bank name is required',
            'account_no.required' => 'Account number is required',
            'branch_name.required' => 'Branch name is required',
            'ifsc_code.required' => 'IFSC Code is required',
            'account_type.required' => 'Account type is required',
            'mobile_number.required' => 'Mobile number connect with this account is required',
        ]);
        // dd($request->all());
        $user = User::findOrFail($userId);
        $userBankDetail = $user->getBankDetail()->first();

        if (!empty($userBankDetail)) {
            $user = $userBankDetail->update($request->all());
        } else {
            $user = UserBankDetail::create($request->all());
        }
        if ($user) {
            return redirect()->back()->with('message', 'Bank Detail updated successfully');
        } else {
            return redirect()->back()->with('message', 'Something went wrong');
        }
    }

    //Udapdte user KYC detail

    public function updateKycDetail(Request $request, $userId)
    {

        request()->validate([
            'kyc_id' => 'required',
        ], [
            'kyc_id.required' => 'KYC type is required',
        ]);

        $user = User::findOrFail($userId);

        $userKycDetail = $user->getKycDetail()->first();

        if (!empty($userKycDetail)) {

            /* if (empty($key_number)) {
              return redirect()->back()->with('message', 'KYC number is required');
              } */

            $input_all = $request->all();
            $kyc_number = !empty($request->kyc_number) ? $request->kyc_number : $request->adhaar_number;
            $input_all['kyc_number'] = $kyc_number;
            $input_all['company_name'] = !empty($request->company_name) ? $request->company_name : null;
            $input_all['dob'] = !empty($request->dob) ? $request->dob : null;
            $input_all['status'] = isset($request->status) ? 1 : 0;

            if ($request->has('machine_number') && $request->has('machine_price') && $request->has('machine_purchase_date') && $request->has('machine_purchase_date')) {
                $input_all['notes'] = $request->notes ?? null;
                $input_all['machine_number'] = $request->machine_number ?? null;
                $input_all['machine_price'] = $request->machine_price ?? null;
                $input_all['machine_purchase_date'] = $request->machine_purchase_date ? \Carbon\Carbon::parse($request->machine_purchase_date)->format('Y-m-d') : null;
            }

            $user = $userKycDetail->update($input_all);
            if ($request->attachment != null) {
                $attachment = time() . '.' . $request->attachment->getClientOriginalExtension();
                $request->attachment->move(public_path('Admin/images/kyc'), $attachment);
                $userKycDetail->attachment = $attachment;
            } else {
                $imageUp = $request->attachmnetUpdate;
                $userKycDetail->attachment = $imageUp;
            }
            $userKycDetail = $userKycDetail->save();
        } else {

            if (!empty($request->attachment)) {
                $attachment = time() . '.' . $request->attachment->getClientOriginalExtension();
                $request->attachment->move(public_path('Admin/images/kyc'), $attachment);
            } else {
                $attachment = '';
            }
            $userKyc = new UserKycDetail();
            $userKyc->attachment = $attachment;
            $userKyc->user_id = $request->user_id;
            $userKyc->kyc_id = $request->kyc_id;
            $userKyc->kyc_number = !empty($request->kyc_number) ? $request->kyc_number : $request->adhaar_number;
            $userKyc->dob = !empty($request->dob) ? $request->dob : null;
            $userKyc->company_name = !empty($request->company_name) ? $request->company_name : null;
            $userKyc->kyc_expiry_date = $request->kyc_expiry_date ? $request->kyc_expiry_date : null;
            $userKyc->customer_name = $request->customer_name ? $request->customer_name : null;
            $userKyc->phone = $request->phone ? $request->phone : null;
            $userKyc->emailKyc = $request->emailKyc ? $request->emailKyc : null;
            $userKyc->address = $request->address ? $request->address : null;
            $userKyc->country_id = $request->country_id ? $request->country_id : null;
            $userKyc->state_id = $request->state_id ? $request->state_id : null;
            $userKyc->city = $request->city ? $request->city : null;
            $userKyc->pin_code = $request->pin_code ? $request->pin_code : null;
            $userKyc->status = isset($request->status) ? 1 : 0;

            if ($request->has('machine_number') && $request->has('machine_price') && $request->has('machine_purchase_date') && $request->has('notes')) {
                $userKyc->notes = $request->notes ?? null;
                $userKyc->machine_number = $request->machine_number ?? null;
                $userKyc->machine_price = $request->machine_price ?? null;
                $userKyc->machine_purchase_date = $request->machine_purchase_date ? Carbon::parse($request->machine_purchase_date)->format('Y-m-d') : null;
            }

            $userKycDetail = $userKyc->save();
        }
        if ($userKycDetail) {
            return redirect()->back()->with('message', 'KYC Detail updated successfully');
        } else {
            return redirect()->back()->with('message', 'Something went wrong');
        }
    }

    //Update user shop detail
    public function updateShopDetail(Request $request, $userId)
    {

        request()->validate(
            ['shop_name' => 'required',],
            [
                //'shop_name.required' => 'Shop name is required',
                'shop_photo' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:5048',
            ]
        );

        // dd($request->all());
        $user = User::findOrFail($userId);
        $user->shop_name = $request->shop_name;
        $user->save();

        $userShopDetail = $user->getShopDetail()->first();

        $all_inputs = $request->all();


        if (!empty($request->file('shop_photo'))) {
            $file = $request->file('shop_photo');
            $destinationPath = public_path('Admin/images/shop'); // upload path
            $photo = time() . 'shop.' . $file->getClientOriginalExtension();
            $file->move($destinationPath, $photo);
            $all_inputs['shop_photo'] = $photo;
        } else {
            $all_inputs['shop_photo'] = $userShopDetail->shop_photo;
        }
        if (!empty($request->file('machine_photo'))) {
            $file = $request->file('machine_photo');
            $destinationPath = public_path('Admin/images/shop'); // upload path
            $photo = time() . 'machine.' . $file->getClientOriginalExtension();
            $file->move($destinationPath, $photo);

            $all_inputs['machine_photo'] = $photo;
        } else {
            $all_inputs['machine_photo'] = $userShopDetail->machine_photo;
        }
        if ($request->file('shop_gallery')) {
            $k = 1;
            foreach ($request->file('shop_gallery') as $file) {
                $destinationPath = public_path('Admin/images/shop'); // upload path
                $profileImage = time() . $k . '.' . $file->getClientOriginalExtension();
                $file->move($destinationPath, $profileImage);
                $dataImage[] = $profileImage;
                $k++;
            }
            $all_inputs['shop_gallery'] = json_encode($dataImage, JSON_FORCE_OBJECT);
        } else {
            $all_inputs['shop_gallery'] = $userShopDetail->shop_gallery;
        }

        if (!empty($userShopDetail)) {

            $ShopDetail = $userShopDetail->update($all_inputs);
        } else {
            $ShopDetail = ShopDetail::create($all_inputs);
        }
        if ($ShopDetail) {
            return redirect()->back()->with('message', 'Shop Detail updated successfully');
        } else {
            return redirect()->back()->with('message', 'Something went wrong');
        }
    }

    public function showDirectTree()
    {
        $child = User::with('getChildMember')->find(auth()->user()->id);
        // $child = $user->getChildMember();
        //dd($child);
        return view('user.account.direct-tree', compact('child'));
    }

    public function showMonthlyRecharge()
    {
        if (!Gate::allows('users_manage')) {
            return abort(401);
        }
        $data = DB::table('commission-setting')->where('user_id', auth()->user()->id)->first();
        if (!$data) {
            $data = DB::table('commission-setting')->where('user_id', NULL)->first();
        }
        return view('user.account.monthly-recharge.recharge', compact('data'));
    }

    public function daysInterval($from, $to)
    {
        $datetime1 = new DateTime($from);
        $datetime2 = new DateTime($to);
        $interval = $datetime1->diff($datetime2);
        $days = $interval->format('%R%a'); //now do whatever you like with $days
        return $days;
    }

    public function monthlyRechargeSummary(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'recharge_duration' => 'required',
            'recharge_amount' => 'required'
        ]);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator);
        } else {


            $date = date("Y-m-d H:i:s");
            $expire_date = auth()->user()->expire_date;
            $days = $this->daysInterval($date, $expire_date);
            //$datetime1 = new DateTime($date);
            //$datetime2 = new DateTime($expire_date);
            //$interval = $datetime1->diff($datetime2);
            //$days = $interval->format('%a');//now do whatever you like with $days

            if ($days > 0 && ($expire_date != NULL && $expire_date != '0000-00-00 00:00:00')) {
                $expire_date = $expire_date;
            } else {
                $expire_date = $date;
            }
            $creditConversion = DB::table('credit-point-setting')->first();
            $creditConversionAmount = $creditConversion->amount;
            return view('user.account.monthly-recharge.recharge-summary', compact('request', 'expire_date', 'creditConversionAmount'));
        }
    }

    public function monthlyRechargePayment(Request $request)
    {


        $validator = Validator::make($request->all(), [
            'payment_method' => 'required'
        ]);
        if ($validator->fails()) {

            return Redirect::back()->withErrors($validator);
        } else {
            if ($request->payment_method == 'credit_wallet') {
                try {
                    $exception = DB::transaction(function () use ($request) {

                        $invoice_no = monthlyRechargeInvoiceNumber();
                        $credit = new CreditWallet();
                        $credit = $credit->retailerCreditBalance();
                        if ($credit->amount >= $request->recharge_amount) {

                            $balance_amount = $credit->amount - $request->recharge_amount;


                            $data = new CreditWalletTransaction;
                            $data->invoice_no = $invoice_no;
                            $data->receiver_id = auth()->user()->id;
                            $data->sender_id = auth()->user()->id;
                            $data->credit_amt = 0;
                            $data->debit_amt = $request->recharge_amount;
                            $data->balance_amt = $balance_amount;
                            $data->singal_credit_pur_amt = ($request->recharge_amount / $request->recharge_duration);
                            $data->total_credit_pur_amt = $request->recharge_amount;
                            $data->type = 'Account Recharge Payment';
                            $data->status = 1;
                            $data->payment_mode = 'Credit Wallet';
                            $data->transaction_description = 'You pay ' . $request->recharge_amount . 'credit points.';
                            if ($data->save()) {
                                CreditWallet::where('user_id', auth()->user()->id)->update(['amount' => $balance_amount]);

                                $accRecharge = new AccountRecharge;
                                $accRecharge->user_id = auth()->user()->id;
                                $accRecharge->recharge_duration = $request->recharge_duration;
                                $accRecharge->recharge_amount = $request->recharge_amount;
                                $accRecharge->expire_date = $request->expire_date;
                                $accRecharge->payment_mode = '1';
                                $accRecharge->invoice_no = $invoice_no;
                                $accRecharge->status = 1;
                                if ($accRecharge->save()) {

                                    foreach ($request->all() as $key => $value) {
                                        $request->session()->forget($key);

                                        session()->put($key, $value);
                                    }

                                    $this->rechargeCommission($request->session(), $invoice_no);

                                    User::where('id', auth()->user()->id)->update(['status' => 1, 'expire_date' => $request->expire_date]);
                                }
                            }
                        } else {


                            return redirect()->route('user.monthly-recharge')->withMessage('You have not sufficient balance in credit wallet.');
                        }

                        return redirect()->route('user.monthly-recharge')->withMessage('Your monthly recharge has been done successfully');
                    });

                    return is_null($exception) ? true : $exception;
                } catch (Exception $e) {
                    return false;
                }
            } else if ($request->payment_method == 'cash_free') {

                //DB::transaction(function () use($request) {

                $invoice_no = monthlyRechargeInvoiceNumber();

                session()->put('monthly_invoice', $invoice_no);

                foreach ($request->all() as $key => $value) {
                    $request->session()->forget($key);

                    session()->put($key, $value);
                }

                return view('user.account.monthly-recharge.monthly-recharege-cashfree', compact('request'));

                //});
                //return redirect()->route('user.monthly-recharge')->withMessage('Your monthly recharge has been done successfully');
            }
        }
    }

    public function monthlyRechargeSuccess(Request $request)
    {
        //dd($request);

        $secretkey = env('CASHFREE_SECRET_KEY');
        $orderId = $request->orderId;
        $orderAmount = $request->orderAmount;
        $referenceId = $request->referenceId;
        $txStatus = $request->txStatus;
        $paymentMode = $request->paymentMode;
        $txMsg = $request->txMsg;
        $txTime = $request->txTime;
        $signature = $request->signature;
        $data = $orderId . $orderAmount . $referenceId . $txStatus . $paymentMode . $txMsg . $txTime;
        $hash_hmac = hash_hmac('sha256', $data, $secretkey, true);
        $computedSignature = base64_encode($hash_hmac);
        $invoice_no = $request->session()->get('monthly_invoice');
        $data = $request->session();
        //dd($request->session());
        try {
            $exception = DB::transaction(function () use ($data, $invoice_no, $txStatus, $signature, $computedSignature, $paymentMode, $request) {
                if ($txStatus == 'SUCCESS' && ($signature == $computedSignature)) {
                    //dd($request->session()->get('expire_date'));
                    $invoice_no = $invoice_no;
                    $accRecharge = new AccountRecharge;
                    $accRecharge->user_id = auth()->user()->id;
                    $accRecharge->recharge_duration = $data->get('recharge_duration');
                    $accRecharge->recharge_amount = $data->get('recharge_amount_after_conversion');
                    $accRecharge->expire_date = $data->get('expire_date');
                    $accRecharge->payment_mode = '2';
                    $accRecharge->invoice_no = $invoice_no;
                    $accRecharge->status = 1;
                    if ($accRecharge->save()) {
                        $this->rechargeCommission($data, $invoice_no);

                        User::where('id', auth()->user()->id)->update(['status' => 1, 'expire_date' => $data->get('expire_date')]);
                    }


                    $request->session()->forget('monthly_invoice');
                    return view('user.account.monthly-recharge.monthly-recharge-response');
                } else {
                    return view('user.account.monthly-recharge.monthly-recharge-cancel');
                }
            });
            return is_null($exception) ? true : $exception;
        } catch (Exception $e) {
            return false;
        }
    }

    public function rechargeCommission($data, $invoice_no)
    {
        $sponsor_id = auth()->user()->referred_by;
        $sponsor_data = User::where('referral_id', $sponsor_id)->first();

        $comData = DB::table('commission-setting')->where([['user_id', $sponsor_id], ['status', 1]])->first();
        if (!$comData) {
            $comData = DB::table('commission-setting')->where('user_id', NULL)->first();
        }
        if ($comData->status == 1) {
            $date = date("Y-m-d H:i:s");
            $sponsor_joining_date = $sponsor_data->created_at;
            $commission_duration = $comData->recharge_month;
            $commission_date = date("Y-m-d H:i:s", strtotime("+" . $commission_duration . " years", strtotime($sponsor_joining_date)));
            $comDays = $this->daysInterval($date, $commission_date);
            $invoice_credit = $data->get('recharge_amount');
            $commission_credit = ($data->get('recharge_amount') * $comData->commission) / 100; //percentage
            $creditConversion = DB::table('credit-point-setting')->first();
            $creditConversionAmount = $creditConversion->amount;
            $invoice_amt = $invoice_credit * $creditConversionAmount;
            $commission_amt = $commission_credit * $creditConversionAmount;

            if ($comDays > 0) {

                if ($sponsor_data->status == 1) {
                    $cash = new CashWallet();
                    $balance = $cash->retailerCashBalance();
                    if ($balance->amount > 0) {
                        $balance_amount = $balance->amount + $commission_amt;
                    } else {
                        $balance_amount = $commission_amt;
                    }

                    $data = new CashWalletTransaction;
                    $data->invoice_no = $invoice_no;
                    $data->receiver_id = $sponsor_data->id;
                    $data->sender_id = auth()->user()->id;
                    $data->credit_amt = $commission_amt;
                    $data->debit_amt = 0;
                    $data->balance_amt = $balance_amount;
                    $data->type = 'Recharge Commission';
                    $data->payment_mode = 'Credit Wallet';
                    $data->transaction_description = 'You get recharge commission ' . env('CURRENCY_SYMBOL') . ' ' . $commission_amt;
                    if ($data->save()) {
                        CashWallet::where('user_id', auth()->user()->id)->update(['amount' => $balance_amount]);

                        $commissionData = new RechargeCommission;
                        $commissionData->user_id = $sponsor_data->id;
                        $commissionData->sender_id = auth()->user()->id;
                        $commissionData->invoice_no = $invoice_no;
                        $commissionData->commission_per = $comData->commission;
                        $commissionData->invoice_amt = $invoice_amt;
                        $commissionData->commission_amt = $commission_amt;
                        $commissionData->invoice_credit = $invoice_credit;
                        $commissionData->commission_credit = $commission_credit;
                        $commissionData->type = 'Recharge Commission';
                        $commissionData->description = 'You get recharge commission ' . env('CURRENCY_SYMBOL') . ' ' . $commission_amt;
                        $commissionData->status = 1;
                        $commissionData->save();
                    }
                } else {

                    $commissionData = new RechargeCommission;
                    $commissionData->user_id = $sponsor_data->id;
                    $commissionData->sender_id = auth()->user()->id;
                    $commissionData->invoice_no = $invoice_no;
                    $commissionData->commission_per = $comData->commission;
                    $commissionData->invoice_amt = $invoice_amt;
                    $commissionData->commission_amt = $commission_amt;
                    $commissionData->invoice_credit = $invoice_credit;
                    $commissionData->commission_credit = $commission_credit;
                    $commissionData->type = 'Recharge Commission';
                    $commissionData->description = 'You get recharge commission ' . env('CURRENCY_SYMBOL') . ' ' . $commission_amt;
                    $commissionData->status = 0;
                    $commissionData->save();

                    $data = new Log;
                    $data->user_id = $sponsor_data->id;
                    $data->type = 'commission';
                    $data->description = "account status is not active";
                    $data->save();
                }
            } else {
                $data = new Log;
                $data->user_id = $sponsor_data->id;
                $data->type = 'commission';
                $data->description = "Commission year expire now, thats why this user not get commission";
                $data->save();
            }
        } else {
            $data = new Log;
            $data->user_id = $sponsor_data->id;
            $data->type = 'commission';
            $data->description = "Commission status is not active";
            $data->save();
        }
    }

    public function showCustomerList()
    {
        $customerLists = Customer::where('retailer_id', auth()->user()->id)->orderBy('id', 'desc')->get();
        // $child = $user->getChildMember();
        //dd($child);
        return view('user.account.customer-list', compact('customerLists'));
    }

    public function showMonthlyRechargeReport()
    {

        $datas = AccountRecharge::where('user_id', auth()->user()->id)->orderBy('id', 'desc')->get();

        // $child = $user->getChildMember();
        //dd($child);
        return view('user.account.monthly-recharge-report', compact('datas'));
    }

    public function showCreditPurchaseReport()
    {
        $datas = CreditWalletTransaction::where([['receiver_id', auth()->user()->id], ['type', 'Credit Purchase']])->orderBy('id', 'desc')->get();
        // $child = $user->getChildMember();
        //dd($child);
        return view('user.account.credit-purchase-report', compact('datas'));
    }

    public function showRechargeCommissionReport()
    {
        $datas = RechargeCommission::with('incomeReceiver', 'creditPurchaser')->where('user_id', auth()->user()->id)->orderBy('id', 'desc')->get();
        // $child = $user->getChildMember();
        //dd($child);
        return view('user.account.recharge-commission-report', compact('datas'));
    }
}
