<?php

namespace App\Http\Controllers\User;

use App\Models\CreditWallet;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\RechargeCommission;
use App\User;
use DB;
use Auth;
use Response;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('home');
    }
    
    public function retailerDashbord(){

        $totalRechargeAmount = DB::table('account_recharges')->where('account_recharges.user_id', Auth::user()->id)->sum('account_recharges.recharge_amount');
        $commission = RechargeCommission::where('user_id', Auth::user()->id)->sum('commission_amt');
        $userStatus = User::where('id', Auth::user()->id)->select('status')->first();
        $child = User::with('getChildMember')->find(auth()->user()->id);

        $result= DB::table('recharge_commissions')->select( DB::raw('sum(commission_amt) as y'), DB::raw("DATE_FORMAT(created_at,'%M %Y') as months"))->where('sender_id', Auth::user()->id)
        ->groupBy('months')
        ->get();

        $kyc_user = DB::select('SELECT count(id) as kyc_count FROM user_kyc_details where STATUS = 0');
        dd($kyc_user);
        die();
       // return $result;
       // return view('user.home', compact('totalRechargeAmount', 'commission','userStatus','child'));

    }
    public function getchart(Request $request)
    {
       
        $result= DB::table('recharge_commissions')->select( DB::raw('sum(commission_amt) as y'), DB::raw("DATE_FORMAT(created_at,'%M %Y') as months"))->where('user_id', Auth::user()->id)
        ->groupBy('months')
        ->get();
    

       
        return Response::json(array('chart'=>$result));  
       // return Response::json($chart);  
    }

}
