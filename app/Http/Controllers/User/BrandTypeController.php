<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\BrandType;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Str;
Use Alert;

class BrandTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
      
        if (! Gate::allows('brandstype_manage')) {
            return abort(401);
        }
        $brandTypes = BrandType::orderBy('id','DESC')->get();
       // dd($brandTypes);
       return view('user.brand-type.index', compact('brandTypes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('brandstype_manage')) {
            return abort(401);
        }
        else{
            return view('admin.brand-type.create');
        }
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      
        if (! Gate::allows('brandstype_manage')) {
            return abort(401);
        }
        else{
            request()->validate([
                'name' => 'required|min:2',
                
                ], [
                    'name.required' => 'Name is required',
                
                ]);
                
                $brandType = new BrandType();
        
                if(!empty($request->brandTypeImg))
                    {
                        $brandTypeImage = time().'.'.$request->brandTypeImg->getClientOriginalExtension();
                        $request->brandTypeImg->move(public_path('images/brand-types'),$brandTypeImage);
                    }
                    else{
                      
                        $brandTypeImage = '';
            
                     }
                     
                     $brandType->name = $request->name;
                     $brandType->slug = Str::slug($request->name, '-');                     
                     $brandType->meta_title = $request->metaTitle;
                     $brandType->meta_description = $request->metaDescription; 
                     $brandType->meta_keywords = $request->metaKeyword;
                     $brandType->image = $brandTypeImage;
                     $brandType->status = '1';
                     $brandType->save();
                     Alert::success('Brand Type', 'Added successfully');
                     return redirect()->route('admin.brand-type.index')->with('message', 'Brand Type created successfully.');
        }
      // return $request->all();
      
     

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
     
        if (! Gate::allows('brandstype_manage')) {
            return abort(401);
        }
        else{
            $brandType = BrandType::findOrFail($id);
            return view('admin.brand-type.edit', compact('brandType'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (! Gate::allows('brandstype_manage')) {
            return abort(401);
        }
        else{
            $brandType = BrandType::findOrFail($id);
            if($request->brandTypeImg != null){
             $imageUpdate = time().'.'.$request->brandTypeImg->getClientOriginalExtension();
             $request->brandTypeImg->move(public_path('images/brand-types'), $imageUpdate);
             $brandType->image= $imageUpdate;
            }
            else{
                $imageUpdate = $request->imageH;
                $brandType->image= $imageUpdate;
                            
            }
            $brandType->name = $request->name;
            $brandType->slug = Str::slug($request->name, '-');
            $brandType->meta_title = $request->metaTitle;
            $brandType->meta_description = $request->metaDescription; 
            $brandType->meta_keywords = $request->metaKeyword;
            $brandType->status = '1';
            $brandType->save();
            Alert::success('Brand Type', 'updated successfully');
            return redirect()->route('admin.brand-type.index')->with('message', 'Brand Type updated successfully.');

            }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return "Hi";
    }
    public function deleteBrandType(Request $request, $id){
       
        $data = BrandType::find($id);
        if($data->delete()){
           Alert::success('Brand Type', 'Deleted successfully');
           return back()->with('message', 'Brand Type Deleted successfully.');
         }
          else{
               $error = "Something wrong! Please try later ";
              //redirect('admin/news-and-events')->with('message', $message);
            
              return back()->with('message', 'Something wrong! Please try later.');

         }
    }
    public function changeStatus(Request $request)
	{
       
        $brandType = BrandType::find($request->brnadType_id);
        $brandType->status = $request->status;
        $brandType->save();
       
       
        return response()->json(['success'=>'Status change successfully.']);
	}

}
