<?php

namespace App\Http\Controllers\User;
use DB;
use App\Models\BrandType;
use App\Models\Brand;
use App\Models\BrandModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
Use Illuminate\Support\Str;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\File;
class BrandModelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		
         if (! Gate::allows('brand_models')) {
            return abort(401);
        }

        $brandModel = BrandModel::with('brandsTypes','brands')->get();

        return view('user.models.index', compact('brandModel'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       if (! Gate::allows('brand_models')) {
            return abort(401);
        }
        $brand_types = BrandType::select('id', 'name')->get();
		$brands = Brand::select('id', 'name')->get();
		
        return view('admin.models.create', compact('brand_types','brands'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         if (! Gate::allows('brand_models')) {
            return abort(401);
        }
		
		//dd($request->all());
		$validator = Validator::make($request->all(), [
		'brand_type_id' => 'required',
		'brand_id' => 'required',
		'name' => 'required',
		'status' => 'required',
		'fileUpload' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
		]);
		if($validator->fails()) {
			//dd("bbb");
			 return Redirect::back()->withErrors($validator);
		}
		else {
			
			
		if ($files = $request->file('fileUpload')) {
           $destinationPath = public_path('images/models'); // upload path
           $profileImage = date('Y-m-d-His') . "." . $files->getClientOriginalExtension();
           $files->move($destinationPath, $profileImage);
           
        }
		
        $model = new BrandModel;
		$model->brand_type_id = $request->brand_type_id;
		$model->brand_id = $request->brand_id;
        $model->name = $request->name;
		$model->slug = Str::slug($request->name);
		$model->meta_title = $request->meta_title;
		$model->meta_keywords = $request->meta_keywords;
		$model->meta_description = $request->meta_description;
		if ($files = $request->file('fileUpload')) {
		$model->image = $profileImage;
		}
		$model->status = $request->status;
		$model->save();

        return redirect()->route('admin.models.index')->withMessage('Created successfully!');;
		}
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BrandModel  $brandModel
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! Gate::allows('brand_models')) {
            return abort(401);
        }

        $brandModel = BrandModel::select('brand_models.*',DB::raw('(CASE  
						WHEN brand_models.status = "0" THEN "Inactive" 
                        WHEN brand_models.status = "1" THEN "Active" 
                        ELSE "" 
                        END) AS status'))->with('brandsTypes','brands')->where('id',$id)->first();

        return view('admin.models.show', compact('brandModel'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BrandModel  $brandModel
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		
        if (! Gate::allows('brand_models')) {
            return abort(401);
        }
        $brand_types = BrandType::select('id', 'name')->get();
		$brands = Brand::select('id', 'name')->get();
		$brandModel = BrandModel::FindOrFail($id);

        return view('admin.models.edit', compact('brandModel','brand_types','brands'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BrandModel  $brandModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         if (! Gate::allows('brand_models')) {
            return abort(401);
        }
		
		//dd($request->all());
		$validator = Validator::make($request->all(), [
		'brand_type_id' => 'required',
		'brand_id' => 'required',
		'name' => 'required',
		'status' => 'required',
		'fileUpload' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
		]);
		if($validator->fails()) {
			//dd("bbb");
			 return Redirect::back()->withErrors($validator);
		}
		else {
			
		$profileImage = '';	
		if ($files = $request->file('fileUpload')) {
           $destinationPath = public_path('images/models'); // upload path
           $profileImage = date('Y-m-d-His') . "." . $files->getClientOriginalExtension();
           $files->move($destinationPath, $profileImage);
           
        }
		
        $model = BrandModel::find($id);
		$model->brand_type_id = $request->brand_type_id;
		$model->brand_id = $request->brand_id;
        $model->name = $request->name;
		$model->slug = Str::slug($request->name);
		$model->meta_title = $request->meta_title;
		$model->meta_keywords = $request->meta_keywords;
		$model->meta_description = $request->meta_description;
		$model->image = $profileImage;
		$model->status = $request->status;
		$model->save();

        return redirect()->route('admin.models.index')->withMessage('Updated successfully!');;
		}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BrandModel  $brandModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(BrandModel $brandModel)
    {
		
        if (! Gate::allows('brand_models')) {
            return abort(401);
        }

        $brandModel->delete();

        return redirect()->route('admin.models.index');
    }
	 public function deleteModel($id)
    {
		
        if (! Gate::allows('brand_models')) {
            return abort(401);
        }

        $imageQry = BrandModel::where('id', $id)->first();
		
		if($imageQry->image) {
		$image_path = 'images/models/'.$imageQry->image; // upload pa	
		//dd($image_path);
		if(File::exists($image_path)) {
		File::delete($image_path);
		$imageQry->delete();
		}
		}

        return redirect()->route('admin.models.index')->withMessage('Deleted successfully!');;
    }
	
	
	
	
	public function massDestroy(Request $request)
    {
		
        if (! Gate::allows('brand_models')) {
            return abort(401);
        }
        BrandModel::whereIn('id', request('ids'))->delete();

        return response()->noContent();
    }
	
	public function getBrandTypes(Request $request)
	{
		
		$brand_type = Brand::select('id', 'name')->where('brand_type_id','=',$request->brand_type)->get();
		//dd($brand_type);
		return $brand_type;
	}
	
	public function changeStatus(Request $request)
	{
		$model = BrandModel::find($request->model_id);

        $model->status = $request->status;

        $model->save();
		
  

        return response()->json(['success'=>'Status change successfully.']);
	}
}
