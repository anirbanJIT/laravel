<?php

namespace App\Http\Controllers\User;
use DB;
use App\Models\BrandType;
use App\Models\Brand;
use App\Models\BrandModel;
use App\Models\BrandModelScreenImage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
Use Illuminate\Support\Str;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\File;

class BrandModelScreenImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         if (! Gate::allows('brand_models_screen_image')) {
            return abort(401);
        }

        $brandModelScreen = BrandModelScreenImage::with('brandsTypes','brands','brandsModel')->get();

        return view('user.models-screen-image.index', compact('brandModelScreen'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         if (! Gate::allows('brand_models_screen_image')) {
            return abort(401);
        }

        $brandModel = BrandModelScreenImage::select('brand_model_screen_images.*',DB::raw('(CASE  
						WHEN brand_model_screen_images.status = "0" THEN "Inactive" 
                        WHEN brand_model_screen_images.status = "1" THEN "Active" 
                        ELSE "" 
                        END) AS status'))->with('brandsTypes','brands','brandsModel')->where('id',$id)->first();

        return view('user.models-screen-image.show', compact('brandModel'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
    }
	
	
	
	
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       
    }
	
	
}
