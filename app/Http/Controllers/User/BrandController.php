<?php

namespace App\Http\Controllers\User;

use Spatie\Permission\Models\Permission;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Brand;
use App\Models\BrandType;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Str;
Use Alert;

class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('brands_manage')) {
            return abort(401);
        }
     
      $brands = Brand::join('brand_types', 'brand_types.id', '=', 'brands.brand_type_id')
                        ->select('brands.*', 'brand_types.name as BrandCatName') 
                        ->get();
      //dd($brands);
        return view('user.brands.index', compact('brands'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('brands_manage')) {
            return abort(401);
        }
        $brandTypes = BrandType::orderBy('id', 'DESC')->get();
        return view('admin.brands.create', compact('brandTypes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       if(!Gate::allows('brands_manage')){
           return abort(401);
       }
       else{
        request()->validate([
              'name' => 'required|min:3',
              'brandType' => 'required',
        ],
          [
              'name.required' => 'Name is required',
              'brandType.required' => 'Brand Type is required',

          ]); 
          
          $brands = new Brand();
        
          if(!empty($request->brandImg))
              {
                  $brandImage = time().'.'.$request->brandImg->getClientOriginalExtension();
                  $request->brandImg->move(public_path('images/brands'),$brandImage);
              }
              else{
                
                  $brandImage = '';
      
               }
               
               $brands->name = $request->name;
               $brands->brand_type_id = $request->brandType;
               $brands->slug = Str::slug($request->name, '-');                     
               $brands->meta_title = $request->metaTitle;
               $brands->meta_description = $request->metaDescription; 
               $brands->meta_keywords = $request->metaKeyword;
               $brands->image = $brandImage;
               $brands->status = '1';
               $brands->save();
               Alert::success('Brand Type', 'Added successfully');
               return redirect()->route('admin.brands.index')->with('message', 'Brand Type created successfully.');


       }
      

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('brands_manage')) {
            return abort(401);
        }
        else{
            $brandTypes = BrandType::orderBy('id', 'DESC')->get();
            $brand = Brand::findOrFail($id);
            return view('admin.brands.edit', compact('brand', 'brandTypes'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(!Gate::allows('brands_manage')){
            return abort(401);
        }
        else{
         request()->validate([
               'name' => 'required|min:3',
               'brandType' => 'required',
         ],
           [
               'name.required' => 'Name is required',
               'brandType.required' => 'Brand Type is required',
 
           ]); 
          
            $brand = Brand::findOrFail($id);
            if($request->brandImg != Null){
                $brandImg = time().'.'.$request->brandImg->getClientOriginalExtension();
                $request->brandImg->move(public_path('images/brands'), $brandImg);
                $brand->image = $brandImg;

            }
            else{
                $brand->image = $request->imageH;
            }
           
            $brand->name = $request->name;
            $brand->brand_type_id = $request->brandType;
            $brand->slug = Str::slug($request->name,'-');
            $brand->meta_title = $request->metaTitle;
            $brand->meta_description = $request->metaDescription;
            $brand->meta_keywords = $request->metaKeyword;
            $brand->status = '1';
            $brand->save();
            Alert::success('Brand', 'Updated successfully');
            return redirect()->route('admin.brands.index')->with('message', 'Brand Type created successfully.');


        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function deleteBrand(Request $request, $id){
       
        $data = Brand::find($id);
        if($data->delete()){
          
            Alert::success('Brand', 'Deleted successfully'); 
           return back()->with('message', 'Brand Deleted successfully.');
         }
          else{
               $error = "Something wrong! Please try later ";
              //redirect('admin/news-and-events')->with('message', $message);
             
              return back()->with('message', 'Something wrong! Please try later.');

         }
    }
    
    public function changeStatus(Request $request)
	{
       
        $brand = Brand::find($request->brand_id);
        $brand->status = $request->status;
        $brand->save();
   /*     return response()->json(['success'=>'Status InActive successfully.']);
        if($request->status == 1){
            $brand->status = $request->status;
            $brand->save();
            return response()->json(['success'=>'Status InActive successfully.']);
          }
          if($request->status == 0){
            $brand->status = $request->status;
            $brand->save();
            return response()->json(['success'=>'Status Active successfully.']);
          } */
       
	}

}
