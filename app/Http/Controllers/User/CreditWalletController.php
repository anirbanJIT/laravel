<?php

namespace App\Http\Controllers\User;
use App\User;
use App\Models\CreditWallet;
use App\Models\CashWallet;
use App\Models\CreditWalletTransaction;
use App\Models\CashWalletTransaction;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Redirect;
Use App\helper;
class CreditWalletController extends Controller
{
   protected $creditWallet;
   
   public function __construct(Request $request)
  {
	  
     $this->creditWallet = new CreditWallet();
  }
   
   
public function showPurchaseCredits()
{	
if (!Gate::allows('manage_credit_wallet')) {
            return abort(401);
        }
		$credit = $this->creditWallet->retailerCreditBalance();
		
        return view('user.credit-wallet.credit_purchase', compact('credit'));
}

public function purchaseCreditsSummary(Request $request)
{
	
	$validator = Validator::make($request->all(), [
	'credit' => 'required|integer'
	]);
	if($validator->fails()) 
	{
		return Redirect::back()->withErrors($validator);
	}
	else
	{
		
		$credit = $this->creditWallet->retailerCreditBalance();
		$credit_setting = $this->creditWallet->creditSetting();
		return view('user.credit-wallet.credit_purchase_summary', compact('request','credit_setting','credit'));
	}
}
public function  purchaseCredits(Request $request)
{
	$invoice_no = creditInvoiceNumber();
	$balance = $this->creditWallet->retailerCreditBalance();
	if($balance->amount>0)
	{
		$balance_amount = $balance->amount+$request->credit_amt;
	}
	else
	{
		$balance_amount = $request->credit_amt;
	}
	
	
	
	$data = new CreditWalletTransaction;
	$data->invoice_no = $invoice_no;
	$data->receiver_id = auth()->user()->id;
	$data->sender_id = auth()->user()->id;
	$data->credit_amt = $request->credit_amt;
	$data->debit_amt = 0;
	$data->balance_amt = ($balance->amount+$request->credit_amt);
	$data->singal_credit_pur_amt = $request->singal_credit_pur_amt;
	$data->total_credit_pur_amt = $request->total_credit_pur_amt;
	$data->type = 'Credit Purchase';
	$data->payment_mode = 'Cash Free';
	$data->transaction_description = 'You purchased '.$request->credit_amt.' credit points and pay for this '.env('CURRENCY_SYMBOL').' '.$request->total_credit_pur_amt;
	if($data->save())
	{
		
		session()->put('credit_invoice', $invoice_no); 
		return view('user.credit-wallet.credit-purchase-cashfree',compact('request'));
	}
	else
	{
		return back()->withMessage('Your Oredr is not completed, Please try again.');
	}
	
}

public function creditPurchaseSuccess(Request $request)  
{
	//dd($request);
	
	$secretkey = env('CASHFREE_SECRET_KEY');
		 $orderId = $request->orderId;
		 $orderAmount = $request->orderAmount;
		 $referenceId = $request->referenceId;
		 $txStatus = $request->txStatus;
		 $paymentMode = $request->paymentMode;
		 $txMsg = $request->txMsg;
		 $txTime = $request->txTime;
		 $signature = $request->signature;
		 $data = $orderId.$orderAmount.$referenceId.$txStatus.$paymentMode.$txMsg.$txTime;
		 $hash_hmac = hash_hmac('sha256', $data, $secretkey, true) ;
		 $computedSignature = base64_encode($hash_hmac);
		 
	
	if($request->txStatus=='SUCCESS' && ($signature == $computedSignature)){
	$credit = CreditWalletTransaction::where('invoice_no', $request->orderId)->select('balance_amt')->first();
			// change status of order after payment confirmation
			CreditWalletTransaction::where('invoice_no', $request->orderId)
            ->update(['status' => 1, 'transaction_id' => $request->referenceId]);
	CreditWallet::where('user_id',auth()->user()->id)->update(['amount' => $credit->balance_amt]);
	$request->session()->forget('credit_invoice');
	return view('user.credit-wallet.credit-order-response',compact('credit'));
	}
	else{
		return view('user.credit-wallet.credit-order-cancel');
	}
}
public function creditPurchaseCancel()  
{

	return view('user.credit-wallet.credit-order-cancel');
}

public function creditTransactionReport()
{
	$credit = $this->creditWallet->retailerCreditBalance();
	$creditTransaction = CreditWalletTransaction::where('receiver_id',auth()->user()->id)->orderBy('id','desc')->get();
	return view('user.credit-wallet.credit-transaction',compact('creditTransaction','credit'));
}

	
	
}
