<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
    
class Controller extends BaseController {

    use AuthorizesRequests,
        DispatchesJobs,
        ValidatesRequests;

    public function checkuser($ref_id) {
        $user = User::where('referral_id', $ref_id)->select('referral_id')->first();
        if ($user) {
            return true;
        } else {
            return false;
        }
    }

    public function check_sms_credit() {
        // Account details
        $apiKey = urlencode(env('SMS_API_KEY'));
        // Prepare data for POST request
        $data = 'apikey=' . $apiKey;
        // Send the GET request with cURL
        $ch = curl_init('https://api.textlocal.in/balance/?' . $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $credits = curl_exec($ch);
        curl_close($ch);
        return $credits;
    }

    //change
    public function send_sms($number, $message) {

//        $api_key = '56087BB805B361  ';
        $api_key = '4617005EE5A04B';
        $contacts = $number;
//        $from = 'WRLDTX';
        $from = 'SELECT';
        $sms_text = urlencode($message);

        //Submit to server

        
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL, "http://msg.pwasms.com/app/smsapi/index.php");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "key=".$api_key."&campaign=0&routeid=69&type=text&contacts=".$contacts."&senderid=".$from."&msg=".$sms_text);
        $response = curl_exec($ch);
        curl_close($ch);

//        $apiKey = urlencode('6cDj7mxTAHo-Kp0sbDypNmYY5N7gB0wZzPZ3odVg2g');
//        $numbers = urlencode($number);
//        $sender = urlencode('TXTLCL');
//        $message = rawurlencode($message);
        // Prepare data for POST request
//        $data = "apikey=" . $apiKey . "&message=" . $message . "&sender=" . $sender . "&numbers=" . $numbers;
//        $ch = curl_init('http://api.textlocal.in/send/?');
//        $data = 'key=45E8C845640108&campaign=0&routeid=9&type=text&contacts='.$numbers.'&senderid=PWASMS&msg='.$message;
//        $ch = curl_init('http://msg.pwasms.com/app/smsapi/index.php?'); 
//        curl_setopt($ch, CURLOPT_POST, true);
//        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//        $response = curl_exec($ch);
//        curl_close($ch);
//
        return $response;
    }

    function generateReferralNumber() {
        $number = mt_rand(1000000, 9999999); // better than rand()
        // call the same function if the barcode exists already
        if ($this->referralNumberExists($number)) {
            return $this->generateReferralNumber();
        }

        // otherwise, it's valid and can be used
        return $number;
    }

    function referralNumberExists($number) {
        // query the database and return a boolean
        // for instance, it might look like this in Laravel
        return User::where('referral_id', $number)->select('referral_id')->first();
    }

    public function getCreditPurchaseToken($amount, $invoice_no) {

        $secret_key = env('CASHFREE_SECRET_KEY');
        $app_id = env('CASHFREE_APP_ID');
        $orderId = $invoice_no;
        $orderAmount = $amount;
        $orderCurrency = env('CURRENCY_SYMBOL');
        // You shouldn't need to change anything here.	
        //$data = "orderId=".$orderId."&orderAmount=".$orderAmount."&orderCurrency=".$orderCurrency;


        $data = '{
	  "orderId": "' . $orderId . '",
	  "orderAmount":"' . $orderAmount . '",
	  "orderCurrency":"' . $orderCurrency . '"
		}';

//return $data;	
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, env('CASHFREE_TOKEN_URL'));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);  //Post Fields
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'x-client-id: ' . $app_id,
            'x-client-secret: ' . $secret_key
        ));
        $response = curl_exec($ch);
        // This is the number of credits you have left	
        curl_close($ch);
        return $response;
    }

    public function verificationBankDetailsToken(array $data) {
        /* $data = '{
          "name": "'.$name.'",
          "phone":"'.$orderAmount.'",
          "bankAccount":"'.$orderCurrency.'"
          "ifsc":"'.$orderCurrency.'"
          }'; */
        $secret_key = env('CASHFREE_SECRET_KEY');
        $app_id = env('CASHFREE_APP_ID');
        $orderCurrency = env('CURRENCY_SYMBOL');
        //return $data;	
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, env('CASHFREE_BULK_VALIDATION'));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);  //Post Fields
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'x-client-id: ' . $app_id,
            'x-client-secret: ' . $secret_key
        ));
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }

}
