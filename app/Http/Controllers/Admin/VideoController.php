<?php
namespace App\Http\Controllers\Admin;
use Spatie\Permission\Models\Permission;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Video;
use App\Models\VideoCategory;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Str;
Use Alert;

class VideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
		if (! Gate::allows('video_manage')) {
          return abort(401);
		}
		
		$videos = Video::orderBy('video_position_order','ASC')->get();
		return view('admin.videos.index', compact('videos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function videoOrderPosition(Request $request){
		$position_order = $request->position;
		$i=1;
		
		foreach($position_order as $k=>$v){
			Video::where("id",$v)->update(["position_order" => $i++]);	
		}
		
		return redirect()->back()->with('message', 'Item Position Update.');
	}


    public function create(Request $request)
    {
      if (! Gate::allows('video_manage')) {
          return abort(401);
      }
	  $id = $request->id;
      $videoCategories = VideoCategory::select('id', 'title')->where('status', 1)->get();
      return view('admin.videos.create',compact('videoCategories','id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

		if(!Gate::allows('video_manage')){
			return abort(401);
		} else {
			request()->validate([
			  'video_title' => 'required',
			  'video_link' => 'required',
			  'video_category_id' => 'required',
			],
			[
			  'video_title.required' => 'Title is required',
			  'video_link.required' => 'Link is required',
			  'video_category_id.required' => 'Category is required',
			]); 

			$videos = new Video();
			$videos->video_title = $request->video_title;
			$videos->video_link = $request->video_link;
			$videos->video_category_id = $request->video_category_id;
			if(!empty($request->video_image)) {
                $videoImage = time().'.'.$request->video_image->getClientOriginalExtension();
                $request->video_image->move(public_path('Admin/images/videos'),$videoImage);
            }else{
                $videoImage = '';
			}
			$videos->video_image = $videoImage;
			$videos->video_description = $request->video_description;
			$videos->save();

			Alert::success('Video', 'Added successfully');
			return redirect()->back()->with('message', 'Video created successfully.');
		}      

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! Gate::allows('video_manage')) {
          return abort(401);
		}
		if($id){
			$videos = Video::with('videoCategory')->where("video_category_id",$id)->orderBy('video_position_order','ASC')->get();
			return view('admin.videos.index', compact('videos','id'));
		}else{
			return redirect()->action('Admin\VideoCategoryController@index');
		}
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('video_manage')) {
            return abort(401);
        }
        else{

          $videos = Video::findOrFail($id);
          $videoCategories = VideoCategory::select('id', 'title')->where('status', 1)->get();
          return view('admin.videos.edit', compact('videos','videoCategories'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(!Gate::allows('video_manage')){
            
            return abort(401);

        } else{
         
          request()->validate([
            'video_title' => 'required',
            'video_link' => 'required',
            'video_category_id' => 'required',
          ],
          [
            'video_title.required' => 'Title is required',
            'video_link.required' => 'Link is required',
            'video_category_id.required' => 'Category is required',
          ]); 

          $videos = Video::findOrFail($id);
          $videos->video_title = $request->video_title;
          $videos->video_link = $request->video_link;
          $videos->video_category_id = $request->video_category_id;
          if($request->video_image != null){
             $imageUpdate = time().'.'.$request->video_image->getClientOriginalExtension();
             $request->video_image->move(public_path('Admin/images/videos'), $imageUpdate);
             $videos->video_image= $imageUpdate;
            }
            else{
                $imageUpdate = $request->imageH;
                $videos->video_image= $imageUpdate;
                            
            }  
          $videos->video_description = $request->video_description;
          $videos->save();

          Alert::success('Video', 'Updated successfully');
          return redirect()->back()->with('message', 'Video Updated successfully.');

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('video_manage')) {
            return abort(401);
        }
        
        $videos = Video::findOrFail($id);
        if($videos->delete()){          
            Alert::success('Video', 'Deleted successfully'); 
           return back()->with('message', 'Video Deleted successfully.');
         }
          else{
              $error = "Something wrong! Please try later ";
              return back()->with('message', 'Something wrong! Please try later.');

         }
    }
    
    
    public function deleteVideo(Request $request, $id){

        if (! Gate::allows('video_manage')) {
            return abort(401);
        }
        
        $videos = Video::findOrFail($id);
        if($videos->delete()){          
            Alert::success('Video', 'Deleted successfully'); 
           return back()->with('message', 'Video Deleted successfully.');
         }
          else{
              $error = "Something wrong! Please try later ";
              return back()->with('message', 'Something wrong! Please try later.');

         }
    }

}
