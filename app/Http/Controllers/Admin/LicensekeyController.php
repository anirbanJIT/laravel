<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Licensekey;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class LicensekeyController extends Controller
{
    public function index()
    {
        $items = Licensekey::get();
        return view('admin.licensekey.list')->with(['items' => $items]);
    }

    public function create()
    {
        return view('admin.licensekey.createkey');
    }

    public function store(Request $r)
    {
        $this->validate($r, [
            'quantity' => 'required|integer|min:1|max:100'

        ]);

        $qty = $r->quantity;

        for ($i = 0; $i <= $qty; $i++) {

            $key = rand(11111, 99999) .  Str::random(30) . rand(11111, 99999);

            Licensekey::create([
                'licensekey' => $key
            ]);
        }

        return redirect()->route('admin.licenseKeyList');
    }
}
