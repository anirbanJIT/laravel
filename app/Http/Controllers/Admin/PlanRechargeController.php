<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Plotter;
use App\Models\PlanRecharge;
use App\Models\BrandType;

use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Str;
use DB;
Use Alert;
use Symfony\Component\HttpFoundation\Response;

class PlanRechargeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
		
		$planrecharges_data = PlanRecharge::orderBy('id','DESC')->select(DB::raw('CONCAT(id,"_",status) AS id_status'),'id','name','category','amount','yearly_charges','plotter','speed','pressure','status','created_at')->get();
		$planrecharges = [];
		foreach($planrecharges_data as $row){
			
			$row->category = json_decode($row->category);
			$category = BrandType::whereIn("id",$row->category)->select("id","name")->get();
			
			$row->plotter = json_decode($row->plotter);
			$plotter = Plotter::whereIn("id",$row->plotter)->select("id","name")->get();
			
			array_push($planrecharges,
				array(
					"id" => $row->id,
					"id_status" => $row->id_status,
					"name" => $row->name,
					"category" => $category,
					"amount" => $row->amount,
					"yearly_charges" => $row->yearly_charges,
					"plotter" => $plotter,
					"speed" => json_decode($row->speed),
                    "pressure" => json_decode($row->pressure),
					"status" => $row->status,
					"created_at" => $row->created_at,
				)
			);
		}
       
       return view('admin.plan-recharge.index', compact('planrecharges'));
    }
	
	/**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$brandTypes = BrandType::get();
		$plotters = Plotter::get();
        return view('admin.plan-recharge.create',compact('brandTypes','plotters'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		/* dd($request); */
        $validated = $request->validate([
			'name' => 'required|min:2',
			'categories' => 'required',
			'amount' => 'required',
			'yearly_charges' => 'required',
			// 'plotter' => 'required',
			'plotter_arr_ids' => 'required',
			'plotter_speed_arr' => 'required',
			'status'=>'required'
		]); 
		
		$planrecharge = new PlanRecharge();
		$planrecharge->name = $request->name;
		$planrecharge->category	 = json_encode($request->categories);
		$planrecharge->amount = $request->amount;
		$planrecharge->yearly_charges = $request->yearly_charges;
		$planrecharge->plotter = json_encode($request->plotter_arr_ids);                     
		$planrecharge->speed = json_encode($request->plotter_speed_arr);
        $planrecharge->pressure = json_encode($request->plotter_pressure_arr);                     
		$planrecharge->status = $request->status;                     
		$planrecharge->save();
		
		Alert::success('PlanRecharge', 'Added successfully');
		return redirect()->route('admin.planrecharge.index')->with('message', 'Plan Recharge created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
		$planrecharge = PlanRecharge::findOrFail($id);
		$category_arr = json_decode($planrecharge->category);
		$plotter_arr = json_decode($planrecharge->plotter);
		
		$category = BrandType::whereIn("id",$category_arr)->select("id","name")->get();
		$plotter = Plotter::whereIn("id",$plotter_arr)->select("id","name")->get();
        
		return view('admin.plan-recharge.show', compact('planrecharge','category','plotter'));
    }
	
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
     
        $planrecharge = PlanRecharge::findOrFail($id);
		$category_arr = json_decode($planrecharge->category);
		$plotter_arr = json_decode($planrecharge->plotter);
		
		$category = BrandType::whereIn("id",$category_arr)->select("id","name")->get()->toArray();
		$category_ids = array_column($category,'id');
		$plotter_name_id = Plotter::whereIn("id",$plotter_arr)->select("id","name", "speed", "pressure")->get()->toArray();
		$plotter_ids = array_column($plotter_name_id,'id');
		
		$brandTypes = BrandType::get();
		$plotters = Plotter::get();
		
		return view('admin.plan-recharge.edit', compact('planrecharge','brandTypes','plotters','category_ids','plotter_ids','plotter_name_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		
        $validated = $request->validate([
			'name' => 'required|min:2',
			'categories' => 'required',
			'amount' => 'required',
			'yearly_charges' => 'required',
			// 'plotter' => 'required',
			'plotter_arr_ids' => 'required',
			'plotter_speed_arr' => 'required',
			'status'=>'required'
		]); 
			
		$planrecharge = PlanRecharge::findOrFail($id);
		$planrecharge->name = $request->name;
		$planrecharge->category	 = json_encode($request->categories);
		$planrecharge->amount = $request->amount;
		$planrecharge->yearly_charges = $request->yearly_charges;
		$planrecharge->plotter = json_encode($request->plotter_arr_ids);                     
		$planrecharge->speed = json_encode($request->plotter_speed_arr);
        $planrecharge->pressure = json_encode($request->plotter_pressure_arr);                     
		$planrecharge->status = $request->status;                     
		$planrecharge->save();
		
		
		Alert::success('PlanRecharge', 'updated successfully');
		return redirect()->route('admin.planrecharge.index')->with('message', 'PlanRecharge updated successfully.');
		
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
	 
	public function destroy($id) {
		
        $data = PlanRecharge::find($id);
				
        if($data->delete()){
			return redirect()->route('admin.planrecharge.index')->withMessage('Deleted successfully!');
        } else{
            $error = "Something wrong! Please try later ";            
            return back()->with('message', 'Something wrong! Please try later.');
        }
    }
	
    public function changeStatus(Request $request)
	{       
		$planrecharge = PlanRecharge::find($request->id);
		$planrecharge->status = $request->status;
        $planrecharge->save();
		return response()->json(['success'=>'Status change successfully.']);
	}

    public function massDestroy(Request $request)
    {
        
        $ids = $request->ids;
		
        PlanRecharge::whereIn('id',explode(",",$ids))->delete();
        return response()->json(['success'=>'Deleted successfully']);
    }

}
