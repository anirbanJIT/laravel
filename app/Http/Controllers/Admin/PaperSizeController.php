<?php



namespace App\Http\Controllers\Admin;



use Spatie\Permission\Models\Permission;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request; 

use App\Models\PaperSize;

use Illuminate\Support\Facades\Gate;

use Illuminate\Support\Str;

Use Alert;
Use DB;


class PaperSizeController extends Controller

{

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function index()

    {

      return redirect()->action('Admin\PaperSizeController@paperSizeDetails');
      if (! Gate::allows('papersize_manage')) {
          return abort(401);
      }
      $paperSizes = PaperSize::orderBy('id','DESC')->get();
      return view('admin.paper-size.index', compact('paperSizes'));
    }

    public function paperSizeDetails(Request $request) {
        if (! Gate::allows('papersize_manage')) {
          return abort(401);
        }
        $paperSizes = PaperSize::orderBy('id','DESC')->select(DB::raw('CONCAT(id,"_",status) AS id_status'),'id','title','paper_height','paper_width','status')->get();
        return view('admin.paper-size.paperSize-list', compact('paperSizes'));
    }



    /**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function create()

    {
		if (! Gate::allows('papersize_manage')) {

            return abort(401);

        }

        return view('admin.paper-size.create');

    }



    /**

     * Store a newly created resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */

    public function store(Request $request)

    {



      if(!Gate::allows('papersize_manage'))

      {

        return abort(401);

      }  



      request()->validate([

        'title' => 'required|string|max:255',

        'paper_width' => 'required',

          'paper_height' => 'required',

        ],

        [

          'title.required' => 'Title is required',

          'title.unique' => 'Title already exists',

          'paper_width.required' => 'Paper Width is required',

          'paper_height.required' => 'Paper Height is required',

        ]); 



      //$paperSize = PaperSize::create( $request->all() );



      $paperSize = new PaperSize();

      $paperSize->title = $request->title;

      $paperSize->paper_width = $request->paper_width;

      $paperSize->paper_height = $request->paper_height;

      $paperSize->save();



      Alert::success('Paper size', 'Added successfully');

      return redirect('admin/paper-sizes')->with('message', 'Paper size created successfully.');



    }



    /**

     * Show the form for editing the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function edit(PaperSize $paperSize)

    {

        if (! Gate::allows('papersize_manage')) {

            return abort(401);

        }



        return view('admin.paper-size.edit', compact('paperSize'));

    }



    /**

     * Update the specified resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function update(Request $request, PaperSize $paperSize)

    {

        if (! Gate::allows('papersize_manage')) {

            return abort(401);

        }

         

        request()->validate([

          'title' =>'required|string|max:255|unique:paper_sizes,title,'.$paperSize->id.',id',

          'paper_width' => 'required',

          'paper_height' => 'required',

        ],

        [

          'title.required' => 'Title is required',

          'title.unique' => 'Title already exists',

          'paper_width.required' => 'Paper Width is required',

          'paper_height.required' => 'Paper Height is required',

        ]); 



        $paperSize->update($request->all());

        Alert::success('Paper size', 'Updated successfully');

        return redirect()->route('admin.paper-sizes.index')->with('message', 'Paper size Updated successfully.');



    }



    public function deletePaperSize(Request $request, PaperSize $paperSize){

        //$paperSize = PaperSize::findOrFail($id);

        if($paperSize->delete()){          

          Alert::success('Paper Size', 'Deleted successfully'); 

          return back()->with('message','Paper Size Deleted successfully.');

         }

          else{

              $error = "Something wrong! Please try later ";

              return back()->with('message', 'Something wrong! Please try later.');



         }

    }



    public function changeStatus(Request $request)

    {

      $paperSize = PaperSize::find($request->paperSize_id);

      $paperSize->status = $request->status;

      $paperSize->save();

      return response()->json(['success'=>'Status change successfully.']);

    }



}

