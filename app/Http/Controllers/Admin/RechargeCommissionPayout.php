<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Models\CashWallet;
use App\Models\CreditWalletTransaction;
use App\Models\CashWalletTransaction;
use App\Models\RechargeCommission;
use App\Models\UserBankDetail;
use DB;
class RechargeCommissionPayout extends Controller
{
	
	public function create_header($token){
		$header = array(
			'X-Client-Id: '.env('CASHFREE_PAYOUT_APP_ID'),
			'X-Client-Secret: '.env('CASHFREE_PAYOUT_SECRET_KEY'), 
			'Content-Type: application/json',
		);
		$headers = $header;
		if(!is_null($token)){
			array_push($headers, 'Authorization: Bearer '.$token);
		}
		return $headers;
	}

	public function post_helper($action, $data, $token){

		$baseurl = env('CASHFREE_PAYOUT_BASEURL');

		$finalUrl = $baseurl.$action;
		$headers = $this->create_header($token);
		//dd($headers);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_URL, $finalUrl);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch,  CURLOPT_RETURNTRANSFER, true);
		if(!is_null($data)) curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data)); 
		$r = curl_exec($ch);
		if(curl_errno($ch)){
			print('error in posting');
			print(curl_error($ch));
			die();
		}
		curl_close($ch);
		$rObj = json_decode($r, true); 
		//$rObj['data']['token'];
		if($rObj['status'] == 'SUCCESS' || $rObj['subCode'] == '200') 
			return $rObj;
		else
		//return 'incorrect response: '.$rObj['message'];
			return $rObj;
	}

	public function get_helper($finalUrl, $token){
		$headers = $this->create_header($token);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $finalUrl);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch,  CURLOPT_RETURNTRANSFER, true);

		$r = curl_exec($ch);

		if(curl_errno($ch)){
			print('error in posting');
			print(curl_error($ch));
			die();
		}
		curl_close($ch);
		$rObj = json_decode($r, true);    
		if($rObj['status'] == 'SUCCESS' || $rObj['subCode'] == '200') 
			return $rObj;
		else
		//return 'incorrect response: '.$rObj['message'];
			return $rObj;
	}

	#get auth token
	public function getToken(){
		try{
			$auth = env('CASHFREE_PAYOUT_AUTHURL');

			$response = $this->post_helper($auth, null, null);

			return $response;
		}
		catch(Exception $ex){
			error_log('error in getting token');
			error_log($ex->getMessage());
			die();
		}
	}

	#get beneficiary details
	function getBeneficiary($token,$beneficiary){
		try{
        //global $baseurl, $urls, $beneficiary;
			$beneId = $beneficiary['beneId'];
			$baseurl = env('CASHFREE_PAYOUT_BASEURL');
			$getBene = env('CASHFREE_PAYOUT_GETBENEURL');
			$finalUrl = $baseurl.$getBene.$beneId;
			$response = $this->get_helper($finalUrl, $token);
       // return true;
			return $response;
		}
		catch(Exception $ex){
			$msg = $ex->getMessage();
			if(strstr($msg, 'Beneficiary does not exist')) return false;
			error_log('error in getting beneficiary details');
			error_log($msg);
			die();
		}    
	}

	#add beneficiary
	public function addBeneficiary($token,$beneficiary){
		try{
        //global $beneficiary;
			$addBene = env('CASHFREE_PAYOUT_ADDBENEURL');
			$response = $this->post_helper($addBene, $beneficiary, $token);
        //error_log('beneficiary created');
			return $response;
		}
		catch(Exception $ex){
			$msg = $ex->getMessage();
        //error_log('error in creating beneficiary');
        //error_log($msg);
        //die();
			return $msg;
		}    
	}

	#remove beneficiary
	public function removeBeneficiary($token,$beneficiary){
		try{
        //global $beneficiary;
			$removeBene = env('CASHFREE_PAYOUT_REMOVEBENEURL');
			$response = $this->post_helper($removeBene, $beneficiary, $token);
        //error_log('beneficiary created');
			return $response;
		}
		catch(Exception $ex){
			$msg = $ex->getMessage();
        //error_log('error in creating beneficiary');
        //error_log($msg);
        //die();
			return $msg;
		}    
	}

	#request transfer
	public function requestTransfer($token,$transfer){
		try{
        //global $transfer;
			$requestTransfer = env('CASHFREE_PAYOUT_REQUESTTRANSFERURL');
			$response = $this->post_helper($requestTransfer, $transfer, $token);
        //error_log('transfer requested successfully');
			return $response;
		}
		catch(Exception $ex){
			$msg = $ex->getMessage();
			error_log('error in requesting transfer');
			error_log($msg);
			die();
		}
	}

	#get transfer status
	function getTransferStatus($token,$transfer){
		try{
       // global $baseurl, $urls, $transfer;
			$transferId = $transfer['transferId'];
			$baseurl = env('CASHFREE_PAYOUT_BASEURL');
			$getTransferStatus = env('CASHFREE_PAYOUT_GETTRANSFERSTATUSURL');
			$finalUrl = $baseurl.$getTransferStatus.$transferId;
			$response = $this->get_helper($finalUrl, $token);
        //error_log(json_encode($response));
			return $response;
		}
		catch(Exception $ex){
			$msg = $ex->getMessage();
			error_log('error in getting transfer status');
			error_log($msg);
			die();
		}
	}

	public function rechargeCommissionPayout(Request $request)
	{
		$user_id = $request->user_id ?? 1;
		$retailerUsers = User::with('roles')->role('retailer')->get();
		return view('admin.retailers.recharge-commission-payout',compact('user_id','retailerUsers'));
	}

	public function rechargeCommissionPayoutReport(Request $request)
	{
		
		//https://github.com/cashfree/cashfree-payout-php/blob/master/index.php
		//prod secret key :  4a9cec11fd63c309d2a7069fa2fa8b3d48d0979f
		//prod App id :  CF43631FMH3FLP2HUY6MYA

		$usersQuery = RechargeCommission::with('incomeReceiver','creditPurchaser');

		if (request()->has('user_id') && !empty($request->user_id) ) {
			$user_id = $request->user_id;
			$usersQuery->where('user_id',$user_id);
		}else{
			$usersQuery->where('user_id','!=',1);
		}

		if($_GET["from_date"]!='' && $_GET["to_date"]!='')
		{
			$from = $_GET["from_date"]." 00:00:00";
			$to = $_GET["to_date"]." 00:00:00";
			$usersQuery->whereBetween(DB::raw('DATE(created_at)'),[$from, $to]);
			
		}

		if($_GET["invoice_no"]!='' )
		{
			$usersQuery->where('invoice_no',$_GET["invoice_no"]);
		}

		$users = $usersQuery->select('*')->orderBy('id','desc');
		
		return datatables()->of($users)->editColumn('created_at', function ($users) {
			return [
				'display' => e($users->created_at->format('d M Y')),
				'timestamp' => $users->created_at->timestamp
			];
		})->filterColumn('created_at', function ($query, $keyword) {
			$query->whereRaw("DATE_FORMAT(created_at,'%d %M %Y') LIKE ?", ["%$keyword%"]);
		})->addColumn('user_id', function ($users) {
			return $users->incomeReceiver->name ?? '';
		})->addColumn('sender_id', function ($users) {
			return $users->creditPurchaser->name ?? '';
		})->addColumn('status', function ($users) {
			if ($users->status == 1) {
				return "Paid";
			} else {
				return "Unpaid";
			}
		})->with('total_invoice_credit', $users->sum('invoice_credit'))
		->with('total_commission_credit', $users->sum('commission_credit'))
		->with('total_invoice_amt', $users->sum('invoice_amt'))
		->with('total_commission_amt', $users->sum('commission_amt'))
		->addColumn('checkbox', function ($users) {
			return '<input type="checkbox" name="ids[]" class="pdr_checkbox" value="'.$users->id.'" />
			<input type="hidden" name="retailer_id[]" value="'.$users->user_id.'" />';
		})->rawColumns(['checkbox','action'])->make(true);
	}

	public function commissionPayout(Request $request)
	{  
		$payoutMsg = array();
		$rechargeCom = 0;
		$invoice_no = rechargeCommissionInvoiceNumber();
		$i=0;
		$ids = $request->ids;
		if($ids!='') {
			if ($request->payout_total_amount == 'yes') {
				/*pay total amount */
				$comQuery = RechargeCommission::select('user_id', DB::raw('SUM(commission_amt) as total_commission_amt'))->whereIn('user_id',$ids)->where('status','1')->groupBy('user_id')->get();
			}else{
				/*pay select row amount */
				$comQuery = RechargeCommission::select('user_id', DB::raw('SUM(commission_amt) as total_commission_amt'))->whereIn('id',$ids)->where('status','1')->groupBy('user_id')->get();
			}		

			if (!$comQuery) {
				$payoutMsg[$i]['user_id'] = 0;
				$payoutMsg[$i]['user_name'] = 0;
				$payoutMsg[$i]['message'] = 'No data found';
				return redirect()->back()->with('payoutMsg',$payoutMsg);
			}
 
			foreach($comQuery as $comVal)
			{ 
				$rechargeCom = $comVal->total_commission_amt;

				$user = User::find($comVal->user_id);
				$bank_detail = $user->getBankDetail;
				$contact_detail = $user->getContactDetail;

				$balance = ($user->getCashWalletBalance->amount) ?? 0;
dd($balance);
				if($balance>=$rechargeCom) {

					if($balance>0)
					{
						$balance_amount = $balance-$rechargeCom;
					}
					else
					{
						$balance_amount = $rechargeCom;
					}
					dd($bank_detail);
					if($bank_detail && $contact_detail) 
					{
						$tokenData = $this->getToken();
						$token = $tokenData['data']['token'];
						if($token)
						{
							$stateName = DB::table('states')->where('id',$contact_detail->state)->select('name')->first();
							$name = strtok($user->name,' ');
							$beneficiary = array(
								'beneId' => $name.$user->id,
								'name' => $user->name,
								'email' => $user->email,
								'phone' => $user->phone,
								'bankAccount' => $bank_detail->account_no,
								'ifsc' => $bank_detail->ifsc_code,
								'address1' => $contact_detail->address,
								'city' => $contact_detail->city,
								'state' => $stateName->name,
								'pincode' => $contact_detail->pin_code,
							);
							$transfer = array(
								'beneId' => $name.$user->id,
								'amount' => $rechargeCom,
								'transferId' => $invoice_no,
							);

							$getBeneficiary  = $this->getBeneficiary($token,$beneficiary);
					
							if($getBeneficiary['status']=='SUCCESS' && $getBeneficiary['subCode']==200)
							{
								$requestTransfer  = $this->requestTransfer($token,$transfer);
					//dd($requestTransfer); 	
								if($requestTransfer['status']=='SUCCESS' && $requestTransfer['subCode']==200)
								{
									$getTransferStatus  = $this->getTransferStatus($token,$transfer);
					//dd($getTransferStatus);
									if($getTransferStatus['status']=='SUCCESS' && $getTransferStatus['subCode']==200)
									{

										$data = new CashWalletTransaction;
										$data->invoice_no = $invoice_no;
										$data->receiver_id = $comVal->user_id;
										$data->sender_id = auth()->user()->id;
										$data->credit_amt = 0;
										$data->debit_amt = $rechargeCom;
										$data->balance_amt = $balance_amount;
										$data->type = 'Payout Transfer';
										$data->payment_mode = $getTransferStatus['data']['transfer']['transferMode'];
										$data->transaction_id  = $getTransferStatus['data']['transfer']['referenceId'];
										$data->transaction_description = 'your recharge commission '.env('CURRENCY_SYMBOL').' '.$rechargeCom.' has been transfered';
										if($data->save())
										{
											CashWallet::where('user_id',$comVal->user_id)->update(['amount' => $balance_amount]);
											RechargeCommission::whereIn('id',$request->id)->where('user_id',$comVal->user_id)->update(['status' => 2]);
										}


					//return redirect()->route('admin.recharge-commission-payout')->with('message',$getTransferStatus['message']);
										$payoutMsg[$i]['user_id'] = $user->id;
										$payoutMsg[$i]['user_name'] = $user->name;
										$payoutMsg[$i]['message'] = $getTransferStatus['message'];
									}
									else{
						//return redirect()->route('admin.recharge-commission-payout')->with('message',$getTransferStatus['message']);
										$payoutMsg[$i]['user_id'] = $user->id;
										$payoutMsg[$i]['user_name'] = $user->name;
										$payoutMsg[$i]['message'] = $getTransferStatus['message'];
									}
								}
								else{
									$removeBeneficiary  = $this->removeBeneficiary($token,$beneficiary);
									if($removeBeneficiary['status']=='SUCCESS' && $removeBeneficiary['subCode']==200)
									{
										goto B;
									}
									else{
						//return redirect()->route('admin.recharge-commission-payout')->with('message',$removeBeneficiary['message']);
										$payoutMsg[$i]['user_id'] = $user->id;
										$payoutMsg[$i]['user_name'] = $user->name;
										$payoutMsg[$i]['message'] = $removeBeneficiary['message'];
									}

								}

							}
							else {
								B : $addBeneficiary  = $this->addBeneficiary($token,$beneficiary);

					//dd($addBeneficiary); 
								if($addBeneficiary['status']=='SUCCESS' && $addBeneficiary['subCode']==200)
								{
									$requestTransfer  = $this->requestTransfer($token,$transfer);
					//dd($requestTransfer); 	
									if($requestTransfer['status']=='SUCCESS' && $requestTransfer['subCode']==200)
									{
										$getTransferStatus  = $this->getTransferStatus($token,$transfer);
					//dd($getTransferStatus);
										if($getTransferStatus['status']=='SUCCESS' && $getTransferStatus['subCode']==200)
										{

											$data = new CashWalletTransaction;
											$data->invoice_no = $invoice_no;
											$data->receiver_id = $comVal->user_id;
											$data->sender_id = auth()->user()->id;
											$data->credit_amt = 0;
											$data->debit_amt = $rechargeCom;
											$data->balance_amt = $balance_amount;
											$data->type = 'Payout Transfer';
											$data->payment_mode = $getTransferStatus['data']['transfer']['transferMode'];
											$data->transaction_id  = $getTransferStatus['data']['transfer']['referenceId'];
											$data->transaction_description = 'your recharge commission '.env('CURRENCY_SYMBOL').' '.$rechargeCom.' has been transfered';
											if($data->save())
											{
												CashWallet::where('user_id',$comVal->user_id)->update(['amount' => $balance_amount]);
												RechargeCommission::whereIn('id',$request->id)->where('user_id',$comVal->user_id)->update(['status' => 2]);
											}


					//return redirect()->route('admin.recharge-commission-payout')->with('message',$getTransferStatus['message']);
											$payoutMsg[$i]['user_id'] = $user->id;
											$payoutMsg[$i]['user_name'] = $user->name;
											$payoutMsg[$i]['message'] = $getTransferStatus['message'];

										}
										else{
						//return redirect()->route('admin.recharge-commission-payout')->with('message',$getTransferStatus['message']);
											$payoutMsg[$i]['user_id'] = $user->id;
											$payoutMsg[$i]['user_name'] = $user->name;
											$payoutMsg[$i]['message'] = $getTransferStatus['message'];
										}
									}
									else{
						//return redirect()->route('admin.recharge-commission-payout')->with('message',$requestTransfer['message']);
										$payoutMsg[$i]['user_id'] = $user->id;
										$payoutMsg[$i]['user_name'] = $user->name;
										$payoutMsg[$i]['message'] = $requestTransfer['message'];
									}

								}
								else{
						//return redirect()->route('admin.recharge-commission-payout')->with('message',$addBeneficiary['message']);
									$payoutMsg[$i]['user_id'] = $user->id;
									$payoutMsg[$i]['user_name'] = $user->name;
									$payoutMsg[$i]['message'] = $addBeneficiary['message'];
								}
							}
						}
						else
						{
							return redirect()->back()->with('message',$tokenData['message']);
							return redirect()->route('admin.recharge-commission-payout')->with('message',$tokenData['message']);
						}
					}
					else
					{
						 //return redirect()->route('admin.recharge-commission-payout')->with('message','Please complete user bank details and contact details first for retailer '.$comVal->user_id);

						$payoutMsg[$i]['user_id'] = $user->id;
						$payoutMsg[$i]['user_name'] = $user->name;
						$payoutMsg[$i]['message'] = 'Please complete user bank details and contact details first for retailer '.$comVal->user_id;
					}
					
				}
				else{
					
					//return redirect()->route('admin.recharge-commission-payout')->with('message','You have not sufficient balance in cash wallet');
					$payoutMsg[$i]['user_id'] = $user->id;
					$payoutMsg[$i]['user_name'] = $user->name;
					$payoutMsg[$i]['message'] = 'You have not sufficient balance in cash wallet';
								//return redirect()->route('admin.recharge-commission-payout')->with('payoutMsg',$payoutMsg);


				}
				$i++;
			}




		}
		else{


			return redirect()->back()->with('message','Please check at least one record for payment');
			return redirect()->route('admin.recharge-commission-payout')->with('message','Please check at least one record for payment');
		}
		return redirect()->back()->with('payoutMsg',$payoutMsg);
		return redirect()->route('admin.recharge-commission-payout')->with('payoutMsg',$payoutMsg);
	}
}