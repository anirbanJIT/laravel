<?php

namespace App\Http\Controllers\Admin;

use Spatie\Permission\Models\Permission;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\GeneralNotification;
use Illuminate\Support\Facades\Gate;
use App\FCM;
use DB;
use Illuminate\Support\Str;
use App\User;
Use Alert;
use File;

class GeneralNotificationController extends Controller
{
  
    public function index()
    {
      return redirect()->action('Admin\GeneralNotificationController@generalNotificationDetails');
      if (! Gate::allows('notification_manage')) {
          return abort(401);
      }
   
      $generalnotifications = GeneralNotification::with('users')->orderBy('id','desc')->get();

      return view('admin.generalnotifications.index', compact('generalnotifications'));
    }

    public function generalNotificationDetails(Request $request) {
        if (! Gate::allows('notification_manage')) {
          return abort(401);
        }
        $dataDetails = GeneralNotification::with('users')->orderBy('id','desc')->get();

        $generalnotifications = array();
        foreach($dataDetails as $notification){
          
          $user_name = [];
          if($notification->user_id != NULL){
            $ids = explode(',',$notification->user_id);
            $user_name = User::whereIn('id',$ids)->select('name')->get()->toArray();

          }else{
            $user_name = [];
          }

          array_push($generalnotifications,array(
            "id" => $notification->id,
            "title" => $notification->title,
            "image" => $notification->image,
            "message" => $notification->message,
            "user_id" => $notification->user_id,
            "user_name" => implode(',',array_column($user_name, 'name')).'_'.$notification->user_id,
            "read" => $notification->read,
            "created_at" => $notification->created_at
          ));
          
        }

       
        return view('admin.generalnotifications.generalNotification-list', compact('generalnotifications'));
    }

    public function create(Request $request)
    {
        if (! Gate::allows('notification_manage')) {
            return abort(401);
        }

        $users = User::all();
        
        
        return view('admin.generalnotifications.create',compact('users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function savenotification(Request $request)
    {
  		if(!Gate::allows('notification_manage'))
        {

          return abort(401);

      } else {

        request()->validate([
          'title' => 'required',
          'message' => 'required',
		      //'user_id' => 'required',
          'notificationImage.*' => 'nullable|mimes:jpeg,png,jpg,gif',
        ],
        [
		      'user_id.required' => 'User Name is required',
          'title.required' => 'Title is required',
          'message.required' => 'Message is required',
          'notificationImage.required' => 'Image is required',
        ]); 
        //dd($request->user_id);
        $notifications = new GeneralNotification();

        if ($request->file('notificationImage')) {
          ini_set('memory_limi', -1);        
          $k=1;
          foreach($request->file('notificationImage') as $file)
          {
            $destinationPath = public_path('Admin/images/notifications');
            $notificationImage = time().$k.'.'.$file->getClientOriginalExtension();
            $file->move($destinationPath, $notificationImage);
            $dataImage[] = $notificationImage;  
            $k++;
          }
          $notifications->image= json_encode($dataImage,JSON_FORCE_OBJECT);
        }else{
          $notifications->image= json_encode([],JSON_FORCE_OBJECT);
        }
        $notifications->title = $request->title;
        $notifications->message = $request->message;
		if(!empty($request->user_id)){
            $notifications->user_id =  implode(",",$request->user_id);
        }
        //$notifications->user_id= json_encode($request->user_id,JSON_FORCE_OBJECT);
        $notifications->save();
        
        if (!empty($request->user_id)) {
    			foreach($request->user_id as $row){
    				DB::table('users')->select('fcm_token')->where('id', $row)->where('status', '1')->whereNotNull('fcm_token')->orderBy('id')->chunk(500, function ($users) use($notifications) {
    				  $userTokens = $users->pluck('fcm_token')->toArray();
    				  $fcm = new FCM();
    				  $fcm->token = $userTokens;
    				  $fcm->notification['title'] = Str::limit($notifications->title, 100);
    				  $fcm->notification['body'] = Str::limit($notifications->message, 100);
    				  $fcm->notification['image'] = $notifications->image;
    				  $fcm->data['general_notifications_id'] = $notifications->id;
    				  $fcm->send();
    				});
    			}

          
        }else{
            DB::table('users')->select('fcm_token')->where('status', '1')->whereNotNull('fcm_token')
                  ->orderBy('id')->chunk(500, function ($users) use($notifications) {
              $userTokens = $users->pluck('fcm_token')->toArray();
              $fcm = new FCM();
              $fcm->token = $userTokens;
              $fcm->notification['title'] = Str::limit($notifications->title, 100);
              $fcm->notification['body'] = Str::limit($notifications->message, 100);
              $fcm->notification['image'] = $notifications->image;
              $fcm->data['general_notifications_id'] = $notifications->id;
             $fcm->send();

          });

        }
        Alert::success('Notification', 'Added successfully');
        return redirect()->route('admin.generalnotification.index');
      }      
    
    }

    public function edit($id)
    {
        if (! Gate::allows('notification_manage')) {
            return abort(401);
        }
        $notifications = GeneralNotification::findOrFail($id);
        $user_ids = explode(',',$notifications->user_id);
        // $user = '';
        // if(!empty($notifications->user_id)) {
          // $user_ids = implode(',',$notifications->user_id);
          // $user = User::whereIn('id', $user_ids)->get();
        // }
        //dd(in_array(962,$user_ids));
        $users = User::all();
        return view('admin.generalnotifications.edit',compact('notifications','users','user_ids'));
    }

     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      if(!Gate::allows('notification_manage'))
      {

        return abort(401);

      } else {

        request()->validate([
          'title' => 'required',
          'user_id' => 'required',
          'message' => 'required',
          'notificationImage' => 'nullable|file|mimes:jpeg,png,jpg,gif',
        ],
        [
          'user_id.required' => 'User Name is required',
          'title.required' => 'Title is required',
          'message.required' => 'Message is required',
          'notificationImage.required' => 'Image is required',
        ]); 

        $notifications = GeneralNotification::findOrFail($id);

        ini_set('memory_limi', -1);  
        $notificationImage = '';              
        if ($request->hasFile('notificationImage')) {
            $notificationImage = time().'.'.$request->notificationImage->getClientOriginalExtension();
            $request->notificationImage->move(public_path('Admin/images/notifications'),$notificationImage);
        }
        $notifications->title = $request->title;
        $notifications->message = $request->message;
        $notifications->image = $notificationImage;
        //$notifications->user_id = !empty($request->user_id) ? $request->user_id : null;
        $notifications->user_id =  implode(",",$request->user_id);
        $notifications->save();

        Alert::success('Notification', 'Update successfully');
        
        return redirect()->route('admin.generalnotification.index')->with('message', 'Notification Update successfully.');

      }  
    }

    public function show($id)
    {
        if (! Gate::allows('notification_manage')) {
            return abort(401);
        }
        $notifications = GeneralNotification::findOrFail($id);
        $user = '';
        if(!empty($notifications->user_id)) {
          $user = User::where('id', $notifications->user_id)->first();
        }
        return view('admin.generalnotifications.show',compact('notifications','user'));
    }

    public function destroy(Request $request, $id){

      if(!Gate::allows('notification_manage'))
      {
        return abort(401);
      }

        $notifications = GeneralNotification::findOrFail($id);
        $notifications->delete();      
			//Alert::success('Notification', 'Deleted successfully'); 
        return redirect()->route('admin.generalnotification.index');
     
    }

    public function notificationSend(Request $request)
    {
		$notfication = $request->notfication;
		$sendaspopup = $request->sendaspopup;
		$id = $request->id;
		$notifications = GeneralNotification::findOrFail($id);
		$status = "";
		if($sendaspopup == "on"){
			if($notifications->user_id != ""){
				$user = User::where("id",$notifications->user_id)->first();
				$fcm = new FCM();
				$fcm->token = $user->fcm_token;
				$fcm->notification['title'] = 'Change Complaint Status';
				$fcm->notification['body'] = "Your Complaint is ".$status." of that ".$user->name." User.";
				$fcm->send();
			}
		}else{
			if($notifications->user_id != ""){
				$user = User::where("id",$notifications->user_id)->first();
				$fcm = new FCM();
				$fcm->token = $user->fcm_token;
				$fcm->notification['title'] = 'Change Complaint Status';
				$fcm->notification['body'] = "Your Complaint is ".$status." of that ".$user->name." User.";
				$fcm->send();
			}
		}
		return redirect()->back()->with('success', 'Send Notification.'); 
	}

}
