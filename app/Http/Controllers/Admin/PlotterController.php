<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Plotter;
use App\Models\PlanRecharge;

use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Str;
use DB;
Use Alert;
use Symfony\Component\HttpFoundation\Response;

class PlotterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
		$plotters = Plotter::orderBy('id','DESC')->select(DB::raw('CONCAT(id,"_",status) AS id_status'),'id','name','speed','pressure','image','status','created_at')->get();
       
       return view('admin.plotter.index', compact('plotters'));
    }
	
	/**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $plans = PlanRecharge::all();
        return view('admin.plotter.create', compact('plans'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
			'name' => 'required|min:2',
			'plotterImg' => 'required|mimes:jpeg,jpg,png|max:2048',
			'speed' => 'required',
			'pressure' => 'required',
			'status'=>'required'
		]); 

		$plotter = new Plotter();
	
		if(!empty($request->plotterImg)){
			$plotterImage = time().'.'.$request->plotterImg->getClientOriginalExtension();
			$request->plotterImg->move(public_path('Admin/images/plotters'),$plotterImage);
		}else{
			$plotterImage = '';
		}
				 
		$plotter->name = $request->name;
		$plotter->image = $plotterImage;
		$plotter->speed = $request->speed;
		$plotter->status = $request->status;
		$plotter->pressure = $request->pressure;                     
		$plotter->save();
		Alert::success('Plotter', 'Added successfully');
		return redirect()->route('admin.plotter.index')->with('message', 'Plotter created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
		$plotter = Plotter::findOrFail($id);
        return view('admin.plotter.show', compact('plotter'));
    }
	
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
     
        $plotter = Plotter::findOrFail($id);
		return view('admin.plotter.edit', compact('plotter'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validated = $request->validate([
			'name' => 'required|min:2',
			'plotterImg' => 'nullable|mimes:jpeg,jpg,png|max:2048',
			'speed' => 'required',
			'pressure' => 'required',
			'status'=>'required'
		]); 
			
		$plotter = Plotter::findOrFail($id);
		if($request->plotterImg != null){
			if(file_exists(public_path('Admin/images/plotters/'.$plotter->image))){
			   unlink(public_path('Admin/images/plotters/'.$plotter->image)); // so you never get here
			}
			$imageUpdate = time().'.'.$request->plotterImg->getClientOriginalExtension();
			$request->plotterImg->move(public_path('Admin/images/plotters'), $imageUpdate);
			$plotter->image= $imageUpdate;
		}else{
			$imageUpdate = $request->imageH;
			$plotter->image= $imageUpdate;
		}
		$plotter->name = $request->name;
		$plotter->speed = $request->speed;
		$plotter->pressure = $request->pressure;
		$plotter->status = $request->status;
		$plotter->save();
		
		
		Alert::success('Plotter', 'updated successfully');
		return redirect()->route('admin.plotter.index')->with('message', 'Plotter updated successfully.');
		
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
	 
	public function destroy($id) {
		
        $data = Plotter::find($id);
		if(file_exists(public_path('Admin/images/plotters/'.$data->image))){
		   unlink(public_path('Admin/images/plotters/'.$data->image)); // so you never get here
		}
		
        if($data->delete()){
			return redirect()->route('admin.plotter.index')->withMessage('Deleted successfully!');
        } else{
            $error = "Something wrong! Please try later ";            
            return back()->with('message', 'Something wrong! Please try later.');
        }
    }
	
    public function changeStatus(Request $request)
	{       
		$plotter = Plotter::find($request->plotter_id);
		$plotter->status = $request->status;
        $plotter->save();
		return response()->json(['success'=>'Status change successfully.']);
	}

    public function massDestroy(Request $request)
    {
        
        $ids = $request->ids;
		$getdata = Plotter::whereIn('id',explode(",",$ids))->get();
		foreach($getdata as $r){
			if(file_exists(public_path('Admin/images/plotters/'.$r->image))){
			   unlink(public_path('Admin/images/plotters/'.$r->image)); // so you never get here
			}
		}
        Plotter::whereIn('id',explode(",",$ids))->delete();
        return response()->json(['success'=>'Deleted successfully']);
    }

}
