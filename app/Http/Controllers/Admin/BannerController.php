<?php

namespace App\Http\Controllers\Admin;

use Spatie\Permission\Models\Permission;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Banner;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Str;
Use Alert;
use File;
use DB;

class BannerController extends Controller
{
  
    public function index()
    {
      if (! Gate::allows('banner_manage')) {
          return abort(401);
      }
   
       
	   $banners = DB::table('banners')->groupBy('banner_location')->orderBy('banner_location_position_order','ASC')->get()->toArray(); 
	   
	  
       
      return view('admin.banners.banner-list', compact('banners'));
    }
	public function bannerOrderPosition(Request $request){
		$position_order = $request->position;
		$i=1;
		
		foreach($position_order as $k=>$v){
			Banner::where("title",$v)->update(["banner_location_position_order" => $i++]);	
		}
		
		return redirect()->back()->with('message', 'Item Position Update.');
	}

    public function bannerDetails($location)
    {
      if (! Gate::allows('banner_manage')) {
          return abort(401);
      }
   
       $banner_details = Banner::where('banner_location',$location)->select(DB::raw('CONCAT(id,"_",status) AS id_status'),'id','title','image','ex_link','status','banner_location','banner_location_position_order','created_at')->orderBy('banner_location_position_order','ASC')->get();

      return view('admin.banners.index', compact('banner_details','location'));
    }

    public function create(Request $request,$location)
    {
		
        if (! Gate::allows('banner_manage')) {
            return abort(401);
        }
		
        return view('admin.banners.create',compact('location'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

		if(!Gate::allows('banner_manage')){
			return abort(401);
		} else {

			request()->validate([
			  'title' => 'required',
			  'ex_link' => 'required',
			  'image' => 'required|file|mimes:jpeg,png,jpg,gif',
			],
			[
			  'title.required' => 'Title is required',
			  'ex_link.required' => 'link is required',
			  'image.required' => 'Image is required',
			]); 
			$lastRecord = Banner::orderBy('banner_location_position_order','DESC')->first();
			$banners = new Banner();
			ini_set('memory_limi', -1);  
			$image = '';              
			if ($request->hasFile('image')) {
				$image = time().'.'.$request->image->getClientOriginalExtension();
				$request->image->move(public_path('images/banners'),$image);
			}else{
			  $image = $request->attachmnetUpdate;
			}
			$banners->title = $request->title;
			$banners->ex_link = $request->ex_link;
			$banners->banner_location = $request->banner_location;
			$banners->image = $image;
			$banners->status = 1;
			$banners->banner_location_position_order = isset($lastRecord->banner_location_position_order) ? ($lastRecord->banner_location_position_order + 1):1;
			$banners->save();

			Alert::success('Banner', 'Added successfully');
			return redirect()->back()->with('message', 'Banner created successfully.');
		}      
    }

    public function edit($id)
    {
        if (! Gate::allows('banner_manage')) {
            return abort(401);
        }
        $banners = Banner::findOrFail($id);
        return view('admin.banners.edit',compact('banners'));
    }

     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		if(!Gate::allows('banner_manage')){
			return abort(401);
		} else {
			request()->validate([
			  'title' => 'required',
			  'ex_link' => 'required',
			  'image' => 'nullable|file|mimes:jpeg,png,jpg,gif',
			],
			[
			  'title.required' => 'Title is required',
			  'ex_link.required' => 'link is required',
			]); 

			$banners = Banner::findOrFail($id);
			ini_set('memory_limi', -1);           
			if ($request->hasFile('image')) {
				$image = time().'.'.$request->image->getClientOriginalExtension();
				$request->image->move(public_path('images/banners'),$image);
				$banners->image = $image;
			}
			$banners->title = $request->title;
			$banners->ex_link = $request->ex_link;
			$banners->banner_location = $request->banner_location;        
			$banners->status = 1;
			$banners->save();

			Alert::success('Banner', 'Update successfully');
			return redirect()->back()->with('message', 'Banner Update successfully.');
		}   
    }
	
	public function destroy(Request $request, $id){

      if(!Gate::allows('banner_manage'))
      {
        return abort(401);
      }

		$banners = Banner::findOrFail($id);
	  
		$updaterecords = Banner::where("banner_location_position_order",'>',$banners->banner_location_position_order)->orderBy("banner_location_position_order",'ASC')->get();
		
		foreach($updaterecords as $row){
			$update = Banner::where("id",$row->id)->update(["banner_location_position_order"=>($row->banner_location_position_order-1)]);
		} 
		
      if($banners->delete()){          
          Alert::success('Banner', 'Deleted successfully'); 
          return back()->with('message', 'Banner Deleted successfully.');
      }
      else{
          $error = "Something wrong! Please try later ";
          return back()->with('message', 'Something wrong! Please try later.');

      }
    }

    public function delete(Request $request, $id){

      if(!Gate::allows('banner_manage'))
      {
        return abort(401);
      }

      $banners = Banner::findOrFail($id);
      if($banners->delete()){          
          Alert::success('Banner', 'Deleted successfully'); 
          return back()->with('message', 'Banner Deleted successfully.');
      }
      else{
          $error = "Something wrong! Please try later ";
          return back()->with('message', 'Something wrong! Please try later.');

      }
    }

    public function changeStatus(Request $request)
	{
		$banner = Banner::find($request->banner_id);
        $banner->status = $request->status;
        $banner->save();
		return response()->json(['success'=>'Status change successfully.']);
        /*if($request->status == 1){
            $brand->status = $request->status;
            $brand->save();
            return response()->json(['success'=>'Status InActive successfully.']);
          }
          if($request->status == 0){
            $brand->status = $request->status;
            $brand->save();
            return response()->json(['success'=>'Status Active successfully.']);
          } */
       
	}
}
