<?php
namespace App\Http\Controllers\Admin;
use Spatie\Permission\Models\Permission;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\VideoCategory;
use App\Models\Video;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Str;
Use Alert;
Use DB;

class VideoCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      if (! Gate::allows('video_category_manage')) {
          return abort(401);
      }
   
      $videoCategories = VideoCategory::orderBy('position_order','ASC')->select('id','title','position_order','image',DB::raw('CONCAT(id,"_",status) AS id_status'),'created_at')->get();
	  
      return view('admin.video-category.index', compact('videoCategories'));
    }
	
	public function videoCategoryOrderPosition(Request $request){
		$position_order = $request->position;
		$i=1;
		$currurl = $request->currurl;
        if($currurl == "admin.video.show"){
            foreach($position_order as $k=>$v){
			Video::where("id",$v)->update(["video_position_order" => $i++]);	
            }
            
            return redirect()->back()->with('message', 'Item Position Update.');    
        }

		foreach($position_order as $k=>$v){
			VideoCategory::where("id",$v)->update(["position_order" => $i++]);	
		}
		
		return redirect()->back()->with('message', 'Item Position Update.');
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('video_category_manage')) {
            return abort(401);
        }
        return view('admin.video-category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      if(!Gate::allows('video_category_manage')){
        return abort(401);
      } else {
		$validated = $request->validate([
			'title' => 'required',
		]); 
		$lastRecord = VideoCategory::orderBy('position_order','DESC')->first();
		
        $videoCategory = new VideoCategory();
        $videoCategory->title = $request->title;
        if(!empty($request->image)){
			$videoCategoryImage = time().'.'.$request->image->getClientOriginalExtension();
			$request->image->move(public_path('Admin/images/video-category'),$videoCategoryImage);
        }else{
            $videoCategoryImage = '';
		}
        $videoCategory->image = $videoCategoryImage;
        $videoCategory->status = '1';
		$videoCategory->position_order = isset($lastRecord->position_order) ? ($lastRecord->position_order + 1):1;     
        $videoCategory->save();

        Alert::success('Video Category', 'Added successfully');
        return redirect()->route('admin.video-category.index')->with('message', 'Video Category created successfully.');

      }      

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('video_category_manage')) {
            return abort(401);
        }else{
			$videoCategory = VideoCategory::findOrFail($id);
			return view('admin.video-category.edit', compact('videoCategory'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(!Gate::allows('video_category_manage')){
            
            return abort(401);

        } else{
        	$validated = $request->validate([
				'title' => 'required',
			]); 

			$videoCategory = VideoCategory::findOrFail($id);
			$videoCategory->title = $request->title;
			if($request->image != null){
				$imageUpdate = time().'.'.$request->image->getClientOriginalExtension();
				$request->image->move(public_path('Admin/images/video-category'), $imageUpdate);
				$videoCategory->image= $imageUpdate;
            }else{
                $imageUpdate = $request->imageH;
                $videoCategory->image= $imageUpdate;
            }         
			$videoCategory->status = '1';
			$videoCategory->save();

			Alert::success('Video Category', 'Updated successfully');
			return redirect()->route('admin.video-category.index')->with('message', 'Video Category Updated successfully.');

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if (! Gate::allows('video_category_manage')) {
            return abort(401);
        }
        
        $videoCategory = VideoCategory::findOrFail($id);
		if(isset($videoCategory->position_order)){
			$updaterecords = VideoCategory::where("position_order",'>',$videoCategory->position_order)->orderBy("position_order",'ASC')->get();
			
			foreach($updaterecords as $row){
				$update = VideoCategory::where("id",$row->id)->update(["position_order"=>($row->position_order-1)]);
			} 
		}
		
        if($videoCategory->delete()){ 
			Video::where('video_category_id', $id)->delete();	
            Alert::success('Video Category', 'Deleted successfully'); 
			return back()->with('message', 'Video Category Deleted successfully.');
        }else{
            $error = "Something wrong! Please try later ";
            return back()->with('message', 'Something wrong! Please try later.');
		}
    }
    
    
    public function deleteVideoCategory(Request $request, $id){

        if (! Gate::allows('video_category_manage')) {
            return abort(401);
        }
        
        $videoCategory = VideoCategory::findOrFail($id);
        if($videoCategory->delete()){          
            Alert::success('Video Category', 'Deleted successfully'); 
           return back()->with('message', 'Video Category Deleted successfully.');
         }
          else{
              $error = "Something wrong! Please try later ";
              return back()->with('message', 'Something wrong! Please try later.');

         }
    }
    public function changeStatus(Request $request)
    {
        $videoCategory = VideoCategory::find($request->videoCategory_id);
        $videoCategory->status = $request->status;
        $videoCategory->save();
        return response()->json(['success'=>'Status change successfully.']);
    }

}
