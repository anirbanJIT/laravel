<?php



namespace App\Http\Controllers\Admin;



use Spatie\Permission\Models\Permission;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;


use App\Models\PlanCommission;

use App\Models\CommissionSetting;

use App\Models\BrandType;

use App\Models\PlanBrandTypes;
use App\Models\Plan;
use App\Models\PlanRecharge;
use App\Models\Customer;
use App\User;


use App\Models\PlanSerialNumbers;

use Illuminate\Support\Facades\Gate;

use Illuminate\Support\Str;

use Alert;

use File;

use DB;



class SerialNumbersController extends Controller
{



    public function index()
    {

        return redirect()->action('Admin\SerialNumbersController@serialNumberDetails');
        $serialNumbers = PlanSerialNumbers::select('planserial_numbers.*', 'plans.name', 'users.name as username')
            ->leftJoin('plans', 'plan.id', '=', 'planserial_numbers.plan_id')
            ->leftJoin('users', 'users.id', '=', 'planserial_numbers.user_id')
            ->orderBy('id', 'DESC')->get();

        return view('admin.serial-numbers.index', compact('serialNumbers'));
    }
    public function serialNumberDetails(Request $request)
    {
        $plan = Plan::select('plans.*')->get();
        $user_customer = DB::table('customers')->select('customers.name', 'customers.id')->limit(10)->get();
        $serialNumbers = PlanSerialNumbers::select('planserial_numbers.*', 'plan_recharges.name as planname', 'users.name as username')
            ->leftJoin('plan_recharges', 'plan_recharges.id', '=', 'planserial_numbers.plan_id')
            ->leftJoin('users', 'users.id', '=', 'planserial_numbers.user_id')
            ->orderBy('id', 'DESC')->get();
        return view('admin.serial-numbers.serialNumbers-list', compact('plan', 'user_customer', 'serialNumbers'));
    }

    public function create()
    {
        $plan = PlanRecharge::select('plan_recharges.*')->where('status', "=", '1')->get();
        $rownumber = count($plan);
        return view('admin.serial-numbers.create', compact('plan', 'rownumber'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'quantity' => 'required',
            'plan_id' => 'required',
        ]);
        $quantity = $request->quantity;
        $plan_id = $request->plan_id;
        $i = 0;
        foreach ($quantity as $row) {
            $plan = new PlanSerialNumbers;
            $plan->serial_number = uniqid();
            $plan->quantity = $row;
            $plan->plan_id = $plan_id[$i++];
            $plan->save();
        }
        Alert::success('Serial Number', 'Added successfully');
        return redirect()->route('admin.serial-numbers.index')->with('message', 'Serial Number created successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $plan = Plan::select('plans.*')->get();
        $user_customer = DB::table('customers')->select('customers.name', 'customers.id')->limit(10)->get();

        $serialNumbers = PlanSerialNumbers::select('planserial_numbers.*', 'plans.name as planname', 'users.name as username')
            ->leftJoin('plans', 'plans.id', '=', 'planserial_numbers.plan_id')
            ->leftJoin('users', 'users.id', '=', 'planserial_numbers.user_id')
            ->where('planserial_numbers.id', $id)->first();

        return view('admin.serial-numbers.edit', compact('plan', 'user_customer', 'serialNumbers'));
    }

    public function update(Request $request, $id)
    {
        request()->validate([
            'quantity' => 'required',
            'plan_id' => 'required',
        ]);
        $plan = PlanSerialNumbers::findOrFail($id);
        $plan->quantity = $request->quantity;
        $plan->plan_id = $request->plan_id;
        $plan->save();
        Alert::success('Serial Number', 'Updated successfully');
        return redirect()->route('admin.serial-numbers.index')->with('message', 'Serial Number updated successfully.');
    }


    public function destroy($id)
    {
        PlanSerialNumbers::where('id', $id)->delete();
        return redirect()->route('admin.serial-numbers.index')->withMessage('Deleted successfully!');
    }
}
