<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\BrandType;
use App\Models\Brand;
use App\Models\BrandModel;
use App\Models\BrandSubModel;
use App\Models\BrandModelScreenImage;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Str;
use DB;
Use Alert;
use Symfony\Component\HttpFoundation\Response;

class BrandTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
		return redirect()->action('Admin\BrandTypeController@brandTypeDetails');
        if (! Gate::allows('brandstype_manage')) {
            return abort(401);
        }
        //$brandTypes = BrandType::orderBy('order_number','DESC')->orderBy('id','DESC')->get();
       // dd($brandTypes);
       return view('admin.brand-type.index', compact('brandTypes'));
    }
	
	public function brandTypeDetails(Request $request) {
		if (! Gate::allows('brandstype_manage')) {
            return abort(401);
        }
        $brandTypes = BrandType::orderBy('position_order','ASC')->select(DB::raw('CONCAT(id,"_",status) AS id_status'),'id','name','position_order','slug','image','status','download','meta_title','meta_description','meta_keywords','order_number','created_at')->get();
		
        return view('admin.brand-type.brandType-list', compact('brandTypes'));
    }
	
	public function brandTypeOrderPosition(Request $request){
		$position_order = $request->position;
		$i=1;
		
		foreach($position_order as $k=>$v){
			BrandType::where("id",$v)->update(["position_order" => $i++]);	
		}
		
		return redirect()->back()->with('message', 'Item Position Update.');
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('brandstype_manage')) {
            return abort(401);
        }
        else{
            return view('admin.brand-type.create');
        }
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (! Gate::allows('brandstype_manage')) {
            return abort(401);
        }else{
			$validated = $request->validate([
				'name' => 'required|min:2',
				'brandTypeImg' => 'required|mimes:jpeg,jpg,png|max:2048',
				'download' => 'required'
			]); 
	
			$lastRecord = BrandType::orderBy('position_order','DESC')->first();
			$brandType = new BrandType();
        
			if(!empty($request->brandTypeImg)){
				$brandTypeImage = time().'.'.$request->brandTypeImg->getClientOriginalExtension();
				$request->brandTypeImg->move(public_path('Admin/images/brand-types'),$brandTypeImage);
			}else{
				$brandTypeImage = '';
			}
                     
			$brandType->name = $request->name;
			$brandType->image = $brandTypeImage;
			$brandType->download = $request->download;
			$brandType->status = '1'; 
			$brandType->slug = Str::slug($request->name, '-');                     
			$brandType->meta_title = $request->metaTitle;
			$brandType->meta_description = $request->metaDescription; 
			$brandType->meta_keywords = $request->metaKeyword;
			$brandType->position_order = isset($lastRecord->position_order) ? ($lastRecord->position_order + 1):1;
			$brandType->save();
			Alert::success('Category', 'Added successfully');
            return redirect()->route('admin.brand-type.index')->with('message', 'Category created successfully.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
		$brandType = BrandType::findOrFail($id);
        return view('admin.brand-type.show', compact('brandType'));
    }
	
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
     
        if (! Gate::allows('brandstype_manage')) {
            return abort(401);
        }else{
            $brandType = BrandType::findOrFail($id);
			//dd($brandType);
            return view('admin.brand-type.edit', compact('brandType'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (! Gate::allows('brandstype_manage')) {
            return abort(401);
        }else{
			$validated = $request->validate([
				'name' => 'required|min:2',
				'brandTypeImg' => 'nullable|mimes:jpeg,jpg,png|max:2048',
				'download' => 'required'
			]); 
			
            $brandType = BrandType::findOrFail($id);
            if($request->brandTypeImg != null){
				$imageUpdate = time().'.'.$request->brandTypeImg->getClientOriginalExtension();
				$request->brandTypeImg->move(public_path('Admin/images/brand-types'), $imageUpdate);
				$brandType->image= $imageUpdate;
            }else{
                $imageUpdate = $request->imageH;
                $brandType->image= $imageUpdate;
            }
            $brandType->name = $request->name;
            $brandType->download = $request->download;
            $brandType->status = '1';
            $brandType->slug = Str::slug($request->name, '-');
            $brandType->meta_title = $request->metaTitle;
            $brandType->meta_description = $request->metaDescription; 
            $brandType->meta_keywords = $request->metaKeyword;
            $brandType->save();
            
            DB::select('update brand_model_screen_images set download ='.$request->download.' where brand_type_id='.$id);
            Alert::success('Category', 'updated successfully');
            return redirect()->route('admin.brand-type.index')->with('message', 'Category updated successfully.');
		}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
	 
	public function destroy($id) {
		
        if (! Gate::allows('brandstype_manage')) {
            return abort(401);
        }
        $data = BrandType::find($id);
		if(isset($data->position_order)){
			$updaterecords = BrandType::where("position_order",'>',$data->position_order)->orderBy("position_order",'ASC')->get();
			
			foreach($updaterecords as $row){
				$update = BrandType::where("id",$row->id)->update(["position_order"=>($row->position_order-1)]);
			} 
		}
		
        if($data->delete()){
			Brand::where('brand_type_id', $id)->delete();
            BrandModel::where('brand_type_id', $id)->delete();
            BrandSubModel::where('brand_type_id', $id)->delete();
            BrandModelScreenImage::where('brand_type_id', $id)->delete();
            return redirect()->route('admin.brand-type.index')->withMessage('Deleted successfully!');
        } else{
            $error = "Something wrong! Please try later ";            
            return back()->with('message', 'Something wrong! Please try later.');
        }
    }
	
    public function deleteBrandType(Request $request, $id){
        if (! Gate::allows('brandstype_manage')) {
            return abort(401);
        }
        $data = BrandType::find($id);
        if($data->delete()){
            Brand::where('brand_type_id', $id)->delete();
            BrandModel::where('brand_type_id', $id)->delete();
            BrandSubModel::where('brand_type_id', $id)->delete();
            BrandModelScreenImage::where('brand_type_id', $id)->delete();
            Alert::success('Category', 'Deleted successfully');
            return back()->with('message', 'Category Deleted successfully.');
        } else{
            $error = "Something wrong! Please try later ";            
            return back()->with('message', 'Something wrong! Please try later.');
        }
    }

    public function changeStatus(Request $request)
	{       
		$brandType = BrandType::find($request->brandType_id);
		$brandType->status = $request->status;
        $brandType->save();
		return response()->json(['success'=>'Status change successfully.']);
	}

    public function massDestroy(Request $request)
    {
        if (! Gate::allows('brandstype_manage')) {
            return abort(401);
        }
        $ids = $request->ids;
        BrandType::whereIn('id',explode(",",$ids))->delete();
        Brand::whereIn('brand_type_id', explode(",",$ids))->delete();
        BrandModel::whereIn('brand_type_id', explode(",",$ids))->delete();
        BrandSubModel::whereIn('brand_type_id', explode(",",$ids))->delete();
        BrandModelScreenImage::whereIn('brand_type_id', explode(",",$ids))->delete();
        return response()->json(['success'=>'Deleted successfully']);
    }

}
