<?php
namespace App\Http\Controllers\Admin;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\BulkCutPlan;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Str;
Use Alert;

class BulkCutPlanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect()->action('Admin\BulkCutPlanController@bulkcutPlanDetails');
        if (! Gate::allows('bulk_cut_plan_manage')) {
          return abort(401);
        }
   
        $bulkcut_plans = BulkCutPlan::orderBy('id','DESC')->get();
        return view('admin.bulkcut-plan.index', compact('bulkcut_plans'));
    }
    public function bulkcutPlanDetails(Request $request) {
        if (! Gate::allows('bulk_cut_plan_manage')) {
          return abort(401);
        }
        $bulkcut_plans = BulkCutPlan::orderBy('id','DESC')->get();
        
        return view('admin.bulkcut-plan.bulkcutPlan-list', compact('bulkcut_plans'));
    }
    

    /**
     * Show the form for creating a new resource.
     * name','price','credit','validity','hrs','time'
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('bulk_cut_plan_manage')) {
            return abort(401);
        }
        $roles = Role::get()->pluck('name', 'name');
        return view('admin.bulkcut-plan.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      if(!Gate::allows('bulk_cut_plan_manage'))
      {

        return abort(401);

      } else {

        request()->validate([
          'name' => 'required|unique:bulk_cut_plans',
          // 'price' => 'required|min:1',
          // 'credit' => 'required|min:1',
          'validity' => 'required|min:1',
          'duration' => 'required|min:1',
          'cut_limit' => 'required|min:1',
        ],
        [
          'name.required' => 'Name is required',
          // 'price.required' => 'Price is required',
          // 'credit.required' => 'Bulk balance is required',
          'validity.required' => 'Validity is required',
          'duration.required' => 'Duration is required',
          'cut_limit.required' => 'Limit is required',
        ]);

        $bluk_cut_plan = new BulkCutPlan();
        $bluk_cut_plan->name = $request->name;
        // $bluk_cut_plan->price = $request->price;
        // $bluk_cut_plan->credit = $request->credit;
        $bluk_cut_plan->validity = $request->validity;
        $bluk_cut_plan->duration = $request->duration;
        $bluk_cut_plan->cut_limit = $request->cut_limit;
        $bluk_cut_plan->save();

        Alert::success('Bluk cut Plan', 'Added successfully');

        return redirect()->route('admin.bulkcut-plan.index')->with('message', 'Bluk cut plan created successfully.');

      }      

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('bulk_cut_plan_manage')) {
            return abort(401);
        }
        else{
          
          $bulkcut_plan = BulkCutPlan::findOrFail($id);
          return view('admin.bulkcut-plan.edit', compact('bulkcut_plan'));

        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(!Gate::allows('bulk_cut_plan_manage')){
            
            return abort(401);

        } else{ 
          
        request()->validate([
          'name' => 'required',
          // 'price' => 'required|min:1',
          // 'credit' => 'required|min:1',
          'validity' => 'required|min:1',
          'duration' => 'required|min:1',
          'cut_limit' => 'required|min:1',
        ],
        [
          'name.required' => 'Name is required',
          // 'price.required' => 'Price is required',
          // 'credit.required' => 'Bulk balance is required',
          'validity.required' => 'Validity is required',
          'duration.required' => 'Duration is required',
          'cut_limit.required' => 'Limit is required',
        ]);

          $bluk_cut_plan = BulkCutPlan::findOrFail($id);
          $bluk_cut_plan->name = $request->name;
          // $bluk_cut_plan->price = $request->price;
          // $bluk_cut_plan->credit = $request->credit;
          $bluk_cut_plan->validity = $request->validity;
          $bluk_cut_plan->duration = $request->duration;
          $bluk_cut_plan->cut_limit = $request->cut_limit;
          $bluk_cut_plan->save();

          Alert::success('Bulk cut plan', 'Updated successfully');
          return redirect()->route('admin.bulkcut-plan.index')->with('message', 'Bulk cut plan Updated successfully.');

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    
    public function deleteBulkCutPlan(Request $request, $id){

        if (! Gate::allows('bulk_cut_plan_manage')) {
            return abort(401);
        }

        $bluk_cut_plan = BulkCutPlan::findOrFail($id);
        if($bluk_cut_plan->delete()){          
            Alert::success('Bulk Cut Plan', 'Deleted successfully'); 
           return back()->with('message', 'Bulk Cut Plan Deleted successfully.');
         }
          else{
              $error = "Something wrong! Please try later ";
              return back()->with('message', 'Something wrong! Please try later.');

         }
    }
    
    public function changeStatus(Request $request)
    { 
      $bluk_cut_plan = BulkCutPlan::findOrFail($request->id);
      $bluk_cut_plan->status = intval($request->status);
      $bluk_cut_plan->save();  
    }

}
