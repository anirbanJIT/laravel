<?php
namespace App\Http\Controllers\Admin;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\RechargePlan;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Str;
use App\Models\Plan;
Use Alert;
Use DB;

class RechargePlanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      if (! Gate::allows('recharge_plan_manage')) {
          return abort(401);
      }
   
      $recharge_plans = RechargePlan::with('plan_type')->orderBy('id','DESC')->select(DB::raw('CONCAT(id,"_",recharge_plan_status) AS id_status'),'id','recharge_plan_name','recharge_plan_type','recharge_plan_credit','recharge_plan_amount','recharge_plan_per_day_cost','recharge_plan_validity','recharge_plan_rewards','recharge_plan_status','plan_roles','country_id','currency','created_at')->get();
      return view('admin.recharge-plan.rechargePlan-list', compact('recharge_plans'));  
	  /* return view('admin.recharge-plan.index', compact('recharge_plans')); */
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('recharge_plan_manage')) {
            return abort(401);
        }

        $plans = Plan::all()->pluck('name', 'id')->prepend('Please Select Current Plan', '');
        $roles = Role::get()->pluck('name', 'name');
        $countries = DB::table("countries")->pluck("name","id");
        return view('admin.recharge-plan.create', compact('plans','roles','countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      if(!Gate::allows('recharge_plan_manage'))
      {

        return abort(401);

      } else {

        request()->validate([
          'recharge_plan_name' => 'required|unique:recharge_plans',
          'recharge_plan_type' => 'required',
          'recharge_plan_credit' => 'required|min:1',
          'recharge_plan_amount' => 'required',
          'recharge_plan_validity' => 'required',
          'country_id' => 'required',
          // 'currency'=>'required',
          'plan_roles' => 'required',
          'recharge_plan_per_day_cost' => 'required',
        ],
        [
          'recharge_plan_name.required' => 'Name is required',
          'recharge_plan_type.required' => 'Type is required',
          'recharge_credit.required' => 'Credit is required',
          'recharge_amount.required' => 'Amount is required',
          'recharge_plan_validity.required' => 'Validity is required',
          'plan_roles.required' => 'plan role is required',
          'country_id.required' =>'Country is required',
          // 'currency.required' =>'Currency is required',
        ]); 
        $recharge_plans = new RechargePlan();
        $recharge_plans->recharge_plan_name = $request->recharge_plan_name;
        $recharge_plans->recharge_plan_type = $request->recharge_plan_type;
        $recharge_plans->recharge_plan_credit = $request->recharge_plan_credit;
        $recharge_plans->recharge_plan_amount = $request->recharge_plan_amount;
        $recharge_plans->recharge_plan_validity = $request->recharge_plan_validity;
        $recharge_plans->recharge_plan_rewards = $request->recharge_plan_rewards;
        $recharge_plans->country_id = $request->country_id;
        // $recharge_plans->currency = $request->currency;
        $recharge_plans->plan_roles = $request->input('plan_roles') ? $request->input('plan_roles') : [];
        $recharge_plans->recharge_plan_per_day_cost = $request->recharge_plan_per_day_cost;
        $recharge_plans->save();

        Alert::success('Recharge Plan', 'Added successfully');

        return redirect()->route('admin.recharge-plan.index')->with('message', 'Recharge Plan created successfully.');

      }      

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('recharge_plan_manage')) {
            return abort(401);
        }
        else{
          $plans = Plan::all()->pluck('name', 'id')->prepend('Please Select Current Plan', '');
          $recharge_plan = RechargePlan::findOrFail($id);
          $roles = Role::get()->pluck('name', 'name');
          $countries = DB::table("countries")->pluck("name","id");
          return view('admin.recharge-plan.edit', compact('plans','recharge_plan','roles','countries'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(!Gate::allows('recharge_plan_manage')){
            
            return abort(401);

        } else{ 
          
          request()->validate([
            'recharge_plan_name' => 'required',
            'recharge_plan_type' => 'required',
            'recharge_plan_credit' => 'required|min:1',
            'recharge_plan_amount' => 'required',
            'recharge_plan_validity' => 'required',
            'plan_roles' => 'required',
            'country_id' => 'required',
            // 'currency'=>'required',
            'recharge_plan_per_day_cost'=>'required',
          ],
          [
            'recharge_plan_name.required' => 'Name is required',
            'recharge_plan_type.required' => 'Type is required',
            'recharge_credit.required' => 'Credit is required',
            'recharge_amount.required' => 'Amount is required',
            'recharge_plan_validity.required' => 'Validity is required',
            'plan_roles.required' => 'plan role is required',
            'country_id.required' =>'Country is required',
            // 'currency.required' =>'Currency is required',
          ]); 

          $recharge_plan = RechargePlan::findOrFail($id);
          $recharge_plan->recharge_plan_name = $request->recharge_plan_name;
          $recharge_plan->recharge_plan_type = $request->recharge_plan_type;
          $recharge_plan->recharge_plan_credit = $request->recharge_plan_credit;
          $recharge_plan->recharge_plan_amount = $request->recharge_plan_amount;
          $recharge_plan->recharge_plan_validity = $request->recharge_plan_validity;
          $recharge_plan->recharge_plan_rewards = $request->recharge_plan_rewards;
          $recharge_plan->country_id = $request->country_id;
          // $recharge_plan->currency = $request->currency;
          $recharge_plan->plan_roles = $request->input('plan_roles') ? $request->input('plan_roles') : [];
          $recharge_plan->recharge_plan_per_day_cost = $request->recharge_plan_per_day_cost;
          $recharge_plan->save();

          Alert::success('Recharge Plan', 'Updated successfully');
          return redirect()->route('admin.recharge-plan.index')->with('message', 'Recharge Plan Updated successfully.');

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    
    public function deleteRechargePlan(Request $request, $id){

        if (! Gate::allows('recharge_plan_manage')) {
            return abort(401);
        }

        $recharge_plan = RechargePlan::findOrFail($id);
        if($recharge_plan->delete()){          
            Alert::success('Recharge Plan', 'Deleted successfully'); 
           return back()->with('message', 'Recharge Plan Deleted successfully.');
         }
          else{
              $error = "Something wrong! Please try later ";
              return back()->with('message', 'Something wrong! Please try later.');

         }
    }
    
    public function changeStatus(Request $request)
    { 
      $id = intval($request->id);
      $recharge_plan = RechargePlan::findOrFail($id);
      $recharge_plan->recharge_plan_status = intval($request->status);
      $recharge_plan->save();  
    }

}
