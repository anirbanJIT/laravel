<?php

namespace App\Http\Controllers\Admin;
use DB;
use App\Models\ComplaintFeedback;
use App\User;
use App\FCM;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
Use Illuminate\Support\Str;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\File;

class ComplaintFeedbackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // return redirect()->action('Admin\ComplaintFeedbackController@complaintFeedbackDetails');
        if (! Gate::allows('complaint_feedback')) {
            return abort(401);
        }
        
        $complain_index = DB::table('complaint_feedback as a')
        ->select('a.*',DB::raw("CONCAT(a.status,'_',a.id) AS cust_status"), 'b.name')
        ->join('users as b', 'a.user_id', '=', 'b.id')
        ->where('a.type', "=", 'complaint')->get();
        
        return view('admin.complaint-feedback.complaint-list', compact('complain_index'));
    }

    public function feedbackindex()
    {
        if (! Gate::allows('complaint_feedback')) {
            return abort(401);
        }
        
        $feedback_index = DB::table('complaint_feedback as a')
        ->select('a.*', 'b.name')
        ->join('users as b', 'a.user_id', '=', 'b.id')
        ->where('a.type', "=", 'feedback')->get();
        
        return view('admin.complaint-feedback.feedback-list', compact('feedback_index'));
    }
	
	public function create(){
		$users = User::all();
		 return view('admin.complaint-feedback.create',compact('users'));
	}
	
	public function store(Request $request){
        
        if (! Gate::allows('complaint_feedback')) {
            return abort(401);
        }
        $complaintfeedbacks = new ComplaintFeedback;
        $complaintfeedbacks->user_id = $request->name;
        $complaintfeedbacks->title = $request->name;
        $complaintfeedbacks->description = $request->description;
        $complaintfeedbacks->type = "complaint";
        //$complaintfeedbacks->comment = $request->input('comment');
        //print_r($complaintfeedbacks);exit;
        $complaintfeedbacks->save();
        
        return redirect(route('admin.complaint-feedback.index'));
    }
    
    public function complaintFeedbackDetails(Request $request) {
        if (! Gate::allows('complaint_feedback')) {
            return abort(401);
        }
        $complaintfeedbacks = ComplaintFeedback::orderBy('created_at', 'asc')->get();
        
        return view('admin.complaint-feedback.complaintFeedback-list', compact('complaintfeedbacks'));
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\ComplaintFeedback  $complaintfeedback
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! Gate::allows('complaint_feedback')) {
            return abort(401);
        }

        $complaintfeedback = ComplaintFeedback::select('complaint_feedback.*')->where('id',$id)->first();

        return view('admin.complaint-feedback.show', compact('complaintfeedback'));
    }
    
    public function update(Request $request,$id){
        
        if (! Gate::allows('complaint_feedback')) {
            return abort(401);
        }
        $complaintfeedbacks = ComplaintFeedback::find($id);
        $complaintfeedbacks->status = 'Solved';
        $complaintfeedbacks->comment = $request->input('comment');
        //print_r($complaintfeedbacks);exit;
        $complaintfeedbacks->save();
        
        return redirect(route('admin.complaint-feedback.index'));
    }

    public function change_staus(Request $request){
        
        if (! Gate::allows('complaint_feedback')) {
            return abort(401);
        }
		
        $complaintfeedbacks = ComplaintFeedback::find($request->input('status_id'));
		if($request->input('status') == "UnSolved"){
			$complaintfeedbacks->comment = $request->input('comment');
			$complaintfeedbacks->status = "Solved";
		}else{
			$complaintfeedbacks->status = $request->input('status');
		}
		
		$complaintfeedbacks->save();
		
        return redirect(route('admin.complaint-feedback.index'));
    }
	
	 public function changeStausNotification(Request $request){
        
        $notfication = $request->notfication;
		$sendaspopup = $request->sendaspopup;
		$status = $request->status;
		$complain_id = $request->complain_id;
		
        $complaintfeedbacks = ComplaintFeedback::find($complain_id);
		$complaintfeedbacks->status = $status;
		$complaintfeedbacks->save();
		
		if($sendaspopup == "on"){
			$user = User::where("id",$complaintfeedbacks->user_id)->first();
			$fcm = new FCM();
			$fcm->token = $user->fcm_token;
			$fcm->notification['title'] = 'Change Complaint Status';
			$fcm->notification['body'] = "Your Complaint is ".$status." of that ".$user->name." User.";
			$fcm->send();
		}else{
			$user = User::where("id",$complaintfeedbacks->user_id)->first();
			$fcm = new FCM();
			$fcm->token = $user->fcm_token;
			$fcm->notification['title'] = 'Change Complaint Status';
			$fcm->notification['body'] = "Your Complaint is ".$status." of that ".$user->name." User.";
			$fcm->send();
		}
		
        return redirect(route('admin.complaint-feedback.index'));
    }
}
