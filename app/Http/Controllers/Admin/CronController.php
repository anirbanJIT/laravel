<?php

namespace App\Http\Controllers\Admin;

use Spatie\Permission\Models\Permission;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Brand;
use App\Models\BrandType;
use App\Models\BrandModel;
use App\Models\BrandSubModel;
use App\Models\BrandModelScreenImage;
use App\Models\ModelScreenImage;
use App\Models\UserAddonPlans;
use App\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Str;
Use Alert;
Use File;
use Symfony\Component\HttpFoundation\Response;
use \Carbon\Carbon;
use \App\FCM;
use DB;

class CronController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {


        $users = User::where('parent_user_id', 0)
                ->get()
                ->toArray();

echo "<pre>";
print_r($users);
die;
        if (!empty($users)) {

            foreach ($users as $key => $user) {

                $basic = UserAddonPlans::select('*')
                        ->where('is_addon', 0)
                        ->where('user_id', $user['id'])
                        ->orderBy('id', 'desc')
                        ->first();

                if (!empty($basic)) {
                    $start_date = \Carbon\Carbon::parse(Carbon::now());
                    $end_date = \Carbon\Carbon::parse($basic->expire_date);
                    if ($end_date > $start_date) {
                        if ($user['id'] == 913) {
                            $different_days = $start_date->diffInDays($end_date);

                            $fcm = new FCM();
                            $fcm->token = $user['fcm_token'];
                            $fcm->notification['title'] = 'Plan Expiring Alert!';
                            $fcm->notification['body'] = "Your plan is expiring in $different_days days.";
                            $fcm->send();
                        }
                    }
                }
            }
        }
        echo "Cron run successfully!";
        die;
    }

    public function image() {


//        $brand_model_screen_data = DB::select('select * from brand_model_screen_images order by id limit 100')->chunk(500, function($rows) {
        DB::table('brand_model_screen_images')->orderBy('id')->chunk(100, function($rows) {
            $brand_model_screen_data = json_decode(json_encode($rows), true);
echo "<pre>";
print_r($brand_model_screen_data);
die;
            if (!empty($brand_model_screen_data)) {

                foreach ($brand_model_screen_data as $key => $v) {

                    $exist = ModelScreenImage::where('brand_model_screen_image_id', $v['id'])->first();
                    if (!empty($exist)) {
                        continue;
                    }
//                $download = DB::select('SELECT * FROM brand_types where id=' . $v['brand_type_id'])[0]->download;
                    $name = json_decode($v['name'], TRUE);
                    $name = $name === NULL ? [0 => ''] : $name;
                    $images = json_decode($v['image'], TRUE);

                    $blt_files = json_decode($v['blt_file'], TRUE);

                    $json_width = json_decode($v['width'], TRUE);
                    $width = $json_width === NULL ? [0 => ''] : $json_width;

                    $json_height = json_decode($v['height'], TRUE);
                    $height = $json_height === NULL ? [0 => ''] : $json_height;
                    $k = 0;

                    $images = ($images);
                    $brand_model_screen_data1 = [];
                    if (!empty($images)) {
                        $created_at = Carbon::now();
                        foreach ($images as $key => $value) {
                            $brand_model_screen_data1[$k]['brand_model_screen_image_id'] = $v['id'];
                            $brand_model_screen_data1[$k]['screen_name'] = (isset($name[$k]) && !empty($name[$k])) ? $name[$k] : strtok($value, '.') ?? '';
                            $brand_model_screen_data1[$k]['image_file'] = $value;
                            $brand_model_screen_data1[$k]['blt_file'] = isset($blt_files[$k]) ? $blt_files[$k] : "";
                            $brand_model_screen_data1[$k]['width'] = (isset($width[$k]) && !empty($width[$k])) ? $width[$k] : 0;
                            $brand_model_screen_data1[$k]['height'] = (isset($height[$k]) && !empty($height[$k])) ? $height[$k] : 0;
                            $brand_model_screen_data1[$k]['status'] = 1;
                            $brand_model_screen_data1[$k]['created_at'] = $created_at;
                            $brand_model_screen_data1[$k]['updated_at'] = $created_at;
                            $k++;
                        }

                        ModelScreenImage::insert($brand_model_screen_data1);
                    }
                    echo "<pre>";
                    print_r($v['id']);
//                die;
                }
            }
        });

        echo "Model screen images imported successfully!";
        die;
    }

    public function plans() {


        $users = User::where('parent_user_id', 0)->get()->toArray();
        echo "<pre>";
        print_r($users);
        die;
        if (!empty($users)) {

            foreach ($users as $key => $user) {

                if ($user['current_plan'] > 0 && $user['current_plan_id'] > 0) {
                    $plan['user_id'] = $user['id'];
                    $plan['plan_id'] = $user['current_plan'];
                    $plan['recharge_plan_id'] = $user['current_plan_id'];
                    $plan['start_date'] = NULL;
                    $plan['expire_date'] = $user['expire_date'];
                    $plan['is_addon'] = 0;
                    $plan['created_at'] = Carbon::now();
                    $plan['updated_at'] = Carbon::now();
                    UserAddonPlans::insert($plan);
                }
            }
        }
        echo "Plans imported successfully!";
        die;
    }

}
