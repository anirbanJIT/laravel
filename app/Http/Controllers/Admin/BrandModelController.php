<?php

namespace App\Http\Controllers\Admin;
use DB;
use App\Models\BrandType;
use App\Models\Brand;
use App\Models\BrandModel;
use App\Models\BrandSubModel;
use App\Models\BrandModelScreenImage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
Use Illuminate\Support\Str;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\File;
use Symfony\Component\HttpFoundation\Response;
Use Alert;

class BrandModelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		if (! Gate::allows('brand_models')) {
            return abort(401);
        }

        $brandModel = BrandModel::with('brandsTypes','brands')->orderBy('order_number','DESC')->orderBy('id','DESC')->get();

        return view('admin.models.index', compact('brandModel'));
    }
	
	public function modelsDetails($id){
		$brandModel = BrandModel::with('brandsTypes','brands')->where("id",$id)->first();

        return view('admin.models.show', compact('brandModel','id'));
	}
	
	

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if (! Gate::allows('brand_models')) {
            return abort(401);
        }
        $brand_id = $request->brand_id;
        $brand_types = BrandType::select('id', 'name')->where('status', 1)->get();
		$brands = Brand::select('id', 'name')->where('status', 1)->get();
        $selected_brand = Brand::select('id', 'name','brand_type_id')->where('id', $brand_id)->first();
		
        return view('admin.models.create', compact('brand_types','brands','selected_brand'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (! Gate::allows('brand_models')) {
            return abort(401);
        }else {
			/*Remove this validation for client request 10122020 'name' => 'required|unique:brand_models',*/
			$validated = $request->validate([
				'brand_type_id' => 'required',
				'brand_id' => 'required',
				'name' => 'required',
				'status' => 'required',
				'fileUpload' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
			]);
			$lastRecord = BrandModel::orderBy('position_order','DESC')->first();
			
			$model = new BrandModel;
			$model->brand_type_id = $request->brand_type_id;
			$model->brand_id = $request->brand_id;
			$model->name = $request->name;
			$model->slug = Str::slug($request->name);
			$model->cut_point = $request->cut_point ?? 0;
			$model->meta_title = $request->metaTitle;
			$model->meta_keywords = $request->metaKeyword;
			$model->meta_description = $request->metaDescription;
			if ($request->hasFile('fileUpload')) {
				ini_set('memory_limi', -1);
				$files = $request->file('fileUpload');
				$destinationPath = public_path('Admin/images/models');
				$profileImage = date('Y-m-d-His') . "." . $files->getClientOriginalExtension();
				$files->move($destinationPath, $profileImage);
				$model->image = $profileImage;
			}
			$model->status = $request->status;
			$model->position_order = isset($lastRecord->position_order) ? ($lastRecord->position_order + 1):1;
			$model->save();


            return redirect()->route("admin.models.show",$request->get_brand_id)->withMessage('Created successfully!');
		}
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BrandModel  $brandModel
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $brand = Brand::where("id",$id)->first();
		//dd($brand);
		$brandModel = BrandModel::with('brandsTypes','brands')->where("brand_id",$id)->orderBy('position_order','ASC')->select(DB::raw('CONCAT(id,"_",status) AS id_status'),'id','brand_type_id','brand_id','name','position_order','slug','image','status','cut_point','meta_title','meta_description','meta_keywords','order_number')->get();
		//dd($brandModel);
        return view('admin.models.index', compact('brandModel','brand','id'));
    }
	
	public function modelsOrderPosition(Request $request){
		$position_order = $request->position;
		$i=1;
		
		foreach($position_order as $k=>$v){
			BrandModel::where("id",$v)->update(["position_order" => $i++]);	
		}
		
		return redirect()->back()->with('message', 'Item Position Update.');
	}

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BrandModel  $brandModel
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		
        if (! Gate::allows('brand_models')) {
            return abort(401);
        }
        $brand_types = BrandType::select('id', 'name')->where('status', 1)->get();
		$brands = Brand::select('id', 'name')->where('status', 1)->get();
		$brandModel = BrandModel::FindOrFail($id);

        return view('admin.models.edit', compact('brandModel','brand_types','brands'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BrandModel  $brandModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (! Gate::allows('brand_models')) {
            return abort(401);
        } 

		$validator = Validator::make($request->all(), [
		'brand_type_id' => 'required',
		'brand_id' => 'required',
		'name' => 'required',
		'status' => 'required',
		'fileUpload' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
		]);
		if($validator->fails()) {
			return Redirect::back()->withInput()->withErrors($validator);
		}
		else {
            $model = BrandModel::find($id);
    		$model->brand_type_id = $request->brand_type_id;
    		$model->brand_id = $request->brand_id;
            $model->name = $request->name;
    		$model->slug = Str::slug($request->name);
    		//$model->cut_point = $request->cut_point;
            $model->meta_title = $request->metaTitle;
    		$model->meta_keywords = $request->metaKeyword;
    		$model->meta_description = $request->metaDescription;
            if ($request->hasFile('fileUpload')) {
                ini_set('memory_limi', -1);
                $files = $request->file('fileUpload');
                $destinationPath = public_path('Admin/images/models'); // upload path
                $profileImage = date('Y-m-d-His') . "." . $files->getClientOriginalExtension();
                $files->move($destinationPath, $profileImage);
                $model->image = $profileImage;
            }
    		$model->status = $request->status ?? 1;
            //$model->order_number = $request->order_number;
    		$model->save();

            return redirect()->route("admin.models.show",$request->get_brand_id)->withMessage('Updated successfully!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BrandModel  $brandModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(BrandModel $brandModel,$id)
    {
		
        if (! Gate::allows('brand_models')) {
            return abort(401);
        }
		
        /* $brandModel->delete();

        return redirect()->route('admin.models.index'); */
		$imageQry = BrandModel::where('id', $id)->first();
		
        if($imageQry->image) {
            $image_path = 'images/models/'.$imageQry->image; // upload pa   
            if(File::exists($image_path)) {
              File::delete($image_path);            
            }
        }
		if(isset($imageQry->position_order)){
			$updaterecords = BrandModel::where("position_order",'>',$imageQry->position_order)->orderBy("position_order",'ASC')->get();
			
			foreach($updaterecords as $row){
				$update = BrandModel::where("id",$row->id)->update(["position_order"=>($row->position_order-1)]);
			} 
		}
		
        if($imageQry->delete()){
          BrandSubModel::where('brand_models_id', $id)->delete();
          BrandModelScreenImage::where('model_id', $id)->delete();
        }
        return redirect()->back()->withMessage('Deleted successfully!');;
    }
	
    public function deleteModel($id)
    {
		if (! Gate::allows('brand_models')) {
            return abort(401);
        }
        $imageQry = BrandModel::where('id', $id)->first();
        if($imageQry->image) {
            $image_path = 'images/models/'.$imageQry->image; // upload pa   
            if(File::exists($image_path)) {
              File::delete($image_path);            
            }
        }
        if($imageQry->delete()){
          BrandSubModel::where('brand_models_id', $id)->delete();
          BrandModelScreenImage::where('model_id', $id)->delete();
        }
        return redirect()->route('admin.models.index')->withMessage('Deleted successfully!');;
    }
	
	public function massDestroy(Request $request)
    {
        if (! Gate::allows('brand_models')) {
            return abort(401);
        }
        $ids = $request->ids;
        BrandModel::whereIn('id', explode(",",$ids))->delete();
        BrandSubModel::whereIn('brand_models_id', explode(",",$ids))->delete();
        BrandModelScreenImage::whereIn('model_id', explode(",",$ids))->delete();
        return response()->json(['success'=>'Deleted successfully']);
    }
	
	public function getBrandTypes(Request $request)
	{  
        $brand_type = Brand::select('id', 'name')->where('brand_type_id','=',$request->brand_type)->where('status','1')->whereNull('deleted_at')->get();
		return $brand_type;
	}

    public function getModelList(Request $request)
    {  
        $brand_type = BrandModel::select('id', 'name')->where('brand_id','=',$request->brand_models_id)->where('status','1')->whereNull('deleted_at')->get();
        return $brand_type;
    }
	
	public function changeStatus(Request $request)
	{
		$model = BrandModel::find($request->model_id);
        $model->status = $request->status;
        $model->save();
        return response()->json(['success'=>'Status change successfully.']);
	}
}
