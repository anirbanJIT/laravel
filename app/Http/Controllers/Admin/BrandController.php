<?php

namespace App\Http\Controllers\Admin;

use Spatie\Permission\Models\Permission;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Brand;
use App\Models\BrandType;
use App\Models\BrandModel;
use App\Models\BrandSubModel;
use App\Models\BrandModelScreenImage;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Str;
Use Alert;
Use DB ;
Use File;
use Symfony\Component\HttpFoundation\Response;

class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		return redirect()->action('Admin\BrandController@show');
        if (! Gate::allows('brands_manage')) {
            return abort(401);
        }
     
		/* $brands = Brand::join('brand_types', 'brand_types.id', '=', 'brands.brand_type_id')
                        ->select('brands.*', 'brand_types.name as BrandCatName') 
                        ->orderBy('brands.order_number','DESC')->orderBy('brands.id','DESC')->get(); */
		//dd($brands);
        return view('admin.brands.index', compact('brands'));
    }
	
	public function brandDetails($id) {
		if (! Gate::allows('brands_manage')) {
            return abort(401);
        }
		
		$brands = Brand::join('brand_types', 'brand_types.id', '=', 'brands.brand_type_id')
				->where("brands.id",$id)
                ->select('brands.*', 'brand_types.name as brand_type_name','brand_types.id as brand_type_id') 
                ->orderBy('brands.position_order','ASC')->first();
        //$brands = Brand::where('id',$id)->orderBy('position_order','ASC')->get();
        //dd($brands);
		return view('admin.brands.show', compact('brands'));
    }
	
	

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if (! Gate::allows('brands_manage')) {
            return abort(401);
        }
        $brandtype_id = isset($request->id) ? $request->id : 0;
        $brandTypes = BrandType::orderBy('id', 'DESC')->where('status', 1)->get();
        return view('admin.brands.create', compact('brandTypes','brandtype_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		if(!Gate::allows('brands_manage')){
           return abort(401);
		}else{
        	$validated = $request->validate([
				'name' => 'required|min:3',
				'brandType' => 'required'
			],[
				'name.required' => 'The name field is required.',
				'brandType.required' => 'The category field is required.',
			]); 
		  
			$lastRecord = Brand::orderBy('position_order','DESC')->first();
			$brands = new Brand();
        	if(!empty($request->brandImg)){
                $brandImage = time().'.'.$request->brandImg->getClientOriginalExtension();
                $request->brandImg->move(public_path('Admin/images/brands'),$brandImage);
            }else{
                $brandImage = '';
			}
            $brands->name = $request->name;
			$brands->brand_type_id = $request->brandType;
			$brands->status = '1';
			$brands->slug = Str::slug($request->name, '-');                     
			$brands->meta_title = $request->metaTitle;
			$brands->meta_description = $request->metaDescription; 
			$brands->meta_keywords = $request->metaKeyword;
			$brands->image = $brandImage;
			$brands->position_order = isset($lastRecord->position_order) ? ($lastRecord->position_order + 1):1;
			$brands->save();
            Alert::success('Brand Type', 'Added successfully');
			return redirect()->route("admin.brands.show",$request->get_brand_type_id)->with('message', 'Brand Type created successfully.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($brandtype_id)
    {
	
        if (! Gate::allows('brands_manage')) {
            return abort(401);
        }
		if($brandtype_id){
			$brand_type_name = BrandType::where("id",$brandtype_id)->first();
			$brands = Brand::where("brand_type_id",$brandtype_id)->orderBy('position_order','ASC')->select(DB::raw('CONCAT(id,"_",status) AS id_status'),'id','brand_type_id','name','position_order','slug','image','status','meta_title','meta_description','meta_keywords','order_number')->get();

			return view('admin.brands.brand-list', compact('brands','brandtype_id'));
		}else{
			return redirect()->action('Admin\BrandTypeController@brandTypeDetails');
		}
    }
	
	public function brandOrderPosition(Request $request){
		$position_order = $request->position;
		$i=1;
		
		foreach($position_order as $k=>$v){
			$brand = Brand::where("id",$v)->update(["position_order" => $i++]);	
		}
		
		return redirect()->back()->with('message', 'Category created successfully.');
	}

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('brands_manage')) {
            return abort(401);
        }
        else{
            $brandTypes = BrandType::orderBy('id', 'DESC')->where('status', 1)->get();
            $brand = Brand::findOrFail($id);
            return view('admin.brands.edit', compact('brand', 'brandTypes'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(!Gate::allows('brands_manage')){
            return abort(401);
        }else{
        	$validated = $request->validate([
				'name' => 'required|min:3',
				'brandType' => 'required'
			],[
				'name.required' => 'The name field is required.',
				'brandType.required' => 'The category field is required.',
			]);
            $brand = Brand::findOrFail($id);
            if($request->brandImg != Null){
                $brandImg = time().'.'.$request->brandImg->getClientOriginalExtension();
                $request->brandImg->move(public_path('Admin/images/brands'), $brandImg);
                $brand->image = $brandImg;

            }else{
                $brand->image = $request->imageH;
            }
			$brand->name = $request->name;
            $brand->brand_type_id = $request->brandType;
            $brand->status = '1';
            $brand->slug = Str::slug($request->name,'-');
            $brand->meta_title = $request->metaTitle;
            $brand->meta_description = $request->metaDescription;
            $brand->meta_keywords = $request->metaKeyword;            
            $brand->save();
            Alert::success('Brand', 'Updated successfully');
            
            return redirect()->route('admin.brands.show',$request->get_brand_type_id)->with('message', 'Brand Type created successfully.');
		}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        if(!Gate::allows('brands_manage')){
            return abort(401);
        }
        $imageQry = Brand::where('id','=',$id)->first();
        /*if($imageQry->image) {
            $image_path = 'images/brands/'.$imageQry->image; // upload pa   
            if(File::exists($image_path)) {
              File::delete($image_path);            
            }
        }*/
		
		if(isset($imageQry->position_order)){
			$updaterecords = Brand::where("position_order",'>',$imageQry->position_order)->orderBy("position_order",'ASC')->get();
			
			foreach($updaterecords as $row){
				$update = Brand::where("id",$row->id)->update(["position_order"=>($row->position_order-1)]);
			} 
		}
		
        if($imageQry->delete()){
          BrandModel::where('brand_id', $id)->delete();
          BrandSubModel::where('brand_id', $id)->delete();
          BrandModelScreenImage::where('brand_id', $id)->delete();
        }
        return redirect()->back()->withMessage('Deleted successfully!');
    }
    public function deleteBrand(Request $request, $id){
        if(!Gate::allows('brands_manage')){
            return abort(401);
        }
        $imageQry = Brand::where('id','=',$id)->first();
        /*if($imageQry->image) {
            $image_path = 'images/brands/'.$imageQry->image; // upload pa   
            if(File::exists($image_path)) {
              File::delete($image_path);            
            }
        }*/
        if($imageQry->delete()){
          BrandModel::where('brand_id', $id)->delete();
          BrandSubModel::where('brand_id', $id)->delete();
          BrandModelScreenImage::where('brand_id', $id)->delete();
        }
        return redirect()->route('admin.brands.index')->withMessage('Deleted successfully!');
    }

    public function massDestroy(Request $request)
    {
      if(!Gate::allows('brands_manage')){
          return abort(401);
      }
      $ids = $request->ids;
      Brand::whereIn('id', explode(",",$ids))->delete();
      BrandModel::whereIn('brand_id', explode(",",$ids))->delete();
      BrandSubModel::whereIn('brand_id', explode(",",$ids))->delete();
      BrandModelScreenImage::whereIn('brand_id', explode(",",$ids))->delete();
      return response()->json(['success'=>'Deleted successfully']);
    }
    
    public function changeStatus(Request $request)
    {       
      $brand = Brand::find($request->brand_id);
      $brand->status = $request->status;
      $brand->save();
    }

}
