<?php
/*
* @narenthaker create 07072021 
*/
namespace App\Http\Controllers\Admin;
use App\User;

use App\Models\BrandType;
use App\Models\Brand;
use App\Models\BrandModel;
use App\Models\BrandSubModel;
use App\Models\BrandModelScreenImage;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Spatie\Permission\Models\Role;
use App\Models\UserContactDetail;
use App\Models\UserBankDetail;
use App\Models\KycCategory;
use App\Models\UserKycDetail;
use App\Models\BrandModelScreenRetailer;
use App\Models\ShopDetail;
use App\Models\AccountRecharge;
use App\Models\CreditWalletTransaction;
use App\Models\CashWalletTransaction;
use App\Models\RechargeCommission;
use App\Models\RechargePlan;
use App\Models\Customer;
use App\Models\CreditWallet;
use App\Models\UserLevel;
use App\Models\CashWallet;
use App\Models\Plan;
use App\Models\UserPlanDetail;
use App\Models\UserImeiChangeRequest;
use DB;
use Illuminate\Support\Str;
Use Alert;
use Datatables;
use App\Models\UserProfileChangeRequest;
use App\Notifications\KYCNotification;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;
use Symfony\Component\HttpFoundation\Response;
use \Carbon\Carbon;

class StaffController extends Controller
{
    public function staffsDetails(Request $request)
    {
        $countries = DB::table("countries")->pluck("name","id");
        // $retailerUsers = User::with('roles')->role('retailer')->get();
        $retailerUsers = User::get();
        $status = $request->status ?? 2;
        return view('admin.staff.staff-list', compact('countries','retailerUsers','status'));
    }


    public function staffDetailsReport(Request $request)
    {


       
        $usersQuery = User::where('type',2)->with('getKycDetail');
  
       

        $id = $request->input('id');
        $name = $request->input('name');
        $phone = $request->input('phone');
        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');
        $status = $request->input('status');
        $parent_user_id = $request->input('parent_user_id');

        if( isset($id) && !empty($id) ) {
            $usersQuery->where('id','like',"{$id}%");
        }

        if( isset($name) && !empty($name) ) {
            $usersQuery->where('name','like',"{$name}%");
        }

        if( isset($phone) && !empty($phone) ) {
            $usersQuery->where('phone',$phone);
        }

        if( isset($from_date) && !empty($from_date) ) {
            $from_date = \Carbon\Carbon::parse($from_date." 00:00:00")->format('Y-m-d H:i:s');
            $to_date = \Carbon\Carbon::parse($to_date." 00:00:00")->format('Y-m-d H:i:s');
            $usersQuery->whereBetween(DB::raw('DATE(created_at)'),[$from_date, $to_date]);  
        }

        if( isset($status) && $status != "" ) {
            $usersQuery->where('status',$status);
        }

        if( isset($parent_user_id) && !empty($parent_user_id) ) {
            $usersQuery->where('parent_user_id',$parent_user_id);
        }

        $users = $usersQuery->select('*')->orderBy('id','desc');

        return datatables()->of($users)->editColumn('created_at', function ($user) {
           return [
              'display' => e($user->created_at->format('d M Y')),
              'timestamp' => $user->created_at->timestamp
          ];

        })->filterColumn('created_at', function ($query, $keyword) {

            $query->whereRaw("DATE_FORMAT(created_at,'%d %M %Y') LIKE ?", ["%$keyword%"]);

        })->addColumn('status', function ($users) {

            if (isset($users->getKycDetail) && $users->getKycDetail->status === 1){
                return '<div class="modal fade change_status_modal" id="change_status_'.$users->id.'" role="dialog" aria-labelledby="change_status_'.$users->id.'" aria-hidden="true" data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content" style="padding:20px">
                <div class="modal-header">
                <h5 class="modal-title" id="change_status_'.$users->id.'"">Are you sure you want to Unapproved?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
                </div>
                <form action="'.url('admin/kyc/adminChangeStatus').'" method="post">
                <div class="modal-body">
                <input type="hidden" name="id" value="'.$users->id.'">
                <input type="hidden" name="status" value="0">
                <input type="hidden" name="_token" value="'.csrf_token().'">
                <div class="row"> 
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-group mg-t-10"> 
                <div class="nk-int-st">
                <input type="text" class="form-control" value="" placeholder="Notes*" name="notes" required>
                </div>
                </div> 
                </div>
                </div>
                </div>
                <div class="modal-footer">
                <button  type="submit" class="btn btn-success notika-btn-success waves-effect">Yes, Change It</button>
                </div>
                </form>
                </div>
                </div>
                </div>
                <a  type="button" class="btn btn-orange orange-icon-notika btn-reco-mg btn-button-mg waves-effect" data-toggle="modal" href="#change_status_'.$users->id.'" title="Active"><i class="notika-icon notika-checked"></i></a>';
            } else {
                return '<div class="modal fade change_status_modal" id="change_status_'.$users->id.'" role="dialog" aria-labelledby="change_status_'.$users->id.'" aria-hidden="true" data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content" style="padding:20px">
                <div class="modal-header">
                <h5 class="modal-title" id="change_status_'.$users->id.'"">Are you sure you want to Approved?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
                </div>
                <form action="'.url('admin/kyc/adminChangeStatus').'" method="post">
                <div class="modal-body">
                <input type="hidden" name="id" value="'.$users->id.'">
                <input type="hidden" name="status" value="1">
                <input type="hidden" name="_token" value="'.csrf_token().'">
                <div class="row"> 
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-group mg-t-10"> 
                <div class="nk-int-st">
                <input type="text" class="form-control" value="" placeholder="Notes*" name="notes" required>
                </div>
                </div> 
                </div>
                </div>
                </div>
                <div class="modal-footer">
                <button  type="submit" class="btn btn-success notika-btn-success waves-effect">Yes, Change It</button>
                </div>
                </form>
                </div>
                </div>
                </div>
                <a  type="button" class="btn btn-orange orange-icon-notika btn-reco-mg btn-button-mg waves-effect" data-toggle="modal" href="#change_status_'.$users->id.'" title="Inactive"><i class="notika-icon notika-close"></i></a>';
            }

        })->addColumn('allAction', function ($user) {

           return '<a href="'.route('admin.staffs.show', $user->id).'"><button class="btn btn-info info-icon-notika btn-reco-mg btn-button-mg waves-effect" title="Show"><i class="notika-icon notika-menu"></i></button></a>';
        }) ->addColumn('delete_chk', function ($user) {
            return  '<input type="checkbox" class="sub_chk" value="'.$user->id.'" name="delete_chk[]" data-id="'.$user->id.'">';
        })  

        ->addColumn('request', function ($user) {
    
            if (isset($user->getKycDetail) && $user->getKycDetail->status_change_to == "active"){
                  return '<button class="stt btn btn-success" title="Request" style="    background-color: #5cb85c;
    border-color: #4cae4c;">Active</button>';
            } else {
                     return '<button class="stt1 btn btn-danger" title="Request">InActive</button>';
            }
          /* return '<a href="'.route('admin.staffs.show', $user->id).'"><button class="btn btn-info info-icon-notika btn-reco-mg btn-button-mg waves-effect" title="Show"><i class="notika-icon notika-menu"></i></button></a>';*/
        }) 



         ->rawColumns(['status','delete_chk','allAction','request'])->make(true);

    }

    public function show($id)
    {

        if (! Gate::allows('users_manage')) {
            return abort(401);
        }      
        $staff = User::has('getKycDetail')->findOrFail($id);

        $data['status'] = isset($staff->getKycDetail->status) ? $staff->getKycDetail->status : 0;
        $data['notes'] = isset($staff->getKycDetail->notes) ? $staff->getKycDetail->notes : 'N/A';
        $data['current_location'] = isset($staff->getKycDetail->current_location) ? $staff->getKycDetail->current_location : '';
        $data['lat_lan'] = isset($staff->getKycDetail->lat_lan) ? $staff->getKycDetail->lat_lan : '';
        $data['shop_visiting_card'] = isset($staff->getKycDetail->shop_visiting_card) ? $staff->getKycDetail->shop_visiting_card : '';
        $data['shop_pic_with_selfie'] = isset($staff->getKycDetail->shop_pic_with_selfie) ? $staff->getKycDetail->shop_pic_with_selfie : '';

        return view('admin.staff.show', compact('staff','data'));
    }
    public function retailerlist(){
        $retailerlist = DB::select('SELECT u.name as username,u.id as userid,count(*) as design from brand_model_screen_retailers as mr JOIN users as u ON mr.upload_by = u.id group by mr.upload_by');
        //print_r($brandModelScreen);exit;
        return view('admin.screen_upload_list.retailerlist', compact('retailerlist'));
    }
    public function screenuploadlist($userid){
        $brandModelScreen = DB::select('SELECT mr.*,u.name as username from brand_model_screen_retailers as mr JOIN users as u ON mr.upload_by = u.id where mr.upload_by ='.$userid);
        //print_r($brandModelScreen);exit;
        return view('admin.screen_upload_list.index', compact('brandModelScreen'));
    }
    public function viewuploadlistbyretailer($id){
        $brandModelScreen = DB::select('SELECT mr.*,u.name as username from brand_model_screen_retailers as mr JOIN users as u ON mr.upload_by = u.id where mr.id ='.$id);
        //print_r($brandModelScreen);exit;
        return view('admin.screen_upload_list.show', compact('brandModelScreen'));
    }
    public function changeStatusModelScreenofretailer(Request $request){
        
        $model = BrandModelScreenRetailer::find($request->modelScreen_id);

        $model->status = $request->status;

        $model->save();
        return response()->json(['success'=>'Status change successfully.']);
    }
}
