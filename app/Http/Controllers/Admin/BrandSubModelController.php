<?php

namespace App\Http\Controllers\Admin;
use DB;
use App\Models\BrandType;
use App\Models\Brand;
use App\Models\BrandModel;
use App\Models\BrandSubModel;
use App\Models\BrandModelScreenImage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
Use Illuminate\Support\Str;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\File;
use Symfony\Component\HttpFoundation\Response;
Use Alert;

class BrandSubModelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		if (! Gate::allows('brand_sub_models')) {
            return abort(401);
        }

        $brand_sub_models = BrandSubModel::with('brandsModels','brandsTypes','brands')->orderBy('order_number','DESC')->orderBy('id','DESC')->get();

        return view('admin.submodels.index', compact('brand_sub_models'));
    }
	
	public function submodelDetails($id){
		$brandSubModel = BrandSubModel::with('brandsModels','brandsTypes','brands')->where("id",$id)->first();

        return view('admin.submodels.show', compact('brandSubModel','id'));
	}
	

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if (! Gate::allows('brand_sub_models')) {
            return abort(401);
        }
        $model_id = isset($request->model_id) ? $request->model_id :0;
        $brand_types = BrandType::select('id', 'name')->where('status', 1)->get();
        $brands = Brand::select('id', 'name')->where('status', 1)->get();
        $models = BrandModel::select('id', 'name')->where('status', 1)->get();

        $selected_model = BrandModel::select('id', 'name','brand_id','brand_type_id')->where('id', $model_id)->first();

        return view('admin.submodels.create', compact('brand_types','brands','models','selected_model'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (! Gate::allows('brand_sub_models')) {
            return abort(401);
        }

        $request->validate([
            'brand_type_id' => 'required',
            'brand_id' => 'required',
            'brand_models_id' => 'required',
            'name' => 'required',
            'status' => 'required',
            'fileUpload' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        
		$lastRecord = BrandSubModel::orderBy('position_order','DESC')->first();
			
        $submodel = new BrandSubModel;
        $submodel->brand_type_id = $request->brand_type_id;
        $submodel->brand_id = $request->brand_id;
        $submodel->brand_models_id = $request->brand_models_id;
        $submodel->name = $request->name;
        $submodel->slug = Str::slug($request->name);
        $submodel->cut_point = $request->cut_point ??0;
        $submodel->meta_title = $request->metaTitle;
        $submodel->meta_keywords = $request->metaKeyword;
        $submodel->meta_description = $request->metaDescription;
        if ($request->hasFile('fileUpload')) {
            ini_set('memory_limi', -1);
            $files = $request->file('fileUpload');
            $destinationPath = public_path('Admin/images/submodels/'); // upload path
            $profileImage = date('Y-m-d-His') . "." . $files->getClientOriginalExtension();
            $files->move($destinationPath, $profileImage);
            $submodel->image = $profileImage;
        }
        $submodel->status = $request->status ?? 1;
        $submodel->position_order = isset($lastRecord->position_order) ? ($lastRecord->position_order + 1):1;
        $submodel->save();
		
        
        return redirect()->route("admin.submodels.show",$request->get_model_id)->withMessage('Created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BrandModel  $brandModel
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! Gate::allows('brand_sub_models')) {
            return abort(401);
        }
		$model_id = $id;
		$brand_models = BrandModel::where("id",$model_id)->first();
		$brand_sub_models = BrandSubModel::with('brandsModels','brandsTypes','brands')->where("brand_models_id",$id)->orderBy('position_order','ASC')->select(DB::raw('CONCAT(id,"_",status) AS id_status'),'id','brand_type_id','brand_id','brand_models_id','name','position_order','slug','image','status','cut_point','meta_title','meta_description','meta_keywords','order_number')->get();

        return view('admin.submodels.index', compact('brand_sub_models','brand_models','model_id'));

    }
	
	public function submodelsOrderPosition(Request $request){
		$position_order = $request->position;
		$i=1;
		
		foreach($position_order as $k=>$v){
			BrandSubModel::where("id",$v)->update(["position_order" => $i++]);	
		}
		
		return redirect()->back()->with('message', 'Item Position Update.');
	}

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BrandModel  $brandModel
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        if (! Gate::allows('brand_sub_models')) {
            return abort(401);
        }
        $brand_types = BrandType::select('id', 'name')->where('status', 1)->get();
        $brands = Brand::select('id', 'name')->where('status', 1)->get();
        $models = BrandModel::select('id', 'name')->where('status', 1)->get();
        $brand_sub_model = BrandSubModel::FindOrFail($id);

        return view('admin.submodels.edit', compact('brand_sub_model','models','brand_types','brands'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BrandModel  $brandModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (! Gate::allows('brand_sub_models')) {
            return abort(401);
        }
        
        $request->validate([
            'brand_type_id' => 'required',
            'brand_id' => 'required',
            'brand_models_id' => 'required',
            'name' => 'required',
            'status' => 'required',
            'fileUpload' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $brand_sub_model = BrandSubModel::find($id);
        $brand_sub_model->brand_type_id = $request->brand_type_id;
        $brand_sub_model->brand_id = $request->brand_id;
        $brand_sub_model->brand_models_id = $request->brand_models_id;
        $brand_sub_model->name = $request->name;
        $brand_sub_model->slug = Str::slug($request->name);
        $brand_sub_model->cut_point = $request->cut_point;
        $brand_sub_model->meta_title = $request->metaTitle;
        $brand_sub_model->meta_keywords = $request->metaKeyword;
        $brand_sub_model->meta_description = $request->metaDescription;
        if ($request->hasFile('fileUpload')) {
            ini_set('memory_limi', -1);
            $files = $request->file('fileUpload');
            $destinationPath = public_path('Admin/images/submodels'); // upload path
            $profileImage = date('Y-m-d-His') . "." . $files->getClientOriginalExtension();
            $files->move($destinationPath, $profileImage);
            $brand_sub_model->image = $profileImage;
        }
        $brand_sub_model->status = $request->status ?? 1;        
        $brand_sub_model->order_number = $request->order_number;
        $brand_sub_model->save();
		
        
        return redirect()->route("admin.submodels.show",$request->get_model_id)->withMessage('Updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BrandModel  $brandModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(BrandSubModel $brand_sub_model,$id)
    {        
        /* if (! Gate::allows('brand_sub_models')) {
            return abort(401);
        }
        $brand_sub_model->delete();
        return redirect()->route('admin.submodels.index'); */
		if (! Gate::allows('brand_sub_models')) {
            return abort(401);
        }
        $imageQry = BrandSubModel::where('id', $id)->first();        
        if($imageQry->image) {
            $image_path = 'images/submodels/'.$imageQry->image; // upload pa   
            if(File::exists($image_path)) {
                File::delete($image_path);            
            }
        }  
		if(isset($imageQry->position_order)){
			$updaterecords = BrandSubModel::where("position_order",'>',$imageQry->position_order)->orderBy("position_order",'ASC')->get();
			
			foreach($updaterecords as $row){
				$update = BrandSubModel::where("id",$row->id)->update(["position_order"=>($row->position_order-1)]);
			} 
		}
        if($imageQry->delete()){
          BrandModelScreenImage::where('sub_model_id', $id)->delete();
        }
		return redirect()->back()->withMessage('Deleted successfully!');;
    }
    public function deleteSubModel($id)
    {        
        if (! Gate::allows('brand_sub_models')) {
            return abort(401);
        }
        $imageQry = BrandSubModel::where('id', $id)->first();        
        if($imageQry->image) {
            $image_path = 'images/submodels/'.$imageQry->image; // upload pa   
            if(File::exists($image_path)) {
                File::delete($image_path);            
            }
        }        
        if($imageQry->delete()){
          BrandModelScreenImage::where('sub_model_id', $id)->delete();
        }
        return redirect()->route('admin.submodels.index')->withMessage('Deleted successfully!');;
    }
    
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('brand_sub_models')) {
            return abort(401);
        }
        $ids = $request->ids;
        BrandSubModel::whereIn('id', explode(",",$ids))->delete();
        BrandModelScreenImage::whereIn('sub_model_id', explode(",",$ids))->delete();
        return response()->json(['success'=>'Deleted successfully']);
    }

    public function changeStatus(Request $request)
    {
        $brand_sub_model = BrandSubModel::find($request->submodel_id);
        $brand_sub_model->status = $request->status;
        $brand_sub_model->save();
        return response()->json(['success'=>'Status change successfully.']);
    }

    public function getBrandTypes(Request $request)
    {   
        $BrandSubModel = BrandSubModel::select('id', 'name')->where('brand_models_id',$request->brand_models_id)->where('status','1')->whereNull('deleted_at')->get();
        return $BrandSubModel;
    }
    
    
}
