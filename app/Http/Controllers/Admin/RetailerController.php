<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Spatie\Permission\Models\Role;
use App\Models\UserContactDetail;
use App\Models\UserBankDetail;
use App\Models\KycCategory;
use App\Models\UserKycDetail;
use App\Models\ShopDetail;
use App\Models\AccountRecharge;
use App\Models\CreditWalletTransaction;
use App\Models\CashWalletTransaction;
use App\Models\RechargeCommission;
use App\Models\RechargePlan;
use App\Models\Customer;
use App\Models\CreditWallet;
use App\Models\UserLevel;
use App\Models\CashWallet;
use App\Models\Plan;
use App\Models\UserPlanDetail;
use App\Models\UserImeiChangeRequest;
use App\Models\PlanSerialNumbers;
use App\Models\UserAddonPlans;
use App\Models\MachineNumbers;
use DB;
use Redirect;
use Illuminate\Support\Str;
Use Alert;
use Datatables;
use App\Models\UserProfileChangeRequest;
use App\Notifications\KYCNotification;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;
use Symfony\Component\HttpFoundation\Response;
use \Carbon\Carbon;

class RetailerController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return redirect()->action('Admin\RetailerController@retailerDetails');
        $retailerUsers = User::with('roles')->role('retailer')->get();
        return view('admin.retailers.index', compact('retailerUsers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        if (!Gate::allows('retailer_manage')) {
            return abort(401);
        }

        $roles = Role::get()->pluck('name', 'name');

        $countries = DB::table("countries")->pluck("name", "id");
        $kycCategory = KycCategory::orderBy('id', 'DESC')->get();

        return view('admin.retailers.create-retailer', compact('roles', 'countries', 'kycCategory'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        if (!Gate::allows('retailer_manage')) {
            return abort(401);
        }

        request()->validate([
            'shop_name' => 'required',
            'name' => 'required',
            'phone' => 'required|max:15|unique:users',
            'email' => 'nullable|string|email|max:255|unique:users',
            'password' => 'required|min:6',
            'address' => 'required',
            'country' => 'required',
            'state' => 'required',
            'city' => 'required',
            'pin_code' => 'required',
                ], [
            'shop_name.required' => 'Shop name is required',
            'name.required' => 'Name is required',
            'phone.required' => 'Mobile Number is required',
            'password.required' => 'Password is required',
            'password.min' => 'Password must be at least 6 characters',
            'address.required' => 'Address is required',
            'country.required' => 'Country is required',
            'state.required' => 'State is required',
            'city.required' => 'City is required',
            'pin_code.required' => 'Pin Code is required',
        ]);

        if ($request->country != 101 && empty($request->email)) {
            return redirect()->back()->with('message', 'Email id is required');
        }

        $user = User::create(['referral_id' => $request->referral_id,
                    'name' => $request->name,
                    'phone' => $request->phone,
                    'other_phone' => $request->other_phone,
                    'shop_name' => $request->shop_name,
                    'email' => $request->email,
                    'password' => $request->password,
                    'type' => 1,
                    'otp_status' => 1
        ]);

        $roles = $request->input('roles') ? $request->input('roles') : [];
        $user->assignRole($roles);

        $contact_details = ['user_id' => $user->id,
            'address' => $request->address,
            'city' => $request->city,
            'state' => $request->state,
            'country' => $request->country,
            'pin_code' => $request->pin_code
        ];

        $userContactDetail = $user->getContactDetail()->first();

        if (!empty($userContactDetail)) {
            $user = $userContactDetail->update($contact_details);
        } else {
            $user = UserContactDetail::create($contact_details);
        }

        CashWallet::create(['user_id' => $user->id, 'amount' => '0']);
        CreditWallet::create(['user_id' => $user->id, 'amount' => '0']);


        return redirect()->route('admin.retailers.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        if (!Gate::allows('retailer_manage')) {
            return abort(401);
        }

        $retailer = User::findOrFail($id);

        $retailerContact = DB::table('user_contact_details')
                ->leftJoin('countries', 'user_contact_details.country', '=', 'countries.id')
                ->leftJoin('states', 'user_contact_details.state', '=', 'states.id')
                ->select('user_contact_details.*', 'countries.name as country', 'states.name as state')
                ->where('user_contact_details.user_id', $retailer->id)
                ->first();

        $retailerBank = UserBankDetail::where('user_id', $retailer->id)->first();

        $retailerKyc = DB::table('user_kyc_details')
                ->leftJoin('kyc_categories', 'user_kyc_details.kyc_id', '=', 'kyc_categories.id')
                ->leftJoin('countries', 'user_kyc_details.country_id', '=', 'countries.id')
                ->leftJoin('states', 'user_kyc_details.state_id', '=', 'states.id')
                ->select('user_kyc_details.*', 'kyc_categories.name as catName', 'countries.name as countryName', 'states.name as stateName')
                ->where('user_kyc_details.user_id', $retailer->id)
                ->first();

        $retailerShop = ShopDetail::where('user_id', $retailer->id)->first();

        //dd($retailerKyc);           
        // dd($retailerContact);
        $retailer->load('roles');
        //dd($user->load('roles'));

        return view('admin.retailers.show', compact('retailer', 'retailerContact', 'retailerBank', 'retailerKyc', 'retailerShop'));
    }

    public function addShopGallery(Request $request) {

        if (!Gate::allows('retailer_manage')) {
            return abort(401);
        }

        if ($request->file('shop_gallery')) {
            $k = 1;
            foreach ($request->file('shop_gallery') as $file) {
                $destinationPath = public_path('images/shops'); // upload path
                $profileImage = time() . $k . '.' . $file->extension();

                $file->move($destinationPath, $profileImage);
                $dataImage[] = $profileImage;
                $k++;
            }
        }

        $model = ShopDetail::find($request->id);

        if ($model->shop_gallery) {
            $arr = json_decode($model->shop_gallery, TRUE);
            $arr = array_merge($arr, $dataImage);
        } else {
            $arr = $dataImage;
        }
        if ($request->file('shop_gallery')) {
            $model->shop_gallery = json_encode($arr, JSON_FORCE_OBJECT);
        }
        $model->save();
        return redirect()->back()->withMessage('Image uploaded successfully!');
    }

    public function deleteShopGallery(Request $request) {

        if (!Gate::allows('retailer_manage')) {
            return abort(401);
        }

        $retailerShop = ShopDetail::where('id', $request->id)->first();
        if ($retailerShop->shop_gallery) {
            $myArray = json_decode($retailerShop->shop_gallery);
            foreach ($myArray as $key => $value) {
                if ($value == $request->shop_gallery) {
                    $image_path = public_path('images/shops/') . $value; // upload pa   
                    //dd($image_path);
                    if (File::exists($image_path)) {
                        unset($myArray->$key);
                        File::delete($image_path);
                        $model = ShopDetail::find($request->id);
                        $model->shop_gallery = json_encode($myArray);
                        $model->save();
                    }
                }
            }
        }
        return redirect()->back()->withMessage('Deleted successfully!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        if (!Gate::allows('retailer_manage')) {
            return abort(401);
        }
        $plans = Plan::all()->pluck('name', 'id')->prepend('Please Select Current Plan', '');
        $addons = Plan::all()->pluck('name', 'id')->prepend('Please Select Addon Plan', '');
        $user = User::where('id', $id)->first();
        $countries = DB::table("countries")->pluck("name", "id");
        $kycCategory = KycCategory::orderBy('id', 'DESC')->get();
        $currentPlans = DB::table("recharge_plans")->pluck("recharge_plan_name", "id");
//        $currentaddons = $user->getAddonPlans()->where('is_addon',1)->get()->toArray();
        $userSerialNumber = $user->getSerialNumber()->first();
        $machine_numbers = MachineNumbers::leftJoin('user_kyc_details as ky', 'ky.machine_number', '=', 'machine_numbers.machine_number')
                        ->where(function ($query) use ($id) {
                            $query->where('ky.id', NULL)
                            ->orWhere('ky.user_id', '=', $id);
                        })->pluck('machine_numbers.machine_number');

        $allplans = Plan::where('plan_status', 1)->get();

        if (!empty($allplans)) {
            foreach ($allplans as $key => $plan) {
                $activerechargePlans = UserAddonPlans::where('plan_id', $plan->id)
                        ->where('user_id', $id)
//                        ->where('expire_date', '>', Carbon::now())
                        ->orderBy('id', 'DESC')
                        ->first();
                $plan['activerechargePlan'] = $activerechargePlans->recharge_plan_id ?? 0;
                $plan['rechargePlans'] = $plan->RechargePlans()->get();
                $plan['activePlanKey'] = PlanSerialNumbers::where('plan_id', $plan->id)->where('user_id', $id)->value('id');
                $plan['serialNumbers'] = $plan->PlanSerialNumbers()->where(function ($query) use ($id) {
                            $query->where('user_id', NULL)
                                    ->orWhere('user_id', $id);
                        })->get();
                $plan['expired'] = 0;
                if (!empty($activerechargePlans)) {
                    if ($activerechargePlans->expire_date < Carbon::now()) {
                        $plan['expired'] = 1;
                    }
                }
                $allplans[$key] = $plan;
            }
        }

        return view('admin.retailers.edit', compact('user', 'countries', 'kycCategory', 'plans', 'currentPlans', 'addons', 'userSerialNumber', 'machine_numbers', 'allplans'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $userId) {
        if (!Gate::allows('retailer_manage')) {
            return abort(401);
        }

        request()->validate([
            'shop_name' => 'required',
            'name' => 'required',
            'phone' => 'required',
            'address' => 'required',
            'country' => 'required',
            'country' => 'required',
            'state' => 'required',
            'city' => 'required',
            'pin_code' => 'required',
                ], [
            'shop_name.required' => 'Shop name is required',
            'name.required' => 'Name is required',
            'phone.required' => 'Mobile Number is required',
            'address.required' => 'Address is required',
            'country.required' => 'Country is required',
            'state.required' => 'State is required',
            'city.required' => 'City is required',
            'pin_code.required' => 'Pin Code is required',
        ]);

        if ($request->country != 101 && empty($request->email)) {
            return redirect()->back()->with('message', 'Email id is required');
        }

        $user = User::findOrFail($userId);

        $user = $user->update(['name' => $request->name,
            'phone' => $request->phone,
            'other_phone' => $request->other_phone,
            'shop_name' => $request->shop_name,
            'email' => $request->email
        ]);

        $contact_details = ['address' => $request->address,
            'city' => $request->city,
            'state' => $request->state,
            'country' => $request->country,
            'pin_code' => $request->pin_code
        ];

        $user = User::findOrFail($userId);
        $userContactDetail = $user->getContactDetail()->first();

        if (!empty($userContactDetail)) {
            $user = $userContactDetail->update($contact_details);
        } else {
            $user = UserContactDetail::create($contact_details);
        }

        if ($user) {

            return redirect()->back()->with('message', 'Contact Information updated successfully');
        } else {
            return redirect()->back() - with('message', 'Something went wrong');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        if (!Gate::allows('retailer_manage')) {
            return abort(401);
        }
        $kyc = User::find($id);
        if ($kyc->delete()) {
            Alert::success('Retailer', 'Deleted successfully');
            return back()->with('message', 'Retailer Deleted successfully.');
        } else {
            $error = "Something wrong! Please try later ";
            //redirect('admin/news-and-events')->with('message', $message);      
            return back()->with('message', 'Something wrong! Please try later.');
        }
    }

    public function retailerChangePassword(User $user, Request $request) {

        if (!Gate::allows('retailer_manage')) {
            return abort(401);
        }

        if ($request->getMethod() == 'POST') {

            request()->validate([
                'password' => 'required|min:6',
                    ], [
                'password.required' => 'Password is required',
                'password.min' => 'Password must be at least 6 characters',
            ]);

            $user->password = Hash::make($request->password);
            $user->save();
            Alert::success('Retailer', 'Password Change successfully');
            return redirect()->back()->with('message', 'Password Change successfully.');
        } else {
            $error = "Something wrong! Please try later ";
            return back()->with('message', 'Something wrong! Please try later.');
        }
    }

    public function approveKyc(Request $request) {
        $kyc = UserKycDetail::find($request->kyc_id);
        print_r($request);
        exit;
        if ($request->status == 1) {
            $kyc->status = $request->status;
            $kyc->notes = $request->notes ?? null;
            $kycStatus = $kyc->save();
            if ($kycStatus) {
                $user = User::find($kyc->user_id);
                $user->status = '1';
                $user->save();
            }
            $type = "Approved";
            $user->notify(new KYCNotification($user, $type));
            Alert::success('KYC Approved and User Activated !');
            return response()->json(['success' => 'Approved']);
            //return back()->with('message', 'KYC Approved and User Activated.');
        } else {
            $kyc->status = $request->status;
            $kyc->notes = $request->notes ?? null;
            $kycStatus = $kyc->save();
            if ($kycStatus) {
                $user = User::find($kyc->user_id);
                $user->status = '0';
                $user->save();
            }
            $type = "Unapproved";
            $user->notify(new KYCNotification($user, $type));
            Alert::success('KYC Unapproved and User Deactivated !');
            //return back()->with('message', 'KYC Unapproved and User Deactivated.');  
            return response()->json(['success' => 'Unapproved']);
        }
    }

    /*
     * @narenthaker approveKycAdmin add notes fields $request->notes 07072021 
     */

    public function approveKycAdmin(Request $request) {
        $kyc = UserKycDetail::where('user_id', $request->id)->first();
        if (empty($kyc)) {
            $kyc = new UserKycDetail();
            $kyc->kyc_number = 'approved by admin';
            $kyc->user_id = $request->id;
        }
        if ($request->status == 1) {
            $kyc->status = 1;
            $kyc->notes = $request->notes ?? null;
            if ($request->has('machine_number') && $request->has('machine_price') && $request->has('machine_purchase_date')) {
                $kyc->machine_number = $request->machine_number;
                $kyc->machine_price = $request->machine_price;
                $kyc->machine_purchase_date = $request->machine_purchase_date ? Carbon::parse($request->machine_purchase_date)->format('Y-m-d') : null;
            }
            $kycStatus = $kyc->save();


            $expiry_date = date('Y-12-31 23:59:59');
            if ($kycStatus) {
                $user = User::where('id', $request->id)->first();
                if ($request->has('current_plan') && $request->has('current_plan_id')) {
//                    $validity = RechargePlan::select('*')->where('id', $request->current_plan_id)->get()->toArray();
//                    $planvalidity = $validity[0]['recharge_plan_validity'];
//                    $expiry_date = date('Y-m-d', strtotime(' + ' . $planvalidity . 'days'));
                    $user->expire_date = $expiry_date;
                    $user->current_plan = $request->current_plan;
                    $user->current_plan_id = $request->current_plan_id;

                    $addonData['user_id'] = $request->id;
                    $addonData['plan_id'] = $request->current_plan;
                    $addonData['recharge_plan_id'] = $request->current_plan_id;
                    $addonData['start_date'] = Carbon::now();
                    $addonData['expire_date'] = $expiry_date;
                    $addonData['is_addon'] = 0;
                    UserAddonPlans::Create($addonData);
                }
                $user->status = 1;
                $user->save();
            }
            if ($request->has('serial_number_id')) {
                PlanSerialNumbers::where('id', $request->serial_number_id)->update(['user_id' => $request->id]);
            }
            if ($request->recharge_plans && !empty($request->recharge_plans)) {
                foreach ($request->recharge_plans as $key => $value) {
                    if ($value > 0) {
                        $rech_plan = RechargePlan::select('*')->where('id', $value)->first();

                        $planvalidity = $rech_plan->recharge_plan_validity;
                        $expiry_date = date('Y-m-d', strtotime(' + ' . $planvalidity . 'days'));
                        $addonData['user_id'] = $request->id;
                        $addonData['plan_id'] = $rech_plan->recharge_plan_type;
                        $addonData['recharge_plan_id'] = $value;
                        $addonData['start_date'] = Carbon::now();
                        $addonData['expire_date'] = $expiry_date;
                        $addonData['is_addon'] = 1;
                        UserAddonPlans::Create($addonData);
                    }
                }
            }

            $type = "Approved";
            //$user->notify(new KYCNotification($user,$type));
            Alert::success('KYC Approved and User Activated !');
            return back()->with('message', 'KYC Approved and User Activated.');
        } elseif ($request->status == 0) {
            $kyc->status = 0;
            $kyc->notes = $request->notes ?? null;
            $kycStatus = $kyc->save();
            if ($kycStatus) {
                $user = User::where('id', $request->id)->first();
                $user->status = 0;
                $user->save();
            }
            $type = "Unapproved";
            //$user->notify(new KYCNotification($user,$type));
            Alert::success('KYC Unapproved and User Deactivated !');
            return back()->with('message', 'KYC Unapproved and User Deactivated.');
        }
    }

    public function changeCurrentPlan(Request $request, $id) {

        request()->validate([
//            'current_plan' => 'required',
            'user_plan_light_to_light_discount' => 'nullable|numeric|between:0,99.99',
            'user_plan_light_to_main_discount' => 'nullable|numeric|between:0,99.99',
            'user_plan_main_to_main_discount' => 'nullable|numeric|between:0,99.99',
            'user_plan_main_to_light_discount' => 'nullable|numeric|between:0,99.99',
                ], [
            'current_plan.required' => 'Please select current plan',
        ]);
        //print_r($request->all());exit;
        $user = User::where('id', $request->user_id)->first();

        $recharge_plan = RechargePlan::where('id', $request->current_plan_id)->first();

        if (!empty($recharge_plan) && !empty($user)) {
            $ex_date = date('Y-m-d', strtotime(' + ' . $recharge_plan->recharge_plan_validity . 'days'));
            $user->current_plan_id = $request->current_plan_id;
            $user->current_plan = $request->current_plan;
            $roles = $recharge_plan->roles;
            $user->expire_date = $ex_date;
            $user->syncRoles($roles);
            $user->save();

            $basic = UserAddonPlans::select('*')
                    ->where('is_addon', 0)
                    ->where('user_id', $request->user_id)
                    ->orderBy('id', 'desc')
                    ->first();

            if (!empty($basic)) {
                $expire_date = \Carbon\Carbon::parse($basic->expire_date);
                $start_date = \Carbon\Carbon::parse(Carbon::now());
                $expire_year = \Carbon\Carbon::parse($basic->expire_date)->format('Y');
                $next_year = $expire_year + 1;
                $end_date = \Carbon\Carbon::parse('31-12-' . $expire_year . ' 23:59:59');
                if (\Carbon\Carbon::parse($basic->expire_date)->format('m-d') == '12-31') {
                    $start_date = \Carbon\Carbon::parse('1-1-' . $next_year . '00:00:00');
                    $end_date = \Carbon\Carbon::parse('31-12-' . $next_year . ' 23:59:59');
                } else {
                    $start_date = $expire_date->addDays(1);
                }
            } else {
                $start_date = \Carbon\Carbon::parse(Carbon::now());
                $year = Carbon::now()->format('Y');
                $end_date = \Carbon\Carbon::parse('31-12-' . $year . ' 23:59:59');
            }

            $addonData['user_id'] = $request->user_id;
            $addonData['plan_id'] = $request->current_plan;
            $addonData['recharge_plan_id'] = $request->current_plan_id;
            $addonData['start_date'] = $start_date;
            $addonData['expire_date'] = $end_date;
            $addonData['is_addon'] = 0;
            UserAddonPlans::Create($addonData);

            $user_plan_existe = UserPlanDetail::where('user_id', $user->id)->first();
            $input_all = $request->all();
            $input_all['user_plan_id'] = $request->current_plan;
            if (empty($user_plan_existe)) {
                $user_plan_existe = UserPlanDetail::create($input_all);
            } else {
                $user_plan_existe = $user_plan_existe->update($input_all);
            }
        }

            if ($request->has('serial_number_id')) {
                PlanSerialNumbers::where('id', $request->serial_number_id)->update(['user_id' => $request->user_id]);
            }
            if ($request->recharge_plans_key && !empty($request->recharge_plans_key)) {
                foreach ($request->recharge_plans_key as $key => $value) {
                    PlanSerialNumbers::where('id', $value)->update(['user_id' => $request->user_id]);
                }
            }
            if ($request->recharge_plans && !empty($request->recharge_plans)) {
                foreach ($request->recharge_plans as $key => $value) {
                    if ($value > 0) {
                        $rech_plan = RechargePlan::select('*')->where('id', $value)->first();

                        $planvalidity = $rech_plan->recharge_plan_validity;
                        $expiry_date = date('Y-m-d', strtotime(' + ' . $planvalidity . 'days'));


                        $basic = UserAddonPlans::select('*')
//                        ->where('is_addon', 0)
                                ->where('user_id', $request->user_id)
                                ->where('plan_id', $rech_plan['recharge_plan_type'])
                                ->orderBy('id', 'desc')
                                ->first();

                        if (!empty($basic)) {
                            $expire_date = \Carbon\Carbon::parse($basic->expire_date);
                            $start_date = \Carbon\Carbon::parse(Carbon::now());
                            $expire_year = \Carbon\Carbon::parse($basic->expire_date)->format('Y');
                            $next_year = $expire_year + 1;
                            $end_date = \Carbon\Carbon::parse('31-12-' . $expire_year . ' 23:59:59');
                            if (\Carbon\Carbon::parse($basic->expire_date)->format('m-d') == '12-31') {
                                $start_date = \Carbon\Carbon::parse('1-1-' . $next_year . '00:00:00');
                                $end_date = \Carbon\Carbon::parse('31-12-' . $next_year . ' 23:59:59');
                            } else {
                                $start_date = $expire_date->addDays(1);
                            }
                        } else {
                            $start_date = \Carbon\Carbon::parse(Carbon::now());
                            $year = Carbon::now()->format('Y');
                            $end_date = \Carbon\Carbon::parse('31-12-' . $year . ' 23:59:59');
                        }


                        $addonData['user_id'] = $request->user_id;
                        $addonData['plan_id'] = $rech_plan->recharge_plan_type;
                        $addonData['recharge_plan_id'] = $value;
                        $addonData['start_date'] = $start_date;
                        $addonData['expire_date'] = $end_date;
                        $addonData['is_addon'] = 1;
                        UserAddonPlans::Create($addonData);
                    }
                }
            }

            Alert::success('Plan updated successfully.');
            return back()->with('message', 'Plan updated successfully.');
        
    }

    public function showDirectTree() {
        return view('admin.retailers.direct-tree');
    }

    public function showDirectUserList() {


        $referred_by = (!empty($_GET["referred_by"])) ? ($_GET["referred_by"]) : ('1');
        if ($referred_by) {
            $usersReferral = User::where('id', $referred_by)->orWhere('email', $referred_by)->select('referral_id')->first();
        } else {
            $usersReferral = User::where('id', $referred_by)->orWhere('email', $referred_by)->select('referral_id')->first();
        }


        if ($usersReferral->referral_id) {

            $referred_by = $usersReferral->referral_id;


            $usersQuery = User::query();

            $usersQuery->where('referred_by', $referred_by);
        }
        $users = $usersQuery->select('*')->orderBy('id', 'desc');

        //dd($users);
        return datatables()->of($users)->editColumn('created_at', function ($user) {
                            return [
                                'display' => e($user->created_at->format('d M Y')),
                                'timestamp' => $user->created_at->timestamp
                            ];
                        })->filterColumn('created_at', function ($query, $keyword) {
                            $query->whereRaw("DATE_FORMAT(created_at,'%d %M %Y') LIKE ?", ["%$keyword%"]);
                        })->addColumn('status', function (User $user) {
                            return $user->status;
                        })
                        ->addColumn('status', function ($user) {
                            if ($user->status == 1) {
                                return "Active";
                            } elseif ($user->status == 2) {
                                return "Expired";
                            } else {
                                return "Pending";
                            }
                        })->make(true);
    }

    public function usersMonthlyRecharge() {
        $retailerUsers = User::with('roles')->role('retailer')->get();
        return view('admin.retailers.monthly-recharge', compact('retailerUsers'));
    }

    public function monthlyRechargeReport() {
        if (!empty($_GET["user_id"])) {
            $user_id = $_GET["user_id"];
            $usersQuery = AccountRecharge::with('accountRechargeUser');
            $usersQuery->where('user_id', $user_id);
        } else {
            $usersQuery = AccountRecharge::with('accountRechargeUser');
        }

        if (!empty($_GET["status"])) {
            $status = request()->status;
            $usersQuery->where('status', $status);
        }

        if (!empty($_GET["payment_mode"])) {
            $payment_mode = request()->payment_mode;
            $usersQuery->where('payment_mode', $payment_mode);
        }

        if ($_GET["from_date"] != '' && $_GET["to_date"] != '' && $_GET["serach_date_by"] == 'expire_date') {
            $from = \Carbon\Carbon::parse($_GET["from_date"] . " 00:00:00")->format('Y-m-d H:i:s');
            $to = \Carbon\Carbon::parse($_GET["to_date"] . " 00:00:00")->format('Y-m-d H:i:s');
            $usersQuery->whereBetween(DB::raw('DATE(expire_date)'), [$from, $to]);
        }

        if ($_GET["from_date"] != '' && $_GET["to_date"] != '' && $_GET["serach_date_by"] == 'recharge_date') {
            $from = \Carbon\Carbon::parse($_GET["from_date"] . " 00:00:00")->format('Y-m-d H:i:s');
            $to = \Carbon\Carbon::parse($_GET["to_date"] . " 00:00:00")->format('Y-m-d H:i:s');
            $usersQuery->whereBetween(DB::raw('DATE(created_at)'), [$from, $to]);
        }

        $users = $usersQuery->select('*')->orderBy('id', 'desc');

        return datatables()->of($users)->editColumn('created_at', function ($user) {
                    return [
                        'display' => e($user->created_at->format('d M Y')),
                        'timestamp' => $user->created_at->timestamp
                    ];
                })->filterColumn('created_at', function ($query, $keyword) {
                    $query->whereRaw("DATE_FORMAT(created_at,'%d %M %Y') LIKE ?", ["%$keyword%"]);
                })->editColumn('expire_date', function ($user) {
                    return [
                        'display' => e($user->expire_date->format("d M Y")),
                        'timestamp' => $user->expire_date->timestamp
                    ];
                })->filterColumn('expire_date', function ($query, $keyword) {
                    $query->whereRaw("DATE_FORMAT(expire_date,'%d %M %Y') LIKE ?", ["%$keyword%"]);
                })->addColumn('user_id', function ($users) {
                    return $users->accountRechargeUser->name ?? '';
                })->addColumn('account_user_id', function ($users) {
                    return $users->accountRechargeUser->id ?? '';
                })->addColumn('status', function ($user) {
                    if ($user->status == 1) {
                        return "Paid";
                    } else {
                        return "Unpaid";
                    }
                })->addColumn('payment_mode', function ($user) {
                    if ($user->payment_mode == 1) {
                        return "Credit Wallet";
                    } elseif ($user->payment_mode == 2) {
                        return "Cashfree";
                    } else {
                        return "None";
                    }
                })->addColumn('recharge_duration', function ($user) {
                    return $user->recharge_duration . " Month";
                })->make(true);
    }

    public function usersCreditPurchase() {
        return view('admin.retailers.credit-purchase-report');
    }

    public function creditPurchaseReport() {
        if (!empty($_GET["user_id"])) {
            $user_id = $_GET["user_id"];
            $usersQuery = CreditWalletTransaction::with('creditReceiver');
            $usersQuery->where([['receiver_id', $user_id], ['type', 'Credit Purchase']]);
            $users = $usersQuery->select('*')->orderBy('id', 'desc');
        } else {
            $usersQuery = CreditWalletTransaction::with('creditReceiver');
            $usersQuery->where('type', 'Credit Purchase');
            $users = $usersQuery->select('*')->orderBy('id', 'desc');
        }


        //dd($users);
        return datatables()->of($users)->editColumn('created_at', function ($user) {
                    return [
                        'display' => e($user->created_at->format('d M Y')),
                        'timestamp' => $user->created_at->timestamp
                    ];
                })->filterColumn('created_at', function ($query, $keyword) {
                    $query->whereRaw("DATE_FORMAT(created_at,'%d %M %Y') LIKE ?", ["%$keyword%"]);
                })->addColumn('receiver_id', function ($users) {
                    return $users->creditReceiver->name;
                })->addColumn('status', function ($user) {
                    if ($user->status == 1) {
                        return "Paid";
                    } else {
                        return "Unpaid";
                    }
                })->make(true);
    }

    public function rechargeCommission() {
        return view('admin.retailers.recharge-commission');
    }

    public function rechargeCommissionReport() {


        $user_id = (!empty($_GET["user_id"])) ? ($_GET["user_id"]) : ('1');
        if ($user_id) {
            $usersId = User::where('id', $user_id)->orWhere('email', $user_id)->select('id')->first();
        } else {
            $usersId = User::where('id', $user_id)->orWhere('email', $user_id)->select('id')->first();
        }


        if ($usersId->id) {

            $user_id = $usersId->id;


            $usersQuery = RechargeCommission::with('incomeReceiver', 'creditPurchaser');

            $usersQuery->where('user_id', $user_id)->where('status', 0);
        }
        $users = $usersQuery->select('*')->orderBy('id', 'desc');


        //dd($users);
        return datatables()->of($users)->editColumn('created_at', function ($user) {
                    return [
                        'display' => e($user->created_at->format('d M Y')),
                        'timestamp' => $user->created_at->timestamp
                    ];
                })->filterColumn('created_at', function ($query, $keyword) {
                    $query->whereRaw("DATE_FORMAT(created_at,'%d %M %Y') LIKE ?", ["%$keyword%"]);
                })->addColumn('user_id', function ($users) {
                    return $users->incomeReceiver->name ?? '';
                })->addColumn('sender_id', function ($users) {
                    return $users->creditPurchaser->name ?? '';
                })->addColumn('status', function ($user) {
                    if ($user->status == 1) {
                        return "Paid";
                    } else {
                        return "Unpaid";
                    }
                })->make(true);
    }

    public function customerRechargeCommission() {
        $retailerUsers = User::with('roles')->role('retailer')->get();

        $data = DB::table('fix_commission')->first();
        $data1 = DB::table('fix_reward')->first();

        return view('admin.retailers.customer-recharge-commission', compact('retailerUsers', 'data', 'data1'));
    }

    public function fixcommissionupdate(Request $request) {


        $data = DB::table('fix_commission')->where('id', $request->id)->update(array('commission' => $request->commission));


        return redirect('admin/customer-recharge-commission')->withMessage('Updated commission successfully!');
    }

    public function fixrewardupdate(Request $request) {


        $data = DB::table('fix_reward')->where('id', $request->id)->update(array('reward' => $request->commission));


        return redirect('admin/customer-recharge-commission')->withMessage('Updated reward successfully!');
    }

    /* customer Recharge Commission NT26022121 */

    public function customerRechargeCommissionReport(Request $request) {
        $usersQuery = RechargeCommission::with('incomeReceiver', 'commisionPaid', 'commisionUnpaid')->where('user_id', '!=', 1);

        $user = $usersQuery->select('user_id')->orderBy('user_id', 'desc')->distinct('user_id');
        //print_r($user);exit;
        return datatables()->of($user)->addColumn('user_id', function ($user) {
                            $name = $user->incomeReceiver->name ?? 'N/A';
                            $url = url('admin/recharge-commission-payout?user_id=' . $user->user_id);
                            return '<a href="' . $url . '">' . $name . '</a>';
                            //return $user->incomeReceiver->name ?? 'User Deleted';
                        })->editColumn('status', function ($user) {
                            if (isset($user->commisionUnpaid->unpaid) && $user->commisionUnpaid->unpaid > 0) {
                                return "Unpaid";
                            } else {
                                return "Paid";
                            }
                        })->addColumn('paid', function ($user) {
                            return $user->commisionPaid->paid ?? 0;
                        })
                        ->addColumn('unpaid', function ($user) {
                            return $user->commisionUnpaid->unpaid ?? 0;
                        })->addColumn('total', function ($user) {
                    return ($user->commisionPaid->paid ?? 0) + ($user->commisionUnpaid->unpaid ?? 0);
                })->filter(function ($query) {
                    if (request()->has('status') && !empty(request('status'))) {
                        $query->where('status', request('status'));
                    }
                    if (request()->has('user_id') && !empty(request('user_id'))) {
                        $query->where('user_id', request('user_id'));
                    }
                    if (request()->has('to_date') && !empty(request('to_date')) && request()->has('from_date') && !empty(request('from_date'))) {
                        $start = date("Y-m-d", strtotime(request('from_date')));
                        $end = date("Y-m-d", strtotime(request('to_date') . "+1 day"));
                        $query->whereBetween('created_at', [$start, $end]);
                    }
                })->addColumn('checkbox', function ($user) {
                    return '<input type="checkbox" name="ids[]" class="pdr_checkbox" value="' . $user->user_id . '" />
            <input type="hidden" name="retailer_id[]" value="' . $user->user_id . '" />';
                })->rawColumns(['checkbox', 'user_id'])->make(true);
    }

    public function creditWalletTransaction() {
        return view('admin.retailers.credit-wallet-transaction');
    }

    public function creditWalletReport() {


        /* $user_id = (!empty($_GET["user_id"])) ? ($_GET["user_id"]) : ('1');
          if($user_id){
          $usersId = User::where('id',$user_id)->orWhere('email',$user_id)->select('id')->first();
          }
          else
          {
          $usersId = User::where('id',$user_id)->orWhere('email',$user_id)->select('id')->first();
          } */


        if (!empty($_GET["user_id"])) {
            $user_id = empty($_GET["user_id"]);
            $usersQuery = CreditWalletTransaction::with('creditReceiver', 'creditSender');
            $usersQuery->where('receiver_id', $user_id);
            $users = $usersQuery->select('*')->orderBy('id', 'desc');
        } else {
            $usersQuery = CreditWalletTransaction::with('creditReceiver', 'creditSender');
            $users = $usersQuery->select('*')->orderBy('id', 'desc');
        }
        
        //dd($users);die();
        //
        
        
        $data =  datatables()->of($users)->editColumn('created_at', function ($user) 
        {
            return ['display' => e($user->created_at->format('d M Y')),'timestamp' => $user->created_at->timestamp]; 
        })->filterColumn('created_at', function ($query, $keyword) {
            $query->whereRaw("DATE_FORMAT(created_at,'%d %M %Y') LIKE ?", ["%$keyword%"]);
        })->addColumn('receiver_id', function ($users) {
            return $users->creditReceiver->name ?? '';
        })->addColumn('sender_id', function ($users) {
            return $users->creditSender->name ?? '';
        })->addColumn('status', function ($user) {
            if ($user->status == 1) { return "Paid"; } else { return "Unpaid"; }
        })->make(true);

        return view('admin.retailers.credit-wallet-transaction', compact('data'));
    }

    public function cashWalletTransaction() {
        return view('admin.retailers.cash-wallet-transaction');
    }

    public function cashWalletReport() {


        $user_id = (!empty($_GET["user_id"])) ? ($_GET["user_id"]) : ('1');
        if ($user_id) {
            $usersId = User::where('id', $user_id)->orWhere('email', $user_id)->select('id')->first();
        } else {
            $usersId = User::where('id', $user_id)->orWhere('email', $user_id)->select('id')->first();
        }


        if ($usersId->id) {

            $user_id = $usersId->id;


            $usersQuery = CashWalletTransaction::with('cashReceiver', 'cashSender');

            $usersQuery->where('receiver_id', $user_id);
        }
        $users = $usersQuery->select('*')->orderBy('id', 'desc');


        //dd($users);
        return datatables()->of($users)->editColumn('created_at', function ($user) {
                    return [
                        'display' => e($user->created_at->format('d M Y')),
                        'timestamp' => $user->created_at->timestamp
                    ];
                })->filterColumn('created_at', function ($query, $keyword) {
                    $query->whereRaw("DATE_FORMAT(created_at,'%d %M %Y') LIKE ?", ["%$keyword%"]);
                })->addColumn('receiver_id', function ($users) {
                    return $users->cashReceiver->name;
                })->addColumn('sender_id', function ($users) {
                    return $users->cashSender->name;
                })->addColumn('status', function ($user) {
                    if ($user->status == 1) {
                        return "Paid";
                    } else {
                        return "Unpaid";
                    }
                })->make(true);
    }

    public function showRetailerWiseCustomer() {
        $retailers = User::join('customers', 'users.id', '=', 'customers.retailer_id')
                ->selectRaw('users.id, users.name, users.email, users.phone, sum(customers.amount) as amount, users.current_plan')
                ->groupBy('users.id', 'users.name', 'users.email', 'users.phone', 'customers.retailer_id', 'users.current_plan')
                ->get();
        return view('admin.retailers.customer-retailer-wise-list', compact('retailers'));
    }

    public function showCustomer($retailer_id = null) {
        $user = DB::SELECT('select customers.*,users.name as shopname,brand_models.name as brand_name,model_screen_images.screen_name,model_screen_images.brand_model_screen_image_id FROM customers 
            LEFT Join users On customers.retailer_id = users.id 
            LEFT Join brand_models ON customers.model_id = brand_models.id 
            LEFT Join model_screen_images ON customers.image_id = model_screen_images.id 
            order by customers.id DESC LIMIT 100');

        return view('admin.retailers.customer-list', compact('user'));
    }

    public function showCustomerList() {
        if (request()->is('admin/*')) {

            //admin show all buyer list
            $usersQuery = Customer::query();
            // $usersQuery->where('retailer_id',(!empty($_GET["retailer_id"])) ? ($_GET["retailer_id"]) : ('1'));
        } else {

            $referred_by = (!empty($_GET["referred_by"])) ? ($_GET["referred_by"]) : ('1');

            if ($referred_by) {
                $usersReferral = User::where('id', $referred_by)->orWhere('email', $referred_by)->select('id')->first();
            }

            if ($usersReferral->id) {

                $referred_by = $usersReferral->id;

                $usersQuery = Customer::query();

                $usersQuery->where('retailer_id', $referred_by);
            }
        }

        $users = $usersQuery->join('brand_models', 'customers.model_id', 'brand_models.id');
        $users = $usersQuery->join('users', 'customers.retailer_id', 'users.id');
        $users = $usersQuery->select('users.name as retailer_name', 'customers.*', 'brand_models.name as brand_model_name')->orderBy('id', 'desc');
        //dd($users);
        return datatables()->of($users)->editColumn('retailer_id', function ($users) {
                    return '<a class="btn btn-link" href="' . route('admin.retailers.edit', $users->retailer_id) . '">' . $users->retailer_name . '</a>';
                })->editColumn('brand_model_name', function ($users) {
                    return '<a class="btn btn-link" href="' . url('admin/models-screen-image', $users->model_id) . '">' . $users->brand_model_name . '</a>';
                })->editColumn('created_at', function ($user) {
                    return [
                        'display' => e($user->created_at->format('d M Y h:i:A')),
                        'timestamp' => $user->created_at->timestamp
                    ];
                })->filterColumn('created_at', function ($query, $keyword) {
                    $query->whereRaw("DATE_FORMAT(created_at,'%d %M %Y %H:%i') LIKE ?", ["%$keyword%"]);
                })->rawColumns(['retailer_id', 'brand_model_name'])->make(true);
    }

    public function showRetailerCreditBalance() {
		/* return view('admin.retailers.retailer-credit-balance');  */
		 
        $users = User::with('getCreditWalletBalance')->where('type', 1)->select('*')->orderBy('id', 'desc')->get();
		
        return view('admin.retailers.retailer-credit-balance-list',compact('users'));
    }

    public function showRetailerCreditBalanceList() 
    {
        $userDetail = '';
        $user_id = (!empty($_GET["user_id"])) ? ($_GET["user_id"]) : '';
        if ($user_id) {
            $userDetail = User::where('id', $user_id)->orWhere('email', $user_id)->select('id')->first();
        }
        $usersQuery = User::with('getCreditWalletBalance');
        if ($userDetail) {
            $user_id = $userDetail->id;
            $usersQuery->where('id', $user_id);
        } else {
            $usersQuery->where('type', 1);
        }
        $users = $usersQuery->select('*')->orderBy('id', 'desc');
		
        return datatables()->of($users)->editColumn('created_at', function ($user) {
        return [
            'display' => e($user->created_at->format('d M Y')),
            'timestamp' => $user->created_at->timestamp
        ];
        })->filterColumn('created_at', function ($query, $keyword) {
            $query->whereRaw("DATE_FORMAT(created_at,'%d %M %Y') LIKE ?", ["%$keyword%"]);
        })->addColumn('amount', function ($users) {
            return $users->getCreditWalletBalance->amount ?? 0;
        })->addColumn('status', function (User $user) {
            return $user->status;
        })
        ->addColumn('status', function ($user) {
        if ($user->status == 1) {
            return "Active";
        } elseif ($user->status == 2) {
            return "Expired";
        } else {
            return "Pending";
        }
        })->addColumn('Action', function ($users) {
        return '<a class="btn btn-success notika-btn-success waves-effect" href="' . route('admin.add-credit-point', $users->id) . '">Add Credit Balance </a>'; })->rawColumns(['Action', 'action'])->make(true);
    }

    public function addCreditPoint($id) {
        if (!Gate::allows('manage_credit_wallet')) {
            return abort(401);
        }
        $credit = CreditWallet::where('user_id', $id)->first();
        if (!$credit) {
            return redirect()->back()->withErrors('No wallet found')->withInput();
        }
        return view('admin.retailers.credit_purchase', compact('credit'));
    }

    public function addCreditsSummary(Request $request) {
        $validator = Validator::make($request->all(), [
                    'credit' => 'required|integer',
                    'credit_note' => 'required|string|max:255',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        } else {
            $creditWallet = new CreditWallet();
            $credit = $creditWallet->retailerCreditBalanceWithoutAuthUser($request->user_id);
            $credit_setting = $creditWallet->creditSetting();
            return view('admin.retailers.credit_purchase_summary', compact('request', 'credit_setting', 'credit'));
        }
    }

    public function purchaseCredits(Request $request) {
        $invoice_no = creditInvoiceNumber();
        $creditWallet = new CreditWallet();
        $balance = $creditWallet->retailerCreditBalanceWithoutAuthUser($request->user_id);
        if ($balance->amount > 0) {
            $balance_amount = $balance->amount + $request->credit_amt;
        } else {
            $balance_amount = $request->credit_amt;
        }

        $data = new CreditWalletTransaction;
        $data->invoice_no = $invoice_no;
        $data->receiver_id = $request->user_id;
        $data->sender_id = auth()->user()->id;
        $data->credit_amt = $request->credit_amt;
        $data->debit_amt = 0;
        $data->balance_amt = ($balance->amount + $request->credit_amt);
        $data->singal_credit_pur_amt = $request->singal_credit_pur_amt;
        $data->total_credit_pur_amt = $request->total_credit_pur_amt;
        $data->type = 'Manual Credit Add';
        $data->payment_mode = 'Manual';
        $data->status = 1;
        $data->transaction_description = 'You get ' . $request->credit_amt . ' credit points and pay for this ' . env('CURRENCY_SYMBOL') . ' ' . $request->total_credit_pur_amt;
        $data->transaction_note = $request->credit_note;

        if ($data->save()) {
            CreditWallet::where('user_id', $request->user_id)->update(['amount' => ($balance->amount + $request->credit_amt), 'credit_note' => $request->credit_note]);

            return redirect()->route('admin.add-credit-point', ['id' => $request->user_id])->with('message', 'Credit points added successfully in user credit wallet.');
        } else {

            return redirect()->route('admin.add-credit-point', ['id' => $request->user_id])->with('message', 'Credit points added successfully in user credit wallet.');
        }
    }

    public function showMemberTree() {
        abort_if(Gate::denies('manage_members_tree'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        
        //$users = User::where('referred_by','1234567')->where('type','1')->get();
        $users = User::get();
        return view('admin.retailers.show-member-tree', compact('users'));
    }

    public function showMemberTreeDetail($id) {
        abort_if(Gate::denies('manage_members_tree'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $categories = User::where('id', $id)->get();
        return view('admin.retailers.tree-view-retailers', compact('categories'));
    }

    public function commissionSlabTreeViewRetailers() {

        $categories = User::where([['referred_by', '=', 1234567], ['type', 1]])->get();
        return view('admin.retailers.commission-slab-tree-view-retailers', compact('categories'));
    }

    public function retailerDetails(Request $request) {
        if (!Gate::allows('retailer_manage')) {
            return Redirect::route('admin.home');
        }

        $countries = DB::table("countries")->pluck("name", "id");
        $plans = DB::table("plans")->where('plan_status','1')->pluck("name", "id");
        $machine_numbers = DB::table("machine_numbers")->pluck("machine_number", "id");

        $retailerUsers = User::with('roles')->role('retailer')->get();
        $status = $request->status ?? 2;
        return view('admin.retailers.retailers-list', compact('countries','retailerUsers', 'status','plans','machine_numbers'));
        
        
    }

    /*
     * @narenthaker status change 07072021 
     */

    public function retailerDetailsReport() 
    {
        
         $retailerUsers = User::where('type', '1')->get();
         return view('admin.retailers.retailers-list', compact('retailerUsers'));

        //return view('admin.retailers.retailers-list');
 
 //        $country = $_GET['country'];
 //        $kyc_number = $_GET['kyc_number'];
 //        if ($country != '' && $kyc_number != '') {
 //            $usersQuery = User::where('type', 1)->whereHas('getKycDetail', function($q) use($kyc_number) {
 //                        if ($kyc_number) {
 //                            $q->where('kyc_number', '=', $kyc_number);
 //                        }
 //                    })->whereHas('getContactDetail', function($q) use($country) {
 //                        if ($country) {
 //                            $q->where('country', '=', $country);
 //                        }
 //                    })->with('getKycDetail', 'getContactDetail');
 //        } else if ($country) {
 //            $usersQuery = User::where('type', 1)->WhereHas('getContactDetail', function($q) use($country) {
 //                        if ($country) {
 //                            $q->where('country', '=', $country);
 //                        }
 //                    })->with('getKycDetail', 'getContactDetail');
 //        } else if ($kyc_number) {
 //            $usersQuery = User::where('type', 1)->whereHas('getKycDetail', function($q) use($kyc_number) {
 //                        if ($kyc_number) {
 //                            $q->where('kyc_number', '=', $kyc_number);
 //                        }
 //                    })->with('getKycDetail', 'getContactDetail');
 //        } else {
 //            $usersQuery = User::where('type', 1)->with('getKycDetail', 'getContactDetail');
 //        }

 //        if ($_GET["id"] != '') {
 //            $id = $_GET["id"];
 //            $usersQuery->where('id', 'like', "{$id}%");
 //        }

 //        if ($_GET["name"] != '') {
 //            $name = $_GET["name"];
 //            $usersQuery->where('name', 'like', "{$name}%");
 //        }
 //        if ($_GET["phone"] != '') {
 //            $phone = $_GET["phone"];
 //            $usersQuery->where('phone', $phone);
 //        }
 //        if ($_GET["from_date"] != '' && $_GET["to_date"] != '') {
 //            $from = \Carbon\Carbon::parse($_GET["from_date"] . " 00:00:00")->format('Y-m-d H:i:s');
 //            $to = \Carbon\Carbon::parse($_GET["to_date"] . " 00:00:00")->format('Y-m-d H:i:s');
 //            $usersQuery->whereBetween(DB::raw('DATE(created_at)'), [$from, $to]);
 //        }
 //        if ($_GET["status"] != '') {
 //            $status = $_GET["status"];
 //            $usersQuery->where('status', $status);
 //        }

 //        if ($_GET["current_plan"] != '') {
 //            $current_plan = $_GET["current_plan"];
 //            $usersQuery->where('current_plan', $current_plan);
 //        }

 //        if ($_GET["referred_by"] != '') {
 //            $referred_by = $_GET["referred_by"];
 //            $usersQuery->where('referred_by', $referred_by);
 //        }



 //        $users = $usersQuery->select('*')->orderBy('id', 'desc');
 //        //dd($users);
 //        $allplans = Plan::where('plan_status', 1)->get();

 //        if (!empty($allplans)) {
 //            foreach ($allplans as $key => $plan) {

 //                $plan['rechargePlans'] = $plan->RechargePlans()->get();
 //                $allplans[$key] = $plan;
 //            }
 //        }

 //        $addonplans = '';
 //        if (!empty($allplans)) {
 //            foreach ($allplans as $k => $plan) {
 //                if ($plan['id'] != config('constants.plans.basic')) {
 //                    if (!empty($plan['rechargePlans'])) {
 //                        $addonplans .= '<div class="row">
 //                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
 //                            <div class="form-group">
 //                                <label>' . $plan['name'] . ' Addon Plans</label>
 //                                <select class="form-control" name="recharge_plans[]" id="">
 //                                    <option value="" ></option>';
 //                        foreach ($plan['rechargePlans'] as $k => $rechargeplan) {
 //                            $addonplans .= '<option value="' . $rechargeplan['id'] . '" >' . $rechargeplan['recharge_plan_name'] . '</option>';
 //                        }
 //                        $addonplans .= '</select>
 //                            </div>
 //                        </div>
 //                    </div>';
 //                    }
 //                }
 //            }
 //        }

 //        return datatables()->of($users, $allplans)->editColumn('created_at', function ($user) {
 //                            return [
 //                                'display' => e($user->created_at->format('d M Y')),
 //                                'timestamp' => $user->created_at->timestamp
 //                            ];
 //                        })->editColumn('current_plan', function ($user) {
 //                            return e(ucwords(str_replace('_', ' ', $user->current_plan)));
 //                        })->filterColumn('created_at', function ($query, $keyword) {
 //                            $query->whereRaw("DATE_FORMAT(created_at,'%d %M %Y') LIKE ?", ["%$keyword%"]);
 //                        })->filterColumn('country', function($query, $keyword) {
 //                            $query->whereRaw('getContactDetail.country', '=', $keyword);
 //                        })->addColumn('kyc_number', function ($users) {
 //                            if ($users->getKycDetail) {
 //                                return $users->getKycDetail->kyc_number;
 //                            } else {
 //                                return "";
 //                            }
 //                        })->addColumn('country', function ($users) {
 //                            if ($users->getContactDetail) {
 //                                $country = DB::table('countries')->select('name')->find($users->getContactDetail->country);
 //                                return $country->name ?? "";
 //                            } else {
 //                                return "";
 //                            }
 //                        })
 //                        ->addColumn('status', function ($user) use ($addonplans) {
 //                            if ($user->status == 1) {
 //                                $sure = trans("global.areYouSure");
 //                                return '<div class="modal fade change_status_modal" id="change_status_' . $user->id . '" role="dialog" aria-labelledby="change_status_' . $user->id . '" aria-hidden="true" data-backdrop="static" data-keyboard="false">
 //            <div class="modal-dialog modal-sm" role="document">
 //            <div class="modal-content" style="padding:20px">
 //            <div class="modal-header">
 //            <h5 class="modal-title" id="change_status_' . $user->id . '"">Are you sure you want to change status Inactive?</h5>
 //            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
 //            <span aria-hidden="true">&times;</span>
 //            </button>
 //            </div>
 //            <form action="' . url('admin/kyc/adminChangeStatus') . '" method="post">
 //            <div class="modal-body">
 //            <input type="hidden" name="id" value="' . $user->id . '">
 //            <input type="hidden" name="status" value="0">
 //            <input type="hidden" name="_token" value="' . csrf_token() . '">
 //            <div class="row"> 
 //            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
 //            <div class="form-group mg-t-10"> 
 //            <div class="nk-int-st">
 //            <input type="text" class="form-control" value="" placeholder="Notes*" name="notes" required>
 //            </div>
 //            </div> 
 //            </div>
 //            </div>
 //            </div>
 //            <div class="modal-footer">
 //            <button  type="submit" class="btn btn-success notika-btn-success waves-effect">Yes, Change It</button>
 //            </div>
 //            </form>
 //            </div>
 //            </div>
 //            </div>
 //            <a  type="button" class="btn btn-orange orange-icon-notika btn-reco-mg btn-button-mg waves-effect" data-toggle="modal" href="#change_status_' . $user->id . '" title="Active"><i class="notika-icon notika-checked"></i></a>';
 //                            } else {
 //                                if (!isset($user->current_plan) || empty($user->current_plan) || $user->current_plan == 0) {
 //                                    $plansdata = '';
 //                                    $addondata = '<option value="">Select Plan for Serial Numbers</option>';
 //                                    $plans = Plan::all()->pluck('name', 'id')->prepend('Select Current Plan*', '');
 //                                    foreach ($plans as $key => $plan) {
 //                                        $plansdata .= '<option value="' . $key . '">' . $plan . '</option>';
 //                                    }
 //                                    foreach ($plans as $key => $plan) {
 //                                        if ($key > 0) {
 //                                            $addondata .= '<option value="' . $key . '">' . $plan . '</option>';
 //                                        }
 //                                    }
 //                                    $machine_numbers = MachineNumbers::leftJoin('user_kyc_details as ky', 'ky.machine_number', '=', 'machine_numbers.machine_number')
 //                                            ->where('ky.id', NULL)->pluck('machine_numbers.machine_number');

 //                                    $machinenumdata = '<option value="">Select Machine Number</option>';
 //                                    foreach ($machine_numbers as $key => $machine) {
 //                                        $machinenumdata .= '<option value="' . $machine . '">' . $machine . '</option>';
 //                                    }
 //                                    return '<div class="modal fade change_status_modal" id="change_status_' . $user->id . '" role="dialog" aria-labelledby="change_status_' . $user->id . '" aria-hidden="true" data-backdrop="static" data-keyboard="false">
 //            <div class="modal-dialog modal-sm" role="document">
 //            <div class="modal-content" style="padding:20px">
 //            <div class="modal-header">
 //            <h5 class="modal-title" id="change_status_' . $user->id . '"">Are you sure you want to change status Active?</h5>
 //            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
 //            <span aria-hidden="true">&times;</span>
 //            </button>
 //            </div>
 //            <form action="' . url('admin/kyc/adminChangeStatus') . '" method="post">
 //            <div class="modal-body">
 //            <input type="hidden" name="id" value="' . $user->id . '">
 //            <input type="hidden" name="status" value="1">
 //            <input type="hidden" name="_token" value="' . csrf_token() . '">
 //            <div class="row"> 
 //            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
 //            <div class="form-group mg-t-20"> 
 //            <div class="nk-int-st">
 //            <select class="form-control  current_plan" name="current_plan" id="current_plan" required onchange="changePlandID(this.value)">' . $plansdata . '</select>
 //            </div>
 //            </div> 
 //            </div>
 //            </div>
 //            <div class="row"> 
 //            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
 //            <div class="form-group mg-t-10"> 
 //            <div class="nk-int-st">
 //            <select class="form-control current_plan_id" name="current_plan_id" id="current_plan_id" required>
 //            <option value="">Select Sub-Plan Id*</option>
 //            </select>
 //            </div>
 //            </div> 
 //            </div>
 //            </div>
 //            <div class="row"> 
 //            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
 //            <div class="form-group mg-t-10"> 
 //            <div class="nk-int-st">
 //            <input type="text" class="form-control" placeholder="Custom Price" name="machine_price">
 //            </div>
 //            </div> 
 //            </div>
 //            </div>
 //            <div class="row"> 
 //            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
 //            <div class="form-group mg-t-10"> 
 //            <div class="nk-int-st">
 //            <select class="form-control" name="machine_number" id="machine_number" required>' . $machinenumdata . '</select>
 //            </div>
 //            </div> 
 //            </div>
 //            </div>
 //            <div class="row"> 
 //            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
 //            <div class="form-group mg-t-10"> 
 //            <div class="nk-int-st">
 //            <input type="date" class="form-control" value="' . date("d/m/Y") . '" placeholder="Machine Purchase Date*" name="machine_purchase_date" required>
 //            </div>
 //            </div> 
 //            </div>
 //            </div>
 //            <div class="row"> 
 //            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
 //            <div class="form-group mg-t-10"> 
 //            <div class="nk-int-st">
 //            <input type="text" class="form-control" value="" placeholder="Notes*" name="notes" required>
 //            </div>
 //            </div> 
 //            </div>
 //            </div>
 //            <div class="row"> 
 //            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
 //            <div class="form-group mg-t-20"> 
 //            <div class="nk-int-st">
 //            <select class="form-control addon_plan" name="addon_plan[]" id="addon_plan" multiple onchange="changeAddonPlandID(this)">' . $addondata . '</select>
 //            </div>
 //            </div> 
 //            </div>
 //            </div>
 //            <div class="row"> 
 //            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
 //            <div class="form-group mg-t-20"> 
 //            <div class="nk-int-st">
 //            <select class="form-control" name="serial_number_id" id="serial_number_id" ><option value="">Select Serial Number</option></select>
 //            </div>
 //            </div> 
 //            </div>
 //            </div>
 //            </div>
 //            ' . $addonplans . '
 //            <div class="modal-footer">
 //            <button  type="submit" class="btn btn-success notika-btn-success waves-effect">Yes, Change It</button>
 //            </div>
 //            </form>
 //            </div>
 //            </div>
 //            </div>
 //            <a  type="button" class="btn btn-success success-icon-notika btn-reco-mg btn-button-mg waves-effect" data-toggle="modal" href="#change_status_' . $user->id . '" title="Inactive"><i class="notika-icon notika-close"></i></a>';
 //                                } else {
 //                                    $sure = trans("global.areYouSure");
 //                                    return '<div class="modal fade change_status_modal" id="change_status_' . $user->id . '" role="dialog" aria-labelledby="change_status_' . $user->id . '" aria-hidden="true" data-backdrop="static" data-keyboard="false">
 //            <div class="modal-dialog modal-sm" role="document">
 //            <div class="modal-content" style="padding:20px">
 //            <div class="modal-header">
 //            <h5 class="modal-title" id="change_status_' . $user->id . '"">Are you sure you want to change status Inactive?</h5>
 //            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
 //            <span aria-hidden="true">&times;</span>
 //            </button>
 //            </div>
 //            <form action="' . url('admin/kyc/adminChangeStatus') . '" method="post">
 //            <div class="modal-body">
 //            <input type="hidden" name="id" value="' . $user->id . '">
 //            <input type="hidden" name="status" value="1">
 //            <input type="hidden" name="_token" value="' . csrf_token() . '">
 //            <div class="row"> 
 //            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
 //            <div class="form-group mg-t-10"> 
 //            <div class="nk-int-st">
 //            <input type="text" class="form-control" value="" placeholder="Notes*" name="notes" required>
 //            </div>
 //            </div> 
 //            </div>
 //            </div>
 //            </div>
 //            <div class="modal-footer">
 //            <button  type="submit" class="btn btn-success notika-btn-success waves-effect">Yes, Change It</button>
 //            </div>
 //            </form>
 //            </div>
 //            </div>
 //            </div>
 //            <a  type="button" class="btn btn-orange orange-icon-notika btn-reco-mg btn-button-mg waves-effect" data-toggle="modal" href="#change_status_' . $user->id . '" title="Inactive"><i class="notika-icon notika-close"></i></a>';
 //                                }
 //                            }
 //                        })
 //                        ->addColumn('allAction', function ($user) {

 //                            return '<a href="' . route('admin.retailers.edit', $user->id) . '"><button class="btn btn-info info-icon-notika btn-reco-mg btn-button-mg waves-effect" title="Edit"><i class="notika-icon notika-edit"></i></button></a>
 // <form action="' . route('admin.retailers.destroy', $user->id) . '" method="POST" onsubmit="return confirm("' . trans('global.areYouSure') . '");" style="display:none;">
 // <input type="hidden" name="_method" value="DELETE">
 // <input type="hidden" name="_token" value="' . csrf_token() . '">
 // <button type="submit" class="btn btn-danger danger-icon-notika btn-reco-mg btn-button-mg waves-effect" title="Delete"> <i class="notika-icon notika-close"></i></button>
 // </form>

 // <a href="#user_password_change"><button class="btn btn-primary primary-icon-notika btn-reco-mg btn-button-mg waves-effect" data-toggle="modal" title="Change Password" href="#user_password_change" data-id="' . $user->id . '" data-url="' . route('admin.retailer-change-password', ["user" => $user->id]) . '" data-toggle="modal"><i class="notika-icon notika-promos"></i></button></a>

 // <a href="' . route('admin.setting.commission', $user->id) . '"><button class="btn btn-amber amber-icon-notika btn-reco-mg btn-button-mg waves-effect" title="Commission Setting"><i class="notika-icon notika-star"></i></button></a>

 // <a href="' . route('admin.generalnotification.create', ["user_id" => $user->id]) . '"><button class="btn btn-amber amber-icon-notika btn-reco-mg btn-button-mg waves-effect" title="Send Notification"><i class="notika-icon notika-chat"></i></button></a>';
 //                        })
 //                        ->addColumn('delete_chk', function ($user) {
 //                            return '<input type="checkbox" class="sub_chk" value="' . $user->id . '" name="delete_chk[]" data-id="' . $user->id . '">';
 //                        })
 //                        ->rawColumns(['status', 'delete_chk', 'allAction'])->make(true);
    }

    public function retailerProfileChangeRequestList() {
		
        $retailerUsers = DB::table('user_profile_change_request')
                ->join('users', 'users.id', '=', 'user_profile_change_request.user_id')
                ->join('countries', 'countries.id', '=', 'user_profile_change_request.country')
                ->join('states', 'states.id', '=', 'user_profile_change_request.state')
                ->select('user_profile_change_request.*', 'users.name', 'states.name as state_name', 'countries.name as country_name',DB::raw('CONCAT(user_id,"_",users.name) AS user_id_name'),DB::raw('CONCAT(user_profile_change_request.id,"_",user_profile_change_request.status) AS id_status'))
                ->orderBy('user_profile_change_request.id', 'desc')
                ->get();

        return view('admin.retailers.retailer-profile-change-request-list', compact('retailerUsers'));
    }

    /**
     * Retailer Profile Change RequestList
     * 
     */
    public function changeProfileRequestStatus(Request $request) {
        $request_statu = UserProfileChangeRequest::find($request->id);

        if ($request->status == 'Approved') {

            $validator = User::where('email', $request_statu->email)->orwhere('phone', $request_statu->phone)->count();
            if ($validator > 1) {
                return response()->json(array('success' => 'Email/Phone already Exist'), 200);
            }

            $user = User::find($request_statu->user_id);
            $user->email = (isset($request_statu->email) && !empty($request_statu->email)) ? $request_statu->email : $user->email;
            $user->phone = (isset($request_statu->phone) && !empty($request_statu->phone)) ? $request_statu->phone : $user->phone;
            $user->other_phone = (isset($request_statu->other_phone) && !empty($request_statu->other_phone)) ? $request_statu->other_phone : $user->other_phone;
            $user->save();
            /* get contact detail and update */
            $userContactDetail = $user->getContactDetail()->first();
            if ($userContactDetail) {
                $user = $userContactDetail->update([
                    'state' => (isset($request_statu->state) && !empty($request_statu->state)) ? $request_statu->state : $userContactDetail->state,
                    'country' => (isset($request_statu->country) && !empty($request_statu->country)) ? $request_statu->country : $userContactDetail->country,
                ]);
            }
        }

        $request_statu->status = $request->status;
        $request_statu->save();


        return response()->json(array('success' => 'Status change ' . $request->status), 200);
    }

    /**
     * Retailer IMEI Change request list NT:03032021
     * 
     */
    public function imeiChangeRequestList() {
        $retailerUsers = UserImeiChangeRequest::join('users', 'users.id', '=', 'user_imei_change_request.user_id')
                ->select('user_imei_change_request.*', 'users.name', 'users.email', 'users.phone')
                ->orderBy('user_imei_change_request.id', 'desc')
                ->whereNotNull('user_imei_change_request.device_id')
                ->get();

        return view('admin.retailers.retailer-imei-change-request-list', compact('retailerUsers'));
    }

    /**
     * Retailer IMEI Update NT:03032021
     * 
     */
    public function updateImeiRequest(Request $request) {

        $request_statu = UserImeiChangeRequest::find($request->id);
        if (!empty($request_statu->user_id)) {
            $user = User::find($request_statu->user_id);
            $user->device_id = $request_statu->device_id;
//            $request_statu->delete();
            $user->save();

            $request_statu->status = 1;
            $request_statu->save();
            //$request_statu = UserImeiChangeRequest::find($request->id);
            return Redirect::route('admin.imei-change-request');
        } else {
            return Redirect::route('admin.imei-change-request');
        }
    }

    public function retailerDeleteAll(Request $request) {
        $ids = $request->ids;
        DB::table("users")->whereIn('id', explode(",", $ids))->delete();
        DB::table("users")->whereIn('parent_user_id', explode(",", $ids))->delete();
        DB::table("user_kyc_details")->whereIn('user_id', explode(",", $ids))->delete();
        DB::table("user_contact_details")->whereIn('user_id', explode(",", $ids))->delete();
        DB::table("user_bank_details")->whereIn('user_id', explode(",", $ids))->delete();
        return response()->json(['success' => "Retailers Deleted successfully."]);
    }

}
