<?php

namespace App\Http\Controllers\Admin;

use Spatie\Permission\Models\Permission;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\BrandType;
use App\Models\Brand;
use App\Models\BrandModel;
use App\Models\BrandSubModel;
use App\Models\BrandModelScreenImage;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Str;
use Alert;
use File;
use Store;
use Response;

class BulkUploadController extends Controller
{
  
    public function create(Request $request)
    {
        if (! Gate::allows('bulkupload_manage')) {
            return abort(401);
        }

        $brand_types_list = BrandType::select('id', 'name')->where('status', 1)->get();
        $brands_list = Brand::select('id', 'name')->where('status', 1)->get();
        $brandModel_list = BrandModel::select('id', 'name')->where('status', 1)->get();
        $brandSubModel_list = BrandSubModel::select('id', 'name')->where('status', 1)->get();
        return view('admin.bulk-upload.create', ['brand_types_list' => $brand_types_list ,'brands_list'  => $brands_list,'brandModel_list'  => $brandModel_list,'brandSubModel_list'  => $brandSubModel_list]);
      
        return view('admin.bulk-upload.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      if(!Gate::allows('bulkupload_manage'))
      {

        return abort(401);

      } else {
        
            request()->validate([
              'sample_file_id' => 'required',
              'action_type' => 'required',
              'csv_import' => 'required|file',
              'file' => 'required_if:action_type,add|file|mimes:zip',
            ],
            [
              'sample_file_id.required' => 'Import Data is required',
              'action_type.required' => 'Action Type is required',
              'csv_import.required' => 'CSV file is required',
              'file.required' => 'ZIP file is required',
            ]); 

            ini_set('memory_limi', -1);
 
            $uploadedFile = $request->file('csv_import');

            $status = $request->status;

            $action_type = $request->action_type ?? 'add';

            $extension = $uploadedFile->getClientOriginalExtension();

            if (strtolower($extension) != 'csv') {

                return redirect()->back()->with('error', 'Uploaded file is not CSV file.');
            }
  
            $inputFileName = $uploadedFile->move(public_path('uploads'),'abc.csv');
	
            if (!mb_check_encoding(file_get_contents($inputFileName), 'UTF-8')) {
                return redirect()->back()->with('error', 'Uploaded file is not UTF-8 formate.');
            }

            $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($inputFileName);

            $rows = $spreadsheet->getSheet(0)->toArray();


            $headers =  array_shift($rows);

            if ($request->sample_file_id == 'category') {
                if ($action_type == 'add') {
                    $data = $this->uploadBrandTypeAdd($rows);
                }else{
                    $data = $this->uploadBrandTypeUpdate($rows);
                } 
                if ($request->hasFile('file')) { $this->storeImageZip($request, 'brand-types'); }

            }elseif ($request->sample_file_id == 'brand') {
                if ($action_type == 'add') {
                    $data = $this->uploadBrandAdd($rows);
                }else{
                    $data = $this->uploadBrandUpdate($rows);
                }
                if ($request->hasFile('file')) {$this->storeImageZip($request, 'brands');}

            }elseif ($request->sample_file_id == 'model') {
                if ($action_type == 'add') {
                    $data = $this->uploadModelAdd($rows);
                }else{
                    $data = $this->uploadModelUpdate($rows);
                }
                if ($request->hasFile('file')) {$this->storeImageZip($request, 'models');}

            }elseif ($request->sample_file_id == 'sub_model') {
                if ($action_type == 'add') {
                    $data = $this->uploadSubModelAdd($rows);
                }else{
                    $data = $this->uploadSubModelUpdate($rows);
                }
                if ($request->hasFile('file')) {$this->storeImageZip($request, 'submodels');}

            }elseif ($request->sample_file_id == 'modelscreen') {
                if ($action_type == 'add') {
                    $data = $this->uploadModelScreenAdd($rows);
                }else{
                    $data = $this->uploadModelScreenUpdate($rows);
                }
                if ($request->hasFile('file')) {$this->storeImageZip($request, 'models-screen-image');}
            }

            unlink($inputFileName);
            if (empty($data)) {
                Alert::success('Bluk Data', 'Uploaded successfully');
                return redirect()->route('admin.bulkupload.create')->with('message', 'Uploaded successfully.');
            }else{
                $error = implode(',', $data);
                Alert::error('Bluk Data Error', 'Following row data not save '. $error);
                return redirect()->route('admin.bulkupload.create')->with('message', 'Following row data not save '. $error);
            }
            

      }      

    }

    /*start brand type bulk upload add and update*/
    public function uploadBrandTypeAdd($rows){
        $bulk_brand_type_records = [];
        $error = null;
        $order_number = \DB::table('brand_types')->max('id');
        foreach ($rows as $index => $value) {
          //dd($value);
            if (isset($value[0]) ){
                $date = [
                    'name'              => $value[0],
                    'slug'              => Str::slug($value[0], '-'),
                    'image'             => $value[1] ?? null,
                    'meta_title'        => $value[2] ?? null,
                    'meta_description'  => $value[3] ?? null,
                    'meta_keywords'     => $value[4] ?? null,
                    'status'            => 1,
                    'order_number'      => (intval($order_number) + $index + 1),
                    'created_at'        => now()->toDateTimeString(),
                    'updated_at'        => now()->toDateTimeString(),
                ];             
                $bulk_brand_type_records[] = $date;
            }else{
                $error[] = $index + 2;                
            }
        }
        if(count($bulk_brand_type_records)>0){
            BrandType::insert($bulk_brand_type_records);
        }

        return $error;
    } 

    public function uploadBrandTypeUpdate($rows){
        $error = null;
        foreach ($rows as $index => $value) {
            if (is_int($value[0]) && isset($value[1]) && isset($value[3]) ){
                $data=BrandType::where('id',$value[0])->first();
                if($data){
                    $data->name=$value[1]; 
                    $data->image= $value[2] ?? $data->image;
                    $data->status= $value[3] == 'Active' ? 1 : 0;
                    $data->save();
                }
                else{
                    $error[]=$index+1; 
                }
            }else{
                $error[]=$index+1;                
            }
        }
        return $error;
    }
    /*end brand type bulk upload */

    /*start brand bulk upload */
    public function uploadBrandAdd($rows){

        $bulk_brand_type_records = [];
        $error = null;
        $order_number = \DB::table('brands')->max('id');
        foreach ($rows as $index => $value) {
            if (is_int($value[0]) && isset($value[1]) ){
                $date = [
                    'brand_type_id'     => $value[0],
                    'name'              => $value[1],
                    'slug'              => Str::slug($value[1], '-'),
                    'image'             => $value[2] ?? null,
                    'meta_title'        => $value[3] ?? null,
                    'meta_description'  => $value[4] ?? null,
                    'meta_keywords'     => $value[5] ?? null,
                    'status'            => 1,
                    'order_number'      => (intval($order_number) + $index + 1),
                    'created_at'        => now()->toDateTimeString(),
                    'updated_at'        => now()->toDateTimeString(),
                ];             
                $bulk_brand_type_records[] = $date;
            }else{
                $error[] = $index + 2;                
            }
          
        }
        
        if(count($bulk_brand_type_records)>0){
            Brand::insert($bulk_brand_type_records);
        }

        return $error;
    } 

    public function uploadBrandUpdate($rows){
        $error=null;
        foreach($rows as $index=>$value) {
            if(is_int($value[0]) && is_int($value[1]) && isset($value[2]) && isset($value[3]) && isset($value[5])){
                $data=Brand::where('id',$value[0])->first() ;
                if($data){
                    $data->brand_type_id = $value[1];
                    $data->name= $value[3];
                    $data->slug= Str::slug($value[3], '-');
                    $data->image= $value[4]??$data->image;
                    $data->status= $value[5] == 'Active' ? 1 : 0;
                    $data->updated_at=now()->toDateTimeString();
                    $data->save();
                }
                else{
                    $error[]=$index+1; 
                }
            }else{
                $error[]=$index+1;                
            }
        }
        return $error;
    }
    /*end brand bulk upload */

    /*start Model upload */
    public function uploadModelAdd($rows){

        $bulk_brand_type_records = [];
        $error = null;        
        $order_number = \DB::table('brand_models')->max('id');
        foreach ($rows as $index => $value) {
             if (is_int($value[0]) && is_int($value[1]) && isset($value[2]) ){
                $date = [
                    'brand_type_id'     => $value[0],
                    'brand_id'          => $value[1],
                    'name'              => $value[2],
                    'slug'              => Str::slug($value[2], '-'),
                    'image'             => $value[3] ?? null,
                    'cut_point'         => $value[4] ?? null,
                    'meta_title'        => $value[5] ?? null,
                    'meta_description'  => $value[6] ?? null,
                    'meta_keywords'     => $value[7] ?? null,
                    'status'            => 1,
                    'order_number'      => (intval($order_number) + $index + 1),
                    'created_at'        => now()->toDateTimeString(),
                    'updated_at'        => now()->toDateTimeString(),
                ];             
                $bulk_brand_type_records[] = $date;
            }else{
                $error[] = $index + 2;                
            }
        }
        if(count($bulk_brand_type_records)>0){
            BrandModel::insert($bulk_brand_type_records);
        }

        return $error;
    } 
    public function uploadModelUpdate($rows){
        $error=null;
        foreach ($rows as $index => $value) {
            if (is_int($value[0]) && is_int($value[1]) && isset($value[2]) && is_int($value[3]) && isset($value[4]) && isset($value[5]) && isset($value[7]) )
            {
                $data=BrandModel::where('id',$value[0])->first() ;
                if($data){
                    $data->brand_type_id= $value[1]; //CategoryId
                    $data->brand_id=$value[3]; //BrandId
                    $data->name=$value[5]; //ModelName
                    $data->slug=Str::slug($value[5], '-');
                    $data->image= $value[6] ?? $data->image; //Image
                    $data->status= $value[7] == 'Active' ? 1 : 0; //Status
                    $data->updated_at=now()->toDateTimeString();
                    $data->save();
                }
                else{
                    $error[]=$index+1; 
                }
            }else{
                $error[]=$index+1;                
            }
        }
        return $error;
    }
    /*end Model bulk upload */


    /*start Sub-Model upload */
    public function uploadSubModelAdd($rows){
        $bulk_brand_type_records = [];
        $error = null;
        $order_number = \DB::table('brand_sub_models')->max('id');
        foreach ($rows as $index => $value) {
        if (is_int($value[0]) && is_int($value[1]) && is_int($value[2]) && isset($value[3]) ){
            $date = [
                'brand_type_id'     => $value[0], //category
                'brand_id'          => $value[1],
                'brand_models_id'   => $value[2],
                'name'              => $value[3],
                'slug'              => Str::slug($value[3], '-'),
                'image'             => $value[4] ?? null,
                'cut_point'         => $value[5] ?? null,
                'meta_title'        => $value[6] ?? null,
                'meta_description'  => $value[7] ?? null,
                'meta_keywords'     => $value[8] ?? null,
                'status'            => 1,
                'order_number'      => (intval($order_number) + $index + 1),
                'created_at'        => now()->toDateTimeString(),
                'updated_at'        => now()->toDateTimeString(),
            ];             
            $bulk_brand_type_records[] = $date;
            }else{
                $error[] = $index + 2;                
            }
        }
        if(count($bulk_brand_type_records)>0){
            BrandSubModel::insert($bulk_brand_type_records);
        }

        return $error;
    } 

    public function uploadSubModelUpdate($rows){
        $error=null;
        foreach ($rows as $index => $value) {
            if (is_int($value[0]) && is_int($value[1]) && isset($value[2]) && is_int($value[3]) && isset($value[4]) && is_int($value[5]) && isset($value[6]) && isset($value[7]) && is_int($value[8]) && isset($value[10])){
                $data=BrandSubModel::where('id',$value[0])->first() ;
                if($data){
                    $data->brand_type_id=$value[1];//CategoryId
                    $data->brand_id=$value[3];//BrandId
                    $data->brand_models_id=$value[5];//ModelId
                    $data->name=$value[7];
                    $data->slug=Str::slug($value[7], '-');
                    $data->cut_point= $value[8] ?? $data->cut_point;
                    $data->image= $value[9] ?? $data->image; //Image
                    $data->status= $value[10] == 'Active' ? 1 : 0; //Status
                    $data->updated_at=now()->toDateTimeString();
                    $data->save();
                }
                else{
                    $error[]=$index+1; 
                }
            }else{
                $error[]=$index+1;                
            }
        }
        return $error;
    }
    /*end Model bulk upload */


    /*start modelscreen upload */
    public function uploadModelScreenAdd($rows){

        $bulk_brand_type_records = [];
        $error = null;
        $error_flag = false;
        $order_number = \DB::table('brand_model_screen_images')->max('id');
        foreach ($rows as $index => $value) {

        if (isset($value[4])) {
            $names = explode("|",$value[4]);
            $names = is_array($names) ? json_encode($names,JSON_FORCE_OBJECT) : json_encode([],JSON_FORCE_OBJECT);
        }else{
            $error_flag = true;
            $names = json_encode([],JSON_FORCE_OBJECT);
        }

        if (isset($value[5])) {
            $image = explode("|",$value[5]);
            $image = is_array($image) ? json_encode($image,JSON_FORCE_OBJECT) : json_encode([],JSON_FORCE_OBJECT);
        }else{
            $error_flag = true;
            $image = json_encode([],JSON_FORCE_OBJECT);
        }
          
        if (isset($value[6])) {
            $blt_file = explode("|",$value[6]);
            $blt_file = is_array($blt_file) ? json_encode($blt_file,JSON_FORCE_OBJECT) : json_encode([],JSON_FORCE_OBJECT);
        }else{
            $error_flag = true;
            $blt_file = json_encode([],JSON_FORCE_OBJECT);
        }
          
        if (is_int($value[0]) && is_int($value[1]) && is_int($value[2]) && is_int($value[3]) && $error_flag == false){
            $date = [
                'brand_type_id'     => $value[0], //category
                'brand_id'          => $value[1],
                'model_id'          => $value[2],
                'sub_model_id'      => $value[3],
                'name'              => $names,
                'image'             => $image,
                'blt_file'          => $blt_file,
                //'meta_title'        => $value[6] ?? null,
               // 'meta_description'  => $value[7] ?? null,
               // 'meta_keywords'     => $value[8] ?? null,
                'status'            => 1,
                'order_number'      => (intval($order_number) + $index + 1),
                'created_at'        => now()->toDateTimeString(),
                'updated_at'        => now()->toDateTimeString(),
            ];             
            $bulk_brand_type_records[] = $date;
            }else{
                $error[] = $index + 2;                
            }
        }
        if(count($bulk_brand_type_records)>0){
            BrandModelScreenImage::insert($bulk_brand_type_records);
        }

        return $error;
    } 

    public function uploadModelScreenUpdate($rows){
        $error = null;
        $error_flag = false;
        foreach ($rows as $index => $value) {

        if (isset($value[9])) {
            $names = explode("|",$value[9]);
            $names = is_array($names) ? json_encode($names,JSON_FORCE_OBJECT) : json_encode([],JSON_FORCE_OBJECT);
        }else{
            $error_flag = true;
            $names = json_encode([],JSON_FORCE_OBJECT);
        }

        if (isset($value[11])) {
            $image = explode("|",$value[11]);
            $image = is_array($image) ? json_encode($image,JSON_FORCE_OBJECT) : json_encode([],JSON_FORCE_OBJECT);
        }else{
            $error_flag = true;
            $image = json_encode([],JSON_FORCE_OBJECT);
        }
          
        if (isset($value[10])) {
            $blt_file = explode("|",$value[10]);
            $blt_file = is_array($blt_file) ? json_encode($blt_file,JSON_FORCE_OBJECT) : json_encode([],JSON_FORCE_OBJECT);
        }else{
            $error_flag = true;
            $blt_file = json_encode([],JSON_FORCE_OBJECT);
        }
          
        if (is_int($value[0]) && is_int($value[1]) && isset($value[2]) && is_int($value[3]) && isset($value[4]) && is_int($value[5]) && isset($value[6]) && is_int($value[7]) && isset($value[8]) && isset($value[12]) && $error_flag == false){
            $data =BrandModelScreenImage::where('id',$value[0])->first() ;
            if($data){
                $data->brand_type_id=$value[1];//CategoryId
                $data->brand_id=$value[3];//BrandId
                $data->model_id=$value[5];//ModelId
                $data->sub_model_id=$value[7];//SubModelId
                $data->blt_file=$blt_file;
                $data->image=$image;
                $data->name= $names;
                $data->status=$value[12] == 'Active' ? 1 : 0; //Status
                $data->updated_at=now()->toDateTimeString();
                $data->save();
                }
                else{
                    $error[]=$index+1; 
                }
            }else{
                $error[]=$index+1;                
            }
        }
        return $error;
    }
    /*end modelscreen upload */


    public function storeImageZip($data, $filepath)
    {
        $imageZip = new \ZipArchive();
        $imageZipName = null;
        $extractedPath = public_path('images/'.$filepath.'/');
        if ($imageZip->open($data->file('file'))) {
            for ($i = 0; $i < $imageZip->numFiles; $i++) {
                $filename = $imageZip->getNameIndex($i);
                $imageZipName = pathinfo($filename);
            }
            $imageZip->extractTo($extractedPath);
            $imageZip->close();
        }
        $listOfImages = scandir($extractedPath.$imageZipName['dirname'].'/');
        foreach ($listOfImages as $key => $imageName) {
            if($imageName != '.' && $imageName != '..'){
                rename($extractedPath.$imageZipName['dirname'].'/'.$imageName, $extractedPath.$imageName);
            }
        }
        return $imageZipName;
    }

    //export data
    public function export(Request $request)
    {
        $export_table = $request->export_table ?? 'category';
        $filename = $export_table.time().'.csv';
        $headers = array(
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=".$filename,
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );

        if ($export_table == 'category') {

            $datas = BrandType::orderBy('id')->get();
            /*$columns = array('CategoryID', 'Name', 'Image', 'Status', 'MetaTitle', 'MetaDescription', 'MetaKeywords');*/
            $columns = array('CategoryID', 'Name', 'Image', 'Status');

            $callback = function() use ($datas, $columns)
            {
                $file = fopen('php://output', 'w');
                fputcsv($file, $columns);

                foreach($datas as $data) {
                    fputcsv($file, array($data->id ?? null, $data->name ?? null, $data->image ?? null, $data->status == 1 ? 'Active' : 'InActive',

                    ));
                }
                fclose($file);
            };

        }elseif ($export_table == 'brand') {

            $datas = Brand::join('brand_types', 'brand_types.id', '=', 'brands.brand_type_id');
            if ($request->has('brand_type_id')) {
                $datas = $datas->where('brand_type_id',$request->brand_type_id);
            }
            $datas = $datas->select('brands.*', 'brand_types.name as category_name')->orderBy('id')->get();

            $columns = array('BrandId', 'CategoryId', 'CategoryName', 'BrandName', 'Image', 'Status');

            $callback = function() use ($datas, $columns)
            {
                $file = fopen('php://output', 'w');
                fputcsv($file, $columns);

                foreach($datas as $data) {
                    fputcsv($file, array($data->id ?? null, $data->brand_type_id ?? null, $data->category_name ?? null, $data->name ?? null, $data->image ?? null, $data->status == 1 ? 'Active' : 'InActive',

                    ));
                }
                fclose($file);
            };

        }elseif ($export_table == 'model') {

            $datas = BrandModel::join('brands', 'brands.id', '=', 'brand_models.brand_id')->join('brand_types', 'brand_types.id', '=', 'brand_models.brand_type_id');           
            if ($request->has('brand_type_id') && !empty($request->brand_type_id) ) {
                $datas = $datas->where('brand_models.brand_type_id',$request->brand_type_id);
            }
            if ($request->has('brand_id') && !empty($request->brand_id) ) {
                $datas = $datas->where('brand_models.brand_id',$request->brand_id);
            }
            $datas = $datas->select('brand_models.*', 'brand_types.name as CategoryName','brands.name as BrandName')->orderBy('id')->get();
           /* $datas = BrandModel::orderBy('id')->get();*/
            $columns = array('ModelId', 'CategoryId', 'CategoryName', 'BrandId', 'BrandName', 'ModelName', 'Image', 'Status');
            $callback = function() use ($datas, $columns)
            {
                $file = fopen('php://output', 'w');
                fputcsv($file, $columns);

                foreach($datas as $data) {
                    fputcsv($file, array($data->id ?? null, $data->brand_type_id ?? null, $data->CategoryName ?? null, $data->brand_id ?? null, $data->BrandName ?? null, $data->name ?? null, $data->image ?? null, $data->status == 1 ? 'Active' : 'InActive',

                    ));
                }
                fclose($file);
            };

        }elseif ($export_table == 'sub_model') {

          	$datas = BrandSubModel::join('brands', 'brands.id', '=', 'brand_sub_models.brand_id')
                        ->join('brand_types', 'brand_types.id', '=', 'brand_sub_models.brand_type_id')
                        ->join('brand_models', 'brand_models.id', '=', 'brand_sub_models.brand_models_id');

            if ($request->has('brand_type_id') && !empty($request->brand_type_id) ) {
                $datas = $datas->where('brand_sub_models.brand_type_id',$request->brand_type_id);
            }
            if ($request->has('brand_id') && !empty($request->brand_id) ) {
                $datas = $datas->where('brand_sub_models.brand_id',$request->brand_id);
            }
            if ($request->has('model_id') && !empty($request->model_id) ) {
                $datas = $datas->where('brand_sub_models.brand_models_id',$request->model_id);
            }

            $datas = $datas->select('brand_sub_models.*', 'brand_types.name as CategoryName','brands.name as BrandName','brand_models.name as ModelName')->orderBy('id')->get();
            
            $columns = array('SubModelId', 'CategoryId', 'CategoryName', 'BrandId', 'BrandName', 'ModelId', 'ModelName', 'SubModeName', 'CutPoint', 'Image', 'Status');
            $callback = function() use ($datas, $columns)
            {
                $file = fopen('php://output', 'w');
                fputcsv($file, $columns);

                foreach($datas as $data) {
                    fputcsv($file, array($data->id ?? null, 
                    	$data->brand_type_id ?? null, $data->CategoryName ?? null, 
                    	$data->brand_id ?? null, $data->BrandName ?? null, 
                    	$data->brand_models_id ?? null, $data->ModelName ?? null, 
                    	$data->name ?? null, $data->cut_point ?? null, $data->image ?? null, $data->status == 1 ? 'Active' : 'InActive',));
                }
                fclose($file);
            };

        }elseif ($export_table =='model_screen') {

          	$datas = BrandModelScreenImage::join('brands', 'brands.id', '=', 'brand_model_screen_images.brand_id')
                        ->join('brand_types', 'brand_types.id', '=', 'brand_model_screen_images.brand_type_id')
                        ->join('brand_models', 'brand_models.id', '=', 'brand_model_screen_images.model_id')
                        ->join('brand_sub_models', 'brand_sub_models.id', '=', 'brand_model_screen_images.sub_model_id');
                        
            if ($request->has('brand_type_id') && !empty($request->brand_type_id) ) {
                $datas = $datas->where('brand_model_screen_images.brand_type_id',$request->brand_type_id);
            }
            if ($request->has('brand_id') && !empty($request->brand_id) ) {
                $datas = $datas->where('brand_model_screen_images.brand_id',$request->brand_id);
            }
            if ($request->has('model_id') && !empty($request->model_id) ) {
                $datas = $datas->where('brand_model_screen_images.model_id',$request->model_id);
            }
            if ($request->has('sub_model_id') && !empty($request->sub_model_id) ) {
                $datas = $datas->where('brand_model_screen_images.sub_model_id',$request->sub_model_id);
            }

            $datas = $datas->select('brand_model_screen_images.*', 'brand_types.name as CategoryName','brands.name as BrandName','brand_models.name as ModelName','brand_sub_models.name as SubModelName')->orderBy('id')->get();
 
            /*$datas = BrandModelScreenImage::orderBy('id')->get();*/
            $columns = array('ModelScreenId', 'CategoryId', 'CategoryName', 'BrandId', 'BrandName', 'ModelId', 'ModelName', 'SubModelId', 'SubModelName', 'Name', 'Blt', 'Image', 'Status');

            $callback = function() use ($datas, $columns)
            {
                $file = fopen('php://output', 'w');
                fputcsv($file, $columns);

                foreach($datas as $data) {

                    $nameArray = json_decode($data->name,TRUE);
                    if( $nameArray === NULL ){
                        $names = $data->name ?? null;
                    }else{
                        $names =  implode("|",$nameArray); 
                    }

                    $imagesArray = json_decode($data->image,TRUE);
                    if( $imagesArray === NULL ){
                        $images = $data->image;
                    }else{
                        $images =  implode("|",$imagesArray); 
                    }
                    
                    $blt_filesArray = json_decode($data->blt_file,TRUE);
                    if( $blt_filesArray === NULL ){
                        $blt_files = $data->blt_file;
                    }else{
                        $blt_files =  implode("|",$blt_filesArray); 
                    }
                    
                    fputcsv($file, array($data->id ?? null, 
                    	$data->brand_type_id ?? null, $data->CategoryName ?? null,  
                    	$data->brand_id ?? null, $data->BrandName ?? null, 
                    	$data->model_id ?? null, $data->ModelName ?? null, 
                    	$data->sub_model_id ?? null, $data->SubModelName ?? null, $names,
                    	$blt_files, $images, $data->status == 1 ? 'Active' : 'InActive'

                    ));
                }
                fclose($file);
            }; 

        }
        
        return Response::stream($callback, 200, $headers);
    }
  
}
