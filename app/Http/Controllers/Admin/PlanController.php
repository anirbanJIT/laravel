<?php



namespace App\Http\Controllers\Admin;



use Spatie\Permission\Models\Permission;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Models\Plan;

use App\Models\PlanCommission;

use App\Models\CommissionSetting;

use App\Models\BrandType;

use App\Models\PlanBrandTypes;

use App\Models\PlanSerialNumbers;

use Illuminate\Support\Facades\Gate;

use Illuminate\Support\Str;

Use Alert;

use File;

use DB;



class PlanController extends Controller {
    public function index() {
        return redirect()->action('Admin\PlanController@planDetails');
        if (!Gate::allows('plan_manage')) {
            return abort(401);
        }
        $plans = Plan::orderBy('id', 'DESC')->get();
        return view('admin.plans.index', compact('plans'));
    }
    public function planDetails(Request $request) {
        if (!Gate::allows('plan_manage')) {
            return abort(401);
        }
        $plans = Plan::orderBy('id', 'DESC')->select(DB::raw('CONCAT(id,"_",plan_status) AS id_status'),'id','name','plan_status','created_at')->get();
        // dd($plans);
        return view('admin.plans.plan-list', compact('plans'));
    }



    public function create(Request $request) {

        if (!Gate::allows('plan_manage')) {

            return abort(401);

        }

        $plans = Plan::all();

        $brandTypes = BrandType::get();

        return view('admin.plans.create', compact('plans', 'brandTypes'));

    }



    /**

     * Store a newly created resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */

    public function store(Request $request) {

        if (!Gate::allows('plan_manage')) {



            return abort(401);

        } else {



            request()->validate([

                'name' => 'required|unique:plans|max:191',

//                'commission_plan.*.discount_value' => 'required|numeric|between:0,99.99',

//                'commission_plan.*.discount_type' => 'required',

            ]);



            $plan = new Plan;

            $plan->name = $request->name;

            $plan->slug = Str::slug($request->name, "-");

            $plan->save();





            $categories = $request->post('categories');



            if (!empty($categories)) {

                foreach ($categories as $key => $category) {

                    $category_data['plan_id'] = $plan->id;

                    $category_data['brand_type_id'] = $category;

                    PlanBrandTypes::Create($category_data);

                }

            }



//            $input_all = $request->commission_plan;

//            foreach ($input_all as $key => $value) {

//                if (empty($value['commission_plan_id_to'])) {

//                    $value['commission_plan_id_to'] = $plan->id;

//                }

//                if (empty($value['commission_plan_id_from'])) {

//                    $value['commission_plan_id_from'] = $plan->id;

//                }

//                $commission_plan = PlanCommission::create($value);

//            }



            Alert::success('Plan', 'Added successfully');

            return redirect()->route('admin.plans.index')->with('message', 'plan created successfully.');

        }

    }



    public function edit($id) {

        if (!Gate::allows('plan_manage')) {

            return abort(401);

        }

        $plans = Plan::all();

        $plan = Plan::findOrFail($id);

        $plan_commission = PlanCommission::where('commission_plan_id_from', $id)->get();

        $brandTypes = BrandType::get();

        return view('admin.plans.edit', compact('plans', 'plan', 'plan_commission', 'brandTypes'));

    }



    /**

     * Update the specified resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function update(Request $request, $id) {

        if (!Gate::allows('plan_manage')) {



            return abort(401);

        } else {



            /* $plan = Plan::findOrFail($id);

              $plan->update($request->all()); */



            request()->validate([

                'name' => 'required|unique:plans,name,' . $id,

                'commission_plan.*.discount_value' => 'required|numeric|between:0,99.99',

                'commission_plan.*.discount_type' => 'required',

            ]);



            $plan = Plan::findOrFail($id);

            $plan->name = $request->name;

            $plan->save();



            $categories = $request->post('categories');



            if (!empty($categories)) {

                foreach ($categories as $key => $category) {

                    $category_data['plan_id'] = $plan->id;

                    $category_data['brand_type_id'] = $category;

                    PlanBrandTypes::firstOrCreate($category_data);

                }

            }



            $deleted = PlanBrandTypes::where('plan_id', $plan->id)->whereNotIn('brand_type_id', $categories)->delete();



//            $input_all = $request->commission_plan;

//

//            foreach ($input_all as $key => $value) {

//                $commission_plan = PlanCommission::firstOrNew(['id' => $value['id']]);

//                $commission_plan->discount_value = $value['discount_value'];

//                $commission_plan->discount_type = $value['discount_type'];

//                $commission_plan->save();

//            }





            Alert::success('Plan', 'Update successfully');

            return redirect()->route('admin.plans.index')->with('message', 'Plan Update successfully.');

        }

    }



    public function deletePlan(Request $request, $id) {



        if (!Gate::allows('plan_manage')) {

            return abort(401);

        }



        $plan = Plan::findOrFail($id);

        if ($plan->delete()) {

            PlanCommission::where('commission_plan_id_from', $id)->orwhere('commission_plan_id_to', $id)->delete();

            CommissionSetting::where('commission_plan_id_from', $id)->orwhere('commission_plan_id_to', $id)->delete();

            Alert::success('Plan', 'Deleted successfully');

            return back()->with('message', 'Plan Deleted successfully.');

        } else {

            $error = "Something wrong! Please try later ";

            return back()->with('message', 'Something wrong! Please try later.');

        }

    }



    public function changeStatus(Request $request) {

        $plan = Plan::find($request->plan_id);

        $plan->plan_status = $request->status;

        $plan->save();

    }



    public function show(Plan $plan) {



        $serialNumbers = PlanSerialNumbers::where('plan_id', $plan->id)->get();

        return view('admin.plans.show', compact('plan', 'serialNumbers'));

    }



    public function uploadSerialNumbers(Request $request, $id) {



//        if (!Gate::allows('bulkupload_manage')) {

//            return abort(401);

//        } else {



        request()->validate([

            'csv_import' => 'required|file',

                ], [

            'csv_import.required' => 'CSV file is required',

        ]);



        ini_set('memory_limi', -1);



        $uploadedFile = $request->file('csv_import');



        $status = $request->status;



        $action_type = $request->action_type ?? 'add';



        $extension = $uploadedFile->getClientOriginalExtension();



        if (strtolower($extension) != 'csv') {



            return redirect()->back()->with('error', 'Uploaded file is not CSV file.');

        }



        $inputFileName = $uploadedFile->move(public_path('uploads'), 'serialnumbers.csv');



        if (!mb_check_encoding(file_get_contents($inputFileName), 'UTF-8')) {

            return redirect()->back()->with('error', 'Uploaded file is not UTF-8 formate.');

        }



        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($inputFileName);



        $rows = $spreadsheet->getSheet(0)->toArray();



        $headers = array_shift($rows);



        $data = $this->importSerialNumbers($rows, $id);



        unlink($inputFileName);

        if (empty($data)) {

            Alert::success('Serial Numbers', 'Uploaded successfully');

            return redirect()->route('admin.plans.show', $id)->with('message', 'Uploaded successfully.');

        } else {

            $error = implode(',', $data);

            Alert::error('Serial Numbers Error', 'Following row data not save ' . $error);

            return redirect()->route('admin.plans.show',$id)->with('message', 'Following row data not save ' . $error);

        }

//        }

    }



    /* start serial number bulk upload add and update */



    public function importSerialNumbers($rows, $planid) {

        $bulk_serial_number_records = [];

        $error = null;

        foreach ($rows as $index => $value) {

            //dd($value);

            if (isset($value[1])) {

                $date = [

                    'plan_id' => $planid,

                    'serial_number' => $value[1],

                    'created_at' => now()->toDateTimeString(),

                    'updated_at' => now()->toDateTimeString(),

                ];

                $bulk_serial_number_records[] = $date;

                $exist = PlanSerialNumbers::where('plan_id', $planid)->where('serial_number', $value[1])->first();

                if (empty($exist)) {

                    PlanSerialNumbers::Create($date);

                } else {

                    $error[] = $index + 2;

                }

            } else {

                $error[] = $index + 2;

            }

        }

        if (count($bulk_serial_number_records) > 0) {

//            PlanSerialNumbers::insert($bulk_serial_number_records);

        }



        return $error;

    }



}

