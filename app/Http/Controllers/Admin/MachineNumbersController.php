<?php



namespace App\Http\Controllers\Admin;



use Spatie\Permission\Models\Permission;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Models\Plan;

use App\Models\PlanCommission;

use App\Models\CommissionSetting;

use App\Models\BrandType;

use App\Models\PlanBrandTypes;

use App\Models\PlanSerialNumbers;

use App\Models\MachineNumbers;

use Illuminate\Support\Facades\Gate;

use Illuminate\Support\Str;

Use Alert;

use File;

use DB;



class MachineNumbersController extends Controller {



    public function index() {

        return redirect()->action('Admin\MachineNumbersController@machineNumberDetails');

        $machineNumbers = MachineNumbers::select('machine_numbers.*','users.name as username','users.id as u_id')
            ->leftJoin('user_kyc_details as ky', 'ky.machine_number', '=', 'machine_numbers.machine_number')
            ->leftJoin('users', 'users.id', '=', 'ky.user_id')
            ->orderBy('id', 'DESC')->get();

        return view('admin.machine-numbers.index', compact('machineNumbers'));
    }

    public function machineNumberDetails(Request $request) {
        $machineNumbers = MachineNumbers::select('machine_numbers.*','users.name as username','users.id as u_id')
            ->leftJoin('user_kyc_details as ky', 'ky.machine_number', '=', 'machine_numbers.machine_number')
            ->leftJoin('users', 'users.id', '=', 'ky.user_id')
            ->orderBy('id', 'DESC')->get();
        return view('admin.machine-numbers.machineNumbers-list', compact('machineNumbers'));
    }

    public function create()
    {
        return view('admin.machine-numbers.create');
    }

    public function store(Request $request) {
        request()->validate([
            'quantity' => 'required',
    	]);
        $quantity = $request->quantity;
		$status = $request->status;
		$i = 0;
		foreach($quantity as $row){
			$date = [
				'machine_number' => uniqid(),
				'quantity' => $row,
				'status' => $status[$i++],
				'created_at' => now()->toDateTimeString(),
				'updated_at' => now()->toDateTimeString(),
			];
			MachineNumbers::Create($date);
		}
        
        Alert::success('Machine Number', 'Added successfully');
        return redirect()->route('admin.machine-numbers.index')->with('message', 'Machine Number created successfully.');
    }

    public function edit($id)
    {
        $machineNumbers = MachineNumbers::select('machine_numbers.*','users.name as username','users.id as u_id')
            ->leftJoin('user_kyc_details as ky', 'ky.machine_number', '=', 'machine_numbers.machine_number')
            ->leftJoin('users', 'users.id', '=', 'ky.user_id')
            ->where('machine_numbers.id',$id)->first();

       	return view('admin.machine-numbers.edit', compact('machineNumbers'));
    }

    public function update(Request $request, $id){
        request()->validate([
            'quantity' => 'required',
	        'status' => 'required',
        ]);

	   $update = MachineNumbers::findOrFail($id);
        $update->quantity = $request->quantity;
        $update->status = $request->status;
        $update->save();
        Alert::success('Machine Number', 'Updated successfully'); 
        return redirect()->route('admin.machine-numbers.index')->with('message', 'Machine Number updated successfully.');
    }


    public function destroy($id) {
        MachineNumbers::where('id', $id)->delete();
        return redirect()->route('admin.machine-numbers.index')->withMessage('Deleted successfully!');
    }




    public function uploadMachineNumbers(Request $request) {
	request()->validate([
            'csv_import' => 'required|file',
                ], [
            'csv_import.required' => 'CSV file is required',
        ]);
        ini_set('memory_limi', -1);
        $uploadedFile = $request->file('csv_import');
        $extension = $uploadedFile->getClientOriginalExtension();
        if (strtolower($extension) != 'csv') {
            return redirect()->back()->with('error', 'Uploaded file is not CSV file.');
        }
        $inputFileName = $uploadedFile->move(public_path('uploads'), 'meternumbers.csv');
        if (!mb_check_encoding(file_get_contents($inputFileName), 'UTF-8')) {
            return redirect()->back()->with('error', 'Uploaded file is not UTF-8 formate.');
        }
        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($inputFileName);
        $rows = $spreadsheet->getSheet(0)->toArray();
        $headers = array_shift($rows);
        $data = $this->importMachineNumbers($rows);
        unlink($inputFileName);
        if (empty($data)) {
            Alert::success('Machine Numbers', 'Uploaded successfully');
            return redirect()->route('admin.machine-numbers.index')->with('message', 'Uploaded successfully.');
        } else {
            $error = implode(',', $data);
            Alert::error('Machine Numbers Error', 'Following row data not save ' . $error);
            return redirect()->route('admin.machine-numbers.index')->with('message', 'Following row data not save ' . $error);
        }
    }
    /* start machine number bulk upload add and update */
    public function importMachineNumbers($rows) {
        $bulk_serial_number_records = [];
        $error = null;
	
        foreach ($rows as $index => $value) {
            //dd($value);
            if (isset($value[0])) {
                $date = [
                    'customer_name' => $value[0],
		    'machine_number' => $value[1],
		    'quantity' => $value[2],
		    'plan' => $value[3],
		    'status' => $value[4],	
                    'created_at' => now()->toDateTimeString(),
                    'updated_at' => now()->toDateTimeString(),
                ];
                $bulk_serial_number_records[] = $date;
                $exist = MachineNumbers::where('machine_number', $value[0])->first();
                if (empty($exist)) {
                    MachineNumbers::Create($date);
                } else {
                    $error[] = $index + 2;
                }
            } else {
                $error[] = $index + 2;
            }
        }
        if (count($bulk_serial_number_records) > 0) {
		//            PlanSerialNumbers::insert($bulk_serial_number_records);
        }
        return $error;
    }
}

