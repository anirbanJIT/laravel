<?php

namespace App\Http\Controllers\Admin;

use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
Use Illuminate\Support\Str;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\File;
use App\User;
use App\Models\Plan;
use App\Models\PlanCommission;
use App\Models\CommissionSetting;
use App\Models\Currency;
use App\Models\Privacypolicy;  
use App\FCM;

class SettingController extends Controller {

    public function creditPoint() {
        if (!Gate::allows('setting_manage')) {
            return abort(401);
        }

        $data = DB::table('credit-point-setting')->first();

        return view('admin.setting.credit-point', compact('data'));
    }

    public function creditPointSetting(Request $request) {
        //dd($request);
        if (!Gate::allows('setting_manage')) {
            return abort(401);
        }

        $data = DB::table('credit-point-setting')->where('id', $request->id)->update(['credit' => $request->credit, 'amount' => $request->amount]);

        return redirect('admin/setting/credit-point')->withMessage('Updated successfully!');
    }

    public function currencySetting() {
        if (!Gate::allows('currency_manage')) {
            return abort(401);
        }

        $data = Currency::first();

        return view('admin.setting.currency', compact('data'));
    }

    public function updateCurrencySetting(Request $request) {
        //dd($request);
        if (!Gate::allows('currency_manage')) {
            return abort(401);
        }

        $data = Currency::where('id', $request->id)->update(['name' => $request->name, 'rate' => $request->rate]);

        return redirect('admin/setting/currency')->withMessage('Updated successfully!');
    }

    public function commission($user_id = NULL) {
        if (!Gate::allows('setting_manage')) {
            return abort(401);
        }

        if ($user_id) {

            $user = User::select('current_plan')->where('id', $user_id)->first();

            if (!isset($user->current_plan)) {
                return back()->withMessage('Please first select plan and thereafter set commission!');
            }
            $current_plan = $user->current_plan;

            $data = PlanCommission::select('commission_plan_id_from', 'commission_plan_id_to', 'discount_type', 'discount_value')->where('commission_plan_id_from', $current_plan)->orderBY('commission_plan_id_from')->get();

            return view('admin.setting.commission', compact('data', 'user_id', 'user'));
        } else {
            $data = DB::table('commission-setting')->where('user_id', NULL)->first();
            return view('admin.setting.commission-old', compact('data', 'user_id'));
        }
    }

    public function commissionSetting(Request $request) {
        if (!Gate::allows('setting_manage')) {
            return abort(401);
        }
        if ($request->user_id) {

            $input_all = $request->commission_plan;

            foreach ($input_all as $key => $value) {
                $commission_plan = CommissionSetting::firstOrNew([
                            'user_id' => $value['user_id'],
                            'commission_plan_id_from' => $value['commission_plan_id_from'],
                            'commission_plan_id_to' => $value['commission_plan_id_to']
                ]);
                $commission_plan->commission = $value['discount_value'];
                $commission_plan->commission_type = $value['discount_type'];
                $commission_plan->minimum_recharge = $value['minimum_recharge'];
                $commission_plan->save();
            }

            return redirect('admin/setting/commission/' . $request->user_id)->withMessage('Updated successfully!');
        } else {
            $data = DB::table('commission-setting')->where('id', $request->id)->update([
                'commission' => $request->commission,
                'minimum_recharge' => $request->minimum_recharge,
                'recharge_month' => $request->recharge_month,
                'status' => $request->status]);
            return redirect('admin/setting/commission/')->withMessage('Updated successfully!');
        }
    }

    public function commissionSettingReport() {
        if (!Gate::allows('setting_manage')) {
            return abort(401);
        }

        $datas = DB::table('commission-setting')->leftJoin('users', 'commission-setting.user_id', '=', 'users.id')->select('commission-setting.*', 'users.name as name')->get();
        return view('admin.retailers.commission-setting-report', compact('datas'));
    }

    public function privacypolicy() {
        /* $data = Privacypolicy::first(); */

        $data = DB::table('privacypolicy')->first();
		
        return view('admin.setting.privacypolicy', compact('data'));
    }
    public function settingpdf(Request $request) {
        /* $data = Privacypolicy::first(); */

        $settingPDF = DB::table('settingpdf')->orderBy('position_order','ASC')->get();

        return view('admin.setting.settingpdf', compact('settingPDF'));
    }
    
    public function updateSettingPdf(Request $request) {

        $this->validate($request, [
            'settingpdf'=>'required',
        ]);
        if($request->hasFile('settingpdf')){
            $i=1;
            $images=array();
            $allowedfileExtension=['jpeg','jpg','png'];
            $files = $request->file('settingpdf');
            if($files){
                foreach($files as $file){
                    $extension = $file->getClientOriginalExtension();
                    $name = time() . '_'.$i++.'.'.$extension;
                    $check = in_array($extension,$allowedfileExtension);
                    
                    if($check){
                        $file->move(public_path('/Admin/settingpdf/'),$name);
                        $images[] = $name;
						$data = DB::table('settingpdf')->insert(array('settingpdf' => $name));
                    }else{
                        return redirect()->route('admin.setting.setting_pdf')->with('error','Sorry Only Upload png , jpg , jpeg');
                    }
                }
            }
        }
        return redirect()->route('admin.setting.setting_pdf','')->with('success','Updated successfully!');
    }
	
	public function updateSettingPdfOrder(Request $request){
		$count = 1; 
		$id_array = isset($request->ids) ? explode(',',$request->ids):[];
		
        foreach ($id_array as $id){ 
			$update = DB::table('settingpdf')->where("id",$id)->update(["position_order" => $count]); 
            $count ++;     
        } 
		return response()->json(["success" => true]);
	}
	
	public function updateSettingPdfDelete(Request $request){
		$id = $request->id;
		DB::table('settingpdf')->where("id",$id)->delete();
		return response()->json(["success" => true]);
	}
	
    public function updatePrivacypolicy(Request $request) {

		$privacy_policy = $request->privacy_policy;
        $data = DB::table('privacypolicy')->where('id', $request->id)->update(array('privacy_policy' => $privacy_policy));
		
		DB::table('users')->select('fcm_token')->whereNotNull('fcm_token')->orderBy('id')->chunk(500, function ($users) use($privacy_policy) {
			
			$userTokens = $users->pluck('fcm_token')->toArray();
			$fcm = new FCM();
			$fcm->token = $userTokens;
			$fcm->notification['title'] = "Privacy Policy Changed";
			$fcm->notification['body'] = $privacy_policy;
			$fcm->send();
		});

        if($data){
			// all users need to confirm privacy policy again if it changed
            DB::table('users')
                    ->update(array('privacy_policy' => NULL));
        }

        return redirect('admin/setting/privacypolicy')->withMessage('Updated successfully!');
    }

    public function listprivacypolicy() {
        /* $data = Privacypolicy::first(); */

        $data = DB::table('privacypolicy')->first();

        if (!empty($data)) {

            $privacy_policy_list['responseCode'] = "200";
            $privacy_policy_list['responseMessage'] = "Success";
            $privacy_policy_list['responseDescription'] = "Success";
            $privacy_policy_list['data'] = $data;
        } else {

            $privacy_policy_list['responseCode'] = "100";
            $privacy_policy_list['responseMessage'] = "Success";
            $privacy_policy_list['responseDescription'] = "No data found";
        }

        return $privacy_policy_list;
    }
	
	public function websiteSetting(){
		$users = DB::table('users')->select("id","name","shop_name","email","phone")->get();
		return view('admin.setting.website', compact('users'));
	}
	
	public function otppriceSetting(){
		return view('admin.setting.otpprice');
	}
	
	public function otppriceUpdate(Request $request){
		$otp = $request->noofotp;
		DB::table('users')->update(['no_of_otp_send' => $otp]);
		return redirect()->route('admin.setting.otpprice')->with('success','Updated successfully!');
	}

}
