<?php

namespace App\Http\Controllers\Admin;

use DB;
use App\Models\BrandType;
use App\Models\Brand;
use App\Models\BrandModel;
use App\Models\BrandSubModel;
use App\Models\BrandModelScreenImage;
use App\Models\ModelScreenImage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
Use Illuminate\Support\Str;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Symfony\Component\HttpFoundation\Response;
use Imagick;
Use Alert;
use File;
use storage;

class BrandModelScreenImageController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
		return redirect()->action('Admin\BrandModelScreenImageController@modelScreenImageDetails');
        if (!Gate::allows('brand_models_screen_image')) {
            return abort(401);
        }

        $brandModelScreen = BrandModelScreenImage::with('brandsTypes', 'brands', 'brandsModel')->orderBy('order_number', 'DESC')->orderBy('id', 'DESC')->get();

        return view('admin.models-screen-image.index', compact('brandModelScreen'));
    }
	public function modelScreenImageDetails(Request $request) {
		if (!Gate::allows('brand_models_screen_image')) {
            return abort(401);
        }
        $submodule_id = isset($request->id) ? $request->id: '';
        if($submodule_id){
            $brandModelScreen = BrandModelScreenImage::with('brandsTypes', 'brands', 'brandsModel','brandSubModel')->where("sub_model_id",$submodule_id)->select(DB::raw('CONCAT(id,"_",status) AS id_status'),'id','upload_by','brand_type_id','brand_id','model_id','sub_model_id','name','position_order','image','blt_file','width','height','status','download','meta_title','meta_description','meta_keywords','order_number','width_height')->orderBy('position_order','ASC')->get();
			return view('admin.models-screen-image.modelScreenImage-list', compact('brandModelScreen','submodule_id'));
        }else{
			return redirect()->action('Admin\BrandTypeController@brandTypeDetails');
		}
		
        
    }
	
	public function modelsScreenImageOrderPosition(Request $request){
		$position_order = $request->position;
		$i=1;
		
		foreach($position_order as $k=>$v){
			BrandModelScreenImage::where("id",$v)->update(["position_order" => $i++]);	
		}
		
		return redirect()->back()->with('message', 'Item Position Update.');
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request) {
        if (!Gate::allows('brand_models_screen_image')) {
            return abort(401);
        }
		$submodel_id = isset($request->submodel_id) ? $request->submodel_id :0;
		
        $brand_types = BrandType::select('id', 'name')->where('status', 1)->get();
        $brands = Brand::select('id', 'name')->where('status', 1)->get();
        $brandModel = BrandModel::select('id', 'name')->where('status', 1)->get();
        $brandSubModel = BrandSubModel::select('id', 'name')->where('status', 1)->get();

        $selected_submodel = BrandSubModel::select('id', 'name','brand_models_id','brand_id','brand_type_id')->where('id', $submodel_id)->first();

        return view('admin.models-screen-image.create', compact('brand_types', 'brands', 'brandModel', 'brandSubModel','selected_submodel'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        if (!Gate::allows('brand_models_screen_image')) {
            return abort(401);
        }

        $validator = $request->validate([
            'brand_type_id' => 'required',
            'brand_id' => 'required',
            'model_id' => 'required',
            'sub_model_id' => 'required',
            'name' => 'required',
            'status' => 'required',
            'blt_file' => 'required',
            'filenames' => 'required',
            'name' => 'required',
            'width' => 'required',
            'height' => 'required'
        ]);


        if ($request->file('filenames')) {
            $k = 1;
            foreach ($request->file('filenames') as $file) {
                $destinationPath = public_path('Admin/images/models-screen-image/'); // upload path
                $profileImage = time() . $k . '.' . $file->getClientOriginalExtension();
//                $file->move($destinationPath, $profileImage);
                $file_location = $destinationPath . $profileImage;
                move_uploaded_file($file->getPathName(), $file_location);
                $dataImage[] = $profileImage;
                $k++;
            }
        }

        if ($request->file('blt_file')) {
            $k = 1;
            foreach ($request->file('blt_file') as $file) {
                $destinationPath = public_path('Admin/images/models-screen-image/'); // upload path
                $blt_file = time() . $k . 'blt.' . $file->getClientOriginalExtension();
//                $file->move($destinationPath, $blt_file);
                $file_location = $destinationPath . $blt_file;
                move_uploaded_file($file->getPathName(), $file_location);
                $dataBltImage[] = $blt_file;
                $k++;
            }
        }
		$lastRecord = BrandModelScreenImage::orderBy('position_order','DESC')->first();

        $model = new BrandModelScreenImage;
        $model->brand_type_id = $request->brand_type_id;
        $model->brand_id = $request->brand_id;
        $model->model_id = $request->model_id;
        $model->sub_model_id = $request->sub_model_id;
        $model->meta_title = $request->meta_title ?? null;
        $model->meta_keywords = $request->meta_keywords ?? null;
        $model->meta_description = $request->meta_description ?? null;
        if ($request->has('name')) {
            $model->name = json_encode($request->name, JSON_FORCE_OBJECT);
        } else {
            $model->name = json_encode([], JSON_FORCE_OBJECT);
        }

        if (!empty($dataImage)) {
            $model->image = json_encode($dataImage, JSON_FORCE_OBJECT);
        } else {
            $model->image = json_encode([], JSON_FORCE_OBJECT);
        }
        if (!empty($dataBltImage)) {
            $model->blt_file = json_encode($dataBltImage, JSON_FORCE_OBJECT);
        } else {
            $model->blt_file = json_encode([], JSON_FORCE_OBJECT);
        }
        if ($request->has('width')) {
            $model->width = json_encode($request->width, JSON_FORCE_OBJECT);
        } else {
            $model->width = json_encode([], JSON_FORCE_OBJECT);
        }
        if ($request->has('height')) {
            $model->height = json_encode($request->height, JSON_FORCE_OBJECT);
        } else {
            $model->height = json_encode([], JSON_FORCE_OBJECT);
        }
        $model->status = $request->status;
        $model->position_order = isset($lastRecord->position_order) ? ($lastRecord->position_order + 1):1;
        //print_r($model);exit;
        $model->save();

        if ($model->id) {
            if (!empty($dataImage)) {
                $width = $request->width;
                $height = $request->height;
                $screen_name = $request->name;
                foreach ($dataImage as $key => $image) {
                    if ($image != "") {
                        $imageData['brand_model_screen_image_id'] = $model->id;
                        $imageData['screen_name'] = isset($screen_name[$key]) ? $screen_name[$key] : "";
                        $imageData['image_file'] = $image;
                        $imageData['blt_file'] = $dataBltImage[$key];
                        $imageData['width'] = isset($width[$key]) ? $width[$key] : "";
                        $imageData['height'] = isset($height[$key]) ? $height[$key] : "";
                        ModelScreenImage::Create($imageData);
                    }
                }
            }
        }

        if(isset($request->get_sub_model_id))
            return redirect()->route('admin.models-screen-image-list','id='.$request->get_sub_model_id)->withMessage('Created successfully!');
        else
           return redirect()->back()->withMessage('Created successfully!'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        if (!Gate::allows('brand_models_screen_image')) {
            return abort(401);
        }
		
		$brandModelScreenImage = BrandModelScreenImage::with('brandsTypes', 'brands', 'brandsModel','brandSubModel')->where('id', $id)->select(DB::raw('CONCAT(id,"_",status) AS id_status'),'id','upload_by','brand_type_id','brand_id','model_id','sub_model_id','name','position_order','image','blt_file','width','height','status','download','meta_title','meta_description','meta_keywords','order_number','width_height')->first();

        return view('admin.models-screen-image.show', compact('brandModelScreenImage','id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        if (!Gate::allows('brand_models_screen_image')) {
            return abort(401);
        }
        $brand_types = BrandType::select('id', 'name')->where('status', 1)->get();
        $brands = Brand::select('id', 'name')->where('status', 1)->get();
        $brandsModel = BrandModel::select('id', 'name')->where('status', 1)->get();
        $brandSubModel = BrandSubModel::select('id', 'name')->where('status', 1)->get();
        $brandModelScreen = BrandModelScreenImage::FindOrFail($id);
        $brandImages = $brandModelScreen->brandsImages()->get();

        return view('admin.models-screen-image.edit', compact('brand_types', 'brands', 'brandsModel', 'brandSubModel', 'brandModelScreen', 'brandImages'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        if (!Gate::allows('brand_models_screen_image')) {
            return abort(401);
        }

        //dd($request->all());
        $validator = Validator::make($request->all(), [
                    'brand_type_id' => 'required',
                    'brand_id' => 'required',
                    'model_id' => 'required',
                    'sub_model_id' => 'required',
                    //'model_id' => 'required,unique:brand_model_screen_images,'.$id.',id',
                    'old_name' => 'required',
                    'status' => 'required',
                    'fileUpload' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        if ($validator->fails()) {
            //dd("bbb");
            return Redirect::back()->withErrors($validator);
        } else {
            
            $post = $request->post();

            if ($request->file('filenames')) {
                $k = 1;
                foreach ($request->file('filenames') as $file) {
                    $destinationPath = public_path('Admin/images/models-screen-image/'); // upload path
                    $profileImage = time() . $k . '.' . $file->getClientOriginalExtension();

//                    $file->move($destinationPath, $profileImage);
                    $file_location = $destinationPath . $profileImage;
                    move_uploaded_file($file->getPathName(), $file_location);
                    $dataImage[] = $profileImage;
                    $k++;
                }
            }


            if (!empty($request->file('blt_file'))) {
                /* $file = $request->file('blt_file');
                  $destinationPath = public_path('images/models-screen-image'); // upload path
                  $blt_file = time().'blt.'.$file->getClientOriginalExtension();
                  $file->move($destinationPath, $blt_file); */
                $k = 1;
                foreach ($request->file('blt_file') as $file) {
                    $destinationPath = public_path('Admin/images/models-screen-image/'); // upload path
                    $blt_file = time() . $k . 'blt.' . $file->getClientOriginalExtension();
//                    $file->move($destinationPath, $blt_file);
                    $file_location = $destinationPath . $blt_file;
                    move_uploaded_file($file->getPathName(), $file_location);
                    $dataBltFile[] = $blt_file;
                    $k++;
                }
            }

            if ($request->has('old_name') && !empty($request->old_name)) {
                $old_name = explode(",", $request->old_name);
                foreach ($old_name as $old_key => $name) {
                    $name = trim($name);
                    if (empty($name)) {
                        unset($old_name[$old_key]);
                    } else {
                        $old_name[$old_key] = $name;
                    }
                }
            } else {
                $old_name = [];
            }

            if ($request->has('old_height') && !empty($request->old_height)) {
                $old_height = $request->old_height;
            } else {
                $old_height = [];
            }

            if ($request->has('old_width') && !empty($request->old_width)) {
                $old_width = $request->old_width;
            } else {
                $old_width = [];
            }

            $model = BrandModelScreenImage::find($id);
            $model->brand_type_id = $request->brand_type_id;
            $model->brand_id = $request->brand_id;
            $model->model_id = $request->model_id;
            $model->sub_model_id = $request->sub_model_id;
            $model->meta_title = $request->meta_title ?? null;
            $model->meta_keywords = $request->meta_keywords ?? null;
            $model->meta_description = $request->meta_description ?? null;

            if ($request->has('old_name')) {

                $dataName = $request->name;

                //$old_name = json_decode($model->name,TRUE);
                //$old_name = !empty($old_name) ? $old_name : [];

                $new_name = array_merge($old_name, $dataName);

                $model->name = json_encode($new_name, JSON_FORCE_OBJECT);
            }

            if ($request->file('filenames')) {

                $old_filenames = json_decode($model->image, TRUE);

                $old_filenames = !empty($old_filenames) ? $old_filenames : [];

                $new_filenames = array_merge($old_filenames, $dataImage);

                $model->image = json_encode($new_filenames, JSON_FORCE_OBJECT);
            }
            if ($request->file('blt_file')) {

                $old_filenames = json_decode($model->blt_file, TRUE);

                $old_filenames = !empty($old_filenames) ? $old_filenames : [];

                $new_filenames = array_merge($old_filenames, $dataBltFile);

                $model->blt_file = json_encode($new_filenames, JSON_FORCE_OBJECT);
            }

            if ($request->has('width')) {

                $data_width = $request->width;

                $new_width = array_merge($old_width, $data_width);

                $model->width = json_encode($new_width, JSON_FORCE_OBJECT);
            }

            if ($request->has('height')) {

                $data_height = $request->height;

                $new_height = array_merge($old_height, $data_height);

                $model->height = json_encode($new_height, JSON_FORCE_OBJECT);
            }
            $model->status = $request->status;
            $model->order_number = $request->order_number;
            $model->save();

            if (!empty($post['old_id'])) {
                $width = $request->old_width;
                $height = $request->old_height;
                $screen_name = explode(",", $request->old_name);
                foreach ($post['old_id'] as $key => $id) {
                    $imageData['screen_name'] = isset($screen_name[$key]) ? trim($screen_name[$key]) : "";
                    $imageData['width'] = isset($width[$key]) ? $width[$key] : "";
                    $imageData['height'] = isset($height[$key]) ? $height[$key] : "";
                    ModelScreenImage::where('id', $id)->update($imageData);
                }
            }
            
            if (!empty($dataImage)) {
                $width = $request->width;
                $height = $request->height;
                $screen_name = $request->name;
                foreach ($dataImage as $key => $image) {
                    if ($image != "") {
                        $imageData['brand_model_screen_image_id'] = $model->id;
                        $imageData['screen_name'] = isset($screen_name[$key]) ? $screen_name[$key] : "";
                        $imageData['image_file'] = $image;
                        $imageData['blt_file'] = isset($dataBltFile[$key]) ? $dataBltFile[$key] : "";
                        $imageData['width'] = isset($width[$key]) ? $width[$key] : "";
                        $imageData['height'] = isset($height[$key]) ? $height[$key] : "";
                        ModelScreenImage::Create($imageData);
                    }
                }
            }

            return redirect()->route('admin.models-screen-image-list','id='.$request->get_sub_model_id)->withMessage('Updated successfully!');
            
        }
    }

    public function addMoreImage(Request $request) {


        if (!Gate::allows('brand_models_screen_image')) {
            return abort(401);
        }

        if ($request->file('filenames')) {
            $k = 1;
            foreach ($request->file('filenames') as $file) {
                $destinationPath = public_path('Admin/images/models-screen-image'); // upload path
                $profileImage = time() . $k . '.' . $file->extension();

                $file->move($destinationPath, $profileImage);
                $dataImage[] = $profileImage;
                $k++;
            }
        }

        $model = BrandModelScreenImage::find($request->id);
        if ($model->image) {
            $arr = json_decode($model->image, TRUE);
            $arr = array_merge($arr, $dataImage);
        } else {
            $arr = $dataImage;
        }
        if ($request->file('filenames')) {
            $model->image = json_encode($arr, JSON_FORCE_OBJECT);
        }

        $model->save();
        return redirect()->back()->withMessage('Image uploaded successfully!');
        ;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(BrandModelScreenImage $brandModelScreenImage,$id) {
        if (!Gate::allows('brand_models_screen_image')) {
            return abort(401);
        }

        /* $brandModelScreenImage->delete();

        return redirect()->route('admin.models-screen-image.index'); */
		$imageQry = BrandModelScreenImage::where('id', $id)->first();
		
		$updaterecords = BrandModelScreenImage::where("position_order",'>',$imageQry->position_order)->orderBy("position_order",'ASC')->get();
		
		foreach($updaterecords as $row){
			$update = BrandModelScreenImage::where("id",$row->id)->update(["position_order"=>($row->position_order-1)]);
		} 
		
        if ($imageQry) {
            if ($imageQry->image) {
                $image_path = public_path('Admin/images/models-screen-image/') . $imageQry->image;
                if (File::exists($image_path)) {
                    File::delete($image_path);
                }
            }
            $imageQry->delete();
        }
        return redirect()->back()->withMessage('Deleted successfully!');
    }

    public function deleteModelScreen($id) {
        if (!Gate::allows('brand_models_screen_image')) {
            return abort(401);
        }
        $imageQry = BrandModelScreenImage::where('id', $id)->first();
        if ($imageQry) {
            if ($imageQry->image) {
                $image_path = public_path('Admin/images/models-screen-image/') . $imageQry->image;
                if (File::exists($image_path)) {
                    File::delete($image_path);
                }
            }
            $imageQry->delete();
        }
        return redirect()->route('admin.models-screen-image.index')->withMessage('Deleted successfully!');
    }

    public function deleteModelScreenBltFiles(Request $request) {
        if (!Gate::allows('brand_models_screen_image')) {
            return abort(401);
        }

        $brandModel = BrandModelScreenImage::where('id', $request->id)->first();
        if ($brandModel->blt_file) {
            $bltArray = json_decode($brandModel->blt_file);
            $imageArray = json_decode($brandModel->image);
            if ($bltArray === NULL) {
                $image_path = public_path('Admin/images/models-screen-image/') . $brandModel->blt_file; // upload pa	
                //dd($image_path);
                if (File::exists($image_path)) {
                    File::delete($image_path);
                    $model = BrandModelScreenImage::find($request->id);
                    $model->blt_file = json_encode([]);
                    $model->save();
                }
            } else {
                foreach ($bltArray as $key => $value) {
                    if ($value == $request->blt_file) {
                        $image_path = public_path('Admin/images/models-screen-image/') . $value; // upload pa	
                        //dd($image_path);
                        if (File::exists($image_path)) {
                            unset($bltArray->$key);
                            unset($imageArray->$key);
                            File::delete($image_path);
                            $model = BrandModelScreenImage::find($request->id);
                            $model->blt_file = json_encode($bltArray);
                            $model->image = json_encode($imageArray);
                            $model->save();
                        }
                    }
                }
            }
        }

        return redirect()->route('admin.models-screen-image.show', $request->id)->withMessage('Deleted successfully!');
        ;
    }

    public function deleteModelScreenImage(Request $request) {

        if (!Gate::allows('brand_models_screen_image')) {
            return abort(401);
        }

        $brandModel = BrandModelScreenImage::where('id', $request->id)->first();
        if ($brandModel->image) {
            $nameArray = json_decode($brandModel->name);
            $imageArray = json_decode($brandModel->image);
            $bltArray = json_decode($brandModel->blt_file);
            foreach ($imageArray as $key => $value) {
                if ($value == $request->image) {
                    $image_path = public_path('Admin/images/models-screen-image/') . $value; // upload pa	
                    //dd($image_path);
                    if (File::exists($image_path)) {
                        unset($imageArray->$key);
                        File::delete($image_path);
                        $model = BrandModelScreenImage::find($request->id);
                        $model->image = json_encode($imageArray);
                        if (isset($bltArray->$key)) {
                            unset($bltArray->$key);
                            $model->blt_file = json_encode($bltArray);
                        }
                        if (isset($nameArray->$key)) {
                            unset($nameArray->$key);
                            $model->name = json_encode($nameArray);
                        }
                        $model->save();
                    }
                }
            }
        }

        return redirect()->route('admin.models-screen-image.show', $request->id)->withMessage('Deleted successfully!');
    }

    public function massDestroy(Request $request) {
        if (!Gate::allows('brand_models_screen_image')) {
            return abort(401);
        }
		$ids = $request->ids;
        BrandModelScreenImage::whereIn('id', explode(",",$ids))->delete();
        
        return response()->json(['success'=>'Deleted successfully']);
    }

    public function getBrandTypes(Request $request) {

        $brand_type = Brand::select('id', 'name')->where('brand_type_id', '=', $request->brand_type)->where('status', '1')->whereNull('deleted_at')->get();
        //dd($brand_type);
        return $brand_type;
    }

    public function getBrand(Request $request) {
        //dd($request->brand);
        $brand = BrandModel::select('id', 'name')->where('brand_id', '=', $request->brand)->where('status', '1')->whereNull('deleted_at')->get();

        return $brand;
    }

    public function changeStatusModelScreen(Request $request) {
        $model = BrandModelScreenImage::find($request->modelScreen_id);

        $model->status = $request->status;

        $model->save();



        return response()->json(['success' => 'Status change successfully.']);
    }

}
