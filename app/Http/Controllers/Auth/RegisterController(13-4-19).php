<?php

namespace App\Http\Controllers\Auth;
use Illuminate\Http\Request;
use App\User;
use JWTAuth;
use Auth;
use App\Models\UserLevel;
use App\Models\CashWallet;
use App\Models\CreditWallet;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Auth\Events\Registered;
use Illuminate\Contracts\Encryption\DecryptException;
use App\Notifications\RegistrationOTP;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    //protected $redirectTo = '/admin/home';
	
	public function redirectTo(){
        if(Auth::user()->hasRole('retailer')){
           return route('user.home');
        }else{
           return route('admin.home');
        }
       }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
			'phone' => 'required|max:15',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|min:6|required_with:password_confirmation|same:password_confirmation',
			'password_confirmation' => 'min:6'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
	 
	  public function showRegistrationForm()
    {
		//$data['ref_id']=$ref_id;
        return view('auth.register');
    }
	
	
	
    protected function create(Request $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }
	
	public function register(Request $request)
    {
        $this->validator($request->all())->validate();
		
		if($request->referred_by!='')
		{
			if($this->checkuser($request->referred_by)) {
				$referred_by = $request->referred_by;
			}
			else{
				return back()->withInput()->withMessage('Referral id not found');
			}
		}
		else
		{
			$referred_by = '1234567';
		}
		$referral_id = $this->generateReferralNumber();
		if($referred_by) {
		//$token = JWTAuth::fromUser($user);
        $user = new User();
		$user->referred_by = $referred_by;
        $user->name = $request->name;
        $user->email = $request->email;
		$user->phone = $request->phone;
		$user->referral_id = $referral_id;
		$user->type = 1;
        $user->password = bcrypt($request->password);
        $user->save();
		$user->assignRole('retailer');
		

        
					$input = $request->only('email', 'password');
					$token = null;
					$token = JWTAuth::attempt($input);
					$r=$referred_by;
					$l=1;
					
					while($r!='1')
					{
						if($r!='1')
						{
						UserLevel::create(['user_id' => $user->referral_id, 'upline_id' => $r, 'level' => $l]);
						}
						$userData = User::select('referred_by')->where('referral_id',$r)->first();
						$r=$userData->referred_by;
						$l++;
					}
					
					CashWallet::create(['user_id' => $user->id, 'amount' => '0']);
					CreditWallet::create(['user_id' => $user->id, 'amount' => '0']);
					
					$otp = mt_rand(100000,999999);
					$credit = json_decode($this->check_sms_credit(),true);
					//return $credit['status'];
					if($credit['status']=='success') {
					
					$sms_credit=$credit['balance']['sms'];
					
					
					if($sms_credit>0)
					{
						
					
					$message = "Thank you for registering with Modern Mart. Your OTP code is ".$otp;
					$sms = json_decode($this->send_sms($request->phone,$message),true);
					
					
					}}
					//else{
						//$user->notify(new RegistrationOTP($user,$otp));
					//}
					
					//else{
						//return "Not balance available in SMS Api";
					//}
					//return back()->withMessage('Your registration has been done successfully');
					
					$user->notify(new RegistrationOTP($user,$otp));
					$en_user_id = encrypt($user->id);
					User::where('id',$user->id)->update(['otp' => $otp,'api_token' => $token]);
					
					return redirect()->route('otp-verify',['ui' => $en_user_id]);
					//return redirect()->route('otp-verify',['ui' => $en_user_id]);
					
		}
		else
		{
					return back()->withInput()->withMessage('Referral id not found');
		}

    }
	public function showVerifyOtp()
	{
		return view('auth.verify-otp');
	}
	public function verifyOtp(Request $request)
	{
		//dd($request->u_id);
		$user_id = decrypt($request->ui);
	$user = User::find($user_id);
	if($user->otp == $request->otp_number)
	{
	User::where('id',$user->id)->update(['otp_status' => 1]);	
	//return back()->withInput()->withMessage('Your mobile number has been verified successfully');
	Auth::login($user);
	return redirect('/user/home');
	}
	else{
		
		return back()->withInput()->withMessage('Wrong OTP number');
	}
	}
}
