<?php

namespace App\Http\Middleware;

use Closure;

class GetNotificationMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        dd("hello");
        $user = JWTAuth::parseToken()->authenticate();
        return $next($request);
    }
}
