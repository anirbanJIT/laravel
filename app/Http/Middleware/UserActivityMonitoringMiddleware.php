<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use DB;
Use File;


class UserActivityMonitoringMiddleware
{
    
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed && Auth::user()->id != 1
     */
    public function handle($request, Closure $next, $guard = null)
    {
        
        $response = $next($request);

        if (Auth::guard($guard)->check()) {
            $this->track($request);
//            return $next($request);
        } 
        return $response;
    }

    /**
     * @param Request $request
     * @param Response $response
     */
    protected function track(Request $request)
    {
        $user = Auth::user();
        $activity['user_id'] = intval($user->id);
        $activity['url'] = $request->getUri() ?? '';
        //$activity['response_http_code'] = intval($response->getStatusCode());
        //$activity['response_time'] = $this->getResponseDispatchTimeHumanized();
        //$activity['response'] = $response->getContent();
       // $activity['payload'] = json_encode($this->getRequestPayload());
        $activity['created_at'] = Carbon::now('Asia/Kolkata');
        $activity['updated_at'] = Carbon::now('Asia/Kolkata');
        DB::table('user_activity_monitor')->insert($activity);
        DB::table('user_activity_monitor')->where('created_at', '<', Carbon::now('Asia/Kolkata')->subDays(30) )->where('user_id',$user->id)->delete();
    }

    /**
     * @return string
     */
    protected function getResponseDispatchTimeHumanized()
    {
        $timeTaken = microtime(true) - LARAVEL_START;
        return number_format($timeTaken, 2) . ' seconds';
    }  

    /**
        * @return array
    */
    protected function getRequestPayload()
    {
        $payload = [];

        if (\Request::isMethod('GET')) {
            $payload = \Request::query();
        }

        if (\Request::isMethod('POST')) {
            $payload = array_merge($payload, \Request::input());
        }

        return $payload;
     }
}
