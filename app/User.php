<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use App\Models\UserContactDetail;
use App\Models\UserBankDetail;
use App\Models\UserKycDetail;
use App\Models\ShopDetail;
use App\Models\Plan;
use App\Models\UserPlanDetail;
use Spatie\Permission\Traits\HasRoles;
use Hash;
use App\Models\RechargeCommission;
use App\Models\CreditWallet;
use App\Models\CashWallet;
use App\Models\GeneralNotification;
use App\Models\UserAddonPlans;
use App\Models\PlanSerialNumbers;

/**
 * Class User
 *
 * @package App
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $remember_token
 */
class User extends Authenticatable implements JWTSubject
{

    use Notifiable;
    use HasRoles;

    protected $fillable = ['name', 'username', 'email', 'password', 'phone', 'remember_token', 'referral_id', 'otp_status', 'type', 'shop_name', 'other_phone', 'profile_pic', 'current_plan'];
    protected $hidden = [
        'password', 'remember_token',
    ];
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Hash password
     * @param $input
     */
    public function setPasswordAttribute($input)
    {
        if ($input)
            $this->attributes['password'] = app('hash')->needsRehash($input) ? Hash::make($input) : $input;
    }

    public function role()
    {
        return $this->belongsToMany(Role::class, 'role_user');
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public function getContactDetail()
    {
        return $this->hasOne(UserContactDetail::class);
    }

    public function getBankDetail()
    {
        return $this->hasOne(UserBankDetail::class);
    }

    public function getKycDetail()
    {
        return $this->hasOne(UserKycDetail::class);
    }

    public function getShopDetail()
    {
        return $this->hasOne(ShopDetail::class);
    }

    public function plan()
    {
        return $this->hasOne(Plan::class, 'id', 'current_plan');
    }

    public function getUserPlanDetail()
    {
        return $this->hasOne(UserPlanDetail::class);
    }

    public function getChildMember()
    {
        return $this->hasMany(User::class, 'referred_by', 'referral_id');
    }

    public function getTotalPaidCommission()
    {
        return $this->hasMany(RechargeCommission::class, 'user_id', 'id')->where('status', 1);
    }

    public function getTotalUnpaidCommission()
    {
        return $this->hasMany(RechargeCommission::class, 'user_id')->where('status', 0);
    }

    public function getCreditWalletBalance()
    {
        return $this->hasOne(CreditWallet::class, 'user_id', 'id');
    }

    public function getCashWalletBalance()
    {
        return $this->hasOne(CashWallet::class, 'user_id', 'id');
    }

    public function getGeneralNotification()
    {
        return $this->hasMany(GeneralNotification::class, 'user_id', 'id');
    }

    public function getAddonPlans()
    {
        return $this->hasMany(UserAddonPlans::class, 'user_id', 'id');
    }

    public function getSerialNumber()
    {
        return $this->hasOne(PlanSerialNumbers::class, 'user_id', 'id');
    }
}
