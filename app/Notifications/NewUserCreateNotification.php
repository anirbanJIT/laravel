<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Encryption\DecryptException;

class NewUserCreateNotification extends Notification
{
    use Queueable;

	private $user;
    private $password;
    private $roles;
    private $title;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user, $roles, $password)
    {
		$this->user = $user;
        $this->password = $password;
        $this->roles  = $roles;
        $this->title = $this->user->name;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
		
        return (new MailMessage)
					->greeting('Hello '.$this->title.'!')
                    ->line('Your Login Details')
                    ->line('Email Id: '.$this->user->email)
                    ->line('Password: '.$this->password)
                    ->line('Role: '.$this->roles)
                    ->action('Login Link',  route('login'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
           'message' => 'Thank you for using Modern Mart.',
            'url'  => route('login'),
        ];
    }
}
