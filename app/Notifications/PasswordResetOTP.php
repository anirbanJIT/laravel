<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Encryption\DecryptException;

class PasswordResetOTP extends Notification
{
    use Queueable;

	private $user;
    private $otp;
    private $title;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user,$otp)
    {
		$this->user         = $user;
        $this->otp  = $otp;
        $this->title = $this->user->name;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
		
        return (new MailMessage)
					->greeting('Hello '.$this->title.'!')
                    ->line('Thank you for using Modern Mart. Your OTP code is '.$this->otp);
					//->line('Click on below link and verify otp')
                    //->action('OTP Verification Link',  route('otp-verify', ['ui' => encrypt($this->user->id)]))
                    //->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
           'message' => 'Thank you for using Modern Mart. Your OTP code is '.$this->otp ,
            'url'  => route('login'),
        ];
    }
}
