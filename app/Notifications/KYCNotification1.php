<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Encryption\DecryptException;

class KYCNotification extends Notification {

    use Queueable;

    private $user;
    private $type;
    private $title;

    /**

     * Create a new notification instance.

     *

     * @return void

     */
    public function __construct($user, $type) {

        $this->user = $user;

        $this->type = $type;

        $this->title = $this->user->name;
    }

    /**

     * Get the notification's delivery channels.

     *

     * @param  mixed  $notifiable

     * @return array

     */
    public function via($notifiable) {

        return ['mail'];
    }

    /**

     * Get the mail representation of the notification.

     *

     * @param  mixed  $notifiable

     * @return \Illuminate\Notifications\Messages\MailMessage

     */
    public function toMail($notifiable) {

        if ($this->type == 'Approved') {

            return (new MailMessage)
                            ->greeting('Hello ' . $this->title . '!')
                            ->line('Thank you for providing KYC details with Modern Mart. Your KYC is approved now.')
                            ->line('Click on below link and login')
                            ->action('Login Link', route('login'))
                            ->line('Thank you for using our application!');
        } else {



            return (new MailMessage)
                            ->greeting('Hello ' . $this->title . '!')
                            ->line('Thank you for providing KYC details with Modern Mart. Sorry, your KYC is unapproved, Please provide proper kyc details.')
                            ->line('Click on below link and login')
                            ->action('Login Link', route('login'))
                            ->line('Thank you for using our application!');
        }
    }

    /**

     * Get the array representation of the notification.

     *

     * @param  mixed  $notifiable

     * @return array

     */
    public function toArray($notifiable) {

        if ($this->type == 'Approved') {

            return [
                'message' => 'Thank you for for providing KYC details with Modern Mart. Your KYC is approved now',
                'url' => route('login'),
            ];
        } else {

            return [
                'message' => 'Thank you for for providing KYC details with Modern Mart. Sorry, your KYC is unapproved, Please provide proper kyc details.',
                'url' => route('login'),
            ];
        }
    }

}
