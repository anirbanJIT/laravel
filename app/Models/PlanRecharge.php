<?php

namespace App\Models;
use App\Models\Brands;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PlanRecharge extends Model
{
   use SoftDeletes;
   protected $guarded = [];

   protected $fillable = ['name','category', 'amount', 'yearly_charges','plotter','speed','pressure','status'];
   
   

   
}
