<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use App\User;

class ComplaintFeedback extends Model{

	protected $table= "complaint_feedback";

	protected $fillable = ['user_id','title','description','type'];

	public function User()
	{
		return $this->belongsTo(User::class,'user_id','id');
	}
}

