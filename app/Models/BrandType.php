<?php

namespace App\Models;
use App\Models\Brands;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BrandType extends Model
{
   use SoftDeletes;
   protected $guarded = [];

   protected $fillable = ['name','position_order', 'slug', 'image', 'status', 'meta_title', 'meta_description', 'meta_keywords','order_number'];
   
   public function brands(){
	   return $this->hasMany(Brands::class);
   }

   public function nextOrderNumber()
    {
    	$max = \DB::table('brand_types')->max('id');
    	return empty($max) ? 1 : intval($max)+1;
    }
}
