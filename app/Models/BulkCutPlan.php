<?php

namespace App\Models;
use App\User;
use Illuminate\Database\Eloquent\Model;
use DB;
class BulkCutPlan extends Model
{
    protected $fillable = ['name','price','credit','validity','cut_limit','duration','status'];
}