<?php

namespace App\Models;

use App\Models\BrandType;
use App\Models\Brand;
use App\Models\BrandModel;
use App\Models\BrandSubModel;
use App\Models\ModelScreenImage;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BrandModelScreenImage extends Model {

    use SoftDeletes;

    protected $fillable = ['upload_by','brand_type_id', 'brand_id','model_id', 'sub_model_id', 'name','position_order',  'image','blt_file','width','height', 'status','download', 'meta_title', 'meta_keywords', 'meta_description', 'order_number', 'width_height'];

    public function brandsTypes() {
        return $this->belongsTo(BrandType::class, 'brand_type_id', 'id');
    }

    public function brands() {
        return $this->belongsTo(Brand::class, 'brand_id', 'id');
    }

    public function brandsModel() {
        return $this->belongsTo(BrandModel::class, 'model_id', 'id');
    }

    public function brandSubModel() {
        return $this->belongsTo(BrandSubModel::class, 'sub_model_id', 'id');
    }

    public function nextOrderNumber() {
        $max = \DB::table('brand_model_screen_images')->max('id');
        return empty($max) ? 1 : intval($max) + 1;
    }
    
    public function brandsImages() {
        return $this->hasMany(ModelScreenImage::class, 'brand_model_screen_image_id', 'id');
    }

}
