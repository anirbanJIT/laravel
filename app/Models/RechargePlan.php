<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use DB;

class RechargePlan extends Model {

    protected $fillable = ['recharge_plan_name', 'recharge_plan_type', 'recharge_plan_credit', 'recharge_plan_amount', 'recharge_plan_validity', 'recharge_plan_rewards', 'recharge_plan_status', 'plan_roles', 'country_id', 'currency', 'recharge_plan_per_day_cost'];

    public function plan_type() {
        return $this->belongsTo(Plan::class, 'recharge_plan_type');
    }

}
