<?php

namespace App\Models;

use App\Models\BrandModelScreenImage;
use App\Models\UserReactions;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ModelScreenImage extends Model {

//    use SoftDeletes;

    protected $fillable = ['brand_model_screen_image_id', 'screen_name', 'image_file', 'blt_file', 'width', 'height'];

    public function images() {
        return $this->belongsTo(BrandModelScreenImage::class, 'brand_model_screen_image_id', 'id');
    }

    public function userReaction() {
        return $this->hasMany(UserReactions::class, 'model_screen_image_id', 'id');
    }
    
    public function userlikes() {
        return $this->hasMany(UserReactions::class, 'model_screen_image_id', 'id')->where('status','=',1)->where('admin_status','=',0);
    }
    
    public function userunlikes() {
        return $this->hasMany(UserReactions::class, 'model_screen_image_id', 'id')->where('status','=',2)->where('admin_status','=',0);
    }

}
