<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class UserContactDetail extends Model
{
    protected $table= "user_contact_details";
    protected $fillable = ['user_id','address','city','state','country','pin_code'];

   public function user()
   {
       return $this->belongsTo(User::class);

   }
}
