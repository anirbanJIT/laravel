<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class UserBankDetail extends Model
{
   protected $table = "user_bank_details";

   protected $fillable = ['user_id','bank_name','account_no','branch_name','account_name','ifsc_code','account_type','mobile_number'];

   public function user(){
       return $this->belongsTo(User::class);
   }
}
