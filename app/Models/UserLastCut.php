<?php

namespace App\Models;

use App\Models\BrandType;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserLastCut extends Model {

//   use SoftDeletes;

    protected $table = 'users_last_cut';
    protected $fillable = ['user_id', 'image_id'];

}
