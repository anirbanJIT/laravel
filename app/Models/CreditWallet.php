<?php

namespace App\Models;
use App\User;
use Illuminate\Database\Eloquent\Model;
use DB;

class CreditWallet extends Model
{
	protected $table = 'credit_wallet';
    protected $fillable = ['user_id','amount','credit_note'];
	
	public function retailer()
	{
		return $this->belongsTo(User::class,'user_id','id');
		
	}
	
	public function retailerCreditBalance()
	{
		return CreditWallet::where('user_id','=',auth()->user()->id)->first();
	}
	
	public function creditSetting()
	{
		return DB::table('credit-point-setting')->first();
	}
	
	public function retailerCreditBalanceWithoutAuthUser($id)
	{
		return CreditWallet::where('user_id','=',$id)->first();
	}
}
