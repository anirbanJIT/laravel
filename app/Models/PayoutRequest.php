<?php

namespace App\Models;
use App\User;
use Illuminate\Database\Eloquent\Model;
use DB;
class PayoutRequest extends Model
{
    protected $fillable = ['user_id','amount','type','status'];
}
