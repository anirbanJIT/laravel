<?php

namespace App\Models;
use App\User;
use Illuminate\Database\Eloquent\Model;
use DB;

class RechargeCommission extends Model
{
    protected $fillable = ['invoice_no','user_id','commission_per','invoice_amt','commission_amt','invoice_credit','commission_credit','type','description'];
	
	function incomeReceiver()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
	
	function creditPurchaser()
    {
        return $this->belongsTo(User::class, 'sender_id', 'id');
    }

    function commisionPaid()
    {
        return  $this->belongsTo(RechargeCommission::class, 'user_id', 'user_id')
                ->select('user_id', DB::raw('SUM(commission_amt) as paid'))
                ->groupBy('user_id')->where('status','1');
    }
    
    function commisionUnpaid()
    {
        return  $this->belongsTo(RechargeCommission::class, 'user_id', 'user_id')
                ->select('user_id', DB::raw('SUM(commission_amt) as unpaid'))
                ->groupBy('user_id')->where('status','0');
    }


}
