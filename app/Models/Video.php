<?php

namespace App\Models;
use App\User;
use App\Models\VideoCategory;
use Illuminate\Database\Eloquent\Model;
use DB;

class Video extends Model
{
    protected $fillable = ['video_title','video_link','video_category_id','video_image','video_description','video_status','position_order'];

    public function videoCategory()
    {
        return $this->belongsTo(VideoCategory::class,'video_category_id','id');
    }
}
