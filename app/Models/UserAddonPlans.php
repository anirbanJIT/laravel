<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use DB;

class UserAddonPlans extends Model {

    protected $fillable = ['user_id', 'plan_id', 'recharge_plan_id', 'start_date', 'expire_date', 'is_addon'];

    public function users() {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

}
