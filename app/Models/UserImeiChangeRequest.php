<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
use DB;

class UserImeiChangeRequest extends Model
{
    protected $table= "user_imei_change_request";
	
    protected $fillable = ['user_id','device_id'];

    public function user(){
        return $this->belongsTo(User::class);
    }
}
