<?php



namespace App\Models;



use Illuminate\Database\Eloquent\Model;



class Customer extends Model

{

    protected $fillable =['retailer_id','name','phone','model_id','qty'];

	

	

	function Retailer()

    {

        return $this->belongsTo(User::class, 'retailer_id', 'id')

            ->select('name as retailer_name');

    }

    function BrandModel()

    {

        return $this->belongsTo(BrandModel::class, 'model_id', 'id')

            ->select('name as brand_model_name');

    }

}

