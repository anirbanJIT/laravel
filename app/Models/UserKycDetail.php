<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use App\Models\MachineNumbers;

class UserKycDetail extends Model {

    protected $table = "user_kyc_details";
    protected $fillable = ['user_id', 'kyc_document', 'kyc_number', 'customer_name', 'company_name', 'relation', 'dob', 'phone', 'emailKyc', 'address', 'city', 'state_id', 'country_id', 'pin_code', 'attachment', 'kyc_created_date', 'kyc_expiry_date', 'notes', 'status', 'machine_number', 'machine_price', 'machine_purchase_date', 'shop_visiting_card', 'shop_pic_with_selfie', 'current_location', 'lat_lan'];

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function getMachineNumber() {
        return $this->hasOne(MachineNumbers::class, 'machine_number', 'machine_number');
    }

}
