<?php

namespace App\Models;

use App\Models\BrandType;
use App\Models\Brand;
use App\Models\BrandModel;
use App\Models\BrandSubModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BrandModelScreenRetailer extends Model
{
	use SoftDeletes;
    protected $fillable = ['brand_id','model_id','slug','image','status','meta_title','meta_keywords','meta_description','blt_file','width','height','order_number'];
	
	
	

	public function nextOrderNumber()
    {
    	$max = \DB::table('brand_model_screen_retailers')->max('id');
    	return empty($max) ? 1 : intval($max)+1;
    }
}
