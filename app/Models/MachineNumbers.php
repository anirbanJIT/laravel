<?php



namespace App\Models;



use App\User;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;



class MachineNumbers extends Model {



    protected $table = 'machine_numbers';

    protected $fillable = ['machine_number','customer_name','quantity','plan','status'];



}

