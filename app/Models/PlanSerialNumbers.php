<?php



namespace App\Models;



use App\User;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;



class PlanSerialNumbers extends Model {



    protected $table = 'planserial_numbers';

    protected $fillable = ['plan_id','quantity', 'user_id','serial_number','status'];



    

    public function userSerialNumber() {

        return $this->belongsTo(User::class, 'user_id', 'id');

    }



}

