<?php

namespace App\Models;
use App\User;
use Illuminate\Database\Eloquent\Model;
use DB;
class Rewards extends Model
{
    protected $table = 'rewards';
    protected $fillable = ['plan_id','user_id','reward','action'];
}
