<?php

namespace App\Models;
use App\Models\BrandType;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Brand extends Model
{
   use SoftDeletes;
   
   protected $fillable = ['brand_type_id', 'name','position_order', 'slug', 'image', 'status', 'meta_title', 'meta_description', 'meta_keywords','order_number'];
   
    public function brandCat()
    {
        return $this->belongsTo(BrandType::class,'brand_type_id','id');
    }
	
	 public function model()
    {
        return $this->hasMany(BrandModel::class,'brand_id','id')->where('status','=',1);
    }

    public function nextOrderNumber()
    {
      $max = \DB::table('brands')->max('id');
      return empty($max) ? 1 : intval($max)+1;
    }
}
