<?php

namespace App\Models;
use App\User;
use Illuminate\Database\Eloquent\Model;
use DB;
class CutWallet extends Model
{
	protected $table = 'cut_wallet';
    protected $fillable = ['user_id','model_id','module_name','amount','actions'];
}