<?php

namespace App\Models;
use App\User;
use Illuminate\Database\Eloquent\Model;
use DB;
class CommissionSetting extends Model
{
	protected $table = 'commission-setting';
    protected $fillable = ['user_id','commission_plan_id_from','commission_plan_id_to','commission','commission_type','minimum_recharge'];

    public function planto()
    {
        return $this->belongsTo(Plan::class, 'commission_plan_id_to');
    }

    public function planfrom()
    {
        return $this->belongsTo(Plan::class, 'commission_plan_id_from');
    }
}