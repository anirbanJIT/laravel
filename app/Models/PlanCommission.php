<?php

namespace App\Models;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PlanCommission extends Model
{
    use SoftDeletes;
    
    protected $table = 'plans_commission';

    protected $fillable = ['commission_plan_id_from','commission_plan_id_to','discount_value','discount_type','user_id'];
	
	public function plan()
    {
        return $this->belongsTo(Plan::class, 'commission_plan_id_to');
    }

    public function planto()
    {
        return $this->belongsTo(Plan::class, 'commission_plan_id_to');
    }

    public function planfrom()
    {
        return $this->belongsTo(Plan::class, 'commission_plan_id_from');
    }

    public function getCommissionSettingDetail($commission_plan_id_from,$commission_plan_id_to,$user_id){
       // dd($commission_plan_id_from);
        return CommissionSetting::where('commission_plan_id_from',$commission_plan_id_from)
                ->Where('commission_plan_id_to',$commission_plan_id_to)
                ->Where('user_id',$user_id)
                ->first();
    }


}
