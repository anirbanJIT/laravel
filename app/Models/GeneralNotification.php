<?php

namespace App\Models;
use App\User;
use Illuminate\Database\Eloquent\Model;
use DB;
class GeneralNotification extends Model
{
    //protected $fillable = ['title','image','message','user_id'];

    public function users(){
       return $this->belongsTo(User::class,'user_id','id');
   }
    
}
