<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PlanBrandTypes extends Model {

    protected $table = 'plan_brandtypes';
    protected $fillable = ['plan_id', 'brand_type_id'];

    public function plan() {
        return $this->belongsTo(Plan::class, 'plan_id');
    }


}
