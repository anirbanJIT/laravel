<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use DB;

class CashWalletTransaction extends Model
{
	protected $table = 'cash_wallet_transaction';
   protected $fillable = ['invoice_no','receiver_id','sender_id','credit_amt','debit_amt','balance_amt','type','transaction_description'];
   
   function receiver()
    {
        return $this->belongsTo(User::class, 'receiver_id', 'id')
            ->select('name as receiver_name');
    }
	
	function sender()
    {
        return $this->belongsTo(User::class, 'sender_id', 'id')
            ->select('name as sender_name');
    }
	
	 function cashReceiver()
    {
        return $this->belongsTo(User::class, 'receiver_id', 'id');
    }
	
	function cashSender()
    {
        return $this->belongsTo(User::class, 'sender_id', 'id');
    }
}
