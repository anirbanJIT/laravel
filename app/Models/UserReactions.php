<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserReactions extends Model {

    protected $table = 'user_reactions';
    protected $fillable = ['user_id', 'model_screen_image_id', 'status', 'rating','comment'];

    public function reactions() {
        return $this->belongsTo(ModelScreenImage::class, 'model_screen_image_id', 'id');
    }

}
