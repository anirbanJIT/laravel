<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class PaperSize extends Model
{
	//protected $table= "paper_size";
	
    protected $fillable = ['title','paper_width', 'paper_height', 'status'];
}