<?php

namespace App\Models;

use App\Models\BrandType;
use App\Models\Brand;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BrandModel extends Model
{
	use SoftDeletes;
    protected $fillable = ['brand_type_id','brand_id','name','position_order','slug','image','status','cut_point','meta_title','meta_keywords','meta_description','order_number'];
	
	
	public function brandsTypes()
	{
		return $this->belongsTo(BrandType::class,'brand_type_id','id');
	}
	public function brands()
	{
		return $this->belongsTo(Brand::class,'brand_id','id');
	}
	public function brandsModelScreenImage()
	{
		return $this->hasMany(BrandModelScreenImage::class);
	}
	
	public function screenShape()
	{
		return $this->hasMany(BrandModelScreenImage::class,'model_id','id')->where('status','=',1);
	}

	public function nextOrderNumber()
    {
    	$max = \DB::table('brand_models')->max('id');
    	return empty($max) ? 1 : intval($max)+1;
    }
}
