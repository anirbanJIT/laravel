<?php

namespace App\Models;
use App\User;
use Illuminate\Database\Eloquent\Model;
use DB;
class AccountRecharge extends Model
{
    protected $fillable = ['user_id','recharge_duration','recharge_amount','expire_date','payment_mode','invoice_no','tansaction_id','status'];
	
	protected $dates = ['expire_date'];
	
	public function accountRechargeUser()
	{
		return $this->belongsTo(User::class,'user_id','id');
	}
}
