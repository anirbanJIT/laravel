<?php

namespace App\Models;
use App\User;
use Illuminate\Database\Eloquent\Model;
use DB;
class CashWallet extends Model
{
	protected $table = 'cash_wallet';
    protected $fillable = ['user_id','amount'];
	
	public function retailerCashBalance()
	{
		return CashWallet::where('user_id','=',auth()->user()->id)->first();
	}
	
	public function creditSetting()
	{
		return DB::table('credit-point-setting')->first();
	}
	public function user(){
       return $this->belongsTo(User::class);
   }
}
