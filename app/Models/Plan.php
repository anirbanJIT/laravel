<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Plan extends Model {

    use SoftDeletes;

    protected $table = 'plans';
    protected $fillable = ['name', 'slug', 'plan_status'];

    public function PlanCommissions() {
        return $this->hasMany(PlanCommission::class, 'commission_plan_id_to', 'id');
    }

    public function RechargePlans() {
        return $this->hasMany(RechargePlan::class, 'recharge_plan_type', 'id');
    }

    public function user() {
        return $this->belongsTo(User::class);
    }
    
    public function PlanBrandTypes() {
        return $this->hasMany(PlanBrandTypes::class, 'plan_id', 'id');
    }
    
    public function PlanSerialNumbers() {
        return $this->hasMany(PlanSerialNumbers::class, 'plan_id', 'id');
    }

}
