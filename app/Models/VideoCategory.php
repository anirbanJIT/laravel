<?php

namespace App\Models;
use App\User;
use Illuminate\Database\Eloquent\Model;
use DB;
class VideoCategory extends Model
{
	protected $table= "video_category";
	
    protected $fillable = ['title','image','status'];
}