<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class UserLevel extends Model
{
	protected $table = 'user_level';
    protected $fillable = ['user_id','upline_id','level'];
}
