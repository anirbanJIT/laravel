<?php

namespace App\Models;
use App\User;
use Illuminate\Database\Eloquent\Model;
use DB;
class OffLineCut extends Model
{
	protected $table = 'off_line_cut';
    protected $fillable = ['user_id','amount'];
}