<?php

namespace App\Models;

use App\Models\BrandType;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Design extends Model {

    protected $fillable = ['brand_name', 'model_name', 'model_image', 'plt_file', 'created_at'];

}
