<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use App\User;
use DB;
class UserProfileChangeRequest extends Model
{
	protected $table= "user_profile_change_request";
	
    protected $fillable = ['user_id','email','phone','other_phone','state','country','status'];
}