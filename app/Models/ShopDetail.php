<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class ShopDetail extends Model
{
    protected $table= "shop_details";

	protected $fillable = ['user_id','shop_name','shop_photo','machine_photo','shop_gallery','address','state','city','pin_code','country_id'];

	public function User()
	{
		return $this->belongsTo(User::class,'user_id','id');
	}
}


