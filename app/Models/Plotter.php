<?php

namespace App\Models;
use App\Models\Brands;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Plotter extends Model
{
   use SoftDeletes;
   protected $guarded = [];

   protected $fillable = ['name','image', 'speed', 'pressure','status'];
   
   

   
}
