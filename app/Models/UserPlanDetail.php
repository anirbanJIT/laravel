<?php

namespace App\models;
use App\User;
use App\Models\Plan;

use Illuminate\Database\Eloquent\Model;

class UserPlanDetail extends Model
{
	protected $table = 'user_plans';
    protected $fillable = ['user_id','user_plan_id','user_plan_light_to_light_discount','user_plan_light_to_light_discount_type','user_plan_light_to_main_discount', 'user_plan_light_to_main_discount_type', 'user_plan_main_to_main_discount', 'user_plan_main_to_main_discount_type', 'user_plan_main_to_light_discount', 'user_plan_main_to_light_discount_type','user_plan_status'];

    public function user(){
        return $this->belongsTo(User::class);
    }

    /*public function userPlan(){
        return $this->belongsTo(Plan::class);
    }*/
}
