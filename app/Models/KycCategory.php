<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KycCategory extends Model
{
   protected $table = 'kyc_categories';
   protected $fillable =['name'];
}
