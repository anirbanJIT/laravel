<?php

namespace App\Models;
use App\User;
use Illuminate\Database\Eloquent\Model;
use DB;
class BulkCutWallet extends Model
{
	protected $table = 'bulk_cut_wallet';
    protected $fillable = ['user_id','blukcut_plan_id','model_id','cuts','expires_at','actions'];
}