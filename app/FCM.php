<?php

namespace App;

use Exception;

/**
 * https://firebase.google.com/docs/cloud-messaging/http-server-ref
 * @author Naren Thaker @ narenthaker.com
 */
class FCM {

    public $token = null;
    private $fcm_registration_ids = null;
    private $fcm_to = null;
    private $fcm_payload = [
        'priority' => 'normal',
        'content_available' => true,
        'mutable_content' => true,
    ];
    public $notification = [
        'click_action' => "FCM_PLUGIN_ACTIVITY",
        'sound' => 'default',
        'color' => '#F91942',
    ];
    public $options = [];
    public $data = [];
    private $key = 'AAAAgVWVnS8:APA91bGGV94k_FnTbRkZVCqNJ1Ss_DIZFZd-kPUIoszJ6QQUmllwqL7kr2s5v35t7xWjonuhXBxa-3TkEemzxLQidn8IALkTaQ8Jve7EJgKRv5X-R32KknMWe0aoWZZL4bkIsD6sk_FC';

    public function send($key = null) {
        $this->fcm_notification_validation();
        if(!empty($this->data)){
            $this->fcm_payload['data'] = $this->data;
            $this->notification['data'] = $this->data;
        }
        $this->notification['data']['title'] = $this->notification['title'];
        $this->notification['data']['body'] = $this->notification['body'];
        if(array_key_exists('image', $this->notification)){
             $this->notification['data']['image'] = $this->notification['image'];
        }
        $this->fcm_payload['notification'] = $this->notification;
        return $this->fcm_send($key);
    }

    private function fcm_send() {
        $headers = ['Authorization: key=' . $this->key, 'Content-Type: application/json'];
        if (!empty($this->fcm_to)) {
            
            $this->fcm_payload['to'] = $this->fcm_to;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
            curl_setopt($ch, CURLOPT_POST, TRUE);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($this->fcm_payload));
            $result = curl_exec($ch);
            return $result;
        } else {
            $fcmTokenCollection = array_values(array_filter($this->fcm_registration_ids));
            $split_array = array_chunk($fcmTokenCollection, 500);
            foreach ($split_array as $collection) {
                $this->fcm_payload['registration_ids'] = $collection;
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
                curl_setopt($ch, CURLOPT_POST, TRUE);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($this->fcm_payload));
                $result = curl_exec($ch);
                curl_close($ch);
            }
        }
    }

    private function fcm_notification_validation() {
        if (is_array($this->token)) {
            $this->fcm_registration_ids = $this->token;
        } else if (is_string($this->token)) {
            $this->fcm_to = $this->token;
        } else {
            throw new Exception("Token Must Be String OR An Array\nGiven: " . gettype($this->token));
        }
        if (array_key_exists('image',$this->notification)) {
            if (empty($this->notification['image'])) {
                throw new Exception("Image Can Not Empty");
            }
            if (!is_string($this->notification['image'])) {
                throw new Exception("Image Must Be String\nGiven: " . gettype($this->notification['image']));
            }
        }
        if (!array_key_exists('title',$this->notification)) {
            throw new Exception("Title Must Be Present");
        }
        if (!is_string($this->notification['title'])) {
            throw new Exception("Title Must Be String\nGiven: " . gettype($this->notification['title']));
        }
        if (empty(trim($this->notification['title']))) {
            throw new Exception("Title Can Not Empty");
        }
        if (!array_key_exists('body',$this->notification)) {
            throw new Exception("Body Must Be Present");
        }
        if (!is_string($this->notification['body'])) {
            throw new Exception("Body Must Be String\nGiven: " . gettype($this->notification['body']));
        }
        if (empty(trim($this->notification['body']))) {
            throw new Exception("Body Can Not Empty");
        }
        if (array_key_exists('badge',$this->notification)) {
            if (!is_integer($this->notification['badge'])) {
                throw new Exception("Badge Must Be INT\nGiven: " . gettype($this->notification['badge']));
            }
        }
        if (!is_array($this->data)) {
            throw new Exception("Data Must Be Array\nGiven: " . gettype($this->data));
        }
        if (!is_array($this->options)) {
            throw new Exception("Options Must Be Array\nGiven: " . gettype($this->options));
        }
    }

}
