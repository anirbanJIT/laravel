<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlansCommissionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plans_commission', function (Blueprint $table) {
            $table->bigIncrements('id');
            
            $table->unsignedBigInteger('commission_plan_id_from')->nullable();
            $table->foreign('commission_plan_id_from')->references('id')->on('plans')->onDelete('cascade')->onUpdate('cascade');

            $table->unsignedBigInteger('commission_plan_id_to')->nullable();
            $table->foreign('commission_plan_id_to')->references('id')->on('plans')->onDelete('cascade')->onUpdate('cascade');

            $table->double('discount_value', 4, 2)->nullable();
            $table->tinyInteger('discount_type')->comment('0 => Percentage, 1 => Fixed');
            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plans_commission');
    }
}
