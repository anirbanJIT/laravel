<?php



use Illuminate\Database\Migrations\Migration;

use Illuminate\Database\Schema\Blueprint;

use Illuminate\Support\Facades\Schema;



class CreateCustomersTable extends Migration

{

    /**

     * Run the migrations.

     *

     * @return void

     */

    public function up()

    {

        Schema::create('customers', function (Blueprint $table) {

            $table->bigIncrements('id');

			$table->unsignedBigInteger('retailer_id')->nullable();

			$table->foreign('retailer_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');

			$table->string('name');

            $table->string('phone',15)->nullable();

            $table->unsignedBigInteger('model_id')->nullable();

            $table->timestamps();

        });

    }



    /**

     * Reverse the migrations.

     *

     * @return void

     */

    public function down()

    {

        Schema::dropIfExists('customers');

    }

}

