<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('credit-point-setting', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('credit')->default('0');
            $table->float('amount')->default('0.0');
            $table->timestamps();
        });
		
		Schema::create('commission-setting', function (Blueprint $table) {
            $table->bigIncrements('id');
			$table->integer('user_id')->nullable();
			$table->integer('commission_type')->default('2')->comment('1 => Fixed, 2 => Percent');
            $table->float('commission')->nullable();
			$table->integer('commission_get_on')->default('2')->comment('1 => Own Rcharge, 2 => Referral Recharge, 3 => Both');
			$table->float('minimum_recharge')->nullable();
			$table->integer('recharge_month')->nullable()->comment('its yearly');
			$table->tinyInteger('status')->default('1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('credit-point-setting');
		Schema::dropIfExists('commission-setting');
    }
}
