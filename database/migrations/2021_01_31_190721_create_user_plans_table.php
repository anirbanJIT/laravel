<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_plans', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->unsignedBigInteger('user_plan_id')->nullable();
            $table->foreign('user_plan_id')->references('id')->on('plans')->onDelete('cascade')->onUpdate('cascade');

            $table->double('user_plan_light_to_light_discount', 4, 2)->nullable();
            $table->string('user_plan_light_to_light_discount_type')->nullable();
            $table->double('user_plan_light_to_main_discount', 4, 2)->nullable();
            $table->string('user_plan_light_to_main_discount_type')->nullable();
            $table->double('user_plan_main_to_main_discount', 4, 2)->nullable();
            $table->string('user_plan_main_to_main_discount_type')->nullable();
            $table->double('user_plan_main_to_light_discount', 4, 2)->nullable();
            $table->string('user_plan_main_to_light_discount_type')->nullable();
            $table->tinyInteger('user_plan_status')->default('1')->comment('0 => Deactivate, 1 => Active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_plan');
    }
}
