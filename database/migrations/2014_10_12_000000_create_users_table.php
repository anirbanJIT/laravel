<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
			$table->unsignedBigInteger('parent_user_id')->default('0');
			$table->text('app_role')->nullable();
			$table->string('referral_id')->unique();
			$table->unsignedBigInteger('referred_by')->nullable();
			$table->foreign('referred_by')->references('referral_id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->string('shop_name')->nullable();
            $table->string('name');
            $table->string('username')->unique()->nullable();
            $table->string('email')->unique();
            $table->string('phone',15)->nullable();
            $table->string('other_phone',15)->default('0');
			$table->string('password');
			$table->integer('otp',6)->nullable();
			$table->tinyInteger('otp_status')->default('0')->comment('0 => Not verify','1 => Verified');
			$table->string('api_token',256)->nullable();
			$table->tinyInteger('status')->default('0');
			$table->tinyInteger('type')->default('0')->comment('0 => Admin','1 => Retailer');
            $table->string('remember_token')->nullable();
			$table->dateTime('expire_date')->nullable();
            $table->unsignedBigInteger('current_plan_id')->nullable()->comment('recharge plans id');
            $table->string('current_plan')->nullable()->default('light_plan');
            $table->unsignedBigInteger('current_blukcut_plan')->nullable();
            $table->unsignedBigInteger('current_blukcut_credit')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
