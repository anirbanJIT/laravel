<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBrandTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       
        Schema::create('brand_types', function (Blueprint $table) {
            $table->bigIncrements('id');
			$table->string('name');
			$table->string('slug')->nullable();
			$table->string('image')->nullable();
			$table->tinyInteger('status');
			$table->string('meta_title',255)->nullable();
            $table->text('meta_description')->nullable();
            $table->string('meta_keywords',255)->nullable();
            $table->timestamps();
			$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brand_types');
    }
}
