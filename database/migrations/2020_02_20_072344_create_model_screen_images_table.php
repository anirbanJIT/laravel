<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModelScreenImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('brand_model_screen_images', function (Blueprint $table) {
            $table->bigIncrements('id');
			$table->unsignedBigInteger('brand_type_id')->nullable();
			$table->foreign('brand_type_id')->references('id')->on('brand_types')->onDelete('cascade')->onUpdate('cascade');
			$table->unsignedBigInteger('brand_id')->nullable();
			$table->foreign('brand_id')->references('id')->on('brands')->onDelete('cascade')->onUpdate('cascade');
			$table->unsignedBigInteger('model_id')->nullable();
			$table->foreign('model_id')->references('id')->on('brand_models')->onDelete('cascade')->onUpdate('cascade');
			$table->longtext('name')->nullable();
            $table->longtext('image')->nullable();
            $table->longtext('blt_file',255)->nullable();
			$table->tinyInteger('status');
			$table->string('meta_title',255)->nullable();
			$table->string('meta_keywords',255)->nullable();
			$table->text('meta_description')->nullable();
            $table->timestamps();
			$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brand_model_screen_images');
		
		Schema::table('brand_model_screen_images', function (Blueprint $table) {
            $table->dropForeign('brand_model_screen_images_brand_type_id_foreign');
			$table->dropForeign('brand_model_screen_images_brand_id_foreign');
			$table->dropForeign('brand_model_screen_images_model_id_foreign');
            
        });
    }
}
