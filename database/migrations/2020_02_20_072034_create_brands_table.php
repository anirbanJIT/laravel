<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBrandsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::enableForeignKeyConstraints();

        Schema::create('brands', function (Blueprint $table) {
            $table->bigIncrements('id');
			$table->unsignedBigInteger('brand_type_id')->nullable();
			$table->foreign('brand_type_id')->references('id')->on('brand_types')->onDelete('cascade')->onUpdate('cascade');
			$table->string('name');
			$table->string('slug')->nullable();
			$table->string('image')->nullable();
			$table->tinyInteger('status');
			$table->string('meta_title',255)->nullable();
            $table->text('meta_description')->nullable();
            $table->string('meta_keywords',255)->nullable();
            $table->timestamps();
			$table->softDeletes();
        });
		
		
   
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::dropIfExists('brands');
        
		Schema::table('brands', function (Blueprint $table) {
            $table->dropForeign('brands_brand_type_id_foreign');
		
            
        });
	   
	   
    }
}
