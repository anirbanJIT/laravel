<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMachineNumberMachinePurchaseDateToUserKycDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_kyc_details', function (Blueprint $table) {
            $table->text('notes')->after('status')->nullable();
            $table->string('machine_price')->after('status')->nullable();
            $table->string('machine_number')->nullable();
            $table->date('machine_purchase_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_kyc_details', function (Blueprint $table) {
            $table->dropColumn('notes');
            $table->dropColumn('machine_price');
            $table->dropColumn('machine_number');
            $table->dropColumn('machine_purchase_date');
        });
    }
}
