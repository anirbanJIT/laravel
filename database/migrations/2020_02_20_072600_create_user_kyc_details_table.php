<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserKycDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_kyc_details', function (Blueprint $table) {
            $table->bigIncrements('id');
			$table->unsignedBigInteger('user_id')->nullable();
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
			$table->unsignedBigInteger('kyc_id')->nullable();
			$table->foreign('kyc_id')->references('id')->on('kyc_categories')->onDelete('cascade')->onUpdate('cascade');
			$table->string('kyc_number',50);
			$table->string('company_name')->nullable();
			$table->string('customer_name')->nullable();
			$table->enum('relation',['Father','Mother','Husband','Wife'])->nullable();
			$table->date('dob')->nullable();
			$table->string('phone',15)->nullable();
			$table->string('emailKyc',150)->nullable();
			$table->string('address',255)->nullable();
			$table->string('city')->nullable();
			$table->integer('state_id')->nullable();
			$table->integer('country_id')->nullable();
			$table->integer('pin_code')->nullable();
			$table->string('attachment')->nullable();
			$table->date('kyc_created_date')->nullable();
			$table->date('kyc_expiry_date')->nullable();
			$table->integer('status')->nullable();
			$table->timestamps();
			$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       
		Schema::dropIfExists('user_kyc_details');
		Schema::table('user_kyc_details', function (Blueprint $table) {
            $table->dropForeign('user_kyc_details_user_id_foreign');
			$table->dropForeign('user_kyc_details_kyc_id_foreign');
            
        });
    }
}
