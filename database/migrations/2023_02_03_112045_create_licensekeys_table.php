<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLicensekeysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('licensekeys', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('licensekey')->unique();
            $table->enum('is_active', ['active', 'inactive'])->default('inactive');
            $table->string('used_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('licensekeys');
    }
}
