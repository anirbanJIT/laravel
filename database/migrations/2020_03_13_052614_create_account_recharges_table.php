<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccountRechargesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('account_recharges', function (Blueprint $table) {
            $table->bigIncrements('id');
			$table->unsignedBigInteger('user_id')->nullable();
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
			$table->integer('recharge_duration')->nullable();
			$table->float('recharge_amount')->nullable();
			$table->dateTime('expire_date')->nullable();
			$table->tinyInteger('payment_mode')->nullable()->comment('1=>Credit Wallet,2=>Cashfree');
			$table->string('invoice_no')->nullable();
			$table->string('tansaction_id')->nullable();
			$table->tinyInteger('status')->default('0')->comment('0=>Unpaid, 1=>Paid');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('account_recharges');
    }
}
