<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserLevelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_level', function (Blueprint $table) {
            $table->bigIncrements('id');
			$table->unsignedBigInteger('user_id')->nullable();
			$table->foreign('user_id')->references('referral_id')->on('users')->onDelete('cascade')->onUpdate('cascade');
			$table->unsignedBigInteger('upline_id')->nullable();
			$table->foreign('upline_id')->references('referral_id')->on('users')->onDelete('cascade')->onUpdate('cascade');
			$table->integer('level')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_level');
    }
}
