<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserBankDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('user_bank_details', function (Blueprint $table) {
            $table->bigIncrements('id');
			$table->unsignedBigInteger('user_id')->nullable();
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
			$table->string('bank_name',150);
			$table->string('account_no',30);
			$table->string('branch_name');
			$table->string('account_name',150);
			$table->string('ifsc_code',20);
			$table->enum('account_type',['Current','Saving']);
            $table->timestamps();
			$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_bank_details');
		
		Schema::table('user_bank_details', function (Blueprint $table) {
            $table->dropForeign('user_bank_details_user_id_foreign');
            
        });
    }
}
