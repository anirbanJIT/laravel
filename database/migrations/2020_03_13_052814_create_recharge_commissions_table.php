<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRechargeCommissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recharge_commissions', function (Blueprint $table) {
            $table->bigIncrements('id');
			$table->unsignedBigInteger('user_id')->nullable();
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
			$table->unsignedBigInteger('sender_id')->nullable();
			$table->foreign('sender_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
			$table->string('invoice_no')->nullable();
			$table->float('commission_per')->nullable();
			$table->float('invoice_amt')->nullable();
			$table->float('commission_amt')->nullable();
			$table->float('invoice_credit')->nullable();
			$table->float('commission_credit')->nullable();
			$table->string('type')->nullable();
			$table->string('description')->nullable();
			$table->tinyInteger('status')->default('0')->comment('0=>Unpaid, 1=>Paid');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recharge_commissions');
    }
}
