<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRechargePlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recharge_plans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('recharge_plan_name')->nullable();
            //$table->enum('recharge_plan_type',['main_plan','light_plan'])->default('light_plan');
            $table->string('recharge_plan_type');
            $table->integer('recharge_plan_credit')->nullable();
            $table->float('recharge_plan_amount')->nullable();
            $table->string('recharge_plan_validity')->nullable();
            $table->integer('recharge_plan_rewards')->nullable();
            $table->text('plan_roles')->nullable();
            $table->tinyInteger('recharge_plan_status')->default('1')->comment('0 => Deactivate, 1 => Active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recharge_plan');
    }
}
