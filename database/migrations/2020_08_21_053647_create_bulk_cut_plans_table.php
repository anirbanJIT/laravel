<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBulkCutPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bulk_cut_plans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->enum('type',['blukcut'])->default('blukcut');
            $table->integer('credit')->nullable();
            $table->float('price')->nullable();
            $table->string('validity')->nullable();
            $table->integer('hrs')->nullable();
            $table->tinyInteger('status')->default('1')->comment('0 => Deactivate, 1 => Active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bulk_cut_plans');
    }
}
