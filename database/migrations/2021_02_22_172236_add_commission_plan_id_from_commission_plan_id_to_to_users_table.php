<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCommissionPlanIdFromCommissionPlanIdToToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('commission-setting', function (Blueprint $table) {

            $table->unsignedBigInteger('commission_plan_id_from')->nullable()->after('user_id');
            $table->foreign('commission_plan_id_from')->references('id')->on('plans')->onDelete('cascade')->onUpdate('cascade');

            $table->unsignedBigInteger('commission_plan_id_to')->nullable()->after('user_id');
            $table->foreign('commission_plan_id_to')->references('id')->on('plans')->onDelete('cascade')->onUpdate('cascade');
            
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('commission-setting', function (Blueprint $table) {
             $table->dropColumn('commission_plan_id_from');
             $table->dropColumn('commission_plan_id_to');
             $table->dropColumn('deleted_at');
        });
    }
}
