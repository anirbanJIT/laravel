<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCreditWalletTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('credit_wallet_transaction', function (Blueprint $table) {
            $table->bigIncrements('id');
			$table->string('invoice_no');
			$table->string('transaction_id')->nullable();
			$table->unsignedBigInteger('receiver_id')->nullable();
			$table->foreign('receiver_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
			$table->unsignedBigInteger('sender_id')->nullable();
			$table->foreign('sender_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
			$table->float('credit_amt')->default('0');
			$table->float('debit_amt')->default('0');
			$table->float('balance_amt')->default('0');
			$table->float('singal_credit_pur_amt')->nullable();
			$table->float('total_credit_pur_amt')->nullable();
			$table->string('type')->nullable();
			$table->string('payment_mode')->nullable();
			$table->tinyinteger('status')->default('0');
			$table->string('transaction_description')->nullable();
            $table->string('transaction_note')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('credit_wallet_transaction');
    }
}
