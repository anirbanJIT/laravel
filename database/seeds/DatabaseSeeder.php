<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PermissionSeed::class);
        $this->call(RoleSeed::class);
        $this->call(UserSeed::class);
        $this->call(CurrencySeed::class);
		

		//setting module seeder
		$credit_point = DB::table('credit-point-setting')->insert([
            'credit' => '1',
            'amount' => '2.5'
        ]);
 		$commision_setting = DB::table('commission-setting')->insert([
            'commission_type' => '2',
			'commission' => '10',
			'commission_get_on' => '2',
			'minimum_recharge' => '100',
            'recharge_month' => '2',
			'status' => '1'
        ]);
    }
}
