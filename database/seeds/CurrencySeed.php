<?php

use Illuminate\Database\Seeder;
use App\Models\Currency;

class CurrencySeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $currency = Currency::create([
            'name' => '$',
            'rate' => '72.00'
        ]);
    }
}
