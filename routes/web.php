<?php

use App\Http\Controllers\Admin\LicensekeyController;

Route::get('/', function () {
    return redirect('/login');
});
Auth::routes(['register' => false]);
Auth::routes(['verify' => true]);

// Change Password Routes...
Route::get('change_password', 'Auth\ChangePasswordController@showChangePasswordForm')->name('auth.change_password');
Route::post('change_password', 'Auth\ChangePasswordController@changePassword')->name('auth.change_password');
Route::get('checkotp', 'Auth\ChangePasswordController@showOtpPasswordForm')->name('auth.checkOtp');
Route::post('checkotp', 'Auth\ChangePasswordController@chekOtpPassword')->name('auth.checkOtp');


Route::get('cache-clear', function () {
    \Artisan::call('config:cache');
    \Artisan::call('cache:clear');
    \Artisan::call('config:clear');
    \Artisan::call('view:clear');
    dd('cache-clear');
    return redirect('/');
});

// Registration Routes...
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');
Route::get('otp-verify', 'Auth\RegisterController@showVerifyOtp')->name('otp-verify');
Route::post('otp-verify', 'Auth\RegisterController@verifyOtp')->name('otp-verify-submit');


Route::get('cron', 'Admin\CronController@index')->name('cron');
Route::get('cron/image', 'Admin\CronController@image')->name('cronimage');
Route::get('cron/plans', 'Admin\CronController@plans')->name('cronplans');

Route::group(['middleware' => ['auth'], 'prefix' => 'admin', 'as' => 'admin.'], function () {

    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/like-list/{type}', 'HomeController@likeList')->name('like-list');
    Route::get('chart/getchatdata', 'HomeController@getchatdata');
    Route::resource('permissions', 'Admin\PermissionsController');
    Route::delete('permissions_mass_destroy', 'Admin\PermissionsController@massDestroy')->name('permissions.mass_destroy');
    Route::resource('roles', 'Admin\RolesController');
    Route::delete('roles_mass_destroy', 'Admin\RolesController@massDestroy')->name('roles.mass_destroy');
    Route::resource('users', 'Admin\UsersController');
    Route::delete('users_mass_destroy', 'Admin\UsersController@massDestroy')->name('users.mass_destroy');
    Route::get('user-activity/{id}', 'Admin\UsersController@userActivity')->name('users.users_activity');

    /*** Plan and Recharge  **/
    Route::resource('planrecharge', 'Admin\PlanRechargeController');
    Route::get('changeStatusplanrecharge', 'Admin\PlanRechargeController@changeStatus');
    Route::delete('planrecharges_mass_destroy', 'Admin\PlanRechargeController@massDestroy')->name('planrecharge.mass_destroy');

    /*** plotter  **/
    Route::resource('plotter', 'Admin\PlotterController');
    Route::get('changeStatusplotter', 'Admin\PlotterController@changeStatus');
    Route::delete('plotters_mass_destroy', 'Admin\PlotterController@massDestroy')->name('plotter.mass_destroy');


    /*** Category (old brand-type) NT **/
    Route::resource('brand-type', 'Admin\BrandTypeController');
    Route::post('brand-type-order-position', 'Admin\BrandTypeController@brandTypeOrderPosition')->name('brandTypeOrderPosition');
    Route::get('changeStatusBrandType', 'Admin\BrandTypeController@changeStatus');
    Route::get('brand-type/delete/{id}', 'Admin\BrandTypeController@deleteBrandType');
    Route::delete('brand_types_mass_destroy', 'Admin\BrandTypeController@massDestroy')->name('brandtype.mass_destroy');
    Route::get('brand-type-list', 'Admin\BrandTypeController@brandTypeDetails')->name('brand-type-list');

    /*** Brand NT **/
    Route::resource('brands', 'Admin\BrandController');
    Route::post('brand-order-position', 'Admin\BrandController@brandOrderPosition')->name('brandOrderPosition');
    Route::get('changeStatusBrand', 'Admin\BrandController@changeStatus');
    Route::get('brand/delete/{id}', 'Admin\BrandController@deleteBrand');
    Route::delete('brands_mass_destroy', 'Admin\BrandController@massDestroy')->name('brands.mass_destroy');
    //Route::get('brand-list', 'Admin\BrandController@brandDetails')->name('brand-list');
    Route::get('brandsdetails/{id}', 'Admin\BrandController@brandDetails')->name('brands.brandsdetails');


    /*** BrandModel NT **/
    Route::resource('models', 'Admin\BrandModelController');
    Route::post('models-order-position', 'Admin\BrandModelController@modelsOrderPosition')->name('modelsOrderPosition');
    Route::get('changeStatus', 'Admin\BrandModelController@changeStatus');
    Route::get('models/delete/{id}', 'Admin\BrandModelController@deleteModel');
    Route::delete('models_mass_destroy', 'Admin\BrandModelController@massDestroy')->name('models.mass_destroy');
    Route::get('modelsdetails/{id}', 'Admin\BrandModelController@modelsDetails')->name('models.modelsDetails');

    Route::get('get-brand-type-list', 'Admin\BrandModelController@getBrandTypes');
    Route::get('get-model-list', 'Admin\BrandModelController@getModelList');

    /*** Brand Submodel NT **/
    Route::resource('submodels', 'Admin\BrandSubModelController');
    Route::post('submodels-order-position', 'Admin\BrandSubModelController@submodelsOrderPosition')->name('submodelsOrderPosition');
    Route::get('changeSubmodelStatus', 'Admin\BrandSubModelController@changeStatus');
    Route::get('deleteSubModel/{id}', 'Admin\BrandSubModelController@deleteSubModel');
    Route::delete('submodels_mass_destroy', 'Admin\BrandSubModelController@massDestroy')->name('submodels.mass_destroy');
    Route::get('submodelDetails/{id}', 'Admin\BrandSubModelController@submodelDetails')->name('submodels.submodelDetails');

    Route::get('get-sub-brand-type-list', 'Admin\BrandSubModelController@getBrandTypes');

    /*** BrandModelScreenImageController **/
    Route::resource('models-screen-image', 'Admin\BrandModelScreenImageController');
    Route::post('models-screen-image-order-position', 'Admin\BrandModelScreenImageController@modelsScreenImageOrderPosition')->name('modelsScreenImageOrderPosition');
    Route::get('changeStatusModelScreen', 'Admin\BrandModelScreenImageController@changeStatusModelScreen');
    //Route::get('deleteModelScreenBltFiles', 'Admin\BrandModelScreenImageController@deleteModelScreenBltFiles');
    Route::get('deleteModelScreenImage', 'Admin\BrandModelScreenImageController@deleteModelScreenImage');
    Route::get('deleteModelScreenBltFiles/{id}', 'Admin\BrandModelScreenImageController@deleteModelScreenBltFiles');
    Route::delete('models_screen_image_mass_destroy', 'Admin\BrandModelScreenImageController@massDestroy')->name('models-screen-image.mass_destroy');
    Route::get('models-screen-image-list', 'Admin\BrandModelScreenImageController@modelScreenImageDetails')->name('models-screen-image-list');
    Route::get('deleteModelScreen/{id}', 'Admin\BrandModelScreenImageController@deleteModelScreen');


    Route::get('get-brand-list', 'Admin\BrandModelScreenImageController@getBrand');
    Route::post('addMoreImage', 'Admin\BrandModelScreenImageController@addMoreImage');

    /*** Bulk Upload NT **/
    Route::get('bulkupload', 'Admin\BulkUploadController@create')->name('bulkupload.create');
    Route::post('bulkupload/store', 'Admin\BulkUploadController@store')->name('bulkupload.store');
    Route::post('bulk-export', 'Admin\BulkUploadController@export')->name('bulkupload.export');

    /*** Retailers **/
    Route::resource('retailers', 'Admin\RetailerController');
    Route::post('retailers-list-deleteall', 'Admin\RetailerController@retailerDeleteAll');

    /*** Plans NT **/
    Route::resource('plans', 'Admin\PlanController');
    Route::get('changeStatusPlan', 'Admin\PlanController@changeStatus');
    Route::get('plan/delete/{id}', 'Admin\PlanController@deletePlan');
    Route::delete('plans_mass_destroy', 'Admin\PlanController@massDestroy')->name('plans.mass_destroy');
    Route::post('upload/serialnumber/{id}', 'Admin\PlanController@uploadSerialNumbers')->name('plans.upload.serialnumber');


    Route::resource('serial-numbers', 'Admin\SerialNumbersController');

    Route::resource('machine-numbers', 'Admin\MachineNumbersController');
    Route::post('upload/machinenumber', 'Admin\MachineNumbersController@uploadMachineNumbers')->name('upload.machinenumber');

    /*** Indivisual user commission change  NT **/
    Route::post('plan/change/{id}', 'Admin\RetailerController@changeCurrentPlan')->name('planChangeAdmin');

    /*customer Recharge Commission NT26022121*/
    Route::get('customer-recharge-commission', 'Admin\RetailerController@customerRechargeCommission')->name('customer-recharge-commission');
    Route::get('customer-recharge-commission-report', 'Admin\RetailerController@customerRechargeCommissionReport')->name('customer-recharge-commission-report');

    /*customer profile change NT26022121*/
    Route::get('retailer-profile-change-request', 'Admin\RetailerController@retailerProfileChangeRequestList')->name('retailer-profile-change-request');
    Route::get('change-profile-request-status', 'Admin\RetailerController@changeProfileRequestStatus');

    /*customer imei change request list and update NT26022121*/
    Route::get('imei-change-request-list', 'Admin\RetailerController@imeiChangeRequestList')->name('imei-change-request');
    Route::get('update-imei-request/{id}', 'Admin\RetailerController@updateImeiRequest');

    /*Currency show and update NT04032121*/
    Route::get('setting/currency', 'Admin\SettingController@currencySetting')->name('setting.currency');
    Route::get('setting/website', 'Admin\SettingController@websiteSetting')->name('setting.website');
    Route::get('setting/otpprice', 'Admin\SettingController@otppriceSetting')->name('setting.otpprice');
    Route::put('setting/otpprice-update', 'Admin\SettingController@otppriceUpdate')->name('setting.otpprice-update');
    Route::put('setting/currency-update', 'Admin\SettingController@updateCurrencySetting')->name('setting.currency-update');

    Route::post('addShopGallery', 'Admin\RetailerController@addShopGallery');
    Route::get('deleteShopGallery', 'Admin\RetailerController@deleteShopGallery');
    Route::get('get-state-list', 'Admin\UsersController@getStateList');
    Route::get('kyc/changeStatus', 'Admin\RetailerController@approveKyc');
    Route::post('kyc/adminChangeStatus', 'Admin\RetailerController@approveKycAdmin');

    Route::resource('recharge-plan', 'Admin\RechargePlanController');
    Route::get('recharge-plan/delete/{id}', 'Admin\RechargePlanController@deleteRechargePlan');
    Route::get('changeStatusRechargePlan', 'Admin\RechargePlanController@changeStatus');
    Route::resource('video', 'Admin\VideoController');
    Route::post('video-order-position', 'Admin\VideoController@videoOrderPosition')->name('videoOrderPosition');
    Route::get('video/delete/{id}', 'Admin\VideoController@deleteVideo');

    Route::resource('video-category', 'Admin\VideoCategoryController');
    Route::post('video-category-order-position', 'Admin\VideoCategoryController@videoCategoryOrderPosition')->name('videoCategoryOrderPosition');
    Route::get('video-category/delete/{id}', 'Admin\VideoCategoryController@deleteVideoCategory');
    Route::get('changeStatusVideoCategory', 'Admin\VideoCategoryController@changeStatus');

    /*PaperSizeController NT09031644*/
    /*Route::resource('/paper-size', 'Admin\PaperSizeController');
    Route::get('/paper-size/delete/{id}', 'Admin\PaperSizeController@deletePaperSize');
    Route::get('/changeStatusPaperSize/', 'Admin\PaperSizeController@changeStatus');
    */

    Route::resource('paper-sizes', 'Admin\PaperSizeController');
    /* ---------create by neha---------*/
    // Route::get('privacypolicy', 'Admin\SettingController@privacypolicy')->name('privacypolicy');

    Route::any('setting/privacypolicy', 'Admin\SettingController@privacypolicy')->name('setting.privacypolicy');
    Route::any('setting/setting_pdf', 'Admin\SettingController@settingpdf')->name('setting.setting_pdf');
    Route::post('setting/settingpdf-update', 'Admin\SettingController@updateSettingPdf')->name('setting.settingpdf-update');
    Route::post('setting/settingpdf-order', 'Admin\SettingController@updateSettingPdfOrder')->name('setting.settingpdf-order');
    Route::post('setting/settingpdf-delete', 'Admin\SettingController@updateSettingPdfDelete')->name('setting.settingpdf-delete');

    Route::put('setting/privacypolicy-update', 'Admin\SettingController@updatePrivacypolicy')->name('setting.privacypolicy-update');
    Route::post('setting/listprivacypolicy', 'Admin\SettingController@listprivacypolicy')->name('setting.listprivacypolicy');


    /* Route::get('customer-recharge-commission', 'Admin\RetailerController@customerRechargeCommission')->name('customer-recharge-commission');*/


    Route::put('fix-commission-update', 'Admin\RetailerController@fixcommissionupdate')->name('fixcommissionupdate');
    Route::put('fix-reward-update', 'Admin\RetailerController@fixrewardupdate')->name('fixrewardupdate');

    Route::get('paper-sizes/delete/{paperSize}', 'Admin\PaperSizeController@deletePaperSize')->name('paper-sizes.delete');
    Route::get('/changeStatusPaperSize/', 'Admin\PaperSizeController@changeStatus');


    Route::resource('generalnotification', 'Admin\GeneralNotificationController');
    Route::post('generalnotification/save', 'Admin\GeneralNotificationController@savenotification')->name('savenotification');
    Route::post('generalnotification/delete/{id}', 'Admin\GeneralNotificationController@delete')->name('deletenotification');
    Route::resource('banner', 'Admin\BannerController');
    Route::post('banner-order-position', 'Admin\BannerController@bannerOrderPosition')->name('bannerOrderPosition');
    Route::get('createBanner/{id}', 'Admin\BannerController@create')->name('createBanner');
    Route::get('bannerdetails/{id}', 'Admin\BannerController@bannerDetails')->name('bannerDetails');
    Route::get('banner/delete/{id}', 'Admin\BannerController@delete');
    Route::get('changeStatusBanner', 'Admin\BannerController@changeStatus');

    Route::resource('bulkcut-plan', 'Admin\BulkCutPlanController');
    Route::get('bulkcut/delete/{id}', 'Admin\BulkCutPlanController@deleteBulkCutPlan');
    Route::get('changeStatusBulkCutPlan', 'Admin\BulkCutPlanController@changeStatus');
    //Divya Route


    /*Member Tree*/
    Route::get('member-tree', 'Admin\RetailerController@showMemberTree')->name('members-tree');
    Route::get('tree-view-retailers/{id}', 'Admin\RetailerController@showMemberTreeDetail')->name('tree-view-retailers');


    Route::get('setting/credit-point', 'Admin\SettingController@creditPoint')->name('setting.credit-point');
    Route::put('setting/credit-point-setting', 'Admin\SettingController@creditPointSetting')->name('setting.credit-point-setting');

    /*commission set*/
    Route::get('setting/commission/{id?}', 'Admin\SettingController@commission')->name('setting.commission');
    Route::put('setting/commission-setting', 'Admin\SettingController@commissionSetting')->name('setting.commission-setting');
    Route::get('setting/commission-setting-report', 'Admin\SettingController@commissionSettingReport')->name('setting.commission-setting-report');

    Route::get('direct-tree', 'Admin\RetailerController@showDirectTree')->name('direct-tree');
    Route::get('direct-users-list', 'Admin\RetailerController@showDirectUserList')->name('direct-users-list');
    Route::get('users-monthly-recharge', 'Admin\RetailerController@usersMonthlyRecharge')->name('users-monthly-recharge');
    Route::get('monthly-recharge-report', 'Admin\RetailerController@monthlyRechargeReport')->name('monthly-recharge-report');
    Route::get('users-credit-purchase', 'Admin\RetailerController@usersCreditPurchase')->name('users-credit-purchase');
    Route::get('credit-purchase-report', 'Admin\RetailerController@creditPurchaseReport')->name('credit-purchase-report');

    Route::get('recharge-commission', 'Admin\RetailerController@rechargeCommission')->name('recharge-commission');
    Route::get('recharge-commission-report', 'Admin\RetailerController@rechargeCommissionReport')->name('recharge-commission-report');
    Route::get('credit-wallet-transaction', 'Admin\RetailerController@creditWalletReport')->name('credit-wallet-transaction');
    Route::get('credit-wallet-report', 'Admin\RetailerController@creditWalletReport')->name('credit-wallet-report');
    Route::get('cash-wallet-transaction', 'Admin\RetailerController@cashWalletTransaction')->name('cash-wallet-transaction');
    Route::get('cash-wallet-report', 'Admin\RetailerController@cashWalletReport')->name('cash-wallet-report');

    Route::get('buyer-list/{id?}', 'Admin\RetailerController@showCustomer')->name('buyer-list');
    Route::get('retailer-customer-list', 'Admin\RetailerController@showCustomerList')->name('retailer-customer-list');
    Route::get('retailer-wise-buyer-list', 'Admin\RetailerController@showRetailerWiseCustomer')->name('retailer-wise-buyer-list');

    Route::get('retailer-credit-balance', 'Admin\RetailerController@showRetailerCreditBalance')->name('retailer-credit-balance');
    Route::get('retailer-credit-balance-report', 'Admin\RetailerController@showRetailerCreditBalanceList')->name('retailer-credit-balance-report');
    Route::get('add-credit-point/{id?}', 'Admin\RetailerController@addCreditPoint')->name('add-credit-point');
    Route::post('add-credit-point-summary', 'Admin\RetailerController@addCreditsSummary')->name('add-credit-point-summary');
    Route::post('add-credit-manual', 'Admin\RetailerController@purchaseCredits')->name('add-credit-manual');

    Route::get('recharge-commission-payout', 'Admin\RechargeCommissionPayout@rechargeCommissionPayout')->name('recharge-commission-payout');
    Route::get('recharge-commission-payout-report', 'Admin\RechargeCommissionPayout@rechargeCommissionPayoutReport')->name('recharge-commission-payout-report');
    Route::put('commission-payout', 'Admin\RechargeCommissionPayout@commissionPayout')->name('commission-payout');

    Route::get('commission-slab-tree-view', 'Admin\RetailerController@commissionSlabTreeViewRetailers')->name('commission-slab-tree-view');

    Route::get('machine-numbers-list', 'Admin\MachineNumbersController@machineNumberDetails')->name('machine-numbers-list');
    Route::get('serial-numbers-list', 'Admin\SerialNumbersController@serialNumberDetails')->name('serial-numbers-list');
    Route::get('paper-sizes-list', 'Admin\PaperSizeController@paperSizeDetails')->name('paper-sizes-list');
    Route::get('user-reactions-list', 'Admin\UserReactionsController@userReactionsDetails')->name('user-reactions-list');
    Route::get('complaint-feedback-list', 'Admin\ComplaintFeedbackController@complaintFeedbackDetails')->name('complaint-feedback-list');
    Route::get('feedback-list', 'Admin\ComplaintFeedbackController@feedbackindex')->name('feedback-list');
    Route::post('complaint_changestatus', 'Admin\ComplaintFeedbackController@change_staus')->name('complaint_changestatus');
    Route::post('changeStausNotification', 'Admin\ComplaintFeedbackController@changeStausNotification')->name('changeStausNotification');

    Route::get('generalnotification-list', 'Admin\GeneralNotificationController@generalNotificationDetails')->name('generalnotification-list');
    Route::post('sendNotification', 'Admin\GeneralNotificationController@notificationSend')->name('notificationSend');
    Route::get('bulkcut-plan-list', 'Admin\BulkCutPlanController@bulkcutPlanDetails')->name('bulkcut-plan-list');
    Route::get('plans-list', 'Admin\PlanController@planDetails')->name('plans-list');

    Route::get('retailer-profile-change-request-list', 'Admin\RetailerController@paperSizeDetails')->name('retailer-profile-change-request-list');

    Route::get('recharge-plan-list', 'Admin\RechargePlanController@paperSizeDetails')->name('recharge-plan-list');
    Route::get('retailer-credit-balance-list', 'Admin\RetailerController@paperSizeDetails')->name('retailer-credit-balance-list');


    Route::get('users-list', 'Admin\UsersController@paperSizeDetails')->name('users-list');




    Route::get('retailers-list', 'Admin\RetailerController@retailerDetails')->name('retailers-list');
    Route::get('retailers-list-report', 'Admin\RetailerController@retailerDetailsReport')->name('retailers-list-report');
    Route::post('retailer-change-password/{user}', 'Admin\RetailerController@retailerChangePassword')->name('retailer-change-password');
    Route::resource('complaint-feedback', 'Admin\ComplaintFeedbackController');
    Route::post('compaint-feedback-update/{id}', 'Admin\ComplaintFeedbackController@update')->name('update-compaint-feedback-status');
    //----- Route for Manage Statewise-Gst--------

    Route::resource('user-reactions', 'Admin\UserReactionsController');
    Route::post('user-reactions-update/{id}', 'Admin\UserReactionsController@update')->name('update-user-reactions-status');


    /**
     * Staffs Route @narenthaker July 14, 2021 15:00
     */
    Route::get('changeStatusModelScreenofretailer', 'Admin\StaffController@changeStatusModelScreenofretailer')->name('changeStatusModelScreenofretailer');
    Route::get('staffs-list', 'Admin\StaffController@staffsDetails')->name('staffs-list');
    Route::get('screenuploadlist/{userid}', 'Admin\StaffController@screenuploadlist')->name('screenuploadlist');
    Route::get('retailerlist', 'Admin\StaffController@retailerlist')->name('retailerlist');
    Route::get('viewupload/{id}', 'Admin\StaffController@viewuploadlistbyretailer')->name('viewuploadlistbyretailer');
    Route::get('staffs-list-report', 'Admin\StaffController@staffDetailsReport')->name('staffs-list-report');
    Route::resource('staffs', 'Admin\StaffController');

    Route::get('license-key-list', [LicensekeyController::class, 'index'])->name('licenseKeyList');
    Route::get('license-key-create', [LicensekeyController::class, 'create'])->name('licenseKeyCreate');
    Route::post('license-key-store', [LicensekeyController::class, 'store'])->name('licenseKeyStore');
});


Route::group(['middleware' => ['auth'], 'prefix' => 'user', 'as' => 'user.'], function () {

    Route::get('/home', 'User\HomeController@retailerDashbord')->name('home');
    Route::get('chart/getchart', 'User\HomeController@getchart');
    Route::get('update-profile', 'User\UserController@updateProfile')->name('update-profile');
    Route::post('update-general-information/{id}', 'User\UserController@updateGeneralInformation')->name('general-information.update');
    Route::get('get-state-list', 'User\UserController@getStateList');
    Route::get('get-plans-list', 'User\UserController@getPlanList');
    Route::get('get-serialnumber-list', 'User\UserController@getSerialNumberList');
    Route::post('update-user-contact/{id}', 'User\UserController@updateContactDetail')->name('contact-detail.update');
    Route::post('update-user-bank-detail/{id}', 'User\UserController@updateBankDetail')->name('bank-detail.update');
    Route::post('update-user-kyc-detail/{id}', 'User\UserController@updateKycDetail')->name('kyc-detail.update');
    Route::post('update-user-shop-detail/{id}', 'User\UserController@updateShopDetail')->name('shop-detail.update');

    //User brands
    Route::resource('brands', 'User\BrandController');
    Route::resource('brand-type', 'User\BrandTypeController');
    Route::resource('models', 'User\BrandModelController');
    Route::resource('models-screen-image', 'User\BrandModelScreenImageController');

    Route::get('monthly-recharge', 'User\UserController@showMonthlyRecharge')->name('monthly-recharge');
    Route::match(array('GET', 'POST'), 'recharge-summary', 'User\UserController@monthlyRechargeSummary')->name('recharge-summary');
    Route::match(array('GET', 'POST'), 'recharge', 'User\UserController@monthlyRechargePayment')->name('recharge');
    Route::match(array('GET', 'POST'), 'monthly-recharge-cancel',  'User\UserController@monthlyRechargeSuccess');
    Route::match(array('GET', 'POST'), 'monthly-recharge-response', 'User\UserController@monthlyRechargeSuccess');
    Route::get('direct-tree', 'User\UserController@showDirectTree')->name('direct-tree');
    Route::get('credit-wallet', 'User\CreditWalletController@creditTransactionReport')->name('credit-wallet');
    Route::get('cash-wallet', 'User\CashWalletController@cashTransactionReport')->name('cash-wallet');
    Route::get('purchase-credits', 'User\CreditWalletController@showPurchaseCredits')->name('purchase-credits');
    Route::post('purchase-credits-summary', 'User\CreditWalletController@purchaseCreditsSummary')->name('purchase-credits-summary');
    Route::post('credit-purchase', 'User\CreditWalletController@purchaseCredits')->name('credit-purchase');
    Route::match(array('GET', 'POST'), 'credit-order-cancel',  'User\CreditWalletController@creditPurchaseCancel');
    Route::match(array('GET', 'POST'), 'credit-order-response', 'User\CreditWalletController@creditPurchaseSuccess');

    Route::get('buyer-list', 'User\UserController@showCustomerList')->name('buyer-list');
    Route::get('monthly-recharge-report', 'User\UserController@showMonthlyRechargeReport')->name('monthly-recharge-report');
    Route::get('credit-purchase-report', 'User\UserController@showCreditPurchaseReport')->name('credit-purchase-report');
    Route::get('recharge-commission-report', 'User\UserController@showRechargeCommissionReport')->name('recharge-commission-report');
});
