<?php

// https://www.avyatech.com/rest-api-with-laravel-8-using-jwt-token/

use App\Http\Controllers\Api\V1\APIController;
use App\Http\Controllers\Api\V1\UserController;
use  App\Http\Controllers\Api\V1\MainController;

Route::group(['prefix' => '/v2'], function () {

    Route::post('user-login', [MainController::class, 'login']);

    Route::post('check-email', 'Api\V1\APIController@checkEmail');
    Route::post('reg', 'Api\V1\MainController@reg1');
});


Route::group(['middleware' => 'jwt.verify'], function () {

    Route::get('checkJWT', function () {
        return auth()->user();
    });
});


Route::group(['prefix' => '/v1', 'namespace' => 'api\v1', 'as' => 'api.'], function () {

    // Route::get('/fetch', [APIController::class, 'checkFun']);

    Route::post('login', 'APIController@login');
    Route::post('new-login', 'APIController@login');
    Route::post('login-otp-verify', 'APIController@LoginOtpverify');
    Route::post('resendotp', 'APIController@resendotp');
    Route::post('reg', 'APIController@reg1');
    Route::post('otp-verify', 'APIController@verifyOtp');
    Route::post('recover_password', 'APIController@recoverPassord');
    Route::post('reset_password', 'APIController@resetPassword');
    Route::post('check-email', 'APIController@checkEmail');
    Route::get('get-banner', 'APIController@getBanner');
    Route::get('country-list', 'APIController@getCountryList');
    Route::get('state-list', 'APIController@getStateList');
    Route::get('paper-size-list', 'APIController@getPaperSizeList');
    Route::post('getImage', 'UserController@getImage');
    Route::get('get-app-version', 'APIController@getAppVersion');


    /* 	-------neha------ */
    Route::post('listprivacypolicy', 'PrivacypolicyController@listprivacypolicy');
    Route::post('settingpdf', 'PrivacypolicyController@settingpdf');
    Route::post('listfixcommission', 'ListfixcommissionController@listfixcommission');


    Route::group(['middleware' => 'auth.jwt'], function () {

        Route::get('/fetch', [APIController::class, 'checkFun']);

        Route::get('logout', 'APIController@logout');

        //Profile 
        Route::get('get-user-current-plan', 'UserController@getUserCurrentPlanDetails');
        Route::post('accept-privacy-policy', 'UserController@acceptPrivacyPolicy');

        Route::get('get-profile', [UserController::class, 'getUserProfile']);

        Route::post('update-profile', 'UserController@updateProfile');
        Route::post('profile-pic-upload', 'UserController@profilePicUpload');
        Route::post('send-user-profile-change-request', 'UserController@sendUserProfileChangeRequest');

        Route::post('get-user-profile-change-request-status/{user_id}', 'UserController@getUserProfileChangeRequestStatus');

        //Sub User Create with role 
        Route::get('get-sub-user', 'UserController@getSubUser');
        Route::post('sub-user-create', 'UserController@subUserCreate');
        Route::post('sub-user-status-change-request', 'UserController@subUserStatusChangeRequest');
        Route::post('sub-user-count', 'UserController@subUserCount');


        Route::get('serach', 'BrandController@Serach');
        Route::get('get-brand-type-list', 'BrandController@getBrandTypeList');
        Route::get('get-brand-list', 'BrandController@getBrandList')->name('get-brand-list');
        Route::get('get-all-brand-list', 'BrandController@getAllBrandList')->name('get-all-brand-list');
        Route::get('get-brand-model-list', 'BrandController@getBrandModelList');
        Route::get('get-brand-sub-model-list', 'BrandController@getBrandSubModelList');
        Route::get('get-brand-model-screen-list', 'BrandController@getBrandModelScreenList');
        Route::get('get-brand-model-screen-list-with-blt', 'BrandController@getBrandModelScreenListWithBlt');
        Route::get('get-today-model-list', 'BrandController@getTodayBrandModelList');
        Route::get('get-brand-model-list-releasewise', 'BrandController@getTodayBrandModelListReleasewise');

        //Route for user contact , bannk and KYC details
        Route::get('get-user-contact-detail', 'UserController@getUserContactDetail');
        Route::post('update-user-contact-detail', 'UserController@UpdateUserContactDetail');
        Route::get('get-user-bank-detail', 'UserController@getUserBankDetail');
        Route::post('update-user-bank-detail', 'UserController@UpdateUserBanktDetail');
        Route::get('get-kyc-type', 'UserController@getKyctypeList');
        Route::get('get-user-kyc-detail', 'UserController@getUserKycDetail');
        Route::post('update-user-kyc-detail', 'UserController@UpdateUserKycDetail');
        Route::post('kyc-file-upload', 'UserController@kycFileUpload');
        Route::post('update-user-shop-detail', 'UserController@UpdateUserShopDetail');

        //Credit point
        Route::get('get-credit-wallet', 'UserWalletController@getCreditWallet');
        Route::get('get-debit-wallet', 'UserWalletController@getDebitWallet');
        Route::get('get-credit-points', 'UserWalletController@getCreditPoints');

        Route::get('get-wallet-detail', 'UserWalletController@getWalletDetail');
        Route::post('get-token', 'UserWalletController@getpurchaseCreditsPointToken');
        Route::post('credit-purchase', 'UserWalletController@creditPurchaseSuccess');

        // Plan
        Route::get('get-plan-list', 'RechargePlanController@getPlanList');

        // Bulkcut Plan
        Route::get('get-bulkcut-plan', 'BulkCutPlanController@getBulkcutList');
        Route::post('recharge-bulk-plan', 'BulkCutPlanController@rechargeBulkplanPayment');
        Route::post('update-bulkcut-wallet', 'BulkCutPlanController@updateBulkCutWallet');
        Route::get('get-bulkcut-wallet', 'BulkCutPlanController@getBulkCutWalletHistory');


        Route::post('set-bulkcut-on', 'BulkCutPlanController@setBulkCutOn');
        Route::get('get-bulkcut-off', 'BulkCutPlanController@getBulkCutOff');

        // Rewards
        Route::get('get-reward-list', 'UserController@getRewardList');

        //recharge
        Route::get('get-recharge-detail', 'UserWalletController@getMonthlyRechargeInformation');
        Route::post('recharge', 'UserWalletController@monthlyRechargePayment');
        Route::post('recharge-plan', 'UserWalletController@monthlyRechargePaymentNew');
        Route::post('recharge-cashfree-payment', 'UserWalletController@monthlyRechargeSuccess');

        // Dashboard
        Route::get('get-dashboard-detail', [UserController::class, 'userDashboardDetail']);


        //Commission Report
        Route::get('get-commission-report', 'UserWalletController@getRechargeCommissionReport');

        //Buyers create and listing
        Route::post('customer-registration', 'APIController@customerRegistration');
        Route::get('get-customer-detail', 'UserController@getCustomerDetail');

        //Bulk quantity status
        Route::post('update-bulk-quantity', 'APIController@updateBulkQuantity');
        Route::post('check-bulk-status', 'APIController@checkBulkStatus');

        //Retailer chain
        Route::get('get-member-tree', 'UserController@showDirectMemberTree');
        Route::post('payout-request', 'UserController@requestPayout');
        Route::get('get-payout-request', 'UserController@getRequestPayout');

        //Tutorial and Video Tutorial 
        Route::get('get-videos-category', 'VideoController@getVideoCategory');
        Route::get('get-videos/{id}', 'VideoController@getVideos');
        Route::get('get-tutorial', 'VideoController@getTutorial');
        Route::get('get-video-tutorial', 'VideoController@getVideoTutorial');

        //complaint-feedback
        Route::post('complaint-feedback', 'UserController@complaintFeedback');
        Route::get('get-complaint-feedback', 'UserController@getcomplaintFeedback');

        // get notification
        Route::get('get-notification-list', 'UserController@getNotification');
        Route::post('update-notification-status', 'UserController@updatenotificationstatus');
        //Cut Wallet
        Route::get('get-cut-wallet-list', 'BulkCutPlanController@getCutWalletList');
        Route::post('update-cut-wallet', 'BulkCutPlanController@updateCutWalletList');
        Route::post('credit-off-line-cut', 'BulkCutPlanController@creditOffLineCut');

        /*         * ** @narenthaker 09072021 ** */
        Route::post('staff-kyc-upload', 'UserController@staffKycUpload');


        Route::post('add-design', 'DesignController@add');
        Route::post('update-design', 'DesignController@update');
        Route::post('delete-design', 'DesignController@delete');
        Route::post('list-design', 'DesignController@designList');

        Route::post('user/reaction', 'DesignController@userReaction');
        Route::post('user/check', 'DesignController@validateUser');

        Route::get('get-active-plan', 'UserWalletController@getActivePlan');
        Route::get('get-upcoming-plan', 'UserWalletController@getUpcomingPlan');
        Route::get('get-expired-plan', 'UserWalletController@getExpiredPlan');
        Route::post('get-plan-price', 'UserWalletController@getPlanPrice');
        Route::post('check-expiry-date', 'UserWalletController@checkExpiryDate');
    });
});
