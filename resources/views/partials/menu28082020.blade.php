
    <!-- Mobile Menu start -->
    <div class="mobile-menu-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="mobile-menu">
                        <nav id="dropdown">
                            <ul class="mobile-menu-nav">
							  @if(Auth::user()->hasRole('administrator'))
                                <li><a data-toggle="collapse" data-target="#home" href="{{ route('admin.home') }}">{{ trans('global.dashboard') }}</a>
                                </li>
								
                                <li><a data-toggle="collapse" data-target="#user_management" href="#">{{ trans('cruds.userManagement.title') }}</a>
                                <ul id="user_management" class="collapse dropdown-header-top">
                                <li> <a href="{{ route('admin.permissions.index') }}" >{{ trans('cruds.permission.title') }}</a></li>
                                <li><a href="{{ route('admin.roles.index') }}" >{{ trans('cruds.role.title') }}</a></li>
                                <li> <a href="{{ route('admin.users.index') }}" >{{ trans('cruds.user.title') }}</a></li>
                                </ul>
                                </li>
								 <li><a data-toggle="collapse" data-target="#mobile_brands" href="#">Brands</a>
                                <ul id="mobile_brands" class="collapse dropdown-header-top">
                                <li> <a href="{{ route('admin.brand-type.index') }}" >Brand Types</a></li>
                                <li><a href="{{ route('admin.brands.index') }}" >Brands</a></li>
                                <li> <a href="{{ route('admin.models.index') }}" >Models</a></li>
								<li> <a href="{{ route('admin.models-screen-image.index') }}" >Upload Model Screen</a></li>
                                </ul>
                                </li>
								<li><a data-toggle="collapse" data-target="#retailer_management" href="#">Retailer Management</a>
                                <ul id="retailer_management" class="collapse dropdown-header-top">
									<li> <a href="{{ route('admin.retailers-list') }}" >Retailers</a></li>
									<li> <a href="{{ route('admin.direct-tree') }}" >Direct Members</a></li>
									<li> <a href="{{ route('admin.tree-view-retailers') }}" >Members Tree</a></li>
                                    <li> <a href="{{ route('admin.commission-slab-tree-view') }}" >Members</a></li>
									<li> <a href="{{ route('admin.users-monthly-recharge') }}" >Monthly Recharge</a></li>
									<li> <a href="{{ route('admin.users-credit-purchase') }}" >Credit Purchase</a></li>  
									<li> <a href="{{ route('admin.recharge-commission') }}" >Recharge Commission</a></li> 
									<li> <a href="{{ route('admin.recharge-commission-payout') }}" >Recharge Commission Payout</a></li>  
									<li> <a href="{{ route('admin.credit-wallet-transaction') }}" >Credit Wallet Transaction</a></li>  
									<li> <a href="{{ route('admin.cash-wallet-transaction') }}" >Cash Wallet Transaction</a></li> 
									<li> <a href="{{ route('admin.buyer-list') }}" >Buyer List</a></li> 
									<li> <a href="{{ route('admin.retailer-credit-balance') }}" >Credit Balance</a></li> 
                                    <li> <a href="{{ route('admin.recharge-plan.index') }}" >Recharge Plan</a></li> 
                                    <li> <a href="{{ route('admin.bulkcut-plan.index') }}" >Bulkcut Plan</a></li> 
                                </ul>
                                </li>
								<li><a data-toggle="collapse" data-target="#mobile_brands" href="#">Setting</a>
                                <ul id="mobile_brands" class="collapse dropdown-header-top">
                                <li> <a href="{{ route('admin.setting.credit-point') }}" >Credit Point</a></li>
                                <li><a href="{{ route('admin.setting.commission','') }}" >Commission</a></li>
                                <li><a href="{{ route('admin.setting.commission-setting-report','') }}" >Commission Setting Report</a></li>
                                <li><a href="{{ route('admin.generalnotification.index') }}" >Notification</a></li>
                                </ul>
                                </li>
                                <li><a data-toggle="collapse" data-target="#vidoes" href="{{ route('admin.video.index') }}">Vidoes</a>
								@endif
								@if(Auth::user()->hasRole('retailer'))
						        @if(Auth::user()->status == 1)
							    <li><a data-toggle="collapse" data-target="#home" href="{{ route('user.home') }}">{{ trans('global.dashboard') }}</a></li>
                                
								<li><a data-toggle="collapse" data-target="#brands_management" href="#">Brands</a>
                                <ul id="brands_management" class="collapse dropdown-header-top">
									 <li> <a href="{{ route('user.brand-type.index') }}" >Brand Types</a></li>
									<li><a href="{{ route('user.brands.index') }}" >Brands</a></li>
									<li> <a href="{{ route('user.models.index') }}" >Models</a></li>
									<li> <a href="{{ route('user.models-screen-image.index') }}" >Model Screen</a></li>
                                </ul>
                                </li>	
								<li><a data-toggle="collapse" data-target="#account_management" href="#">Manage Account</a>
                                <ul id="account_management" class="collapse dropdown-header-top">
									    <li> <a href="{{route('user.update-profile')}}" >Manage Profile</a></li>
										<li><a href="{{ route('user.monthly-recharge') }}" >Monthly Recharge</a></li>
										<li> <a href="{{ route('user.direct-tree') }}" >My Direct Tree</a></li>
										<li> <a href="{{ route('user.buyer-list') }}">Buyer List</a></li>
										<li> <a href="{{ route('user.monthly-recharge-report') }}">Monthly Recharge Report</a></li>
										<li> <a href="{{ route('user.credit-purchase-report') }}">Credit Purchase Report</a></li>
										<li> <a href="{{ route('user.recharge-commission-report') }}">Recharge Commission Report</a></li>
                                </ul>
                                </li>
								<li><a data-toggle="collapse" data-target="#wallet_management" href="#">Manage Wallets</a>
                                <ul id="wallet_management" class="collapse dropdown-header-top">
									     <li> <a href="{{route('user.credit-wallet')}}" >Credit Wallet</a></li>
                                         <li><a href="{{ route('user.cash-wallet') }}" >Cash Wallet</a></li>
                                         <li> <a href="{{ route('user.purchase-credits') }}" >Purchase Credits</a></li>
                                </ul>
                                </li>
								@else
							    <li><a data-toggle="collapse" data-target="#wallet_management" href="#" class="inactive-warning">{{ trans('global.dashboard') }}</a> </li>
							    <li><a data-toggle="collapse" data-target="#wallet_management" href="#" class="inactive-warning">Brands</a> </li>
								<li><a data-toggle="collapse" data-target="#account_management" href="#">Manage Account</a>
                                <ul id="account_management" class="collapse dropdown-header-top">
									    <li> <a href="{{route('user.update-profile')}}" >Manage Profile</a></li>
										<li><a href="{{ route('user.monthly-recharge') }}" >Monthly Recharge</a></li>
										<li> <a href="{{ route('user.direct-tree') }}" >My Direct Tree</a></li>
										<li> <a href="{{ route('user.buyer-list') }}">Buyer List</a></li>
										<li> <a href="{{ route('user.monthly-recharge-report') }}">Monthly Recharge Report</a></li>
										<li> <a href="{{ route('user.credit-purchase-report') }}">Credit Purchase Report</a></li>
										<li> <a href="{{ route('user.recharge-commission-report') }}">Recharge Commission Report</a></li>
                                </ul>
                                </li>
								<li><a data-toggle="collapse" data-target="#wallet_management" href="#" class="inactive-warning">Manage Wallets</a> </li>
								
                                
                               
								   
								@endif
								
								@endif
								
                                <li><a data-toggle="collapse" data-target="#change_password" href="{{ route('auth.change_password') }}">Change password</a>
                                </li>
								 <li><a data-toggle="collapse" data-target="#logout" href="#" onclick="event.preventDefault(); document.getElementById('logoutform').submit();">{{ trans('global.logout') }}</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Mobile Menu end -->
	
	
    <!-- Main Menu area start-->
    <div class="main-menu-area mg-tb-40">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <ul class="nav nav-tabs notika-menu-wrap menu-it-icon-pro">
					
						@if(Auth::user()->hasRole('administrator'))
						 <li class="{{ request()->is('admin/home') || request()->is('admin/home/*') ? 'active' : '' }}"><a href="{{ route('admin.home') }}"><i class="notika-icon notika-house"></i> {{ trans('global.dashboard') }}</a>
                        </li>	
                        <li class="{{ request()->is('admin/permissions') || request()->is('admin/permissions/*') || request()->is('admin/roles') || request()->is('admin/roles/*') || request()->is('admin/users') || request()->is('admin/users/*') ? 'active' : '' }}"><a data-toggle="tab" href="#user_manage"><i class="notika-icon notika-support"></i> {{ trans('cruds.userManagement.title') }}</a>
                        </li>
					  <li class="{{ request()->is('admin/brand-type') || request()->is('admin/brand-type/*') || request()->is('admin/brands') || request()->is('admin/brands/*') || request()->is('admin/models') || request()->is('admin/models/*') || request()->is('admin/models-screen-image') || request()->is('admin/models-screen-image/*') ? 'active' : '' }}"><a data-toggle="tab" href="#brands"><i class="fa fa-list"></i> Brands</a>
                        </li>
						<li class="{{ request()->is('admin/setting/credit-point') || request()->is('admin/setting/credit-point/*') || request()->is('admin/setting/commission') || request()->is('admin/setting/commission/*') || request()->is('admin/setting/commission-setting-report') || request()->is('admin/setting/commission-setting-report/*') || request()->is('admin/generalnotification/*') ? 'active' : '' }}"><a data-toggle="tab" href="#setting"><i class="notika-icon notika-app"></i> Setting</a>
                        </li>
						
						  <li class="{{ request()->is('admin/retailers') || request()->is('admin/retailers/*') || request()->is('admin/commission-slab-tree-view') || request()->is('admin/direct-tree') || request()->is('admin/direct-tree/*') || request()->is('admin/users-monthly-recharge') || request()->is('admin/users-monthly-recharge/*') || request()->is('admin/recharge-commission') || request()->is('admin/recharge-commission/*') || request()->is('admin/credit-wallet-transaction') || request()->is('admin/credit-wallet-transaction/*') || request()->is('admin/cash-wallet-transaction') || request()->is('admin/cash-wallet-transaction/*') || request()->is('admin/buyer-list') || request()->is('admin/buyer-list/*') || request()->is('admin/retailer-credit-balance') || request()->is('admin/retailer-credit-balance/*') || request()->is('admin/recharge-plan/*') ? 'active' : '' }}"><a data-toggle="tab" href="#retailers"><i class="notika-icon notika-support"></i> Retailer Management</a>
                        </li>
                        <li class="{{ request()->is('admin/video') || request()->is('admin/video/*') ? 'active' : '' }}"><a href="{{ route('admin.video.index') }}"><i class="notika-icon notika-cloud"></i> {{ trans('global.video') }}</a>
					    @endif
						
						
						@if(Auth::user()->hasRole('retailer'))
						@if(Auth::user()->status == 1)	
						 <li class="{{ request()->is('user/home') || request()->is('user/home/*') ? 'active' : '' }}"><a href="{{ route('user.home') }}"><i class="notika-icon notika-house"></i> {{ trans('global.dashboard') }}</a>
                        </li>
                        <li class="{{ request()->is('user/brand-type') || request()->is('user/brand-type/*') || request()->is('user/brands') || request()->is('user/brands/*') || request()->is('user/models') || request()->is('user/models/*') || request()->is('user/models-screen-image') || request()->is('user/models-screen-image/*') ? 'active' : '' }}"><a data-toggle="tab" href="#brands"><i class="fa fa-list"></i> Brands</a>
                        </li>
                          <li class="{{ request()->is('user/update-profile') || request()->is('admin/update-profile/*') || request()->is('admin/monthly-recharge') || request()->is('admin/monthly-recharge/*') || request()->is('admin/direct-tree') || request()->is('admin/direct-tree/*') || request()->is('user/buyer-list') || request()->is('user/buyer-list/*') || request()->is('user/monthly-recharge-report') || request()->is('user/monthly-recharge-report/*') || request()->is('user/credit-purchase-report') || request()->is('user/credit-purchase-report/*') || request()->is('user/recharge-commission-report') || request()->is('user/recharge-commission-report/*') ? 'active' : '' }}"><a data-toggle="tab" href="#manage_account" ><i class="notika-icon notika-support"></i> Manage Account</a>
                        </li>
						<li class="{{ request()->is('user/credit-wallet') || request()->is('admin/credit-wallet/*') || request()->is('admin/cash-wallet') || request()->is('admin/cash-wallet/*') || request()->is('admin/purchase-credits') || request()->is('admin/purchase-credits/*') ? 'active' : '' }}"><a data-toggle="tab" href="#manage_wallet" ><i class="notika-icon notika-dollar"></i> Manage Wallets</a>
                        </li>
                        @else
							
						<li class="{{ request()->is('user/home') || request()->is('user/home/*') ? 'active' : '' }}"><a href="{{ route('user.home') }}"><i class="notika-icon notika-house"></i> {{ trans('global.dashboard') }}</a>
                        </li>
                        <li class="{{ request()->is('user/brand-type') || request()->is('user/brand-type/*') || request()->is('user/brands') || request()->is('user/brands/*') || request()->is('user/models') || request()->is('user/models/*') || request()->is('user/models-screen-image') || request()->is('user/models-screen-image/*') ? 'active' : '' }}"><a href="#" class="inactive-warning" ><i class="fa fa-list"></i> Brands</a>
                        </li>
                         <li class="{{ request()->is('user/update-profile') || request()->is('admin/update-profile/*') || request()->is('admin/monthly-recharge') || request()->is('admin/monthly-recharge/*') || request()->is('admin/direct-tree') || request()->is('admin/direct-tree/*') ? 'active' : '' }}"><a data-toggle="tab" href="#manage_account" ><i class="notika-icon notika-support"></i> Manage Account</a>
                        </li>
						<li class="{{ request()->is('user/credit-wallet') || request()->is('admin/credit-wallet/*') || request()->is('admin/cash-wallet') || request()->is('admin/cash-wallet/*') || request()->is('admin/purchase-credits') || request()->is('admin/purchase-credits/*') ? 'active' : '' }}"><a href="#" class="inactive-warning" ><i class="notika-icon notika-dollar"></i> Manage Wallets</a>
                        </li>		
						 					
						@endif	
						@endif	
                        <li class="{{ request()->is('change_password') || request()->is('change_password/*') ? 'active' : '' }}"><a href="{{ route('auth.change_password') }}"><i class="notika-icon notika-edit"></i> Change password</a>
                        </li>
                        <li><a data-toggle="tab" href="#" onclick="event.preventDefault(); document.getElementById('logoutform').submit();"><i class="notika-icon notika-form"></i> {{ trans('global.logout') }}</a>
                        </li>
						 
						
                       
                    </ul>
                    <div class="tab-content custom-menu-content">
                       @if(Auth::user()->hasRole('administrator'))
					   <div id="user_manage" class="tab-pane in {{ request()->is('admin/permissions') || request()->is('admin/permissions/*') || request()->is('admin/roles') || request()->is('admin/roles/*') || request()->is('admin/users') || request()->is('admin/users/*') ? 'active' : '' }} notika-tab-menu-bg animated flipInX">
                            <ul class="notika-main-menu-dropdown">
                             <li> <a href="{{ route('admin.permissions.index') }}" >{{ trans('cruds.permission.title') }}</a></li>
                             <li><a href="{{ route('admin.roles.index') }}" >{{ trans('cruds.role.title') }}</a></li>
                             <li> <a href="{{ route('admin.users.index') }}" >{{ trans('cruds.user.title') }}</a></li>
							
                            </ul>
                        </div>

						 <div id="brands" class="tab-pane in {{ request()->is('admin/brand-type') || request()->is('admin/brand-type/*') || request()->is('admin/brands') || request()->is('admin/brands/*') || request()->is('admin/models') || request()->is('admin/models/*') || request()->is('admin/models-screen-image') || request()->is('admin/models-screen-image/*') ? 'active' : '' }} notika-tab-menu-bg animated flipInX">
                            <ul class="notika-main-menu-dropdown">
                               <li> <a href="{{ route('admin.brand-type.index') }}" >Brand Types</a></li>
                                <li><a href="{{ route('admin.brands.index') }}" >Brands</a></li>
                                <li> <a href="{{ route('admin.models.index') }}" >Models</a></li>
								<li> <a href="{{ route('admin.models-screen-image.index') }}" >Upload Model Screen</a></li>
                            </ul>
                        </div>
						
						
						<div id="setting" class="tab-pane in {{ request()->is('admin/setting/credit-point') || request()->is('admin/setting/credit-point/*') || request()->is('admin/setting/commission') || request()->is('admin/setting/commission/*') || request()->is('admin/setting/commission-setting-report') || request()->is('admin/setting/commission-setting-report/*') || request()->is('admin/generalnotification/*') ? 'active' : '' }} notika-tab-menu-bg animated flipInX">
                            <ul class="notika-main-menu-dropdown">
                            <li> <a href="{{ route('admin.setting.credit-point') }}" >Credit Point</a></li>
                                <li><a href="{{ route('admin.setting.commission', '') }}" >Commission</a></li>
								<li><a href="{{ route('admin.setting.commission-setting-report','') }}" >Commission Setting Report</a></li>
                                <li><a href="{{ route('admin.generalnotification.index') }}" >Notification</a></li>
                            </ul>
                        </div>
						<div id="retailers" class="tab-pane in {{ request()->is('admin/retailers-list') || request()->is('admin/retailers-list/*') || request()->is('admin/commission-slab-tree-view') || request()->is('admin/direct-tree') || request()->is('admin/direct-tree/*') || request()->is('admin/users-monthly-recharge') || request()->is('admin/users-monthly-recharge/*') || request()->is('admin/recharge-commission') || request()->is('admin/recharge-commission/*') || request()->is('admin/users-credit-purchase') || request()->is('admin/users-credit-purchase/*') || request()->is('admin/credit-wallet-transaction') || request()->is('admin/credit-wallet-transaction/*') || request()->is('admin/cash-wallet-transaction') || request()->is('admin/cash-wallet-transaction/*') || request()->is('admin/buyer-list') || request()->is('admin/buyer-list/*') || request()->is('admin/retailer-credit-balance') || request()->is('admin/retailer-credit-balance/*') || request()->is('admin/recharge-commission-payout') || request()->is('admin/recharge-commission-payout/*') || request()->is('admin/recharge-plan/*') || request()->is('admin/bulkcut-plan/*') ? 'active' : '' }} notika-tab-menu-bg animated flipInX">
                            <ul class="notika-main-menu-dropdown">
                            <li> <a href="{{ route('admin.retailers-list') }}" >Retailers</a></li>
							<li> <a href="{{ route('admin.direct-tree') }}" >Direct Members</a></li>
							<li> <a href="{{ route('admin.tree-view-retailers') }}" >Members Tree</a></li>
                            <li> <a href="{{ route('admin.commission-slab-tree-view') }}" >Members</a></li>
							<li> <a href="{{ route('admin.users-monthly-recharge') }}" >Monthly Recharge</a></li>
                            <li> <a href="{{ route('admin.users-credit-purchase') }}" >Credit Purchase</a></li>  
							<li> <a href="{{ route('admin.recharge-commission') }}" >Recharge Commission</a></li>  
							<li> <a href="{{ route('admin.recharge-commission-payout') }}" >Recharge Commission Payout</a></li>  
							<li> <a href="{{ route('admin.credit-wallet-transaction') }}" >Credit Wallet Transaction</a></li>  
							<li> <a href="{{ route('admin.cash-wallet-transaction') }}" >Cash Wallet Transaction</a></li> 
							<li> <a href="{{ route('admin.buyer-list') }}" >Buyer List</a></li> 
							<li> <a href="{{ route('admin.retailer-credit-balance') }}" >Credit Balance</a></li> 
                            <li> <a href="{{ route('admin.recharge-plan.index') }}" >Recharge Plan</a></li> 
                           <li> <a href="{{ route('admin.bulkcut-plan.index') }}" >Bulkcut Plan</a></li> 
                            </ul>
                        </div>
						@endif
						@if(Auth::user()->hasRole('retailer'))
						 @if(Auth::user()->status == 1)	
						<div id="brands" class="tab-pane in {{ request()->is('user/brand-type') || request()->is('user/brand-type/*') || request()->is('user/brands') || request()->is('user/brands/*') || request()->is('user/models') || request()->is('user/models/*') || request()->is('user/models-screen-image') || request()->is('user/models-screen-image/*') ? 'active' : '' }} notika-tab-menu-bg animated flipInX">
                            <ul class="notika-main-menu-dropdown">
                             <li> <a href="{{ route('user.brand-type.index') }}" >Brand Types</a></li>
                                <li><a href="{{ route('user.brands.index') }}" >Brands</a></li>
                                <li> <a href="{{ route('user.models.index') }}" >Models</a></li>
								<li> <a href="{{ route('user.models-screen-image.index') }}" >Model Screen</a></li>
                            </ul>
                        </div>	
						
						<div id="manage_account" class="tab-pane in {{ request()->is('user/update-profile') || request()->is('user/update-profile/*') || request()->is('user/monthly-recharge') || request()->is('user/monthly-recharge/*') || request()->is('user/direct-tree') || request()->is('user/direct-tree/*') || request()->is('user/buyer-list') || request()->is('user/buyer-list/*') || request()->is('user/monthly-recharge-report') || request()->is('user/monthly-recharge-report/*') || request()->is('user/credit-purchase-report') || request()->is('user/credit-purchase-report/*') || request()->is('user/recharge-commission-report') || request()->is('user/recharge-commission-report/*') ? 'active' : '' }} notika-tab-menu-bg animated flipInX">
                            <ul class="notika-main-menu-dropdown">
                             <li> <a href="{{route('user.update-profile')}}" >Manage Profile</a></li>
                                <li><a href="{{ route('user.monthly-recharge') }}" >Monthly Recharge</a></li>
								<li> <a href="{{ route('user.direct-tree') }}" >My Direct Tree</a></li>
								<li> <a href="{{ route('user.buyer-list') }}">Buyer List</a></li>
								<li> <a href="{{ route('user.monthly-recharge-report') }}">Monthly Recharge Report</a></li>
								<li> <a href="{{ route('user.credit-purchase-report') }}">Credit Purchase Report</a></li>
								<li> <a href="{{ route('user.recharge-commission-report') }}">Recharge Commission Report</a></li>
								
                            </ul>
                        </div>	
						
						<div id="manage_wallet" class="tab-pane in {{ request()->is('user/credit-wallet') || request()->is('user/credit-wallet/*') || request()->is('user/cash-wallet') || request()->is('user/cash-wallet/*') || request()->is('user/purchase-credits') || request()->is('user/purchase-credits/*') ? 'active' : '' }} notika-tab-menu-bg animated flipInX">
                            <ul class="notika-main-menu-dropdown">
                               <li> <a href="{{route('user.credit-wallet')}}" >Credit Wallet</a></li>
                                <li><a href="{{ route('user.cash-wallet') }}" >Cash Wallet</a></li>
                                <li> <a href="{{ route('user.purchase-credits') }}" >Purchase Credits</a></li>
                            </ul>
                        </div>	
						 @else
						<div id="manage_account" class="tab-pane in {{ request()->is('user/update-profile') || request()->is('user/update-profile/*') || request()->is('user/monthly-recharge') || request()->is('user/monthly-recharge/*') || request()->is('user/direct-tree') || request()->is('user/direct-tree/*') || request()->is('user/buyer-list') || request()->is('user/buyer-list/*') || request()->is('user/monthly-recharge-report') || request()->is('user/monthly-recharge-report/*') || request()->is('user/credit-purchase-report') || request()->is('user/credit-purchase-report/*') || request()->is('user/recharge-commission-report') || request()->is('user/recharge-commission-report/*') ? 'active' : '' }} notika-tab-menu-bg animated flipInX">
                            <ul class="notika-main-menu-dropdown">
                             <li> <a href="{{route('user.update-profile')}}" >Manage Profile</a></li>
                                <li><a href="#" class="inactive-warning" >Monthly Recharge</a></li>
								<li> <a href="#" class="inactive-warning" >My Direct Tree</a></li>
								<li> <a href="#" class="inactive-warning">Buyer List</a></li>
								<li> <a href="#" class="inactive-warning">Monthly Recharge Report</a></li>
								<li> <a href="#" class="inactive-warning">Credit Purchase Report</a></li>
								<li> <a href="#" class="inactive-warning">Recharge Commission Report</a></li>
								
                            </ul>
                        </div>	 
						 @endif
						@endif	
                    </div>
					
					
					
                       
                    
                </div>
            </div>
        </div>
    </div>
    <!-- Main Menu area End-->
<script>
$(function(){
  $('.inactive-warning').on('click', function (e) {
   event.preventDefault();
   const url =  $(this).attr('href');
  
   swal({
       title: "Your Account is not activated!",  
text: "Please contact Admin to approve KYC and activate your account!",  
type: "warning",   
confirmButtonText: "Ok",
   });
});
});

</script>








