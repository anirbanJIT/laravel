
    <!-- Mobile Menu start -->
    <div class="mobile-menu-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="mobile-menu">
                        <nav id="dropdown">
                            <ul class="mobile-menu-nav">
                                <li><a data-toggle="collapse" data-target="#home" href="#">{{ trans('global.dashboard') }}</a>
                                </li>
                                <li><a data-toggle="collapse" data-target="#user_management" href="#">{{ trans('cruds.userManagement.title') }}</a>
                                <ul id="user_management" class="collapse dropdown-header-top">
                                <li> <a href="{{ route('admin.permissions.index') }}" >{{ trans('cruds.permission.title') }}</a></li>
                                <li><a href="{{ route('admin.roles.index') }}" >{{ trans('cruds.role.title') }}</a></li>
                                <li> <a href="{{ route('admin.users.index') }}" >{{ trans('cruds.user.title') }}</a></li>
                                </ul>
                                </li>
								 <li><a data-toggle="collapse" data-target="#mobile_brands" href="#">Brands</a>
                                <ul id="mobile_brands" class="collapse dropdown-header-top">
                                <li> <a href="{{ route('admin.brand-type.index') }}" >Brand Types</a></li>
                                <li><a href="{{ route('admin.brands.index') }}" >Brands</a></li>
                                <li> <a href="{{ route('admin.models.index') }}" >Models</a></li>
								<li> <a href="{{ route('admin.models-screen-image.index') }}" >Upload Model Screen</a></li>
                                </ul>
                                </li>
								
								 <li><a data-toggle="collapse" data-target="#mobile_brands" href="#">Setting</a>
                                <ul id="mobile_brands" class="collapse dropdown-header-top">
                                <li> <a href="{{ route('admin.setting.credit-point') }}" >Credit Point</a></li>
                                <li><a href="{{ route('admin.setting.commission') }}" >Commission</a></li>
                                
                                </ul>
                                </li>
								
								
								
                                <li><a data-toggle="collapse" data-target="#change_password" href="{{ route('auth.change_password') }}">Change password</a>
                                </li>
								 <li><a data-toggle="collapse" data-target="#logout" href="#" onclick="event.preventDefault(); document.getElementById('logoutform').submit();">{{ trans('global.logout') }}</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Mobile Menu end -->
	
	
    <!-- Main Menu area start-->
    <div class="main-menu-area mg-tb-40">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <ul class="nav nav-tabs notika-menu-wrap menu-it-icon-pro">
                        <li class="{{ request()->is('admin/home') || request()->is('admin/home/*') ? 'active' : '' }}"><a href="{{ route('admin.home') }}"><i class="notika-icon notika-house"></i> {{ trans('global.dashboard') }}</a>
                        </li>
						
                        <li class="{{ request()->is('admin/permissions') || request()->is('admin/permissions/*') || request()->is('admin/roles') || request()->is('admin/roles/*') || request()->is('admin/users') || request()->is('admin/users/*') ? 'active' : '' }}"><a data-toggle="tab" href="#user_manage"><i class="notika-icon notika-support"></i> {{ trans('cruds.userManagement.title') }}</a>
                        </li>
						<li class="{{ request()->is('admin/brand-type') || request()->is('admin/brand-type/*') || request()->is('admin/brands') || request()->is('admin/brands/*') || request()->is('admin/models') || request()->is('admin/models/*') || request()->is('admin/models-screen-image') || request()->is('admin/models-screen-image/*') ? 'active' : '' }}"><a data-toggle="tab" href="#brands"><i class="fa fa-list"></i> Brands</a>
                        </li>
						<li class="{{ request()->is('admin/setting/credit-point') || request()->is('admin/setting/credit-point/*') || request()->is('admin/setting/commission') || request()->is('admin/setting/commission/*') ? 'active' : '' }}"><a data-toggle="tab" href="#setting"><i class="notika-icon notika-app"></i> Setting</a>
                        </li>
                        <li class="{{ request()->is('change_password') || request()->is('change_password/*') ? 'active' : '' }}"><a href="{{ route('auth.change_password') }}"><i class="notika-icon notika-edit"></i> Change password</a>
                        </li>
                        <li><a data-toggle="tab" href="#" onclick="event.preventDefault(); document.getElementById('logoutform').submit();"><i class="notika-icon notika-form"></i> {{ trans('global.logout') }}</a>
                        </li>
                       
                    </ul>
                    <div class="tab-content custom-menu-content">
                       
					   <div id="user_manage" class="tab-pane in {{ request()->is('admin/permissions') || request()->is('admin/permissions/*') || request()->is('admin/roles') || request()->is('admin/roles/*') || request()->is('admin/users') || request()->is('admin/users/*') ? 'active' : '' }} notika-tab-menu-bg animated flipInX">
                            <ul class="notika-main-menu-dropdown">
                             <li> <a href="{{ route('admin.permissions.index') }}" >{{ trans('cruds.permission.title') }}</a></li>
                             <li><a href="{{ route('admin.roles.index') }}" >{{ trans('cruds.role.title') }}</a></li>
                             <li> <a href="{{ route('admin.users.index') }}" >{{ trans('cruds.user.title') }}</a></li>
                            </ul>
                        </div>

						 <div id="brands" class="tab-pane in {{ request()->is('admin/brand-type') || request()->is('admin/brand-type/*') || request()->is('admin/brands') || request()->is('admin/brands/*') || request()->is('admin/models') || request()->is('admin/models/*') || request()->is('admin/models-screen-image') || request()->is('admin/models-screen-image/*') ? 'active' : '' }} notika-tab-menu-bg animated flipInX">
                            <ul class="notika-main-menu-dropdown">
                             <li> <a href="{{ route('admin.brand-type.index') }}" >Brand Types</a></li>
                                <li><a href="{{ route('admin.brands.index') }}" >Brands</a></li>
                                <li> <a href="{{ route('admin.models.index') }}" >Models</a></li>
								<li> <a href="{{ route('admin.models-screen-image.index') }}" >Upload Model Screen</a></li>
                            </ul>
                        </div>
						
						
						<div id="setting" class="tab-pane in {{ request()->is('admin/setting/credit-point') || request()->is('admin/setting/credit-point/*') || request()->is('admin/setting/commission') || request()->is('admin/setting/commission/*') ? 'active' : '' }} notika-tab-menu-bg animated flipInX">
                            <ul class="notika-main-menu-dropdown">
                            <li> <a href="{{ route('admin.setting.credit-point') }}" >Credit Point</a></li>
                                <li><a href="{{ route('admin.setting.commission') }}" >Commission</a></li>
                            </ul>
                        </div>
						
						
                    </div>
					
					
					
                       
                    
                </div>
            </div>
        </div>
    </div>
    <!-- Main Menu area End-->









