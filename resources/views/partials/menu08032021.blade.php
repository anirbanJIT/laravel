@php
	$user = Auth::user()
@endphp
    <!-- Mobile Menu start -->
    <div class="mobile-menu-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="mobile-menu">
                        <nav id="dropdown">
                            <ul class="mobile-menu-nav">
                                <li>
                                	<a data-toggle="collapse" data-target="#home" href="{{ route('admin.home') }}">
                                	{{ trans('global.dashboard') }}</a>
                                </li>

                                @if( Gate::check('permission_manage') || Gate::check('role_manage') || Gate::check('users_manage') )							
                                <li>
                                	<a data-toggle="collapse" data-target="#user_management" href="#"> 
                                		{{ trans('cruds.userManagement.title') }} 
                                	</a>
	                                <ul id="user_management" class="collapse dropdown-header-top">
	                                	@can('permission_manage')
		                                <li> <a href="{{ route('admin.permissions.index') }}" >{{ trans('cruds.permission.title') }}</a></li>
		                                @endcan
		                                @can('role_manage')
		                                <li><a href="{{ route('admin.roles.index') }}" >{{ trans('cruds.role.title') }}</a></li> 
		                                @endcan
		                                @can('users_manage')
		                                <li> <a href="{{ route('admin.users.index') }}" >{{ trans('cruds.user.title') }}</a></li>
		                                @endcan		                                
	                                </ul>
                                </li>
								@endif

								@if( $user->can('brandstype_manage') || $user->can('brands_manage') || $user->can('brand_models') || $user->can('brand_sub_models') || $user->can('brand_models_screen_image') || $user->can('bulkupload_manage') )
								<li><a data-toggle="collapse" data-target="#mobile_brands" href="#">Brands</a>
	                                <ul id="mobile_brands" class="collapse dropdown-header-top">
	                                	@can('brandstype_manage')
		                                	<li> <a href="{{ route('admin.brand-type.index') }}" >Categories</a></li>
		                                @endcan
	                                	@can('brands_manage')
	                                		<li><a href="{{ route('admin.brands.index') }}" >Brands</a></li>
	                                	@endcan
	                                	@can('brand_models')
		                                	<li> <a href="{{ route('admin.models.index') }}" >Models</a></li>
		                                @endcan
	                                	@can('brand_sub_models')
		                                	<li> <a href="{{ route('admin.submodels.index') }}" >Sub Models</a></li>
		                                @endcan
										@can('brand_models_screen_image')
											<li> <a href="{{ route('admin.models-screen-image.index') }}" >Upload Model Screen</a></li>
										@endcan
										@can('bulkupload_manage')
											<li> <a href="{{ route('admin.bulkupload.create') }}" >Bulk Upload</a></li>
										@endcan
	                                </ul>
                                </li>
	                    		@endif

	                    		@if( $user->can('users_manage') ||  $user->can('retailer_manage') || $user->can('manage_members_tree') ||  $user->can('manage_monthly_recharge') ||  $user->can('manage_customer_recharge_commission') ||  $user->can('manage_recharge_commission_payout') ||  $user->can('manage_credit_wallet_transaction') ||  $user->can('buyer-manage') || $user->can('manage_credit_wallet') ||  $user->can('recharge_plan_manage') || $user->can('bulk_cut_plan_manage')  || $user->can('profile_change_request') || $user->can('imei_change_request')
	                    		)
								<li><a data-toggle="collapse" data-target="#retailer_management" href="#">Retailer Management</a>
	                                <ul id="retailer_management" class="collapse dropdown-header-top">
	                                	@can('retailer_manage')
										<li> <a href="{{ route('admin.retailers-list') }}" >Retailers</a></li>
										@endcan

										<!--  <li><a href="{{ route('admin.direct-tree') }}" >Direct Members</a></li> -->
										@can('manage_members_tree')
										<li> <a href="{{ route('admin.members-tree') }}" >Members Tree</a></li>
										@endcan

	                                    <!-- <li><a href="{{ route('admin.commission-slab-tree-view') }}" >Members</a></li> -->
										@can('manage_monthly_recharge')
										<li> <a href="{{ route('admin.users-monthly-recharge') }}" >Monthly Recharge</a></li>
										@endcan

										<!-- <li><a href="{{ route('admin.users-credit-purchase') }}" >Credit Purchase</a></li> -->
										@can('manage_recharge_commission_payout')
										<li> <a href="{{ route('admin.customer-recharge-commission') }}" >Customer Recharge Commission</a></li>
										@endcan

										@can('manage_recharge_commission_payout')
										<!-- <li> <a href="{{ route('admin.recharge-commission-payout') }}" >Customer Recharge Commission Payout</a></li>  -->
										@endcan	

										@can('manage_credit_wallet_transaction')
										<li> <a href="{{ route('admin.credit-wallet-transaction') }}" >Wallet Transaction</a></li>
										@endcan										
										<!-- <li> <a href="{{ route('admin.cash-wallet-transaction') }}" >Cash Wallet Transaction</a></li>  -->									
										@can('buyer-manage')
										<li> <a href="{{ route('admin.retailer-wise-buyer-list') }}" >Buyer List</a></li> 
										@endcan

										@can('manage_credit_wallet')
										<li> <a href="{{ route('admin.retailer-credit-balance') }}" >Balances</a></li>
	                                    @endcan

	                                    @can('plan_manage')
		                            	<li> <a href="{{ route('admin.plans.index') }}" >Plans</a></li> 
	                                    @endcan

	                                    @can('recharge_plan_manage')
		                            	<li> <a href="{{ route('admin.recharge-plan.index') }}" >Recharge Plan</a></li> 
	                                    @endcan

	                                    @can('bulk_cut_plan_manage')
											<li> <a href="{{ route('admin.bulkcut-plan.index') }}" >Bulkcut Plan</a></li> 
	                                    @endcan
	                                    @can('profile_change_request')
											<li> <a href="{{ route('admin.retailer-profile-change-request') }}" >Profile Change Requests</a></li>
	                                    @endcan
	                                    @can('imei_change_request')
											<li> <a href="{{ route('admin.imei-change-request') }}" >IMEI Change Requests</a></li>
	                                    @endcan	                                    
	                                </ul>
                                </li>	                    		
	                    		@endif

                                <li><a data-toggle="collapse" data-target="#mobile_brands" href="#">Setting</a>
	                                <ul id="mobile_brands" class="collapse dropdown-header-top">
	                                	<!-- 
		                                <li> <a href="{{ route('admin.setting.credit-point') }}" >Credit Point</a></li>
		                                <li><a href="{{ route('admin.setting.commission','') }}" >Commission</a></li>
		                                <li><a href="{{ route('admin.setting.commission-setting-report','') }}" >Commission Setting Report</a></li> -->		                                
		                                @can('banner_manage')
		                                	<li><a href="{{ route('admin.banner.index') }}" >Banner</a></li>
		                                @endcan
		                                @can('currency_manage')
		                                	<li><a href="{{ route('admin.setting.currency') }}" >Currency </a></li>
		                                @endcan
	                                	@can('notification_manage')
		                                	<li><a href="{{ route('admin.generalnotification.index') }}" >Notification</a></li>
		                                @endcan
		                                @can('complaint_feedback')	                                	
		                                	<li><a href="{{ route('admin.complaint-feedback.index') }}" >Complaint Feedback</a></li>
		                                @endcan
	                                	<li><a href="{{ route('auth.change_password') }}">Change password</a></li>
	                                </ul>
                                </li>
                                @if($user->can('video_category_manage') || $user->can('video_manage'))
                                <li><a data-toggle="collapse" data-target="#mobile_vidoes" href="#">Vidoes</a>
	                                <ul id="mobile_vidoes" class="collapse dropdown-header-top"> 
		                                @can('video_category_manage')
		                                <li><a href="{{ route('admin.video-category.index') }}" >Video Category</a></li>
		                                @endcan
		                                @can('video_manage')
		                                <li><a href="{{ route('admin.video.index') }}" >Video</a></li>
		                                @endcan
	                                </ul>
                                </li>
                                @endif
                                <li><a data-toggle="collapse" data-target="#logout" href="#" onclick="event.preventDefault(); document.getElementById('logoutform').submit();">{{ trans('global.logout') }}</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Mobile Menu end -->
	
	
    <!-- Main Menu area start-->
    <div class="main-menu-area mg-tb-40">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <ul class="nav nav-tabs notika-menu-wrap menu-it-icon-pro">

						<li class="{{ request()->is('admin/home') || request()->is('admin/home/*') ? 'active' : '' }}">
							<a href="{{ route('admin.home') }}"><i class="notika-icon notika-house"></i> {{ trans('global.dashboard') }}</a>
						</li>

                        @can(['users_manage'])
						<li class="{{ request()->is('admin/permissions') || request()->is('admin/permissions/*') || request()->is('admin/roles') || request()->is('admin/roles/*') || request()->is('admin/users') || request()->is('admin/users/*') ? 'active' : '' }}">
							<a data-toggle="tab" href="#user_manage"><i class="notika-icon notika-support"></i> {{ trans('cruds.userManagement.title') }}</a>
						</li>
						@endcan

						@if($user->can('brandstype_manage') || $user->can('brands_manage') || $user->can('brand_models') || $user->can('brand_sub_models') || $user->can('brand_models_screen_image') || $user->can('bulkupload_manage') )
						<li class="{{ request()->is('admin/brand-type') || request()->is('admin/brand-type/*') || request()->is('admin/brands') || request()->is('admin/brands/*') || request()->is('admin/models') || request()->is('admin/models/*') || request()->is('admin/submodels') || request()->is('admin/submodels/*') || request()->is('admin/models-screen-image') || request()->is('admin/models-screen-image/*') || request()->is('admin/bulkupload') || request()->is('admin/bulkupload/*') ? 'active' : '' }}">
							<a data-toggle="tab" href="#brands"><i class="fa fa-list"></i> Brands</a>
						</li>
						@endif

						<li class="{{ request()->is('admin/setting/currency') || request()->is('admin/setting/credit-point') || request()->is('admin/setting/credit-point/*') || request()->is('admin/setting/commission') || request()->is('admin/setting/commission/*') || request()->is('admin/setting/commission-setting-report') || request()->is('admin/setting/commission-setting-report/*') || request()->is('admin/banner') || request()->is('admin/banner/*') || request()->is('admin/generalnotification') || request()->is('admin/generalnotification/*') || request()->is('change_password') || request()->is('change_password/*') || request()->is('admin.complaint-feedback') ||  request()->is('admin.complaint-feedback/*') ? 'active' : '' }}">
							<a data-toggle="tab" href="#setting"><i class="notika-icon notika-app"></i> Setting</a>
						</li>

						@if( $user->can('users_manage') ||  $user->can('retailer_manage') || $user->can('manage_members_tree') ||  $user->can('manage_monthly_recharge') ||  $user->can('manage_customer_recharge_commission') ||  $user->can('manage_recharge_commission_payout') ||  $user->can('manage_credit_wallet_transaction') ||  $user->can('buyer-manage') || $user->can('manage_credit_wallet') ||  $user->can('plan_manage') ||  $user->can('recharge_plan_manage') || $user->can('bulk_cut_plan_manage') 
                    		)
						<li class="{{ request()->is('admin/retailers') || request()->is('admin/retailers/*') || request()->is('admin/commission-slab-tree-view') || request()->is('admin/direct-tree') || request()->is('admin/direct-tree/*') || request()->is('admin/users-monthly-recharge') || request()->is('admin/users-monthly-recharge/*') || request()->is('admin/recharge-commission') || request()->is('admin/recharge-commission/*') || request()->is('admin/customer-recharge-commission') || request()->is('admin/customer-recharge-commission/*') || request()->is('admin/credit-wallet-transaction') || request()->is('admin/credit-wallet-transaction/*') || request()->is('admin/cash-wallet-transaction') || request()->is('admin/cash-wallet-transaction/*') || request()->is('admin/buyer-list') || request()->is('admin/buyer-list/*') || request()->is('admin/retailer-credit-balance') || request()->is('admin/retailer-credit-balance/*') || request()->is('admin/recharge-plan/*')  || request()->is('admin/recharge-plans/*')  || request()->is('admin/retailer-profile-change-request') || request()->is('admin/imei-change-request-list') ||  request()->is('admin/member-tree') || request()->is('admin/member-tree/*') || request()->is('admin/tree-view-retailers') || request()->is('admin/tree-view-retailers/*') ? 'active' : '' }}">
								<a data-toggle="tab" href="#retailers"><i class="notika-icon notika-support"></i> Retailer Management</a>
						</li>
						@endif

                        @if($user->can('video_manage'))
							<li class="{{ request()->is('admin/video') || request()->is('admin/video/*') ||  request()->is('admin/video-category') || request()->is('admin/video-category/*') ? 'active' : '' }}">
								<a data-toggle="tab" href="#video"><i class="notika-icon notika-cloud"></i> {{ trans('global.video') }}</a>
							</li>
						@endif 
                    	<li>
                    		<a data-toggle="tab" href="#" onclick="event.preventDefault(); document.getElementById('logoutform').submit();"><i class="notika-icon notika-form"></i> {{ trans('global.logout') }}</a>
                    	</li>
                	</ul>
                    <div class="tab-content custom-menu-content">
                		@if( Gate::check('permission_manage') || Gate::check('role_manage') || Gate::check('users_manage') )
						   <div id="user_manage" class="tab-pane in {{ request()->is('admin/permissions') || request()->is('admin/permissions/*') || request()->is('admin/roles') || request()->is('admin/roles/*') || request()->is('admin/users') || request()->is('admin/users/*') ? 'active' : '' }} notika-tab-menu-bg animated flipInX">
                            <ul class="notika-main-menu-dropdown">
                            @can('permission_manage')
	                            <li> <a href="{{ route('admin.permissions.index') }}" >{{ trans('cruds.permission.title') }}</a></li>
	                        @endcan
	                        @can('role_manage')
	                            <li><a href="{{ route('admin.roles.index') }}" >{{ trans('cruds.role.title') }}</a></li> 
	                        @endcan
	                        @can('users_manage')
	                            <li> <a href="{{ route('admin.users.index') }}" >{{ trans('cruds.user.title') }}</a></li>
	                        @endcan
                            </ul>
                        </div>
	                    @endif

						@if($user->can('brandstype_manage') || $user->can('brands_manage') || $user->can('brand_models') || $user->can('brand_sub_models') || $user->can('brand_models_screen_image') || $user->can('bulkupload_manage'))
						<div id="brands" class="tab-pane in {{ request()->is('admin/brand-type') || request()->is('admin/brand-type/*') || request()->is('admin/brands') || request()->is('admin/brands/*') || request()->is('admin/models') || request()->is('admin/models/*') || request()->is('admin/submodels') || request()->is('admin/submodels/*') || request()->is('admin/models-screen-image') || request()->is('admin/models-screen-image/*') || request()->is('admin/bulkupload') || request()->is('admin/bulkupload/*') ? 'active' : '' }} notika-tab-menu-bg animated flipInX">
                            <ul class="notika-main-menu-dropdown">
                            	@can('brandstype_manage')
                                	<li> <a href="{{ route('admin.brand-type.index') }}" >Categories</a></li>
                                @endcan
                            	@can('brands_manage')
                            		<li><a href="{{ route('admin.brands.index') }}" >Brands</a></li>
                            	@endcan
                            	@can('brand_models')
                                	<li> <a href="{{ route('admin.models.index') }}" >Models</a></li>
                                @endcan
                            	@can('brand_sub_models')
                                	<li> <a href="{{ route('admin.submodels.index') }}" >Sub Models</a></li>
                                @endcan
								@can('brand_models_screen_image')
									<li> <a href="{{ route('admin.models-screen-image.index') }}" >Upload Model Screen</a></li>
								@endcan
								@can('bulkupload_manage')
									<li> <a href="{{ route('admin.bulkupload.create') }}" >Bulk Upload</a></li>
								@endcan
                            </ul>
                        </div>
	                    @endif

	                    <div id="setting" class="tab-pane in {{  request()->is('admin/setting/currency') ||  request()->is('admin/setting/credit-point') || request()->is('admin/setting/credit-point/*') ||  request()->is('admin/setting/commission') || request()->is('admin/setting/commission/*') ||  request()->is('admin/setting/commission-setting-report') ||  request()->is('admin/setting/commission-setting-report/*') ||  request()->is('admin/generalnotification') ||  request()->is('admin/generalnotification/*') || request()->is('auth.change_password') ||  request()->is('auth.change_password/*') ||  request()->is('admin/complaint-feedback') ||  request()->is('admin/complaint-feedback/*') ||  request()->is('admin/banner') || request()->is('admin/banner/*') || request()->is('change_password') ? 'active' : '' }} notika-tab-menu-bg animated flipInX">
                            <ul class="notika-main-menu-dropdown">
                                @can('banner_manage')
                                	<li><a href="{{ route('admin.banner.index') }}" >Banner</a></li>
                                @endcan
                                @can('currency_manage')
                                	<li><a href="{{ route('admin.setting.currency') }}" >Currency </a></li>
                                @endcan
                                @can('notification_manage')
                                	<li><a href="{{ route('admin.generalnotification.index') }}" >Notification</a></li>
                                @endcan
                                @can('complaint_feedback')
                                	<li><a href="{{ route('admin.complaint-feedback.index') }}" >Complaint Feedback</a></li>
                                @endcan
                                	<li><a href="{{ route('auth.change_password') }}">Change Password</a></li>
                            </ul>
                        </div>

	                    @if( $user->can('users_manage') ||  $user->can('retailer_manage') || $user->can('manage_members_tree') ||  $user->can('manage_monthly_recharge') ||  $user->can('manage_customer_recharge_commission') ||  $user->can('manage_recharge_commission_payout') ||  $user->can('manage_credit_wallet_transaction') ||  $user->can('buyer-manage') || $user->can('manage_credit_wallet') ||  $user->can('plan_manage') ||  $user->can('recharge_plan_manage') || $user->can('bulk_cut_plan_manage') || $user->can('profile_change_request') || $user->can('imei_change_request')
	                    		)						
							<div id="retailers" class="tab-pane in 
							{{ request()->is('admin/retailers-list') || request()->is('admin/retailers-list/*') || 
							request()->is('admin/commission-slab-tree-view') || request()->is('admin/direct-tree') ||
							request()->is('admin/direct-tree/*') || request()->is('admin/users-monthly-recharge') || 
							request()->is('admin/users-monthly-recharge/*') || request()->is('admin/recharge-commission') || 
							request()->is('admin/recharge-commission/*') || request()->is('admin/users-credit-purchase') || request()->is('admin/users-credit-purchase/*') || request()->is('admin/credit-wallet-transaction') || request()->is('admin/credit-wallet-transaction/*') || request()->is('admin/cash-wallet-transaction') || request()->is('admin/cash-wallet-transaction/*') || request()->is('admin/buyer-list') || 
							request()->is('admin/buyer-list/*') || request()->is('admin/retailer-credit-balance') || 
							request()->is('admin/retailer-credit-balance/*') || request()->is('admin/customer-recharge-commission') || request()->is('admin/customer-recharge-commission/*') || request()->is('admin/recharge-commission-payout') || request()->is('admin/recharge-commission-payout/*') || request()->is('admin/recharge-plan') || request()->is('admin/recharge-plan/*') || request()->is('admin/plans') || request()->is('admin/recharge-plans/*') || request()->is('admin/bulkcut-plan') || request()->is('admin/bulkcut-plan/*') || request()->is('admin/retailer-profile-change-request') || request()->is('admin/imei-change-request-list') ||  request()->is('admin/retailers') || request()->is('admin/retailers/*') || request()->is('admin/members-tree') || request()->is('admin/members-tree/*') || request()->is('admin/member-tree') || request()->is('admin/member-tree/*') || request()->is('admin/tree-view-retailers') || request()->is('admin/tree-view-retailers/*') || request()->is('admin/retailer-wise-buyer-list') || request()->is('admin/retailer-wise-buyer-list/*')  ? 'active' : '' }} notika-tab-menu-bg animated flipInX">
	                        <ul class="notika-main-menu-dropdown">
	                            @can('retailer_manage')
									<li> <a href="{{ route('admin.retailers-list') }}" >Retailers</a></li>
								@endcan
								<!-- <li> <a href="{{ route('admin.direct-tree') }}" >Direct Members</a></li> -->
								@can('manage_members_tree')
									<li> <a href="{{ route('admin.members-tree') }}" >Members Tree</a></li>
								@endcan
	                            <!-- <li> <a href="{{ route('admin.commission-slab-tree-view') }}" >Members</a></li> -->
								@can('manage_monthly_recharge')
									<li> <a href="{{ route('admin.users-monthly-recharge') }}" >Monthly Recharge</a></li>
								@endcan
								<!-- <li> <a href="{{ route('admin.users-credit-purchase') }}" >Credit Purchase</a></li> -->  
								<!-- <li> <a href="{{ route('admin.recharge-commission') }}" >Recharge Commission</a></li>   -->
								@can('manage_recharge_commission_payout')
								<li> <a href="{{ route('admin.customer-recharge-commission') }}" >Customer Recharge Commission & Payout</a></li>
								@endcan
								<!-- <li> <a href="{{ route('admin.recharge-commission-payout') }}" >Recharge Commission Payout</a></li> -->  
								<!-- @can('manage_recharge_commission_payout')
									<li> <a href="{{ route('admin.recharge-commission-payout') }}" >Customer Recharge Commission Payout</a></li> 
								@endcan	 -->
								@can('manage_credit_wallet_transaction')
									<li> <a href="{{ route('admin.credit-wallet-transaction') }}" >Wallet Transaction</a></li>
								@endcan	
								<!-- <li> <a href="{{ route('admin.cash-wallet-transaction') }}" >Cash Wallet Transaction</a></li>  -->
								@can('buyer-manage')
								<li> <a href="{{ route('admin.retailer-wise-buyer-list') }}" >Buyer List</a></li>
								@endcan
								@can('manage_credit_wallet')
								<li> <a href="{{ route('admin.retailer-credit-balance') }}" >Balances</a></li>
                                @endcan
                                @can('plan_manage')
                            	<li> <a href="{{ route('admin.plans.index') }}" >Plans</a></li> 
                                @endcan
                                @can('recharge_plan_manage')
                            	<li> <a href="{{ route('admin.recharge-plan.index') }}" >Recharge Plan</a></li> 
                                @endcan
                                @can('bulk_cut_plan_manage')
									<li> <a href="{{ route('admin.bulkcut-plan.index') }}" >Bulkcut Plan</a></li> 
                                @endcan                                
                                @can('profile_change_request')
									<li> <a href="{{ route('admin.retailer-profile-change-request') }}" >Profile Change Requests</a></li>
                                @endcan
                                @can('imei_change_request')
									<li> <a href="{{ route('admin.imei-change-request') }}" >IMEI Change Requests</a></li>
                                @endcan	  
	                       </ul>
	                    </div>
	                    @endif

	                    @if($user->can('video_category_manage') || $user->can('video_manage'))
						<div id="video" class="tab-pane in {{ request()->is('admin/video') ||  request()->is('admin/video/*') || 
							request()->is('admin/video-category') ||  request()->is('admin/video-category/*')  ? 'active' : '' }} notika-tab-menu-bg animated flipInX">
                            <ul class="notika-main-menu-dropdown">
                            	@can('video_category_manage')
                                <li><a href="{{ route('admin.video-category.index') }}" >Video Category</a></li>
                                @endcan
                                @can('video_manage')
                                <li><a href="{{ route('admin.video.index') }}" >Video</a></li>
                                @endcan                                
                            </ul>
                        </div>
	                    @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- Main Menu area End-->
<script>
$(function(){
  	$('.inactive-warning').on('click', function (e) {
	   	event.preventDefault();
	   	const url =  $(this).attr('href');  
	   	swal({
			title: "Your Account is not activated!",  
			text: "Please contact Admin to approve KYC and activate your account!",  
			type: "warning",   
			confirmButtonText: "Ok",
	   	});
	});
});
</script>