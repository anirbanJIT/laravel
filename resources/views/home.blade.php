<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
      <meta name="description" content="POS - Bootstrap Admin Template">
      <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
      <meta name="author" content="Dreamguys - Bootstrap Admin Template">
      <meta name="robots" content="noindex, nofollow">
      <title>Modern Mart :: Dashboard</title>
      <link rel="shortcut icon" type="image/x-icon" href="{{ asset('public/Admin/img/favicon.png') }}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/bootstrap.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/animate.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/dataTables.bootstrap4.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/fontawesome.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/all.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/style.css')}}">
   </head>
   <body>
      <div id="global-loader">
         <div class="whirly-loader"> </div>
      </div>
  <div class="main-wrapper">
     
     @extends('layouts.header')
     
     @extends('layouts.sidebar')

     @php $retailer = getCountRetailer() @endphp

     <div class="page-wrapper">
        <div class="content">
           <div class="row">
				<div class="col-lg-3 col-sm-6 col-12">
					<div class="dash-widget">
						<div class="dash-widgetimg">
						   <span><img src="{{ asset('public/Admin/img/icons/dash1.svg') }}" alt="img"></span>
						</div>
						<div class="dash-widgetcontent">
						   <h5><span class="counters" data-count="{{$totalUser}}">{{$totalUser}}</span></h5>
						   <h6>Total User</h6>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-sm-6 col-12">
					<div class="dash-widget">
						<div class="dash-widgetimg">
						   <span><img src="{{ asset('public/Admin/img/icons/dash1.svg') }}" alt="img"></span>
						</div>
						<div class="dash-widgetcontent">
						   <h5><span class="counters" data-count="{{$activeUser}}">{{$activeUser}}</span></h5>
						   <h6>Active User</h6>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-sm-6 col-12">
					<div class="dash-widget">
						<div class="dash-widgetimg">
						   <span><img src="{{ asset('public/Admin/img/icons/dash1.svg') }}" alt="img"></span>
						</div>
						<div class="dash-widgetcontent">
						   <h5><span class="counters" data-count="{{$inactiveUser}}">{{$inactiveUser}}</span></h5>
						   <h6>Inactive User</h6>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-sm-6 col-12">
					<div class="dash-widget">
						<div class="dash-widgetimg">
						   <span><img src="{{ asset('public/Admin/img/icons/dash1.svg') }}" alt="img"></span>
						</div>
						<div class="dash-widgetcontent">
						   <h5><span class="counters" data-count="{{$retailer}}">{{$retailer}}</span></h5>
						   <h6>Number Of Retailers</h6>
						</div>
					</div>
				</div>
					<div class="col-lg-3 col-sm-6 col-12">
					<div class="dash-widget">
						<div class="dash-widgetimg">
						   <span><img src="{{ asset('public/Admin/img/icons/dash1.svg') }}" alt="img"></span>
						</div>
						<div class="dash-widgetcontent">
						   <h5><span class="counters" data-count="{{$kyc_user}}">{{$kyc_user}}</span></h5>
						   <h6>KYC Pending Users</h6>
						</div>
					</div>
				</div>

					<div class="col-lg-3 col-sm-6 col-12">
					<div class="dash-widget">
						<div class="dash-widgetimg">
						   <span><img src="{{ asset('public/Admin/img/icons/dash1.svg') }}" alt="img"></span>
						</div>
						<div class="dash-widgetcontent">
						   <h5><span class="counters" data-count="{{$complaint_feedback_total}}">{{$complaint_feedback_total}}</span></h5>
						   <h6>Total Complaints and Feedback</h6>
						</div>
					</div>
				</div>

					<div class="col-lg-3 col-sm-6 col-12">
					<div class="dash-widget">
						<div class="dash-widgetimg">
						   <span><img src="{{ asset('public/Admin/img/icons/dash1.svg') }}" alt="img"></span>
						</div>
						<div class="dash-widgetcontent">
						   <h5><span class="counters" data-count="{{$complaint_total}}">{{$complaint_total}}</span></h5>
						   <h6>Total Complaints</h6>
						</div>
					</div>
				</div>

					<div class="col-lg-3 col-sm-6 col-12">
					<div class="dash-widget">
						<div class="dash-widgetimg">
						   <span><img src="{{ asset('public/Admin/img/icons/dash1.svg') }}" alt="img"></span>
						</div>
						<div class="dash-widgetcontent">
						   <h5><span class="counters" data-count="{{$complaint_feedback_solved_total}}">{{$complaint_feedback_solved_total}}</span></h5>
						   <h6>Total Resolved Complaints</h6>
						</div>
					</div>
				</div>

					<div class="col-lg-3 col-sm-6 col-12">
					<div class="dash-widget">
						<div class="dash-widgetimg">
						   <span><img src="{{ asset('public/Admin/img/icons/dash1.svg') }}" alt="img"></span>
						</div>
						<div class="dash-widgetcontent">
						   <h5><span class="counters" data-count="{{$complaint_feedback_unsolved_total}}">{{$complaint_feedback_unsolved_total}}</span></h5>
						   <h6>Total Unresolved Complaints</h6>
						</div>
					</div>
				</div>

				<div class="col-lg-3 col-sm-6 col-12">
					<div class="dash-widget">
						<div class="dash-widgetimg">
						   <span><img src="{{ asset('public/Admin/img/icons/dash1.svg') }}" alt="img"></span>
						</div>
						<div class="dash-widgetcontent">
						   <h5><span class="counters" data-count="{{$feedback_total}}">{{$feedback_total}}</span></h5>
						   <h6>Total Feedbacks</h6>
						</div>
					</div>
				</div>

				<div class="col-lg-3 col-sm-6 col-12">
					<a href="{{route('admin.like-list','like')}}">
					<div class="dash-widget">
						<div class="dash-widgetimg">
						   <span><img src="{{ asset('public/Admin/img/icons/dash1.svg') }}" alt="img"></span>
						</div>
						<div class="dash-widgetcontent">
						   	<h5><span class="counters" data-count="{{$like_total}}">{{$like_total}}</span></h5>
						   <h6>Total Like</h6>
						</div>
					</div>
					</a>
				</div>

				<div class="col-lg-3 col-sm-6 col-12">
					<a href="{{route('admin.like-list','unlike')}}">
					<div class="dash-widget">
						<div class="dash-widgetimg">
						   <span><img src="{{ asset('public/Admin/img/icons/dash1.svg') }}" alt="img"></span>
						</div>
						<div class="dash-widgetcontent">
						   	<h5><span class="counters" data-count="{{$unlike_total}}">{{$unlike_total}}</span></h5>
						   <h6>Total Dislike</h6>
						</div>
					</div>
				</a>
				</div>

				

				<div class="col-lg-3 col-sm-6 col-12">
					<div class="dash-widget dash1">
						<div class="dash-widgetimg">
						   <span><img src="{{ asset('public/Admin/img/icons/dash2.svg') }}" alt="img"></span>
						</div>
						<div class="dash-widgetcontent">
						   <h5>₹<span class="counters" data-count="{{ $totalCreditPoints}}">{{ $totalCreditPoints}}</span></h5>
						   <h6>Total Wallet Balance</h6>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-sm-6 col-12">
					<div class="dash-widget dash2">
						<div class="dash-widgetimg">
						   <span><img src="{{ asset('public/Admin/img/icons/dash3.svg') }}" alt="img"></span>
						</div>
						<div class="dash-widgetcontent">
						   <h5>₹ <span class="counters" data-count="{{ $totalRechargeAmount }}">{{ $totalRechargeAmount }}</span></h5>
						   <h6>Total Recharge Amount</h6>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-sm-6 col-12">
					<div class="dash-widget dash3">
						<div class="dash-widgetimg">
						   <span><img src="{{ asset('public/Admin/img/icons/dash4.svg') }}" alt="img"></span>
						</div>
						<div class="dash-widgetcontent">
						   <h5>₹ <span class="counters" data-count="{{ $totalRechargeAmountMonth }}">{{ $totalRechargeAmountMonth }}</span></h5>
						   <h6>Current Month Recharge Amount</h6>
						</div>
					</div>
              </div>
              <!-- <div class="col-lg-3 col-sm-6 col-12 d-flex">
                 <div class="dash-count">
                    <div class="dash-counts">
                       <h4>100</h4>
                       <h5>Customers</h5>
                    </div>
                    <div class="dash-imgs">
                       <i data-feather="user"></i>
                    </div>
                 </div>
              </div>
              <div class="col-lg-3 col-sm-6 col-12 d-flex">
                 <div class="dash-count das1">
                    <div class="dash-counts">
                       <h4>100</h4>
                       <h5>Suppliers</h5>
                    </div>
                    <div class="dash-imgs">
                       <i data-feather="user-check"></i>
                    </div>
                 </div>
              </div>
              <div class="col-lg-3 col-sm-6 col-12 d-flex">
                 <div class="dash-count das2">
                    <div class="dash-counts">
                       <h4>100</h4>
                       <h5>Purchase Invoice</h5>
                    </div>
                    <div class="dash-imgs">
                       <i data-feather="file-text"></i>
                    </div>
                 </div>
              </div>
              <div class="col-lg-3 col-sm-6 col-12 d-flex">
                 <div class="dash-count das3">
                    <div class="dash-counts">
                       <h4>105</h4>
                       <h5>Sales Invoice</h5>
                    </div>
                    <div class="dash-imgs">
                       <i data-feather="file"></i>
                    </div>
                 </div>
              </div> -->
			</div>
			<!--<div class="row">
				<div class="col-lg-12 col-sm-12 col-12 d-flex">
					<div class="card flex-fill">
						<div class="card-header pb-0 d-flex justify-content-between align-items-center">
							<h5 class="card-title mb-0">Recharge Statistics</h5> -->
							  <!--  <div class="graph-sets">
								  <ul>
									 <li>
										<span>Sales</span>
									 </li>
									 <li>
										<span>Purchase</span>
									 </li>
								  </ul>
								  <div class="dropdown">
									 <button class="btn btn-white btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-expanded="false">
									 2022 <img src="{{ asset('public/Admin/img/icons/dropdown.svg') }}" alt="img" class="ms-2">
									 </button>
									 <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">
										<li>
										   <a href="javascript:void(0);" class="dropdown-item">2022</a>
										</li>
										<li>
										   <a href="javascript:void(0);" class="dropdown-item">2021</a>
										</li>
										<li>
										   <a href="javascript:void(0);" class="dropdown-item">2020</a>
										</li>
									 </ul>
								  </div>
							   </div> -->
						<!-- </div>
						<div class="card-body">
							<div id="sales_charts"></div>
						</div>
					</div>
				</div>
			</div>-->
			<div class="card mb-0">
				<div class="card-body">
					<h4 class="card-title">Recent Recharges</h4>
					<div class="table-responsive dataview">
						<table class="table datatable ">
						    <thead>
							  <tr>
								 <th>SNo</th>
								 <th>Name</th>
								 <th>Email</th>
								 <th>Recharge Amount</th>
							  </tr>
						    </thead>
						    <tbody>
							@php $i = 1 @endphp
							   @foreach($recentRecharges as $recentRecharge)
							  <tr>
								 <td>{{ $i }}</td>
								 <td>{{ $recentRecharge->retailerName }}</td>
								 <td>{{ $recentRecharge->retailerEmail }}</td>
								 <td>₹ {{ $recentRecharge->recharge_amount }}</td>
							  </tr>
							  @php $i++ @endphp
							  @endforeach
						    </tbody>
						</table>
					</div>
				</div>
			</div>
        </div>
     </div>
  </div>
 
<script src="{{ asset('public/Admin/js/jquery-3.6.0.min.js')}}"></script>
<script src="{{ asset('public/Admin/js/feather.min.js')}}"></script>
<script src="{{ asset('public/Admin/js/jquery.slimscroll.min.js')}}"></script>
<script src="{{ asset('public/Admin/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('public/Admin/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{ asset('public/Admin/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{ asset('public/Admin/plugins/apexchart/apexcharts.min.js')}}"></script>
<script src="{{ asset('public/Admin/plugins/apexchart/chart-data.js')}}"></script>
<script src="{{ asset('public/Admin/js/script.js')}}"></script>
   </body>
</html>