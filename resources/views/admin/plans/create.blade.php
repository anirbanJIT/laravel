<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
      <meta name="description" content="POS - Bootstrap Admin Template">
      <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
      <meta name="author" content="Dreamguys - Bootstrap Admin Template">
      <meta name="robots" content="noindex, nofollow">
      <title>{{ trans('global.create') }} {{ trans('cruds.plan.title_singular') }}</title>
      
      <link rel="shortcut icon" type="image/x-icon" href="{{ asset('public/Admin/img/favicon.png')}}">
      
      <link rel="stylesheet" href="{{ asset('public/Admin/css/bootstrap.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/select2/css/select2.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/animate.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/dataTables.bootstrap4.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/fontawesome.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/all.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/style.css')}}">
      
   </head>
   <body>
      <div id="global-loader">
         <div class="whirly-loader"> </div>
      </div>
      <div class="main-wrapper">
        @extends('layouts.header')
     
        @extends('layouts.sidebar')
        
        <div class="page-wrapper">
            <div class="content container-fluid">
               <div class="page-header">
                  <div class="row">
                     <div class="col">
                        <h3 class="page-title">{{ trans('global.create') }} {{ trans('cruds.plan.title_singular') }}</h3>
                        <ul class="breadcrumb">
                           <li class="breadcrumb-item"><a href="{{route('admin.home')}}">{{ trans('global.dashboard') }}</a></li>
                           <li class="breadcrumb-item"><a href="{{ route('admin.plans.index') }}">{{ trans('cruds.plan.title') }}</a></li>
                           <li class="breadcrumb-item active">{{ trans('global.create') }} {{ trans('cruds.plan.title_singular') }}</li>
                        </ul>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-12">
                     <div class="card">
                        <!-- <div class="card-header row">
                            <div class="col-lg-6"><h4 class="card-title">{{ trans('global.create') }} {{ trans('cruds.permission.title_singular') }}</h4></div>
                            
                        </div> -->
                        <div class="card-body">
                           <form action="{{ route('admin.plans.store') }}" method="POST" id="frm_plan">
                            {{ csrf_field() }}
                           <div class="row">   
                              <div class="form-group col-lg-6">
                                 <label>{{ trans('cruds.plan.fields.name') }}*</label>
                                <input type="text" id="name" name="name" value="{{ old('name') }}" required class="form-control" placeholder="{{ trans('cruds.plan.fields.name') }}*" >
                                 @if($errors->has('name')) <em class="invalid-feedback"> {{ $errors->first('name') }} </em> @endif
                              </div>

                              <div class="form-group col-lg-6">
                                 <label>{{ trans('cruds.plan.fields.create_plan_to') }} {{ trans('cruds.plan.fields.discount_value') }} *</label>
                                 <input type="hidden" name="commission_plan[0][commission_plan_id_to]" value="{{ old('commission_plan.0.commission_plan_id_to') }}">
                                 
                                 <input type="text" id="discount_value" name="commission_plan[0][discount_value]" value="{{ old('commission_plan.0.discount_value') }}" class="form-control" placeholder="{{ trans('cruds.plan.fields.create_plan_to') }} {{ trans('cruds.plan.fields.discount_value') }}*" onkeyup="this.value = this.value.replace(/[^0-9\.]/g, '');" data-mask="99.99" required>
                                 
                                 @if ($errors->has('commission_plan.0.discount_value')) <em class="invalid-feedback"> {{ $errors->first('commission_plan.0.discount_value') }} </em> @endif
                              </div>

                              <div class="form-group col-lg-6">
                                 <label>{{ trans('cruds.plan.fields.discount_type') }}*</label>
                                 <select class="form-control" id="discount_type" name="commission_plan[0][discount_type]" required>
                                   <option value="" selected>{{ trans('cruds.plan.fields.discount_type') }}*</option>
                                   <option value="0" {{ old('commission_plan.0.discount_type') == '0' ? 'selected' : '' }} >Percentage</option>
                                   <option value="1" {{ old('commission_plan.0.discount_type') == '1' ? 'selected' : '' }} >Fixed</option>
                               </select>
                                 @if($errors->has('commission_plan.0.discount_type')) <em class="invalid-feedback"> {{ $errors->first('commission_plan.0.discount_type') }} </em> @endif
                              </div>

                              <div class="form-group col-lg-6">
                                 <label>{{ trans('cruds.plan.fields.categories') }}*</label>
                                 <select class="form-control" name="categories[]" id="categories" multiple required>
                                       @foreach($brandTypes as $id => $brandType)
                                       <option value="{{ $brandType->id }}">{{ $brandType->name }} </option>
                                       @endforeach
                                   </select>
                                 @if($errors->has('commission_plan.0.categories')) <em class="invalid-feedback"> {{ $errors->first('commission_plan.0.categories') }} </em> @endif
                              </div>

                              @foreach($plans as $key => $plan)
                              @php
                              $i = $key +1;
                              @endphp

                              <div class="form-group col-lg-6">
                                 <label>To {{ isset($plan->name) ? $plan->name : '' }} {{ trans('cruds.plan.fields.discount_value') }} *</label>
                                 <input type="hidden" name="commission_plan[{{$i}}][commission_plan_id_to]" value="{{ isset($plan->id) ? $plan->id : '' }}">

                                        <input type="text" id="discount_value" name="commission_plan[{{$i}}][discount_value]" value="{{ old('commission_plan'.$i.'discount_value') }}" class="form-control" placeholder="{{ trans('cruds.plan.fields.create_plan_to') }} {{ trans('cruds.plan.fields.discount_value') }}*" onkeyup="this.value = this.value.replace(/[^0-9\.]/g, '');" data-mask="99.99">
                                 
                                 @if ($errors->has('commission_plan'.$i.'discount_value')) <em class="invalid-feedback"> {{ $errors->first('commission_plan.$i.discount_value') }} </em> @endif
                              </div>

                              <div class="form-group col-lg-6">
                                 <label>{{ trans('cruds.plan.fields.discount_type') }}*</label>
                                 <select class="form-control" id="discount_type" name="commission_plan[{{$i}}][discount_type]">
                                      <option value="" selected>{{ trans('cruds.plan.fields.discount_type') }}*</option>
                                      <option value="0" {{ old('commission_plan'.$i.'discount_type') == '0' ? 'selected' : '' }} >Percentage</option>
                                      <option value="1" {{ old('commission_plan'.$i.'discount_type') == '1' ? 'selected' : '' }} >Fixed</option>
                                  </select>
                                 @if ($errors->has('commission_plan'.$i.'discount_type'))
                                    <span class="text-danger">{{ $errors->first('commission_plan'.$i.'discount_type') }}</span>
                                    @endif
                              </div>
                               @endforeach

                              @foreach($plans as $key => $plan)
                              @php
                              $i = $i +1;
                              @endphp
                              <div class="form-group col-lg-6">
                                 <label>{{ isset($plan->name) ? $plan->name : '' }} To Create {{ trans('cruds.plan.fields.discount_value') }} *</label>
                                 <input type="hidden" name="commission_plan[{{$i}}][commission_plan_id_from]" value="{{ isset($plan->id) ? $plan->id : '' }}">

                                        <input type="text" id="discount_value" name="commission_plan[{{$i}}][discount_value]" value="{{ old('commission_plan'.$i.'discount_value') }}" class="form-control" placeholder="{{ trans('cruds.plan.fields.create_plan_to') }} {{ trans('cruds.plan.fields.discount_value') }}*" onkeyup="this.value = this.value.replace(/[^0-9\.]/g, '');" data-mask="99.99">
                                 
                                 @if ($errors->has('commission_plan'.$i.'discount_value'))
                                    <span class="text-danger">{{ $errors->first('commission_plan.$i.discount_value') }}</span>
                                    @endif
                              </div>

                              <div class="form-group col-lg-6">
                                  <label>{{ trans('cruds.plan.fields.discount_type') }}*</label>
                                 <select class="form-control" id="discount_type" name="commission_plan[{{$i}}][discount_type]">
                                      <option value="" selected>{{ trans('cruds.plan.fields.discount_type') }}*</option>
                                      <option value="0" {{ old('commission_plan'.$i.'discount_type') == '0' ? 'selected' : '' }} >Percentage</option>
                                      <option value="1" {{ old('commission_plan'.$i.'discount_type') == '1' ? 'selected' : '' }} >Fixed</option>
                                  </select>  
                                 @if ($errors->has('commission_plan'.$i.'discount_type'))
                                    <span class="text-danger">{{ $errors->first('commission_plan'.$i.'discount_type') }}</span>
                                    @endif
                              </div>
                              @endforeach 
                              <div class="">
                                 <button type="submit" class="btn btn-success">Save</button>
                              </div>
                           </div>
                           </form>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
    <script src="{{ asset('public/Admin/js/jquery-3.6.0.min.js')}}"></script>
    <script src="{{ asset('public/Admin/plugins/select2/js/select2.min.js')}}"></script>
    <script src="{{ asset('public/Admin/plugins/select2/js/custom-select.js')}}"></script>
    <script src="{{ asset('public/Admin/js/feather.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.slimscroll.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/script.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js">
    </script>
    <script>
        $(function () {
            $("#frm_plan").validate({
                rules: {
                    name: {
                        required: true,
                    },
                    "commission_plan[]['discount_value']": {
                        required: true,
                    },
                    "commission_plan[]['discount_type']": {
                        required: true,
                    }
                },
                messages: {
                    name: {
                        required: "Please enter name"
                    },
                    "commission_plan[]['discount_value']": {
                        required: "Please enter commission",
                    },
                    "commission_plan[]['discount_type']": {
                        required: "Please select discount type",
                    }
                },
            });
        });
        $(document).ready(function() {
          $('#categories').select2();
      });
  </script>
   </body>
</html>