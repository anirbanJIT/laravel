<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
      <meta name="description" content="POS - Bootstrap Admin Template">
      <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
      <meta name="author" content="Dreamguys - Bootstrap Admin Template">
      <meta name="robots" content="noindex, nofollow">
      <title>Plan List</title>
      
      <link rel="shortcut icon" type="image/x-icon" href="{{ asset('public/Admin/img/favicon.png')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/bootstrap.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/animate.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/dataTables.bootstrap4.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/fontawesome.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/all.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/style.css')}}">
   </head>
   <body>
      <div id="global-loader">
         <div class="whirly-loader"> </div>
      </div>
      <div class="main-wrapper">
        @extends('layouts.header')
     
        @extends('layouts.sidebar')
        
        <div class="page-wrapper">
            <div class="content container-fluid">
               <div class="page-header">
                  <div class="row">
                     <div class="col">
                        <h3 class="page-title">Plan List</h3>
                        <ul class="breadcrumb">
                           <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">{{ trans('global.dashboard') }}</a></li>
                           <li class="breadcrumb-item active">Plan List</li>
                        </ul>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-12">
                     <div class="card">
                        <div class="card-header row">
                            <div class="col-lg-6"><h4 class="card-title">Plan List</h4></div>
                            <div class="col-lg-6"><a href="{{ route('admin.plans.create') }}" class="btn btn-success" style="float: right;">+ Add Plan</a></div>
                        </div>
                        <div class="card-body">
                           <div class="table-responsive">
                              <table class="table  datanew ">
                                 <thead>
                                    <tr>
                                       <th>{{ trans('cruds.plan.fields.name') }}</th>
                                       <th width="60">{{ trans('cruds.plan.fields.status') }}</th>
                                       <th width="150">&nbsp;</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    @foreach($plans as $key => $plan)
                                <tr data-entry-id="{{ $plan->id }}">
                                    <td>{{ $plan->name ?? '' }}</td>                         
                                    <td>
                                        <div class="status-toggle d-flex justify-content-between align-items-center">
                                       <input type="checkbox" class="check status_change" data-id="{{$plan->id}}" id="ts{{$plan->id}}" data-on="Active" data-off="InActive" {{ $plan->plan_status ? 'checked' : '' }}>
                                       <label for="ts{{$plan->id}}" class="checktoggle">checkbox</label>
                                       </div>
                                    </td>
                                    <td>
                                        <a  href="{{ route('admin.plans.show', $plan->id) }}" class="btn btn-primary text-white sa-warning" title="View"><i class="fa fa-eye"></i></a>

                                        <a  href="{{ route('admin.plans.edit', $plan->id) }}" class="btn btn-success text-white" title="Edit"><i class="fa fa-edit"></i></a>

                                        <a  href="{{ url('admin/plan/delete', $plan->id) }}" class="btn btn-danger text-white sa-warning" title="Remove" onclick="return confirm('{{ trans('global.areYouSure') }}');"><i class="fa fa-trash"></i></a>

                                    </td>
                                </tr>
                                @endforeach

                                    
                                    
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
    <script src="{{ asset('public/Admin/js/jquery-3.6.0.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/feather.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.slimscroll.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{ asset('public/Admin/plugins/apexchart/apexcharts.min.js')}}"></script>
    <script src="{{ asset('public/Admin/plugins/apexchart/chart-data.js')}}"></script>
    <script src="{{ asset('public/Admin/js/script.js')}}"></script>
    <script type="text/javascript">
        $('.status_change').change(function () {

            var status = $(this).prop('checked') == true ? 1 : 0;
            var model_id = $(this).data('id');
            $.ajax({
                type: "GET",
                url: "{{url('admin/changeStatusPlan')}}?status=" + status + "&plan_id=" + model_id,
                success: function (res) {
                    if (res) {
                        //alert(res);
                        console.log(res.success);

                    } else {
                        //alert("nnnn");
                    }
                }
            });

        });
    </script>
   </body>
</html>