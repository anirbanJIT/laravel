<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
      <meta name="description" content="POS - Bootstrap Admin Template">
      <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
      <meta name="author" content="Dreamguys - Bootstrap Admin Template">
      <meta name="robots" content="noindex, nofollow">
      <title>Likes & Dislikes List</title>
      
      <link rel="shortcut icon" type="image/x-icon" href="{{ asset('public/Admin/img/favicon.png')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/bootstrap.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/animate.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/dataTables.bootstrap4.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/fontawesome.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/all.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/style.css')}}">
   </head>
   <body>
      <div id="global-loader">
         <div class="whirly-loader"> </div>
      </div>
      <div class="main-wrapper">
        @extends('layouts.header')
     
        @extends('layouts.sidebar')
        
        <div class="page-wrapper">
            <div class="content container-fluid">
               <div class="page-header">
                  <div class="row">
                     <div class="col">
                        <h3 class="page-title">Likes & Dislikes List</h3>
                        <ul class="breadcrumb">
                           <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">{{ trans('global.dashboard') }}</a></li>
                           <li class="breadcrumb-item active">Likes & Dislikes List</li>
                        </ul>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-12">
                     <div class="card">
                        <div class="card-header row">
                            <div class="col-lg-6"><h4 class="card-title">Likes & Dislikes List</h4></div>
                        </div>
                        <div class="card-body">
                           <div class="table-responsive">
                              <table class="table  datanew ">
                                 <thead>
                                    <tr role="row" class="heading">
                                    <th width="5%">ID</th>
                                    <th width="10%">User Name</th>
                                    <th width="5%">Screen Name</th>
                                    <th width="5%">Reaction</th>
                                    <th width="5%">Rating</th>
                                    <th width="10%">Comment</th>
                                    <th width="10%">Admin Status</th>
                                    <th width="20%">Admin Comment</th>
                                    <th width="20%">Date</th>
                                    <th width="10%">&nbsp;</th>
                                </tr>
                                 </thead>
                                 <tbody>
                                 @foreach($userReactions as $key => $userReaction)
                                   <tr data-entry-id="{{ $userReaction->id }}">
                                       <td>{{ $userReaction->id }}</td>
                                       <td>
                                           <a href="{{ route('admin.retailers.show', $userReaction->user_id)}}">{{ $userReaction->name}}</a>
                                       </td>
                                       <td>
                                           <a href="{{ route('admin.models-screen-image.show', $userReaction->brand_model_screen_image_id) }}">{{ $userReaction->screen_name}}</a>
                                       </td>
                                       <td>
                                           @if($userReaction->status == 1)
                                           {{'Like'}}
                                           @elseif($userReaction->status == 2)
                                           {{'Dislike'}}
                                           @else
                                           {{'No Action'}}
                                           @endif
                                       </td>
                                       <td>{{ $userReaction->rating}}</td>
                                       <td>{{ $userReaction->comment}}</td>
                                       <td>{{ $userReaction->admin_status == 1 ? 'Resolved' : ''}}</td>
                                       <td>{{ $userReaction->admin_comment}}</td>
                                       <td data-order="{{ \Carbon\Carbon::parse($userReaction->created_at)->format('Y-m-d') ?? '' }}"> {{ convertUtcToLocal($userReaction->created_at)->format('d/m/Y H:i:s') ?? '' }}</td>
                                       <td>
                                           @if($userReaction->status == 2)
                                           <button class="btn btn-primary success-icon-notika btn-reco-mg btn-button-mg waves-effect" data-bs-toggle="modal" data-bs-target="#modal{{$userReaction->id}}" data-bs-toggle="tooltip" data-placement="bottom" title="" data-original-title="Change Status"><i class="fa fa-times-circle"></i></button>
                                           <div class="modal" tabindex="-1" id="modal{{$userReaction->id}}" role="dialog">
                                               <div class="modal-dialog" role="document">
                                                   <div class="modal-content">
                                                       <div class="modal-header">
                                                           <h5 class="modal-title">Modal title</h5>
                                                           <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                                                               <span aria-hidden="true">X</span>
                                                           </button>
                                                       </div>
                                                       <form name="update_compaint" method="post" action="{{route('admin.update-user-reactions-status',$userReaction->id)}}">
                                                           @csrf
                                                           <div class="modal-body">
                                                               <div class="form-group row">
                                                                   <div class="col-sm-4">
                                                                       <label for="comment">Comment</label>
                                                                   </div>
                                                                   <div class="col-sm-8">
                                                                       <textarea class="form-group" style="min-width: 100%" name="comment" id="comment"></textarea>
                                                                   </div>
                                                               </div>
                                                           </div>
                                                           <div class="modal-footer">
                                                               <button type="submit" class="btn btn-primary">Save changes</button>
                                                               <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                                           </div>
                                                       </form>
                                                   </div>
                                               </div>
                                           </div>
                                           @endif
                                       </td>
                                   </tr>
                                   @endforeach
                                    
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
    <script src="{{ asset('public/Admin/js/jquery-3.6.0.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/feather.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.slimscroll.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{ asset('public/Admin/plugins/apexchart/apexcharts.min.js')}}"></script>
    <script src="{{ asset('public/Admin/plugins/apexchart/chart-data.js')}}"></script>
    <script src="{{ asset('public/Admin/js/script.js')}}"></script>
   </body>
</html>