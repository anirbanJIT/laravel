<!DOCTYPE html>
<html lang="en">
  <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
      <meta name="description" content="POS - Bootstrap Admin Template">
      <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
      <meta name="author" content="Dreamguys - Bootstrap Admin Template">
      <meta name="robots" content="noindex, nofollow">
      <title>Likes & Dislikes List</title>
      
      <link rel="shortcut icon" type="image/x-icon" href="{{ asset('public/Admin/img/favicon.png')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/bootstrap.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/animate.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/dataTables.bootstrap4.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/fontawesome.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/all.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/style.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/select2/css/select2.min.css')}}">
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <link rel="stylesheet" href="{{ asset('public/Admin/tabulator/bootstrap/tabulator_bootstrap4.css')}}">
  </head>
  <body>
    <div class="main-wrapper">
      @extends('layouts.header')
      @extends('layouts.sidebar')
      <div class="page-wrapper">
        <div class="content container-fluid">

          <div class="page-header">
            <div class="row">
              <div class="col">
                <h3 class="page-title">Likes & Dislikes List</h3>
                <ul class="breadcrumb">
                  <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">{{ trans('global.dashboard') }}</a></li>
                  <li class="breadcrumb-item active">Likes & Dislikes List</li>
                </ul>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-sm-12">
              <div class="card">

                <div class="card-header row">
                  <div class="col-lg-6">
                    <h4 class="card-title">Likes & Dislikes List</h4>
                  </div>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <button class="btn btn-outline-info waves-effect" id="download-csv">Download CSV</button> <p><br/></p>
                    <div id="data-table-user-reactions"></div>
					
					<div class="modal fade"  id="modal" role="dialog">
						<div class="modal-dialog modal-dialog-centered"  role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title">Modal title</h5>
									<button type="button" class="close" data-bs-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>
								</div>
								<form id="update_compaint" method="post" action="">
									<div class="modal-body">
										<input type="hidden" name="_token" value="{{ csrf_token() }}">
										<div class="form-group row">
											<div class="col-sm-4">
												<label for="comment">Comment</label>
											</div>
											<div class="col-sm-8">
												<textarea class="form-group" style="min-width: 100%" name="comment" id="comment"></textarea>
											</div>
										
											<div class="col-sm-4">
												<label for="comment">Notification</label>
											</div>
											<div class="col-sm-8">
												<input type="checkbox" id="notfication" name="notfication" onchange="
												if ($(this).prop('checked')==true){
													$('.popup').attr('style','display:block')
												}else{
													$('.popup').attr('style','display:none');
												}">
											</div>
											
											<div class="col-sm-4 popup" style="display:none">
												<label for="comment">Send as a popup</label>
											</div>
											<div class="col-sm-8 popup" style="display:none">
												<input type="checkbox" id="sendaspopup" name="sendaspopup">
											</div>
										</div>
									</div>
									<div class="modal-footer">
										<button type="submit" class="btn btn-primary">Save changes</button>
										<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
									</div>
								</form>
							</div>
						</div>
					</div>
					
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
    <script src="{{ asset('public/Admin/js/jquery-3.6.0.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/feather.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.slimscroll.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/script.js')}}"></script>
    <script type="text/javascript" language="javascript" src='https://cdn.datatables.net/buttons/2.2.3/js/dataTables.buttons.min.js' defer></script>
    <script type="text/javascript" language="javascript" src='https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js' defer></script>
    <script type="text/javascript" language="javascript" src='https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js' defer></script>
    <script type="text/javascript" language="javascript" src='https://cdn.datatables.net/buttons/2.2.3/js/buttons.html5.min.js' defer></script> 
    <script src="{{ asset('public/Admin/tabulator/tabulator.min.js') }}"></script>
    <script src="{{ asset('public/Admin/tabulator/jspdf.min.js') }}"></script>
    <script src="{{ asset('public/Admin/tabulator/xlsx.full.min.js') }}"></script>
    <script src="{{ asset('public/Admin/tabulator/jspdf.plugin.autotable.js') }}"></script>
    <script type="text/javascript">
      var tabledata = <?php echo json_encode($userReactions); ?>;
      console.log(tabledata);
      function customFilter(data){
          return data.car && data.rating < 3;
      }

      function updateFilter(){
        var filter = $("#filter-field").val() == "function" ? customFilter : $("#filter-field").val();
        if($("#filter-field").val() == "function" ){
          $("#filter-type").prop("disabled", true);
          $("#filter-value").prop("disabled", true);
        }else{
          $("#filter-type").prop("disabled", false);
          $("#filter-value").prop("disabled", false);
        }
        table.setFilter(filter, $("#filter-type").val(), $("#filter-value").val());
      }

      //Update filters on value change
      $("#filter-field, #filter-type").change(updateFilter);
      $("#filter-value").keyup(updateFilter);

      var table = new Tabulator("#data-table-user-reactions", {
        data:tabledata,           //load row data from array
        layout:"fitColumns",      //fit columns to width of table
        responsiveLayout:"collapse",  //hide columns that dont fit on the table
        tooltips:true,            //show tool tips on cells
        addRowPos:"top",          //when adding a new row, add it to the top of the table
        history:true,             //allow undo and redo actions on the table
        pagination:"local",       //paginate the data
        paginationSize:10,         //allow 7 rows per page of data
        movableColumns:true,      //allow column order to be changed
        resizableRows:true,       //allow row order to be changed
        initialSort:[             //set the initial sort order of the data
            {column:"id", dir:"desc"},
        ],
        columns: [
          {title:"Id", field:"id", editor:"label",headerFilter:"input",headerFilterPlaceholder:"ID"},
          {title:"User Name", field:"name", editor:"label",headerFilter:"input",headerFilterPlaceholder:"User Name"},
          {title:"Screen Name", field:"screen_name", editor:"label",headerFilter:"input",headerFilterPlaceholder:"Screen Name"},
          {title:"Reaction", field:"status", 
            formatter:function(cell, formatterParams, onRendered){
              console.log(cell);                  
              if(cell.getValue()==1){
                return "Like";
              }else if(cell.getValue()==2){
                return "Dislike";
              } else {
                return "No Action"
              }            
            }
          },
          {title:"Rating", field:"rating", editor:"label",headerFilter:"input",headerFilterPlaceholder:"Rating"},
		  {title:"Comment", field:"comment", editor:"label",headerFilter:"input",headerFilterPlaceholder:"Comment"},
          {title:"Admin Status", field:"admin_status", 
            formatter:function(cell, formatterParams, onRendered){
              var status = cell.getValue().split("_");
              
              if(status[0] == 1){
                return "Resolved";
              } else {
                return ""
              }            
            }
          },
          {title:"Admin Comment", field:"admin_comment", editor:"label",headerFilter:"input",headerFilterPlaceholder:"Admin Comment"},
          {title:"Date", field:"created_at", editor:"label",headerFilter:"input",headerFilterPlaceholder:"Date"},
          {title:"Action", field:"admin_status", 
            formatter:function(cell, formatterParams, onRendered){
				var status = cell.getValue().split("_");
				
				var editlink = '';
				var userReaction_id = status[1];
				var action_url = "{{route('admin.update-user-reactions-status',':id')}}";
				action_url = action_url.replace(':id', userReaction_id);
				
				if(status[2] == 2){
					editlink = '<button class="btn btn-primary  text-white sa-warning waves-effect" data-bs-toggle="modal" data-bs-target="#modal"  data-url="'+action_url+'" data-bs-toggle="tooltip" data-placement="bottom" title="" data-original-title="Change Status" onclick="dislikepopup(this)"><i class="fa fa-times-circle"></i></button>';
				}
              return editlink; 
            },
          }
        ],
      });
	  
	  function dislikepopup(current){
		  var action_url = $(current).data('url');
		  $("#update_compaint").attr("action",action_url);
	  }

      //trigger download of data.xlsx file
      document.getElementById("download-csv").addEventListener("click", function(){
          table.download("csv", "data.csv");
      });
    </script>
  </body>
</html>