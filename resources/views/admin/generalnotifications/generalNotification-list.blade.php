<!DOCTYPE html>
<html lang="en">
  <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
      <meta name="description" content="POS - Bootstrap Admin Template">
      <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
      <meta name="author" content="Dreamguys - Bootstrap Admin Template">
      <meta name="robots" content="noindex, nofollow">
      <title>General Notification List</title>
      
      <link rel="shortcut icon" type="image/x-icon" href="{{ asset('public/Admin/img/favicon.png')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/bootstrap.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/animate.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/dataTables.bootstrap4.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/fontawesome.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/all.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/style.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/select2/css/select2.min.css')}}">
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <link rel="stylesheet" href="{{ asset('public/Admin/tabulator/bootstrap/tabulator_bootstrap4.css')}}">
  </head>
  <body>
    <div class="main-wrapper">
		@extends('layouts.header')
		@extends('layouts.sidebar')
		<div class="page-wrapper">
			<div class="content container-fluid">
				<div class="page-header">
					<div class="row">
						<div class="col">
							<h3 class="page-title">General Notification List</h3>
							<ul class="breadcrumb">
								<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">{{ trans('global.dashboard') }}</a></li>
								<li class="breadcrumb-item active">General Notification List</li>
							</ul>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-sm-12">
						<div class="card">
							<div class="card-header row">
								<div class="col-lg-6">
									<h4 class="card-title">General Notification List</h4>
								</div>
								<div class="col-lg-6">
									<a href="{{ route('admin.generalnotification.create') }}" class="btn btn-success" style="float: right;">+ {{ trans('global.add') }} {{ trans('cruds.generalnotifications.title_singular') }}</a>
								</div>
							</div>
							<div class="card-body">
								<div class="table-responsive">
									<button class="btn btn-outline-info waves-effect" id="download-csv">Download CSV</button> <p><br/></p>
									<div id="data-table-general-notification"></div>
								
									<div class="modal fade" id="notificationModel"  role="dialog" >
										<div class="modal-dialog modal-dialog-centered" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<h5 class="modal-title">Modal title</h5>
													<button type="button" class="close" data-bs-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>
												</div>
												<form id="formOfNotification" action="{{ route('admin.notificationSend') }}" method="POST" >
													<div class="modal-body">
														<input type="hidden" name="id" id="id" value="">
														<input type="hidden" name="_token" value="{{ csrf_token() }}">
														
														<div class="form-group row">
															<div class="col-sm-4">
																<label for="comment">Notification</label>
															</div>
															<div class="col-sm-8">
																<input type="checkbox" id="notfication" name="notfication" onchange="
																if ($(this).prop('checked')==true){
																	$('.popup').attr('style','display:block')
																}else{
																	$('.popup').attr('style','display:none');
																}">
															</div>
															<div class="col-sm-4 popup" style="display:none">
																<label for="comment">Send as a popup</label>
															</div>
															<div class="col-sm-8 popup" style="display:none">
																<input type="checkbox" id="sendaspopup" name="sendaspopup">
															</div>
														</div>
														<div class="modal-footer">
															<button type="submit" class="btn btn-primary">Save changes</button>
															<button type="button" data-bs-dismiss="modal" class="btn btn-secondary">Close</button>
														</div>
													</div>
												</form>
											</div>
										</div>
									</div>
								
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
    </div>
    <script src="{{ asset('public/Admin/js/jquery-3.6.0.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/feather.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.slimscroll.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/script.js')}}"></script>
    <script type="text/javascript" language="javascript" src='https://cdn.datatables.net/buttons/2.2.3/js/dataTables.buttons.min.js' defer></script>
    <script type="text/javascript" language="javascript" src='https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js' defer></script>
    <script type="text/javascript" language="javascript" src='https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js' defer></script>
    <script type="text/javascript" language="javascript" src='https://cdn.datatables.net/buttons/2.2.3/js/buttons.html5.min.js' defer></script> 
    <script src="{{ asset('public/Admin/tabulator/tabulator.min.js') }}"></script>
    <script src="{{ asset('public/Admin/tabulator/jspdf.min.js') }}"></script>
    <script src="{{ asset('public/Admin/tabulator/xlsx.full.min.js') }}"></script>
    <script src="{{ asset('public/Admin/tabulator/jspdf.plugin.autotable.js') }}"></script>
    <script type="text/javascript">
      var tabledata = <?php echo json_encode($generalnotifications); ?>;
      console.log(tabledata);
      function customFilter(data){
          return data.car && data.rating < 3;
      }

      function updateFilter(){
        var filter = $("#filter-field").val() == "function" ? customFilter : $("#filter-field").val();
        if($("#filter-field").val() == "function" ){
          $("#filter-type").prop("disabled", true);
          $("#filter-value").prop("disabled", true);
        }else{
          $("#filter-type").prop("disabled", false);
          $("#filter-value").prop("disabled", false);
        }
        table.setFilter(filter, $("#filter-type").val(), $("#filter-value").val());
      }

      //Update filters on value change
      $("#filter-field, #filter-type").change(updateFilter);
      $("#filter-value").keyup(updateFilter);
      

      var table = new Tabulator("#data-table-general-notification", {
        data:tabledata,           //load row data from array
        layout:"fitColumns",      //fit columns to width of table
        responsiveLayout:"collapse",  //hide columns that dont fit on the table
        tooltips:true,            //show tool tips on cells
        addRowPos:"top",          //when adding a new row, add it to the top of the table
        history:true,             //allow undo and redo actions on the table
        pagination:"local",       //paginate the data
        paginationSize:10,         //allow 7 rows per page of data
        movableColumns:true,      //allow column order to be changed
        resizableRows:true,       //allow row order to be changed
        initialSort:[             //set the initial sort order of the data
            {column:"id", dir:"desc"},
        ],
        columns: [
          {title:"Title", field:"title", editor:"label",headerFilter:"input",headerFilterPlaceholder:"Title"},
          {title:"Image", field:"image", 
            formatter:function(cell, formatterParams, onRendered){
              if(cell.getValue() != ""){
                var url = "{{ asset('Admin/images/notifications/:id')}}";
                url = url.replace(':id', cell.getValue());
                return '<img src="'+url+'" style="width:60px;height:60px; margin: 5px 5px 5px 0;">';
              }else{
                return "";
              }
            }
          },
          {title:"Retailer", field:"user_name", 
            formatter:function(cell, formatterParams, onRendered){
            	var arr = cell.getValue();
							var data = arr.split("_");
              if(data[1] != ''){
                var show_url = "{{ route('admin.retailers.show',':id') }}";
                show_url = show_url.replace(':id', data[1]);
                
                return '<a href="#">'+data[0]+'</a>';
              }else{
                return "All";
              }
            }
          },
		      {title:"Action", field:"id", 
            formatter:function(cell, formatterParams, onRendered){
              var view_url = '{{ route("admin.generalnotification.show", ":id")}}';
              view_url = view_url.replace(':id', cell.getValue());

              var edit_url = '{{ route("admin.generalnotification.edit", ":id")}}';
              edit_url = edit_url.replace(':id', cell.getValue());

              var delete_url = '{{ route("admin.generalnotification.destroy",":id")}}';
              delete_url = delete_url.replace(':id', cell.getValue());
			  			var msg = "Are you sure you want to delete this Notification?";
							var token = '{{ csrf_token() }}';

              var link = '<a  href="'+view_url+'" class="btn btn-primary text-white"><i class="fa fa-eye "></i></a><a  href="'+edit_url+'" class="btn btn-success text-white"><i class="fa fa-edit "></i></a><form action="'+delete_url+'" method="POST" onclick="$(\'#is_delete\').val(1)" onsubmit="return confirm(\''+msg+'\');" style="display: inline-block;"><input type="hidden" name="_method" value="DELETE"><input type="hidden" name="_token" value="'+token+'"><button type="submit" class="btn btn-danger danger-icon-notika btn-reco-mg btn-button-mg waves-effect"> <i class="fa fa-trash"></i></button></form><button class="btn btn-primary  text-white sa-warning waves-effect" data-bs-toggle="modal" data-bs-target="#notificationModel" onclick="popupmodel(this)" data-id="'+cell.getValue()+'"><i class="fa fa-times-circle"></i></button>';
                  return link; 
            },
          }
        ],
      });
	  
	  function popupmodel(current){
		  var action_url = $(current).data('url');
		  $("#formOfNotification").attr("action",action_url);
		  $("#formOfNotification").find("#id").val($(current).data("id"));
	  }

      //trigger download of data.xlsx file
      document.getElementById("download-csv").addEventListener("click", function(){
          table.download("csv", "data.csv");
      });
    </script>
  </body>
</html>