<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
      <meta name="description" content="POS - Bootstrap Admin Template">
      <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
      <meta name="author" content="Dreamguys - Bootstrap Admin Template">
      <meta name="robots" content="noindex, nofollow">
      <title>{{ trans('global.edit') }} {{ trans('cruds.generalnotifications.title_singular') }}</title>
      
      <link rel="shortcut icon" type="image/x-icon" href="{{ asset('public/Admin/img/favicon.png')}}">
      
      <link rel="stylesheet" href="{{ asset('public/Admin/css/bootstrap.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/select2/css/select2.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/animate.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/dataTables.bootstrap4.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/fontawesome.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/all.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/style.css')}}">
      <link href="https://cdn.rawgit.com/harvesthq/chosen/gh-pages/chosen.min.css" rel="stylesheet"/>
   </head>
   <body>
      <div id="global-loader">
         <div class="whirly-loader"> </div>
      </div>
      <div class="main-wrapper">
        @extends('layouts.header')
     
        @extends('layouts.sidebar')
        
        <div class="page-wrapper">
            <div class="content container-fluid">
               <div class="page-header">
                  <div class="row">
                     <div class="col">
                        <h3 class="page-title">{{ trans('global.edit') }} {{ trans('cruds.generalnotifications.title_singular') }}</h3>
                        <ul class="breadcrumb">
                           <li class="breadcrumb-item"><a href="{{route('admin.home')}}">{{ trans('global.dashboard') }}</a></li>
                           <li class="breadcrumb-item"><a href="{{ route('admin.generalnotification.index') }}">{{ trans('cruds.generalnotifications.title') }}</a></li>
                           <li class="breadcrumb-item active">{{ trans('global.edit') }} {{ trans('cruds.generalnotifications.title_singular') }}</li>
                        </ul>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-12">
                     <div class="card">
                        <!-- <div class="card-header row">
                            <div class="col-lg-6"><h4 class="card-title">{{ trans('global.create') }} {{ trans('cruds.permission.title_singular') }}</h4></div>
                            
                        </div> -->
                        <div class="card-body">
                           <form action="{{ route('admin.generalnotification.update', [$notifications->id]) }}" method="POST" enctype="multipart/form-data" id="frm_notification" novalidate="">

                            {{ csrf_field() }}
                              @method('PUT')
                              
                              <div class="form-group">
                                 <label>Title<span class="text-danger">*</span></label>
                                 <input type="text" id="title" name="title" value="{{ $notifications->title }}" required class="form-control @error('title') is-invalid @enderror" placeholder="{{ trans('cruds.generalnotifications.fields.title') }}*" >
                                 @if($errors->has('title')) <em class="invalid-feedback"> {{ $errors->first('title') }} </em> @endif
                              </div>
                              <div class="form-group">
                                 <label>Name</label>
                                 <select name="user_id[]" data-placeholder="Begin typing a name to filter..." multiple class="chosen-select form-control @error('user_id') is-invalid @enderror">
                                    @foreach($users as $row)
                                       <option value="{{ $row->id }}" {{ (in_array($row->id,$user_ids) ? 'selected': '')}}>{{ $row->name }}</option>
                                    @endforeach
                                 </select>
                                
                                 @if($errors->has('user_id')) <em class="invalid-feedback"> {{ $errors->first('user_id') }} </em> @endif
                              </div>

                              

                              <div class="form-group">
                                 <label>File Upload</label>
                                 <input type="file" accept="image/*" class="form-control @error('notificationImage') is-invalid @enderror notificationImage" name="notificationImage[]" multiple id="notificationImage">
                                 @if($errors->has('notificationImage')) <em class="invalid-feedback"> {{ $errors->first('notificationImage') }} </em> @endif
                              </div>
							        <span id="preview">
      								  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 mb-2">
      									  @if(!empty($notifications->image))
      									  <img src="{{ asset('Admin/images/notifications/'.$notifications->image) }}" width="50" height="50">
      									  @endif
      									  <input type="hidden" name="attachmnetUpdate" id = "attachmnetUpdate" value="{{ $notifications->image }}" >
      								  </div>
								      </span>

                              <div class="form-group">
                                 <label>Message<span class="text-danger">*</span></label>
                                 <textarea class="form-control @error('message') is-invalid @enderror" rows="5" id="messages" name="message">{{ $notifications->message }}</textarea>
                                 @if ($errors->has('message'))
                                 <span class="text-danger">{{ $errors->first('message') }}</span>
                                 @endif
                              </div>
                              
                              <div class="">
                                 <button type="submit" class="btn btn-success">Save</button>
                              </div>
                           </form>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
    <script src="{{ asset('public/Admin/js/jquery-3.6.0.min.js')}}"></script>
    <script src="{{ asset('public/Admin/plugins/select2/js/select2.min.js')}}"></script>
    <script src="{{ asset('public/Admin/plugins/select2/js/custom-select.js')}}"></script>
    <script src="{{ asset('public/Admin/js/feather.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.slimscroll.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/script.js')}}"></script>
    <script src="https://cdn.rawgit.com/harvesthq/chosen/gh-pages/chosen.jquery.min.js"></script>
    <script type="text/javascript">
      $(".chosen-select").chosen({
        no_results_text: "Oops, nothing found!"
      });
       $(function(){
         $("#frm_banner_notification").validate({
             rules: {
               title: {
                  required: true,
               },
               image: {
                  required: true,
               }
           },
            messages: { 
                 title: {
                     required: "Please enter title"
                 },
                 image: {
                     required: "Please enter title"
                 },
             },
         });
      });
	  $('.notificationImage').change(function(){
			const file = this.files[0];
			
			console.log(file);
			if (file){
			let reader = new FileReader();
			reader.onload = function(event){
				console.log(event.target.result);
				$("#preview").html('<img id="imgPreview" hight="50px" width="50px" src="'+event.target.result+'" alt="pic" />');
				//$('#imgPreview').attr('src', );
			}
			reader.readAsDataURL(file);
			}
		});
		

	
    </script>
   </body>
</html>