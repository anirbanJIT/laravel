<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
      <meta name="description" content="POS - Bootstrap Admin Template">
      <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
      <meta name="author" content="Dreamguys - Bootstrap Admin Template">
      <meta name="robots" content="noindex, nofollow">
      <title>{{ trans('cruds.generalnotifications.title_singular') }} {{ trans('global.list') }}</title>
      
      <link rel="shortcut icon" type="image/x-icon" href="{{ asset('public/Admin/img/favicon.png')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/bootstrap.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/animate.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/dataTables.bootstrap4.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/fontawesome.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/all.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/style.css')}}">
   </head>
   <body>
      <div id="global-loader">
         <div class="whirly-loader"> </div>
      </div>
      <div class="main-wrapper">
        @extends('layouts.header')
     
        @extends('layouts.sidebar')
        
        <div class="page-wrapper">
            <div class="content container-fluid">
               <div class="page-header">
                  <div class="row">
                     <div class="col">
                        <h3 class="page-title">{{ trans('cruds.generalnotifications.title_singular') }} {{ trans('global.list') }}</h3>
                        <ul class="breadcrumb">
                           <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">{{ trans('global.dashboard') }}</a></li>
                           <li class="breadcrumb-item active">{{ trans('cruds.generalnotifications.title_singular') }} {{ trans('global.list') }}</li>
                        </ul>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-12">
                     <div class="card">
                        <div class="card-header row">
                            <div class="col-lg-6"><h4 class="card-title">{{ trans('cruds.generalnotifications.title_singular') }} {{ trans('global.list') }}</h4></div>
                            <div class="col-lg-6"><a href="{{ route('admin.generalnotification.create') }}" class="btn btn-success" style="float: right;">+ {{ trans('global.add') }} {{ trans('cruds.generalnotifications.title_singular') }}</a></div>
                        </div>
                        <div class="card-body">
                           <div class="table-responsive">
                              <table class="table  datanew ">
                                 <thead>
                                    <tr>
                                       <th>Title</th>
                                       <th>Image</th>
                                       <th>Retailer</th>
                                       <th>Action</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    @foreach($generalnotifications as $key => $generalnotification)
                                    <tr>
                                       <td>{{ $generalnotification->title ?? '' }}</td>
                                       <td>@if($generalnotification->image)
                                          @php
                                          $myArray = json_decode($generalnotification->image, True); 
                                          @endphp
                                          @if( $myArray === NULL )
                                          <img src="{{ asset('Admin/images/notifications/'.$generalnotification->image) }}" style="width:60px;height:60px; margin: 5px 5px 5px 0;">
                                          @else
                                          @foreach($myArray as $key => $value)
                                            <img src="{{ asset('Admin/images/notifications/'.$value) }}" style="width:60px;height:60px; margin: 5px 5px 5px 0;">
                                          @endforeach
                                          @endif
                                          @endif
                                       </td>
                                       <td>
                                          @if(!empty($generalnotification->user_id))
                                                   <a href="{{ route('admin.retailers.show', $generalnotification->users->id ?? 0 ) }}"> 
                                                      {{ $generalnotification->users->name ?? '' }}
                                                   </a>
                                                  @else
                                                   All
                                          @endif
                                       </td>
                                       <td>
                                          <a  href="{{ route('admin.generalnotification.show', $generalnotification->id)}}" class="btn btn-primary text-white"><i class="fa fa-eye "></i></a>

                                          <a  href="{{ route('admin.generalnotification.edit', $generalnotification->id)}}" class="btn btn-success text-white"><i class="fa fa-edit "></i></a>

                                          <a  href="{{ url('admin/generalnotification/delete', $generalnotification->id) }}" class="btn btn-danger text-white"><i class="fa fa-trash "></i></a>
                                             
                                    </tr>
                                    @endforeach
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
    <script src="{{ asset('public/Admin/js/jquery-3.6.0.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/feather.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.slimscroll.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{ asset('public/Admin/plugins/apexchart/apexcharts.min.js')}}"></script>
    <script src="{{ asset('public/Admin/plugins/apexchart/chart-data.js')}}"></script>
    <script src="{{ asset('public/Admin/js/script.js')}}"></script>
   </body>
</html>