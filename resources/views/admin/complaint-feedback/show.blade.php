<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
      <meta name="description" content="POS - Bootstrap Admin Template">
      <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
      <meta name="author" content="Dreamguys - Bootstrap Admin Template">
      <meta name="robots" content="noindex, nofollow">
      <title>Show Complaint Feedback</title>
      
      <link rel="shortcut icon" type="image/x-icon" href="{{ asset('public/Admin/img/favicon.png')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/bootstrap.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/animate.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/dataTables.bootstrap4.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/fontawesome.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/all.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/style.css')}}">
   </head>
   <body>
      <div id="global-loader">
         <div class="whirly-loader"> </div>
      </div>
      <div class="main-wrapper">
        @extends('layouts.header')
     
        @extends('layouts.sidebar')
        
        <div class="page-wrapper">
            <div class="content container-fluid">
               <div class="page-header">
                  <div class="row">
                     <div class="col">
                        <h3 class="page-title">Show Complaint Feedback</h3>
                        <ul class="breadcrumb">
                           <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">{{ trans('global.dashboard') }}</a></li>
						   <li class="breadcrumb-item"><a href="{{ route('admin.complaint-feedback.index') }}">Complaint List</a></li>
                           <li class="breadcrumb-item active">Show Complaint Feedback</li>
                        </ul>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-12">
                     <div class="card">
                        <div class="card-header row">
                            <div class="col-lg-6"><h4 class="card-title">Show Complaint Feedback</h4></div>
                            <div class="col-lg-6"><a href="{{ url()->previous() }}" class="btn btn-success" style="float: right;">{{ trans('global.back_to_list') }}</a></div>
                        </div>
                        <div class="card-body">
                           <div class="table-responsive">
                              <table class="table table-border">
                                 <tbody>
                                    <tr>
                                       <td>Title</th>
                                       <td>{{ $complaintfeedback->title }}</th>
                                    </tr>
                                    
                                    <tr>
                                       <td>Type</td>
                                       <td>{{ $complaintfeedback->type }}({{$complaintfeedback->status}})</td>
                                    </tr>
                                    <tr>
                                       <td>Name</td>
                                       <td>{{ $complaintfeedback->user->name }}</td>
                                    </tr>
                                    <tr>
                                       <td>Date</td>
                                       <td>{{ $complaintfeedback->created_at->format('d M Y') }}</td>
                                    </tr>
                                    <tr>
                                       <td>Description</td>
                                       <td>{{ $complaintfeedback->description }}</td>
                                    </tr>
                                    @if($complaintfeedback->status == 'Solved')
                                    <tr>
                                       <td>Comment</td>
                                       <td>{{ $complaintfeedback->comment }}</td>
                                    </tr>
                                    @endif
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
    <script src="{{ asset('public/Admin/js/jquery-3.6.0.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/feather.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.slimscroll.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{ asset('public/Admin/plugins/apexchart/apexcharts.min.js')}}"></script>
    <script src="{{ asset('public/Admin/plugins/apexchart/chart-data.js')}}"></script>
    <script src="{{ asset('public/Admin/js/script.js')}}"></script>
   </body>
</html>