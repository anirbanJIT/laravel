<!DOCTYPE html>
<html lang="en">
  <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
      <meta name="description" content="POS - Bootstrap Admin Template">
      <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
      <meta name="author" content="Dreamguys - Bootstrap Admin Template">
      <meta name="robots" content="noindex, nofollow">
      <title>Feedback List</title>
      
      <link rel="shortcut icon" type="image/x-icon" href="{{ asset('public/Admin/img/favicon.png')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/bootstrap.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/animate.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/dataTables.bootstrap4.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/fontawesome.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/all.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/style.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/select2/css/select2.min.css')}}">
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <link rel="stylesheet" href="{{ asset('public/Admin/tabulator/bootstrap/tabulator_bootstrap4.css')}}">
  </head>
  <body>
    <div class="main-wrapper">
      @extends('layouts.header')
      @extends('layouts.sidebar')
      <div class="page-wrapper">
        <div class="content container-fluid">

          <div class="page-header">
            <div class="row">
              <div class="col">
                <h3 class="page-title">Feedback List</h3>
                <ul class="breadcrumb">
                  <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">{{ trans('global.dashboard') }}</a></li>
                  <li class="breadcrumb-item active">Feedback List</li>
                </ul>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-sm-12">
              <div class="card">

                <div class="card-header row">
                  <div class="col-lg-6">
                    <h4 class="card-title">Feedback List</h4>
                  </div>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <button class="btn btn-outline-info waves-effect" id="download-csv">Download CSV</button> <p><br/></p>
                    <div id="data-table-complaint-feedback"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
    <script src="{{ asset('public/Admin/js/jquery-3.6.0.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/feather.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.slimscroll.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/script.js')}}"></script>
    <script type="text/javascript" language="javascript" src='https://cdn.datatables.net/buttons/2.2.3/js/dataTables.buttons.min.js' defer></script>
    <script type="text/javascript" language="javascript" src='https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js' defer></script>
    <script type="text/javascript" language="javascript" src='https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js' defer></script>
    <script type="text/javascript" language="javascript" src='https://cdn.datatables.net/buttons/2.2.3/js/buttons.html5.min.js' defer></script> 
    <script src="{{ asset('public/Admin/tabulator/tabulator.min.js') }}"></script>
    <script src="{{ asset('public/Admin/tabulator/jspdf.min.js') }}"></script>
    <script src="{{ asset('public/Admin/tabulator/xlsx.full.min.js') }}"></script>
    <script src="{{ asset('public/Admin/tabulator/jspdf.plugin.autotable.js') }}"></script>
    <script type="text/javascript">
      var tabledata = <?php echo json_encode($feedback_index); ?>;
      console.log(tabledata);
      function customFilter(data){
          return data.car && data.rating < 3;
      }

      function updateFilter(){
        var filter = $("#filter-field").val() == "function" ? customFilter : $("#filter-field").val();
        if($("#filter-field").val() == "function" ){
          $("#filter-type").prop("disabled", true);
          $("#filter-value").prop("disabled", true);
        }else{
          $("#filter-type").prop("disabled", false);
          $("#filter-value").prop("disabled", false);
        }
        table.setFilter(filter, $("#filter-type").val(), $("#filter-value").val());
      }

      //Update filters on value change
      $("#filter-field, #filter-type").change(updateFilter);
      $("#filter-value").keyup(updateFilter);

      var table = new Tabulator("#data-table-complaint-feedback", {
        data:tabledata,           //load row data from array
        layout:"fitColumns",      //fit columns to width of table
        responsiveLayout:"collapse",  //hide columns that dont fit on the table
        tooltips:true,            //show tool tips on cells
        addRowPos:"top",          //when adding a new row, add it to the top of the table
        history:true,             //allow undo and redo actions on the table
        pagination:"local",       //paginate the data
        paginationSize:10,         //allow 7 rows per page of data
        movableColumns:true,      //allow column order to be changed
        resizableRows:true,       //allow row order to be changed
        initialSort:[             //set the initial sort order of the data
            {column:"id", dir:"desc"},
        ],
        columns: [
          {title:"Id", field:"id", editor:"label",headerFilter:"input",headerFilterPlaceholder:"Feedback ID"},
          {title:"Title", field:"title", editor:"label",headerFilter:"input",headerFilterPlaceholder:"Feedback Title"},
          //{title:"Type", field:"type", editor:"label",headerFilter:"input",headerFilterPlaceholder:"Feedback Type"},
          {title:"Name", field:"name", editor:"label",headerFilter:"input",headerFilterPlaceholder:"Feedback User Name"},
          //{title:"Status", field:"status", editor:"label",headerFilter:"input",headerFilterPlaceholder:"Feedback Status"},
          
          {title:"Date", field:"created_at", editor:"label",headerFilter:"input",headerFilterPlaceholder:"Feedback Date"},
          {title:"Action", field:"id", 
            formatter:function(cell, formatterParams, onRendered){
              
              var view_url = '{{ route("admin.complaint-feedback.show", ":id") }}';
              view_url = view_url.replace(':id', cell.getValue());

              var url1 = '{{ route("admin.generalnotification.create", "user_id=:id") }}';
              url1 = url1.replace(':id', cell.getValue());

              var editlink = '<a  href="'+view_url+'" class="btn btn-info text-white" title="View"><i class="fa fa-eye"></i></a><a  href="'+url1+'" class="btn btn-success text-white" title="Send Notification"><i class=" fa fa-comment"></i></button></a>';
                  return editlink; 
            },
          }
        ],
      });

      //trigger download of data.xlsx file
      document.getElementById("download-csv").addEventListener("click", function(){
          table.download("csv", "data.csv");
      });
    </script>
  </body>
</html>