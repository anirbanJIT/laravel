<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
      <meta name="description" content="POS - Bootstrap Admin Template">
      <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
      <meta name="author" content="Dreamguys - Bootstrap Admin Template">
      <meta name="robots" content="noindex, nofollow">
      <title>{{ trans('global.create') }} Complaint</title>
      
      <link rel="shortcut icon" type="image/x-icon" href="{{ asset('public/Admin/img/favicon.png')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/bootstrap.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/animate.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/dataTables.bootstrap4.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/fontawesome.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/all.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/style.css')}}">
   </head>
   <body>
      <div id="global-loader">
         <div class="whirly-loader"> </div>
      </div>
      <div class="main-wrapper">
        @extends('layouts.header')
     
        @extends('layouts.sidebar')
        
        <div class="page-wrapper">
            <div class="content container-fluid">
               <div class="page-header">
                  <div class="row">
                     <div class="col">
                        <h3 class="page-title">{{ trans('global.create') }}  Complaint</h3>
                        <ul class="breadcrumb">
                           <li class="breadcrumb-item"><a href="{{route('admin.home')}}">{{ trans('global.dashboard') }}</a></li>
                           <li class="breadcrumb-item"><a href="{{ route('admin.complaint-feedback.index') }}">Complaint</a></li>
                           <li class="breadcrumb-item active">{{ trans('global.create') }} Complaint</li>
                        </ul>
                     </div>
                  </div>
               </div>
               <style>
               .details{display:none;}
               </style>
               <div class="row">
                  <div class="col-sm-12">
                     <div class="card">
                        <div class="card-body">
                           <form action="{{ route('admin.complaint-feedback.store') }}" method="POST" enctype="multipart/form-data">
                            {{ csrf_field() }}
                              <div class="form-group">
                                 <label>User Name</label>
								 
								 <select class="form-control" id="name" name="name">
									@foreach($users as $row)
										<option value="{{ $row->id}}"> {{ $row->name }}</option><p id="{{ $row->id }}" style="float:right;">{{ $row->phone }}</p>
									@endforeach
								 </select>
                                 <div id="details-container" >
                                    @foreach($users as $row)
										<div class="{{ $row->id}} details">
                                        <!--<p style="text-align:right;">{{ $row->phone }}</p>-->
                                        <input type="text" class="form-control" style="border:none; background:none;" value="Mobile No. : {{ $row->phone }}" disabled/>
                                        </div>
									@endforeach
                                 </div>

                                 @if($errors->has('name')) <em class="invalid-feedback"> {{ $errors->first('name') }} </em> @endif
                              </div>
                              <div class="form-group row">
                                    <label>Complaint</label>
                                    <div class="col-sm-12">
                                        <textarea class="form-group" style="min-width: 100%" name="description" id="comment"required></textarea>
                                    </div>
                                </div>

                              
                              <div>
                                 <button type="submit" class="btn btn-success">Save</button>
                              </div>
                           </form>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
    <script src="{{ asset('public/Admin/js/jquery-3.6.0.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/feather.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.slimscroll.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{ asset('public/Admin/plugins/apexchart/apexcharts.min.js')}}"></script>
    <script src="{{ asset('public/Admin/plugins/apexchart/chart-data.js')}}"></script>
    <script src="{{ asset('public/Admin/js/script.js')}}"></script>
    
    <script>
    $(document).ready(function(){
        $("#name").change(function(){
            var data = $("#name").val();
            $(".details").hide();
            $("."+data).show();
        })
    })
    </script>
   </body>
</html>