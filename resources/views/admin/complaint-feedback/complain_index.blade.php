<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
      <meta name="description" content="POS - Bootstrap Admin Template">
      <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
      <meta name="author" content="Dreamguys - Bootstrap Admin Template">
      <meta name="robots" content="noindex, nofollow">
      <title>Complaint List</title>
      
      <link rel="shortcut icon" type="image/x-icon" href="{{ asset('public/Admin/img/favicon.png')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/bootstrap.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/animate.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/dataTables.bootstrap4.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/fontawesome.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/all.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/style.css')}}">
   </head>
   <body>
      <div id="global-loader">
         <div class="whirly-loader"> </div>
      </div>
      <div class="main-wrapper">
        @extends('layouts.header')
     
        @extends('layouts.sidebar')
        
        <div class="page-wrapper">
            <div class="content container-fluid">
               <div class="page-header">
                  <div class="row">
                     <div class="col">
                        <h3 class="page-title">Complaint List</h3>
                        <ul class="breadcrumb">
                           <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">{{ trans('global.dashboard') }}</a></li>
                           <li class="breadcrumb-item active">Complaint List</li>
                        </ul>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-12">
                     <div class="card">
                        <div class="card-header row">
                            <div class="col-lg-6"><h4 class="card-title">Complaint List</h4></div>
                        </div>
                        <div class="card-body">
                           <div class="table-responsive">
                              <table class="table  datanew " id="datatable-ComplaintFeedback">
                                 <thead>
                                    <tr role="row" class="heading">
                                       <th width="5%">ID</th>
                                       <th width="15%">Title</th>
                                       <th width="10%">Type</th>
                                       <th width="20%">Name</th>
                                       <th width="10%">Status</th>
                                       <th width="15%">Date</th>
                                       <th width="25%">&nbsp;</th>
                                   </tr>
                                 </thead>
                                 <tbody>
                                    @foreach($complain_index as $key => $complaintfeedback)
                                <tr data-entry-id="{{ $complaintfeedback->id }}">
                                    <td>{{ $complaintfeedback->id }}</td>
                                    <td>{{ ucwords($complaintfeedback->title ?? '' )}}</td>
                                    <td>{{ $complaintfeedback->type ?? '' }}</td>
                                    <td>{{ ucwords($complaintfeedback->user->name ?? '') }}</td>
                                    <td>{{ $complaintfeedback->status }}
                                    </td>
                                    <td data-order="{{ \Carbon\Carbon::parse($complaintfeedback->created_at)->format('Y-m-d') ?? '' }}">
                                        {{ $complaintfeedback->created_at->format('d M Y') ?? '' }}</td>
                                    <td>
                                        <a  href="{{ route('admin.complaint-feedback.show', $complaintfeedback->id) }}" class="btn btn-info text-white" title="View"><i class="fa fa-eye"></i></a>

                                        <a  href="{{ route('admin.generalnotification.create', ['user_id' => $complaintfeedback->user_id]) }}" class="btn btn-success text-white" title="Send Notification"><i class=" fa fa-comment"></i></button></a>
                                        @if($complaintfeedback->status == 'UnSolved' && $complaintfeedback->type == 'complaint')
                                        <button class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#modal{{$complaintfeedback->id}}" data-toggle="tooltip" data-placement="bottom" title="Change Status" data-original-title=""><i class="fa fa-times-circle"></i></button>
                                        @endif
                                        <div class="modal" tabindex="-1" id="modal{{$complaintfeedback->id}}" role="dialog">
                                          <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                              <div class="modal-header">
                                                <h5 class="modal-title">Modal title</h5>
                                                <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                                                  <span aria-hidden="true">X</span>
                                                </button>
                                              </div>
                                              <form name="update_compaint" method="POST" action="{{route('admin.update-compaint-feedback-status',$complaintfeedback->id)}}">
                                             @csrf
                                              <div class="modal-body">
                                                <div class="form-group row">
                                                    <div class="col-sm-4">
                                                        <label for="comment">Comment</label>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <textarea class="form-control" style="min-width: 100%" name="comment" id="comment"></textarea>
                                                    </div>
                                                </div>
                                              </div>
                                              <div class="modal-footer">
                                                <button type="submit" class="btn btn-primary">Save changes</button>
                                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                              </div>
                                              </form>
                                            </div>
                                          </div>
                                        </div>
                                        
                              
                                    </td>
                                </tr>
                            @endforeach
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
    <script src="{{ asset('public/Admin/js/jquery-3.6.0.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/feather.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.slimscroll.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{ asset('public/Admin/plugins/apexchart/apexcharts.min.js')}}"></script>
    <script src="{{ asset('public/Admin/plugins/apexchart/chart-data.js')}}"></script>
    <script src="{{ asset('public/Admin/js/script.js')}}"></script>
   </body>
</html>