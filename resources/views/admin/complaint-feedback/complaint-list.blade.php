<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
		<meta name="description" content="POS - Bootstrap Admin Template">
		<meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
		<meta name="author" content="Dreamguys - Bootstrap Admin Template">
		<meta name="robots" content="noindex, nofollow">
		<title>Complaint List</title>
      
		<link rel="shortcut icon" type="image/x-icon" href="{{ asset('public/Admin/img/favicon.png')}}">
		<link rel="stylesheet" href="{{ asset('public/Admin/css/bootstrap.min.css')}}">
		<link rel="stylesheet" href="{{ asset('public/Admin/css/animate.css')}}">
		<link rel="stylesheet" href="{{ asset('public/Admin/css/dataTables.bootstrap4.min.css')}}">
		<link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/fontawesome.min.css')}}">
		<link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/all.min.css')}}">
		<link rel="stylesheet" href="{{ asset('public/Admin/css/style.css')}}">
		<link rel="stylesheet" href="{{ asset('public/Admin/plugins/select2/css/select2.min.css')}}">
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<link rel="stylesheet" href="{{ asset('public/Admin/tabulator/bootstrap/tabulator_bootstrap4.css')}}">
	</head>
	<body>
		<div class="main-wrapper">
			@extends('layouts.header')
			@extends('layouts.sidebar')
			<div class="page-wrapper">
				<div class="content container-fluid">
					<div class="page-header">
						<div class="row">
							<div class="col">
								<h3 class="page-title">Complaint List</h3>
								<ul class="breadcrumb">
									<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">{{ trans('global.dashboard') }}</a></li>
									<li class="breadcrumb-item active">Complaint List</li>
								</ul>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-sm-12">
							<div class="card">

								<div class="card-header row">
									<div class="col-lg-6">
										<h4 class="card-title">Complaint List</h4>
									</div>
									<div class="col-lg-6"><a href="{{ route('admin.complaint-feedback.create') }}" class="btn btn-success" style="float: right;">+ Create Complain</a></div>
								</div>
								
								<div class="card-body">
									<div class="table-responsive">
									
										<button class="btn btn-outline-info waves-effect" id="download-csv">Download CSV</button> <p><br/></p>
										<div id="data-table-complaint-feedback"></div>

										
										<div class="modal fade" id="status_model"  role="dialog" >
											<div class="modal-dialog modal-dialog-centered" role="document">
												<div class="modal-content">
													<div class="modal-header">
														<h5 class="modal-title">Modal title</h5>
														<button type="button" class="close" data-bs-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>
													</div>
													<form action="{{ route('admin.complaint_changestatus') }}" method="POST" id="frm_change_status">
														<div class="modal-body">
															<input type="hidden" name="status_id" id="status_id" value="">
															<input type="hidden" name="status" value="UnSolved">
															<input type="hidden" name="_token" value="{{ csrf_token() }}">
												 
															<div class="form-group row">
																<div class="col-sm-4">
																	<label for="comment">Comment</label>
																</div>
																<div class="col-sm-8">
																	<textarea class="form-group" style="min-width: 100%" name="comment" id="comment"required></textarea>
																</div>
															</div>
															<div class="modal-footer">
																<button type="submit" class="btn btn-primary">Save changes</button>
																<button type="button" data-bs-dismiss="modal" class="btn btn-secondary">Close</button>
															</div>
														</div>
													</form>
												</div>
											</div>
										</div>
										
										<div class="modal fade" id="statusNotificationModel"  role="dialog" >
											<div class="modal-dialog modal-dialog-centered" role="document">
												<div class="modal-content">
													<div class="modal-header">
														<h5 class="modal-title">Modal title</h5>
														<button type="button" class="close" data-bs-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>
													</div>
													<form action="{{ route('admin.changeStausNotification') }}" method="POST" >
														<div class="modal-body">
															<input type="hidden" name="complain_id" id="complain_id" value="">
															<input type="hidden" name="status" id="status" value="">
															<input type="hidden" name="_token" value="{{ csrf_token() }}">
												 
															<div class="form-group row">
																<div class="col-sm-4">
																	<label for="comment">Notification</label>
																</div>
																<div class="col-sm-8">
																	<input type="checkbox" id="notfication" name="notfication" onchange="
																	if ($(this).prop('checked')==true){
																		$('.popup').attr('style','display:block')
																	}else{
																		$('.popup').attr('style','display:none');
																	}">
																</div>
																<div class="col-sm-4 popup" style="display:none">
																	<label for="comment">Send as a popup</label>
																</div>
																<div class="col-sm-8 popup" style="display:none">
																	<input type="checkbox" id="sendaspopup" name="sendaspopup">
																</div>
															</div>
															<div class="modal-footer">
																<button type="submit" class="btn btn-primary">Save changes</button>
																<button type="button" data-bs-dismiss="modal" class="btn btn-secondary">Close</button>
															</div>
														</div>
													</form>
												</div>
											</div>
										</div>
									</div>
								</div>
								
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
		<script src="{{ asset('public/Admin/js/jquery-3.6.0.min.js')}}"></script>
		<script src="{{ asset('public/Admin/js/feather.min.js')}}"></script>
		<script src="{{ asset('public/Admin/js/jquery.slimscroll.min.js')}}"></script>
		<script src="{{ asset('public/Admin/js/jquery.dataTables.min.js')}}"></script>
		<script src="{{ asset('public/Admin/js/dataTables.bootstrap4.min.js')}}"></script>
		<script src="{{ asset('public/Admin/js/bootstrap.bundle.min.js')}}"></script>
		<script src="{{ asset('public/Admin/js/script.js')}}"></script>
		<script type="text/javascript" language="javascript" src='https://cdn.datatables.net/buttons/2.2.3/js/dataTables.buttons.min.js' defer></script>
		<script type="text/javascript" language="javascript" src='https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js' defer></script>
		<script type="text/javascript" language="javascript" src='https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js' defer></script>
		<script type="text/javascript" language="javascript" src='https://cdn.datatables.net/buttons/2.2.3/js/buttons.html5.min.js' defer></script> 
		<script src="{{ asset('public/Admin/tabulator/tabulator.min.js') }}"></script>
		<script src="{{ asset('public/Admin/tabulator/jspdf.min.js') }}"></script>
		<script src="{{ asset('public/Admin/tabulator/xlsx.full.min.js') }}"></script>
		<script src="{{ asset('public/Admin/tabulator/jspdf.plugin.autotable.js') }}"></script>
		<script type="text/javascript">
			var tabledata = <?php echo json_encode($complain_index); ?>;
			//console.log(tabledata);
      

			var table = new Tabulator("#data-table-complaint-feedback", {
				data:tabledata,           //load row data from array
				layout:"fitColumns",      //fit columns to width of table
				responsiveLayout:"collapse",  //hide columns that dont fit on the table
				tooltips:true,            //show tool tips on cells
				addRowPos:"top",          //when adding a new row, add it to the top of the table
				history:true,             //allow undo and redo actions on the table
				pagination:"local",       //paginate the data
				paginationSize:10,         //allow 7 rows per page of data
				movableColumns:true,      //allow column order to be changed
				resizableRows:true,       //allow row order to be changed
				initialSort:[             //set the initial sort order of the data
					{column:"id", dir:"desc"},
				],
				columns: [
					{title:"Id", field:"id", editor:"label",headerFilter:"input",headerFilterPlaceholder:"Complaint ID"},
					{title:"Title", field:"title", editor:"label",headerFilter:"input",headerFilterPlaceholder:"Complaint Title"},
					// {title:"Type", field:"type", editor:"label",headerFilter:"input",headerFilterPlaceholder:"Complaint Type"},
					{title:"Name", field:"name", editor:"label",headerFilter:"input",headerFilterPlaceholder:"Complaint User Name"},
					{title:"Status", field:"cust_status", 
						formatter:function(cell, formatterParams, onRendered){
							var status = cell.getValue().split("_");
							//console.log(status[0]);
							var view_url = '{{ route("admin.complaint-feedback.show", ":id") }}';
							view_url = view_url.replace(':id', cell.getValue());
							var options = {
								"backdrop" : "static",
								"show":true
							}
							var editlink2 = '<select class="form-control change_status" data-status-id="'+status[1]+'" data-id="'+status[1]+'" onchange="change_status(this)"><option value="Solved" '+ ((status[0] == 'Solved') ? "selected": "") +' data-bs-toggle="modal" data-bs-target="#statusNotificationModel">Solved</option><option value="UnSolved" '+ ((status[0] == 'UnSolved') ? "selected": "") +'  data-bs-toggle="modal" data-bs-target="#statusNotificationModel">UnSolved</option></select>';
							return editlink2; 
						},
					},
					{title:"Date", field:"created_at", editor:"label",headerFilter:"input",headerFilterPlaceholder:"Complaint Date"},
					{title:"Action", field:"cust_status", 
						formatter:function(cell, formatterParams, onRendered){
							var status_id = cell.getValue().split("_");
							var id = status_id[1];
							var status = status_id[0];
							
							var view_url = '{{ route("admin.complaint-feedback.show", ":id") }}';
							view_url = view_url.replace(':id', id);

							var url1 = '{{ route("admin.generalnotification.create", "user_id=:id") }}';
							url1 = url1.replace(':id', id);

							var model_form_url= '{{route("admin.update-compaint-feedback-status",":id")}}';
							model_form_url = model_form_url.replace(':id', id);
							var token = "{{ csrf_token() }}";

							var editlink = '<a  href="'+view_url+'" class="btn btn-info text-white" title="View"><i class="fa fa-eye"></i></a><a  href="'+url1+'" class="btn btn-success text-white" title="Send Notification"><i class=" fa fa-comment"></i></button></a>';
							
							if(status == "UnSolved"){
								editlink += '<button class="btn btn-primary  text-white sa-warning waves-effect" data-bs-toggle="modal" data-bs-target="#status_model" data-id="'+id+'" onclick="status('+id+')"> <i class="fa fa-times-circle"></i> </button>';
								
							}
							return editlink; 
						},
					}
				],
			});
			
			function status(id){
				$("#status_id").val(id);
			}

			//trigger download of data.xlsx file
			document.getElementById("download-csv").addEventListener("click", function(){
				table.download("csv", "data.csv");
			});

			//$(".change_status").on('change',function(){
			function change_status(current){
				
				$('#statusNotificationModel').find('#status').val(current.value);
				$('#statusNotificationModel').find('#complain_id').val($(current).data('id'));
				$('#statusNotificationModel').modal('show');
				
				//var status_id = $(".change_status").attr("data-id");
				//var status = $(this).val();
				
				/*$.ajax({
					url: "{{ route('admin.complaint_changestatus') }}",
					type: "POST",
					data: {
						status_id: status_id,
						status: status
					},
					headers: {
						'X-CSRF-TOKEN': "{{csrf_token()}}"
					},
					success: function(response) {
						window.location.reload();
					},
				});*/
			}
		</script>
	</body>
</html>