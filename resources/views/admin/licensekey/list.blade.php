<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <meta name="description" content="POS - Bootstrap Admin Template">
    <meta name="keywords"
        content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
    <meta name="author" content="Dreamguys - Bootstrap Admin Template">
    <meta name="robots" content="noindex, nofollow">
    <title>License Key</title>

    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('public/Admin/img/favicon.png') }}">
    <link rel="stylesheet" href="{{ asset('public/Admin/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/Admin/css/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('public/Admin/css/dataTables.bootstrap4.min.css') }}">

    <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/fontawesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/Admin/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('public/Admin/plugins/select2/css/select2.min.css') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{ asset('public/Admin/tabulator/bootstrap/tabulator_bootstrap4.css') }}">

</head>

<body>

    <div class="main-wrapper">
        @extends('layouts.header')

        @extends('layouts.sidebar')

        <div class="page-wrapper">
            <div class="content container-fluid">
                <div class="page-header">
                    <div class="row">
                        <div class="col">
                            <h3 class="page-title">License Key List</h3>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item">
                                    <a href="{{ route('admin.home') }}">{{ trans('global.dashboard') }}</a>
                                </li>
                                <li class="breadcrumb-item active">License Key List</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-header row">
                                <div class="col-lg-6">
                                    <h4 class="card-title">License Key List</h4>
                                </div>
                                <div class="col-lg-6"><a href="{{ route('admin.licenseKeyCreate') }}"
                                        class="btn btn-success" style="float: right;">+ {{ trans('global.add') }}
                                        License Key</a></div>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <button class="btn btn-outline-info waves-effect d-none" id="download-csv">Download
                                        CSV</button>
                                    <p><br /></p>

                                    <div id="data-table-licensekey"> </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="{{ asset('public/Admin/js/jquery-3.6.0.min.js') }}"></script>
    <script src="{{ asset('public/Admin/js/feather.min.js') }}"></script>
    <script src="{{ asset('public/Admin/js/jquery.slimscroll.min.js') }}"></script>
    <script src="{{ asset('public/Admin/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('public/Admin/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('public/Admin/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('public/Admin/js/script.js') }}"></script>

    <script type="text/javascript" language="javascript"
        src='https://cdn.datatables.net/buttons/2.2.3/js/dataTables.buttons.min.js' defer></script>

    <script type="text/javascript" language="javascript"
        src='https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js' defer></script>

    <script type="text/javascript" language="javascript"
        src='https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js' defer></script>

    <script type="text/javascript" language="javascript"
        src='https://cdn.datatables.net/buttons/2.2.3/js/buttons.html5.min.js' defer></script>

    <script src="{{ asset('public/Admin/tabulator/tabulator.min.js') }}"></script>
    <script src="{{ asset('public/Admin/tabulator/jspdf.min.js') }}"></script>
    <script src="{{ asset('public/Admin/tabulator/xlsx.full.min.js') }}"></script>
    <script src="{{ asset('public/Admin/tabulator/jspdf.plugin.autotable.js') }}"></script>


    <script type="text/javascript">
        $(document).ready(function() {

            var tabledata = <?php echo json_encode($items); ?>;
            console.log(tabledata);

            var table = new Tabulator("#data-table-licensekey", {
                data: tabledata, //load row data from array
                layout: "fitColumns", //fit columns to width of table
                responsiveLayout: "collapse", //hide columns that dont fit on the table
                tooltips: true, //show tool tips on cells
                addRowPos: "top", //when adding a new row, add it to the top of the table
                history: true, //allow undo and redo actions on the table
                pagination: "local", //paginate the data
                paginationSize: 10, //allow 7 rows per page of data
                movableColumns: true, //allow column order to be changed
                resizableRows: true, //allow row order to be changed
                initialSort: [ //set the initial sort order of the data
                    {
                        column: "id",
                        dir: "desc"
                    },
                ],
                columns: [{
                        title: "License key",
                        field: "licensekey",
                        editor: "input"
                    },
                    {
                        title: "Is Active",
                        field: "is_active",
                        editor: "input"
                    },
                    {
                        title: "Used By",
                        field: "used_by",
                        editor: "input"
                    },
                ]
            });

        });
    </script>
</body>

</html>
