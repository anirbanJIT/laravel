<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
      <meta name="description" content="POS - Bootstrap Admin Template">
      <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
      <meta name="author" content="Dreamguys - Bootstrap Admin Template">
      <meta name="robots" content="noindex, nofollow">
      <title>{{ trans('global.edit') }} {{ trans('cruds.brand_type.title_singular') }}</title>
      
      <link rel="shortcut icon" type="image/x-icon" href="{{ asset('public/Admin/img/favicon.png')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/bootstrap.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/animate.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/dataTables.bootstrap4.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/fontawesome.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/all.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/style.css')}}">
   </head>
   <body>
    <div id="global-loader">
        <div class="whirly-loader"> </div>
    </div>
    <div class="main-wrapper">
        @extends('layouts.header')
		@extends('layouts.sidebar')
        <div class="page-wrapper">
            <div class="content container-fluid">
				<div class="page-header">
					<div class="row">
						<div class="col">
							<h3 class="page-title">{{ trans('global.edit') }} {{ trans('cruds.brand_type.title_singular') }}</h3>
							<ul class="breadcrumb">
								<li class="breadcrumb-item"><a href="{{route('admin.home')}}">{{ trans('global.dashboard') }}</a></li>
								<li class="breadcrumb-item"><a href="{{ route('admin.brand-type.index') }}">{{ trans('cruds.brand_type.title') }}</a></li>
								<li class="breadcrumb-item active">{{ trans('global.edit') }} {{ trans('cruds.brand_type.title_singular') }}</li>
							</ul>
						</div>
                    </div>
                </div>
                <div class="row">
					<div class="col-sm-12">
						<div class="card">
							<div class="card-body">
								<form action="{{ route('admin.brand-type.update', [$brandType->id]) }}" method="POST" enctype="multipart/form-data">
									{{ csrf_field() }} 
									@method('PUT')
									<div class="form-group">
										<label>Name<span class="text-danger">*</span></label>
										<input type="text" class="form-control @error('name') is-invalid @enderror" placeholder="{{ trans('cruds.brand_type.fields.name') }}*" id="name" name="name" value="{{$brandType->name}}">
										@if($errors->has('name')) 
											<em class="invalid-feedback"> {{ $errors->first('name') }} </em> 
										@endif
									</div>
									<div class="form-group">
										<label>Upload image<span class="text-danger">*</span></label>
										<input type="file" accept="image/*" class="form-control @error('brandTypeImg') is-invalid @enderror" name="brandTypeImg" id="brandTypeImg">
										@if($errors->has('brandTypeImg')) 
											<em class="invalid-feedback"> {{ $errors->first('brandTypeImg') }} </em> 
										@endif
										<span id="preview">
											<input type="hidden" name="imageH" value="{{$brandType->image}}">
											@if(!empty($brandType->image))
												<a href="{{ asset('Admin/images/brand-types/'.$brandType->image)}}" data-lightbox="lightbox-1"> 
													<img src="{{ asset('Admin/images/brand-types/'.$brandType->image)}}" width="50">
												</a>
											@else
												{{ $brandType->image ?? '' }} 
											@endif
										</span>
									</div>
									<div class="form-group">
										<label>Download Access<span class="text-danger">*</span></label>
										<select class="form-control @error('download') is-invalid @enderror" name="download">
											<option value="" >Select Download Access</option>
											<option value="1" @if($brandType->download == 1) selected @endif>Enable</option>
											<option value="0" @if($brandType->download == 0) selected @endif>Disable</option>
										</select>
										@if($errors->has('download')) 
											<em class="invalid-feedback"> {{ $errors->first('download') }} </em> 
										@endif
									</div>
									<div class="form-group">
										<label>Meta Title</label>
										<input type="text" class="form-control" placeholder="Meta Title" id="metaTitle" name="metaTitle" value="{{$brandType->meta_title}}">
										@if($errors->has('meta_title')) 
											<em class="invalid-feedback"> {{ $errors->first('meta_title') }} </em> 
										@endif
									</div>
									<div class="form-group">
										<label>Meta Keyword</label>
										<input type="text" class="form-control"  placeholder="Meta Keyword" id="metaKeyword" name="metaKeyword" value="{{$brandType->meta_keywords}}">
										@if($errors->has('meta_keywords')) 
											<em class="invalid-feedback">{{ $errors->first('meta_keywords') }}</em>
										@endif
									</div>
									<div class="form-group">
										<label>Meta Description</label>
										<input type="text" class="form-control"  placeholder="Meta Keyword" id="metaDescription" name="metaDescription" value="{{$brandType->meta_description}}">
										@if($errors->has('meta_description')) 
											<em class="invalid-feedback"> {{ $errors->first('meta_description') }}</em>
										@endif
									</div>
									<div>
										<button type="submit" class="btn btn-success">Save</button>
									</div>
								</form>
							</div>
						</div>
					</div>
                </div>
            </div>
         </div>
    </div>
    <script src="{{ asset('public/Admin/js/jquery-3.6.0.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/feather.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.slimscroll.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{ asset('public/Admin/plugins/apexchart/apexcharts.min.js')}}"></script>
    
    <script src="{{ asset('public/Admin/js/script.js')}}"></script>
	<script>
		
		$('#brandTypeImg').change(function(){
			const file = this.files[0];
			
			console.log(file);
			if (file){
			let reader = new FileReader();
			reader.onload = function(event){
				console.log(event.target.result);
				$("#preview").html('<img id="imgPreview" hight="50px" width="50px" src="'+event.target.result+'" alt="pic" />');
				//$('#imgPreview').attr('src', );
			}
			reader.readAsDataURL(file);
			}
		});
		

	</script>
   </body>
</html>