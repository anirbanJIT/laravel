@extends('layouts.admin')
@section('content')


<div class="breadcomb-area">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="breadcomb-list">
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<div class="breadcomb-wp">
									<div class="breadcomb-icon">
										<i class="notika-icon notika-windows"></i>
									</div>
									<div class="breadcomb-ctn">
										<h2>{{ trans('cruds.brands.title_singular') }} {{ trans('global.list') }}</h2>
										<p>Show <span class="bread-ntd">all list</span></p>
									</div>
								</div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-3">
								<div class="breadcomb-report">
								
									 <a class="btn btn-success notika-btn-success waves-effect" href="{{ route('admin.brands.create') }}">
                {{ trans('global.add') }} {{ trans('cruds.brands.title_singular') }}
            </a>
									
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
 <!-- Data Table area Start-->
    <div class="data-table-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="data-table-list">
                        <!--<div class="basic-tb-hd">
                            <h2>Basic Example</h2>
                            <p>It's just that simple. Turn your simple table into a sophisticated data table and offer your users a nice experience and great features without any effort.</p>
                        </div>-->
                        <div class="table-responsive">
                            <table id="data-table-basic" class="table table-striped">
                                <thead>
                                    <tr>
                         <th width="10">
                            
                        </th>
                        <th>
                           Title
                        </th>
                        <th>
                            Brand Type
                        </th>
                       
                        <th>
                           Image
                        </th>
                                    </tr>
                                </thead>
                                <tbody>
								
                                    @foreach($brands as $key => $brand)
                        <tr data-entry-id="{{ $brand->id }}">
							<td></td>
                            <td>
                                {{ $brand->name ?? '' }}
                            </td>
                            <td>
                                {{ $brand->BrandCatName ?? '' }}
                            </td>
                            <td>
                               @if(!empty($brand->image))
								<img src="{{ asset('images/brands/'.$brand->image)}}" width="100">
							    @else
								{{ $brand->image ?? '' }}	
							    @endif
                            </td>
                           
                            <td>
                               
							   {{-- <a  href="{{ route('admin.brand.show', $brand->id) }}"><button class="btn btn-success success-icon-notika btn-reco-mg btn-button-mg waves-effect"><i class="notika-icon notika-menus"></i></button></a>  --}}

								<a  href="{{ route('admin.brands.edit', $brand->id) }}"><button class="btn btn-info info-icon-notika btn-reco-mg btn-button-mg waves-effect"><i class="notika-icon notika-edit"></i></button></a>
								<a  href="{{ url('admin/brand/delete', $brand->id) }}" class="sa-warning"><button class="btn btn-danger danger-icon-notika btn-reco-mg btn-button-mg waves-effect"><i class="notika-icon notika-close"></i></button></a>
							
        
                            </td>
                                    </tr>
                                   @endforeach
                                </tbody>
                                <tfoot>
                                    
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
<script>
   $('.sa-warning').on('click', function (e) {
    event.preventDefault();
    const url = $(this).attr('href');
    swal({
        title: "Are you sure?",   
			text: "Once Deleted. You will also able to recover this again!",   
			type: "warning",   
			showCancelButton: true,   
			confirmButtonText: "Yes, delete it!",
    }).then(function(value) {
        if (value) {
            window.location.href = url;
        }
    });
});
 //Warning Message
/*	$('.sa-warning').on('click', function(){
		swal({   
			title: "Are you sure?",   
			text: "You will not be able to recover this imaginary file!",   
			type: "warning",   
			showCancelButton: true,   
			confirmButtonText: "Yes, delete it!",
		}).then(function(){
			swal("Deleted!", "Your imaginary file has been deleted.", "success"); 
		});
	}); */
</script>			
@endpush
