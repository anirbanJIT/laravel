@extends('layouts.admin')
@section('content')
<div class="breadcomb-area">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="breadcomb-list">
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<div class="breadcomb-wp">
									<div class="breadcomb-icon">
										<i class="notika-icon notika-windows"></i>
									</div>
									<div class="breadcomb-ctn">
										<h2>{{ trans('cruds.brand_type.title_singular') }} {{ trans('global.list') }}</h2>
										<p>Show <span class="bread-ntd">all list</span></p>
									</div>
								</div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-3">
								<div class="breadcomb-report">								
									<a class="btn btn-success notika-btn-success waves-effect" href="{{ route('admin.brand-type.create') }}">{{ trans('global.add') }} {{ trans('cruds.brand_type.title_singular') }}</a>									
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
 <!-- Data Table area Start-->
    <div class="data-table-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="data-table-list">
                        <div class="table-responsive">
                            <table id=""  class="datatable-Brand table table-striped">
                                <thead>
                                    <tr>
                                        <th width="10">
                                            </th>
                                            <th>
                                                ID
                                            </th>
                                            <th>
                                               {{ trans('cruds.brand_type.fields.name') }}
                                            </th>
                                            <th>
                                                Order
                                            </th>
                                            <th>
                                                 Image
                                            </th>
                                            <th>
                                                Status
                                            </th>
                                            <th>
                                                &nbsp;
                                            </th>
                                        </tr>
                                </thead>
                                <tbody>
                                    @foreach($brandTypes as $key => $brandType)
                                    <tr data-entry-id="{{ $brandType->id }}">
                                        <td>
                                        </td>
                                        <td>
                                            {{ $brandType->id }}
                                        </td>
                                        <td>
                                            {{ $brandType->name ?? '' }}
                                        </td>
                                        <td>
                                            {{ $brandType->order_number ?? $brandType->id }}
                                        </td>
                                        <td>@if($brandType->image)
                                            <image src="{{URL::asset('images/brand-types/'.$brandType->image)}}" style="width:70px;height:70px;">
            							@endif            								
                                        </td>
                                        <td>
                                            <div class="nk-toggle-switch" data-ts-color="green">
                                                <input class="status_change" data-id="{{$brandType->id}}" id="ts{{$brandType->id}}" type="checkbox" hidden="hidden" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" data-off="InActive" {{ $brandType->status ? 'checked' : '' }}>
                                                <label for="ts{{$brandType->id}}" class="ts-helper"></label>
                                            </div>
                                        </td>
                                        <td>
                                            <a  href="{{ route('admin.brand-type.edit', $brandType->id) }}"><button class="btn btn-info info-icon-notika btn-reco-mg btn-button-mg waves-effect" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Edit"><i class="notika-icon notika-edit"></i></button></a>
                                            <a  href="{{ url('admin/brand-type/delete', $brandType->id) }}" class="sa-warning"><button class="btn btn-danger danger-icon-notika btn-reco-mg btn-button-mg waves-effect" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Delete"><i class="notika-icon notika-close"></i></button></a>
                                        </td>
                                    </tr>
                                   @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th width="10">
                                        </th>
                                        <th>
                                            ID
                                        </th>
                                        <th>
                                            {{ trans('cruds.brand_type.fields.name') }}
                                        </th>
                                        <th>
                                            Order
                                        </th>
                                        <th>
                                           Image
                                        </th>
                                        <th>
                                            Status
                                        </th>
                                        <th>
                                            &nbsp;
                                        </th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
<script>
$(document).ready(function(){
    $('.sa-warning').on('click', function (e) {
        e.preventDefault();
        const url = $(this).attr('href');
        swal({
            title: "Are you sure?",   
                text: "Once Deleted. You will also able to recover this again!",   
                type: "warning",   
                showCancelButton: true,   
                confirmButtonText: "Yes, delete it!",
        }).then(function(value) {
            if (value) {
                window.location.href = url;
            }
        });
    });
	$('.status_change').change(function() {		
        var status = $(this).prop('checked') == true ? 1 : 0; 
        var model_id = $(this).data('id');		
		 $.ajax({
           type:"GET",
           url:"{{url('admin/changeStatusBrandType')}}?status="+status+"&brnadType_id="+model_id,
           success:function(res){               
            if(res){
                console.log(res.success);
            }
           }
        });
	
    });
});
</script>		
<script type="text/javascript">
$(document).ready(function (){
    /* @author Naren Thaker @narenthaker.com 101220201500 */
    window._token = $('meta[name="csrf-token"]').attr('content');

    $('.select-all').click(function () {
      let $select2 = $(this).parent().siblings('.select2')
      $select2.find('option').prop('selected', 'selected')
      $select2.trigger('change')
    })
    $('.deselect-all').click(function () {
      let $select2 = $(this).parent().siblings('.select2')
      $select2.find('option').prop('selected', '')
      $select2.trigger('change')
    })

    $.extend(true, $.fn.dataTable.Buttons.defaults.dom.button, { className: 'btn' })

    $.extend(true, $.fn.dataTable.defaults, {
        columnDefs: [{
            orderable: false,
            className: 'select-checkbox',
            checkboxes: {
               'selectRow': true
            },
            selector: 'td:first-child',
            targets: 0
        }, {
            orderable: false,
            searchable: false,
            targets: [-3,-2,-1]
        }],
        order: [[ 1, 'desc' ]],
        lengthMenu: [
            [ 10, 25, 50, 100, -1 ],
            [ '10', '25', '50', '100', 'All' ]
        ],
        buttons: [],
        dom: 'lBfrtip<"actions">',
    });

    let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons);
    let deleteButtonTrans = '{{ trans('global.datatables.delete') }}';
    let deleteButton = {
        text: deleteButtonTrans,
        url: "{{ route('admin.brandtype.mass_destroy') }}",
        className: 'btn btn-success notika-btn-success waves-effect',
        action: function (e, dt, node, config) {
          var ids = [];
          $("input:checkbox[class=dt-checkboxes]:checked").each(function () {
             ids.push( $(this).parent().parent().data('entry-id')  );
          });

          if (ids.length === 0) {
            alert('{{ trans('global.datatables.zero_selected') }}')
            return
          }

          if (confirm('{{ trans('global.areYouSure') }}')) {
            $.ajax({
              headers: {'x-csrf-token': _token},
              method: 'POST',
              url: config.url,
              data: { ids: ids, _method: 'DELETE' }})
              .done(function () { location.reload() })
          }
        }
    }
    dtButtons.push(deleteButton);

    var table = $('.datatable-Brand:not(.ajaxTable)').DataTable({
        buttons: dtButtons,
        initComplete: function() {
            this.api().columns([2]).every(function(d) {
                var column = this;
                var theadname = $('#DataTables_Table_0 th').eq([d]).text();
                var select = $('<select class ="filter"><option value="">'+theadname+': All</option></select>')
                             .appendTo($(column.footer()).empty())
                             .tailselect({ search: true, deselect: true, width: 130, placeholder: theadname});
                             
                select.on("change", function(){
                    var val = $.fn.dataTable.util.escapeRegex(this.value());
                    column.search(val? ('^' + val + '$'): '', true, false).draw();
                });
                
                column.data().unique().sort().each(function(d, j) {
                    select.options.add(d, d, '#');
                });
                select.query();
            });
        }
    });
    table.on('draw', function() {
        table.columns([2]).indexes().each(function(idx) {
            var select = $(table.column(idx).footer()).find('select').tailselect('.select');

            if (select.value() === null) {
                select.reload();
                
                table.column(idx, {
                    search: 'applied'
                }).data().unique().sort().each(function(d, j) {
                    select.options.add(d, d);
                });

                select.query();
            }
        });
    });

    /*$('.datatable-Brand:not(.ajaxTable)').DataTable({ 
        buttons: dtButtons,
        initComplete: function () {
            this.api().columns([2]).every( function (d) { 
                var column = this;
                var theadname = $('#DataTables_Table_0 th').eq([d]).text();
                var select = $('<select class="mx-1"><option value="">'+theadname+': All</option></select>')
                    .appendTo( '#DataTables_Table_0_filter' )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );

                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
                column.data().unique().sort().each( function ( d, j ) {
                    var val = $('<div/>').html(d).text();
                    select.append( '<option value="'+val+'">'+val+'</option>' )
                } );
            } );
        } 
    })*/

    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
        $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust();
    });

});
</script>
@endpush
