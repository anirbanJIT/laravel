<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
      <meta name="description" content="POS - Bootstrap Admin Template">
      <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
      <meta name="author" content="Dreamguys - Bootstrap Admin Template">
      <meta name="robots" content="noindex, nofollow">
      <title>{{ trans('cruds.brand_type.title_singular') }} {{ trans('global.list') }}</title>
      
      <link rel="shortcut icon" type="image/x-icon" href="{{ asset('public/Admin/img/favicon.png')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/bootstrap.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/animate.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/dataTables.bootstrap4.min.css')}}">

      <!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.7.1/css/buttons.dataTables.min.css"> -->
      
       

      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/fontawesome.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/all.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/style.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/select2/css/select2.min.css')}}">
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <link rel="stylesheet" href="{{ asset('public/Admin/tabulator/bootstrap/tabulator_bootstrap4.css')}}">
		
   </head>
   <body>
      <!-- <div id="global-loader">
         <div class="whirly-loader"> </div>
      </div> -->
      <div class="main-wrapper">
        @extends('layouts.header')
     
        @extends('layouts.sidebar')
        
        <div class="page-wrapper">
            <div class="content container-fluid">
               <div class="page-header">
                  <div class="row">
                     <div class="col">
                        <h3 class="page-title">{{ trans('cruds.brand_type.title_singular') }} {{ trans('global.list') }}</h3>
                        <ul class="breadcrumb">
                           <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">{{ trans('global.dashboard') }}</a></li>
                           <li class="breadcrumb-item active">{{ trans('cruds.brand_type.title_singular') }} {{ trans('global.list') }}</li>
                        </ul>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-12">
                     <div class="card">
                        <div class="card-header row">
                            <div class="col-lg-6"><h4 class="card-title">{{ trans('cruds.brand_type.title_singular') }} {{ trans('global.list') }}</h4></div>
                            <div class="col-lg-6">
								<a href="{{ route('admin.brand-type.create') }}" class="btn btn-success" style="float: right;margin-left: 2%;">+ {{ trans('global.add') }} {{ trans('cruds.brand_type.title_singular') }}</a>
								<a id="openmodel" class="btn btn-success" style="float: right;">Rearrange</a>
							</div>
                        </div>
                        <div class="card-body">
                            <button style="margin-bottom: 10px" class="btn btn-primary delete_all" data-url="{{ route('admin.brandtype.mass_destroy') }}">Delete All Selected</button> 
							
                           <div class="table-responsive">
								<div id="data-table-brandType-list" ></div>
								<input type="hidden" name="is_delete" value="0" id="is_delete">
								 
								<div class="modal fade" id="dragdrop_model"  role="dialog" >
									<div class="modal-dialog modal-dialog-centered" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title">Modal title</h5>
												<button type="button" class="close" data-bs-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>
											</div>
											<div class="modal-body">
												<table class="table table-bordered">
													<tr>
														<th>Id</th>
														<th>Image</th>
														<th>Name</th>
													</tr>
													<tbody class="row_position">
														@foreach($brandTypes as $row)
															<tr  id="{{ $row->id }}">
																<td>{{ $row->id }}</td>
																<td><img src="{{ URL::asset('public/Admin/images/brand-types/'.$row->image) }}" height="50px" width="50px"/></td>
																<td>{{ $row->name }}</td>
															</tr>
														@endforeach
													</tbody>
												</table>	
											</div>
											<div class="modal-footer">
												<button type="button" onclick="updateOrder()" class="btn btn-primary">Save changes</button>
												<button type="button" data-bs-dismiss="modal" class="btn btn-secondary">Close</button>
											</div>
										</div>
									</div>
								</div>
								
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
    <script src="{{ asset('public/Admin/js/jquery-3.6.0.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/feather.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.slimscroll.min.js')}}"></script>
  
    <script src="{{ asset('public/Admin/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/script.js')}}"></script>
    <script src="{{ asset('public/Admin/tabulator/tabulator.min.js') }}"></script>
    <script src="{{ asset('public/Admin/tabulator/jspdf.min.js') }}"></script>
    <script src="{{ asset('public/Admin/tabulator/xlsx.full.min.js') }}"></script>
    <script src="{{ asset('public/Admin/tabulator/jspdf.plugin.autotable.js') }}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script type="text/javascript">
		$( ".row_position" ).sortable({
			delay: 150,
			stop: function() {
				var selectedData = new Array();
				$('.row_position>tr').each(function() {
					selectedData.push($(this).attr("id"));
				});
				
			}
		});
		function updateOrder() {
			var selectedData = new Array();
			$('.row_position>tr').each(function() {
				selectedData.push($(this).attr("id"));
			});
			
			var edit_url = '{{ route("admin.brandTypeOrderPosition") }}';
            	  
			$.ajax({
				url:edit_url,
				type:'post',
				data:{position:selectedData},
				headers: {
						'X-CSRF-TOKEN': "{{csrf_token()}}"
					},
				success:function(){
					alert('your change successfully saved');
					window.location.reload();
				}
			});
		}
		
		var tabledata = <?php echo json_encode($brandTypes); ?>;
		var url = "{{URL::asset('public/Admin/images/brand-types/')}}"+'/';
      
		function customFilter(data){
          return data.car && data.rating < 3;
		}

		function updateFilter(){
			var filter = $("#filter-field").val() == "function" ? customFilter : $("#filter-field").val();
			if($("#filter-field").val() == "function" ){
              $("#filter-type").prop("disabled", true);
              $("#filter-value").prop("disabled", true);
			}else{
              $("#filter-type").prop("disabled", false);
              $("#filter-value").prop("disabled", false);
			}
			table.setFilter(filter, $("#filter-type").val(), $("#filter-value").val());
		}

		//Update filters on value change
		$("#filter-field, #filter-type").change(updateFilter);
		$("#filter-value").keyup(updateFilter);
		
		$("#openmodel").click(function(){
		  $("#dragdrop_model").modal('show');
		});
		var checkboxIcon = function(cell, formatterParams){ //plain text value
			return '<input class="sub_chk" type="checkbox" onclick="$(\'#is_delete\').val(1)" data-id="'+cell.getData().id+'">';
		};
		var table = new Tabulator("#data-table-brandType-list", {
			data:tabledata,           //load row data from array
			layout:"fitColumns",      //fit columns to width of table
			responsiveLayout:"collapse",  //hide columns that dont fit on the table
			tooltips:true,            //show tool tips on cells
			addRowPos:"top",          //when adding a new row, add it to the top of the table
			history:true,             //allow undo and redo actions on the table
			pagination:"local",       //paginate the data
			paginationSize:10,         //allow 7 rows per page of data
			movableColumns:true,      //allow column order to be changed
			resizableRows:true,       //allow row order to be changed
			/* movableRows: true, //enable movable rows
     		rowMoved:function(row){
				$("#dragdrop_model").modal('show');
			}, */
			columns: [
				{title:'<input id="master" type="checkbox" onclick="$(\'#is_delete\').val(1)">', formatter:checkboxIcon, width:40, align:"center", cellClick:function(e, cell){cell.getRow().toggleSelect();},headerSort:false},
				/* {formatter:"rowSelection", titleFormatter:"rowSelection", cssClass:"custom-cell", hozAlign:"center",headerSort:false, 
					cellClick:function(e, cell){cell.getRow().toggleSelect();}
				}, */
				{title:"Image", field:"image", formatter:"image", 
					formatterParams:{height:"50px",width:"50px",urlPrefix:url,urlSuffix:"",},headerSort:false,
				},
				/* {title:"Category ID", field:"id", editor:"label",headerFilter:"input",headerSort:false,				headerFilterPlaceholder:"Category Id",
			    }, */
				{title:"Category Name", field:"name", editor:"label",headerFilter:"input",headerSort:false,			headerFilterPlaceholder:"Category Name"},
				{title:"Order", field:"position_order", editor:"label",headerFilter:"input",headerSort:true,		headerFilterPlaceholder:"Order"},
        	    /* {title:"Date", field:"created_at", editor:"label",headerFilter:"input",headerSort:false,			headerFilterPlaceholder:"Date"}, */
				{title:"Status", field:"id_status", headerSort:false,
					formatter:function(cell, formatterParams, onRendered){
						var arr = cell.getValue();
						var data = arr.split("_");
						if(data[1] == '1'){
							return '<div class="status-toggle d-flex justify-content-between align-items-center" onclick="$(\'#is_delete\').val(1)"><input type="checkbox" class="check status_change" data-id="'+data[0]+'" id="ts'+data[0]+'" data-on="Active"  checked ><label for="ts'+data[0]+'" class="checktoggle">checkbox</label></div>';
						} else {
							return '<div class="status-toggle d-flex justify-content-between align-items-center" onclick="$(\'#is_delete\').val(1)"><input type="checkbox" class="check status_change" data-id="'+data[0]+'" id="ts'+data[0]+'" data-off="InActive"><label for="ts'+data[0]+'" class="checktoggle">checkbox</label></div>';
						}            
					}
				},
				{title:"Action",width:170, field:"id", headerSort:false,
					formatter:function(cell, formatterParams, onRendered) { 
						var view_url = '{{ route("admin.brand-type.show",":id") }}';
						view_url = view_url.replace(':id', cell.getValue());

						var edit_url = '{{ route("admin.brand-type.edit",":id") }}';
						edit_url = edit_url.replace(':id', cell.getValue());

						var delete_url = '{{ route("admin.brand-type.destroy",":id") }}';
						delete_url = delete_url.replace(':id', cell.getValue());
						var msg = "Are you sure you want to delete this Category?";
						var token = '{{ csrf_token() }}';
						
						var editlink = '<a href="'+view_url+'" class="btn btn-primary text-white"> <i class="fa fa-eye"></i></a><a href="'+edit_url+'" class="btn btn-success text-white"><i class="fa fa-edit"></i></a> <form action="'+delete_url+'" method="POST" onclick="$(\'#is_delete\').val(1)" onsubmit="return confirm(\''+msg+'\');" style="display: inline-block;"><input type="hidden" name="_method" value="DELETE"><input type="hidden" name="_token" value="'+token+'"><button type="submit" class="btn btn-danger danger-icon-notika btn-reco-mg btn-button-mg waves-effect"> <i class="fa fa-trash"></i></button></form>';
                  
						return editlink; 
					},
				},
			],
			rowClick: function (e, row) {
            	var is_delete = $("#is_delete").val();
				if(is_delete == 0){
					/* setTimeout(function(){ */
						var link_url= '{{ route('admin.brands.show',":id") }}';
						link_url = link_url.replace(':id', row.getData().id);
						window.location.href = link_url;
					/*},200); */
					
				}
			}
		});
		
		$('.status_change').change(function() 
         {      
			$('#is_delete').val(1);	
            var status = $(this).prop('checked') == true ? 1 : 0; 
            var model_id = $(this).data('id');  
					
             $.ajax({
                 type:"GET",
                 url:"{{url('admin/changeStatusBrandType')}}?status="+status+"&brandType_id="+model_id,
                 success:function(res){               
                  if(res){
					  $('#is_delete').val(0);
                      console.log(res.success);
                  }
                 }
              });
   
         });
		
		
		$('#master').on('click', function(e) {  
			 if($(this).is(':checked',true))    
			 {  
				$(".sub_chk").prop('checked', true);    
			 } else {    
				$(".sub_chk").prop('checked',false);    
			 }    
        });  
		
		 $('.delete_all').on('click', function(e) {  
			/* $("input[type='checkbox']").prop('checked', true);     */
			 
            var allVals = [];    
            $(".sub_chk:checked").each(function() {    
                allVals.push($(this).attr('data-id'));  
            }); 

			
            if(allVals.length <=0)    
            {    
                alert("Please select Category row.");    
            }  else {    
  
                var check = confirm("Are you sure you want to delete this Category?");    
                if(check == true){    
  
                    var join_selected_values = allVals.join(",");   
					console.log(join_selected_values);
                    $.ajax({  
                        url: $(this).data('url'),  
                        type: 'DELETE',  
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},  
                        data: 'ids='+join_selected_values,  
                        success: function (data) {  
                            if (data['success']) {  
                                $("input[type='checkbox']:checked").each(function() {    
                                    $(this).parents("tr").remove();  
                                });  
                                alert(data['success']); 
								window.location.reload();
                            } else if (data['error']) {  
                                alert(data['error']);  
                            } else {  
                                alert('Whoops Something went wrong!!');  
                            }  
                        },  
                        error: function (data) {  
                            alert(data.responseText);  
                        }  
                    });   
  
                  $.each(allVals, function( index, value ) {  
                      $('table tr').filter("[data-row-id='" + value + "']").remove();  
                  });  
                }    
            }    
        }); 


		//trigger download of data.xlsx file
		/* document.getElementById("download-csv").addEventListener("click", function(){
			table.download("csv", "data.csv");
		}); */
	</script>
   </body>
</html>