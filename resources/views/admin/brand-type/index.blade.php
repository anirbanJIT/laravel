<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
      <meta name="description" content="POS - Bootstrap Admin Template">
      <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
      <meta name="author" content="Dreamguys - Bootstrap Admin Template">
      <meta name="robots" content="noindex, nofollow">
      <title>{{ trans('cruds.brand_type.title_singular') }} {{ trans('global.list') }}</title>
      
      <link rel="shortcut icon" type="image/x-icon" href="{{ asset('public/Admin/img/favicon.png')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/bootstrap.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/animate.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/dataTables.bootstrap4.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/fontawesome.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/all.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/style.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/select2/css/select2.min.css')}}">
      <meta name="csrf-token" content="{{ csrf_token() }}">
   </head>
   <body>
      <!-- <div id="global-loader">
         <div class="whirly-loader"> </div>
      </div> -->
      <div class="main-wrapper">
        @extends('layouts.header')
     
        @extends('layouts.sidebar')
        
        <div class="page-wrapper">
            <div class="content container-fluid">
               <div class="page-header">
                  <div class="row">
                     <div class="col">
                        <h3 class="page-title">{{ trans('cruds.brand_type.title_singular') }} {{ trans('global.list') }}</h3>
                        <ul class="breadcrumb">
                           <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">{{ trans('global.dashboard') }}</a></li>
                           <li class="breadcrumb-item active">{{ trans('cruds.brand_type.title_singular') }} {{ trans('global.list') }}</li>
                        </ul>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-12">
                     <div class="card">
                        <div class="card-header row">
                            <div class="col-lg-6"><h4 class="card-title">{{ trans('cruds.brand_type.title_singular') }} {{ trans('global.list') }}</h4></div>
                            <div class="col-lg-6"><a href="{{ route('admin.brand-type.create') }}" class="btn btn-success" style="float: right;">+ {{ trans('global.add') }} {{ trans('cruds.brand_type.title_singular') }}</a></div>
                        </div>
                        <div class="card-body">
                           <button style="margin-bottom: 10px" class="btn btn-primary delete_all" data-url="{{ route('admin.brandtype.mass_destroy') }}">Delete All Selected</button>  
                           <div class="table-responsive">
                              <table class="table  datanew datatable-Brand" id="datatable-Brand">
                                 <thead>
                                    <tr>
                                       <th><input type="checkbox" id="master"></th>
                                       <th> {{ trans('cruds.brand_type.fields.image') }} </th>
                                       <th> Category {{ trans('cruds.brand_type.fields.id') }} </th>
                                       <th> {{ trans('cruds.brand_type.title_singular') }} {{ trans('cruds.brand_type.fields.name') }} </th>
                                       <th> {{ trans('cruds.brand_type.fields.order') }} </th>
                                       <th> {{ trans('cruds.brand_type.fields.date') }} </th>
                                       <th> {{ trans('cruds.brand_type.fields.status') }} </th>
                                       <th> {{ trans('cruds.brand_type.fields.actions') }} </th>
                                    </tr>

                                 </thead>
                                 <tbody>
                                     @foreach($brandTypes as $key => $brandType)
                                    <tr data-entry-id="{{ $brandType->id }}">
                                       <td><input type="checkbox" class="sub_chk" data-id="{{$brandType->id}}"></td>
                                       <td>
                                    @if(isset($brandType->image))
                                        <image src="{{URL::asset('Admin/images/brand-types/'.$brandType->image)}}" width="60" height="60"/>
                                    @else
                                        <image src="{{URL::asset('Admin/images/no-image-available.png')}}" width="60" height="60"/>
                                    @endif 
                                    </td>
                                    <td>{{ $brandType->id }}</td>
                                    <td>{{ $brandType->name ?? '' }}</td>
                                    <td>{{ $brandType->order_number ?? $brandType->id }}</td>
                                    <td data-order="{{ \Carbon\Carbon::parse($brandType->created_at)->format('Y-m-d') ?? '' }}">{{ \Carbon\Carbon::parse($brandType->created_at)->format('d/m/Y') ?? '' }}</td>
                                    <td data-filter="{{ $brandType->status ?? '' }}" data-search="{{ $brandType->status ?? '' }}">
                                       
                                       <div class="status-toggle d-flex justify-content-between align-items-center">
                                          <input type="checkbox" class="check status_change" data-id="{{$brandType->id}}" id="ts{{$brandType->id}}" data-on="Active" data-off="InActive" {{ $brandType->status ? 'checked' : '' }}>
                                          <label for="ts{{$brandType->id}}" class="checktoggle">checkbox</label>
                                       </div>
                                       
                                       <!-- <div class="nk-toggle-switch" data-ts-color="green">
                                            <input class="status_change" data-id="{{$brandType->id}}" id="ts{{$brandType->id}}" type="checkbox" hidden="hidden" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" data-off="InActive" {{ $brandType->status ? 'checked' : '' }}>
                                            <label for="ts{{$brandType->id}}" class="ts-helper"></label>
                                        </div> -->
                                    </td>
                                    <td>
                                        <a href="{{ route('admin.brand-type.edit', $brandType->id) }}" class="btn btn-success text-white" data-toggle="tooltip" data-placement="bottom" title="Edit"><i class="fa fa-edit"></i></a>
                                        
                                        <a  href="{{ url('admin/brand-type/delete', $brandType->id) }}" class="btn btn-danger text-white" data-toggle="tooltip" onclick="return confirm('{{ trans('global.areYouSure') }}');" data-placement="bottom" title="Delete"><i class="fa fa-trash"></i></a>
                                       </td>
                                    </tr>
                                    @endforeach
                                    
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
    <script src="{{ asset('public/Admin/js/jquery-3.6.0.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/feather.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.slimscroll.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/script.js')}}"></script>
    
    <!-- <script type="text/javascript" language="javascript" src='https://cdn.datatables.net/buttons/2.2.3/js/dataTables.buttons.min.js' defer></script>
      
    <script type="text/javascript" language="javascript" src='https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js' defer></script>
      
    <script type="text/javascript" language="javascript" src='https://cdn.datatables.net/buttons/2.2.3/js/buttons.html5.min.js' defer></script> -->
      
      <script type="text/javascript">
         $('.status_change').change(function() 
         {      
            var status = $(this).prop('checked') == true ? 1 : 0; 
            var model_id = $(this).data('id');      
             $.ajax({
                 type:"GET",
                 url:"{{url('admin/changeStatusBrandType')}}?status="+status+"&brnadType_id="+model_id,
                 success:function(res){               
                  if(res){
                      console.log(res.success);
                  }
                 }
              });
   
         });

         $(document).ready(function () {  
  
        $('#master').on('click', function(e) {  
         if($(this).is(':checked',true))    
         {  
            $(".sub_chk").prop('checked', true);    
         } else {    
            $(".sub_chk").prop('checked',false);    
         }    
        });  
  
        $('.delete_all').on('click', function(e) {  
  
            var allVals = [];    
            $(".sub_chk:checked").each(function() {    
                allVals.push($(this).attr('data-id'));  
            });    
  
            if(allVals.length <=0)    
            {    
                alert("Please select row.");    
            }  else {    
  
                var check = confirm("Are you sure you want to delete this row?");    
                if(check == true){    
  
                    var join_selected_values = allVals.join(",");   
  
                    $.ajax({  
                        url: $(this).data('url'),  
                        type: 'DELETE',  
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},  
                        data: 'ids='+join_selected_values,  
                        success: function (data) {  
                            if (data['success']) {  
                                $(".sub_chk:checked").each(function() {    
                                    $(this).parents("tr").remove();  
                                });  
                                alert(data['success']);  
                            } else if (data['error']) {  
                                alert(data['error']);  
                            } else {  
                                alert('Whoops Something went wrong!!');  
                            }  
                        },  
                        error: function (data) {  
                            alert(data.responseText);  
                        }  
                    });  
  
                  $.each(allVals, function( index, value ) {  
                      $('table tr').filter("[data-row-id='" + value + "']").remove();  
                  });  
                }    
            }    
        });  
  
        $('[data-toggle=confirmation]').confirmation({  
            rootSelector: '[data-toggle=confirmation]',  
            onConfirm: function (event, element) {  
                element.trigger('confirm');  
            }  
        });  
  
        $(document).on('confirm', function (e) {  
            var eele = e.target;  
            e.preventDefault();  
  
            $.ajax({  
                url: ele.href,  
                type: 'DELETE',  
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},  
                success: function (data) {  
                    if (data['success']) {  
                        $("#" + data['tr']).slideUp("slow");  
                        alert(data['success']);  
                    } else if (data['error']) {  
                        alert(data['error']);  
                    } else {  
                        alert('Whoops Something went wrong!!');  
                    }  
                },  
                error: function (data) {  
                    alert(data.responseText);  
                }  
            });  
  
            return false;  
        });  
    });  
      </script>


   </body>
</html>