<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
      <meta name="description" content="POS - Bootstrap Admin Template">
      <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
      <meta name="author" content="Dreamguys - Bootstrap Admin Template">
      <meta name="robots" content="noindex, nofollow">
      <title>{{ trans('cruds.bulkupload.title_singular') }}</title>
      
      <link rel="shortcut icon" type="image/x-icon" href="{{ asset('public/Admin/img/favicon.png')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/bootstrap.min.css')}}">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
      <link rel="stylesheet" href="{{ asset('public/Admin/css/animate.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/dataTables.bootstrap4.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/fontawesome.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/all.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/style.css')}}">


   </head>
   <body>
      <div id="global-loader">
         <div class="whirly-loader"> </div>
      </div>
      <div class="main-wrapper">
        @extends('layouts.header')
     
        @extends('layouts.sidebar')
        
        <div class="page-wrapper">
            <div class="content container-fluid">
               <div class="page-header">
                  <div class="row">
                     <div class="col">
                        <h3 class="page-title">{{ trans('cruds.bulkupload.title_singular') }}</h3>
                        <ul class="breadcrumb">
                           <li class="breadcrumb-item"><a href="{{route('admin.home')}}">{{ trans('global.dashboard') }}</a></li>
                           <li class="breadcrumb-item active">{{ trans('cruds.bulkupload.title_singular') }}</li>
                        </ul>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-12">
                     <div class="card">
                        <!-- <div class="card-header row">
                            <div class="col-lg-6"><h4 class="card-title">{{ trans('global.create') }} {{ trans('cruds.brands.title_singular') }}</h4></div>
                            
                        </div> -->
                        <div class="card-body">
                           
                           <div class="form-group">
                              <label>Download Sample File</label>
                              <select class="form-control" name="sample_file_download_id" id="sample_file_download_id" onChange="download(this.value)">
                                 <option value="">Please Select</option>
                                 <option value="category.csv">Category</option>
                                 <option value="brand.csv">Brand</option>
                                 <option value="model.csv">Model</option>
                                 <option value="sub_model.csv">Sub Model</option>
                                 <option value="model_screen.csv">Model Screen</option>
                             </select>
                           </div>

                           <hr/>

                           <form action="{!! route('admin.bulkupload.store') !!}" method="POST" enctype="multipart/form-data" id="frm_importcsv" novalidate="">
                            {{ csrf_field() }}
                                <div class="row">
                              
                                  <div class="form-group col-lg-3">
                                     <label>Import Data*</label>
                                     <select class="form-control" name="sample_file_id" id="sample_file_id"  >
                                         <option value="">Please Select</option>
                                            <option value="category" @if(old('sample_file_id') == "category")selected @endif>Category</option>
                                            <option value="brand" @if(old('sample_file_id') == "brand")  selected @endif>Brand</option>
                                            <option value="model" @if(old('sample_file_id') == "model") selected @endif>Model</option>
                                            <option value="sub_model" @if(old('sample_file_id') == "sub_model" ) selected @endif>Sub Model</option>
                                            <option value="modelscreen" @if(old('sample_file_id') == "modelscreen") selected @endif>Model Screen</option>
                                     </select>
                                     @if($errors->has('sample_file_id'))
                                    <em class="invalid-feedback">{{ $errors->first('sample_file_id') }}</em>
                                    @endif
                                  </div>

                              <div class="form-group col-lg-3">
                                 <label>Action*</label>
                                    <select class="form-control" name="action_type" id="action_type" required="">
                                        <option value="add" @if(old('action_type') == 'add') selected @endif>Add New Data</option>
                                        <option value="edit" @if(old('action_type') == 'edit' ) selected @endif>Update Data</option>
                                    </select>
                                 @if($errors->has('action_type'))
                                <em class="invalid-feedback">{{ $errors->first('action_type') }}</em>
                                @endif
                              </div>

                              <div class="form-group col-lg-3">
                                 <label>CSV File*</label>
                                    <input type="file" class="form-control custom-file-input" id="customFile" name="csv_import" accept=".csv" onchange='triggerValidation(this)' required>
                                 @if($errors->has('csv_import'))
                                <em class="invalid-feedback">{{ $errors->first('csv_import') }}</em>
                                @endif
                              </div>

                              <div class="form-group col-lg-3">
                                 <label>ZIP File</label>
                                    <input type="file" class="form-control custom-file-input" id="file" name="file" accept=".zip"  >
                                 @if($errors->has('file'))
                                <em class="invalid-feedback">{{ $errors->first('file') }}</em>
                                @endif
                              </div>

                              </div>  
                              
                              <div>
                                 <button type="submit" class="btn btn-success">Upload</button>
                              </div>
                           </form>

                           <hr/>

                           <form action="{!! route('admin.bulkupload.export') !!}" method="POST" enctype="multipart/form-data" id="frm_importcsv" novalidate="">
                            {{ csrf_field() }}
                           <div class="row">
                              <div class="form-group col-lg-4">
                                 <label>Download Uploaded data</label>
                                 <select class="form-control" name="export_table" id="export_table">
                                        <option value="">Select Data </option>
                                        <option value="category">Category</option>
                                        <option value="brand">Brand</option>
                                        <option value="model">Model</option>
                                        <option value="sub_model">Sub Model</option>
                                        <option value="model_screen">Model Screen</option>
                                 </select>
                              </div>
                           </div>  
                           <div class="row">
                              <div class="form-group col-lg-3 category model_screen">
                                 <label>Filter With Category</label>
                                  <select class="form-control" name="brand_type_id" id="brand_type_id"  >
                                        <option value="">Select Category</option>
                                        @foreach($brand_types_list as $id => $brand_type)
                                        <option value="{{ $brand_type->id }}">{{ $brand_type->name }}</option>
                                        @endforeach
                                    </select>
                                    
                              </div>

                              <div class="form-group col-lg-3 brand model_screen">
                                 <label>Filter With Brand</label>
                                  <select class="form-control" name="brand_id" id="brand_id"   >
                                        <option value=" ">Select Brand </option>
                                        @foreach($brands_list as $id => $brand)
                                        <option value="{{ $brand->id }}" {{ (in_array($brand->id, old('brand', [])) ) ? 'selected' : '' }}>{{ $brand->name }}</option>
                                        @endforeach
                                    </select>
                                    @if($errors->has('brand_id'))<em class="invalid-feedback">{{ $errors->first('brand_id') }}</em>@endif
                              </div>

                              <div class="form-group col-lg-3 model model_screen">
                                 <label>Filter With Model</label>
                                 <select class="form-control" name="model_id" id="model_id"   >
                                        <option value=" ">Select Model </option>
                                        @foreach($brandModel_list as $id => $models)
                                        <option value="{{ $models->id }}" {{ (in_array($models->id, old('models', [])) ) ? 'selected' : '' }}>{{ $models->name }}</option>
                                        @endforeach
                                 </select>
                                 @if($errors->has('model_id'))<em class="invalid-feedback">{{ $errors->first('model_id') }}</em>@endif
                              </div>

                              <div class="form-group col-lg-3 sub_model model_screen">
                                 <label>Filter With SubModel</label>
                                 <select class="form-control" name="sub_model_id" id="sub_model_id">
                                        <option value=" ">Select Sub Model </option>
                                        @foreach($brandSubModel_list as $id => $submodels)
                                        <option value="{{ $submodels->id }}" {{ (in_array($submodels->id, old('submodels', [])) ) ? 'selected' : '' }}>{{ $submodels->name }}</option>
                                        @endforeach
                                    </select>
                                 @if($errors->has('sub_model_id'))<em class="invalid-feedback">{{ $errors->first('sub_model_id') }}</em>@endif
                              </div>

                           </div>   
                              <div>
                                 <button type="submit" class="btn btn-primary">Download</button>
                              </div>
                           </form>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
    <script src="{{ asset('public/Admin/js/jquery-3.6.0.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/feather.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.slimscroll.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{ asset('public/Admin/plugins/apexchart/apexcharts.min.js')}}"></script>
    <script src="{{ asset('public/Admin/plugins/apexchart/chart-data.js')}}"></script> 
    <script src="{{ asset('public/Admin/js/script.js')}}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script>

 $('select').select2();
   $(function () {
       $("#frm_importcsv").validate({
           rules: {
               sample_file_id: {
                   required: true,
               },
               csv_import: {
                   required: true,
               },
           },
           messages: {
               sample_file_id: {
                   required: "Please select import data"
               },
               csv_import: {
                   required: "Please select csv file"
               }
           },
       });
   });
$(function () {
       // Add the following code if you want the name of the file appear on select
       $(".custom-file-input").on("change", function () {
           var fileName = $(this).val().split("\\").pop();
           $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
       });
   
       var regex = new RegExp("(.*?)\.(csv)$");
   
       function triggerValidation(el) {
           if (!(regex.test(el.value.toLowerCase()))) {
               el.value = '';
               alert('Please select correct file format(csv).');
           }
       }
   });

   function download(d) {
       if (d == '')
           return;
       window.location = "{{url('/Admin/uploads/sample')}}/" + d;
   }
   function exportTable(d) {
       if (d == '')
           return;
       $('frm_export_csv').submit();
       //window.location = "{{url('bulk-export')}}/" + d;
   }

   $(document).ready(function () 
   {
       $(".model_screen").hide();
       
       $("#export_table").change(function () 
       {
           var class_name = $(this).find(":selected").val();
           console.log(class_name);
           $('.' + class_name).show();
           if(class_name == "category")
           {
               $(".brand").hide();
               $(".model").hide();
               $(".sub_model").hide();     
           }
           else if(class_name == "brand")
           {
               $(".model").hide();
               $(".sub_model").hide();     
           }
           else if(class_name == "model")
           {
               $(".sub_model").hide();     
           }
           else if(class_name == "model_screen")
           {
               $(".model_screen").hide();     
           }
       });
   
       $("#brand_type_id").change(function () 
       {
           var brand_type = $(this).find(":selected").val();
           var request = $.ajax({
               type: 'GET',
               url: "{{url('admin/get-brand-type-list')}}?brand_type=" + brand_type,
           });
           request.done(function (data) {
               $("#brand_id").empty();
               if (data) {
                   $("#brand_id").append('<option value="">Select Brand</option>');
                   $.each(data, function (key, value) {
                       $("#brand_id").append('<option value="' + value['id'] + '">' + value['name'] + '</option>');
                   });
               }
               $('#brand_id').selectpicker('refresh');
           });
      });
   
      $("#brand_id").change(function () 
      {
           var brand = $(this).find(":selected").val();
           var request = $.ajax({
               type: 'GET',
               url: "{{url('admin/get-brand-list')}}?brand=" + brand,
           });
   
           request.done(function (data) {
               $("#model_id").empty();
               if (data) {
                   //alert(data);
                   $("#model_id").append('<option value="">Select Model</option>');
                   $.each(data, function (key, value) {
                       $("#model_id").append('<option value="' + value['id'] + '">' + value['name'] + '</option>');
                   });
               }
               $('#model_id').selectpicker('refresh');
           });
      });
   
      $("#model_id").change(function () 
      {
           var brand = $(this).find(":selected").val();
           var request = $.ajax({
               type: 'GET',
               url: "{{url('admin/get-sub-brand-type-list')}}?model_id=" + brand,
           });
   
           request.done(function (data) 
           {
               $("#sub_model_id").empty();
               if (data) {
                   //alert(data);
                   $("#sub_model_id").append('<option value="">Select Model</option>');
                   $.each(data, function (key, value) {
                       $("#sub_model_id").append('<option value="' + value['id'] + '">' + value['name'] + '</option>');
                   });
               }
               $('#sub_model_id').selectpicker('refresh');
           });
      });
   
   });
</script>
   </body>
</html>