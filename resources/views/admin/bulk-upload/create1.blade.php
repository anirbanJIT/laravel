@extends('layouts.admin')
@section('content')
<div class="breadcomb-area">
   <div class="container">
      <div class="row">
         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="breadcomb-list">
               <div class="row">
                  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                     <div class="breadcomb-wp">
                        <div class="breadcomb-icon">
                           <i class="notika-icon notika-windows"></i>
                        </div>
                        <div class="breadcomb-ctn">
                           <h2>{{ trans('cruds.bulkupload.title_singular') }}</h2>
                           <p>Here <span class="bread-ntd">you can upload bulk data</span></p>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-3">
                     <div class="breadcomb-report"> 
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="form-element-area">
   <div class="container">
      <div class="row">
         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-element-list">
               <div class="row">
                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                     <div class="nk-int-mk sl-dp-mn mg-t-10">
                        <label>Download Sample File</label>
                     </div>
                     <div class="bootstrap-select">
                        <select class="selectpicker" name="sample_file_download_id" id="sample_file_download_id" onChange="download(this.value)">
                           <option value="">Please Select</option>
                           <option value="category.csv">Category</option>
                           <option value="brand.csv">Brand</option>
                           <option value="model.csv">Model</option>
                           <option value="sub_model.csv">Sub Model</option>
                           <option value="model_screen.csv">Model Screen</option>
                        </select>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<br/>
<div class="form-element-area">
   <div class="container">
      <form action="{!! route('admin.bulkupload.store') !!}" method="POST" enctype="multipart/form-data" id="frm_importcsv" novalidate="">
         @csrf
         <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
               <div class="form-element-list">
                  <div class="row">
                     <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                        <div class="nk-int-mk sl-dp-mn mg-t-10">
                           <label>Import Data*</label>
                        </div>
                        <div class="bootstrap-select">
                           <select class="selectpicker" name="sample_file_id" id="sample_file_id" required="">
                              <option value="">Please Select</option>
                              <option value="category" @if(old('sample_file_id') == "category")selected @endif>Category</option>
                              <option value="brand" @if(old('sample_file_id') == "brand")  selected @endif>Brand</option>
                              <option value="model" @if(old('sample_file_id') == "model") selected @endif>Model</option>
                              <option value="sub_model" @if(old('sample_file_id') == "sub_model" ) selected @endif>Sub Model</option>
                              <option value="modelscreen" @if(old('sample_file_id') == "modelscreen") selected @endif>Model Screen</option>
                           </select>
                           @if ($errors->has('sample_file_id'))
                           <span class="text-danger">{{ $errors->first('sample_file_id') }}</span>
                           @endif
                        </div>
                     </div>
                     <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                        <div class="nk-int-mk sl-dp-mn mg-t-10">
                           <label>Action*</label>
                        </div>
                        <div class="bootstrap-select">
                           <select class="selectpicker" name="action_type" id="action_type" required="">
                           <option value="add" @if(old('action_type') == 'add') selected @endif>Add New Data</option>
                           <option value="edit" @if(old('action_type') == 'edit' ) selected @endif>Update Data</option>
                           </select>
                           @if ($errors->has('action_type'))
                           <span class="text-danger">{{ $errors->first('action_type') }}</span>
                           @endif
                        </div>
                     </div>
                     <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                        <div class="nk-int-mk sl-dp-mn mg-t-10">
                           <label>CSV File*</label>
                        </div>
                        <div class="nk-int-st">
                           <input type="file" class="custom-file-input" id="customFile" name="csv_import" accept=".csv" onchange='triggerValidation(this)' required>
                        </div>
                        @if ($errors->has('csv_import'))
                        <span class="text-danger">{{ $errors->first('csv_import') }}</span>
                        @endif
                     </div>
                     <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                        <div class="nk-int-mk sl-dp-mn mg-t-10">
                           <label>ZIP File</label>
                        </div>
                        <div class="nk-int-st">
                           <input type="file" class="custom-file-input" id="file" name="file" accept=".zip"  >
                        </div>
                        @if ($errors->has('file'))
                        <span class="text-danger">{{ $errors->first('file') }}</span>
                        @endif
                     </div>
                  </div>
                  <br/>
                  <div class="row">
                     <div class="col-lg-8 col-md-7 col-sm-7 col-xs-12">
                        <button input type="sumit" name="submit" class="btn btn-success notika-btn-success" id="submitBtn">Upload</button>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </form>
   </div>
</div>
<br>
<div class="form-element-area">
   <div class="container">
      <div class="row">
         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-element-list">
               <form action="{!! route('admin.bulkupload.export') !!}" method="POST" enctype="multipart/form-data" id="frm_export_csv" novalidate="">
                  @csrf         
                  <div class="row">
                     <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                        <div class="nk-int-mk sl-dp-mn mg-t-10">
                           <label>Download Uploaded data</label>
                        </div>
                        <div class="bootstrap-select">
                           <select class="selectpicker" name="export_table" id="export_table">
                              <option value="category">Category</option>
                              <option value="brand">Brand</option>
                              <option value="model">Model</option>
                              <option value="sub_model">Sub Model</option>
                              <option value="model_screen">Model Screen</option>
                           </select>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 brand model sub_model model_screen" style="display: none;">
                        <div class="nk-int-mk sl-dp-mn mg-t-10">
                           <label>Filter With Category</label>
                        </div>
                        <div class="bootstrap-select">
                           <select class="selectpicker" name="brand_type_id" id="brand_type_id"  >
                              <option value="">Select Category</option>
                              @foreach($brand_types_list as $id => $brand_type)
                              <option value="{{ $brand_type->id }}">{{ $brand_type->name }}</option>
                              @endforeach
                           </select>
                        </div>
                     </div>
                     <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 model sub_model model_screen" style="display: none;">
                        <div class="nk-int-mk sl-dp-mn mg-t-10">
                           <label>Filter With Brand</label>
                        </div>
                        <div class="bootstrap-select">
                           <select class="selectpicker" name="brand_id" id="brand_id"   >
                              <option value=" ">Select Brand </option>
                              @foreach($brands_list as $id => $brand)
                              <option value="{{ $brand->id }}" {{ (in_array($brand->id, old('brand', [])) ) ? 'selected' : '' }}>{{ $brand->name }}</option>
                              @endforeach
                           </select>
                        </div>
                        @if($errors->has('brand_id'))
                        <em class="invalid-feedback">
                        {{ $errors->first('brand_id') }}
                        </em>
                        @endif
                     </div>
                     <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 sub_model model_screen" style="display: none;">
                        <div class="nk-int-mk sl-dp-mn mg-t-10">
                           <label>Filter With Model</label>
                        </div>
                        <div class="bootstrap-select">
                           <select class="selectpicker" name="model_id" id="model_id"   >
                              <option value=" ">Select Model </option>
                              @foreach($brandModel_list as $id => $models)
                              <option value="{{ $models->id }}" {{ (in_array($models->id, old('models', [])) ) ? 'selected' : '' }}>{{ $models->name }}</option>
                              @endforeach
                           </select>
                        </div>
                        @if($errors->has('model_id'))
                        <em class="invalid-feedback">
                        {{ $errors->first('model_id') }}
                        </em>
                        @endif
                     </div>
                     <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 model_screen" style="display: none;">
                        <div class="nk-int-mk sl-dp-mn mg-t-10">
                           <label>Filter With SubModel</label>
                        </div>
                        <div class="bootstrap-select">
                           <select class="selectpicker" name="sub_model_id" id="sub_model_id">
                              <option value=" ">Select Sub Model </option>
                              @foreach($brandSubModel_list as $id => $submodels)
                              <option value="{{ $submodels->id }}" {{ (in_array($submodels->id, old('submodels', [])) ) ? 'selected' : '' }}>{{ $submodels->name }}</option>
                              @endforeach
                           </select>
                        </div>
                        @if($errors->has('sub_model_id'))
                        <em class="invalid-feedback">
                        {{ $errors->first('sub_model_id') }}
                        </em>
                        @endif
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                        <div class="nk-int-mk sl-dp-mn mg-t-10"> <label>&nbsp;</label></div>
                        <button input type="sumit" name="submit" class="btn btn-success notika-btn-success" id="submitBtn">Download</button>
                     </div>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
@endsection
@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
<script>
   $(function () {
       $("#frm_importcsv").validate({
           rules: {
               sample_file_id: {
                   required: true,
               },
               csv_import: {
                   required: true,
               },
           },
           messages: {
               sample_file_id: {
                   required: "Please select import data"
               },
               csv_import: {
                   required: "Please select csv file"
               }
           },
       });
   });
</script>
<script>
   $(function () {
       // Add the following code if you want the name of the file appear on select
       $(".custom-file-input").on("change", function () {
           var fileName = $(this).val().split("\\").pop();
           $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
       });
   
       var regex = new RegExp("(.*?)\.(csv)$");
   
       function triggerValidation(el) {
           if (!(regex.test(el.value.toLowerCase()))) {
               el.value = '';
               alert('Please select correct file format(csv).');
           }
       }
   });
</script>
<script type="text/javascript">
   function download(d) {
       if (d == '')
           return;
       window.location = "{{url('/uploads/sample')}}/" + d;
   }
   function exportTable(d) {
       if (d == '')
           return;
       $('frm_export_csv').submit();
       //window.location = "{{url('bulk-export')}}/" + d;
   }
</script>
<script type="text/javascript">
   $(document).ready(function () {
       $("#export_table").change(function () {
           $('.model_screen').hide();
           var class_name = $(this).find(":selected").val();
           console.log(class_name);
           $('.' + class_name).show();
       });
   
       $("#brand_type_id").change(function () {
           var brand_type = $(this).find(":selected").val();
           var request = $.ajax({
               type: 'GET',
               url: "{{url('admin/get-brand-type-list')}}?brand_type=" + brand_type,
           });
           request.done(function (data) {
               $("#brand_id").empty();
               if (data) {
                   $("#brand_id").append('<option value="">Select Brand</option>');
                   $.each(data, function (key, value) {
                       $("#brand_id").append('<option value="' + value['id'] + '">' + value['name'] + '</option>');
                   });
               }
               $('#brand_id').selectpicker('refresh');
           });
   
       });
   
       $("#brand_id").change(function () {
           var brand = $(this).find(":selected").val();
           var request = $.ajax({
               type: 'GET',
               url: "{{url('admin/get-brand-list')}}?brand=" + brand,
           });
   
           request.done(function (data) {
               $("#model_id").empty();
               if (data) {
                   //alert(data);
                   $("#model_id").append('<option value="">Select Model</option>');
                   $.each(data, function (key, value) {
                       $("#model_id").append('<option value="' + value['id'] + '">' + value['name'] + '</option>');
                   });
               }
               $('#model_id').selectpicker('refresh');
           });
   
       });
   
       $("#model_id").change(function () {
           var brand = $(this).find(":selected").val();
           var request = $.ajax({
               type: 'GET',
               url: "{{url('admin/get-sub-brand-type-list')}}?model_id=" + brand,
           });
   
           request.done(function (data) {
               $("#sub_model_id").empty();
               if (data) {
                   //alert(data);
                   $("#sub_model_id").append('<option value="">Select Model</option>');
                   $.each(data, function (key, value) {
                       $("#sub_model_id").append('<option value="' + value['id'] + '">' + value['name'] + '</option>');
                   });
               }
               $('#sub_model_id').selectpicker('refresh');
           });
       });
   
   });
</script>
@endpush