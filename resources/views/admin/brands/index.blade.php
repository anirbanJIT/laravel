<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
      <meta name="description" content="POS - Bootstrap Admin Template">
      <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
      <meta name="author" content="Dreamguys - Bootstrap Admin Template">
      <meta name="robots" content="noindex, nofollow">
      <title>{{ trans('cruds.brands.title_singular') }} {{ trans('global.list') }}</title>
      
      <link rel="shortcut icon" type="image/x-icon" href="{{ asset('public/Admin/img/favicon.png')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/bootstrap.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/animate.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/dataTables.bootstrap4.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/fontawesome.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/all.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/style.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/select2/css/select2.min.css')}}">
      <meta name="csrf-token" content="{{ csrf_token() }}">
   </head>
   <body>
      <!-- <div id="global-loader">
         <div class="whirly-loader"> </div>
      </div> -->
      <div class="main-wrapper">
        @extends('layouts.header')
     
        @extends('layouts.sidebar')
        
        <div class="page-wrapper">
            <div class="content container-fluid">
               <div class="page-header">
                  <div class="row">
                     <div class="col">
                        <h3 class="page-title">{{ trans('cruds.brands.title_singular') }} {{ trans('global.list') }}</h3>
                        <ul class="breadcrumb">
                           <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">{{ trans('global.dashboard') }}</a></li>
                           <li class="breadcrumb-item active">{{ trans('cruds.brands.title_singular') }} {{ trans('global.list') }}</li>
                        </ul>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-12">
                     <div class="card">
                        <div class="card-header row">
                            <div class="col-lg-6"><h4 class="card-title">{{ trans('cruds.brands.title_singular') }} {{ trans('global.list') }}</h4></div>
                            <div class="col-lg-6"><a href="{{ route('admin.brands.create') }}" class="btn btn-success" style="float: right;">+ {{ trans('global.add') }} {{ trans('cruds.brands.title_singular') }}</a></div>
                        </div>
                        <div class="card-body">
                           <button style="margin-bottom: 10px" class="btn btn-primary delete_all" data-url="{{ route('admin.brands.mass_destroy') }}">Delete All Selected</button>  
                           <div class="table-responsive">
                              <table class="table  datanew ">
                                 <thead>
                                    <tr>
                                       <th width="10"><input type="checkbox" id="master"></th>
                                        <th width="60">  {{ trans('cruds.brand_type.fields.image') }} </th>
                                        <th width="110">{{ trans('cruds.brands.title_singular') }} {{ trans('cruds.brand_type.fields.id') }} </th>
                                        <th width="200"> {{ trans('cruds.brand_type.title_singular') }} {{ trans('cruds.brands.fields.name') }} </th>
                                        <th width="200"> {{ trans('cruds.brands.title_singular') }} {{ trans('cruds.brands.fields.name') }} </th>
                                        <th width="130"> {{ trans('cruds.brand_type.fields.order') }} </th>
                                        <th width="15%"> {{ trans('cruds.brand_type.fields.date') }} </th>
                                        <th width="15%"> {{ trans('cruds.brand_type.fields.status') }} </th>
                                        <th width="12%"> {{ trans('cruds.brand_type.fields.actions') }} </th>
                                    </tr>

                                 </thead>
                                 <tbody>
                                     @foreach($brands as $key => $brand)
                                    <tr data-entry-id="{{ $brand->id }}">
                                       <td><input type="checkbox" class="sub_chk" data-id="{{$brand->id}}"></td>
                                       <td>
                                    @if(isset($brand->image))
                                       <image src="{{URL::asset('Admin/images/brands/'.$brand->image)}}" width="60" height="60"/>
                                    @else
                                        <image src="{{URL::asset('Admin/images/no-image-available.png')}}" width="60" height="60"/>
                                    @endif 
                                    </td>
                                    <td>{{ $brand->id }}</td>
                                    <td>{{ $brand->BrandCatName ?? '' }}</td>
                                    <td>{{ $brand->name ?? '' }}</td>
                                    <td>{{ $brand->order_number ?? $brand->id }}</td>
                                    <td data-order="{{ \Carbon\Carbon::parse($brand->created_at)->format('Y-m-d') ?? '' }}">
                                            {{ \Carbon\Carbon::parse($brand->created_at)->format('d/m/Y') ?? '' }}
                                        </td>
                                        <td data-filter="{{ $brand->status ?? '' }}" data-search="{{ $brand->status ?? '' }}">
                                             <div class="status-toggle d-flex justify-content-between align-items-center">
                                             <input type="checkbox" class="check status_change" data-id="{{$brand->id}}" id="ts{{$brand->id}}" data-on="Active" data-off="InActive" {{ $brand->status ? 'checked' : '' }}>
                                             <label for="ts{{$brand->id}}" class="checktoggle">checkbox</label>
                                          </div>
                                       </td>
                                        <td>
                                            <a  href="{{ route('admin.brands.edit', $brand->id) }}" class="btn btn-success text-white"><i class="fa fa-edit"></i></a>
                                            <a  href="{{ url('admin/brand/delete', $brand->id) }}" class="btn btn-danger text-white"><i class="fa fa-trash" onclick="return confirm('{{ trans('global.areYouSure') }}');"></i></a>
                                        </td>
                                    </tr>
                                    @endforeach
                                    
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
    <script src="{{ asset('public/Admin/js/jquery-3.6.0.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/feather.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.slimscroll.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{ asset('public/Admin/plugins/apexchart/apexcharts.min.js')}}"></script>
    <script src="{{ asset('public/Admin/plugins/apexchart/chart-data.js')}}"></script>
    <script src="{{ asset('public/Admin/js/script.js')}}"></script>
     <script src="{{ asset('public/Admin/plugins/select2/js/select2.min.js')}}"></script>
      <script src="{{ asset('public/Admin/plugins/select2/js/custom-select.js')}}"></script>
      <script type="text/javascript">
         $('.status_change').change(function() 
         {      
            var status = $(this).prop('checked') == true ? 1 : 0; 
            var model_id = $(this).data('id'); 
            $.ajax({
              type:"GET",
              url:"{{url('admin/changeStatusBrand')}}?status="+status+"&brand_id="+model_id,
              success:function(res){               
               if(res){
                   console.log(res.success);               
               }
              }
           }); 
         });

         $(document).ready(function () {  
  
        $('#master').on('click', function(e) {  
         if($(this).is(':checked',true))    
         {  
            $(".sub_chk").prop('checked', true);    
         } else {    
            $(".sub_chk").prop('checked',false);    
         }    
        });  
  
        $('.delete_all').on('click', function(e) {  
  
            var allVals = [];    
            $(".sub_chk:checked").each(function() {    
                allVals.push($(this).attr('data-id'));  
            });    
  
            if(allVals.length <=0)    
            {    
                alert("Please select row.");    
            }  else {    
  
                var check = confirm("Are you sure you want to delete this row?");    
                if(check == true){    
  
                    var join_selected_values = allVals.join(",");   
  
                    $.ajax({  
                        url: $(this).data('url'),  
                        type: 'DELETE',  
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},  
                        data: 'ids='+join_selected_values,  
                        success: function (data) {  
                            if (data['success']) {  
                                $(".sub_chk:checked").each(function() {    
                                    $(this).parents("tr").remove();  
                                });  
                                alert(data['success']);  
                            } else if (data['error']) {  
                                alert(data['error']);  
                            } else {  
                                alert('Whoops Something went wrong!!');  
                            }  
                        },  
                        error: function (data) {  
                            alert(data.responseText);  
                        }  
                    });  
  
                  $.each(allVals, function( index, value ) {  
                      $('table tr').filter("[data-row-id='" + value + "']").remove();  
                  });  
                }    
            }    
        });  
  
        $('[data-toggle=confirmation]').confirmation({  
            rootSelector: '[data-toggle=confirmation]',  
            onConfirm: function (event, element) {  
                element.trigger('confirm');  
            }  
        });  
  
        $(document).on('confirm', function (e) {  
            var eele = e.target;  
            e.preventDefault();  
  
            $.ajax({  
                url: ele.href,  
                type: 'DELETE',  
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},  
                success: function (data) {  
                    if (data['success']) {  
                        $("#" + data['tr']).slideUp("slow");  
                        alert(data['success']);  
                    } else if (data['error']) {  
                        alert(data['error']);  
                    } else {  
                        alert('Whoops Something went wrong!!');  
                    }  
                },  
                error: function (data) {  
                    alert(data.responseText);  
                }  
            });  
  
            return false;  
        });  
    });  
   </script>
   </body>
</html>