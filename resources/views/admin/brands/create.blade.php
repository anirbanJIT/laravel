<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
      <meta name="description" content="POS - Bootstrap Admin Template">
      <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
      <meta name="author" content="Dreamguys - Bootstrap Admin Template">
      <meta name="robots" content="noindex, nofollow">
      <title>{{ trans('global.create') }} {{ trans('cruds.brands.title_singular') }}</title>
      
      <link rel="shortcut icon" type="image/x-icon" href="{{ asset('public/Admin/img/favicon.png')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/bootstrap.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/animate.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/dataTables.bootstrap4.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/fontawesome.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/all.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/style.css')}}">
   </head>
   <body>
      <div id="global-loader">
         <div class="whirly-loader"> </div>
      </div>
      <div class="main-wrapper">
        @extends('layouts.header')
     
        @extends('layouts.sidebar')
        
        <div class="page-wrapper">
            <div class="content container-fluid">
                <div class="page-header">
					<div class="row">
						<div class="col">
							<h3 class="page-title">{{ trans('global.create') }} {{ trans('cruds.brands.title_singular') }}</h3>
							<ul class="breadcrumb">
								<li class="breadcrumb-item"><a href="{{route('admin.home')}}">{{ trans('global.dashboard') }}</a></li>
								<li class="breadcrumb-item">
									@if($brandtype_id)
										<a href="{{ route('admin.brands.show',$brandtype_id) }}">{{ trans('cruds.brands.title') }}</a>
									@else
										<a href="{{ route('admin.brand-type.index') }}">{{ trans('cruds.brands.title') }}</a>
									@endif
								</li>
								<li class="breadcrumb-item active">{{ trans('global.create') }} {{ trans('cruds.brands.title_singular') }}</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div class="card">
							<div class="card-body">
								<form action="{{ route('admin.brands.store') }}" method="POST" enctype="multipart/form-data">
									{{ csrf_field() }}
									<input type="hidden" name="get_brand_type_id" value="{{ $brandtype_id }}">
									<div class="form-group">
										<label>Name<span class="text-danger">*</span></label>
										<input type="text" class="form-control @error('name') is-invalid @enderror" value="{{ old('name') }}" placeholder="{{ trans('cruds.brand_type.fields.name') }}*" id="name" name="name">
										@if($errors->has('name')) 
											<em class="invalid-feedback"> {{ $errors->first('name') }}</em> 
										@endif
									</div>
									<div class="form-group">
										<label>Category<span class="text-danger">*</span></label>
										<select class="form-control @error('brandType') is-invalid @enderror" name="brandType">
											<option value="">Select Category</option>
											@foreach($brandTypes as $key => $brandType)
												<option value="{{$brandType->id}}" {{ $brandtype_id == $brandType->id ?'selected': '' }}>{{$brandType->name}}</option>
											@endforeach
										</select>
										@if($errors->has('brandType'))
											<span class="text-danger">{{ $errors->first('brandType') }}</span>
										@endif
									</div>
									<div class="form-group">
										<label>Upload image</label>
										<input type="file" accept="image/*" class="form-control" name="brandImg" id="brandImg">
										<span id="preview"></span>
									</div>
									<div class="form-group">
										<label>Meta Title</label>
										<input type="text" class="form-control" placeholder="Meta Title" id="metaTitle" name="metaTitle" value="{{ old('metaTitle') }}">
										@if($errors->has('metaTitle')) 
											<em class="invalid-feedback"> {{ $errors->first('metaTitle') }} </em> 
										@endif
									</div>
									<div class="form-group">
										<label>Meta Keyword</label>
										<input type="text" class="form-control"  placeholder="Meta Keyword" id="metaKeyword" name="metaKeyword" value="{{ old('metaKeyword') }}">
										@if($errors->has('metaKeyword')) 
											<em class="invalid-feedback">{{ $errors->first('metaKeyword') }}</em> 
										@endif
									</div>
									<div class="form-group">
										<label>Meta Description</label>
										<input type="text" class="form-control"  placeholder="Meta Description" id="metaDescription" name="metaDescription" value="{{ old('metaDescription') }}">
										@if($errors->has('metadescription')) 
											<em class="invalid-feedback">{{ $errors->first('metadescription')}}</em> 
										@endif
									</div>
									<div>
										<button type="submit" class="btn btn-success">Save</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
        </div>
    </div>
    <script src="{{ asset('public/Admin/js/jquery-3.6.0.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/feather.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.slimscroll.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{ asset('public/Admin/plugins/apexchart/apexcharts.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/script.js')}}"></script>
	<script>
		
		$('#brandImg').change(function(){
			const file = this.files[0];
			
			console.log(file);
			if (file){
			let reader = new FileReader();
			reader.onload = function(event){
				console.log(event.target.result);
				$("#preview").html('<img id="imgPreview" hight="50px" width="50px" src="'+event.target.result+'" alt="pic" />');
				//$('#imgPreview').attr('src', );
			}
			reader.readAsDataURL(file);
			}
		});
		

	</script>
   </body>
</html>