@extends('layouts.admin')

@section('content')
<div class="breadcomb-area">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="breadcomb-list">
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<div class="breadcomb-wp">
								<div class="breadcomb-icon">
									<i class="notika-icon notika-windows"></i>
								</div>
								<div class="breadcomb-ctn">
                                    <h2>Show Complaint Feedback</h2>
                                    <p>Show <span class="bread-ntd">particular Complaint Feedback</span></p>
								</div>								
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-3">
                            <div class="breadcomb-report">
                                <a class="btn btn-success notika-btn-success waves-effect" href="{{ url()->previous() }}">
                                    {{ trans('global.back_to_list') }}
                                </a>
							</div>							
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="data-table-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="data-table-list">
                        <div class="table-responsive">
                            <table id="data-table-basic" class="table table-striped">
                            <tbody>
                                <tr>
                                    <th>Title</th>
                                    <td>{{ $complaintfeedback->title }}</td>
                                </tr>
                                <tr>
                                    <th>Type</th>
                                    <td>{{ $complaintfeedback->type }}({{$complaintfeedback->status}})</td>
                                </tr>
                                <tr>
                                    <th>Name</th>
                                    <td>{{ $complaintfeedback->user->name }}</td>
                                </tr>
                                <tr>
                                    <th>Date</th>
                                    <td>{{ $complaintfeedback->created_at->format('d M Y') }}</td>
                                </tr>
                                <tr>
                                    <th>Description</th>
                                    <td>{{ $complaintfeedback->description }}</td>
                                </tr>
                                @if($complaintfeedback->status == 'Solved')
                                <tr>
                                    <th>Comment</th>
                                    <td>{{ $complaintfeedback->comment }}</td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection