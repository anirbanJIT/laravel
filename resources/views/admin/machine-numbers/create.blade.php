<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
      <meta name="description" content="POS - Bootstrap Admin Template">
      <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
      <meta name="author" content="Dreamguys - Bootstrap Admin Template">
      <meta name="robots" content="noindex, nofollow">
      <title>{{ trans('global.create') }} Machine Numbers</title>
      
      <link rel="shortcut icon" type="image/x-icon" href="{{ asset('public/Admin/img/favicon.png')}}">
      
      <link rel="stylesheet" href="{{ asset('public/Admin/css/bootstrap.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/select2/css/select2.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/animate.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/dataTables.bootstrap4.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/fontawesome.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/all.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/style.css')}}">
      
   </head>
   <body>
      <div id="global-loader">
         <div class="whirly-loader"> </div>
      </div>
      <div class="main-wrapper">
        @extends('layouts.header')
     
        @extends('layouts.sidebar')
        
        <div class="page-wrapper">
            <div class="content container-fluid">
               <div class="page-header">
                  <div class="row">
                     <div class="col">
                        <h3 class="page-title">{{ trans('global.create') }} Machine Numbers</h3>
                        <ul class="breadcrumb">
                           <li class="breadcrumb-item"><a href="{{route('admin.home')}}">{{ trans('global.dashboard') }}</a></li>
                           <li class="breadcrumb-item"><a href="{{route('admin.machine-numbers.index')}}">Machine Numbers List</a></li>
                           <li class="breadcrumb-item active">Create Machine Numbers</li>
                        </ul>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-12">
                     <div class="card">
                        <!-- <div class="card-header row">
                            <div class="col-lg-6"><h4 class="card-title">{{ trans('global.create') }} {{ trans('cruds.permission.title_singular') }}</h4></div>
                            
                        </div> -->
                        <div class="card-body">
							<a class="btn btn-success addnew"  >+</a>
                           <form action="{{ route('admin.machine-numbers.store') }}" method="POST">
                            {{ csrf_field() }}
                              <div class="row tr_clone_first tr_clone">
								  <div class="form-group col-md-6">
									 <label>Quantity</label>
									 <input type="number" name="quantity[]" id="quantity" class="form-control" placeholder="Machine Quantity" required="">
									 @if($errors->has('quantity')) <em class="invalid-feedback"> {{ $errors->first('quantity') }} </em> @endif
								  </div>
								  <div class="form-group col-md-6">
									<label>Status</label>
									<select name="status[]" id="status" class="form-control">
									  <option value="active"  >Active</option> 
									  <option value="deactive" >Deactive</option> 
									</select>
									@if($errors->has('status')) <em class="invalid-feedback"> {{ $errors->first('status') }} </em> @endif
								  </div>
							  </div>
                              <div>
                                 <button type="submit" class="btn btn-success">Save</button>
                              </div>
                           </form>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
    <script src="{{ asset('public/Admin/js/jquery-3.6.0.min.js')}}"></script>
    <script src="{{ asset('public/Admin/plugins/select2/js/select2.min.js')}}"></script>
   
    <script src="{{ asset('public/Admin/js/feather.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.slimscroll.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/script.js')}}"></script>
    <script>
		$(document).on('click', '.addnew', function(e){
			
			$(".tr_clone").append('<div class="row tr_clone_first"><div class="form-group col-md-6"><label>Quantity</label><input type="number" name="quantity[]" id="quantity" class="form-control" placeholder="Machine Quantity" required="">@if($errors->has("quantity")) <em class="invalid-feedback"> {{ $errors->first("quantity") }} </em> @endif</div><div class="form-group col-md-6"><label>Status</label><select name="status[]" id="status" class="form-control"><option value="active"  >Active</option><option value="deactive" >Deactive</option></select>@if($errors->has("status")) <em class="invalid-feedback"> {{ $errors->first("status") }} </em> @endif</div></div>');
			 
			
		});
	</script>
   </body>
</html>