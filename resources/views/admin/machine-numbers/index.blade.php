<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
      <meta name="description" content="POS - Bootstrap Admin Template">
      <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
      <meta name="author" content="Dreamguys - Bootstrap Admin Template">
      <meta name="robots" content="noindex, nofollow">
      <title>Machine Number List</title>
      
      <link rel="shortcut icon" type="image/x-icon" href="{{ asset('public/Admin/img/favicon.png')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/bootstrap.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/animate.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/dataTables.bootstrap4.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/fontawesome.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/all.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/style.css')}}">
   </head>
   <body>
      <div id="global-loader">
         <div class="whirly-loader"> </div>
      </div>
      <div class="main-wrapper">
        @extends('layouts.header')
     
        @extends('layouts.sidebar')
        
        <div class="page-wrapper">
            <div class="content container-fluid">
               <div class="page-header">
                  <div class="row">
                     <div class="col">
                        <h3 class="page-title">Machine Number List</h3>
                        <ul class="breadcrumb">
                           <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">{{ trans('global.dashboard') }}</a></li>
                           <li class="breadcrumb-item active">Machine Number List</li>
                        </ul>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-12">
                     <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Machine Number List</h4>
                        </div>
                        <div class="card-body">
                           <form action="{{ route('admin.machine-numbers.store') }}" method="POST">
                            {{ csrf_field() }}
                              <div class="form-group">
                                 <label>Name</label>
                                 <input type="text" name="machine_number" id="machine_number" class="form-control" placeholder="Machine Number" required="">
                                 @if($errors->has('machine_number')) <em class="invalid-feedback"> {{ $errors->first('machine_number') }} </em> @endif
                              </div>
                              <div>
                                 <button type="submit" class="btn btn-success">Save</button>
                              </div>
                           </form>
                           <hr/>

                           <form action="{{ url('admin/upload/machinenumber') }}" method="POST" enctype="multipart/form-data" id="frm_importcsv" novalidate="">
                            {{ csrf_field() }}
                              <div class="form-group">
                                 <label>CSV File*</label>
                                 <input type="file" class="form-control" id="customFile" name="csv_import" accept=".csv" onchange='triggerValidation(this)' required>
                                 @if($errors->has('csv_import')) <em class="invalid-feedback"> {{ $errors->first('csv_import') }} </em> @endif
                              </div>
                              <div>
                                 <button type="submit" class="btn btn-success">Upload</button> &nbsp; 
                                 <a href="{{URL::asset('Admin/uploads/sample/machine-number.csv')}}" class="btn btn-primary" target="_blank">Download Sample</a>
                              </div>
                           </form>

                            <hr/>

                           <div class="table-responsive">
                              <table class="table  datanew ">
                                 <thead>
                                    <tr role="row" class="heading">
                                    <th width="25%">ID</th>
                                    <th width="25%">Machine Number</th>
                                    <th width="25%">Customer</th>
                                    <th width="25%">Status</th>
                                    <th width="25%">Date</th>
                                    <th width="25%">&nbsp;</th>
                                </tr>
                                 </thead>
                                 <tbody>
                                 @foreach($machineNumbers as $key => $serialNumber)
                                <tr data-entry-id="{{ $serialNumber->id }}">
                                    <td>{{ $serialNumber->id }}</td>
                                    <td>{{ $serialNumber->machine_number}}</td>
                                    <td>{{ $serialNumber->username}}</td>
                                    <td>
                                        @if($serialNumber->u_id)
                                        {{'Active'}}
                                        @else
                                        {{'Inactive'}}
                                        @endif
                                    </td>
                                    <td data-order="{{ \Carbon\Carbon::parse($serialNumber->created_at)->format('Y-m-d') ?? '' }}">
                                        {{ convertUtcToLocal($serialNumber->created_at)->format('d/m/Y H:i:s') ?? '' }}</td>
                                    <td></td>
                                </tr>
                                @endforeach
                                    
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
    <script src="{{ asset('public/Admin/js/jquery-3.6.0.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/feather.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.slimscroll.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{ asset('public/Admin/plugins/apexchart/apexcharts.min.js')}}"></script>
    <script src="{{ asset('public/Admin/plugins/apexchart/chart-data.js')}}"></script>
    <script src="{{ asset('public/Admin/js/script.js')}}"></script>
    
   </body>
</html>