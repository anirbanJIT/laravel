@extends('layouts.admin')

@section('content')

<div class="breadcomb-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="breadcomb-list">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="breadcomb-wp">
                                <div class="breadcomb-icon">
                                    <i class="notika-icon notika-windows"></i>
                                </div>
                                <div class="breadcomb-ctn">
                                    <h2>Machine Number List</h2>
                                    <p>Show <span class="bread-ntd">all list</span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>			
        </div>
    </div>
</div>

<div class="form-element-area">
    <div class="container">
        <form action="{{ route('admin.machine-numbers.store') }}" method="POST">
            @csrf
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="form-element-list">

                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group ic-cmp-int">
                                    <div class="form-ic-cmp">
                                        <!--<i class="notika-icon notika-support"></i>-->
                                    </div>
                                    <div class="nk-int-st">
                                        <input type="text" name="machine_number" id="machine_number" class="form-control" placeholder="Machine Number" required="">
                                    </div>
                                    @if ($errors->has('machine_number'))
                                    <span class="text-danger">{{ $errors->first('machine_number') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <button input type="submit" name="submit" class="btn btn-success notika-btn-success">Submit</button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </form>			
    </div>
</div>

<div class="form-element-area">
    <div class="container">
        <form action="{{ url('admin/upload/machinenumber') }}" method="POST" enctype="multipart/form-data" id="frm_importcsv" novalidate="">  @csrf
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="form-element-list">
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                <div class="nk-int-mk sl-dp-mn mg-t-10">
                                    <label>CSV File*</label>
                                </div>
                                <div class="nk-int-st">
                                    <input type="file" class="custom-file-input" id="customFile" name="csv_import" accept=".csv" onchange='triggerValidation(this)' required>
                                </div>
                                @if ($errors->has('csv_import'))
                                <span class="text-danger">{{ $errors->first('csv_import') }}</span>
                                @endif
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                <br/>
                                <button input type="sumit" name="submit" class="btn btn-success notika-btn-success" id="submitBtn">Upload</button>
                                &emsp;
                                <button input type="button" class="btn btn-success notika-btn-success" onclick="download()">Download Sample</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<!-- Data Table area Start-->
<div class="data-table-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="data-table-list">
                    <div class="table-responsive">
                        <table id="datatable-ComplaintFeedback" class="table table-striped">
                            <thead>
                                <tr role="row" class="heading">
                                    <th width="25%">ID</th>
                                    <th width="25%">Machine Number</th>
                                    <th width="25%">Customer</th>
                                    <th width="25%">Status</th>
                                    <th width="25%">Date</th>
                                    <th width="25%">&nbsp;</th>
                                </tr>
                                <tr role="row" class="filterrow">
                                    <th class="select-filter">ID</th>
                                    <th class="select-filter">Machine Number</th>
                                    <th class="select-filter">Customer</th>
                                    <th class="select-filter">Status</th>
                                    <th class="input-filter">
                                        <div class="input-group nk-int-st date date-picker" data-date-format="dd/mm/yyyy">
                                            <input type="text" class="form-control input-sm" readonly name="date_from" placeholder="From" id="date_from">
                                            <span class="input-group-addon" style="border: none;"></span>
                                        </div>
                                        <div class="input-group nk-int-st date date-picker" data-date-format="dd/mm/yyyy">
                                            <input type="text" class="form-control input-sm" readonly name="date_to" placeholder="To"  id="date_to">
                                            <span class="input-group-addon" style="border: none;"></span>
                                        </div>
                                    </th>
                                    <th>
                                        <button class="btn btn-danger danger-icon-notika waves-effect filter-cancel" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Reset" id="reset"><i class="notika-icon notika-close"></i></button>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($machineNumbers as $key => $serialNumber)
                                <tr data-entry-id="{{ $serialNumber->id }}">
                                    <td>{{ $serialNumber->id }}</td>
                                    <td>{{ $serialNumber->machine_number}}</td>
                                    <td>{{ $serialNumber->username}}</td>
                                    <td>
                                        @if($serialNumber->u_id)
                                        {{'Active'}}
                                        @else
                                        {{'Inactive'}}
                                        @endif
                                    </td>
                                    <td data-order="{{ \Carbon\Carbon::parse($serialNumber->created_at)->format('Y-m-d') ?? '' }}">
                                        {{ convertUtcToLocal($serialNumber->created_at)->format('d/m/Y H:i:s') ?? '' }}</td>
                                    <td></td>
                                </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>ID</th>
                                    <th>Machine Number</th>
                                    <th>Customer</th>
                                    <th>Status</th>
                                    <th>Date</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        /* @author Naren Thaker @narenthaker.com 101220201500 */
        window._token = $('meta[name="csrf-token"]').attr('content');

        $.extend(true, $.fn.dataTable.Buttons.defaults.dom.button, {className: 'btn'})

        $.extend(true, $.fn.dataTable.defaults, {
            columnDefs: [
                {type: 'date', targets: 4},
                {
                    orderable: false,
                    searchable: false,
                    targets: [-1]
                }],
            order: [[0, 'desc']],
            lengthMenu: [
                [10, 25, 50, 100, -1],
                ['10', '25', '50', '100', 'All']
            ],
            buttons: [],
            orderCellsTop: true,
            dom: 'lBfrtip<"actions">',
            fixedHeader: true,
        });

        let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons);
        var table = $('#datatable-ComplaintFeedback:not(.ajaxTable)').DataTable({
            buttons: dtButtons,
            initComplete: function () {
                this.api().columns([0, 1, 2, 3]).every(function (d) {
                    var column = this;
                    var theadname = $('#datatable-ComplaintFeedback thead th').eq([d]).text();
                    var select = $('<select class ="filter searchSelect"><option value="">' + theadname + ': All</option></select>')
                            .appendTo($("#datatable-ComplaintFeedback thead tr:eq(1) th").eq(column.index()).empty())
                            .tailselect({search: true, deselect: true, width: 130, height: 200, placeholder: theadname});
                    select.on("change", function () {
                        var val = $.fn.dataTable.util.escapeRegex(this.value());
                        column.search(val ? ('^' + val + '$') : '', true, false).draw();
                    });
                    column.data().unique().sort().each(function (d, j) {
                        select.options.add(d, d, '#');
                    });
                    select.query();
                });
            }
        });
        table.on('draw', function () {
            table.columns([0, 1, 2, 3]).indexes().each(function (idx) {
                var select = $("#datatable-ComplaintFeedback thead tr:eq(1) th").eq(idx).find('select').tailselect('.select');
                if (select.value() === null) {
                    select.reload();
                    table.column(idx, {
                        search: 'applied'
                    }).data().unique().sort().each(function (d, j) {
                        select.options.add(d, d);
                    });

                    select.query();
                }
            });
        });

        /* start data filter */
        $(".date-picker").datepicker({
            format: "dd/mm/yyyy",
            autoclose: true,
        }).on('change', function (ev) {
            $(this).datepicker('hide');
            table.draw();
        });

        $.fn.dataTableExt.afnFiltering.push(function (settings, data, dataIndex) {
            var dateStart = parseDateValue($('#date_from').val());
            var dateEnd = parseDateValue($("#date_to").val());
            var evalDate = parseDateValueNew(data[4]);
            if ((isNaN(dateStart) && isNaN(dateEnd)) ||
                    (isNaN(dateStart) && evalDate <= dateEnd) ||
                    (dateStart <= evalDate && isNaN(dateEnd)) ||
                    (dateStart <= evalDate && evalDate <= dateEnd))
            {
                return true;
            } else {
                return false;
            }
        });

        function parseDateValue(rawDate) {
            var dateArray = rawDate.split("/");
            var parsedDate = dateArray[2] + dateArray[0] + dateArray[1];
            return parsedDate;
        }

        function parseDateValueNew(rawDate) {
            rawDate = rawDate.split(" ")[0];
            var dateArray = rawDate.split("/");
            var parsedDate = dateArray[2] + dateArray[0] + dateArray[1];
            return parsedDate;
        }
        /* end data filter */

        $('#reset').click(function (e) {
            $(".date-picker").datepicker('setDate', null);
            //tailselect('.select-search').reload();
        });

    });
    function download(d) {
        if (d == '')
            return;
        window.location = "{{url('/uploads/sample')}}/" + 'machine-number.csv';
    }
</script>
@endsection




