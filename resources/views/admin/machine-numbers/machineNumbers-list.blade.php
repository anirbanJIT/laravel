<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
      <meta name="description" content="POS - Bootstrap Admin Template">
      <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
      <meta name="author" content="Dreamguys - Bootstrap Admin Template">
      <meta name="robots" content="noindex, nofollow">
      <title>Machine Numbers List</title>
      
      <link rel="shortcut icon" type="image/x-icon" href="{{ asset('public/Admin/img/favicon.png')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/bootstrap.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/animate.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/dataTables.bootstrap4.min.css')}}">

      <!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.7.1/css/buttons.dataTables.min.css"> -->
      
       

      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/fontawesome.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/all.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/style.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/select2/css/select2.min.css')}}">
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <link rel="stylesheet" href="{{ asset('public/Admin/tabulator/bootstrap/tabulator_bootstrap4.css')}}">
      
   </head>
   <body>
      <!-- <div id="global-loader">
         <div class="whirly-loader"> </div>
      </div> -->
      <div class="main-wrapper">
        @extends('layouts.header')
     
        @extends('layouts.sidebar')
        
        <div class="page-wrapper">
            <div class="content container-fluid">
               <div class="page-header">
                  <div class="row">
                     <div class="col">
                        <h3 class="page-title">Machine Numbers List</h3>
                        <ul class="breadcrumb">
                           <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">{{ trans('global.dashboard') }}</a></li>
                           <li class="breadcrumb-item active">Machine Numbers List</li>
                        </ul>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-12">
                     <div class="card">
                        <div class="card-header row">
                            <div class="col-lg-6"><h4 class="card-title">Machine Numbers List</h4></div>
                            <div class="col-lg-6"><a href="{{ route('admin.machine-numbers.create') }}" class="btn btn-success" style="float: right;">+ {{ trans('global.add') }} Machine Numbers</a></div>
                        </div>
                        <div class="card-body">
                          <div class="table-responsive">
                                 <button class="btn btn-outline-info waves-effect" id="download-csv">Download CSV</button> <p><br/></p>
                                 <div id="data-table-machine-numbers"></div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
    <script src="{{ asset('public/Admin/js/jquery-3.6.0.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/feather.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.slimscroll.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/script.js')}}"></script>
      
     <script type="text/javascript" language="javascript" src='https://cdn.datatables.net/buttons/2.2.3/js/dataTables.buttons.min.js' defer></script>
      
      <script type="text/javascript" language="javascript" src='https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js' defer></script>
      
      <script type="text/javascript" language="javascript" src='https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js' defer></script>
      
      <script type="text/javascript" language="javascript" src='https://cdn.datatables.net/buttons/2.2.3/js/buttons.html5.min.js' defer></script> 
      
      <!-- <script type="text/javascript" src="{{ asset('public/Admin/js/3rdparty/tabulator.min.js') }}"></script> -->
      <script src="{{ asset('public/Admin/tabulator/tabulator.min.js') }}"></script>
      <script src="{{ asset('public/Admin/tabulator/jspdf.min.js') }}"></script>
      <script src="{{ asset('public/Admin/tabulator/xlsx.full.min.js') }}"></script>
      <script src="{{ asset('public/Admin/tabulator/jspdf.plugin.autotable.js') }}"></script>
      <script type="text/javascript">
      
      var tabledata = <?php echo json_encode($machineNumbers); ?>;
      console.log(tabledata);
      function customFilter(data){
          return data.car && data.rating < 3;
      }

      function updateFilter(){

          var filter = $("#filter-field").val() == "function" ? customFilter : $("#filter-field").val();

          if($("#filter-field").val() == "function" ){
              $("#filter-type").prop("disabled", true);
              $("#filter-value").prop("disabled", true);
          }else{
              $("#filter-type").prop("disabled", false);
              $("#filter-value").prop("disabled", false);
          }

          table.setFilter(filter, $("#filter-type").val(), $("#filter-value").val());
      }

      //Update filters on value change
      $("#filter-field, #filter-type").change(updateFilter);
      $("#filter-value").keyup(updateFilter);

      var table = new Tabulator("#data-table-machine-numbers", {
          data:tabledata,           //load row data from array
          layout:"fitColumns",      //fit columns to width of table
          responsiveLayout:"collapse",  //hide columns that dont fit on the table
          tooltips:true,            //show tool tips on cells
          addRowPos:"top",          //when adding a new row, add it to the top of the table
          history:true,             //allow undo and redo actions on the table
          pagination:"local",       //paginate the data
          paginationSize:10,         //allow 7 rows per page of data
          movableColumns:true,      //allow column order to be changed
          resizableRows:true,       //allow row order to be changed
          initialSort:[             //set the initial sort order of the data
              {column:"id", dir:"desc"},
          ],
          columns: [
               // { data: "delete_chk", orderable:false, searchable:false},
                {title:"Id", field:"id", editor:"label",headerFilter:"input",headerFilterPlaceholder:"Id"},
                {title:"Machine Number", field:"machine_number", editor:"label",headerFilter:"input",headerFilterPlaceholder:"Machine Number"},
                {title:"Quantity", field:"quantity", editor:"label",headerFilter:"input",headerFilterPlaceholder:"Quantity"},
                {title:"Status", field:"status", formatter:function(cell, formatterParams, onRendered){
                  if(cell.getValue() == "active"){
                      return "Active";
                  } else {
                      return "Deactive"
                  }            
               }},
               /*{title:"Action", field:"id", 
            			formatter:function(cell, formatterParams, onRendered) { 
            				var edit_url = '{{ route("admin.machine-numbers.edit",":id") }}';
            				edit_url = edit_url.replace(':id',cell.getValue());
            		   		var delete_url1 = '{{ route("admin.machine-numbers.destroy",":id") }}';
            		                delete_url1 = delete_url1.replace(':id', cell.getValue());
                            		var msg = "{{ trans('global.areYouSure') }}";
            				var token = '{{ csrf_token() }}';
            				var editlink = '<a href="'+edit_url+'" class="btn btn-success text-white"> <i class="fa fa-edit"></i></a>'+
            					'<form action="'+delete_url1+'" method="POST" onsubmit="return confirm(\''+msg+'\');" style="display: inline-block;">'+
            						'<input type="hidden" name="_method" value="DELETE">'+
            						'<input type="hidden" name="_token" value="'+token+'">'+
            						'<button type="submit" class="btn btn-danger danger-icon-notika btn-reco-mg btn-button-mg waves-effect"> <i class="fa fa-trash"></i></button>'+
            					'</form>';
            			        return editlink; 
            			},
               }*/
            ],
      });
	  



      //trigger download of data.csv file
      // document.getElementById("download-csv").addEventListener("click", function(){
      //     table.download("csv", "data.csv");
      // });

      //trigger download of data.xlsx file
      document.getElementById("download-csv").addEventListener("click", function(){
          table.download("csv", "data.csv");
      });

      //trigger download of data.pdf file
      // document.getElementById("download-pdf").addEventListener("click", function(){
      //     table.download("pdf", "data.pdf", {
      //         orientation:"portrait", //set page orientation to portrait
      //         title:"Example Report", //add title to report
      //     });
      // });

</script>
   </body>
</html>