<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
      <meta name="description" content="POS - Bootstrap Admin Template">
      <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
      <meta name="author" content="Dreamguys - Bootstrap Admin Template">
      <meta name="robots" content="noindex, nofollow">
      <title>{{ trans('cruds.role.title_singular') }} {{ trans('global.list') }}</title>
      
      <link rel="shortcut icon" type="image/x-icon" href="{{ asset('public/Admin/img/favicon.png')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/bootstrap.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/animate.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/dataTables.bootstrap4.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/fontawesome.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/all.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/style.css')}}">
   </head>
   <body>
      <div id="global-loader">
         <div class="whirly-loader"> </div>
      </div>
      <div class="main-wrapper">
        @extends('layouts.header')
     
        @extends('layouts.sidebar')
        
        <div class="page-wrapper">
            <div class="content container-fluid">
               <div class="page-header">
                  <div class="row">
                     <div class="col">
                        <h3 class="page-title">{{ trans('cruds.role.title_singular') }} {{ trans('global.list') }}</h3>
                        <ul class="breadcrumb">
                           <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">{{ trans('global.dashboard') }}</a></li>
                           <li class="breadcrumb-item active">{{ trans('cruds.role.title_singular') }} {{ trans('global.list') }}</li>
                        </ul>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-12">
                     <div class="card">
                        <div class="card-header row">
                            <div class="col-lg-6"><h4 class="card-title">{{ trans('cruds.role.title_singular') }} {{ trans('global.list') }}</h4></div>
                            <div class="col-lg-6"><a href="{{ route('admin.roles.create') }}" class="btn btn-success" style="float: right;">+ {{ trans('global.add') }} {{ trans('cruds.role.title_singular') }}</a></div>
                        </div>
                        <div class="card-body">
                           <div class="table-responsive">
                              <table class="table  datanew ">
                                 <thead>
                                    <tr>
                                       <th>{{ trans('cruds.role.fields.id') }}</th>
                                       <th>{{ trans('cruds.role.fields.title') }}</th>
                                       <th>{{ trans('cruds.role.fields.permissions') }}</th>
                                       <th>Action</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    @foreach($roles as $key => $role)
                                    <tr>
                                       <td>{{ $role->id ?? '' }}</td>
                                       <td>{{ $role->name ?? '' }}</td>
                                       <td> 
                                          @foreach($role->permissions()->pluck('name') as $permission)
                                             <span class="badges bg-lightgrey">{{ $permission }}</span>
                                          @endforeach
                                       </td>
                                       <td>
                                        <a  href="{{ route('admin.roles.show', $role->id) }}"><button class="btn btn-success"><i class="fa fa-eye"></i></button></a>
                                          <a  href="{{ route('admin.roles.edit', $role->id) }}"><button class="btn btn-info info-icon-notika btn-reco-mg btn-button-mg waves-effect"><i class="fa fa-edit"></i></button></a>
                                          <form action="{{ route('admin.roles.destroy', $role->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                                      <input type="hidden" name="_method" value="DELETE">
                                                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                      <button type="submit" class="btn btn-danger danger-icon-notika btn-reco-mg btn-button-mg waves-effect"> <i class="fa fa-trash"></i></button>
                                                  </form>
                                        </td>
                                    </tr>
                                    @endforeach
                                    
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
    <script src="{{ asset('public/Admin/js/jquery-3.6.0.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/feather.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.slimscroll.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{ asset('public/Admin/plugins/apexchart/apexcharts.min.js')}}"></script>
    <script src="{{ asset('public/Admin/plugins/apexchart/chart-data.js')}}"></script>
    <script src="{{ asset('public/Admin/js/script.js')}}"></script>
   </body>
</html>