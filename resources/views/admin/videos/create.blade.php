<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
      <meta name="description" content="POS - Bootstrap Admin Template">
      <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
      <meta name="author" content="Dreamguys - Bootstrap Admin Template">
      <meta name="robots" content="noindex, nofollow">
      <title>{{ trans('global.add') }} {{ trans('cruds.videos.title_singular') }}</title>
      
      <link rel="shortcut icon" type="image/x-icon" href="{{ asset('public/Admin/img/favicon.png')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/bootstrap.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/animate.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/dataTables.bootstrap4.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/fontawesome.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/all.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/style.css')}}">
   </head>
   <body>
    <div id="global-loader">
        <div class="whirly-loader"> </div>
    </div>
    <div class="main-wrapper">
        @extends('layouts.header')
        @extends('layouts.sidebar')
        <div class="page-wrapper">
            <div class="content container-fluid">
                <div class="page-header">
                    <div class="row">
						<div class="col">
							<h3 class="page-title">{{ trans('global.add') }} {{ trans('cruds.videos.title_singular') }}</h3>
							<ul class="breadcrumb">
							   <li class="breadcrumb-item"><a href="{{route('admin.home')}}">{{ trans('global.dashboard') }}</a></li>
							   <li class="breadcrumb-item">
								@if($id)
									<a href="{{ route('admin.video.show',$id) }}">{{ trans('cruds.videos.title') }}</a>
								@else
									<a href="{{ route('admin.video-category.index') }}">{{ trans('cruds.videos.title') }}</a>
								@endif							
								</li>
							   <li class="breadcrumb-item active">{{ trans('global.add') }} {{ trans('cruds.videos.title_singular') }}</li>
							</ul>
						</div>
					</div>
				</div>
                <div class="row">
					<div class="col-sm-12">
						<div class="card">
							<div class="card-body">
								<form action="{{ route('admin.video.store') }}" method="POST" enctype="multipart/form-data">
									{{ csrf_field() }}
									<div class="form-group">
										<label>Title<span class="text-danger">*</span></label>
										<input type="text" id="video_title" name="video_title" value="{{ old('video_title') }}"  class="form-control @error('video_title') is-invalid @enderror" placeholder="{{ trans('cruds.videos.fields.video_title') }}*" >
										@if($errors->has('video_title'))
											<em class="invalid-feedback">{{ $errors->first('video_title') }}</em>
										@endif
									</div>
									<div class="form-group">
										<label>Category<span class="text-danger">*</span></label>
										<select class="form-control @error('video_category_id') is-invalid @enderror" name="video_category_id" id="video_category_id">
											<option value="">Select Video Category</option>
											@foreach($videoCategories as $key => $videoCategory)
											<option value="{{$videoCategory->id}}" {{($id == $videoCategory->id) ? 'selected': ''}}>{{$videoCategory->title}}</option>
											@endforeach
										</select>
										@if($errors->has('video_category_id'))
											<em class="invalid-feedback">{{ $errors->first('video_category_id') }}</em>
										@endif
									</div>
									<div class="form-group">
										<label>Video Link<span class="text-danger">*</span></label>
										<input type="text" id="video_link" name="video_link" value="{{ old('video_link') }}"  class="form-control @error('video_link') is-invalid @enderror" placeholder="{{ trans('cruds.videos.fields.video_link') }}*" >
										@if($errors->has('video_link'))
											<em class="invalid-feedback">{{ $errors->first('video_link') }}</em>
										@endif
									</div>
									<div class="form-group">
										<label>Upload image</label>
										<input type="file" accept="image/*" name="video_image" id="video_image" class="form-control">
										<span id="preview"></span>
									</div>
									<div class="form-group">
										<label>Video Description</label>
										<textarea class="form-control" rows="5" id="video_description" name="video_description"></textarea>
									</div>
									<div>
										<button type="submit" class="btn btn-success">Save</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
            </div>
        </div>
    </div>
    <script src="{{ asset('public/Admin/js/jquery-3.6.0.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/feather.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.slimscroll.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{ asset('public/Admin/plugins/apexchart/apexcharts.min.js')}}"></script>
    <script src="{{ asset('public/Admin/plugins/apexchart/chart-data.js')}}"></script>
    <script src="{{ asset('public/Admin/js/script.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
    <script>
$(function(){
   $("#frm_video").validate({
       rules: {
         video_title: {
            required: true,
         },
         video_category: {
            required: true,
         },
         video_link: {
            required: true,
         }
     },
      messages: { 
           video_title: {
               required: "Please video title"
           },
           video_category: {
               required: "Please select video category"
           },
           video_link: {
               required: "Please enter video link"
           },
       },
   });
});
$('#video_image').change(function(){
		const file = this.files[0];
		
		console.log(file);
		if (file){
		let reader = new FileReader();
		reader.onload = function(event){
			console.log(event.target.result);
			$("#preview").html('<img id="imgPreview" hight="50px" width="50px" src="'+event.target.result+'" alt="pic" />');
			//$('#imgPreview').attr('src', );
		}
		reader.readAsDataURL(file);
		}
	});
</script>
   </body>
</html>