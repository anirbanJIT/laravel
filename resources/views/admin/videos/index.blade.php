<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
      <meta name="description" content="POS - Bootstrap Admin Template">
      <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
      <meta name="author" content="Dreamguys - Bootstrap Admin Template">
      <meta name="robots" content="noindex, nofollow">
      <title>{{ trans('cruds.videos.title_singular') }} {{ trans('global.list') }}</title>
      
      <link rel="shortcut icon" type="image/x-icon" href="{{ asset('public/Admin/img/favicon.png')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/bootstrap.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/animate.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/dataTables.bootstrap4.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/fontawesome.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/all.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/style.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/select2/css/select2.min.css')}}">
	  <meta name="csrf-token" content="{{ csrf_token() }}">
      <link rel="stylesheet" href="{{ asset('public/Admin/tabulator/bootstrap/tabulator_bootstrap4.css')}}">
   </head>
   <body>
      <!-- <div id="global-loader">
         <div class="whirly-loader"> </div>
      </div> -->
      <div class="main-wrapper">
        @extends('layouts.header')
     
        @extends('layouts.sidebar')
        
        <div class="page-wrapper">
            <div class="content container-fluid">
                <div class="page-header">
					<div class="row">
						<div class="col">
							<h3 class="page-title">{{ trans('cruds.videos.title_singular') }} {{ trans('global.list') }}</h3>
							<ul class="breadcrumb">
							   <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">{{ trans('global.dashboard') }}</a></li>
							   <li class="breadcrumb-item"><a href="{{ route('admin.video-category.index') }}">{{ trans('cruds.video_type.title_singular') }}</a></li>
							   <li class="breadcrumb-item active">{{ trans('cruds.videos.title_singular') }} Details</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div class="card">
							<div class="card-header row">
								<div class="col-lg-6"><h4 class="card-title">{{ trans('cruds.videos.title_singular') }} {{ trans('global.list') }}</h4></div>
								<div class="col-lg-6"><a href="{{ route('admin.video.create','id='.$id)}}" class="btn btn-success" style="float: right;">+ {{ trans('global.add') }} {{ trans('cruds.videos.title_singular') }}</a> <a id="openmodel" class="btn btn-success" style=" float:right; margin-right:1%;">Rearrange</a></div>
                                
							</div>
							<div class="card-body">
							    <div class="table-responsive">
								   <div id="data-table-video-details"></div>
							    </div>
							</div>
                            <div class="card-body">
								<div class="table-responsive">
									<div id="data-table-video-category"></div>
									<input type="hidden" name="is_delete" value="0" id="is_delete">
									<div class="modal fade" id="dragdrop_model"  role="dialog" >
										<div class="modal-dialog modal-dialog-centered" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<h5 class="modal-title">Modal title</h5>
													<button type="button" class="close" data-bs-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>
												</div>
												<div class="modal-body">
													<table class="table table-bordered">
														<tr>
															<th>Id</th>
															<th>Title</th>
														</tr>
														<tbody class="row_position">
															@foreach($videos as $row)
																<tr  id="{{ $row->id }}">
																	<td>{{ $row->id }}</td>
																	<td>{{ $row->video_title }}</td>
																</tr>
															@endforeach
														</tbody>
													</table>	
												</div>
												<div class="modal-footer">
													<button type="button" onclick="updateOrder()" class="btn btn-primary">Save changes</button>
													<button type="button" data-bs-dismiss="modal" class="btn btn-secondary">Close</button>

                                                    {{ Route::currentRouteName() }}
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
            </div>
        </div>
    </div>
    <script src="{{ asset('public/Admin/js/jquery-3.6.0.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/feather.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.slimscroll.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{ asset('public/Admin/plugins/apexchart/apexcharts.min.js')}}"></script>
    <script src="{{ asset('public/Admin/plugins/apexchart/chart-data.js')}}"></script>
    <script src="{{ asset('public/Admin/js/script.js')}}"></script>
     <script src="{{ asset('public/Admin/plugins/select2/js/select2.min.js')}}"></script>
      <script src="{{ asset('public/Admin/plugins/select2/js/custom-select.js')}}"></script>
	  <script src="{{ asset('public/Admin/tabulator/tabulator.min.js') }}"></script>
      <script src="{{ asset('public/Admin/tabulator/jspdf.min.js') }}"></script>
      <script src="{{ asset('public/Admin/tabulator/xlsx.full.min.js') }}"></script>
      <script src="{{ asset('public/Admin/tabulator/jspdf.plugin.autotable.js') }}"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
      <script type="text/javascript">

      $("#openmodel").click(function(){
		  $("#dragdrop_model").modal('show');
		});
		
		$( ".row_position" ).sortable({
			delay: 150,
			stop: function() {
				var selectedData = new Array();
				$('.row_position>tr').each(function() {
					selectedData.push($(this).attr("id"));
				});
				
			}
		});
		function updateOrder() {
			var selectedData = new Array();
			$('.row_position>tr').each(function() {
				selectedData.push($(this).attr("id"));
			});
			
			var edit_url = '{{ route("admin.videoCategoryOrderPosition") }}';
            var curr_url = '{{ Route::currentRouteName() }}';
            	  
			$.ajax({
				url:edit_url,
				type:'post',
				data:{position:selectedData, currurl:curr_url},
				headers: {
						'X-CSRF-TOKEN': "{{csrf_token()}}"
					},
				success:function(){
					alert('your change successfully saved');
					window.location.reload();
				}
			});
		}
         
	
		var tabledata = <?php echo json_encode($videos); ?>;
		var url = "{{URL::asset('public/Admin/images/videos/')}}"+'/';
		var table = new Tabulator("#data-table-video-details", {
			data:tabledata,           //load row data from array
			layout:"fitColumns",      //fit columns to width of table
			responsiveLayout:"collapse",  //hide columns that dont fit on the table
			tooltips:true,            //show tool tips on cells
			addRowPos:"top",          //when adding a new row, add it to the top of the table
			history:true,             //allow undo and redo actions on the table
			pagination:"local",       //paginate the data
			paginationSize:10,         //allow 7 rows per page of data
			movableColumns:true,      //allow column order to be changed
			resizableRows:true,       //allow row order to be changed
			// initialSort:[             //set the initial sort order of the data
			// 	{column:"id", dir:"desc"},
			// ],
			columns: [
				// { data: "delete_chk", orderable:false, searchable:false},
                {title:"Id", field:"id", editor:"label",headerFilter:"input",headerFilterPlaceholder:"Id"},
				{title:"Title", field:"video_title", editor:"label",headerFilter:"input",headerFilterPlaceholder:"Title"},
				{title:"Video Category", field:"video_category.title", editor:"label",headerFilter:"input",headerFilterPlaceholder:"Video Category"},
				{title:"Video Link", field:"video_link", editor:"label",headerFilter:"input",headerFilterPlaceholder:"Video Link"},
				{title:"Image", field:"video_image", formatter:"image", 
					formatterParams:{height:"50px",width:"50px",urlPrefix:url,urlSuffix:"",}
				},
				{title:"Action",width:170, field:"id", 
					formatter:function(cell, formatterParams, onRendered) { 
						var edit_url = '{{ route("admin.video.edit",":id") }}';
						edit_url = edit_url.replace(':id', cell.getValue());

						var delete_url = '{{ route("admin.video.destroy",":id") }}';
						delete_url = delete_url.replace(':id', cell.getValue());
						var msg = "Are you sure you want to delete this Video?";
						var token = '{{ csrf_token() }}';

						var editlink = '<a href="'+edit_url+'" class="btn btn-success text-white"><i class="fa fa-edit"></i></a> <form action="'+delete_url+'" method="POST" onsubmit="return confirm(\''+msg+'\');" style="display: inline-block;"><input type="hidden" name="_method" value="DELETE"><input type="hidden" name="_token" value="'+token+'"><button type="submit" class="btn btn-danger danger-icon-notika btn-reco-mg btn-button-mg waves-effect"> <i class="fa fa-trash"></i></button></form>';
					   return editlink; 
					   
					},
				},
               
		    ],
		});
	  
         
   </script>
   </body>
</html>