<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
      <meta name="description" content="POS - Bootstrap Admin Template">
      <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
      <meta name="author" content="Dreamguys - Bootstrap Admin Template">
      <meta name="robots" content="noindex, nofollow">
      <title>Show Sub Model</title>
      
      <link rel="shortcut icon" type="image/x-icon" href="{{ asset('public/Admin/img/favicon.png')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/bootstrap.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/animate.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/dataTables.bootstrap4.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/fontawesome.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/all.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/style.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/select2/css/select2.min.css')}}">
   </head>
   <body>
      <!-- <div id="global-loader">
         <div class="whirly-loader"> </div>
      </div> -->
      <div class="main-wrapper">
        @extends('layouts.header')
     
        @extends('layouts.sidebar')
        
        <div class="page-wrapper">
            <div class="content container-fluid">
               <div class="page-header">
                  <div class="row">
                     <div class="col">
                        <h3 class="page-title">Show Sub Model</h3>
                        <ul class="breadcrumb">
                           <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">{{ trans('global.dashboard') }}</a></li>
						   <li class="breadcrumb-item">
							@if($brandSubModel->brand_models_id)
								<a href="{{ route('admin.submodels.show',$brandSubModel->brand_models_id) }}">Sub-Models</a>
							@else
								<a href="{{ route('admin.brand-type.index') }}">Sub-Models</a>
							@endif
							</li>
                           <li class="breadcrumb-item active">Show Sub Model</li>
                        </ul>
                     </div>
                  </div>
               </div>  
               <div class="row">
                  <div class="col-sm-12">
                     <div class="card">
                        <div class="card-header row">
                            <div class="col-lg-6"><h4 class="card-title">Show Sub Model</h4></div>
                            <div class="col-lg-6">
							@if($brandSubModel->brand_models_id)
								<a href="{{ route('admin.submodels.show',$brandSubModel->brand_models_id) }}" class="btn btn-success" style="float: right;">{{ trans('global.back_to_list') }}</a>
							@else
								<a href="{{ route('admin.brand-type.index') }}" class="btn btn-success" style="float: right;">{{ trans('global.back_to_list') }}</a>
							@endif
							</div>
                        </div>
                        <div class="card-body">
                           <div class="table-responsive">
                              <table class="table  datanew ">
                                 <tbody>
                                     <tr>
                                    <th>
                                        Category
                                    </th>
                                    <td>
                                        {{ $brandSubModel->brandsTypes->name }}
                                    </td>
                                </tr>
                            <tr>
                                    <th>
                                        Brand
                                    </th>
                                    <td>
                                        {{ $brandSubModel->brands->name }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        Model
                                    </th>
                                    <td>
                                        {{ $brandSubModel->brandsModels->name }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        Name
                                    </th>
                                    <td>
                                        {{ $brandSubModel->name }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        Slug
                                    </th>
                                    <td>
                                        {{ $brandSubModel->slug }}
                                    </td>
                                </tr>
                           <tr>
                                       <td>Status</td>
                                       <td>
										@if($brandSubModel->status == 1)
											{{ 'Active' }}
										@else
											{{ 'Inactive' }}
										@endif
									   </td>
                                    </tr>
                                <tr>
                                    <th>
                                        Cut Point
                                    </th>
                                    <td>
                                        {{ $brandSubModel->cut_point }}
                                    </td>
                                </tr>
                           <tr>
                                    <th>
                                        Meta Title
                                    </th>
                                    <td>
                                        {{ $brandSubModel->meta_title }}
                                    </td>
                                </tr>
                           <tr>
                                    <th>
                                        Meta Keywords
                                    </th>
                                    <td>
                                        {{ $brandSubModel->meta_keywords }}
                                    </td>
                                </tr>
                           <tr>
                                    <th>
                                        Meta Description
                                    </th>
                                    <td>
                                        {{ $brandSubModel->meta_description }}
                                    </td>
                                </tr>
                           <tr>
                                    <th>
                                        Image
                                    </th>
                                    <td>                                    
                                    @if(isset($brandSubModel) && $brandSubModel->image )
                                        <a href="{{ asset('Admin/images/submodels/'.$brandSubModel->image)}}" data-lightbox="lightbox-1"> 
                                            <img src="{{ asset('Admin/images/submodels/'.$brandSubModel->image) }}" style="width:200px;height:200px;">
                                        </a>
                              @endif
                                    </td>
                                </tr>
                                    
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
    <script src="{{ asset('public/Admin/js/jquery-3.6.0.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/feather.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.slimscroll.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{ asset('public/Admin/plugins/apexchart/apexcharts.min.js')}}"></script>
    <script src="{{ asset('public/Admin/plugins/apexchart/chart-data.js')}}"></script>
    <script src="{{ asset('public/Admin/js/script.js')}}"></script>
     <script src="{{ asset('public/Admin/plugins/select2/js/select2.min.js')}}"></script>
      <script src="{{ asset('public/Admin/plugins/select2/js/custom-select.js')}}"></script>
      
   </body>
</html>