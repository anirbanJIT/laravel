<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
      <meta name="description" content="POS - Bootstrap Admin Template">
      <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
      <meta name="author" content="Dreamguys - Bootstrap Admin Template">
      <meta name="robots" content="noindex, nofollow">
      <title>Update Sub-Model</title>
      
      <link rel="shortcut icon" type="image/x-icon" href="{{ asset('public/Admin/img/favicon.png')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/bootstrap.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/animate.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/dataTables.bootstrap4.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/fontawesome.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/all.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/style.css')}}">
   </head>
   <body>
      <div id="global-loader">
         <div class="whirly-loader"> </div>
      </div>
      <div class="main-wrapper">
        @extends('layouts.header')
     
        @extends('layouts.sidebar')
        
        <div class="page-wrapper">
            <div class="content container-fluid">
                <div class="page-header">
					<div class="row">
						<div class="col">
							<h3 class="page-title">Update Sub-Model</h3>
							<ul class="breadcrumb">
							    <li class="breadcrumb-item"><a href="{{route('admin.home')}}">{{ trans('global.dashboard') }}</a></li>
							    <li class="breadcrumb-item">
								   @if($brand_sub_model->brand_models_id)
										<a href="{{ route('admin.submodels.show',$brand_sub_model->brand_models_id) }}">Sub-Models</a>
									@else
										<a href="{{ route('admin.brand-type.index') }}">Sub-Models</a>
									@endif
								</li>
							    <li class="breadcrumb-item active">Update Sub-Model</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div class="card">
							<div class="card-body">
								<form action="{{ route('admin.submodels.update', [$brand_sub_model->id]) }}" method="POST" enctype="multipart/form-data">
									{{ csrf_field() }}
									@method('PUT')
									<input type="hidden" name="get_model_id" value="{{ $brand_sub_model->brand_models_id }}">
									<div class="form-group">
										<label>Category<span class="text-danger">*</span></label>
										<select class="form-control @error('brand_type_id') is-invalid @enderror" name="brand_type_id" id="brand_type_id"  >
											<option value="">Select Category</option>
											@foreach($brand_types as $id => $brand_type)
												<option value="{{ $brand_type->id }}" {{ (isset($brand_sub_model) && $brand_sub_model->brand_type_id ? $brand_sub_model->brand_type_id : old('brand_type_id')) == $brand_type->id ? 'selected' : '' }} >{{ $brand_type->name }}</option>
											@endforeach
										</select>
										@if($errors->has('brand_type_id'))
											<em class="invalid-feedback">{{ $errors->first('brand_type_id') }}</em>
										@endif
									</div>
									<div class="form-group">
										<label>Brands<span class="text-danger">*</span></label>
										<select class="form-control @error('brand_id') is-invalid @enderror" name="brand_id" id="brand_id">
											<option value="">Select Brand </option>
											@foreach($brands as $id => $brand)
												<option value="{{ $brand->id }}" {{ (isset($brand_sub_model) && $brand_sub_model->brand_id ? $brand_sub_model->brand_id : old('brand_id')) == $brand->id ? 'selected' : '' }} >{{ $brand->name }}</option>
											@endforeach
										</select>
										@if($errors->has('brand_id'))
											<em class="invalid-feedback">{{ $errors->first('brand_id') }}</em>
										@endif
									</div>
									<div class="form-group">
										<label>Models<span class="text-danger">*</span></label>
										<select class="form-control @error('brand_models_id') is-invalid @enderror" name="brand_models_id" id="brand_models_id"   >
											<option value="">Select Model </option>
											@foreach($models as $id => $model)
												<option value="{{ $model->id }}" {{ (isset($brand_sub_model) && $brand_sub_model->brand_models_id ? $brand_sub_model->brand_models_id : old('brand_models_id')) == $model->id ? 'selected' : '' }} >{{ $model->name }}</option>
											@endforeach
										</select>
										@if($errors->has('brand_models_id'))
											<em class="invalid-feedback">{{ $errors->first('brand_models_id') }}</em>
										@endif
									</div>
									<div class="form-group">
										<label>Sub-model Name<span class="text-danger">*</span></label>
										<input type="text" class="form-control @error('name') is-invalid @enderror" value="{{ old('name', isset($brand_sub_model) ? $brand_sub_model->name : '') }}" placeholder="Sub Model Name" id="name" name="name">
										@if($errors->has('name'))<span class="text-danger">{{ $errors->first('name') }}</span>@endif
									</div>
									<div class="form-group">
										<label>Cut Point</label>
										<input type="number" class="form-control" placeholder="Cut Point" value="{{ old('cut_point', isset($brand_sub_model) ? $brand_sub_model->cut_point : '') }}" id="cut_point" name="cut_point" min="0">
									</div>
									<div class="form-group">
										<label>Status<span class="text-danger">*</span></label>
										<select class="form-control @error('status') is-invalid @enderror" name="status" id="status"  required >
											<option value="0" {{ (isset($brand_sub_model) && $brand_sub_model->status ? $brand_sub_model->status : old('status')) == 0 ? 'selected' : '' }} >Inactive</option>
											<option value="1" {{ (isset($brand_sub_model) && $brand_sub_model->status ? $brand_sub_model->status : old('status')) == 1 ? 'selected' : 'selected' }}>Active</option>
										</select>   
										@if($errors->has('status'))<span class="text-danger">{{ $errors->first('status') }}</span>@endif
									</div>
									<div class="form-group">
										<label>Upload image</label>
										<input type="file" accept="image/*" class="form-control" name="fileUpload" id="inputImage">
									</div>
									<span id="preview">
										@if(isset($brand_sub_model) && $brand_sub_model->image )
											<a href="{{ asset('Admin/images/submodels/'.$brand_sub_model->image)}}" data-lightbox="lightbox-1"> 
												<img src="{{ asset('Admin/images/submodels/'.$brand_sub_model->image)}}" alt="image-preview" width="50"/>
											</a>
										@endif 
									</span>
									<div class="form-group">
										<label>Meta Title</label>
										<input type="text" class="form-control" placeholder="Meta Title" id="metaTitle" name="metaTitle" value="{{ $brand_sub_model->meta_title }}">
										@if($errors->has('metaTitle')) <em class="invalid-feedback"> {{ $errors->first('metaTitle') }} </em> @endif
									</div>
									<div class="form-group">
										<label>Meta Keyword</label>
										<input type="text" class="form-control"  placeholder="Meta Keyword" id="metaKeyword" name="metaKeyword" value="{{ $brand_sub_model->meta_keywords }}">
										@if($errors->has('metaKeyword')) <em class="invalid-feedback"> {{ $errors->first('metaKeyword') }} </em> @endif
									</div>
									<div class="form-group">
										<label>Meta Description</label>
										<input type="text" class="form-control"  placeholder="Meta Description" id="metaDescription" name="metaDescription" value="{{ $brand_sub_model->meta_description }}">
										@if($errors->has('metadescription')) <em class="invalid-feedback"> {{ $errors->first('metadescription') }} </em> @endif
									</div>
									<div>
										<button type="submit" class="btn btn-success">Save</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
            </div>
        </div>
    </div>
    <script src="{{ asset('public/Admin/js/jquery-3.6.0.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/feather.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.slimscroll.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{ asset('public/Admin/plugins/apexchart/apexcharts.min.js')}}"></script>
   
    <script src="{{ asset('public/Admin/js/script.js')}}"></script>
	<script>
		
		$('#inputImage').change(function(){
			const file = this.files[0];
			
			console.log(file);
			if (file){
			let reader = new FileReader();
			reader.onload = function(event){
				console.log(event.target.result);
				$("#preview").html('<img id="imgPreview" hight="50px" width="50px" src="'+event.target.result+'" alt="pic" />');
				//$('#imgPreview').attr('src', );
			}
			reader.readAsDataURL(file);
			}
		});
		

	</script>
   </body>
</html>