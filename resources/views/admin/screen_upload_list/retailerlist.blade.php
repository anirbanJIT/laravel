@extends('layouts.admin')

@section('content')

<meta name="csrf-token" content="{{ csrf_token() }}">

<div class="breadcomb-area">

		<div class="container">

			<div class="row">

					<div class="row">

				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

					<div class="breadcomb-list">

						<div class="row">

							<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">

								<div class="breadcomb-wp">

									<div class="breadcomb-icon">

										<i class="notika-icon notika-windows"></i>

									</div>

									<div class="breadcomb-ctn">

										<h2>Retailer Design List</h2>

										<p>Show <span class="bread-ntd">all list</span></p>

									</div>

								</div>

							</div>

								

									

								

							

						</div>

					</div>

				</div>

			</div>

			</div>

		</div>

	</div>

	

 <!-- Data Table area Start-->

    <div class="data-table-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
               <div class="data-table-list">
                    <div class="table-responsive">
                        <table id="datatable-customer-list" class="table table-responsive">
                            <thead>
                                <tr role="row" class="heading">
                                    <th width="5%">ID</th>
                                    
                                    <th width="15%">Retailer Name</th>
                                    <th width="10%">Design</th>
                                    
                                    <th width="15%">Action</th>
                                    
                                </tr>
                                <tr role="row" class="filterrow">
                                    <th class="select-filter">ID</th>
                                    
                                    <th class="select-filter">Retailer Name</th>
                                    <th class="select-filter">Design</th>
                                    
                                    
                                    <th>Action</th>
                                </tr>
                            </thead>
                        <tbody>
                            @foreach($retailerlist as $key => $user)
                                <tr data-entry-id="{{ $user->userid }}">
                                    <td>{{ $user->userid }}</td>
                                   
                                    <td>{{ $user->username }}</td>
                                    <td>{{ $user->design }}</td>
                                    
                                    <td><a  href="{{ route('admin.screenuploadlist', $user->userid) }}"><button class="btn btn-success success-icon-notika btn-reco-mg btn-button-mg waves-effect" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="View"><i class="notika-icon notika-menus"></i></button></a></td>
                                    
                                </tr>
                            @endforeach
                        </tbody>
                            <tfoot>
                                <tr>
                                    <th width="5%">ID</th>
                                    
                                    <th width="15%">Retailer Name</th>
                                    <th width="10%">Design</th>
                                    
                                    
                                    <th width="15%">Action</th>
                                </tr>
                            </tfoot>
                        </table>                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

	

	<script type="text/javascript">

$(document).ready(function (){
    /* @author Naren Thaker @narenthaker.com 101220201500 */
    window._token = $('meta[name="csrf-token"]').attr('content');

    $.extend(true, $.fn.dataTable.Buttons.defaults.dom.button, { className: 'btn' })

    $.extend(true, $.fn.dataTable.defaults, {
        columnDefs: [
        { type: 'date', targets: 3 }, 
        {
            orderable: false,
            searchable: false,
            targets: [-1]
        }],
        order: [[ 0, 'desc' ]],
        lengthMenu: [
            [ 10, 25, 50, 100, -1 ],
            [ '10', '25', '50', '100', 'All' ]
        ],
        buttons: [],
        orderCellsTop: true,
        dom: 'lBfrtip<"actions">',
        fixedHeader: true,
    });

    let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons);
    var table = $('#datatable-customer-list:not(.ajaxTable)').DataTable({
        buttons: dtButtons,
        initComplete: function() {
            this.api().columns([0,1,2]).every(function(d) {
                var column = this;
                var theadname = $('#datatable-customer-list thead th').eq([d]).text();
                var select = $('<select class ="filter searchSelect"><option value="">'+theadname+': All</option></select>')
                             .appendTo($("#datatable-customer-list thead tr:eq(1) th").eq(column.index()).empty() )
                             .tailselect({ search: true, deselect: true, width: 130, height: 200, placeholder: theadname});
                select.on("change", function(){
                    var val = $.fn.dataTable.util.escapeRegex(this.value());
                    column.search(val? ('^' + val + '$'): '', true, false).draw();
                });                
                column.data().unique().sort().each(function(d, j) {
                    select.options.add(d, d, '#');
                });
                select.query();
            });
        }
    });
    table.on('draw', function() {
        table.columns([0,1,2,3]).indexes().each(function(idx) {
            var select = $("#datatable-customer-list thead tr:eq(1) th").eq(idx).find('select').tailselect('.select');
            if (select.value() === null) {
                select.reload();                
                table.column(idx, {
                    search: 'applied'
                }).data().unique().sort().each(function(d, j) {
                    select.options.add(d, d);
                });

                select.query();
            }
        });
    });
});
</script>


@endsection



