@extends('layouts.admin')
@section('content')
<style type="text/css">
.word-break {
    word-break: break-all;
    width: 150px;
}
.dataTables_scrollHead{
	overflow: unset !important;
}
</style>
<div class="breadcomb-area">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="breadcomb-list">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="breadcomb-wp">
                                    <div class="breadcomb-icon">
                                        <i class="notika-icon notika-windows"></i>
                                    </div>
                                    <div class="breadcomb-ctn">
                                        <h2>Model Screen List</h2>
                                        <p>Show <span class="bread-ntd">all list</span></p>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Data Table area Start-->
    <div class="data-table-area">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="data-table-list">
                        <div class="table-responsive">
                            <table class="table table-striped datatable-models-screen-image" >
                                <thead>
                                    <tr role="row" class="heading">
                                        <th width="10"></th>
                                        <th width="110"> Model Screen {{ trans('cruds.brand_type.fields.id') }} </th>
                                        
                                        <th width="200"> {{ trans('cruds.brands.title_singular') }} {{ trans('cruds.brands.fields.name') }} </th>                                        
                                        <th width="200"> {{ trans('cruds.models.title_singular') }} {{ trans('cruds.models.fields.name') }} </th>
                                        <th width="200"> Image File </th>
                                        <th width="200"> plt file </th>
                                        <th width="200"> Upload by </th>
                                        <th width="15%"> {{ trans('cruds.brand_type.fields.status') }} </th>
                                        <th width="12%"> {{ trans('cruds.brand_type.fields.actions') }} </th>
                                    </tr>
                                    <tr role="row" class="filterrow">
                                        <th width="10"></th>
                                        <th width="110"> {{ trans('cruds.brand_type.fields.id') }} </th>
                                        
                                        <th width="200"> {{ trans('cruds.brands.title_singular') }} {{ trans('cruds.brands.fields.name') }} </th>
                                        <th width="200"> {{ trans('cruds.models.title_singular') }} {{ trans('cruds.models.fields.name') }} </th>
                                        <th width="200"> Imagefile </th>
                                        <th width="200"> plt file </th>
                                        <th width="200"> Upload by </th>
                                        <th class="select-filter">
                                            <select name="status" class="select-search filter searchSelect">
                                                <option value="">Status</option>
                                                <option value="1">Active</option>
                                                <option value="0">Inactive</option>
                                            </select>
                                        </th>
                                        <th>
                                            <button class="btn btn-danger danger-icon-notika waves-effect filter-cancel" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Reset" id="reset"><i class="notika-icon notika-close"></i></button>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($brandModelScreen as $modelScreen)
                                    <tr data-entry-id="{{ $modelScreen->id }}">
                                        <td>
                                        </td>
                                        <td>
                                            {{ $modelScreen->id ?? '' }}
                                        </td>

                                        <td class="word-break ">
                                            {{ $modelScreen->brand_id ?? '' }}
                                        </td>
                                        <td class="word-break ">
                                            {{ $modelScreen->model_id ?? '' }}
                                        </td>
                                        <td class="word-break ">
                                            @php 
                                                $myArray = json_decode($modelScreen->image, True);
                                            @endphp

						                @foreach($myArray as $key => $value)
                                        
                                        @isset($myArray[$key])
							               <a href="{{ asset('images/models-screen-image/'.$value) }}" data-lightbox="lightbox-{{$key}}"> 
                                <img src="{{ asset('images/models-screen-image/'.$value) }}" class="animate-eleven" style="width:100px;height:100px; margin: 5px 5px 5px 0;">
                            </a>
                                        @endisset
							@endforeach
                                        </td>
                                        <td class="word-break ">
                                            @php 
                                                $bltArray = json_decode($modelScreen->blt_file, True); 
                                            @endphp

						                @foreach($bltArray as $key => $value)
                                        
                                        @isset($bltArray[$key])
							                <a href="{{ asset('images/models-screen-image/'.$bltArray[$key] ?? null) }}" target="download">{{$bltArray[$key] ?? null}}</a>
                                        @endisset
							@endforeach

                                        </td>
                                       <td class="word-break ">
                                            {{ $modelScreen->username ?? '' }}
                                        </td>
                                         <td data-filter="{{ $modelScreen->status ?? '' }}" data-search="{{ $modelScreen->status ?? '' }}">
                                            <div class="nk-toggle-switch" data-ts-color="green">
                                            <input class="status_change" data-id="{{$modelScreen->id}}" id="ts{{$modelScreen->id}}" type="checkbox" hidden="hidden" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" data-off="InActive" {{ $modelScreen->status ? 'checked' : '' }}>
                                            <label for="ts{{$modelScreen->id}}" class="ts-helper"></label>
                                        </div>
                                        </td>
                                        <td>
                                           <a  href="{{ route('admin.viewuploadlistbyretailer', $modelScreen->id) }}"><button class="btn btn-success success-icon-notika btn-reco-mg btn-button-mg waves-effect" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="View"><i class="notika-icon notika-menus"></i></button></a>
                                        </td>
                                    </tr>
                                   @endforeach
                                </tbody>
                                <tfoot>
                                    <th width="10"></th>
                                        <th width="110"> {{ trans('cruds.brand_type.fields.id') }} </th>
                                        
                                        <th width="200"> {{ trans('cruds.brands.title_singular') }} {{ trans('cruds.brands.fields.name') }} </th>                                        
                                        <th width="200"> {{ trans('cruds.models.title_singular') }} {{ trans('cruds.models.fields.name') }} </th>
                                        <th width="200"> Imagefile </th>
                                        <th width="200"> plt file </th>
                                        <th width="200"> Upload by </th>
                                        <th width="15%"> {{ trans('cruds.brand_type.fields.status') }} </th>
                                        <th width="12%"> {{ trans('cruds.brand_type.fields.actions') }} </th>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts') 
<script>
$(function () {    
    $('.sa-warning').on('click', function (e) {
        event.preventDefault();
        const url =  $(this).attr('href');
        swal({
            title: "Are you sure?",  
            text: "Once Deleted. You will also able to recover this again!",  
            type: "warning",  
            showCancelButton: true,  
            confirmButtonText: "Yes, delete it!",
        }).then(function(value) {
            if (value) {window.location.href = url;}
        });
    });    
    
    $('.status_change').change(function() {     
        var status = $(this).prop('checked') == true ? 1 : 0; 
        var modelScreen_id = $(this).data('id'); 
         $.ajax({
           type:"GET",
           url:"{{url('admin/changeStatusModelScreenofretailer')}}?status="+status+"&modelScreen_id="+modelScreen_id,
           success:function(res){               
            if(res){
                console.log(res.success);
            }
           }
        });
    }); 
});
</script>    
<script type="text/javascript">
$(document).ready(function (){
    /* @author Naren Thaker @narenthaker.com 101220201500 */
    window._token = $('meta[name="csrf-token"]').attr('content');
    $('.select-all').click(function () {
      let $select2 = $(this).parent().siblings('.select2')
      $select2.find('option').prop('selected', 'selected')
      $select2.trigger('change')
    })
    $('.deselect-all').click(function () {
      let $select2 = $(this).parent().siblings('.select2')
      $select2.find('option').prop('selected', '')
      $select2.trigger('change')
    })
    $.extend(true, $.fn.dataTable.Buttons.defaults.dom.button, { className: 'btn' })
     $.extend(true, $.fn.dataTable.defaults, {
        columnDefs: [{
            orderable: false,
            className: 'select-checkbox',
            checkboxes: {
               'selectRow': true
            },
            selector: 'td:first-child',
            targets: 0
        }, { type: 'date', targets: 8 }, {
            orderable: false,
            searchable: false,
            targets: [0,-1]
        },{
            orderable: false,
            targets: -2
        }],
        order: [[ 1, 'desc' ]],
        lengthMenu: [
            [ 10, 25, 50, 100, -1 ],
            [ '10', '25', '50', '100', 'All' ]
        ],
        buttons: [],
        orderCellsTop: true,
        dom: 'lBfrtip<"actions">',
       
    });
    let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons);
    let deleteButtonTrans = '{{ trans('global.datatables.delete') }}';
    let deleteButton = {
        text: deleteButtonTrans,
        url: "{{ route('admin.models-screen-image.mass_destroy') }}",
        className: 'btn btn-success notika-btn-success waves-effect',
        action: function (e, dt, node, config) {
          var ids = [];
          $("input:checkbox[class=dt-checkboxes]:checked").each(function () {
             ids.push( $(this).parent().parent().data('entry-id')  );
          });
          if (ids.length === 0) {
            alert('{{ trans('global.datatables.zero_selected') }}')
            return
          }
          if (confirm('{{ trans('global.areYouSure') }}')) {
            $.ajax({
              headers: {'x-csrf-token': _token},
              method: 'POST',
              url: config.url,
              data: { ids: ids, _method: 'DELETE' }})
              .done(function () { location.reload() })
          }
        }
    }
    dtButtons.push(deleteButton);
    var table = $('.datatable-models-screen-image:not(.ajaxTable)').DataTable({ 
        buttons: dtButtons,
        initComplete: function() {
            this.api().columns([1,2,3,4,5,6]).every(function(d) {
                var column = this;
                var theadname = $('.datatable-models-screen-image thead th').eq([d]).text();
                var select = $('<select class ="filter searchSelect"><option value="">'+theadname+': All</option></select>')
                             .appendTo($(".datatable-models-screen-image thead tr:eq(1) th").eq(column.index()).empty() )
                             .tailselect({ search: true, deselect: true, width: 130, height: 150, placeholder: theadname});
                select.on("change", function(){
                    var val = $.fn.dataTable.util.escapeRegex(this.value());
                    column.search(val? ('^' + val + '$'): '', true, false).draw();
                });                
                column.data().unique().sort().each(function(d, j) {
                    select.options.add(d, d, '#');
                });
                select.query();

            });
        }
    });
    table.on('draw', function() {
        table.columns([1,2,3,4,5,6]).indexes().each(function(idx) {
            var select = $(".datatable-models-screen-image thead tr:eq(1) th").eq(idx).find('select').tailselect('.select');
            if (select.value() === null) {
                select.reload();                
                table.column(idx, {
                    search: 'applied'
                }).data().unique().sort().each(function(d, j) {
                    select.options.add(d, d);
                });

                select.query();
            }
        });
    });

    $('.select-search').change( function (e) {
        console.log(this.value);
        table.column( 9 ).search( this.value ).draw(); 
    });

    /* start data filter */
    $(".date-picker").datepicker( {
        format: "dd/mm/yyyy",
         autoclose: true,
    }).on('change', function (ev) {
        $(this).datepicker('hide');
        table.draw();
    });

    $.fn.dataTableExt.afnFiltering.push( function(settings, data, dataIndex){
        var dateStart = parseDateValue($('#date_from').val());
        var dateEnd = parseDateValue($("#date_to").val());
        var evalDate= parseDateValue(data[8]);
        if ( ( isNaN( dateStart ) && isNaN( dateEnd ) ) ||
                 ( isNaN( dateStart ) && evalDate <= dateEnd ) ||
                 ( dateStart <= evalDate && isNaN( dateEnd ) ) ||
                 ( dateStart <= evalDate && evalDate <= dateEnd ) )
        {
            return true;
        }
        else {
            return false;
        }
    });

    function parseDateValue(rawDate) {
        var dateArray= rawDate.split("/");
        var parsedDate= dateArray[2] + dateArray[0] + dateArray[1];
        return parsedDate;
    }
    /* end data filter */

    $('#reset').click( function (e) {
        $(".date-picker").datepicker('setDate', null);
        //tailselect('.select-search').reload();
    } ); 

    tail.select(".select-search", {
        search: true, deselect: true, width: 130, height: 150, placeholder: 'Status'
    });

    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
        $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust();
    });
});
$('.dataTables_scrollHead').css("overflow", "unset");
</script>
@endpush