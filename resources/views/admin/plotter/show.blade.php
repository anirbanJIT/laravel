<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
		<meta name="description" content="POS - Bootstrap Admin Template">
		<meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
		<meta name="author" content="Dreamguys - Bootstrap Admin Template">
		<meta name="robots" content="noindex, nofollow">
		<title>{{ trans('global.show') }} Plotter</title>
      
		<link rel="shortcut icon" type="image/x-icon" href="{{ asset('public/Admin/img/favicon.png')}}">
		<link rel="stylesheet" href="{{ asset('public/Admin/css/bootstrap.min.css')}}">
		<link rel="stylesheet" href="{{ asset('public/Admin/css/animate.css')}}">
		<link rel="stylesheet" href="{{ asset('public/Admin/css/dataTables.bootstrap4.min.css')}}">
		<link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/fontawesome.min.css')}}">
		<link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/all.min.css')}}">
		<link rel="stylesheet" href="{{ asset('public/Admin/css/style.css')}}">
	</head>
	<body>
		<div id="global-loader">
			<div class="whirly-loader"> </div>
		</div>
		<div class="main-wrapper">
			@extends('layouts.header')
			@extends('layouts.sidebar')
        	<div class="page-wrapper">
				<div class="content container-fluid">
					<div class="page-header">
						<div class="row">
							<div class="col">
								<h3 class="page-title">{{ trans('global.show') }} Plotter</h3>
								<ul class="breadcrumb">
								   <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">{{ trans('global.dashboard') }}</a></li>
								   <li class="breadcrumb-item"><a href="{{ route('admin.plotter.index') }}">Plotters</a></li>
								   <li class="breadcrumb-item active">{{ trans('global.show') }} Plotter</li>
								</ul>
							</div>
						</div>
					</div>
				    <div class="row">
						<div class="col-sm-12">
							<div class="card">
								<div class="card-header row">
									<div class="col-lg-6"><h4 class="card-title">{{ trans('global.show') }} Plotter</h4></div>
									<div class="col-lg-6"><a href="{{ url()->previous() }}" class="btn btn-success" style="float: right;">{{ trans('global.back_to_list') }}</a></div>
								</div>
								<div class="card-body">
									<div class="table-responsive">
										<table class="table table-border">
											<tbody>
												<tr>
												   <td>Plotter Id</th>
												   <td>{{ $plotter->id }}</th>
												</tr>
												<tr>
												   <td>Plotter Name</td>
												   <td>{{ $plotter->name }}</td>
												</tr>
												<tr>
												   <td>Plotter Speed</td>
												   <td>{{ isset($plotter->speed) ? $plotter->speed : "-" }}</td>
												</tr>
												<tr>
												   <td>Plotter Pressure</td>
												   <td>{{ isset($plotter->pressure) ? $plotter->pressure : "-" }}</td>
												</tr>
												<tr>
												   <td>Plotter Status</td>
												   <td>
													@if($plotter->status == '1')
														{{ 'Enable' }}
													@else
														{{ 'Disable' }}
													@endif
												   </td>
												</tr>
												<tr>
													<td>Image</td>
												   <td>@if(isset($plotter) && $plotter->image )
														<a href="{{ asset('Admin/images/plotters/'.$plotter->image)}}" data-lightbox="lightbox-1"> 
															<img src="{{ asset('Admin/images/plotters/'.$plotter->image) }}" style="width:200px;height:200px;">
														</a>
													@endif</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<script src="{{ asset('public/Admin/js/jquery-3.6.0.min.js')}}"></script>
		<script src="{{ asset('public/Admin/js/feather.min.js')}}"></script>
		<script src="{{ asset('public/Admin/js/jquery.slimscroll.min.js')}}"></script>
		<script src="{{ asset('public/Admin/js/jquery.dataTables.min.js')}}"></script>
		<script src="{{ asset('public/Admin/js/dataTables.bootstrap4.min.js')}}"></script>
		<script src="{{ asset('public/Admin/js/bootstrap.bundle.min.js')}}"></script>
		<script src="{{ asset('public/Admin/plugins/apexchart/apexcharts.min.js')}}"></script>
		<script src="{{ asset('public/Admin/plugins/apexchart/chart-data.js')}}"></script>
		<script src="{{ asset('public/Admin/js/script.js')}}"></script>
	</body>
</html>