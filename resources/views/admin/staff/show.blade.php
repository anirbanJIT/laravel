@extends('layouts.admin')
@section('content')
<div class="breadcomb-area">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="breadcomb-list">
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<div class="breadcomb-wp">
								<div class="breadcomb-icon">
									<i class="notika-icon notika-windows"></i>
								</div>
								<div class="breadcomb-ctn">
									<h2>{{ $staff->name ? $staff->name : '' }} Details</h2>
									
								</div>
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-3">
							<div class="breadcomb-report">
								<a class="btn btn-success notika-btn-success waves-effect" href="{{ url()->previous() }}">
								{{ trans('global.back_to_list') }}
						  		</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="tabs-info-area">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="form-example-wrap mg-t-30">
					<div class="cmp-tb-hd cmp-int-hd">
						<h2>KYC Details</h2>
					</div>
			
					@if(!empty($data['current_location']) && !empty($data['lat_lan']))
					<div class="form-example-int form-horizental">
						<div class="form-group">
							<div class="row">
								<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-center">
									<div class="nk-int-st">
										<p>
											<a href="{{url(asset('images/kyc/'.$data['shop_visiting_card']))}}" target="_blank" download>
												<img src="{{asset('images/kyc/'.$data['shop_visiting_card'])}}" style="width:auto; height: 200px;" alt="...">
											</a>
										</p>
									</div>
									<strong>Shop Visiting Card</strong>
								</div>
								<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-center">
									<div class="nk-int-st">
										<p>
											<a href="{{url(asset('images/kyc/'.$data['shop_pic_with_selfie']))}}" target="_blank" download>
												<img src="{{asset('images/kyc/'.$data['shop_pic_with_selfie'])}}" style="width:auto; height: 200px;" alt="...">
											</a>
										</p>
									</div>
									<strong>Shop Pic With Selfie</strong>
								</div>
							</div>
						</div>
					</div>
					<div class="form-example-int form-horizental">
						<div class="form-group">
							<div class="row">
								<div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
									<label class="hrzn-fm"><strong>Current Location</strong></label>
								</div>
								<div class="col-lg-8 col-md-7 col-sm-7 col-xs-12">
									<div class="nk-int-st">
										{{ $data['current_location'] }}
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="form-example-int form-horizental mg-t-15">
						<div class="form-group">
							<div class="row">
								<div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
									<label class="hrzn-fm"><strong>Lat-Lan</strong></label>
								</div>
								<div class="col-lg-8 col-md-7 col-sm-7 col-xs-12">
									<div class="nk-int-st">
										{{ $data['lat_lan'] }}
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="form-example-int form-horizental">
						<div class="form-group">
							<div class="row">
								<div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
									<label class="hrzn-fm"><strong>Approve Notes</strong></label>
								</div>
								<div class="col-lg-8 col-md-7 col-sm-7 col-xs-12">
									<div class="nk-int-st">
										{{ $data['notes'] }}
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="form-example-int form-horizental">
						<div class="form-group">
							<div class="row">
								<div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
									<label class="hrzn-fm"><strong>Status</strong></label>
								</div>
								<div class="col-lg-8 col-md-7 col-sm-7 col-xs-12">
									<div class="nk-int-st">
										{!! $data['status'] == 0 ? '<h4 style= "color:red;">Unapproved</h4>' : '<h4 style= "color:green;">Approved</h4>' !!}
									</div>
								</div>
							</div>
						</div>
					</div>
					@else
					<div class="form-example-int form-horizental">
						<div class="form-group">
							<div class="row">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									<div class="nk-int-st">
										<h4 style= "color:red;">KYC  detail is not updated by {{ $staff->name }} </h4>	
									</div>
								</div>
							</div>
						</div>
					</div>
					@endif
				</div>
			</div>
		</div>                        
 	</div>
</div>
@endsection