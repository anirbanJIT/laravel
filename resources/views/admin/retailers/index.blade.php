@extends('layouts.admin')
@section('content')

<div class="breadcomb-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="breadcomb-list">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="breadcomb-wp">
                                <div class="breadcomb-icon">
                                    <i class="notika-icon notika-windows"></i>
                                </div>
                                <div class="breadcomb-ctn">
                                    <h2>Retailers</h2>
                                    <p> <span class="bread-ntd">all list</span></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-3">
                            <div class="breadcomb-report">
                                <a class="btn btn-success notika-btn-success waves-effect" href="{{ route('admin.retailers.create') }}">
                                    {{ trans('global.add') }} {{ trans('cruds.retailer.title_singular') }}
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Data Table area Start-->
<div class="data-table-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="data-table-list">
                    <!--<div class="basic-tb-hd">
                        <h2>Basic Example</h2>
                        <p>It's just that simple. Turn your simple table into a sophisticated data table and offer your users a nice experience and great features without any effort.</p>
                    </div>-->
                    <div class="table-responsive">
                        <table id="data-table-basic" class="table table-striped">
                            <thead>
                                <tr>
                                    <th width="10"> </th>     								   
                                    <th>
                                        {{ trans('cruds.user.fields.name') }}
                                    </th>
                                    <th>
                                        {{ trans('cruds.user.fields.email') }}
                                    </th>
                                    <th>Status</th>

                                    <th> &nbsp;  </th>
                                </tr>                       
                            </thead>
                            <tbody>           

                                @foreach($retailerUsers as $key => $retailerUser)      

                                <tr data-entry-id="{{ $retailerUser ->id }}">
                                    <td> </td>
                                    <td>{{ $retailerUser->name ?? '' }}  </td>
                                    <td width="100">{{ $retailerUser->email ?? '' }}  </td>    
                                    <td>
                                        {{-- <div class="nk-toggle-switch" data-ts-color="green">
                                            <input class="status_change" data-id="{{$retailerUser->id}}" id="ts{{$retailerUser->id}}" type="checkbox" hidden="hidden" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" data-off="InActive" {{ $retailerUser->status ? 'checked' : '' }}>
                                            <label for="ts{{$retailerUser->id}}" class="ts-helper"></label>
                                        </div> --}}
                                        @if($retailerUser->status == 1)
                                               <!-- <button class="btn btn-success success-icon-notika btn-reco-mg btn-button-mg waves-effect" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Active"><i class="notika-icon notika-checked"></i></button> -->
                                        <form action="{{url('admin/kyc/adminChangeStatus')}}" method="post"  onsubmit="return confirm('Are you sure you want to change status');">
                                            <input type="hidden" name="id" value="{{$retailerUser->id}}">
                                            <input type="hidden" name="status" value="{{$retailerUser->status}}">
                                            @csrf
                                            <button  type="submit" class="btn btn-success success-icon-notika btn-reco-mg btn-button-mg waves-effect" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Active"><i class="notika-icon notika-checked"></i></button>
                                        </form>
                                        @else
                                             <!-- <button class="btn btn-orange orange-icon-notika waves-effect" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Inactive"><i class="notika-icon notika-close"></i></button> -->
                                        <form action="{{url('admin/kyc/adminChangeStatus')}}" method="post"  onsubmit="return confirm('Are you sure you want to change status');">
                                            <input type="hidden" name="id" value="{{$retailerUser->id}}">
                                            <input type="hidden" name="status" value="{{$retailerUser->status}}">
                                            @csrf
                                            <button  type="submit" class="btn btn-success success-icon-notika btn-reco-mg btn-button-mg waves-effect" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Inactive"><i class="notika-icon notika-close"></i></button>
                                        </form>
                                        @endif						

                                    </td>


                                    <td>



                                        <a  href="{{ route('admin.retailers.show', $retailerUser->id)}}"><button class="btn btn-success success-icon-notika btn-reco-mg btn-button-mg waves-effect" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="View Details & Approve KYC"><i class="notika-icon notika-menus"></i></button></a>																
                                        <a  href="{{ route('admin.retailers.edit', $retailerUser->id) }}"><button class="btn btn-info info-icon-notika btn-reco-mg btn-button-mg waves-effect" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Edit"><i class="notika-icon notika-edit"></i></button></a>
                                        <form action="{{ route('admin.retailers.destroy', $retailerUser->id) }} " method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display:none;">
                                            <input type="hidden" name="_method" value="DELETE">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <button type="submit" class="btn btn-danger danger-icon-notika btn-reco-mg btn-button-mg waves-effect" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Delete"> <i class="notika-icon notika-close"></i></button>
                                        </form>

                                        <a href="#user_password_change"><button class="btn btn-primary primary-icon-notika btn-reco-mg btn-button-mg waves-effect" data-toggle="modal"   data-placement="bottom" title="" data-original-title="Change Password"  href="#user_password_change" data-id="{{ $retailerUser->id }}" data-url="{{ route('admin.retailer-change-password', ['user' => $retailerUser->id]) }}" data-toggle="modal"><i class="notika-icon notika-promos"></i></button></a>

                                        <a  href="{{ route('admin.setting.commission', $retailerUser->id) }}"><button class="btn btn-amber amber-icon-notika btn-reco-mg btn-button-mg waves-effect" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Commission Setting"><i class="notika-icon notika-star"></i></button></a>

                                        <a  href="{{ route('admin.generalnotification.create', ['user_id' => $retailerUser->id]) }}"><button class="btn btn-amber amber-icon-notika btn-reco-mg btn-button-mg waves-effect" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Send Notification"><i class="notika-icon notika-chat"></i></button></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th width="10">	</th>  
                                    <th>{{ trans('cruds.user.fields.id') }}</th>
                                    <th> {{ trans('cruds.user.fields.name') }} </th> 
                                    <th>{{ trans('cruds.user.fields.email') }}  </th>
                                    <th> &nbsp; </th> 

                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal Change Password start -->
<div class="modal fade" id="user_password_change" tabindex="-1" role="dialog" aria-labelledby="user_password_change" aria-hidden="true">
    <div class="modal-dialog modal-xs" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="user_password_change">Change Password?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="" method="POST" id="frm_password_change">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label>New Password</label>
                        <input type="text" class="form-control" name="password" maxlength="30" id="password" required="" pattern=".{6,}" title="Password must be at least 6 characters" x-moz-errormessage="Password must be at least 6 characters">                        
                        @if ($errors->has('password'))
                        <span class="text-danger">{{ $errors->first('password') }}</span>
                        @endif
                    </div> 
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Modal Change Password End -->  

<script>
    $(function () {
    let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
            @can('users_manage')
            let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
            let deleteButton = {
            text: deleteButtonTrans,
                    url: "{{ route('admin.users.mass_destroy') }}",
                    className: 'btn-danger',
                    action: function (e, dt, node, config) {
                    var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
                    return $(entry).data('entry-id')
                    });
                    if (ids.length === 0) {
                    alert('{{ trans('global.datatables.zero_selected') }}')

                            return
                    }

                    if (confirm('{{ trans('global.areYouSure') }}')) {
                    $.ajax({
                    headers: {'x-csrf-token': _token},
                            method: 'POST',
                            url: config.url,
                            data: { ids: ids, _method: 'DELETE' }})
                            .done(function () { location.reload() })
                    }
                    }
            }
    dtButtons.push(deleteButton)
            @endcan

            $.extend(true, $.fn.dataTable.defaults, {
            order: [[ 1, 'desc' ]],
                    pageLength: 10,
            });
    $('.datatable-User:not(.ajaxTable)').DataTable({ buttons: dtButtons })
            $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
    $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust();
    });
    });
    /*start change password*/
    $('#user_password_change').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget);
    var id = button.data('id');
    var url = button.data('url');
    $('#frm_password_change').attr("action", url);
    $('#frm_password_change').find('#password').val('');
    })
            /*end change password*/
            /*$('.change_status_modal').on('shown', function (event) {
             $(this).find('#plan_name').selectpicker();
             $(this).find('#current_plan_id').selectpicker();
             });*/

//On change 
            $('.change_status_modal #plan_name').change(function(){
    var plan_name = $(this).val();
    if (plan_name){
    $.ajax({
    type:"GET",
            url:"{{url('user/get-plans-list')}}?current_plan_type=" + plan_name,
            success:function(res){
            if (res){
            $('.change_status_modal.in').find("#current_plan_id").empty();
            $('.change_status_modal.in').find("#current_plan_id").append('<option>Select</option>');
            $.each(res, function(key, value){
            $('.change_status_modal').find("#current_plan_id").append('<option value="' + key + '">' + value + '</option>');
            });
            // $('.change_status_modal.in').find('#current_plan_id').selectpicker('refresh');
            } else{
            $('.change_status_modal.in').find("#current_plan_id").empty();
            }
            }
    });
    } else{
    $('.change_status_modal.in').find("#current_plan_id").empty();
    }
    });
//end Current Plan on load
</script>
@endsection

