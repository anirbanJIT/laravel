@extends('layouts.admin')
@section('content')
<div class="breadcomb-area">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="breadcomb-list">
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<div class="breadcomb-wp">
								<div class="breadcomb-icon">
									<i class="notika-icon notika-windows"></i>
								</div>
								<div class="breadcomb-ctn">
									<h2>{{ trans('cruds.imei_change_request.title_singular') }} {{ trans('global.list') }}</h2>
									<p>Show <span class="bread-ntd">all list</span></p>
								</div>
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-3">
							<div class="breadcomb-report">								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Data Table area Start-->
<div class="data-table-area">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="data-table-list">
					<div class="table-responsive">
						<table id="data-table-basic" class="table table-striped">
							<thead>
								<tr>
									<th width="10">Id</th>
									<th>Name</th>
									<th>Email</th>
									<th>Phone</th>
									<th>IMEI</th>
									<th>Date</th>
									<th width="50" class="text-center">Action</th>
								</tr>
							</thead>
							<tbody>
								@foreach($retailerUsers as $retailerUser)
								<tr id="tr_{{$retailerUser->id}}">
									<td>{{$retailerUser->id}}</td>
									<td><a href="{{route('admin.retailers.edit', $retailerUser->user_id)}}">{{$retailerUser->name}}</a></td>
									<td>{{$retailerUser->email}}</td>
									<td>{{$retailerUser->phone}}</td>
									<td>{{$retailerUser->device_id}}</td>
									<td>{{convertUtcToLocal($retailerUser->created_at)}}</td>
									<td width="50" class="text-center">
                                                                            @if($retailerUser->status != 1)
										<a href="{{url('admin/update-imei-request/'.$retailerUser->id)}}" class="btn btn-success success-icon-notika btn-reco-mg btn-button-mg waves-effect" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Update" data-id="{{$retailerUser->id}}">Update</a>
                                                                            @endif
									</td>
								</tr>
								@endforeach
							</tbody>
							<tfoot>
								<tr>
									<th width="10">Id</th>									
									<th>Name</th>
									<th>Email</th>
									<th>Phone</th>
									<th>IMEI</th>
									<th>Date</th>
									<th width="50" class="text-center">Action</th>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
$(function() {
	$('.btn-status-update').click(function(e) {
		var id = $(this).data('id'); 
		var check = confirm("Are you sure you want to update?");  
        if(check == true){ 
			$.ajax({
		   		type:"GET",
		   		url:"{{url('admin/update-imei-request')}}?id="+id,
		   		success:function(res){               
				        window.location.href = "{{url('admin/imei-change-request-list')}}";
	       
				    }
			   
			});
		}
	});	
});
</script>
@endsection

