@extends('layouts.admin')
@section('content')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.5/css/buttons.dataTables.min.css">
<meta name="csrf-token" content="{{ csrf_token() }}">
<div class="breadcomb-area">
  <div class="container">
    <div class="row">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="breadcomb-list">
            <div class="row">
              <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                <div class="breadcomb-wp">
                  <div class="breadcomb-icon">
                    <i class="notika-icon notika-windows"></i>
                  </div>
                  <div class="breadcomb-ctn">
                    <h2>Users Monthly Recharge Report</h2>
                    <p>Show <span class="bread-ntd">all list</span></p>
                  </div>
                </div>
              </div>
              <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <input type="text" class="form-control" name="user_id" id="user_id" placeholder="Entet User Id/Email">
              </div>
              <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                <input type="submit" class="btn btn-success notika-btn-success waves-effect" id="btnFiterSubmitSearch" value="Search" >
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Data Table area Start-->
<div class="data-table-area" id="users-table">
  <div class="container">
    <div class="row">
      <div class="data-table-list">
        <div class="dt-buttons">
          <button style="margin-bottom: 10px" class="btn btn-success notika-btn-success waves-effect" id="ExportReporttoExcel">{{ trans('global.datatables.excel') }}</button>
        </div>
        <div class="table-responsive">
          <table id="data-table-basic" class="table table-striped datatable" >
            <thead>
              <tr>
                <th>Invoice No.</th>
                <th>Name</th>
                <th>Recharge Duration</th>
                <th>Recharge Amount</th>
                <th>Expire Date</th>
                <th>Recharge Date</th>
                <th>Payment Method</th>
                <th>Status</th>
              </tr>
            </thead>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.4.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript">
 $(function () {

  $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });

  let excelButtonTrans = '{{ trans('global.datatables.excel') }}'
  $('#data-table-basic').DataTable({
    dom: 'lBfrtip',
    responsive:!1,
    lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
    pageLength:10,
    processing: true,
    serverSide: true,
    ajax: {
      url: "{{ url('admin/monthly-recharge-report') }}",
      type: 'GET',
      data: function (d) {
        d.user_id = $('#user_id').val();
      }
    },
    columns: [
          { data: 'invoice_no', name: 'invoice_no', title: 'Invoice No' },
          { data: 'user_id', name: 'user_id' ,orderable: false, searchable: false, title: 'Name' },
          { data: 'recharge_duration', name: 'recharge_duration', title: 'Recharge Duration' },
  			  { data: 'recharge_amount', name: 'recharge_amount', title: 'Recharge Amount' },
          {
              data: 'expire_date',
              type: 'num',
              render: {
                _: 'display',
                sort: 'timestamp'
              },
              title: 'Expire Date'
          },
  		    {
              data: 'created_at',
              type: 'num',
              render: {
                _: 'display',
                sort: 'timestamp'
              },
              title: 'Recharge Date'
          },
  	    { data: 'payment_mode', name: 'payment_mode', title: 'Payment Method'},
  	    { data: 'status', name: 'status', title: 'Status'}
    ],
    buttons: [
      {
        extend: 'excel',
        className: 'btn-default',
        text: excelButtonTrans,
        exportOptions: {
          columns: ':visible'
        }
      },
    ]
  });
});

$('#btnFiterSubmitSearch').click(function(){
  $('#data-table-basic').DataTable().draw(true);
});

/* start export_buttons */
$("#ExportReporttoExcel").on('click', function() {
  $('#data-table-basic').DataTable().buttons(0,0).trigger();
});
/* end export_buttons */
</script>
<style>
.dataTables_filter
{
	display:none;
}
</style>
@endsection

