<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
      <meta name="description" content="POS - Bootstrap Admin Template">
      <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
      <meta name="author" content="Dreamguys - Bootstrap Admin Template">
      <meta name="robots" content="noindex, nofollow">
      <title>Add Credit Point</title>
      
      <link rel="shortcut icon" type="image/x-icon" href="{{ asset('public/Admin/img/favicon.png')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/bootstrap.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/animate.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/dataTables.bootstrap4.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/fontawesome.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/all.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/style.css')}}">
   </head>
   <body>
      <div id="global-loader">
         <div class="whirly-loader"> </div>
      </div>
      <div class="main-wrapper">
        @extends('layouts.header')
     
        @extends('layouts.sidebar')
        
        <div class="page-wrapper">
            <div class="content container-fluid">
               <div class="page-header">
                  <div class="row">
                     <div class="col">
                        <h3 class="page-title">Add Credit Point</h3>
                        <ul class="breadcrumb">
                           <li class="breadcrumb-item"><a href="{{route('admin.home')}}">{{ trans('global.dashboard') }}</a></li>
                           <li class="breadcrumb-item active">Add Credit Point</li>
                        </ul>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-12">
                     <div class="card">
                        <!-- <div class="card-header row">
                            <div class="col-lg-6"><h4 class="card-title">{{ trans('global.create') }} {{ trans('cruds.brand_type.title_singular') }}</h4></div>
                            
                        </div> -->
                        <div class="card-body">
                           <h4 style="text-align: right;">Users Credit Point :{{ $credit->amount ?? 0}}</h4>
                           <hr/>
                           <form action="{{ route('admin.add-credit-point-summary') }}" method="POST" >
                            {{ csrf_field() }}
                              
                              <div class="form-group">
                                 <label>Enter Credit Point</label>
                                 <input type="text" class="form-control input-sm" value="{{old('credit')}}" placeholder="Enter Credit Point" id="credit" name="credit" required autofocus>
                                 @if($errors->has('credit')) <em class="invalid-feedback"> {{ $errors->first('credit') }} </em> @endif
                              </div>

                              <div class="form-group">
                                 <label>Enter Credit Notes</label>
                                 <input type="text" class="form-control input-sm" value="{{old('credit_note')}}" placeholder="Enter Credit Note" id="credit_note" name="credit_note" required>
                                 @if($errors->has('credit_note')) <em class="invalid-feedback"> {{ $errors->first('credit_note') }} </em> @endif
                              </div>

                              <div>
                                 <button type="submit" class="btn btn-success">Save</button>
                              </div>
                           </form>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
    <script src="{{ asset('public/Admin/js/jquery-3.6.0.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/feather.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.slimscroll.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{ asset('public/Admin/plugins/apexchart/apexcharts.min.js')}}"></script>
    <script src="{{ asset('public/Admin/plugins/apexchart/chart-data.js')}}"></script>
    <script src="{{ asset('public/Admin/js/script.js')}}"></script>
   </body>
</html>