@extends('layouts.admin')
@section('content')
<!-- <script data-require="jquery@*" data-semver="2.2.0" src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script> -->

<link href="{{ url('css/jquery.treegrid.css') }}" rel="stylesheet" />

<script src="{{ url('js/jquery.treegrid.bootstrap3.js') }}"></script>
<script src="{{ url('js/jquery.treegrid.js') }}"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $('.tree').treegrid({
            expanderExpandedClass: 'glyphicon glyphicon-minus',
            expanderCollapsedClass: 'glyphicon glyphicon-plus'
        });
  });
</script>
<!-- <script src="{{ url('js/data-table/dataTables.treeGrid.js') }}"></script> -->
<meta name="csrf-token" content="{{ csrf_token() }}">
<div class="breadcomb-area">
	<div class="container">
		<div class="row">
				<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="breadcomb-list">
					<div class="row">
						<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
							<div class="breadcomb-wp">
								<div class="breadcomb-icon">
									<i class="notika-icon notika-windows"></i>
								</div>
								<div class="breadcomb-ctn">
									<h2>Commission Slab Retailers Tree View</h2>
									<p>Show <span class="bread-ntd">all retailers</span></p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		</div>
	</div>
</div>
 <!-- Data Table area Start-->
 <div class="data-table-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="data-table-list">
                    <div class="table-responsive">
                        <table id="table-basic" class="table tree table table-striped">
                            <thead>
                                <tr>
                                    <th>
                                        Name
                                    </th>
                                    <th>
                                        Recievable
                                    </th>
                                    <th>
                                        Received
                                    </th>
                                    <th>
                                        Total
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($categories as $key => $category)
                                <tr data-entry-id="{{ $category->id }}" class="treegrid-{{ $category->id }}">
                                    <td>
                                        {{ $category->name ?? '' }}
                                    </td>
                                    <td>
                                        {!! $category->getTotalPaidCommission()->sum('commission_amt')!!}
                                    </td>
                                    <td>
                                        {!! $category->getTotalUnpaidCommission()->sum('commission_amt')!!}
                                    </td>
                                    <td>
                                        {!! $category->getTotalPaidCommission()->sum('commission_amt')!!}
                                    </td>
                                </tr>
                                @if(count($category->getChildMember))
                                        @include('admin.retailers.manageChildTable',['childs' => $category->getChildMember, 'parentId' => $category->id ])
                                @endif
                                @endforeach
                            </tbody>
                            <!-- <tfoot>
                                <tr>
                                    <th>
                                        Name
                                    </th>
                                    <th>
                                        Recievable
                                    </th>
                                    <th>
                                        Received
                                    </th>
                                    <th>
                                        Total
                                    </th>
                                </tr>
                            </tfoot> -->
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
 <!-- Data Table area end-->

            <!-- <table class="table tree">
                <tr class="treegrid-1">
                  <td>Root node</td><td>Additional info</td>
                </tr>
                <tr class="treegrid-2 treegrid-parent-1">
                  <td>Node 1-1</td><td>Additional info</td>
                </tr>
                <tr class="treegrid-3 treegrid-parent-1">
                  <td>Node 1-2</td><td>Additional info</td>
                </tr>
                <tr class="treegrid-4 treegrid-parent-3">
                  <td>Node 1-2-1</td><td>Additional info</td>
                </tr>
            </table>  --> 


@endsection

