<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
      <meta name="description" content="POS - Bootstrap Admin Template">
      <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
      <meta name="author" content="Dreamguys - Bootstrap Admin Template">
      <meta name="robots" content="noindex, nofollow">
      <title>Welcome {{$user->name}}</title>
      
      <link rel="shortcut icon" type="image/x-icon" href="{{ asset('public/Admin/img/favicon.png')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/bootstrap.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/animate.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/dataTables.bootstrap4.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/fontawesome.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/all.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/style.css')}}">
   </head>
   <body>
      <div id="global-loader">
         <div class="whirly-loader"> </div>
      </div>
      <div class="main-wrapper">
        @extends('layouts.header')
     
        @extends('layouts.sidebar')
        
        <div class="page-wrapper">
            <div class="content container-fluid">
               <div class="page-header">
                  <div class="row">
                     <div class="col">
                        <h3 class="page-title">Welcome {{$user->name}}</h3>
                        <ul class="breadcrumb">
                           <li class="breadcrumb-item"><a href="{{route('admin.home')}}">{{ trans('global.dashboard') }}</a></li>
                           <li class="breadcrumb-item"><a href="{{ route('admin.retailers-list') }}">Retailers List</a></li>
                           <li class="breadcrumb-item active">Welcome {{$user->name}}</li>
                        </ul>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-12">
                     <div class="card">
                        <!-- <div class="card-header row">
                            <div class="col-lg-6"><h4 class="card-title">{{ trans('global.create') }} {{ trans('cruds.brands.title_singular') }}</h4></div>
                            
                        </div> -->
                        <div class="card-body">
                          <form action="{{ route('admin.retailers.update', [$user->id]) }}" method="POST" enctype="multipart/form-data" id="frm_retailer">
                            {{ csrf_field() }}
                               @method('PUT')
                              <div class="row">
                              
                              <div class="form-group col-lg-6">
                                 <label>Shop Name*</label>
                                 <input type="text" id="shop_name" name="shop_name" value="{{ old('shop_name', isset($user) ? $user->shop_name : '') }}" required class="form-control" placeholder="{{ trans('cruds.retailer.fields.shop_name') }}*" >
                                 @if($errors->has('shop_name'))
                                <em class="invalid-feedback">{{ $errors->first('shop_name') }}</em>
                                @endif
                              </div>

                              <div class="form-group col-lg-6">
                                 <label>Customer Name*</label>
                                 <input type="text" id="name" name="name" value="{{ old('name', isset($user) ? $user->name : '') }}" required class="form-control" placeholder="{{ trans('cruds.user.fields.name') }}*" >
                                 @if($errors->has('name'))
                                <em class="invalid-feedback">{{ $errors->first('name') }}</em>
                                @endif
                              </div>

                              <div class="form-group col-lg-6">
                                 <label>Email*</label>
                                 <input type="text" id="email" name="email" class="form-control" value="{{ old('email', isset($user) ? $user->email : '') }}" placeholder="{{ trans('cruds.user.fields.email') }}*" >
                                 @if($errors->has('email'))
                                <em class="invalid-feedback">{{ $errors->first('email') }}</em>
                                @endif
                              </div>

                              <div class="form-group col-lg-6">
                                 <label>Phone</label>
                                <input type="text" id="phone" name="phone" value="{{ old('phone', isset($user) ? $user->phone : '') }}" required class="form-control" placeholder="Phone *" >
                                 @if($errors->has('phone'))
                                <em class="invalid-feedback">{{ $errors->first('phone') }}</em>
                                @endif
                              </div>

                              <div class="form-group col-lg-6">
                                 <label>Other Phone Number</label>
                                 <input type="text" id="other_phone" name="other_phone" value="{{ old('other_phone', isset($user) ? $user->other_phone : '') }}" class="form-control" placeholder="Other phone Number" >
                                 @if($errors->has('other_phone'))
                                <em class="invalid-feedback">{{ $errors->first('other_phone') }}</em>
                                @endif
                              </div>

                              <div class="form-group col-lg-6">
                                 <label>Address</label>
                               <input type="text" class="form-control" name="address" id="address" placeholder="Address"  value="{{$user->getContactDetail ? $user->getContactDetail->address : ''}}" required>
                                 @if($errors->has('address'))
                                <em class="invalid-feedback">{{ $errors->first('address') }}</em>
                                @endif
                              </div>
                           
                              <div class="form-group col-lg-6">
                                 <label>Country</label>
                               <select class="form-control" name="country" id="country" required  >
                                   <option value="">Select Country</option>
                                   @foreach($countries as $key => $country)
                                   @if(!empty($user->getContactDetail->country))
                                   <option  value="{{$key}}" {{$user->getContactDetail->country == $key  ? 'selected' : ''}}> {{$country}}</option>
                                   @else
                                   <option  value="{{$key}}"> {{$country}}</option>
                                   @endif                          
                                   @endforeach
                               </select>
                                 @if($errors->has('country'))
                                <em class="invalid-feedback">{{ $errors->first('country') }}</em>
                                @endif
                              </div>

                              <div class="form-group col-lg-6">
                                 <label>State</label>
                               <select class="form-control" name="state" id="state" required></select>
                                 @if($errors->has('state'))
                                <em class="invalid-feedback">{{ $errors->first('state') }}</em>
                                @endif
                              </div>

                              <div class="form-group col-lg-6">
                                 <label>City</label>
                               <input type="text" class="form-control" name="city" id="city" placeholder="City" value="{{$user->getContactDetail ? $user->getContactDetail->city : ''}}" required>
                                 @if($errors->has('city'))
                                <em class="invalid-feedback">{{ $errors->first('city') }}</em>
                                @endif
                              </div>

                              <div class="form-group col-lg-6">
                                 <label>Pincode</label>
                               <input type="text" class="form-control" name="pin_code" id="pin_code" placeholder="Pin Code"  value="{{$user->getContactDetail ? $user->getContactDetail->pin_code : ''}}" required>
                                 @if($errors->has('pin_code'))
                                <em class="invalid-feedback">{{ $errors->first('pin_code') }}</em>
                                @endif
                              </div>

                           </div>
                              
                          <div><button type="submit" class="btn btn-success">Save</button></div>

                           </form>

                            <hr/>
                            <h4><b>Shop Details:</b></h4><br/><br/>
                            
                            <form action="{{ route('user.shop-detail.update', [$user->id]) }}" method="POST" id="shop_detail" enctype="multipart/form-data">
                               @csrf
                              <div class="row">
                              <div class="form-group col-lg-6">
                                 <label>Shop Name*</label>
                                 <input type="text" id="shop_name" name="shop_name" value="{{ old('shop_name', isset($user) ? $user->shop_name : '') }}" required class="form-control" placeholder="{{ trans('cruds.retailer.fields.shop_name') }}*" >
                                 @if($errors->has('shop_name'))
                                <em class="invalid-feedback">{{ $errors->first('shop_name') }}</em>
                                @endif
                              </div>

                              <div class="form-group col-lg-6">
                                 <label>Address</label>
                               <input type="text" class="form-control" name="address" id="address" placeholder="Address"  value="{{$user->getContactDetail ? $user->getContactDetail->address : ''}}" required>
                                 @if($errors->has('address'))
                                <em class="invalid-feedback">{{ $errors->first('address') }}</em>
                                @endif
                              </div>
                           
                              <div class="form-group col-lg-6">
                                 <label>Country</label>
                               <select class="form-control" name="country" id="country" required  >
                                   <option value="">Select Country</option>
                                   @foreach($countries as $key => $country)
                                   @if(!empty($user->getContactDetail->country))
                                   <option  value="{{$key}}" {{$user->getContactDetail->country == $key  ? 'selected' : ''}}> {{$country}}</option>
                                   @else
                                   <option  value="{{$key}}"> {{$country}}</option>
                                   @endif                          
                                   @endforeach
                               </select>
                                 @if($errors->has('country'))
                                <em class="invalid-feedback">{{ $errors->first('country') }}</em>
                                @endif
                              </div>

                              <div class="form-group col-lg-6">
                                 <label>Pincode</label>
                               <input type="text" class="form-control" name="pin_code" id="pin_code" placeholder="Pin Code"  value="{{$user->getContactDetail ? $user->getContactDetail->pin_code : ''}}" required>
                                 @if($errors->has('pin_code'))
                                <em class="invalid-feedback">{{ $errors->first('pin_code') }}</em>
                                @endif
                              </div>

                              <div class="form-group">
                                 <label>Shop Photo</label>
                                 <input type="file" accept="image/*" name="shop_photo" id="shop_photo">
                              </div>
                              
                              <div class="form-group">
                                 @if(!empty($user->getShopDetail->shop_photo))
                                 <img src="{{ asset('Admin/images/shop/'.$user->getShopDetail->shop_photo) }}" width="60" height="60">
                                 @endif
                              </div>

                              <div class="form-group">
                                 <label>Bill Photo</label>
                                 <input type="file" accept="image/*" name="machine_photo" id="machine_photo">
                              </div>

                              <div class="form-group">
                                 @if(!empty($user->getShopDetail->machine_photo))
                                 <img src="{{ asset('Admin/images/shop/'.$user->getShopDetail->machine_photo) }}" width="60" height="60">
                                 @endif
                              </div>

                              <div class="form-group">
                                 <label>Image Gallery</label>
                                 <div class="input-group hdtuto control-group lst increment" >
                                     <input type="file" name="shop_gallery[]" multiple class="myfrm form-control" placeholder="Upload image file ">
                                     <div class="input-group-btn"> 
                                         <button class="btn btn-success btn-success1" type="button"><i class="fldemo glyphicon glyphicon-plus"></i>Add</button>
                                     </div>
                                 </div>

                                 <div class="clone hide">
                                     <div class="hdtuto control-group lst input-group" style="margin-top:10px">
                                         <input type="file" name="shop_gallery[]" multiple class="myfrm form-control">
                                         <div class="input-group-btn"> 
                                             <button class="btn btn-danger btn-danger1" type="button"><i class="fldemo glyphicon glyphicon-remove"></i> Remove</button>
                                         </div>
                                     </div>
                                 </div>

                                 @if(isset($user->getShopDetail->shop_gallery))
                                @php $myArray = json_decode($user->getShopDetail->shop_gallery);  @endphp

                                @foreach($myArray as $key => $value)
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                    <div class="animation-single-int">
                                        <div class="animation-img mg-b-15">
                                            <img src="{{ asset('images/shop/'.$value) }}" class="animate-eleven" style="width:200px;height:200px;">
                                        </div>
                                        <div >
                                            <a href="{{ url('admin/deleteShopGallery/?id='.$user->getShopDetail->id.'&shop_gallery='.$value) }}" class="sa-warning" >

                                                <button class="btn btn-danger danger-icon-notika btn-reco-mg btn-button-mg waves-effect"> <i class="notika-icon notika-close"></i></button>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                                @endif               
                              </div>
                           </div>
                              <div><button type="submit" class="btn btn-success">Save</button></div>
                            </form>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
    <script src="{{ asset('public/Admin/js/jquery-3.6.0.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/feather.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.slimscroll.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{ asset('public/Admin/plugins/apexchart/apexcharts.min.js')}}"></script>
    <script src="{{ asset('public/Admin/plugins/apexchart/chart-data.js')}}"></script>
    <script src="{{ asset('public/Admin/js/script.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>

   <script type="text/javascript">
   $(document).ready(function() 
   {
     // for kyc detail on load
      var countryId = $('#country_id :selected').val();
     var country = $('#country :selected').val();
       
      if(countryId){
        $.ajax({
           type:"GET",
           url:"{{url('user/get-state-list')}}?country_id="+countryId,
           success:function(res){               
            if(res){
                $("#state_id").empty();
                $("#state_id").append('<option value="">Select</option>');
            var s_id = ( {{$user->getContactDetail ? $user->getContactDetail->state : 'null'}} );
            
            if(s_id){
                $.each(res,function(key,value){
               var newClass1 = (s_id == key) ? "selected" : " ";
               //alert(newClass);
                    $("#state_id").append('<option value="'+key+'" '+newClass1+'>'+value+'</option>');
                });
               $('#state_id').selectpicker('refresh');
            }
            
            
               
            }else{
               $("#state_id").empty();
            }
           }
        });
    }else{
        $("#state_id").empty(); 
    } 
//For contact Detail
    if(country){
        $.ajax({
           type:"GET",
           url:"{{url('user/get-state-list')}}?country_id="+country,
           success:function(res){               
            if(res){
                $("#state").empty();
                $("#state").append('<option value="">Select</option>');
            var s_id = ({{$user->getContactDetail ? $user->getContactDetail->state : 'null'}} );
            
            if(s_id){
                $.each(res,function(key,value){
               var newClass = (s_id == key) ? "selected" : " ";
               //alert(newClass);
                    $("#state").append('<option value="'+key+'" '+newClass+'>'+value+'</option>');
                });
               $('#state').selectpicker('refresh');
            }
            
            
               
            }else{
               $("#state").empty();
            }
           }
        });
    }else{
        $("#state").empty();
      
       
    }    
    });
   //On change 
    $('#country').change(function(){
    var countryID = $(this).val();   
   
    if(countryID){
        $.ajax({
           type:"GET",
           url:"{{url('user/get-state-list')}}?country_id="+countryID,
           success:function(res){               
            if(res){
                $("#state").empty();
                $("#state").append('<option>Select</option>');
                $.each(res,function(key,value){
                    $("#state").append('<option value="'+key+'">'+value+'</option>');
                });
               $('#state').selectpicker('refresh');
            }else{
               $("#state").empty();
            }
           }
        });
    }else{
        $("#state").empty();
       
    }      
   }); 
   
    $('#country_id').change(function(){
    var countryID = $(this).val();   
   
    if(countryID){
        $.ajax({
           type:"GET",
           url:"{{url('user/get-state-list')}}?country_id="+countryID,
           success:function(res){               
            if(res){
                $("#state_id").empty();
                $("#state_id").append('<option>Select</option>');
                $.each(res,function(key,value){
                    $("#state_id").append('<option value="'+key+'">'+value+'</option>');
                });
               $('#state_id').selectpicker('refresh');
            }else{
               $("#state_id").empty();
            }
           }
        });
    }else{
        $("#state_id").empty();
      
    }      
   }); 

$.validator.addMethod("countryIsIndia", function(value, element) {
  var email = $('#email').val();
   var country_id = $('#country :selected').val();
   console.log(country_id);
   if ( country_id == 101 && (email.length == 0 || email.length > 1) ) {
      return true;
   } else if ( country_id != 101 && email.length > 1) {
    return true;
  }else{
      return false;
   }

    }, "Please enter a email."
);

$("#frm_retailer").validate({
    rules: {
        shop_name: {
         required: true,
      },
       name: {
         required: true,
       },
       phone: {
          required: true,
          minlength:10,
          maxlength:14,
       },
       email: {
         email: true,
         countryIsIndia: true
       },
       address: {
           required: true,
           maxlength: 150
       },
       country: {
           required: true,
       },
       state: {
           required: true,
       },
      city: {
         required: true,
         },
      pin_code: {
         required: true,
         maxlength:6,
         rangelength: [6,6]
         },
   },
      messages: { 
        shop_name: {
            required: "Please enter Shop Name"
        },
        name: {
            required: "Please enter Name"
        },
        phone: {
         required: "Please enter Phone Number",
         minlength: "The phone number should be 10 digits",
         maxlength: "The phone number not more tahn 14 digits",
      },
         email: {
         email: "The Email should be valid",
         countryIsIndia: "Please enter Email",     
      },
      address: {
            required: "Please enter address",
        },
        country: {
            required: "Please select country",
        },
        state: {
            required: "Please select state",
        },
      city: {
         required: "Please enter city",
      },
      pin_code: {
         required: "Please enter Pin Code",
         maxlength: "The Pin Code should be 6 digits",
         rangelength: "The Pin Code should be 6 digits",
      },

    },
});
     
function fileValidtion(e) {
 var attachment = document.getElementById("attachment");
  if(attachment){
   var filePath = attachment.value;   
  }
 
  
  if(filePath == ''){
    
     document.getElementById("demo").innerHTML = "Please upload file"; 
     e.preventDefault() ;
      returnToPreviousPage();
  }
  var attachmentUp = document.getElementById("attachmentUp"); 
  var attachmentUp = attachmentUp.value;
  var attachmnetUpdate = document.getElementById("attachmnetUpdate");
  var attachmnetUpdate = attachmnetUpdate.value;
  
  if( attachmentUp == '' && attachmnetUpdate == ''){
    document.getElementById("demo").innerHTML = "Please upload file";  
    e.preventDefault() ;
      returnToPreviousPage();
  }
 
}

$(document).ready(function() {
  $(".btn-success1").click(function(){ 
      var lsthmtl = $(".clone").html();
      $(".increment").after(lsthmtl);
  });
  $("body").on("click",".btn-danger1",function(){ 
      $(this).parents(".hdtuto").remove();
  });
});

</script>
   </body>
</html>