@extends('layouts.admin')
@section('content')
<style type="text/css">
.cursor-pointer{
    cursor: pointer;
}
</style>
<div class="breadcomb-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="breadcomb-list">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="breadcomb-wp">
                                    <div class="breadcomb-icon">
                                        <i class="notika-icon notika-windows"></i>
                                    </div>
                                    <div class="breadcomb-ctn">
                                        <h2>{{ trans('cruds.members_tree.title_singular') }}</h2>
                                        <p>Show <span class="bread-ntd">all retailers</span></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
 <!-- Data Table area Start-->
    <div class="data-table-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="data-table-list">
                        <table class="table table-striped datatable-Retailers" id="datatable-Retailers">
                            <thead>
                                <tr role="row" class="heading">
                                    <th width="50">{{ trans('cruds.members_tree.fields.id') }} </th>
                                    <th width="100"> {{ trans('cruds.members_tree.fields.name') }} </th>
                                    <th width="100"> {{ trans('cruds.members_tree.fields.email') }} </th>
                                    <th width="60"> {{ trans('cruds.members_tree.fields.phone') }} </th>
                                    <th width="100"> {{ trans('cruds.members_tree.fields.status') }} </th>
                                    <th width="100"> {{ trans('cruds.members_tree.fields.members_total') }} </th>
                                </tr>
                                <tr role="row" class="filterrow">
                                    <th class="select-filter" width="50">{{ trans('cruds.members_tree.fields.id') }} </th>
                                    <th class="select-filter" width="100">{{ trans('cruds.members_tree.fields.name') }} </th>
                                    <th class="select-filter" width="100">{{ trans('cruds.members_tree.fields.email') }} </th>
                                    <th class="select-filter" width="60">{{ trans('cruds.members_tree.fields.phone') }} </th>
                                    <th class="select-filter" width="100">{{ trans('cruds.members_tree.fields.status') }} </th>
                                    <th class="select-filter" width="100"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($users as $user)
                                <tr data-entry-id="{{ $user->id }}">
                                    <td>
                                        {{ $user->id }}
                                    </td>
                                    <td class="cursor-pointer" onclick="window.location='{{ route("admin.retailers.edit",$user->id) }}'" title='View Detail' > {{ ucwords($user->name ?? '')  }} </td>
                                    <td>
                                        {{ $user->email ?? 'N/A' }}
                                    </td>
                                    <td>
                                        {{ $user->phone ?? 'N/A' }}
                                    </td>
                                    <td>
                                        @if ($user->status == 1) 
                                            Active 
                                        @elseif ($user->status == 2) 
                                            Expired 
                                        @else
                                            Pending
                                        @endif
                                    </td>                                  
                                    <td class="cursor-pointer" onclick="window.location='{{ route("admin.tree-view-retailers",$user->id) }}'"  title='View Member Tree' >
                                        {{ $users->where('referred_by', $user->referral_id)->count('referred_by') ?? 0 }}                                       
                                    </td>
                                </tr>
                               @endforeach
                            </tbody>  
                            <tfoot>
                                <tr role="row">                                    
                                    <th width="50">{{ trans('cruds.members_tree.fields.id') }} </th>
                                    <th width="100"> {{ trans('cruds.members_tree.fields.name') }} </th>
                                    <th width="100"> {{ trans('cruds.members_tree.fields.email') }} </th>
                                    <th width="60"> {{ trans('cruds.members_tree.fields.phone') }} </th>
                                    <th width="100"> {{ trans('cruds.members_tree.fields.status') }} </th>
                                    <th width="100"> {{ trans('cruds.members_tree.fields.members_total') }} </th>
                                </tr>
                            </tfoot>                              
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
<script type="text/javascript">
$(document).ready(function (){
    /* @author Naren Thaker @narenthaker.com 101220201500 */
    window._token = $('meta[name="csrf-token"]').attr('content');

    $.extend(true, $.fn.dataTable.Buttons.defaults.dom.button, { className: 'btn' })
    $.extend(true, $.fn.dataTable.defaults, {
        lengthMenu: [
            [ 10, 25, 50, 100, -1 ],
            [ '10', '25', '50', '100', 'All' ]
        ],
        order: [[ 0, 'desc' ]],
        buttons: [],
        orderCellsTop: true,
        dom: 'lBfrtip<"actions">',
        fixedHeader: true,
    });

    var table = $('.datatable-Retailers:not(.ajaxTable)').DataTable({
        initComplete: function() {
            this.api().columns([0,1,2,3,4,5]).every(function(d) {
                var column = this;
                var theadname = $('.datatable-Retailers thead th').eq([d]).text();
                var select = $('<select class ="filter searchSelect"><option value="">'+theadname+': All</option></select>')
                             .appendTo($(".datatable-Retailers thead tr:eq(1) th").eq(column.index()).empty() )
                             .tailselect({ search: true, deselect: true, width: 110, height: 200, placeholder: theadname});
                select.on("change", function(){
                    var val = $.fn.dataTable.util.escapeRegex(this.value());
                    column.search(val? ('^' + val + '$'): '', true, false).draw();
                });                
                column.data().unique().sort().each(function(d, j) {
                    select.options.add(d, d, '#');
                });
                select.query();

            });
        }
    });
    table.on('draw', function() {
        table.columns([0,1,2,3,4,5]).indexes().each(function(idx) {
            var select = $(".datatable-Retailers thead tr:eq(1) th").eq(idx).find('select').tailselect('.select');
            if (select.value() === null) {
                select.reload();                
                table.column(idx, {
                    search: 'applied'
                }).data().unique().sort().each(function(d, j) {
                    select.options.add(d, d);
                });

                select.query();
            }
        });
    });

    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
        $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust();
    });

});
</script>
@endpush
