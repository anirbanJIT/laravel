<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
      <meta name="description" content="POS - Bootstrap Admin Template">
      <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
      <meta name="author" content="Dreamguys - Bootstrap Admin Template">
      <meta name="robots" content="noindex, nofollow">
      <title>Retailers List</title>
      
      <link rel="shortcut icon" type="image/x-icon" href="{{ asset('public/Admin/img/favicon.png')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/bootstrap.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/animate.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/dataTables.bootstrap4.min.css')}}">

      <!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.7.1/css/buttons.dataTables.min.css"> -->
      
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/fontawesome.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/all.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/style.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/select2/css/select2.min.css')}}">
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <link rel="stylesheet" href="{{ asset('public/Admin/tabulator/bootstrap/tabulator_bootstrap4.css')}}">
      
   </head>
   <body>
      <!-- <div id="global-loader">
         <div class="whirly-loader"> </div>
      </div> -->
      <div class="main-wrapper">
        @extends('layouts.header')
     
        @extends('layouts.sidebar')
        
        <div class="page-wrapper">
            <div class="content container-fluid">
               <div class="page-header">
                  <div class="row">
                     <div class="col">
                        <h3 class="page-title">Retailers List</h3>
                        <ul class="breadcrumb">
                           <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">{{ trans('global.dashboard') }}</a></li>
                           <li class="breadcrumb-item active">Retailers List</li>
                        </ul>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-12">
                     <div class="card">
                        <div class="card-header row">
                            <div class="col-lg-6"><h4 class="card-title">Retailers List</h4></div>
                            <!-- <div class="col-lg-6"><a href="{{ route('admin.retailers.create') }}" class="btn btn-success" style="float: right;">+ {{ trans('global.add') }} {{ trans('cruds.retailer.title_singular') }}</a></div> -->
                        </div>
                        <div class="card-body">
                           <div class="table-responsive">
                              <!-- <table class="table datanew" id="data-table-retailers"> -->
                                
                                <input id="filter-value" type="text" placeholder="value to filter">
                                 <button class="btn btn-outline-info waves-effect" id="download-csv">Download CSV</button> <p><br/></p>
                                 <div id="data-table-retailers"></div>
                                 <div class="modal fade" id="active_model"  role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle"></h5>
        <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="{{ url('admin/kyc/adminChangeStatus') }}" method="POST" id="frm_change_status">
      <div class="modal-body">
         <h5>Are you sure you want to change status Inactive?</h5>
        <div class="form-group">
            <textarea id="notes" name="notes" required class="form-control" placeholder="Notes*"></textarea>  
         </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-success">Yes, Change It</button>
      </div>
   </form>
    </div>
  </div>
</div>


<div class="modal fade" id="Deactive_model"  role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <form action="{{ url('admin/kyc/adminChangeStatus') }}" method="POST" id="frm_change_status">
      <div class="modal-body">
         <h5>Are you sure you want to change status Active?</h5>
         <input type="hidden" name="id" value="">
         <input type="hidden" name="status" value="1">
         <input type="hidden" name="_token" value="">
         
         <div class="form-group">
             <select class="form-control" name="current_plan" id="current_plan" required  >
                 <option value="">Select Current Plan</option>
                 @foreach($plans as $key => $name)
                 <option  value="{{$key}}"> {{$name}}</option>
                 @endforeach
             </select>
         </div>

         <div class="form-group">
           <input type="text" id="machine_price" name="machine_price" required class="form-control" placeholder="Custom Price" >
         </div>

         <div class="form-group">
             <select class="form-control" name="machine_number" id="machine_number" required  >
                 <option value="">Select Machine Number</option>
                 @foreach($machine_numbers as $key => $machine_number)
                 <option  value="{{$key}}"> {{$machine_number}}</option>
                 @endforeach
             </select>
         </div>
         <div class="form-group">
           <input type="date" id="machine_purchase_date" name="machine_purchase_date" required class="form-control" placeholder="Machine Purchse Date" >
         </div>
         <div class="form-group">
            <textarea id="notes" name="notes" required class="form-control" placeholder="Notes*"></textarea>  
         </div>
         <div class="form-group">
             <select class="form-control tagging" name="addon_plan[]" id="addon_plan" placeholder="Select Addon Plan" required  multiple>
                 <option value="">Select Addon Plan</option>
                @foreach($plans as $key => $name)
                 <option  value="{{$key}}"> {{$name}}</option>
                 @endforeach
             </select>
         </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-success">Yes, Change It</button>
        <button type="button" data-bs-dismiss="modal" class="btn btn-danger">Close</button>
      </div>
   </form>
    </div>
  </div>
</div>

                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
    <script src="{{ asset('public/Admin/js/jquery-3.6.0.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/feather.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.slimscroll.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/script.js')}}"></script>
    <script src="{{ asset('public/Admin/plugins/select2/js/select2.min.js')}}"></script> 
     
      <script type="text/javascript" language="javascript" src='https://cdn.datatables.net/buttons/2.2.3/js/dataTables.buttons.min.js' defer></script>
      
      <script type="text/javascript" language="javascript" src='https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js' defer></script>
      
      <!-- <script type="text/javascript" language="javascript" src='https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js' defer></script> -->
      
      <script type="text/javascript" language="javascript" src='https://cdn.datatables.net/buttons/2.2.3/js/buttons.html5.min.js' defer></script> 
      
      <!-- <script type="text/javascript" src="{{ asset('public/Admin/js/3rdparty/tabulator.min.js') }}"></script> -->
      <script src="{{ asset('public/Admin/tabulator/tabulator.min.js') }}"></script>
      <script src="{{ asset('public/Admin/tabulator/jspdf.min.js') }}"></script>
      <script src="{{ asset('public/Admin/tabulator/xlsx.full.min.js') }}"></script>
      <script src="{{ asset('public/Admin/tabulator/jspdf.plugin.autotable.js') }}"></script>
      <script type="text/javascript">
      
      $(document).ready(function() {
          $('#addon_plan').select2();
      });

      var tabledata = <?php echo json_encode($retailerUsers); ?>;
      //console.log(tabledata);
      // function customFilter(data){
      //     return data.car && data.rating < 3;
      // }

      // function updateFilter(){

      //     var filter = $("#filter-field").val() == "function" ? tabledata : $("#filter-field").val();

      //     if($("#filter-field").val() == "function" ){
      //         $("#filter-type").prop("disabled", true);
      //         $("#filter-value").prop("disabled", true);
      //     }else{
      //         $("#filter-type").prop("disabled", false);
      //         $("#filter-value").prop("disabled", false);
      //     }

      //     table.setFilter(filter, $("#filter-type").val(), $("#filter-value").val());
      // }

      // //Update filters on value change
      // $("#filter-field, #filter-type").change(updateFilter);
      // $("#filter-value").keyup(updateFilter);

    var fieldEl = 'id';
    var typeEl = 'like';
    var valueEl = document.getElementById("filter-value");

    //Custom filter example
    function customFilter(data){
        return data.car && data.rating < 3;
    }

    //Trigger setFilter function with correct parameters
    function updateFilter(){
      // var filterVal = fieldEl;
      // var typeVal = typeEl;

      // var filter = filterVal == "function" ? customFilter : filterVal;

      // if(filterVal == "function" ){
      //   typeEl.disabled = true;
      //   valueEl.disabled = true;
      // }else{
      //   typeEl.disabled = false;
      //   valueEl.disabled = false;
      // }

      // if(filterVal){
      //   table.setFilter(filter,typeVal, valueEl.value);
      // }

      // start
    //   $.each ( ['id', 'name', 'email','phone','country','referral_id','created_at','current_plan','referral_by'], function (indexes, values) 
    //   {
        
    //       var filterVal = values;
    //       var typeVal = typeEl;

    //       var filter = filterVal == "function" ? customFilter : filterVal;

    //       if(filterVal == "function" ){
    //         typeEl.disabled = true;
    //         valueEl.disabled = true;
    //       }else{
    //         typeEl.disabled = false;
    //         valueEl.disabled = false;
    //       }

    //       if(filterVal){
            table.setFilter(filter,typeVal, valueEl.value);
    //       }


    //     }
    // );

     function matchAny(tabledata, filterParams){
        //data - the data for the row being filtered
        //filterParams - params object passed to the filter
        console.log(filterParams);
        var match = false;

        for(var key in data){
            if(data[key] == filterParams.value){
                match = true;   
            }
        }

        return match;
    }
    table.tabulator("setFilter", matchAny, {value:5});

      // end

    }

    //Update filters on value change
    // document.getElementById("filter-field").addEventListener("change", updateFilter);
    // document.getElementById("filter-type").addEventListener("change", updateFilter);
     document.getElementById("filter-value").addEventListener("keyup", updateFilter);

    // //Clear filters on "Clear Filters" button click
    // document.getElementById("filter-clear").addEventListener("click", function(){
    //   fieldEl.value = "";
    //   typeEl.value = "=";
    //   valueEl.value = "";

    //   table.clearFilter();
    // });

      var table = new Tabulator("#data-table-retailers", {
          //renderHorizontal:"virtual",
          data:tabledata,           //load row data from array
          layout:"fitColumns",      //fit columns to width of table
          responsiveLayout:"collapse",  //hide columns that dont fit on the table
          tooltips:true,            //show tool tips on cells
          addRowPos:"top",          //when adding a new row, add it to the top of the table
          history:true,             //allow undo and redo actions on the table
          pagination:"local",       //paginate the data
          paginationSize:10,         //allow 7 rows per page of data
          movableColumns:true,      //allow column order to be changed
          resizableRows:true,       //allow row order to be changed
          initialSort:[             //set the initial sort order of the data
              {column:"id", dir:"desc"},
          ],
          columns: [
               // { data: "delete_chk", orderable:false, searchable:false},
                 {title:"Id", field:"id", editor:"label",headerFilter:"input",headerFilterPlaceholder:"Id"},
                {title:"Name", field:"name", editor:"label",headerFilter:"input",headerFilterPlaceholder:"Name"},
                {title:"Email", field:"email", editor:"label",headerFilter:"input",headerFilterPlaceholder:"Email"},
                {title:"Phone", field:"phone", editor:"label",headerFilter:"input",headerFilterPlaceholder:"Phone"},
                {title:"Country", field:"country", editor:"label",headerFilter:"input",headerFilterPlaceholder:"Country"},
                {title:"Approved By", field:"referral_id", editor:"label",headerFilter:"input",headerFilterPlaceholder:"Approved By"},
                {title:"Date", field:"created_at", editor:"label",headerFilter:"input",headerFilterPlaceholder:"Date"},
                {title:"Plan", field:"current_plan", editor:"label",headerFilter:"input",headerFilterPlaceholder:"Plan"},
                {title:"Referred By", field:"referral_by", editor:"label",headerFilter:"input",headerFilterPlaceholder:"Referred By"},
                {title:"Status", field:"status", formatter:function(cell, formatterParams, onRendered){
                  if(cell.getValue() == 1){
                      return "<a class='btn btn-success' data-bs-toggle='modal' data-bs-target='#active_model' data-id='' title='Active'> <i class='fa fa-check'></i> </a>";
                  } else {
                      return "<a class='btn btn-danger' data-bs-toggle='modal' data-bs-target='#Deactive_model' data-id='' title='Deactive'> <i class='fa fa-window-close'></i> </a>"
                  }            
               }},
               {title:"Action",width:125, field:"id", formatter:function(cell, formatterParams, onRendered)
               {
                  var url = '{{ route("admin.retailers.edit", ":id") }}';
                  url = url.replace(':id', cell.getValue());
                  var url1 = '{{ route("admin.generalnotification.create", "user_id=:id") }}';
                  url1 = url1.replace(':id', cell.getValue());

                  var editlink = '<a href="'+url+'" class="btn btn-success text-white"><i class="fa fa-edit"></i></a> <a href="'+url1+'" class="btn btn-info text-white sa-warning" title="Send Notification"><i class="fa fa-sms"></i></a>';
                  return editlink; 
               },
               }
            ],
      });

      //trigger download of data.csv file
      // document.getElementById("download-csv").addEventListener("click", function(){
      //     table.download("csv", "data.csv");
      // });

      //trigger download of data.xlsx file
      document.getElementById("download-csv").addEventListener("click", function(){
          table.download("csv", "data.csv");
      });

      //trigger download of data.pdf file
      // document.getElementById("download-pdf").addEventListener("click", function(){
      //     table.download("pdf", "data.pdf", {
      //         orientation:"portrait", //set page orientation to portrait
      //         title:"Example Report", //add title to report
      //     });
      // });

</script>

   </body>
</html>


