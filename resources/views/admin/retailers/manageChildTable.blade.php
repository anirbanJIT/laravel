@foreach($childs as $child)
	<tr data-entry-id="{{ $child->id }}" class="treegrid-{{ $child->id }} treegrid-parent-{{ $parentId }}">
	    <td>
	        {{ $child->name ?? '' }} ({{ $parentId  }})
	    </td>
	    <td>
            {!! $child->getTotalPaidCommission()->sum('commission_amt')!!}
        </td>
        <td>
            {!! $child->getTotalUnpaidCommission()->sum('commission_amt')!!}
        </td>
        <td>
            {!! $child->getTotalPaidCommission()->sum('commission_amt')!!}
        </td>
        <td>
            {!! ucwords(str_replace('_',' ',$child->current_plan)) !!}
        </td>
	</tr>
	@if(count($child->getChildMember))
		@include('admin.retailers.manageChild',['childs' => $child->getChildMember, 'parentId' => $child->id ])
	@endif
@endforeach