<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
      <meta name="description" content="POS - Bootstrap Admin Template">
      <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
      <meta name="author" content="Dreamguys - Bootstrap Admin Template">
      <meta name="robots" content="noindex, nofollow">
      <title>{{ trans('cruds.profile_change_request.title_singular') }} {{ trans('global.list') }}</title>
      
      <link rel="shortcut icon" type="image/x-icon" href="{{ asset('public/Admin/img/favicon.png')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/bootstrap.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/animate.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/dataTables.bootstrap4.min.css')}}">

      <!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.7.1/css/buttons.dataTables.min.css"> -->
      
       

      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/fontawesome.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/all.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/style.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/select2/css/select2.min.css')}}">
      <meta name="csrf-token" content="{{ csrf_token() }}">
   </head>
   <body>
      <!-- <div id="global-loader">
         <div class="whirly-loader"> </div>
      </div> -->
      <div class="main-wrapper">
        @extends('layouts.header')
     
        @extends('layouts.sidebar')
        
        <div class="page-wrapper">
            <div class="content container-fluid">
               <div class="page-header">
                  <div class="row">
                     <div class="col">
                        <h3 class="page-title">{{ trans('cruds.profile_change_request.title_singular') }} {{ trans('global.list') }}</h3>
                        <ul class="breadcrumb">
                           <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">{{ trans('global.dashboard') }}</a></li>
                           <li class="breadcrumb-item active">{{ trans('cruds.profile_change_request.title_singular') }} {{ trans('global.list') }}</li>
                        </ul>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-12">
                     <div class="card">
                        <div class="card-header row">
                            <div class="col-lg-6"><h4 class="card-title">{{ trans('cruds.profile_change_request.title_singular') }} {{ trans('global.list') }}</h4></div>
                            
                        </div>
                        <div class="card-body">
                           <div class="table-responsive">
                              <!-- <table class="table datanew" id="data-table-retailers"> -->
                                 <table id="data-table-retailers" class="table table-striped datanew" >
                                 <thead>
                                    <tr>
                                       <th width="10">Id</th>
                                       <th>Name</th>
                                       <th>Email</th>
                                       <th>Phone</th>
                                       <th>Other Phone</th>
                                       <th>Country</th>
                                       <th>State</th>
                                       <th width="120">Status</th>
                                    </tr>

                                 </thead>
                                 
                                 <tbody>
                                   @foreach($retailerUsers as $retailerUser)
                        <tr>
                           <td>{{$retailerUser->id}}</td>
                           <td><a href="{{route('admin.retailers.edit', $retailerUser->user_id)}}" style="color:orange;">{{$retailerUser->name}}</a></td>
                           <td>{{$retailerUser->email}}</td>
                           <td>{{$retailerUser->phone}}</td>
                           <td>{{$retailerUser->other_phone}}</td>
                           <td>{{$retailerUser->state_name}}</td>
                           <td>{{$retailerUser->country_name}}</td>
                           <td width="120">
                              <select data-id="{{$retailerUser->id}}" class="status_change">
                                 <option value="Pending" @if ($retailerUser->status == "Pending") {{ 'selected' }} @endif >Pending</option>
                                 <option value="Approved" @if ($retailerUser->status == "Approved") {{ 'selected' }} @endif >Approved</option>
                                 <option value="Rejected" @if ($retailerUser->status == "Rejected") {{ 'selected' }} @endif >Rejected</option>
                              </select>
                           </td>
                        </tr>
                        @endforeach
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
    <script src="{{ asset('public/Admin/js/jquery-3.6.0.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/feather.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.slimscroll.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/script.js')}}"></script>
    <script>
$(function() {
   $('.status_change').change(function() {
      var id = $(this).data('id'); 
      var status = this.value;
      $.ajax({
            type:"GET",
            url:"{{url('admin/change-profile-request-status')}}?status="+status+"&id="+id,
            success:function(res){               
             if(res){
                 console.log(res.success);
                 swal({
                      title: "",
                      text: res.success,
                      type: "success",
                  });          
             }
         }
      });
   });   
});
</script>
   </body>
</html>