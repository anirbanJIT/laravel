@extends('layouts.admin')
@section('content')


	<div class="breadcomb-area">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="breadcomb-list">
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<div class="breadcomb-wp">
									<div class="breadcomb-icon">
										<i class="notika-icon notika-windows"></i>
									</div>
									<div class="breadcomb-ctn">
										<h2>{{ $retailer->name ? $retailer->name : '' }} Details</h2>
										
									</div>
								</div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-3">
								<div class="breadcomb-report">
								
									 <a class="btn btn-success notika-btn-success waves-effect" href="{{ url()->previous() }}">
								{{ trans('global.back_to_list') }}
							  </a>
									
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	

   <div class="tabs-info-area">
 <div class="container">
   <div class="row">
     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	  <div class="widget-tabs-int">
			
	   <div class="widget-tabs-list">
			<ul class="nav nav-tabs">
			    <li class="active"><a data-toggle="tab" href="#general">General</a></li>
				<li><a data-toggle="tab" href="#home">Contact Details</a></li>
				<li><a data-toggle="tab" href="#menu1">Bank Details</a></li>
				<li><a data-toggle="tab" href="#menu2">KYC Details</a></li>
				<li><a data-toggle="tab" href="#tab_shop_detail">Shop Details</a></li>
			
			</ul>
		<div class="tab-content tab-custom-st">
			<!-- start tab_shop_detail -->
			<div id="tab_shop_detail" class="tab-pane fade">
				<div class="tab-ctn">
					@if(!empty($retailerShop))
					<table  class="table table-striped">
						<thead>
							<tr>
							<th style="width:33.33%">Shop Name</th>
							<th style="width:33.33%">Address</th>
							<th style="width:33.33%">Pin Code</th>
							</tr>
						</thead>
						<tbody>				
					     	<tr data-entry-id="{{ $retailer->id }}">										  
								<td style="width:33.33%"> {{ $retailerShop->shop_name ?? '' }} </td>				
								<td style="width:33.33%"> {{ $retailerShop->address ?? '' }} </td> 					
			 					<td style="width:33.33%">{{ $retailerShop->pin_code ?? '' }}</td>                  	 
							</tr>				  
					 	</tbody>
					</table>
					<table  class="table table-striped">
						<thead>
							<tr>			  
							<th style="width:33.33%">Country</th>
							<th style="width:33.33%">Image</th>
							<th style="width:33.33%">Image Gallery</th>
							</tr>
						</thead>
						<tbody>									
					     	<tr data-entry-id="{{ $retailer ->id }}">										  
								<td style="width:33.33%"> {{ $retailerShop->country_id ?? '' }} </td>				
								<td style="width:33.33%"><img src="{{ $retailerShop->shop_photo ?? '' }}" class="animate-eleven" style="width:200px;height:200px;"> </td>					
			 					<td style="width:33.33%"><img src="{{ $retailerShop->shop_gallery ?? '' }}" class="animate-eleven" style="width:200px;height:200px;"></td>
							</tr>								  
						 </tbody>
					</table>
			       @else
			    	<h4 style= "color:red; ">Shop Detail is not updated by {{ $retailer->name }} </h4>					  	
			       @endif		
				</div>
			</div>
			<!-- end tab_shop_detail -->

		<div id="general" class="tab-pane fade in active">
		<div class="tab-ctn">
		  <div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			    <div class="table-responsive">
					<table  class="table table-striped">
						<thead>
							<tr>
							<th style="width:20%">Shop Name</th>										  
							<th style="width:20%">Name</th>
							<th style="width:20%">Email</th>
							<th style="width:20%">Phone Number</th>
							<th style="width:20%">Other Phone Number</th>

							</tr>
						</thead>
						<tbody>
							<tr data-entry-id="{{ $retailer ->id }}">
								<td style="width:20%"> {{ $retailer->shop_name ?? '-' }} </td>
								<td style="width:20%"> {{ $retailer->name ?? '' }} </td>
								<td style="width:20%"> {{ $retailer->email ?? '' }} </td> 
							    <td style="width:20%">{{ $retailer->phone ?? '' }}</td>
							    <td style="width:20%">{{ $retailer->other_phone ?? '-' }}</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>                           
				   
					
			</div>
        </div>
		</div>
			
		
		<div id="home" class="tab-pane fade">
		<div class="tab-ctn">
       
				@if(!empty($retailerContact))
					<table  class="table table-striped">
						<thead>
							<tr>			  
							<th style="width:33.33%">Address</th>
							<th style="width:33.33%">Country</th>
							<th style="width:33.33%">State</th>
							</tr>
						</thead>
				<tbody>
							
			     	<tr data-entry-id="{{ $retailer ->id }}">
											  
						<td style="width:33.33%"> {{ $retailerContact->address ?? '' }} </td>
					
						<td style="width:33.33%"> {{ $retailerContact->country ?? '' }} </td> 
						
     					<td style="width:33.33%">{{ $retailerContact->state ?? '' }}</td>
	                  	 
					</tr>
						  
				 </tbody>
					   
					</table>
					<table  class="table table-striped">
						<thead>
							<tr>			  
							<th style="width:33.33%">City</th>
							<th style="width:33.33%">Pin Code</th>
							<th style="width:33.33%"></th>
							</tr>
						</thead>
				<tbody>
							
			     	<tr data-entry-id="{{ $retailer ->id }}">
											  
						<td style="width:33.33%"> {{ $retailerContact->city ?? '' }} </td>
					
						<td style="width:33.33%"> {{ $retailerContact->pin_code ?? '' }} </td> 
						
     					<td style="width:33.33%"></td>
	                  	 
					</tr>
						  
				 </tbody>
					   
					</table>
			      @else
                <h4 style= "color:red; ">Address is not updated by {{ $retailer->name }} </h4>					  	
			     @endif		
				 
					
		</div>
		</div>
			<div id="menu1" class="tab-pane fade">
				<div class="tab-ctn">
			     @if(!empty($retailerBank)) 
                 <table  class="table table-striped">
						<thead>
							<tr>			  
							<th style="width:33.33%">Bank Name</th>
							<th style="width:33.33%">Account Number</th>
							<th style="width:33.33%">Branch Name</th>
							</tr>
						</thead>
						<tbody>
									
							<tr data-entry-id="{{ $retailer ->id }}">
													  
								<td style="width:33.33%"> {{ $retailerBank->bank_name ?? '' }} </td>
							
								<td style="width:33.33%"> {{ $retailerBank->account_no ?? '' }} </td> 
								
								 <td style="width:33.33%">{{ $retailerBank->branch_name ?? '' }}</td>
								 
							</tr>
								  
						 </tbody>
					   
					</table>
					<table  class="table table-striped">
						<thead>
							<tr>			  
							<th style="width:33.33%">Account Holder Name</th>
							<th style="width:33.33%">IFSC Code</th>
							<th style="width:33.33%">Account Type</th>
							</tr>
						</thead>
						<tbody>
									
							<tr data-entry-id="{{ $retailer ->id }}">
													  
								<td style="width:33.33%"> {{ $retailerBank->account_name ?? '' }} </td>
							
								<td style="width:33.33%"> {{ $retailerBank->ifsc_code ?? '' }} </td> 
								
								 <td style="width:33.33%">{{ $retailerBank->account_type ?? '' }}</td>
								 
							</tr>
								  
						 </tbody>
					   
					</table>
			      @else
                <h4 style= "color:red;">Bank detail is not updated by {{ $retailer->name }} </h4>					  	
			     @endif					  
                 						
				</div>
			</div>
			<div id="menu2" class="tab-pane fade">
				<div class="tab-ctn">
				 
				  @if(!empty($retailerKyc)) 
                 <table  class="table table-striped">
						<thead>
							<tr>			  
							<th style="width:33.33%">KYC Type</th>
							<th style="width:33.33%">KYC Number</th>
							<th style="width:33.33%">Cutomer Name</th>
							</tr>
						</thead>
						<tbody>
									
							<tr data-entry-id="{{ $retailer ->id }}">
													  
								<td style="width:33.33%"> {{ $retailerKyc->catName ?? '' }} </td>
							
								<td style="width:33.33%"> {{ $retailerKyc->kyc_number ?? '' }} </td> 
								
								 <td style="width:33.33%">{{ $retailerKyc->customer_name ?? '' }}</td>
								 
							</tr>
								  
						 </tbody>
					   
					</table>
					<table  class="table table-striped">
						<thead>
							<tr>			  
							<th style="width:33.33%">KYC Expiry Date</th>
							<th style="width:33.33%">Date Of Birth</th>
							<th style="width:33.33%">Phone</th>
							</tr>
						</thead>
						<tbody>
									
							<tr data-entry-id="{{ $retailer ->id }}">
													  
								<td style="width:33.33% ;"> {{ $retailerKyc->kyc_expiry_date ?? '' }} </td>
							
								<td style="width:33.33% ;"> {{ $retailerKyc->dob ?? '' }} </td> 
								
								 <td style="width:33.33%">{{ $retailerKyc->phone ?? '' }}</td>
								 
							</tr>
								  
						 </tbody>
					   
					</table>
					<table  class="table table-striped">
						<thead>
							<tr>			  
							<th style="width:33.33%">Email</th>
							<th style="width:33.33%">Address</th>
							<th style="width:33.33%">Country</th>
							</tr>
						</thead>
						<tbody>
									
							<tr data-entry-id="{{ $retailer ->id }}">
													  
								<td style="width:33.33%"> {{ $retailerKyc->emailKyc ?? '' }} </td>
							
								<td style="width:33.33%"> {{ $retailerKyc->address ?? '' }} </td> 
								
								 <td style="width:33.33%">{{ $retailerKyc->countryName ?? '' }}</td>
								 
							</tr>
								  
						 </tbody>
					   
					</table>
										<table  class="table table-striped">
						<thead>
							<tr>			  
							<th style="width:33.33%">State</th>
							<th style="width:33.33%">City</th>
							<th style="width:33.33%">Relation</th>
							</tr>
						</thead>
						<tbody>
									
							<tr data-entry-id="{{ $retailer ->id }}">
													  
								<td style="width:33.33%"> {{ $retailerKyc->stateName ?? '' }} </td>
							
								<td style="width:33.33%"> {{ $retailerKyc->city ?? '' }} </td> 
								
								 <td style="width:33.33%">{{ $retailerKyc->relation ?? '' }}</td>
								 
							</tr>
								  
						 </tbody>
					   
					</table>
					<div class="container">
					<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-6">
					
					<div class="col-lg-9 col-md-9 col-sm-9 col-xs-3">
					<div class="typography-hd-cr-3">
                            <h3>KYC attachment</h3>
                            <p><a href="{{url(asset('images/kyc/'.$retailerKyc->attachment))}}" target="_blank" download><img src="{{asset('images/kyc/'.$retailerKyc->attachment)}}" style="max-width:40%"></a></p>
                    </div>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
					           <p>Download KYC</p>
								<div class="breadcomb-report" style="float:left;">
								    <a href="{{url(asset('images/kyc/'.$retailerKyc->attachment))}}" target="_blank" download>
									<button data-toggle="tooltip" data-placement="left" title="" class="btn waves-effect" data-original-title="Download KYC"><i class="notika-icon notika-sent"></i></button>
								    </a>
								</div>
					</div>
				   </div>	
                    <div class="container">
					<div class="row">
					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-3">					
					<div class="invoice-hs">
					    <div id="res-success">
						          @if(!empty($retailerKyc->status))
					                @if($retailerKyc->status == 1)
                                    <span>Approved</span>
								    @else
									<span>Unapproved</span>
								    @endif
									@else
									<span>Unapproved</span>
									@endif
						</div>
						
					                
                    </div>
				   </div>
				   <div class="col-lg-4 col-md-4 col-sm-4 col-xs-3">
				   </div>
				   <div class="col-lg-4 col-md-4 col-sm-4 col-xs-3">
					         <h5>Approve Or Unapprove KYC</h5>
                             <div class="nk-toggle-switch" data-ts-color="green">
							   @if(!empty($retailerKyc->id))
								<input class="kyc_status_change" data-id="{{$retailerKyc->id ? $retailerKyc->id : ''}}" id="ks{{$retailerKyc->id ? $retailerKyc->id : ''}}" type="checkbox" hidden="hidden" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Approve" data-off="Unapprove" {{ $retailerKyc->status ? 'checked' : '' }}>
								<label for="ks{{$retailerKyc->id ? $retailerKyc->id : ''}}" class="ts-helper"></label>
								@endif
							 </div>
				   </div>
                   </div>				   
                    </div>					   
			      @else
                <h4 style= "color:red;">KYC  detail is not updated by {{ $retailer->name }} </h4>					  	
			     @endif					  
                
					
				</div>
			</div>
			</div>
			</div>
			


			<div id="menu31" class="tab-pane fade">
				<div class="tab-ctn">
				
				
                 					 
		     
            </div>
            </div>			
			
	   </div>
	 </div>
	</div>
    </div>
  </div>                        
 </div>
</div>
<script>
$(function(){
	//for kyc status
		$('.kyc_status_change').change(function() {
		
        var status = $(this).prop('checked') == true ? 1 : 0; 
        var kyc_id = $(this).data('id'); 
		if(kyc_id){
		 $.ajax({
           type:"GET",
           url:"{{url('admin/kyc/changeStatus')}}?status="+status+"&kyc_id="+kyc_id,
           success:function(res){               
            if(res){
				//alert(res.success);
                setInterval(function(){
					  $("#res-success").load(window.location.href + " #res-success" );
				}, 100);
				
                
            }else{
               //alert("nnnn");
            }
           }
		      
        });
		}else{
			alert('KYC is not uploaded of this user!');
		}
    });
	
});
</script>
@endsection