<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
      <meta name="description" content="POS - Bootstrap Admin Template">
      <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
      <meta name="author" content="Dreamguys - Bootstrap Admin Template">
      <meta name="robots" content="noindex, nofollow">
      <title>{{ trans('global.show') }} {{ trans('cruds.generalnotifications.title_singular') }}</title>
      
      <link rel="shortcut icon" type="image/x-icon" href="{{ asset('public/Admin/img/favicon.png')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/bootstrap.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/animate.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/dataTables.bootstrap4.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/fontawesome.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/all.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/style.css')}}">
   </head>
   <body>
      <div id="global-loader">
         <div class="whirly-loader"> </div>
      </div>
      <div class="main-wrapper">
        @extends('layouts.header')
     
        @extends('layouts.sidebar')
        

		<div class="breadcomb-area">

			<div class="container">

				<div class="row">

					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

						<div class="breadcomb-list">

							<div class="row">

								<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">

									<div class="breadcomb-wp">

										<div class="breadcomb-icon">

											<i class="notika-icon notika-windows"></i>

										</div>

										<div class="breadcomb-ctn">

											<h2>Show Model</h2>

											<p>Show <span class="bread-ntd">particular model</span></p>

										</div>

									</div>

								</div>

								<div class="col-lg-6 col-md-6 col-sm-6 col-xs-3">

									<div class="breadcomb-report">

									

										 <a class="btn btn-success notika-btn-success waves-effect" href="{{ url()->previous() }}">

					{{ trans('global.back_to_list') }}

				</a>

										

									</div>

								</div>

							</div>

						</div>

					</div>

				</div>

			</div>

		</div>

	





		   <div class="data-table-area">

				<div class="container">

					<div class="row">

						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

							<div class="data-table-list">

								<!--<div class="basic-tb-hd">

									<h2>Basic Example</h2>

									<p>It's just that simple. Turn your simple table into a sophisticated data table and offer your users a nice experience and great features without any effort.</p>

								</div>-->

								<div class="table-responsive">

									<table id="data-table-basic" class="table table-striped">

						<tbody>

							<tr>

								<th>

									Category

								</th>

								<td>

									{{ isset($brandModel->brandsTypes->name) ? $brandModel->brandsTypes->name : '' }}

								</td>

							</tr>

							 <tr>

								<th>

									Brand

								</th>

								<td>

									{{ isset($brandModel->brands->name) ? $brandModel->brands->name : '' }}

								</td>

							</tr>

							<tr>

								<th>

									Name

								</th>

								<td>

									{{ isset($brandModel->name) ? $brandModel->name : '' }}

								</td>

							</tr>

							<tr>

								<th>

									Slug

								</th>

								<td>

									{{ isset($brandModel->slug) ? $brandModel->slug:'' }}

								</td>

							</tr>

							 <tr>

								<th>

									Status

								</th>

								<td>

									{{ isset($brandModel->status) ? $brandModel->status:'' }}

								</td>

							</tr>
							<!-- <tr>

								<th>

									Cut Point

								</th>

								<td>

									{{ isset($brandModel->cut_point) ? $brandModel->cut_point : '' }}

								</td>

							</tr> -->


							 <tr>

								<th>

									Meta Title

								</th>

								<td>

									{{ isset($brandModel->meta_title) ? $brandModel->meta_title : '' }}

								</td>

							</tr>

							 <tr>

								<th>

									Meta Keywords

								</th>

								<td>

									{{ isset($brandModel->meta_keywords) ? $brandModel->meta_keywords:'' }}

								</td>

							</tr>

							 <tr>

								<th>

									Meta Description

								</th>

								<td>

									{{ isset($brandModel->meta_description) ? $brandModel->meta_description : '' }}

								</td>

							</tr>

							

							<tr>

								<th>

									Image

								</th>

								<td>
								@if(isset($brandModel) && $brandModel->image )
									<a href="{{ asset('images/models/'.$brandModel->image)}}" data-lightbox="lightbox-1"> 
										<img src="{{ asset('images/models/'.$brandModel->image) }}" style="width:200px;height:200px;">
									</a>
								@endif

								</td>

							</tr>

							

						</tbody>

					</table>

				   

				</div>





			</div>

</div>

</div>

</div>

</div>
	
		</div>
 <script src="{{ asset('public/Admin/js/jquery-3.6.0.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/feather.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.slimscroll.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{ asset('public/Admin/plugins/apexchart/apexcharts.min.js')}}"></script>
    <script src="{{ asset('public/Admin/plugins/apexchart/chart-data.js')}}"></script>
    <script src="{{ asset('public/Admin/js/script.js')}}"></script>
   </body>
</html>