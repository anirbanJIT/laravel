@extends('layouts.admin')
@section('content')

<div class="breadcomb-area">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="breadcomb-list">
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<div class="breadcomb-wp">
									<div class="breadcomb-icon">
										<i class="notika-icon notika-windows"></i>
									</div>
									<div class="breadcomb-ctn">
										<h2>Commission Setting Report</h2>
										<p>Show <span class="bread-ntd">all list</span></p>
									</div>
								</div>
							</div>
								<div class="col-lg-6 col-md-6 col-sm-6 col-xs-3">
								<div class="breadcomb-report">
								
									 
									
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
 <!-- Data Table area Start-->
    <div class="data-table-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="data-table-list">
                        <!--<div class="basic-tb-hd">
                            <h2>Basic Example</h2>
                            <p>It's just that simple. Turn your simple table into a sophisticated data table and offer your users a nice experience and great features without any effort.</p>
                        </div>-->
                        <div class="table-responsive">
                            <table id="data-table-basic" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th width="10">

                        </th>
                        <th>
                            User Id
                        </th>
                        <th>
                            Name
                        </th>
                        <th>
                            Commission %
                        </th>
						 <th>
                            Minimum Recharge Amount
                        </th>
                        <th>
                            Commission Duration
                        </th>
                        <th>
                            Status
                        </th>
						<th>
						</th>
						
                                    </tr>
                                </thead>
                                <tbody>
								
                                    @foreach($datas as $key => $data)
                        <tr data-entry-id="{{ $data->id }}">
                            <td>

                            </td>
                            <td>
                                {{ $data->user_id ?? 'All' }}
                            </td>
                            <td>
                                {{ $data->name ?? 'All' }}
                            </td>
                            <td>
                                {{ $data->commission ?? '' }}
                            </td>
							<td>
                                {{ $data->minimum_recharge ?? '' }}
                            </td>
							<td>
                                {{ $data->recharge_month ?? '' }}
                            </td>
							<td>
                                 {!! $data->status==0?'<div class="btn btn-danger">Disable</div>':'<div class="btn btn-success">Enable</div>' ?? '' !!}
                            </td>
							<td><a  href="{{ route('admin.setting.commission', $data->user_id) }}"><button class="btn btn-amber amber-icon-notika btn-reco-mg btn-button-mg waves-effect"><i class="notika-icon notika-star"></i></button></a></td>
							
                                    </tr>
                                   @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                         <th width="10">

                        </th>
                        <th>
                            User Id
                        </th>
                        <th>
                            Name
                        </th>
                        <th>
                            Commission %
                        </th>
						 <th>
                            Minimum Recharge Amount
                        </th>
                        <th>
                            Commission Duration
                        </th>
                        <th>
                            Status
                        </th>
						<th>
						</th>
						
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	
	<script>
    $(function () {
	
	  $('.sa-warning').on('click', function (e) {
   event.preventDefault();
   const url =  $(this).attr('href');
  
   swal({
       title: "Are you sure?",  
text: "Once Deleted. You will also able to recover this again!",  
type: "warning",  
showCancelButton: true,  
confirmButtonText: "Yes, delete it!",
   }).then(function(value) {
       if (value) {
           window.location.href = url;
		  
       }
   });
});
	
	
	$('.status_change').change(function() {
		
        var status = $(this).prop('checked') == true ? 1 : 0; 
        var model_id = $(this).data('id'); 
		 $.ajax({
           type:"GET",
           url:"{{url('admin/changeStatus')}}?status="+status+"&model_id="+model_id,
           success:function(res){               
            if(res){
				//alert(res);
                console.log(res.success);
           
            }else{
               //alert("nnnn");
            }
           }
        });
	
    });	
	});
	</script>
@endsection

