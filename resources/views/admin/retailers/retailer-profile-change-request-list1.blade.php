@extends('layouts.admin')
@section('content')
<div class="breadcomb-area">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="breadcomb-list">
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<div class="breadcomb-wp">
								<div class="breadcomb-icon">
									<i class="notika-icon notika-windows"></i>
								</div>
								<div class="breadcomb-ctn">
									<h2>{{ trans('cruds.profile_change_request.title_singular') }} {{ trans('global.list') }}</h2>
									<p>Show <span class="bread-ntd">all list</span></p>
								</div>
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-3">
							<div class="breadcomb-report">								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Data Table area Start-->
<div class="data-table-area">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="data-table-list">
					<div class="table-responsive">
						<table id="data-table-basic" class="table table-striped">
							<thead>
								<tr>
									<th width="10">Id</th>
									<th>Name</th>
									<th>Email</th>
									<th>Phone</th>
									<th>Other Phone</th>
									<th>Country</th>
									<th>State</th>
									<th width="120">Status</th>
								</tr>
							</thead>
							<tbody>
								@foreach($retailerUsers as $retailerUser)
								<tr>
									<td>{{$retailerUser->id}}</td>
									<td><a href="{{route('admin.retailers.edit', $retailerUser->user_id)}}">{{$retailerUser->name}}</a></td>
									<td>{{$retailerUser->email}}</td>
									<td>{{$retailerUser->phone}}</td>
									<td>{{$retailerUser->other_phone}}</td>
									<td>{{$retailerUser->state_name}}</td>
									<td>{{$retailerUser->country_name}}</td>
									<td width="120">
										<select data-id="{{$retailerUser->id}}" class="status_change">
											<option value="Pending" @if ($retailerUser->status == "Pending") {{ 'selected' }} @endif >Pending</option>
											<option value="Approved" @if ($retailerUser->status == "Approved") {{ 'selected' }} @endif >Approved</option>
											<option value="Rejected" @if ($retailerUser->status == "Rejected") {{ 'selected' }} @endif >Rejected</option>
										</select>
									</td>
								</tr>
								@endforeach
							</tbody>
							<tfoot>
								<tr>
									<th width="10">Id</th>
									<th>Name</th>
									<th>Email</th>
									<th>Phone</th>
									<th>Other Phone</th>
									<th>Country</th>
									<th>State</th>
									<th width="120">Status</th>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
$(function() {
	$('.status_change').change(function() {
		var id = $(this).data('id'); 
		var status = this.value;
		$.ajax({
	   		type:"GET",
	   		url:"{{url('admin/change-profile-request-status')}}?status="+status+"&id="+id,
	   		success:function(res){               
			    if(res){
			        console.log(res.success);
			        swal({
		                title: "",
		                text: res.success,
		                type: "success",
		            });	       
			    }
		   }
		});
	});	
});
</script>
@endsection

