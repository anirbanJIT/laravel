<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
      <meta name="description" content="POS - Bootstrap Admin Template">
      <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
      <meta name="author" content="Dreamguys - Bootstrap Admin Template">
      <meta name="robots" content="noindex, nofollow">
      <title>{{ trans('cruds.profile_change_request.title_singular') }} {{ trans('global.list') }}</title>
      
      <link rel="shortcut icon" type="image/x-icon" href="{{ asset('public/Admin/img/favicon.png')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/bootstrap.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/animate.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/dataTables.bootstrap4.min.css')}}">

      <!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.7.1/css/buttons.dataTables.min.css"> -->
      
       

      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/fontawesome.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/all.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/style.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/select2/css/select2.min.css')}}">
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <link rel="stylesheet" href="{{ asset('public/Admin/tabulator/bootstrap/tabulator_bootstrap4.css')}}">
		
   </head>
   <body>
      <!-- <div id="global-loader">
         <div class="whirly-loader"> </div>
      </div> -->
      <div class="main-wrapper">
        @extends('layouts.header')
     
        @extends('layouts.sidebar')
        
        <div class="page-wrapper">
            <div class="content container-fluid">
               <div class="page-header">
                  <div class="row">
                     <div class="col">
                        <h3 class="page-title">{{ trans('cruds.profile_change_request.title_singular') }} {{ trans('global.list') }}</h3>
                        <ul class="breadcrumb">
                           <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">{{ trans('global.dashboard') }}</a></li>
                           <li class="breadcrumb-item active">{{ trans('cruds.profile_change_request.title_singular') }} {{ trans('global.list') }}</li>
                        </ul>
                     </div>
                  </div>
               </div>
                <div class="row">
                  <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header row">
                            <div class="col-lg-6"><h4 class="card-title">{{ trans('cruds.profile_change_request.title_singular') }} {{ trans('global.list') }}</h4></div>
                            <div class="col-lg-6">
								
							</div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
								<div id="data-table-profileChangeRequest-list" ></div>
							</div>
                        </div>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
    <script src="{{ asset('public/Admin/js/jquery-3.6.0.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/feather.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.slimscroll.min.js')}}"></script>
  
    <script src="{{ asset('public/Admin/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/script.js')}}"></script>
    <script src="{{ asset('public/Admin/tabulator/tabulator.min.js') }}"></script>
    <script src="{{ asset('public/Admin/tabulator/jspdf.min.js') }}"></script>
    <script src="{{ asset('public/Admin/tabulator/xlsx.full.min.js') }}"></script>
    <script src="{{ asset('public/Admin/tabulator/jspdf.plugin.autotable.js') }}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script type="text/javascript">
		var tabledata = <?php echo json_encode($retailerUsers); ?>;
		var url = "{{URL::asset('public/Admin/images/brand-types/')}}"+'/';
      
		var table = new Tabulator("#data-table-profileChangeRequest-list", {
			data:tabledata,           //load row data from array
			layout:"fitColumns",      //fit columns to width of table
			responsiveLayout:"collapse",  //hide columns that dont fit on the table
			tooltips:true,            //show tool tips on cells
			addRowPos:"top",          //when adding a new row, add it to the top of the table
			history:true,             //allow undo and redo actions on the table
			pagination:"local",       //paginate the data
			paginationSize:10,         //allow 7 rows per page of data
			movableColumns:true,      //allow column order to be changed
			resizableRows:true,       //allow row order to be changed
			columns: [
				{title:"ID", field:"id", editor:"label",headerFilter:"input",headerSort:false,				headerFilterPlaceholder:"Id",},
				{title:"Name", field:"user_id_name", editor:"label",headerFilter:"input",headerSort:false,			headerFilterPlaceholder:"Name",
					formatter:function(cell, formatterParams, onRendered){
						var arr = cell.getValue();
						var data = arr.split("_");
						var link = "{{route('admin.retailers.edit', ':id')}}";
						link = link.replace(':id', data[0]);
						return '<a href="'+link+'" style="color:orange;">'+data[1]+'</a>';
					}
				},
				{title:"Email", field:"email", editor:"label",headerFilter:"input",headerSort:true,		headerFilterPlaceholder:"Email"},
				{title:"Phone", field:"phone", editor:"label",headerFilter:"input",headerSort:true,		headerFilterPlaceholder:"Phone"},
				{title:"Other Phone", field:"other_phone", editor:"label",headerFilter:"input",headerSort:true,		headerFilterPlaceholder:"Other Phone"},
				{title:"Country", field:"country_name", editor:"label",headerFilter:"input",headerSort:true,		headerFilterPlaceholder:"Country"},
				{title:"State", field:"state_name", editor:"label",headerFilter:"input",headerSort:true,		headerFilterPlaceholder:"State"},
        	    {title:"Status", field:"id_status", headerSort:false,
					formatter:function(cell, formatterParams, onRendered){
						var arr = cell.getValue();
						var data = arr.split("_");
						var html = '';
						html += '<select data-id="'+data[0]+'" class="status_change"><option value="Pending"'
							if (data[1] == "Pending")
								html += 'selected';
						html += '>Pending</option><option value="Approved"';
							if (data[1] == "Approved") 
								html += 'selected'; 
						html += '>Approved</option><option value="Rejected"';
							if (data[1] == "Rejected") 
								html += 'selected';  
						html += '>Rejected</option></select>';
						
						return html;
					}
				},
				
			],
			
		});
		
		$('.status_change').change(function() 
         {      
			var id = $(this).data('id'); 
			var status = this.value;
			$.ajax({
				type:"GET",
				url:"{{url('admin/change-profile-request-status')}}?status="+status+"&id="+id,
				success:function(res){               
					if(res){
						console.log(res.success);
						swal({
						  title: "",
						  text: res.success,
						  type: "success",
						});          
					}
				}
			});
		});
		
		
		$('#master').on('click', function(e) {  
			 if($(this).is(':checked',true))    
			 {  
				$(".sub_chk").prop('checked', true);    
			 } else {    
				$(".sub_chk").prop('checked',false);    
			 }    
        });  
		
		 $('.delete_all').on('click', function(e) {  
			/* $("input[type='checkbox']").prop('checked', true);     */
			 
            var allVals = [];    
            $(".sub_chk:checked").each(function() {    
                allVals.push($(this).attr('data-id'));  
            }); 

			
            if(allVals.length <=0)    
            {    
                alert("Please select Category row.");    
            }  else {    
  
                var check = confirm("Are you sure you want to delete this Category?");    
                if(check == true){    
  
                    var join_selected_values = allVals.join(",");   
					console.log(join_selected_values);
                    $.ajax({  
                        url: $(this).data('url'),  
                        type: 'DELETE',  
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},  
                        data: 'ids='+join_selected_values,  
                        success: function (data) {  
                            if (data['success']) {  
                                $("input[type='checkbox']:checked").each(function() {    
                                    $(this).parents("tr").remove();  
                                });  
                                alert(data['success']); 
								window.location.reload();
                            } else if (data['error']) {  
                                alert(data['error']);  
                            } else {  
                                alert('Whoops Something went wrong!!');  
                            }  
                        },  
                        error: function (data) {  
                            alert(data.responseText);  
                        }  
                    });   
  
                  $.each(allVals, function( index, value ) {  
                      $('table tr').filter("[data-row-id='" + value + "']").remove();  
                  });  
                }    
            }    
        }); 


		//trigger download of data.xlsx file
		/* document.getElementById("download-csv").addEventListener("click", function(){
			table.download("csv", "data.csv");
		}); */
	</script>
   </body>
</html>