@extends('layouts.admin')
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
<div class="breadcomb-area">
		<div class="container">
			<div class="row">
					<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="breadcomb-list">
						<div class="row">
							<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
								<div class="breadcomb-wp">
									<div class="breadcomb-icon">
										<i class="notika-icon notika-windows"></i>
									</div>
									<div class="breadcomb-ctn">
										<h2>Users Reffered Member</h2>
										<p>Show <span class="bread-ntd">all list</span></p>
									</div>
								</div>
							</div>
								<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
								
								<input type="text" class="form-control" name="referred_by" id="referred_by" placeholder="Entet User Id/Email"></div>
								<div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
									 <input type="submit" class="btn btn-success notika-btn-success waves-effect" id="btnFiterSubmitSearch" value="Search" >
									 </div>
									
								
							
						</div>
					</div>
				</div>
			</div>
			</div>
		</div>
	</div>
	
 <!-- Data Table area Start-->
    <div class="data-table-area" id="users-table">
        <div class="container">
            <div class="row">
			
                
                    <div class="data-table-list">
                        <!--<div class="basic-tb-hd">
                            <h2>Basic Example</h2>
                            <p>It's just that simple. Turn your simple table into a sophisticated data table and offer your users a nice experience and great features without any effort.</p>
                        </div>-->
                         
   
     <div class="table-responsive">
       <table id="data-table-basic" class="table table-striped" >
       <thead>
          <tr>
             <th>Id</th>
             <th>Name</th>
             <th>Email</th>
			 <th>Phone</th>
             <th>Created At</th>
			 <th>Status</th>
          </tr>
       </thead>
    </table>
 </div>
                   
                </div>
            </div>
        </div>
    </div>
	
	<script type="text/javascript">
$(document).ready( function () {
     $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
  $('#data-table-basic').DataTable({
         processing: true,
         serverSide: true,
         ajax: {
          url: "{{ url('admin/direct-users-list') }}",
          type: 'GET',
          data: function (d) {
          d.referred_by = $('#referred_by').val();
         
          }
         },
         columns: [
                  { data: 'id', name: 'id' },
                  { data: 'name', name: 'name' },
                  { data: 'email', name: 'email' },
				  { data: 'phone', name: 'phone' },
                  {
           data: 'created_at',
           type: 'num',
           render: {
              _: 'display',
              sort: 'timestamp'
           }
        },
				  { data: 'status', name: 'status' }
               ]
      });
   });
 
  $('#btnFiterSubmitSearch').click(function(){
     $('#data-table-basic').DataTable().draw(true);
  });
</script>
	<style>
	.dataTables_filter
	{
		display:none;
	}
	</style>
@endsection

