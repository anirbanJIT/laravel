@extends('layouts.admin')
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
<div class="breadcomb-area">
		<div class="container">
			<div class="row">
					<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="breadcomb-list">
						<div class="row">						
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<div class="breadcomb-wp">
									<div class="breadcomb-icon">
										<i class="notika-icon notika-windows"></i>
									</div>
									<div class="breadcomb-ctn">
										<h2>Retailers List</h2>
										<p>Show <span class="bread-ntd">all list</span></p>
									</div>
								</div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-3">
								<div class="breadcomb-report">
									<a class="btn btn-success notika-btn-success waves-effect" href="{{ route('admin.retailers.create') }}">
						                {{ trans('global.add') }} {{ trans('cruds.retailer.title_singular') }}
						            </a>
						        </div>
							</div>

							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">&nbsp;</div>
							
								<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
								
								<div class="form-group ic-cmp-int">
								<div class="form-ic-cmp">
								<i class="notika-icon notika-support"></i>
								</div>
								<div class="nk-int-st">
								<input type="text" class="form-control" name="name" id="name" placeholder="Name">
								</div>
								</div>
								</div>
								<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
								
								<div class="form-group ic-cmp-int">
								<div class="form-ic-cmp">
								<i class="notika-icon notika-phone"></i>
								</div>
								<div class="nk-int-st">
								<input type="text" class="form-control" name="phone" id="phone" placeholder="Mobile No.">
								</div>
								</div>
								</div>
								
								<div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
								<div class="form-group nk-datapk-ctm form-elet-mg" id="data_1" >
								<div class="input-group date nk-int-st">
								<span class="input-group-addon"></span>
								<input type="text" class="form-control " name="from_date" id="from_date"  placeholder="From Date" >
								</div>
								</div>
								</div>
								
								<div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">								
								<div class="form-group nk-datapk-ctm form-elet-mg" id="data_1">
								<div class="input-group date nk-int-st">
								<span class="input-group-addon"></span>
								<input type="text" class="form-control" name="to_date" id="to_date" placeholder="To Date">
								</div>
								</div>
								</div>
								
								<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                               
                                <div class="bootstrap-select">
                                    <select class="selectpicker" name="status" id="status">
									<option value="">Select Status</option>
										<option value="1">Active</option>
										<option value="0">Inactive</option>
										</select>
										</div>
										
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                               
                                <div class="bootstrap-select">
                                    <select class="selectpicker" name="country" id="country">
									<option value="">Select Country</option>
										@foreach($countries as $key => $country)
										@if(!empty($user->getContactDetail->country))
									    <option  value="{{$key}}" {{$user->getContactDetail->country == $key  ? 'selected' : ''}}> {{$country}}</option>
									    @else
										 <option  value="{{$key}}"> {{$country}}</option>
                                         @endif									 
									    @endforeach
								    </select>
										</select>
										</div>
										
									</div>
								
								<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
									<div class="form-group ic-cmp-int">
										<div class="form-ic-cmp">
											<i class="notika-icon notika-credit-card"></i>
										</div>
										<div class="nk-int-st">
											<input type="text" class="form-control" name="kyc_number" id="kyc_number" placeholder="Approved By">
										</div>
									</div>
								</div>
								<!-- <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
									<div class="form-group ic-cmp-int">
										<div class="form-ic-cmp">
											<i class="notika-icon notika-promos"></i>
										</div>
										<div class="nk-int-st">
											<input type="text" class="form-control" name="current_plan" id="current_plan" placeholder="Plan name">
										</div>
									</div>
								</div> -->
								<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
									<div class="form-group ic-cmp-int">
										<div class="form-ic-cmp">
											<i class="notika-icon notika-support"></i>
										</div>
										<div class="nk-int-st">
											<input type="text" class="form-control" name="referred_by" id="referred_by" placeholder="Referred By">
										</div>
									</div>
								</div>
								<div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
									 <input type="submit" class="btn btn-success notika-btn-success waves-effect" id="btnFiterSubmitSearch" value="Search" >
									 </div>
									
								
							
						</div>
					</div>
				</div>
			</div>
			</div>
		</div>
	</div>
	
 <!-- Data Table area Start-->
    <div class="data-table-area" id="users-table">
        <div class="container">
            <div class="row">
			
                
                    <div class="data-table-list">
                        <!--<div class="basic-tb-hd">
                            <h2>Basic Example</h2>
                            <p>It's just that simple. Turn your simple table into a sophisticated data table and offer your users a nice experience and great features without any effort.</p>
                        </div>-->
                         
   
     <div class="table-responsive">
       <table id="data-table-basic" class="table table-striped" >
       <thead>
          <tr>
             <th>Id</th>
             <th>Name</th>
             <th>Email</th>
			 <th>Phone</th>
			 <th>Country</th>
			 <th>Approved By</th>
             <th>Date</th>
             <th>Plan</th>
             <th>Referred By</th>
			 <th>Status</th>
			 <th>Action</th>
          </tr>
       </thead>
    </table>
 </div>
                   
                </div>
            </div>
        </div>
    </div>

<!-- Modal Change Password -->
<div class="modal fade" id="user_password_change" tabindex="-1" role="dialog" aria-labelledby="user_password_change" aria-hidden="true">
    <div class="modal-dialog modal-xs" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="user_password_change">Change Password?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="" method="POST" id="frm_password_change">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label>New Password</label>
                        <input type="text" class="form-control" name="password" maxlength="30" id="password" required="" pattern=".{6,}" title="Password must be at least 6 characters" x-moz-errormessage="Password must be at least 6 characters">                        
                        @if ($errors->has('password'))
                        <span class="text-danger">{{ $errors->first('password') }}</span>
                        @endif
                    </div> 
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Modal -->	
	<script type="text/javascript">
$(document).ready( function () {
 $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });
  $('#data-table-basic').DataTable({
         processing: true,
         serverSide: true,
         ajax: {
          url: "{{ url('admin/retailers-list-report') }}",
          type: 'GET',
          data: function (d) {
          d.name = $('#name').val();
		  d.phone = $('#phone').val();
		  d.from_date = $('#from_date').val();
		  d.to_date = $('#to_date').val();
		  d.status = $('#status').val();
		  d.country = $('#country').val();
		  d.kyc_number = $('#kyc_number').val();
		  d.referred_by = $('#referred_by').val();
         
          }
         },
         columns: [
					{ data: 'id', name: 'id' },
					{ data: 'name', name: 'name' },
					{ data: 'email', name: 'email' },
					{ data: 'phone', name: 'phone' },
					{ data: 'country', name: 'country' },
					{ data: 'kyc_number', name: 'kyc_number' },
                  	{
			           	data: 'created_at',
			           	type: 'num',
			           	render: {
			              	_: 'display',
			              	sort: 'timestamp'
		           		}
		           	},
				  	{ data: 'current_plan', orderable:false, searchable:false},
				  	{ data: 'referred_by', orderable:false, searchable:false},
				  	{ data: 'status', orderable:false, searchable:false},
					{ data: "allAction", orderable:false, searchable:false}
               ]
      });
   });
 
  $('#btnFiterSubmitSearch').click(function(){
     $('#data-table-basic').DataTable().draw(true);
  });
  
  
  $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('users_manage')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.users.mass_destroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  $.extend(true, $.fn.dataTable.defaults, {
    order: [[ 1, 'desc' ]],
    pageLength: 10,
  });
  $('.datatable-User:not(.ajaxTable)').DataTable({ buttons: dtButtons })
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
        $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust();
    });
	
	
	
});

/*start change password*/
$('#user_password_change').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget);
    var id = button.data('id');
    var url = button.data('url');
    $('#frm_password_change').attr("action", url);
    $('#frm_password_change').find('#password').val('');
})
/*end change password*/
/*start status_change*/
$('.btn-orange').click(function() {
    var status = $(this).prop('checked') == true ? 1 : 0; 
    console.log(status);return false;
    var model_id = $(this).data('id');
    $.ajax({
       type:"GET",
       url:"{{url('admin/changeStatusBrand')}}?status="+status+"&brand_id="+model_id,
       success:function(res){
			if(res){
			    console.log(res.success);
			}else{
				console.log(res);
			}
    	}
	});
});
/*end status_change*/
</script>
	<style>
	.dataTables_filter
	{
		display:none;
	}
	</style>
@endsection

