@extends('layouts.admin')
@section('content')
<style type="text/css">
.text-wrap{
    white-space:normal;
	word-break: break-all;
}
.width-100{
    width:100px;
}
</style>
<meta name="csrf-token" content="{{ csrf_token() }}">
<!-- breadcomb-area -->
<div class="breadcomb-area">
   	<div class="container">
		<div class="row">
		 	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		    	<div class="breadcomb-list">
		       		<div class="row">
		          		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		             		<div class="breadcomb-wp">
		                		<div class="breadcomb-icon">
		                   			<i class="notika-icon notika-support"></i>
		                		</div>
		                		<div class="breadcomb-ctn">
		                   			<h2>Retailers List</h2>
									<p>Show <span class="bread-ntd">all list</span></p>
		                		</div>
		            		</div>
		          		</div>
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-3">
						 	<div class="breadcomb-report">
						    	<a class="btn btn-success notika-btn-success waves-effect" href="{{ route('admin.retailers.create') }}" title="{{ trans('global.add') }} {{ trans('cruds.retailer.title_singular') }}" > {{ trans('global.add') }} {{ trans('cruds.retailer.title_singular') }} </a>
						 	</div>
						</div>
		       		</div>
		    	</div>
		 	</div>
		</div>
		<!-- start filter-area -->
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="form-element-list mg-t-0">
			      	<div class="row">
			      		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
				           	<div class="form-example-int form-example-st">
				              	<div class="form-group">
				                 	<div class="nk-int-st">
				                    	<select class="selectpicker select-filter" name="id" id="id" data-live-search="true">
										<option value="">Id</option>
										@foreach($retailerUsers as $key => $retailerUser) 
											<option value="{{ $retailerUser->id ?? '' }}">{{ $retailerUser->id ?? '' }}  </option>
										@endforeach
										</select>
				                 	</div>
				              	</div>
				           	</div>
				        </div>
				        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
				           	<div class="form-example-int form-example-st">
				              	<div class="form-group">
				                 	<div class="nk-int-st">
				                    	<select class="selectpicker select-filter" name="name" id="name" data-live-search="true">
										<option value="">Name</option>
										@foreach($retailerUsers as $key => $retailerUser) 
											<option value="{{ $retailerUser->name ?? '' }}">{{ $retailerUser->name ?? '' }}  </option>
										@endforeach
										</select>
				                 	</div>
				              	</div>
				           	</div>
				        </div>
				        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
				           	<div class="form-example-int form-example-st">
				              	<div class="form-group">
				                 	<div class="nk-int-st">
				                    	<select class="selectpicker select-filter" name="phone" id="phone" data-live-search="true">
										<option value="">Mobile No.</option>
										@foreach($retailerUsers as $key => $retailerUser) 
											<option value="{{ $retailerUser->phone ?? '' }}">{{ $retailerUser->phone ?? '' }}  </option>
										@endforeach
										</select>
				                 	</div>
				              	</div>
				           	</div>
				        </div>
						<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
							<div class="form-group nk-datapk-ctm form-elet-mg" id="data_1" >
								<div class="input-group date nk-int-st">
									<span class="input-group-addon"></span>
									<input type="text" class="form-control input-filter" name="from_date" id="from_date"  placeholder="From Date" >
								</div>
							</div>
						</div>
						<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">					
							<div class="form-group nk-datapk-ctm form-elet-mg" id="data_1">
								<div class="input-group date nk-int-st">
									<span class="input-group-addon"></span>
									<input type="text" class="form-control input-filter" name="to_date" id="to_date" placeholder="To Date">
								</div>
							</div>
						</div>						
						<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
				           	<div class="form-example-int form-example-st">
				              	<div class="form-group">
				                 	<div class="nk-int-st">
				                    	<select class="selectpicker select-filter" name="country" id="country" data-live-search="true">
											<option value="">Country</option>
											@foreach($countries as $key => $country)
												<option  value="{{$key}}"> {{$country}}</option>
									    	@endforeach
										</select>
				                 	</div>
				              	</div>
				           	</div>
				        </div>
				    </div>
				    <div class="row mg-t-30">				    	
						<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
				           	<div class="form-example-int form-example-st">
				              	<div class="form-group">
				                 	<div class="nk-int-st">
				                    	<select class="selectpicker select-filter" name="current_plan" id="current_plan">
											<option value="">Plan </option>
											<option value="main_plan"> Main Plan</option>
											<option value="light_plan">Light Plan</option>
									</select>
				                 	</div>
				              	</div>
				           	</div>
				        </div>
						<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
				           	<div class="form-example-int form-example-st">
				              	<div class="form-group">
				                 	<div class="nk-int-st">
				                    	<select class="selectpicker select-filter" name="status" id="status">
											<option value="" {{$status==2 ? 'selected' : null}}>Status</option>
											<option value="1" {{$status==1 ? 'selected' : null}}>Active</option>
											<option value="0" {{$status==0 ? 'selected' : null}}>Inactive</option>
										</select>
				                 	</div>
				              	</div>
				           	</div>
				        </div>
				        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
				           	<div class="form-example-int form-example-st">
				              	<div class="form-group">
				                 	<div class="nk-int-st">
				                    	<select class="selectpicker select-filter" data-live-search="true" name="kyc_number" id="kyc_number">
										<option value="">Approved By</option>
										@php
										$kyc_number_data = [];
										@endphp
										@foreach($retailerUsers as $key => $retailerUser)
											@if(
												isset($retailerUser->getKycDetail->kyc_number) && !empty($retailerUser->getKycDetail->kyc_number) 
												)
												@php
													$kyc_number_data[] = $retailerUser->getKycDetail->kyc_number;
												@endphp
											@endif
										@endforeach
										@php
										$collection = collect( $kyc_number_data )->unique();
										@endphp
										@foreach($collection as $key => $value) 
											<option value="{{ $value }}">{{ $value }}  </option>
										@endforeach
										</select>
				                 	</div>
				              	</div>
				           	</div>
				        </div>
				        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
				           	<div class="form-example-int form-example-st">
				              	<div class="form-group">
				                 	<div class="nk-int-st">
				                    	<select class="selectpicker select-filter" data-live-search="true" name="referred_by" id="referred_by" >
										<option value="">Referred By</option>
										@php
										$collection = collect( $retailerUsers )->unique('referred_by');
										@endphp
										@foreach($collection as $key => $retailerUser) 
											@if(isset($retailerUser->referred_by) && !empty($retailerUser->referred_by) )
											<option value="{{ $retailerUser->referred_by }}">{{ $retailerUser->referred_by }}  </option>
											@endif
										@endforeach
										</select>
				                 	</div>
				              	</div>
				           	</div>
				        </div>
				        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
				           	<div class="form-example-int form-example-st">
				              	<div class="form-group">
				                 	<div class="nk-int-st">
				                 		<button class="btn btn-success notika-btn-success" id="resetBtn" title="Reset">Reset</button>
				                 	</div>
				              	</div>
				           	</div>
				        </div>
				    </div>
				</div>
			</div>
		</div>
      <!-- end filter-area -->
   </div>
</div>
<!-- end breadcomb-area -->	
<!-- Data Table area Start-->
    <div class="data-table-area" id="users-table">
        <div class="container">
            <div class="row">
            	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="data-table-list">
                        <div class="basic-tb-hd">
                        	<button style="margin-bottom: 10px" class="btn btn-danger notika-btn-danger waves-effect delete_all" data-url="{{ url('admin/retailers-list-deleteall') }}" title="Delete All Selected">Delete All Selected</button>
                        </div>
					     <div class="table-responsive">
					       <table id="data-table-retailers" class="table table-striped" >
					       <thead>
					          <tr>
								<th><input type="checkbox" id="master"></th>
								<th>Id</th>
								<th width="50">Name</th>
								<th width="60">Email</th>
								<th>Phone</th>
								<th>Country</th>
								<th width="50">Approved By</th>
								<th>Date</th>
								<th>Plan</th>
								<th>Referred By</th>
								<th>Status</th>
								<th width="100">Action</th>
					          </tr>
					       </thead>
					    </table>
					 </div>                   
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal Change Password -->
<div class="modal fade" id="user_password_change" tabindex="-1" role="dialog" aria-labelledby="user_password_change" aria-hidden="true">
    <div class="modal-dialog modal-xs" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="user_password_change">Change Password?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="" method="POST" id="frm_password_change">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label>New Password</label>
                        <input type="text" class="form-control" name="password" maxlength="30" id="password" required="" pattern=".{6,}" title="Password must be at least 6 characters" x-moz-errormessage="Password must be at least 6 characters">                        
                        @if ($errors->has('password'))
                        <span class="text-danger">{{ $errors->first('password') }}</span>
                        @endif
                    </div> 
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Modal -->
@endsection
@push('scripts')
<script type="text/javascript">
$(document).ready( function () {
 	/* @author Naren Thaker @narenthaker.com 101220201500 */
    window._token = $('meta[name="csrf-token"]').attr('content');

    let excelButtonTrans = '{{ trans('global.datatables.export') }} {{ trans('global.datatables.excel') }}';

    $.extend(true, $.fn.dataTable.Buttons.defaults.dom.button, { className: 'btn' })

    $.extend(true, $.fn.dataTable.defaults, {
    	order: [[ 1, 'desc' ]],
        responsive:!1,
        lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
        pageLength:10,
        processing: true,
        serverSide: true,
        buttons: [],
        dom: 'lBfrtip<"actions">',
    });

  	let table = $('#data-table-retailers').DataTable({
        ajax: {
        	headers: {'x-csrf-token': _token},
          	url: "{{ url('admin/retailers-list-report') }}",
          	type: 'GET',
          	data: function (d) {
				d.id = $('#id').val();
				d.name = $('#name').val();
				d.phone = $('#phone').val();
				d.from_date = $('#from_date').val();
				d.to_date = $('#to_date').val();
				d.status = $('#status').val();
				d.country = $('#country').val();
				d.current_plan = $('#current_plan').val();
				d.kyc_number = $('#kyc_number').val();
				d.referred_by = $('#referred_by').val();         
          	}
         },
         columnDefs: [
            {
                render: function (data, type, full, meta) {
                	if (data===null) {
                		return "<div class='text-wrap width-100'>---</div>";
                	}
                    return "<div class='text-wrap width-100'>" + data + "</div>";
                },
                targets: 3
            }
         ],
         columns: [
					{ data: "delete_chk", orderable:false, searchable:false},
					{ data: 'id', name: 'id', orderable:true, searchable:true, title: 'Id' },
					{ data: 'name', name: 'name', title: 'Name'  },
					{ data: 'email', name: 'email', title: 'Email'  },
					{ data: 'phone', name: 'phone', title: 'Phone'  },
					{ data: 'country', name: 'country', title: 'Country'  },
					{ data: 'kyc_number', name: 'kyc_number', title: 'Approved By'  },
                  	{
			           	data: 'created_at',
			           	type: 'num',
			           	render: {
			              	_: 'display',
			              	sort: 'timestamp'
		           		}, 
		           		title: 'Date'
		           	},
				  	{ data: 'current_plan', orderable:false, searchable:false, title: 'Plan'},
				  	{ data: 'referred_by', orderable:false, searchable:false, title: 'Referred By'},
				  	{ data: 'status', orderable:false, searchable:false, title: 'Status'},
					{ data: "allAction", orderable:false, searchable:false}
        ],
		buttons: [
			{
				extend: 'excel',
				className: 'btn btn-success notika-btn-success waves-effect',
				text: excelButtonTrans,
				title: 'Retailers List',
				exportOptions: {
				columns: [ 1, 2, 3, 4, 5, 6, 7, 8, 9 ]
				},
			  	action: function(e, dt, node, config) {
				    var dtButton= this;
				    var currentPageLen = dt.page.len();
				    var currentPage = dt.page.info().page; 
				    dt.one( 'draw', function () {
				        $.fn.DataTable.ext.buttons.excelHtml5.action.call(dtButton, e, dt, node, config);  
				        setTimeout(function() {
				            dt.page.len(currentPageLen).draw();
				            dt.page(currentPage).draw('page');
				        }), 500;
				    });
				    dt.page.len(-1).draw();
				} 
			},
		],
    });

	let searchParams = new URLSearchParams(window.location.search);
	if (searchParams.has('status')) {
		let status = searchParams.get('status')
		$('select[name=status]').val(status);
		$('.selectpicker').selectpicker('refresh');
		$('#btnFiterSubmitSearch').trigger('click');
	}
});
 
$('#btnFiterSubmitSearch').click(function(){
 $('#data-table-retailers').DataTable().draw(true);
});


$('#resetBtn').click(function(){
	$(".selectpicker").val('').trigger('change')
	$('.input-filter').val('');
    $('#data-table-retailers').DataTable().draw(true);
});

$('.select-filter').on('change', function (e) {
    $('#data-table-retailers').DataTable().draw(true);
});
$('.input-filter').on('keyup change', function (e) {
    $('#data-table-retailers').DataTable().draw(true);
});

  
$(function () {
let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('users_manage')
  	let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  	let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.users.mass_destroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan
$.extend(true, $.fn.dataTable.defaults, {
	order: [[ 1, 'desc' ]],
	pageLength: 10,
});
$('.datatable-User:not(.ajaxTable)').DataTable({ buttons: dtButtons })
	$('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
    	$($.fn.dataTable.tables(true)).DataTable()
        .columns.adjust();
	});
});

/*start change password*/
$('#user_password_change').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget);
    var id = button.data('id');
    var url = button.data('url');
    $('#frm_password_change').attr("action", url);
    $('#frm_password_change').find('#password').val('');
})
/*end change password*/
/*start status_change*/
$('.btn-orange').click(function() {
    var status = $(this).prop('checked') == true ? 1 : 0; 
    console.log(status);return false;
    var model_id = $(this).data('id');
    $.ajax({
       type:"GET",
       url:"{{url('admin/changeStatusBrand')}}?status="+status+"&brand_id="+model_id,
       success:function(res){
			if(res){
			    console.log(res.success);
			}else{
				console.log(res);
			}
    	}
	});
});
/*end status_change*/
/*deleted multipal*/
$('#master').on('click', function(e) {
     if($(this).is(':checked',true))  
     {
        $(".sub_chk").prop('checked', true);  
     } else {  
        $(".sub_chk").prop('checked',false);  
     }  
    });

    $('.delete_all').on('click', function(e) {


        var allVals = [];  
        $(".sub_chk:checked").each(function() {  
            allVals.push($(this).attr('data-id'));
        });  

        
        if(allVals.length <=0)  
        {  
            alert("Please select row.");  
        }  else {  
            var check = confirm("Are you sure you want to delete this row?");  
            if(check == true){  
                var join_selected_values = allVals.join(","); 
                console.log(join_selected_values);
                $.ajax({

                    url: $(this).data('url'),
                    type: 'POST',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    data: 'ids='+join_selected_values,
                    success: function (data) {
                        if (data['success']) {
                            $(".sub_chk:checked").each(function() {  
                                $(this).parents("tr").remove();
                            });
                            alert(data['success']);
                        } else if (data['error']) {
                        	
                            alert(data['error']);
                        }
                         else {
                            alert('Whoops Something went wrong!!');
                        }
                    },
                    error: function (data) {
                        alert(data.responseText);
                    }
                });


              $.each(allVals, function( index, value ) {
                  $('table tr').filter("[data-row-id='" + value + "']").remove();
              });
            }  
        }  
    });
/*end deleted multipal*/

/* start export_buttons */
$("#ExportReporttoExcel").on('click', function() {
	$('#data-table-retailers').DataTable().buttons(0,0).trigger();
});
/* end export_buttons */
$.fn.modal.Constructor.prototype.enforceFocus = function() {};
$('.change_status_modal').on('show.bs.modal', function (event) {
	/*$('.change_status_modal.in').find('#current_plan').selectpicker('refresh');
	$('.change_status_modal.in').find('#current_plan_id').selectpicker('refresh');*/
});
//On change
function changePlandID(id) {
  	if(id){
      $.ajax({
         type:"GET",
         url:"{{url('user/get-plans-list')}}?current_plan_type="+id,
         success:function(res){               
          if(res){
              $('.change_status_modal.in').find("#current_plan_id").empty();
              $('.change_status_modal.in').find("#current_plan_id").append('<option>Select</option>');
              $.each(res,function(key,value){
                  $('.change_status_modal').find("#current_plan_id").append('<option value="'+key+'">'+value+'</option>');
              });
              //$('.change_status_modal.in').find('#current_plan_id').selectpicker('refresh');
          }else{
            $('.change_status_modal.in').find("#current_plan_id").empty();
          }
         }
      });
  }else{
    $('.change_status_modal.in').find("#current_plan_id").empty();     
  }    
}

function changeAddonPlandID(elem) {
    var ids = $(elem).val();
    if(ids){
      $.ajax({
         type:"GET",
         url:"{{url('user/get-serialnumber-list')}}?current_plan_type="+ids,
         success:function(res){               
          if(res){
              $('.change_status_modal.in').find("#serial_number_id").empty();
              $('.change_status_modal.in').find("#serial_number_id").append('<option>Select</option>');
              $.each(res,function(key,value){
                  $('.change_status_modal').find("#serial_number_id").append('<option value="'+key+'">'+value+'</option>');
              });
              //$('.change_status_modal.in').find('#current_plan_id').selectpicker('refresh');
          }else{
            $('.change_status_modal.in').find("#serial_number_id").empty();
          }
         }
      });
  }else{
    $('.change_status_modal.in').find("#serial_number_id").empty();     
  }    
}
//end Current Plan on load
</script>

<style>
.dataTables_filter
{
  display:none;
}
div.dt-buttons{float: right;}
</style>
@endpush