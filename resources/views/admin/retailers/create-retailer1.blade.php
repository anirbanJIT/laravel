@extends('layouts.admin')
@section('content')
<div class="breadcomb-area">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="breadcomb-list">
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<div class="breadcomb-wp">
								<div class="breadcomb-icon">
									<i class="notika-icon notika-windows"></i>
								</div>
								<div class="breadcomb-ctn">
									<h2>{{ trans('global.create') }} {{ trans('cruds.user.title_singular') }}</h2>
									<p>Here <span class="bread-ntd">you can create user</span></p>
								</div>
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-3">
							<div class="breadcomb-report">
								<a class="btn btn-success notika-btn-success waves-effect" href="{{ url()->previous() }}">
						            {{ trans('global.back_to_list') }}
						        </a>								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="form-element-area">
   <div class="container">
      <div class="row">
         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-tabs-int">
               <div class="widget-tabs-list" id="example-basic">
                  <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#general">General</a></li>
                    <!-- <li><a data-toggle="tab" href="#home">Contact Details</a></li>
                    <li><a data-toggle="tab" href="#menu1">Bank Details</a></li>
                    <li><a data-toggle="tab" href="#menu2">KYC Details</a></li> -->
                  </ul>
                  <div class="tab-content tab-custom-st">
                     <div id="general" class="tab-pane fade in active">
                        <div class="tab-ctn">
                           <form action="{{ route('admin.retailers.store') }}" method="POST" enctype="multipart/form-data" id="frm_retailer">
                           @csrf
                           @method('POST')
                           <input type="hidden"  name="referral_id" id="referral_id" value="{{Auth::user()->id}}">
                           <input type="hidden"  name="roles[]" id="roles" value="retailer">
                           <div class="row">
                              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                 <div class="form-group">
                                    <label>Shop Name</label>
                                    <div class="nk-int-st">
                                       <input type="text" id="shop_name" name="shop_name" value="{{ old('shop_name') }}" required class="form-control" placeholder="{{ trans('cruds.retailer.fields.shop_name') }}*" >	
                                    </div>
                                    @if ($errors->has('shop_name'))
                                    <span class="text-danger">{{ $errors->first('shop_name') }}</span>
                                    @endif
                                 </div>
                              </div>
                              
                              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                 <div class="form-group">
                                    <label>Customer Name</label>
                                    <div class="nk-int-st">
                                       <input type="text" id="name" name="name" value="{{ old('name') }}" required class="form-control" placeholder="{{ trans('cruds.user.fields.name') }}*" >	
                                    </div>
                                    @if ($errors->has('name'))
                                    <span class="text-danger">{{ $errors->first('name') }}</span>
                                    @endif
                                 </div>
                              </div>
                           </div>

                           <div class="row">
                              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                 <div class="form-group">
                                    <label>Email</label>
                                    <div class="nk-int-st">
                                       <input type="text" id="email" name="email" class="form-control" value="{{ old('email') }}" placeholder="{{ trans('cruds.user.fields.email') }}">
                                    </div>
                                    @if ($errors->has('email'))
                                    <span class="text-danger">{{ $errors->first('email') }}</span>
                                    @endif
                                 </div>
                              </div>
                              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                 <div class="form-group">
                                    <label>Password</label>
                                    <div class="nk-int-st">
                                      <input type="password" id="password" name="password" class="form-control" required placeholder="{{ trans('cruds.user.fields.password') }}">
                                    </div>
                                    @if ($errors->has('password'))
                                    <span class="text-danger">{{ $errors->first('password') }}</span>
                                    @endif
                                 </div>
                              </div>
                           </div>

                           <div class="row">

                              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                 <div class="form-group">
                                    <label>Mobile Number</label>
                                    <div class="nk-int-st">
                                       <input type="text" id="phone" name="phone" value="{{ old('phone') }}" required class="form-control" placeholder="Mobile Number*" >  
                                    </div>
                                    @if($errors->has('phone'))
                                    <em class="invalid-feedback">{{ $errors->first('phone') }}</em>
                                    @endif
                                 </div>
                              </div>

                              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                 <div class="form-group">
                                    <label>Other Phone Number</label>
                                    <div class="nk-int-st">
                                       <input type="text" id="other_phone" name="other_phone" value="{{ old('other_phone') }}" class="form-control" placeholder="Other phone Number" >	
                                    </div>
                                    @if($errors->has('other_phone'))
                                    <em class="invalid-feedback">{{ $errors->first('other_phone') }}</em>
                                    @endif
                                 </div>
                              </div>
                           </div>

                           <div class="row">

                              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                 <div class="form-group">
                                    <label>Address</label>
                                    <div class="nk-int-st">
                                       <input type="text" class="form-control" name="address" id="address" placeholder="Address"  value="" required>
                                    </div>
                                    @if ($errors->has('address'))
                                    <span class="text-danger">{{ $errors->first('address') }}</span>
                                    @endif
                                 </div>
                              </div>

                              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                 <div class="form-group">
                                    <label>Country</label>
                                    <select class="selectpicker" name="country" id="country" required>
                                       <option value="">Select Country</option>
                                       @foreach($countries as $key => $country)
                                       <option  value="{{$key}}"> {{$country}}</option>
                                       @endforeach
                                    </select>
                                    @if ($errors->has('country'))
                                    <span class="text-danger">{{ $errors->first('country') }}</span>
                                    @endif
                                 </div>
                              </div>

                           </div>

                           <div class="row">

                              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                 <div class="form-group">
                                    <label>State</label>
                                    <select class="selectpicker" name="state" id="state" required></select>
                                    @if ($errors->has('state'))
                                    <span class="text-danger">{{ $errors->first('state') }}</span>
                                    @endif            
                                 </div>
                              </div>

                              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                 <div class="form-group">
                                    <label>City</label>
                                    <div class="nk-int-st">
                                       <input type="text" class="form-control" name="city" id="city" placeholder="City" value="{{ old('city') }}" required>
                                    </div>
                                    @if ($errors->has('city'))
                                    <span class="text-danger">{{ $errors->first('city') }}</span>
                                    @endif
                                 </div>
                              </div>
                           </div>

                           <div class="row">
                              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                 <div class="form-group">
                                    <label>Pin Code</label>
                                    <div class="nk-int-st">
                                       <input type="text" class="form-control" name="pin_code" id="pin_code" placeholder="Pin Code"  value="{{ old('pin_code') }}" required>
                                    </div>
                                    @if ($errors->has('pin_code'))
                                    <span class="text-danger">{{ $errors->first('pin_code') }}</span>
                                    @endif
                                 </div>
                              </div>
                           </div>

                           <div class="row">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                	<input type="button" class="btn btn-success notika-btn-success waves-effect" value="{{ trans('global.save') }}" id="generalBtn">
                              </div>
                           </div>

                       		</form>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

			@endsection
			
			@push('scripts')

<script type="text/javascript">
	//On change 
    $('#country').change(function(){
    var countryID = $(this).val();   
	
    if(countryID){
        $.ajax({
           type:"GET",
           url:"{{url('user/get-state-list')}}?country_id="+countryID,
           success:function(res){               
            if(res){
                $("#state").empty();
                $("#state").append('<option>Select</option>');
                $.each(res,function(key,value){
                    $("#state").append('<option value="'+key+'">'+value+'</option>');
                });
               $('#state').selectpicker('refresh');
            }else{
               $("#state").empty();
            }
           }
        });
    }else{
        $("#state").empty();
       
    }      
   }); 
   
    $('#country_id').change(function(){
    var countryID = $(this).val();   
	
    if(countryID){
        $.ajax({
           type:"GET",
           url:"{{url('user/get-state-list')}}?country_id="+countryID,
           success:function(res){               
            if(res){
                $("#state_id").empty();
                $("#state_id").append('<option>Select</option>');
                $.each(res,function(key,value){
                    $("#state_id").append('<option value="'+key+'">'+value+'</option>');
                });
               $('#state_id').selectpicker('refresh');
            }else{
               $("#state_id").empty();
            }
           }
        });
    }else{
        $("#state_id").empty();
      
    }      
   }); 
   
   $('.datepicker').datepicker({
    format: 'yyyy-mm-dd',
	autoclose: true,
    
});
  
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
<script>
$(function(){		 
	//IFSC Code validation
	$(".ifsc_code").change(function () {      
		var inputvalues = $(this).val(); 
		var reg = "^[A-Za-z]{4}0[A-Z0-9a-z]{6}$";    
		if (inputvalues.match(reg)) {    
			return true;    
		}    
		else {    
			 $(".ifsc_code").val("");    
			alert("You entered invalid IFSC code");    
			//document.getElementById("txtifsc").focus();    
			return false;    
		}
	});      

});

$.validator.addMethod("countryIsIndia", function(value, element) {
  var email = $('#email').val();
	var country_id = $('#country :selected').val();
	console.log(country_id);
	if ( country_id == 101 && (email.length == 0 || email.length > 1) ) {
		return true;
	} else if ( country_id != 101 && email.length > 1) {
    return true;
  }else{
		return false;
	}

    }, "Please enter a email."
);

$('#generalBtn').click(function(e){
    e.preventDefault();
	$("#frm_retailer").validate({
	    rules: {
	        shop_name: {
	     		required: true,
	     	},
        name: {
          required: true,
        },
        phone: {
        required: true,
        minlength:10,
        maxlength:14,
        },
        email: {
          email: true,
          countryIsIndia: true
        },
        password:{
          required: true,
          minlength:6
        },
        address: {
            required: true,
            maxlength: 150
        },
        country: {
            required: true,
        },
        state: {
            required: true,
        },
  			city: {
                  required: true,
              },
  			pin_code: {
                  required: true,
  				maxlength:6,
  				rangelength: [6,6]
          },
      },
	   messages: { 
	        shop_name: {
	            required: "Please enter Shop Name"
	        },
	        name: {
	            required: "Please enter Name"
	        },
	        phone: {
				required: "Please enter Phone Number",
				minlength: "The phone number should be 10 digits",
				maxlength: "The phone number not more tahn 14 digits",
			},
	       	email: {
				email: "The Email should be valid",
				countryIsIndia: "Please enter Email",		
			},
      password: {
        required: "Please enter password",
        minlength: "The password more tahn 6 character",
      },
			address: {
                required: "Please enter address",
            },
            country: {
                required: "Please select country",
            },
            state: {
                required: "Please select state",
            },
			city: {
				required: "Please enter city",
			},
			pin_code: {
				required: "Please enter Pin Code",
				maxlength: "The Pin Code should be 6 digits",
				rangelength: "The Pin Code should be 6 digits",
			},

	    },
	});
	if($("#frm_retailer").valid()){
    	$("#frm_retailer").submit();
    }    
});
</script>
<script type="text/javascript">
function fileValidtion(e) {
 var attachment = document.getElementById("attachment");
  if(attachment){
	var filePath = attachment.value;   
  }
 
  
  if(filePath == ''){
	 
	  document.getElementById("demo").innerHTML = "Please upload file"; 
	  e.preventDefault() ;
      returnToPreviousPage();
  }
  var attachmentUp = document.getElementById("attachmentUp"); 
  var attachmentUp = attachmentUp.value;
  var attachmnetUpdate = document.getElementById("attachmnetUpdate");
  var attachmnetUpdate = attachmnetUpdate.value;
  
  if( attachmentUp == '' && attachmnetUpdate == ''){
	 document.getElementById("demo").innerHTML = "Please upload file";  
	 e.preventDefault() ;
      returnToPreviousPage();
  }
 
}
</script>
@endpush			










 