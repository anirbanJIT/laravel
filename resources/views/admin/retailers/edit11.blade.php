@extends('layouts.admin')
@section('content')


<div class="breadcomb-area">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="breadcomb-list">
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<div class="breadcomb-wp">
									<div class="breadcomb-icon">
										<i class="notika-icon notika-windows"></i>
									</div>
									<div class="breadcomb-ctn">
										<h2>{{ trans('global.edit') }} {{ trans('cruds.user.title_singular') }}</h2>
										<p>Here <span class="bread-ntd">you can update user</span></p>
									</div>
								</div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-3">
								<div class="breadcomb-report">
								
									 <a class="btn btn-success notika-btn-success waves-effect" href="{{ url()->previous() }}">
                {{ trans('global.back_to_list') }}
            </a>
									
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<div class="form-element-area">
  <div class="container">
  <form action="{{ route('admin.users.update', [$user->id]) }}" method="POST" enctype="multipart/form-data">
            @csrf
			@method('PUT')
   <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="form-element-list">
                        <div class="basic-tb-hd">
                             <!--<h2>{{ trans('global.edit') }} {{ trans('cruds.user.title_singular') }}</h2>
                           <p>Text Inputs with different sizes by height and column.</p>-->
                        </div>
                       
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group ic-cmp-int">
                                    <div class="form-ic-cmp">
                                        <i class="notika-icon notika-support"></i>
                                    </div>
                                    <div class="nk-int-st">
                                        <input type="text" id="name" name="name" value="{{ old('name', isset($user) ? $user->name : '') }}" required class="form-control" placeholder="{{ trans('cruds.user.fields.name') }}*" >
										
                                    </div>
									 @if($errors->has('name'))
                    <em class="invalid-feedback">
                        {{ $errors->first('name') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group ic-cmp-int">
                                    <div class="form-ic-cmp">
                                        <i class="notika-icon notika-mail"></i>
                                    </div>
                                    <div class="nk-int-st">
                                        <input type="text" id="email" name="email" class="form-control" value="{{ old('email', isset($user) ? $user->email : '') }}" required placeholder="{{ trans('cruds.user.fields.email') }}*">
                                   
									</div>
									 @if($errors->has('email'))
                    <em class="invalid-feedback">
                        {{ $errors->first('email') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.email_helper') }}
                </p>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group ic-cmp-int">
                                    <div class="form-ic-cmp">
                                        <i class="notika-icon notika-edit"></i>
                                    </div>
                                    <div class="nk-int-st">
                                        <input type="password" id="password" name="password" class="form-control" required placeholder="{{ trans('cruds.user.fields.password') }}">
									
                                    </div>
										@if($errors->has('password'))
                    <em class="invalid-feedback">
                        {{ $errors->first('password') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.password_helper') }}
                </p>
                                </div>
                            </div>
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="nk-int-mk sl-dp-mn mg-t-10">
                                    <h2>{{ trans('cruds.user.fields.roles') }}*</h2>
                                </div>
                                <div class="bootstrap-select">
                                    <select class="selectpicker" name="roles[]" id="roles" multiple required>
									
											 @foreach($roles as $id => $roles)
                        <option value="{{ $id }}" {{ (in_array($id, old('roles', [])) || isset($user) && $user->roles()->pluck('name', 'id')->contains($id)) ? 'selected' : '' }}>{{ $roles }}</option>
                    @endforeach
										</select>
                                </div>
								 @if($errors->has('roles'))
                    <em class="invalid-feedback">
                        {{ $errors->first('roles') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.roles_helper') }}
                </p>
								
                            </div>
							
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                           
                            <input type="submit" class="btn btn-success notika-btn-success waves-effect" value="{{ trans('global.save') }}" >
                        </div>
                        </div>
                     
                      
                    </div>
                </div>
            </div>
</form>			
			</div>
            </div>
			@endsection