@extends('layouts.admin')
@section('content')
<style type="text/css">
    .error{
        color:red;
    }
</style>
<div class="breadcomb-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="breadcomb-list">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="breadcomb-wp">
                                <div class="breadcomb-icon">
                                    <i class="notika-icon notika-windows"></i>
                                </div>
                                <div class="breadcomb-ctn">
                                    <h2>Welcome {{$user->name}}</h2>
                                    <p>Here <span class="bread-ntd">you can update {{$user->name}} Profile</span></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-3">
                            <div class="breadcomb-report">

                                <a class="btn btn-success notika-btn-success waves-effect" href="{{ url('admin/retailers')}}">
                                    {{ trans('global.back_to_list') }}
                                </a>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="form-element-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="widget-tabs-int">

                    <div class="widget-tabs-list" id="example-basic">
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#general">General</a></li>
                            <li><a data-toggle="tab" href="#menu2">KYC Details</a></li>
                            <li><a data-toggle="tab" href="#menu1">Bank Details</a></li>
                            <li><a data-toggle="tab" href="#tab_shopdetail">Shop Details</a></li>
                            <li><a data-toggle="tab" href="#tab_currentplan">Current Plan</a></li>
                        </ul>
                        <div class="tab-content tab-custom-st">
                            <!-- general start -->
                            <div id="general" class="tab-pane fade in active">
                                <form action="{{ route('admin.retailers.update', [$user->id]) }}" method="POST" enctype="multipart/form-data" id="frm_retailer">
                                    @csrf
                                    @method('PUT') 
                                    <div class="tab-ctn ">
                                        <div class="basic-tb-hd"></div>
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Shop Name</label>
                                                    <div class="nk-int-st">
                                                        <input type="text" id="shop_name" name="shop_name" value="{{ old('shop_name', isset($user) ? $user->shop_name : '') }}" required class="form-control" placeholder="{{ trans('cruds.retailer.fields.shop_name') }}*" >	
                                                    </div>
                                                    @if ($errors->has('shop_name'))
                                                    <span class="text-danger">{{ $errors->first('shop_name') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Customer Name</label>
                                                    <div class="nk-int-st">
                                                        <input type="text" id="name" name="name" value="{{ old('name', isset($user) ? $user->name : '') }}" required class="form-control" placeholder="{{ trans('cruds.user.fields.name') }}*" >
                                                    </div>
                                                    @if ($errors->has('name'))
                                                    <span class="text-danger">{{ $errors->first('name') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Email</label>
                                                    <div class="nk-int-st">
                                                        <input type="text" id="email" name="email" class="form-control" value="{{ old('email', isset($user) ? $user->email : '') }}" placeholder="{{ trans('cruds.user.fields.email') }}*" >
                                                    </div>
                                                    @if ($errors->has('email'))
                                                    <span class="text-danger">{{ $errors->first('email') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Phone</label>
                                                    <div class="nk-int-st">
                                                        <input type="text" id="phone" name="phone" value="{{ old('phone', isset($user) ? $user->phone : '') }}" required class="form-control" placeholder="Phone *" >
                                                    </div>
                                                    @if ($errors->has('phone'))
                                                    <span class="text-danger">{{ $errors->first('phone') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Other Phone Number</label>
                                                    <div class="nk-int-st">
                                                        <input type="text" id="other_phone" name="other_phone" value="{{ old('other_phone', isset($user) ? $user->other_phone : '') }}" class="form-control" placeholder="Other phone Number" >
                                                    </div>
                                                    @if($errors->has('other_phone'))
                                                    <em class="invalid-feedback">{{ $errors->first('other_phone') }}</em>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Address</label>
                                                    <div class="nk-int-st">
                                                        <input type="text" class="form-control" name="address" id="address" placeholder="Address"  value="{{$user->getContactDetail ? $user->getContactDetail->address : ''}}" required>
                                                    </div>
                                                    @if ($errors->has('address'))
                                                    <span class="text-danger">{{ $errors->first('address') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <div class="bootstrap-select fm-cmp-mg">
                                                    <label>Country</label>
                                                    <select class="selectpicker" name="country" id="country" required  >
                                                        <option value="">Select Country</option>
                                                        @foreach($countries as $key => $country)
                                                        @if(!empty($user->getContactDetail->country))
                                                        <option  value="{{$key}}" {{$user->getContactDetail->country == $key  ? 'selected' : ''}}> {{$country}}</option>
                                                        @else
                                                        <option  value="{{$key}}"> {{$country}}</option>
                                                        @endif									 
                                                        @endforeach
                                                    </select>
                                                    @if ($errors->has('country'))
                                                    <span class="text-danger">{{ $errors->first('country') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>State</label>
                                                    <select class="selectpicker" name="state" id="state" required></select>
                                                    @if ($errors->has('state'))
                                                    <span class="text-danger">{{ $errors->first('state') }}</span>
                                                    @endif            
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>City</label>
                                                    <div class="nk-int-st">
                                                        <input type="text" class="form-control" name="city" id="city" placeholder="City" value="{{$user->getContactDetail ? $user->getContactDetail->city : ''}}" required>
                                                    </div>
                                                    @if ($errors->has('city'))
                                                    <span class="text-danger">{{ $errors->first('city') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Pin Code</label>
                                                    <div class="nk-int-st">
                                                        <input type="text" class="form-control" name="pin_code" id="pin_code" placeholder="Pin Code"  value="{{$user->getContactDetail ? $user->getContactDetail->pin_code : ''}}" required>
                                                    </div>
                                                    @if ($errors->has('pin_code'))
                                                    <span class="text-danger">{{ $errors->first('pin_code') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-3">
                                                <input type="submit" class="btn btn-success notika-btn-success waves-effect" value="{{ trans('global.save') }}" > 
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- general end -->

                            <!-- KYC START -->
                            <div id="menu2" class="tab-pane fade">
                                <form action="{{ route('user.kyc-detail.update', [$user->id]) }}" method="POST" id="kyc_detail" enctype="multipart/form-data">
                                    @csrf
                                    <div class="tab-ctn">
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <input type="hidden" name="user_id" value="{{$user->id}}">
                                                <div class="form-group">
                                                    <div class="bootstrap-select fm-cmp-mg">
                                                        <label>KYC Type</label>
                                                        <select class="selectpicker" name="kyc_id" id="kyc_id">
                                                            <option value="">Select KYC Type</option>
                                                            @foreach($kycCategory as $kycCat)
                                                            @if(!empty($user->getKycDetail->kyc_id))
                                                            <option  value="{{$kycCat->id}}" {{$user->getKycDetail->kyc_id == $kycCat->id  ? 'selected' : ''}}> {{ $kycCat->name }}</option>
                                                            @else
                                                            <option  value="{{ $kycCat->id }}"> {{ $kycCat->name }}</option>
                                                            @endif
                                                            @endforeach
                                                        </select>
                                                        @if ($errors->has('kyc_id'))
                                                        <span class="text-danger">{{ $errors->first('kyc_id') }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>KYC Document</label>
                                                    <div class="nk-int-st">
                                                        <input type="text" class="form-control" name="kyc_document" id="kyc_document" placeholder="KYC Document" value="{{$user->getKycDetail ? $user->getKycDetail->kyc_document : ''}}">
                                                    </div>
                                                    @if ($errors->has('kyc_document'))
                                                    <span class="text-danger">{{ $errors->first('kyc_document') }}</span>
                                                    @endif
                                                </div>
                                            </div> 
                                        </div>
                                        <div class="row" id='adhar_card_fields' >
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <div class="form-group nk-datapk-ctm form-elet-mg" id="data_2">
                                                    <label>KYC Expiry Date</label>
                                                    <div class="input-group date nk-int-st">
                                                        <span class="input-group-addon"></span>
                                                        <input type="text" class="form-control datepicker" name="kyc_expiry_date" id="kyc_expiry_date" placeholder="KYC Expiry Date" value="{{$user->getKycDetail ? $user->getKycDetail->kyc_expiry_date : ''}}">
                                                    </div>
                                                    @if ($errors->has('kyc_expiry_date'))
                                                    <span class="text-danger">{{ $errors->first('kyc_expiry_date') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <div class="form-group nk-datapk-ctm form-elet-mg" id="data_2">
                                                    <label>Date Of Birth</label>
                                                    <div class="input-group date nk-int-st">
                                                        <span class="input-group-addon"></span>
                                                        <input type="text" class="form-control datepicker" name="dob" id="dob" placeholder="Date of Birth" value="{{ $user->getKycDetail ? $user->getKycDetail->dob : ''}}">
                                                    </div>
                                                    @if ($errors->has('dob'))
                                                    <span class="text-danger">{{ $errors->first('dob') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row"><div class="col-lg-12">&nbsp;</div></div>
                                        <div class="row" id='gstin_fields'>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>KYC Number</label>
                                                    <div class="nk-int-st">
                                                        <input type="text" class="form-control" name="kyc_number" id="kyc_number" placeholder="KYC Number" value="{{$user->getKycDetail ? $user->getKycDetail->kyc_number : ''}}">
                                                    </div>
                                                    @if ($errors->has('kyc_number'))
                                                    <span class="text-danger">{{ $errors->first('kyc_number') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Company Name</label>
                                                    <div class="nk-int-st">
                                                        <input type="text" class="form-control" name="company_name" id="company_name" placeholder="Company Name" value="{{$user->getKycDetail ? $user->getKycDetail->company_name : ''}}">
                                                    </div>
                                                    @if ($errors->has('company_name'))
                                                    <span class="text-danger">{{ $errors->first('company_name') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                @if(!empty($user->getKycDetail))
                                                <div class="form-group ic-cmp-int">
                                                    <div class="form-ic-cmp">
                                                        <i class="notika-icon notika-file"></i>
                                                    </div>
                                                    <div class="nk-int-st">		
                                                        <input type="file" accept="image/*" name="attachment" id = "attachmentUp"> Upload image
                                                    </div>
                                                    <input type="hidden" name="attachmnetUpdate" id = "attachmnetUpdate" value="{{$user->getKycDetail ? $user->getKycDetail->attachment : ''}}">
                                                    <p id="demo" style="color:red;"></p>
                                                </div>
                                                @else
                                                <div class="form-group ic-cmp-int">
                                                    <div class="form-ic-cmp">
                                                        <i class="notika-icon notika-file"></i>
                                                    </div>
                                                    <div class="nk-int-st">		
                                                        <input type="file" accept="image/*" name="attachment" id = "attachment"> Upload image
                                                    </div>
                                                    <p id="demo" style="color:red;"></p>
                                                </div>
                                                @endif	  
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Approve Or Unapprove KYC</label>
                                                    <div class="nk-int-st">
                                                        <div class="nk-toggle-switch" data-ts-color="green">
                                                            @if(!empty($user->getKycDetail->status))
                                                            <input name="status" id="ks{{$user->getKycDetail->id ? $user->getKycDetail->id : ''}}" class="kyc_status_change" type="checkbox" hidden="hidden" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Approve" data-off="Unapprove" {{ $user->getKycDetail->status ? 'checked' : '' }}>
                                                            <label for="ks{{$user->getKycDetail->id ? $user->getKycDetail->id : ''}}" class="ts-helper"></label>
                                                            @else
                                                            <input name="status" class="kyc_status_change" type="checkbox" hidden="hidden" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Approve" data-off="Unapprove" id="ks">
                                                            <label for="ks" class="ts-helper"></label>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="row"><div class="col-lg-12">&nbsp;</div></div>
                                        <div class="row" id='gstin_fields'>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <!--					    <div class="form-group">
                                                                                               <label>Machine Number</label>
                                                                                               <div class="nk-int-st">
                                                                                                  <input type="text" class="form-control" name="machine_number" id="machine_number" placeholder="Machine Number" value="{{$user->getKycDetail ? $user->getKycDetail->machine_number : ''}}" required>
                                                                                               </div>
                                                                                               @if ($errors->has('machine_number'))
                                                                                               <span class="text-danger">{{ $errors->first('machine_number') }}</span>
                                                                                               @endif
                                                                                            </div>-->
                                                <div class="form-group">
                                                    <div class="bootstrap-select fm-cmp-mg">
                                                        <label>Machine Number</label>
                                                        <select class="selectpicker" name="machine_number" id="machine_number" required>
                                                            <option value="">Select Machine Number</option>
                                                            @foreach($machine_numbers as $machine_number)
                                                            @if(!empty($user->getKycDetail->machine_number))
                                                            <option  value="{{$machine_number}}" {{$user->getKycDetail->machine_number == $machine_number  ? 'selected' : ''}}> {{ $machine_number }}</option>
                                                            @else
                                                            <option  value="{{ $machine_number }}"> {{ $machine_number }}</option>
                                                            @endif
                                                            @endforeach
                                                        </select>
                                                        @if ($errors->has('kyc_id'))
                                                        <span class="text-danger">{{ $errors->first('kyc_id') }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Machine Price</label>
                                                    <div class="nk-int-st">
                                                        <input type="text" class="form-control" name="machine_price" id="machine_price" placeholder="Machine Price" value="{{$user->getKycDetail ? $user->getKycDetail->machine_price : ''}}" required>
                                                    </div>
                                                    @if ($errors->has('machine_price'))
                                                    <span class="text-danger">{{ $errors->first('machine_price') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row"><div class="col-lg-12">&nbsp;</div></div>
                                        <div class="row" id='gstin_fields'>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <div class="form-group nk-datapk-ctm form-elet-mg" id="data_1">
                                                    <label>Machine Purchase Date</label>
                                                    <div class="input-group date nk-int-st">
                                                        <span class="input-group-addon"></span>
                                                        <input type="text" class="form-control" name="machine_purchase_date" id="machine_purchase_date" placeholder="Machine Purchase Date" value="{{$user->getKycDetail ? $user->getKycDetail->machine_purchase_date : ''}}" required>
                                                    </div>
                                                    @if ($errors->has('machine_purchase_date'))
                                                    <span class="text-danger">{{ $errors->first('machine_purchase_date') }}</span>
                                                    @endif
                                                </div>
                                            </div> 
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Notes</label>
                                                    <div class="nk-int-st">
                                                        <input type="text" class="form-control" name="notes" id="notes" placeholder="Notes" value="{{$user->getKycDetail ? $user->getKycDetail->notes : ''}}" required>
                                                    </div>
                                                    @if ($errors->has('notes'))
                                                    <span class="text-danger">{{ $errors->first('notes') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>


                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <input type="submit" class="btn btn-success notika-btn-success waves-effect" onclick = "fileValidtion(event);" value="{{ trans('global.save') }}" >
                                            </div>
                                        </div>

                                        @if(isset($user->getKycDetail->attachment))
                                        <div class="row mg-t-15">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-6">
                                                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-3">
                                                    <div class="typography-hd-cr-3">
                                                        <h3>KYC attachment</h3>
                                                        <p><a href="{{url(asset('images/kyc/'.$user->getKycDetail->attachment))}}" target="_blank" download><img src="{{asset('images/kyc/'.$user->getKycDetail->attachment)}}" style="max-width:40%"></a></p>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                    <p>Download KYC</p>
                                                    <div class="breadcomb-report" style="float:left;">
                                                        <a href="{{url(asset('images/kyc/'.$user->getKycDetail->attachment))}}" target="_blank" download>
                                                            <button data-toggle="tooltip" data-placement="left" title="" class="btn waves-effect" data-original-title="Download KYC"><i class="notika-icon notika-sent"></i></button>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endif
                                    </div>
                                </form>
                            </div>
                            <!-- KYC END-->

                            <!-- bank_form start -->
                            <div id="menu1" class="tab-pane fade">
                                <div class="tab-ctn">
                                    <form action="{{ route('user.bank-detail.update', [$user->id]) }}" method="POST" id="bank_form" enctype="multipart/form-data">
                                        @csrf
                                        <div class="row">
                                            <input type="hidden" name="user_id" value="{{$user->id}}"> 
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Bank Name</label>
                                                    <div class="nk-int-st">
                                                        <input type="text" class="form-control" name="bank_name" id="bank_name" placeholder="Bank Name" value="{{$user->getBankDetail ? $user->getBankDetail->bank_name : ''}}" required>
                                                    </div>
                                                    @if ($errors->has('bank_name'))
                                                    <span class="text-danger">{{ $errors->first('bank_name') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Account Number</label>
                                                    <div class="nk-int-st">
                                                        <input type="text" class="form-control" name="account_no" id="account_no" value="{{$user->getBankDetail ? $user->getBankDetail->account_no : ''}}" placeholder="Account Number" required>
                                                    </div>
                                                    @if ($errors->has('account_no'))
                                                    <span class="text-danger">{{ $errors->first('account_no') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Branch Name</label>
                                                    <div class="nk-int-st">
                                                        <input type="text" class="form-control" name="branch_name" id="branch_name" value="{{$user->getBankDetail ? $user->getBankDetail->branch_name : ''}}" placeholder="Branch Name" required>
                                                    </div>
                                                    @if ($errors->has('branch_name'))
                                                    <span class="text-danger">{{ $errors->first('branch_name') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Account Holder Name</label>
                                                    <div class="nk-int-st">
                                                        <input type="text" class="form-control" name="account_name" id="account_name" value="{{$user->getBankDetail ? $user->getBankDetail->account_name : ''}}" placeholder="Account Holder Name" required>
                                                    </div>
                                                    @if ($errors->has('account_name'))
                                                    <span class="text-danger">{{ $errors->first('account_name') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>IFSC Code</label>
                                                    <div class="nk-int-st">
                                                        <input type="text" class="form-control ifsc_code" name="ifsc_code" id="ifsc_code" maxlength="11" minlength="11" size="12" value="{{$user->getBankDetail ? $user->getBankDetail->ifsc_code : ''}}" placeholder="IFSC Code" required>
                                                    </div>
                                                    @if ($errors->has('ifsc_code'))
                                                    <span class="text-danger">{{ $errors->first('ifsc_code') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Account Type</label>
                                                    <select class="selectpicker" name="account_type" id="account_type" required>
                                                        <option value="">Select Account Type </option>
                                                        @if(!empty($user->getBankDetail->account_type))
                                                        <option value="Current" {{$user->getBankDetail->account_type == 'Current'  ? 'selected' : ''}}> Current </option>
                                                        <option value="Saving" {{$user->getBankDetail->account_type == 'Saving'  ? 'selected' : ''}}>Saving</option>
                                                        @else
                                                        <option value="Current"> Current </option>
                                                        <option value="Saving">Saving</option>
                                                        @endif
                                                    </select>
                                                    @if ($errors->has('account_type'))
                                                    <span class="text-danger">{{ $errors->first('account_type') }}</span>
                                                    @endif
                                                </div>
                                            </div>					
                                        </div>					
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Mobile Number</label>
                                                    <div class="nk-int-st">
                                                        <input type="text" class="form-control" name="mobile_number" id="mobile_number" value="{{$user->getBankDetail ? $user->getBankDetail->mobile_number : ''}}" placeholder="Mobile Number" required>
                                                    </div>
                                                    @if ($errors->has('mobile_number'))
                                                    <span class="text-danger">{{ $errors->first('mobile_number') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>


                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <input type="submit" class="btn btn-success notika-btn-success waves-effect" value="{{ trans('global.save') }}" >
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <!-- end bank_form -->

                            <!-- shop_detail start -->
                            <div id="tab_shopdetail" class="tab-pane fade">
                                <div class="tab-ctn">
                                    <form action="{{ route('user.shop-detail.update', [$user->id]) }}" method="POST" id="shop_detail" enctype="multipart/form-data">
                                        @csrf
                                        <div class="row">
                                            <input type="hidden" name="user_id" value="{{$user->id}}">
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Shop Name</label>
                                                    <div class="nk-int-st">
                                                        <input type="text" class="form-control" name="shop_name" id="shop_name" value="{{ $user->shop_name ?? ''}}" placeholder="Shop Name" required>
                                                    </div>
                                                    @if ($errors->has('shop_name'))
                                                    <span class="text-danger">{{ $errors->first('shop_name') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Address</label>
                                                    <div class="nk-int-st">
                                                        <input type="text" class="form-control" name="address" id="address" placeholder="Address"  value="{{$user->getShopDetail ? $user->getShopDetail->address : ''}}" required>
                                                    </div>
                                                    @if ($errors->has('address'))
                                                    <span class="text-danger">{{ $errors->first('address') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Pin Code</label>
                                                    <div class="nk-int-st">
                                                        <input type="text" class="form-control" name="pin_code" id="pin_code" placeholder="Pin Code"  value="{{$user->getShopDetail ? $user->getShopDetail->pin_code : ''}}" required>
                                                    </div>
                                                    @if ($errors->has('pin_code'))
                                                    <span class="text-danger">{{ $errors->first('pin_code') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <div class="bootstrap-select fm-cmp-mg">
                                                        <label>Country</label>
                                                        <select class="selectpicker" name="country" id="country" required>
                                                            <option value="">Select Country</option>
                                                            @foreach($countries as $key => $country)
                                                            @if(!empty($user->getShopDetail->country))
                                                            <option  value="{{$key}}" {{$user->getShopDetail->country == $key  ? 'selected' : ''}}> {{$country}}</option>
                                                            @else
                                                            <option  value="{{$key}}"> {{$country}}</option>
                                                            @endif									 
                                                            @endforeach
                                                        </select>
                                                        @if ($errors->has('country'))
                                                        <span class="text-danger">{{ $errors->first('country') }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>					
                                        </div>


                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Shop Photo</label>
                                                    <input type="file" accept="image/*" name="shop_photo" id="shop_photo">
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                @if(!empty($user->getShopDetail->shop_photo))
                                                <img src="{{ asset('images/shop/'.$user->getShopDetail->shop_photo) }}" width="60" height="60">
                                                @endif
                                            </div> 
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Bill Photo</label>
                                                    <input type="file" accept="image/*" name="machine_photo" id="machine_photo">
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                @if(!empty($user->getShopDetail->machine_photo))
                                                <img src="{{ asset('images/shop/'.$user->getShopDetail->machine_photo) }}" width="60" height="60">
                                                @endif
                                            </div> 
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">							
                                                <label>Image Gallery</label>
                                                <div class="input-group hdtuto control-group lst increment" >
                                                    <input type="file" name="shop_gallery[]" multiple class="myfrm form-control" placeholder="Upload image file ">
                                                    <div class="input-group-btn"> 
                                                        <button class="btn btn-success btn-success1" type="button"><i class="fldemo glyphicon glyphicon-plus"></i>Add</button>
                                                    </div>
                                                </div>
                                                <div class="clone hide">
                                                    <div class="hdtuto control-group lst input-group" style="margin-top:10px">
                                                        <input type="file" name="shop_gallery[]" multiple class="myfrm form-control">
                                                        <div class="input-group-btn"> 
                                                            <button class="btn btn-danger btn-danger1" type="button"><i class="fldemo glyphicon glyphicon-remove"></i> Remove</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">&nbsp;</div>
                                        </div>

                                        <div class="row">
                                            @if(isset($user->getShopDetail->shop_gallery))
                                            @php $myArray = json_decode($user->getShopDetail->shop_gallery);  @endphp

                                            @foreach($myArray as $key => $value)
                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                <div class="animation-single-int">
                                                    <div class="animation-img mg-b-15">
                                                        <img src="{{ asset('images/shop/'.$value) }}" class="animate-eleven" style="width:200px;height:200px;">
                                                    </div>
                                                    <div >
                                                        <a href="{{ url('admin/deleteShopGallery/?id='.$user->getShopDetail->id.'&shop_gallery='.$value) }}" class="sa-warning" >

                                                            <button class="btn btn-danger danger-icon-notika btn-reco-mg btn-button-mg waves-effect"> <i class="notika-icon notika-close"></i></button>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            @endforeach
                                            @endif
                                        </div>
                                        <div class="clear"></div>                             
                                        <div class="row"> <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">&nbsp;</div></div>
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <input type="submit" class="btn btn-success notika-btn-success waves-effect" value="{{ trans('global.save') }}" >
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <!-- end shopdetail -->	
                            <!-- current plan start -->
                            <div id="tab_currentplan" class="tab-pane fade">
                                <div class="tab-ctn">
                                    <form action="{{ route('admin.planChangeAdmin', [$user->id]) }}" method="POST" id="current_plan_form" enctype="multipart/form-data">
                                        @csrf
                                        <div class="row">
                                            <input type="hidden" name="user_id" value="{{$user->id}}">						
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <div class="bootstrap-select fm-cmp-mg">
                                                    <label>{{ trans('cruds.current_plan.fields.current_plan') }}</label>
                                                    <select class="selectpicker" name="current_plan" id="current_plan" required>
                                                        @foreach($plans as $id => $name)
                                                        <option value="{{$id}}" {{ (isset($user) && $user->current_plan ? $user->current_plan : old('current_plan')) == $id ? 'selected' : '' }}>{{ $name }}</option>
                                                        @endforeach
                                                    </select>
                                                    @if ($errors->has('current_plan'))
                                                    <span class="text-danger">{{ $errors->first('current_plan') }}</span>
                                                    @endif
                                                </div>
                                            </div> 
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <div class="bootstrap-select fm-cmp-mg">
                                                    <label>{{ trans('cruds.current_plan.fields.current_plan_id') }}</label>
                                                    <select class="selectpicker" name="current_plan_id" id="current_plan_id" required>
                                                        <option value="">Select Current Plan Id</option>
                                                        @foreach($currentPlans as $key => $value)
                                                        <option  value="{{$key}}" {{$user->current_plan_id == $key  ? 'selected' : ''}}> {{$value}}</option>		 
                                                        @endforeach
                                                    </select>
                                                    @if ($errors->has('current_plan_id'))
                                                    <span class="text-danger">{{ $errors->first('current_plan_id') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <br/>
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <div class="bootstrap-select fm-cmp-mg">
                                                    <label>Serial Key Plans</label>
                                                    <select class="selectpicker" name="addon_plan[]" id="addon_plan" multiple="" onchange="changeAddonPlandID(this.value)">
                                                        @foreach($addons as $id => $name)
                                                        <option value="{{$id}}">{{ $name }}</option>
                                                        @endforeach
                                                    </select>
                                                    @if ($errors->has('addon_plan'))
                                                    <span class="text-danger">{{ $errors->first('addon_plan') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <div class="bootstrap-select fm-cmp-mg">
                                                    <label>Serial Number</label>
                                                    <select class="selectpicker" name="serial_number_id" id="serial_number_id">
                                                        @if(!empty($userSerialNumber))
                                                        <option value="{{$userSerialNumber->id}}">{{$userSerialNumber->serial_number}}</option>
                                                        @endif
                                                    </select>
                                                    @if ($errors->has('serial_number_id'))
                                                    <span class="text-danger">{{ $errors->first('serial_number_id') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>	
                                        @if(!empty($allplans))
                                            @foreach($allplans as $k => $plan)
                                            @if ($plan['id'] != config('constants.plans.basic')) 
                                                @if(!empty($plan['rechargePlans']))
                                                    <br/>
                                                    <div class="row">
                                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                            <div class="bootstrap-select fm-cmp-mg">
                                                                <label>{{$plan['name']}} Addon Plans <span class="text-danger">{{($plan['expired'] == 1) ? "(Expired)" : ''}}</span></label>
                                                                <select class="selectpicker" name="recharge_plans[]" id="">
                                                                    <option value="" ></option>
                                                                    @foreach($plan['rechargePlans'] as $k => $rechargeplan)
                                                                    <option value="{{$rechargeplan['id']}}" {{$rechargeplan['id'] == $plan['activerechargePlan']  ? 'selected' : ''}}>{{ $rechargeplan['recharge_plan_name'] }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                            <div class="bootstrap-select fm-cmp-mg">
                                                                <label>{{$plan['name']}} Plan Serial Number</label>
                                                                <select class="selectpicker" name="recharge_plans_key[]" id="">
                                                                    <option value="" ></option>
                                                                    @foreach($plan['serialNumbers'] as $k => $serialNum)
                                                                    <option value="{{$serialNum['id']}}" {{$serialNum['id'] == $plan['activePlanKey']  ? 'selected' : ''}}>{{ $serialNum['serial_number'] }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                                @endif
                                            @endforeach
                                        @endif
                                        <div class="row mg-t-10">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <input type="submit" class="btn btn-success notika-btn-success waves-effect" value="{{ trans('global.save') }}" >
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <!-- end current plan -->			
                        </div>
                    </div>
                </div>
            </div>
        </div>                        
    </div>
</div>
@endsection

@push('scripts')

<script type="text/javascript">
$(document).ready(function() {
     // for kyc detail on load
      var countryId = $('#country_id :selected').val();
	  var country = $('#country :selected').val();
	    
      if(countryId){
        $.ajax({
           type:"GET",
           url:"{{url('user/get-state-list')}}?country_id="+countryId,
           success:function(res){               
            if(res){
                $("#state_id").empty();
                $("#state_id").append('<option value="">Select</option>');
				var s_id = ( {{$user->getContactDetail ? $user->getContactDetail->state : 'null'}} );
				
				if(s_id){
					 $.each(res,function(key,value){
					var newClass1 = (s_id == key) ? "selected" : " ";
					//alert(newClass);
                    $("#state_id").append('<option value="'+key+'" '+newClass1+'>'+value+'</option>');
                });
               $('#state_id').selectpicker('refresh');
				}
				
				
               
            }else{
               $("#state_id").empty();
            }
           }
        });
    }else{
        $("#state_id").empty(); 
    } 
//For contact Detail
    if(country){
        $.ajax({
           type:"GET",
           url:"{{url('user/get-state-list')}}?country_id="+country,
           success:function(res){               
            if(res){
                $("#state").empty();
                $("#state").append('<option value="">Select</option>');
				var s_id = ({{$user->getContactDetail ? $user->getContactDetail->state : 'null'}} );
				
				if(s_id){
					 $.each(res,function(key,value){
					var newClass = (s_id == key) ? "selected" : " ";
					//alert(newClass);
                    $("#state").append('<option value="'+key+'" '+newClass+'>'+value+'</option>');
                });
               $('#state').selectpicker('refresh');
				}
				
				
               
            }else{
               $("#state").empty();
            }
           }
        });
    }else{
        $("#state").empty();
		
       
    } 	
    });
	//On change 
    $('#country').change(function(){
    var countryID = $(this).val();   
	
    if(countryID){
        $.ajax({
           type:"GET",
           url:"{{url('user/get-state-list')}}?country_id="+countryID,
           success:function(res){               
            if(res){
                $("#state").empty();
                $("#state").append('<option>Select</option>');
                $.each(res,function(key,value){
                    $("#state").append('<option value="'+key+'">'+value+'</option>');
                });
               $('#state').selectpicker('refresh');
            }else{
               $("#state").empty();
            }
           }
        });
    }else{
        $("#state").empty();
       
    }      
   }); 
   
    $('#country_id').change(function(){
    var countryID = $(this).val();   
	
    if(countryID){
        $.ajax({
           type:"GET",
           url:"{{url('user/get-state-list')}}?country_id="+countryID,
           success:function(res){               
            if(res){
                $("#state_id").empty();
                $("#state_id").append('<option>Select</option>');
                $.each(res,function(key,value){
                    $("#state_id").append('<option value="'+key+'">'+value+'</option>');
                });
               $('#state_id').selectpicker('refresh');
            }else{
               $("#state_id").empty();
            }
           }
        });
    }else{
        $("#state_id").empty();
      
    }      
   }); 
// for Current Plan on load
    var current_plan = $('#current_plan :selected').val();
    if(current_plan){
        $.ajax({
           	type:"GET",
           	url:"{{url('user/get-plans-list')}}?current_plan_type="+current_plan,
           	success:function(res){
           		if(res){
	                $("#current_plan_id").empty();
	                $("#current_plan_id").append('<option value="">Select</option>');
					var s_id = ( {{$user->current_plan_id ? $user->current_plan_id : 'null'}} );				
					if(s_id){
						$.each(res,function(key,value){
							var newClass1 = (s_id == key) ? "selected" : " ";
		                    $("#current_plan_id").append('<option value="'+key+'" '+newClass1+'>'+value+'</option>');
	                	});
	               		$('#current_plan_id').selectpicker('refresh');
					}
            	}else{
            		$("#current_plan_id").empty();
            	}
        	}
    	});
    }else{
        $("#current_plan_id").empty(); 
    } 
//On change 
    $('#current_plan').change(function(){
	    var current_plan = $(this).val();
	    if(current_plan){
	        $.ajax({
	           type:"GET",
	           url:"{{url('user/get-plans-list')}}?current_plan_type="+current_plan,
	           success:function(res){               
	            if(res){
	                $("#current_plan_id").empty();
	                $("#current_plan_id").append('<option>Select</option>');
	                $.each(res,function(key,value){
	                    $("#current_plan_id").append('<option value="'+key+'">'+value+'</option>');
	                });
	               $('#current_plan_id').selectpicker('refresh');
	            }else{
	               $("#current_plan_id").empty();
	            }
	           }
	        });
	    }else{
	        $("#current_plan_id").empty();
	       
	    }      
   	});
//end Current Plan on load

$('#kyc_id').change(function(e){
	var kyc_document = $("#kyc_id option:selected").text();
	$("#kyc_document").val(kyc_document);
});
   
$('.datepicker').datepicker({
    format: 'yyyy-mm-dd',
	autoclose: true,
});
  
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
<script type="text/javascript">

$.validator.addMethod("countryIsIndia", function(value, element) {
  var email = $('#email').val();
	var country_id = $('#country :selected').val();
	console.log(country_id);
	if ( country_id == 101 && (email.length == 0 || email.length > 1) ) {
		return true;
	} else if ( country_id != 101 && email.length > 1) {
    return true;
  }else{
		return false;
	}

    }, "Please enter a email."
);

$("#frm_retailer").validate({
    rules: {
        shop_name: {
     		required: true,
     	},
	    name: {
	      required: true,
	    },
	    phone: {
		    required: true,
		    minlength:10,
		    maxlength:14,
	    },
	    email: {
			email: true,
			countryIsIndia: true
	    },
	    address: {
	        required: true,
	        maxlength: 150
	    },
	    country: {
	        required: true,
	    },
	    state: {
	        required: true,
	    },
		city: {
        	required: true,
      	},
		pin_code: {
			required: true,
			maxlength:6,
			rangelength: [6,6]
      	},
  	},
   	messages: { 
        shop_name: {
            required: "Please enter Shop Name"
        },
        name: {
            required: "Please enter Name"
        },
        phone: {
			required: "Please enter Phone Number",
			minlength: "The phone number should be 10 digits",
			maxlength: "The phone number not more tahn 14 digits",
		},
       	email: {
			email: "The Email should be valid",
			countryIsIndia: "Please enter Email",		
		},
		address: {
            required: "Please enter address",
        },
        country: {
            required: "Please select country",
        },
        state: {
            required: "Please select state",
        },
		city: {
			required: "Please enter city",
		},
		pin_code: {
			required: "Please enter Pin Code",
			maxlength: "The Pin Code should be 6 digits",
			rangelength: "The Pin Code should be 6 digits",
		},

    },
});
</script>
<script>
      $(function(){
		 
		//IFSC Code validation
		$(".ifsc_code").change(function () {      
		var inputvalues = $(this).val(); 
  	
		  var reg = "^[A-Za-z]{4}0[A-Z0-9a-z]{6}$";    
						if (inputvalues.match(reg)) {    
							return true;    
						}    
						else {    
							 $(".ifsc_code").val("");    
							alert("You entered invalid IFSC code");    
							//document.getElementById("txtifsc").focus();    
							return false;    
						}    
		});      
		
	  });

    if ($("#contact_form").length > 0) {
        $("#contact_form").validate({
 
            rules: {
                address: {
                    required: true,
                    maxlength: 150
                },
 
                country: {
                    required: true,
                },
 
                state: {
                    required: true,                   
                },
				city: {
                    required: true,
                },
				pin_code: {
                    required: true,
					maxlength:6,
					rangelength: [6,6]
                },
            },
            messages: {
 
                address: {
                    required: "Please enter address",
                },
                country: {
                    required: "Please select country",
                },
                state: {
                    required: "Please select state",
                },
				city: {
					required: "Please enter city",
				},
				pin_code: {
					required: "Please enter Pin Code",
					maxlength: "The Pin Code should be 6 digits",
					rangelength: "The Pin Code should be 6 digits",
				},
 
            },
        })
    } 
	  //Bank detail for validation
	if ($("#bank_form").length > 0) {
        $("#bank_form").validate({
 
            rules: {
                bank_name: {
                    required: true,
                },
 
                account_no: {
                    required: true,
                },
 
                branch_name: {
                    required: true,                   
                },
				account_name: {
                    required: true,
                },
				ifsc_code: {
                    required: true,
					
					maxlength:11,
					
                },
				
            },
            messages: {
 
                bank_name: {
                    required: "Please Enter Bank Name",
                },
                account_no: {
                    required: "Please Enter Account Number",
                },
                branch_name: {
                    required: "Please Enter Branch Name",
                },
				account_name: {
					required: "Please Enter Account Holder Name",
				},
				ifsc_code: {
					required: "Please Enter IFSC Code",
					minlength: "The IFSC Code should be 11 digits",
					maxlength: "The IFSC Code not more than 12 digits",
					
				},
				
				
 
            },
        })
    } 
	//KYC detail validation
	if ($("#kyc_detail").length > 0) {
        $("#kyc_detail").validate({ 
            rules: {
                kyc_id: {
                    required: true,
                }, 
                kyc_number: {
                    required: true,
                },
            },
            messages: { 
                kyc_id: {
                    required: "Please Select KYC Category",
                },
                kyc_number: {
                    required: "Please Enter KYC Number",
                }				
            },
        })
    } 
   
    //current_plan_form validation
	if ($("#current_plan_form").length > 0) {
        $("#current_plan_form").validate({ 
            rules: {
                current_plan_id: {
                    required: true,
                }
            },
            messages: { 
                current_plan_id: {
                    required: "Please Select Plan",
                }				
            },
        })
    } 
 </script>
<script type="text/javascript">
function fileValidtion(e) {
 var attachment = document.getElementById("attachment");
  if(attachment){
	var filePath = attachment.value;   
  }
 
  
  if(filePath == ''){
	 
	  document.getElementById("demo").innerHTML = "Please upload file"; 
	  e.preventDefault() ;
      returnToPreviousPage();
  }
  var attachmentUp = document.getElementById("attachmentUp"); 
  var attachmentUp = attachmentUp.value;
  var attachmnetUpdate = document.getElementById("attachmnetUpdate");
  var attachmnetUpdate = attachmnetUpdate.value;
  
  if( attachmentUp == '' && attachmnetUpdate == ''){
	 document.getElementById("demo").innerHTML = "Please upload file";  
	 e.preventDefault() ;
      returnToPreviousPage();
  }
 
}


$(document).ready(function() {
  $(".btn-success1").click(function(){ 
      var lsthmtl = $(".clone").html();
      $(".increment").after(lsthmtl);
  });
  $("body").on("click",".btn-danger1",function(){ 
      $(this).parents(".hdtuto").remove();
  });
});
function changeAddonPlandID(id) {
    var ids = $("#addon_plan").val();
    if(ids){
      $.ajax({
         type:"GET",
         url:"{{url('user/get-serialnumber-list')}}?current_plan_type="+ids,
         success:function(res){               
          if(res){
              $('#current_plan_form').find("#serial_number_id").empty();
              $('#current_plan_form').find("#serial_number_id").append('<option></option>');
              $.each(res,function(key,value){
                  $('#current_plan_form').find("#serial_number_id").append('<option value="'+key+'">'+value+'</option>');
              });
              $('#current_plan_form').find('#serial_number_id').selectpicker('refresh');
          }else{
            $('#current_plan_form').find("#serial_number_id").empty();
          }
         }
      });
  } else {
    $('#current_plan_form').find("#serial_number_id").empty();  
    $('#current_plan_form').find('#serial_number_id').selectpicker('refresh');
    
  }    
}
</script>
@endpush			