@extends('layouts.admin')
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
@if(session('payoutMsg'))
<div class="container">
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="row mb-2">
				<div class="col-lg-12">
					<div class="alert alert-warning" role="alert">
						<h4>Here show payout status for following retailers : </h4>
						<table  class="table" >
							<thead>
								<tr>
									<th>User Id</th>
									<th>User Name</th>
									<th>Meassage</th>
								</tr>
							</thead>
							@foreach(Session::get('payoutMsg') as $payMsg )
							<tr>
								@foreach($payMsg as $key => $val) 
								<td>{{$val}}</td>
								@endforeach
							</tr>
							@endforeach
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endif
<div class="breadcomb-area">
	<div class="container">
		<div class="row">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="breadcomb-list">
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="breadcomb-wp">
									<div class="breadcomb-icon">
										<i class="notika-icon notika-windows"></i>
									</div>
									<div class="breadcomb-ctn">
										<h2>Customer Recharge Commission Payout</h2>
										<p>Show <span class="bread-ntd">all list</span></p>
									</div>
								</div>
							</div>
						</div>
						<div class="row mt_10">
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
					           	<div class="form-example-int form-example-st">
					              	<div class="form-group">
					                 	<div class="nk-int-st">
					                    	<select class="selectpicker select-filter" name="user_id" id="user_id" data-live-search="true">
											<option value="">Id</option>
											@foreach($retailerUsers as $key => $retailerUser) 
												<option value="{{ $retailerUser->id ?? '' }}" {{ isset($user_id) && $user_id == $retailerUser->id ? 'selected' : ''}}>{{ $retailerUser->name ?? '' }} ({{ $retailerUser->id ?? '' }})</option>
											@endforeach
											</select>
					                 	</div>
					              	</div>
					           	</div>
					        </div>

							<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
								<select class="selectpicker select-filter" name="status" id="status" data-live-search="true">
								    <option value="">Status</option>
								    <option value="1">Paid</option>
								    <option value="0">Payout</option>
								</select>
							</div>
							<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
								<div class="form-group nk-datapk-ctm form-elet-mg" id="data_1" >
									<div class="input-group date nk-int-st">
										<span class="input-group-addon"></span>
										<input type="text" class="form-control input-filter" name="from_date" id="from_date"  placeholder="From Date" >
									</div>
								</div>
							</div>
							<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
								<div class="form-group nk-datapk-ctm form-elet-mg" id="data_1">
									<div class="input-group date nk-int-st">
										<span class="input-group-addon"></span>
										<input type="text" class="form-control input-filter" name="to_date" id="to_date" placeholder="To Date">
									</div>
								</div>
							</div>
							<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
								<div class="form-group ic-cmp-int">
									<div class="form-ic-cmp">
										<i class="notika-icon notika-credit-card"></i>
									</div>
									<div class="nk-int-st">
										<input type="text" class="form-control input-filter" name="invoice_no" id="invoice_no" placeholder="Entet Invoice No.">
									</div>
								</div>
							</div>							
							<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
				                <input type="submit" class="btn btn-success notika-btn-success waves-effect" id="btnFiterSubmitSearch" value="Search" >
				                <input type="button" class="btn btn-success notika-btn-success waves-effect" id="btnFiterSubmitReset" value="Reset">
			              	</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Data Table area Start-->
<form action="{{ route('admin.commission-payout') }}" method="POST" enctype="multipart/form-data">
	@csrf
	@method('PUT')
  <input type="hidden" name="payout_total_amount" value="no">
	<div class="data-table-area" id="users-table">
	  	<div class="container">
	    	<div class="row">
			    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			        <div class="data-table-list">
						<div class="basic-tb-hd">
							<input type="submit" class="btn btn-success notika-btn-success waves-effect" id="btnPayNow" value="Pay Now" >
						</div>
						<div class="table-responsive">
			            	<table id="data-table-retailers"  class="table table-striped datatable datatable-customerRechargeCommissionPayout" style="width: 100%;">
			              		<thead>
									<tr>
										<th><input type="checkbox" class="btn btn-success notika-btn-success waves-effect" id="check-all" value="check-all" ></th>
										<th>Invoice No</th>
										<th>Ref. By</th>
										<th>Ref. To</th>
										<th>Commission %</th>
										<th>Plan Amount</th>
										<th>Commission Credit</th>
										<th>Invoice Commission Amt ({{env('CURRENCY_SYMBOL')}})</th>
										<th>Commission Amt ({{env('CURRENCY_SYMBOL')}})</th>
										<th>Date</th>
										<th>Status</th>
									</tr>
								</thead>
								<tfoot>
									<tr>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
									</tr>
								</tfoot>
							</table>
	          			</div>
	        		</div>
	      		</div>
    		</div>
  		</div>
	</div>
</form>

@endsection
@push('scripts')
<script type="text/javascript">
$(document).ready(function (){
    /* @author Naren Thaker @narenthaker.com 101220201500 */
    window._token = $('meta[name="csrf-token"]').attr('content');

    let excelButtonTrans = '{{ trans('global.datatables.export') }} {{ trans('global.datatables.excel') }}';

    $.extend(true, $.fn.dataTable.Buttons.defaults.dom.button, { className: 'btn' })

    $.extend(true, $.fn.dataTable.defaults, {
        responsive:!1,
        lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
        pageLength:10,
        processing: true,
        serverSide: true,
        buttons: [],
        dom: 'lBfrtip<"actions">',
    });

    let table = $('#data-table-retailers').DataTable({
      	processing: true,
        serverSide: true,
        ajax: {
	        headers: {'x-csrf-token': _token},
	        url: "{{ url('admin/recharge-commission-payout-report') }}",
	        type: 'GET',
	        data: function (d) {
				d.user_id = $('#user_id').val();
				d.from_date = $('#from_date').val();
				d.to_date = $('#to_date').val();
				d.invoice_no = $('#invoice_no').val();
				d.status = $('#status').val();
	        }
      	},
      	"fnDrawCallback": function() {
	        var api = this.api()
	        var json = api.ajax.json();
	        $(api.column(0).footer()).html('Total');
			$(api.column(5).footer()).html(json.total_invoice_credit);
			$(api.column(6).footer()).html(json.total_commission_credit);
			$(api.column(7).footer()).html(json.total_invoice_amt);
			$(api.column(8).footer()).html(json.total_commission_amt);
	    },
      	columns: [
			{ data: "checkbox", orderable:false, searchable:false},
			{ data: 'invoice_no', name: 'invoice_no' },
			{ data: 'user_id', name: 'user_id' },
			{ data: 'sender_id', name: 'sender_id' },
			{ data: 'commission_per', name: 'commission_per' },
			{ data: 'invoice_credit', name: 'invoice_credit' },
			{ data: 'commission_credit', name: 'commission_credit' },
			{ data: 'invoice_amt', name: 'invoice_amt', class : 'd-none' },
			{ data: 'commission_amt', name: 'commission_amt', class : 'd-none' },
	        {
	            data: 'created_at',
	            type: 'num',
	            render: {
	              _: 'display',
	              sort: 'timestamp'
	            }
	        },
	        { data: 'status', name: 'status' }
     	],
      	buttons: [
	      	{
				extend: 'excel',
				className: 'btn btn-success notika-btn-success waves-effect',
				text: excelButtonTrans,
				exportOptions: {
                    columns: [ 1, 2, 3, 4, 5, 6, 7, 8 ]
                },
				action: function(e, dt, node, config) {
					var dtButton= this;
					var currentPageLen = dt.page.len();
					var currentPage = dt.page.info().page; 
					dt.one( 'draw', function () {
						$.fn.DataTable.ext.buttons.excelHtml5.action.call(dtButton, e, dt, node, config);  
						setTimeout(function() {
							dt.page.len(currentPageLen).draw();
							dt.page(currentPage).draw('page');
						}), 500;
					});
					dt.page.len(-1).draw();
				}
	        },
	    ],
    });
});
$('#check-all').on('click', function() {
	if(this.checked){
		$('.pdr_checkbox').prop('checked', true);
	}else{
		$('.pdr_checkbox').prop('checked', false);
	}
});

$('#btnFiterSubmitSearch').click(function(e){
 	$('#data-table-retailers').DataTable().draw(true);
});

$('#btnFiterSubmitReset').click(function(){
	$(".selectpicker").val('').trigger('change')
	$('.input-filter').val('');
    $('#data-table-retailers').DataTable().draw(true);
});

$('.select-filter').on('change', function (e) {
	$('#data-table-retailers').DataTable().draw(true);
});
$('.input-filter').on('keyup change', function (e) {
	$('#data-table-retailers').DataTable().draw(true);
});
</script>
<style>
.dataTables_filter
{
  display:none;
}
div.dt-buttons{float: right;}
</style>
@endpush