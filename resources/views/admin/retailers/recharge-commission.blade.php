@extends('layouts.admin')
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
<div class="breadcomb-area">
  <div class="container">
    <div class="row">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="breadcomb-list">
            <div class="row">
              <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                <div class="breadcomb-wp">
                  <div class="breadcomb-icon">
                    <i class="notika-icon notika-windows"></i>
                  </div>
                  <div class="breadcomb-ctn">
                    <h2>
                      <!-- Users Recharge Commission -->Customer Recharge Commission
                    </h2>
                    <p>Show <span class="bread-ntd">all list</span></p>
                  </div>
                </div>
              </div>
              <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <input type="text" class="form-control" name="user_id" id="user_id" placeholder="Entet User Id/Email">
              </div>
              <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                <input type="submit" class="btn btn-success notika-btn-success waves-effect" id="btnFiterSubmitSearch" value="Search" >
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Data Table area Start-->
<div class="data-table-area" id="users-table">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="data-table-list">
          <div class="table-responsive">
            <table class="table table-striped datatable datatable-rechargeCommissionpayout" id="data-table-basic">
              <thead>
                <tr>
                  <th>Invoice No</th>
                  <th>Ref. By</th>
                  <th>Ref. To</th>
                  <th>Commission %</th>
                  <th>Plan Amount</th>
                  <th>Commission Credit</th>
                  <th style="display: none;">Invoice Commission Amt ({{env('CURRENCY_SYMBOL')}})</th>
                  <th style="display: none;">Commission Amt ({{env('CURRENCY_SYMBOL')}})</th>
                  <th>Date</th>
                  <th>Status</th>
                </tr>
              </thead>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
@push('scripts')
<script type="text/javascript">

$(document).ready(function (){
    /* @author Naren Thaker @narenthaker.com 101220201500 */
    window._token = $('meta[name="csrf-token"]').attr('content');

    let excelButtonTrans = '{{ trans('global.datatables.export') }} {{ trans('global.datatables.excel') }}';

    $.extend(true, $.fn.dataTable.Buttons.defaults.dom.button, { className: 'btn' })

    $.extend(true, $.fn.dataTable.defaults, {
        responsive:!1,
        lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
        pageLength:10,
        processing: true,
        serverSide: true,
        buttons: [],
        dom: 'lBfrtip<"actions">',
    });

    var table = $('.datatable-rechargeCommissionpayout').DataTable({
      processing: true,
      serverSide: true,
      ajax: {
        headers: {'x-csrf-token': _token},
        url: "{{ url('admin/recharge-commission-report') }}",
        type: 'GET',
        data: function (d) {
          d.user_id = $('#user_id').val();
        }
      },
      columns: [
        { data: 'invoice_no', name: 'invoice_no' },
        { data: 'user_id', name: 'user_id' },
        { data: 'sender_id', name: 'sender_id' },
        { data: 'commission_per', name: 'commission_per' },
        { data: 'invoice_credit', name: 'invoice_credit' },
        { data: 'commission_credit', name: 'commission_credit' },
        { data: 'invoice_amt', name: 'invoice_amt', class : 'd-none' },
        { data: 'commission_amt', name: 'commission_amt' class : 'd-none'  },
        {
          data: 'created_at',
          type: 'num',
          render: {
            _: 'display',
            sort: 'timestamp'
          }
        },
        { data: 'status', name: 'status' }
      ],
      buttons: [
      {
          extend: 'excel',
          className: 'btn btn-success notika-btn-success waves-effect',
          text: excelButtonTrans,
          exportOptions: {
            columns: ':visible'
          },
          action: function(e, dt, node, config) {
            var dtButton= this;
            var currentPageLen = dt.page.len();
            var currentPage = dt.page.info().page; 
            dt.one( 'draw', function () {
                $.fn.DataTable.ext.buttons.excelHtml5.action.call(dtButton, e, dt, node, config);  
                setTimeout(function() {
                    dt.page.len(currentPageLen).draw();
                    dt.page(currentPage).draw('page');
                }), 500;
            });
            dt.page.len(-1).draw();
          }
        },
      ],
    });
});

$('#btnFiterSubmitSearch').click(function(){
  $('.datatable-rechargeCommissionpayout').DataTable().draw(true);
});
$('#btnFiterSubmitReset').click(function(){
  $("#user_id").val('');
  $("#user_id").selectpicker("refresh");
  $('.datatable-rechargeCommissionpayout').DataTable().draw(true);
});
</script>
<style>
.dataTables_filter
{
  display:none;
}
div.dt-buttons{float: right;}
</style>
@endpush