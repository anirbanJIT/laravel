@extends('layouts.admin')
@section('content')


	<div class="breadcomb-area">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="breadcomb-list">
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<div class="breadcomb-wp">
									<div class="breadcomb-icon">
										<i class="notika-icon notika-windows"></i>
									</div>
									<div class="breadcomb-ctn">
										<h2>{{ $retailer->name ? $retailer->name : '' }} Details</h2>
										
									</div>
								</div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-3">
								<div class="breadcomb-report">
								
									 <a class="btn btn-success notika-btn-success waves-effect" href="{{ url()->previous() }}">
                {{ trans('global.back_to_list') }}
            </a>
									
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	

   <div class="tabs-info-area">
 <div class="container">
   <div class="row">
     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	  <div class="widget-tabs-int">
			
	   <div class="widget-tabs-list">
			<ul class="nav nav-tabs">
			    <li class="active"><a data-toggle="tab" href="#general">General</a></li>
				<li><a data-toggle="tab" href="#home">Contact Details</a></li>
				<li><a data-toggle="tab" href="#menu1">Bank Details</a></li>
				<li><a data-toggle="tab" href="#menu2">KYC Details</a></li>
			</ul>
		<div class="tab-content tab-custom-st">
		<div id="general" class="tab-pane fade in active">
		<div class="tab-ctn">
		  <div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			    <div class="table-responsive">
					<table  class="table table-striped">
						<thead>
							<tr>
										  
							<th>Name</th>
							<th>Email</th>
							<th>Phone Number</th>

							</tr>
						</thead>
				<tbody>
							
			     	<tr data-entry-id="{{ $retailer ->id }}">
											  
						<td> {{ $retailer->name ?? '' }} </td>
						<td> {{ $retailer->email ?? '' }} </td> 
					   <td>{{ $retailer->phone ?? '' }}</td>
				 
					</tr>
						  
				 </tbody>
					   
					</table>
				</div>
			</div>                           
				   
					
			</div>
        </div>
		</div>
			
		
		<div id="home" class="tab-pane fade">
		<div class="tab-ctn">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			    <div class="table-responsive">
					<table  class="table table-striped">
						<thead>
							<tr>			  
							<th>Address</th>
							<th>Country</th>
							<th>State</th>
							</tr>
						</thead>
				<tbody>
							
			     	<tr data-entry-id="{{ $retailer ->id }}">
											  
						<td> {{ $retailer->getContactDetail->address ?? '' }} </td>
						@foreach($countries as $country)
						<td> {{ $retailer->getContactDetail->country == $country->id ? $country->name : '' }} </td> 
						@endforeach
						@foreach($states as $state)
     					   <td>{{ $retailer->getContactDetail->state == $state->id ? $state->name : '' }}</td>
	                    @endforeach 			 
					</tr>
						  
				 </tbody>
					   
					</table>
				</div>
			</div> 		
		</div>
		</div>
			<div id="menu1" class="tab-pane fade">
				<div class="tab-ctn">
			<form action="{{ route('admin.users.store') }}" method="POST" enctype="multipart/form-data">
              @csrf
				  <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <div class="nk-int-st">
                                        <input type="text" class="form-control" name="bankName" id="bankName" placeholder="Bank Name">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <div class="nk-int-st">
                                        <input type="text" class="form-control" name="accountNo" id="accountNo" placeholder="Account Number">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <div class="nk-int-st">
                                        <input type="text" class="form-control" name="branchName" id="branchName" placeholder="Branch Name">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <div class="nk-int-st">
                                        <input type="text" class="form-control" name="accountHolderName" id="accountHolderName" placeholder="Account Holder Name">
                                    </div>
                                </div>
                            </div>
							 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <div class="nk-int-st">
                                        <input type="text" class="form-control" name="ifscCode" id="ifscCode" placeholder="IFSC Code">
                                    </div>
                                </div>
                            </div>
							 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <div class="nk-int-st">
                                        <input type="text" class="form-control" name="accountType" id="accountType" placeholder="Account Type">
                                    </div>
                                </div>
                            </div>
                        </div>	
                     <div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                           
                            <input type="submit" class="btn btn-success notika-btn-success waves-effect" value="{{ trans('global.save') }}" >
                        </div>
						</div>	
                 </form>						
				</div>
			</div>
			<div id="menu2" class="tab-pane fade">
				<div class="tab-ctn">
				  <div class="row">
				            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="bootstrap-select fm-cmp-mg">
                                    <select class="selectpicker" name="brandType">
											<option value="">Select KYC Type</option>
											<option value="">fdgfdg</option>
											<option value="">fdgfdg</option>
											<option value="">fdgfdg</option>
											<option value="">fdgfdg</option>
											<option value="">fdgfdg</option>
											
										</select>
								  
										<span class="text-danger"></span>
									
                                </div>
                               </div>                           
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <div class="nk-int-st">
                                        <input type="text" class="form-control" name="kycNo" id="kycNo" placeholder="KYC Number">
                                    </div>
                                </div>
                            </div>
							
					</div>
					<div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <div class="nk-int-st">
                                        <input type="text" class="form-control" name="customerName" id="customerName" placeholder="Customer Name">
                                    </div>
                                </div>
                            </div>
							 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
								     <label>Relation</label>
                                     <div class="fm-checkbox">
                                    <label><input type="radio" name="relation"> <i></i>Father </label>
									 <label><input type="radio" name="relation"> <i></i>Mother </label>
									  <label><input type="radio" name="relation"> <i></i>Husband </label>
									   <label><input type="radio" name="relation"> <i></i>Wife </label>
                                </div>
                                </div>
                            </div>
					</div>
					<div class="row">		
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <div class="nk-int-st">
                                        <input type="text" class="form-control" name="phone" id="phone" placeholder="Phone Number">
                                    </div>
                                </div>
                            </div>
							 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <div class="nk-int-st">
                                        <input type="text" class="form-control" name="email" id="email" placeholder="Email">
                                    </div>
                                </div>
                            </div>
					</div>
					<div class="row">		
							 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <div class="nk-int-st">
                                        <input type="text" class="form-control" name="address" id="address" placeholder="Address">
                                    </div>
                                </div>
                            </div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <div class="nk-int-st">
                                        <input type="text" class="form-control" name="pinCode" id="pinCode" placeholder="Pin Code">
                                    </div>
                                </div>
                            </div>
                     </div>	
				</div>
			</div>
	   </div>
	 </div>
	</div>
    </div>
  </div>                        
 </div>
</div>
@endsection