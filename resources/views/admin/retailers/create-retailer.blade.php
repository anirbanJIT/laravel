<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
      <meta name="description" content="POS - Bootstrap Admin Template">
      <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
      <meta name="author" content="Dreamguys - Bootstrap Admin Template">
      <meta name="robots" content="noindex, nofollow">
      <title>Create Retailer</title>
      
      <link rel="shortcut icon" type="image/x-icon" href="{{ asset('public/Admin/img/favicon.png')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/bootstrap.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/animate.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/dataTables.bootstrap4.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/fontawesome.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/all.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/style.css')}}">
   </head>
   <body>
      <div id="global-loader">
         <div class="whirly-loader"> </div>
      </div>
      <div class="main-wrapper">
        @extends('layouts.header')
     
        @extends('layouts.sidebar')
        
        <div class="page-wrapper">
            <div class="content container-fluid">
               <div class="page-header">
                  <div class="row">
                     <div class="col">
                        <h3 class="page-title">Create Retailer</h3>
                        <ul class="breadcrumb">
                           <li class="breadcrumb-item"><a href="{{route('admin.home')}}">{{ trans('global.dashboard') }}</a></li>
                           
                           <li class="breadcrumb-item active">Create Retailer</li>
                        </ul>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-12">
                     <div class="card">
                        <!-- <div class="card-header row">
                            <div class="col-lg-6"><h4 class="card-title">{{ trans('global.create') }} {{ trans('cruds.brands.title_singular') }}</h4></div>
                            
                        </div> -->
                        <div class="card-body">
                          <form action="{{ route('admin.retailers.store') }}" method="POST" id="frm_retailer">
                            {{ csrf_field() }}
                            <div class="row">
                              <input type="hidden" name="referral_id" id="referral_id" value="{{Auth::user()->id}}">
                              <input type="hidden"  name="roles[]" id="roles" value="retailer">
                              
                              <div class="form-group col-lg-6">
                                 <label>Shop Name</label>
                                 <input type="text" id="shop_name" name="shop_name" value="{{ old('shop_name') }}" required class="form-control" placeholder="{{ trans('cruds.retailer.fields.shop_name') }}*"> 
                                 @if($errors->has('shop_name'))
                                <em class="invalid-feedback">{{ $errors->first('shop_name') }}</em>
                                @endif
                              </div>

                              <div class="form-group col-lg-6">
                                 <label>Customer Name</label>
                                 <input type="text" id="name" name="name" value="{{ old('name') }}" required class="form-control" placeholder="{{ trans('cruds.user.fields.name') }}*" >   
                                 @if($errors->has('name'))
                                <em class="invalid-feedback">{{ $errors->first('name') }}</em>
                                @endif
                              </div>

                              <div class="form-group col-lg-6">
                                 <label>Email</label>
                                 <input type="text" id="email" name="email" class="form-control" value="{{ old('email') }}" placeholder="{{ trans('cruds.user.fields.email') }}">   
                                 @if($errors->has('email'))
                                <em class="invalid-feedback">{{ $errors->first('email') }}</em>
                                @endif
                              </div>

                              <div class="form-group col-lg-6">
                                 <label>Password</label>
                                 <input type="password" id="password" name="password" class="form-control" required placeholder="{{ trans('cruds.user.fields.password') }}">   
                                 @if($errors->has('password'))
                                <em class="invalid-feedback">{{ $errors->first('password') }}</em>
                                @endif
                              </div>

                              <div class="form-group col-lg-6">
                                 <label>Mobile Number</label>
                                <input type="text" id="phone" name="phone" value="{{ old('phone') }}" required class="form-control" placeholder="Mobile Number*" >    
                                 @if($errors->has('phone'))
                                <em class="invalid-feedback">{{ $errors->first('phone') }}</em>
                                @endif
                              </div>

                              <div class="form-group col-lg-6">
                                 <label>Other Phone Number</label>
                                <input type="text" id="other_phone" name="other_phone" value="{{ old('other_phone') }}" class="form-control" placeholder="Other phone Number" >    
                                 @if($errors->has('other_phone'))
                                <em class="invalid-feedback">{{ $errors->first('other_phone') }}</em>
                                @endif
                              </div>

                              <div class="form-group col-lg-6">
                                 <label>Address</label>
                                <input type="text" class="form-control" name="address" id="address" placeholder="Address" required>   
                                 @if($errors->has('address'))
                                <em class="invalid-feedback">{{ $errors->first('address') }}</em>
                                @endif
                              </div>

                              <div class="form-group col-lg-6">
                                 <label>Country*</label>
                                 <select class="form-control" name="country" id="country" required>
                                       <option value="">Select Country</option>
                                       @foreach($countries as $key => $country)
                                       <option  value="{{$key}}"> {{$country}}</option>
                                       @endforeach
                                    </select>
                                 @if($errors->has('country'))
                                <em class="invalid-feedback">{{ $errors->first('country') }}</em>
                                @endif
                              </div>

                              <div class="form-group col-lg-6">
                                 <label>State*</label>
                                 <select class="form-control" name="state" id="state" required>
                                 </select>
                                 @if($errors->has('state'))
                                <em class="invalid-feedback">{{ $errors->first('state') }}</em>
                                @endif
                              </div>

                              <div class="form-group col-lg-6">
                                 <label>City*</label>
                                  <input type="text" class="form-control" name="city" id="city" placeholder="City" value="{{ old('city') }}" required>
                                 @if($errors->has('city'))
                                <em class="invalid-feedback">{{ $errors->first('city') }}</em>
                                @endif
                              </div>

                              <div class="form-group col-lg-6">
                                 <label>Pin Code</label>
                                 <input type="text" class="form-control" name="pin_code" id="pin_code" placeholder="Pin Code"  value="{{ old('pin_code') }}" required>
                                 @if($errors->has('pin_code'))<span class="text-danger">{{ $errors->first('pin_code') }}</span>@endif
                              </div>

                              <div>
                                 <button type="submit" class="btn btn-success">Save</button>
                              </div>
                           </div>
                           
                           </form>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
    <script src="{{ asset('public/Admin/js/jquery-3.6.0.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/feather.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.slimscroll.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{ asset('public/Admin/plugins/apexchart/apexcharts.min.js')}}"></script>
    <script src="{{ asset('public/Admin/plugins/apexchart/chart-data.js')}}"></script>
    <script src="{{ asset('public/Admin/js/script.js')}}"></script>
    <script type="text/javascript">
   //On change 
    $('#country').change(function(){
    var countryID = $(this).val();   
   
    if(countryID){
        $.ajax({
           type:"GET",
           url:"{{url('user/get-state-list')}}?country_id="+countryID,
           success:function(res){               
            if(res){
                $("#state").empty();
                $("#state").append('<option>Select</option>');
                $.each(res,function(key,value){
                    $("#state").append('<option value="'+key+'">'+value+'</option>');
                });
               $('#state').selectpicker('refresh');
            }else{
               $("#state").empty();
            }
           }
        });
    }else{
        $("#state").empty();
       
    }      
   }); 
   
    $('#country_id').change(function(){
    var countryID = $(this).val();   
   
    if(countryID){
        $.ajax({
           type:"GET",
           url:"{{url('user/get-state-list')}}?country_id="+countryID,
           success:function(res){               
            if(res){
                $("#state_id").empty();
                $("#state_id").append('<option>Select</option>');
                $.each(res,function(key,value){
                    $("#state_id").append('<option value="'+key+'">'+value+'</option>');
                });
               $('#state_id').selectpicker('refresh');
            }else{
               $("#state_id").empty();
            }
           }
        });
    }else{
        $("#state_id").empty();
      
    }      
   }); 
   
   $('.datepicker').datepicker({
    format: 'yyyy-mm-dd',
   autoclose: true,
    
});
  
</script>
   </body>
</html>