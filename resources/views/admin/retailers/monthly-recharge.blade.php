@extends('layouts.admin')
@section('content')
<div class="breadcomb-area">
  <div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="breadcomb-list">
            <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="breadcomb-wp">
                  <div class="breadcomb-icon">
                    <i class="notika-icon notika-windows"></i>
                  </div>
                  <div class="breadcomb-ctn">
                    <h2>Users Monthly Recharge Report</h2>
                    <p>Show <span class="bread-ntd">all list</span></p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
    <!-- start filter-area -->
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="form-element-list mg-t-0">
          <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                <div class="form-example-int form-example-st">
                    <div class="form-group">
                      <div class="nk-int-st">
                          <select class="selectpicker select-filter" name="user_id" id="user_id" data-live-search="true">
                              <option value="">Serach Name / Mobile number</option>
                              @foreach($retailerUsers as $key => $retailerUser) 
                                <option value="{{ $retailerUser->id ?? '' }}">{{ $retailerUser->name ?? '' }} ({{ $retailerUser->phone ?? '' }})  </option>
                              @endforeach
                          </select>
                      </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
              <select class="selectpicker select-filter" name="status" id="status" data-live-search="true">
                  <option value="">Status</option>
                  <option value="1">Paid</option>
                  <option value="0">Unpaid</option>
              </select>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
              <select class="selectpicker select-filter" name="payment_mode" id="payment_mode" data-live-search="true">
                  <option value="">Payment Method</option>
                  <option value="1">Credit Wallet</option>
                  <option value="2">Cashfree</option>
              </select>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
              <div class="form-group nk-datapk-ctm form-elet-mg" id="data_1" >
                <div class="input-group date nk-int-st">
                  <span class="input-group-addon"></span>
                  <input type="text" class="form-control input-filter" name="from_date" id="from_date"  placeholder="From Date" >
                </div>
              </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">          
              <div class="form-group nk-datapk-ctm form-elet-mg" id="data_1">
                <div class="input-group date nk-int-st">
                  <span class="input-group-addon"></span>
                  <input type="text" class="form-control input-filter" name="to_date" id="to_date" placeholder="To Date">
                </div>
              </div>
            </div>
            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
              <select class="selectpicker select-filter" name="serach_date_by" id="serach_date_by" data-live-search="true">
                  <option value="">Date</option>
                  <option value="recharge_date">Recharge Date</option>
                  <option value="expire_date">Expire Date</option>
              </select>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                <div class="form-example-int form-example-st">
                    <div class="form-group">
                      <div class="nk-int-st">
                        <input type="submit" class="btn btn-success notika-btn-success waves-effect" id="btnFiterSubmitSearch" value="Search" >
                        <input type="button" class="btn btn-success notika-btn-success waves-effect" id="btnFiterSubmitReset" value="Reset" >
                      </div>
                    </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- end filter-area -->
  </div>
</div>
<!-- Data Table area Start-->
<div class="data-table-area" id="users-table">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="data-table-list">
          <div class="table-responsive">
            <table class="table table-striped datatable datatable-usersMonthlyRechargeReport">
              <thead>
                <tr role="row" class="heading">
                  <th>Invoice No.</th>
                  <th>Name</th>
                  <th>Recharge Duration</th>
                  <th>Recharge Amount</th>
                  <th>Expire Date</th>
                  <th>Recharge Date</th>
                  <th>Payment Method</th>
                  <th>Status</th>
                </tr>
              </thead>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@push('scripts')
<script type="text/javascript">
$(document).ready(function (){
    /* @author Naren Thaker @narenthaker.com 101220201500 */
    window._token = $('meta[name="csrf-token"]').attr('content');

    let excelButtonTrans = '{{ trans('global.datatables.export') }} {{ trans('global.datatables.excel') }}';

    $.extend(true, $.fn.dataTable.Buttons.defaults.dom.button, { className: 'btn' })

    $.extend(true, $.fn.dataTable.defaults, {
        responsive:!1,
        lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
        pageLength:10,
        processing: true,
        serverSide: true,
        buttons: [],
        dom: 'lBfrtip<"actions">',
    });

    var table = $('.datatable-usersMonthlyRechargeReport').DataTable({
      ajax: {
        headers: {'x-csrf-token': _token},
        url: "{{ url('admin/monthly-recharge-report') }}",
        type: 'GET',
        data: function (d) {
          d.user_id = $('#user_id').val();
          d.status = $('#status').val();
          d.payment_mode = $('#payment_mode').val();
          d.to_date = $('#to_date').val();
          d.from_date = $('#from_date').val();
          d.serach_date_by = $('#serach_date_by').val();
        }
      },
      columns: [
            { data: 'invoice_no', name: 'invoice_no', title: 'Invoice No' },
            {
                data: 'user_id',
                name: 'user_id',
                orderable: false, 
                searchable: false, 
                title: 'Name',                
                "render": function ( data, type, row, meta ) {
                  return '<a href="{{ url("admin/retailers") }}/'+row.account_user_id+'/edit">'+row.user_id+'</a>'; 
                }
            },
            { data: 'recharge_duration', name: 'recharge_duration', title: 'Recharge Duration' },
    			  { data: 'recharge_amount', name: 'recharge_amount', title: 'Recharge Amount' },
            {
                data: 'expire_date',
                type: 'num',
                render: {
                  _: 'display',
                  sort: 'timestamp'
                },
                title: 'Expire Date'
            },
    		    {
                data: 'created_at',
                type: 'num',
                render: {
                  _: 'display',
                  sort: 'timestamp'
                },
                title: 'Recharge Date'
            },
    	    { data: 'payment_mode', name: 'payment_mode', title: 'Payment Method'},
    	    { data: 'status', name: 'status', title: 'Status'}
      ],
      buttons: [
        {
          extend: 'excel',
          className: 'btn btn-success notika-btn-success waves-effect',
          text: excelButtonTrans,
          exportOptions: {
            columns: ':visible'
          },
          action: function(e, dt, node, config) {
            var dtButton= this;
            var currentPageLen = dt.page.len();
            var currentPage = dt.page.info().page; 
            dt.one( 'draw', function () {
                $.fn.DataTable.ext.buttons.excelHtml5.action.call(dtButton, e, dt, node, config);  
                setTimeout(function() {
                    dt.page.len(currentPageLen).draw();
                    dt.page(currentPage).draw('page');
                }), 500;
            });
            dt.page.len(-1).draw();
          }
        },
      ],
    });
});

$('#btnFiterSubmitSearch').click(function(){
  $('.datatable-usersMonthlyRechargeReport').DataTable().draw(true);
});
$('#btnFiterSubmitReset').click(function(){
  $("#user_id").val('');
  $("#user_id").selectpicker("refresh");
  $('.datatable-usersMonthlyRechargeReport').DataTable().draw(true);
});
</script>
<style>
.dataTables_filter
{
  display:none;
}
div.dt-buttons{float: right;}
</style>
@endpush