@extends('layouts.admin')
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
<div class="breadcomb-area">
  <div class="container">
    <div class="row">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="breadcomb-list">
            <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="breadcomb-wp">
                  <div class="breadcomb-icon">
                    <i class="notika-icon notika-windows"></i>
                  </div>
                  <div class="breadcomb-ctn">
                    <h2>Users Credit Purchase Report</h2>
                    <p>Show <span class="bread-ntd">all list</span></p>
                  </div>
                </div>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-3 col-xs-12">
                <input type="text" class="form-control" name="user_id" id="user_id" placeholder="Entet User Id/Email">
              </div>
              <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
                <input type="submit" class="btn btn-success notika-btn-success waves-effect" id="btnFiterSubmitSearch" value="Search" >
                <input type="button" class="btn btn-success notika-btn-success waves-effect" id="btnFiterSubmitReset" value="Reset">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Data Table area Start-->
<!-- Data Table area Start-->
<div class="data-table-area" id="users-table">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="data-table-list">
          <div class="table-responsive">
            <table class="table table-striped datatable datatable-rechargeCommission">
              <thead>
                <tr role="row" class="heading">
                  <th>Invoice No</th>
                  <th>Name</th>
                  <th>Credit Point</th>
                  <th>Description</th>
                  <th>Purchase Date</th>
                  <th>Status</th>
                </tr>
              </thead>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@push('scripts')
<script type="text/javascript">

$(document).ready(function (){
    /* @author Naren Thaker @narenthaker.com 101220201500 */
    window._token = $('meta[name="csrf-token"]').attr('content');

    let excelButtonTrans = '{{ trans('global.datatables.export') }} {{ trans('global.datatables.excel') }}';

    $.extend(true, $.fn.dataTable.Buttons.defaults.dom.button, { className: 'btn' })

    $.extend(true, $.fn.dataTable.defaults, {
        responsive:!1,
        lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
        pageLength:10,
        processing: true,
        serverSide: true,
        buttons: [],
        dom: 'lBfrtip<"actions">',
    });

    var table = $('.datatable-rechargeCommission').DataTable({
      ajax: {
        headers: {'x-csrf-token': _token},
        url: "{{ url('admin/credit-purchase-report') }}",
        type: 'GET',
        data: function (d) {
          d.user_id = $('#user_id').val();
        }
      },
      columns: [
        { data: 'invoice_no', name: 'invoice_no' },
        { data: 'receiver_id', name: 'receiver_id' },
        { data: 'credit_amt', name: 'credit_amt' },
        { data: 'transaction_description', name: 'transaction_description' },
        {
          data: 'created_at',
          type: 'num',
          render: {
            _: 'display',
            sort: 'timestamp'
          }
        },
        { data: 'status', name: 'status' }
      ],
      buttons: [
      {
          extend: 'excel',
          className: 'btn btn-success notika-btn-success waves-effect',
          text: excelButtonTrans,
          exportOptions: {
            columns: ':visible'
          },
          action: function(e, dt, node, config) {
            var dtButton= this;
            var currentPageLen = dt.page.len();
            var currentPage = dt.page.info().page; 
            dt.one( 'draw', function () {
                $.fn.DataTable.ext.buttons.excelHtml5.action.call(dtButton, e, dt, node, config);  
                setTimeout(function() {
                    dt.page.len(currentPageLen).draw();
                    dt.page(currentPage).draw('page');
                }), 500;
            });
            dt.page.len(-1).draw();
          }
        },
      ],
    });
});

$('#btnFiterSubmitSearch').click(function(){
  $('.datatable-rechargeCommission').DataTable().draw(true);
});
$('#btnFiterSubmitReset').click(function(){
  $("#user_id").val('');
  $("#user_id").selectpicker("refresh");
  $('.datatable-rechargeCommission').DataTable().draw(true);
});
</script>
<style>
.dataTables_filter
{
  display:none;
}
div.dt-buttons{float: right;}
</style>
@endpush