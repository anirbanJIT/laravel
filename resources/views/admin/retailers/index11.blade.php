@extends('layouts.admin')
@section('content')

<div class="breadcomb-area">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="breadcomb-list">
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<div class="breadcomb-wp">
									<div class="breadcomb-icon">
										<i class="notika-icon notika-windows"></i>
									</div>
									<div class="breadcomb-ctn">
										<h2>Retailers</h2>
										<p> <span class="bread-ntd">all list</span></p>
									</div>
								</div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-3">
							{{--	<div class="breadcomb-report">
								
									 <a class="btn btn-success notika-btn-success waves-effect" href="{{ route('admin.users.create') }}">
                {{ trans('global.add') }} {{ trans('cruds.user.title_singular') }}
            </a>
									
							</div>  --}}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
 <!-- Data Table area Start-->
    <div class="data-table-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="data-table-list">
                        <!--<div class="basic-tb-hd">
                            <h2>Basic Example</h2>
                            <p>It's just that simple. Turn your simple table into a sophisticated data table and offer your users a nice experience and great features without any effort.</p>
                        </div>-->
                    <div class="table-responsive">
						<table id="data-table-basic" class="table table-striped">
							<thead>
								<tr>
									<th width="10"> </th>     								   
							<th>
								{{ trans('cruds.user.fields.name') }}
							</th>
							<th>
								{{ trans('cruds.user.fields.email') }}
							</th>
						   <th>Status</th>
						   <th>KYC Status</th>
							<th> &nbsp;  </th>
							</tr>                       
                         </thead>
                        <tbody>           
						 
                        @foreach($retailerUsers as $key => $retailerUser)      
							
                        <tr data-entry-id="{{ $retailerUser ->id }}">
                            <td> </td>
                            <td>{{ $retailerUser->name ?? '' }}  </td>
                            <td>{{ $retailerUser->email ?? '' }}  </td>    
						   <td>
							  <div class="nk-toggle-switch" data-ts-color="green">
								<input class="status_change" data-id="{{$retailerUser->id}}" id="ts{{$retailerUser->id}}" type="checkbox" hidden="hidden" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" data-off="InActive" {{ $retailerUser->status ? 'checked' : '' }}>
								<label for="ts{{$retailerUser->id}}" class="ts-helper"></label>
							 </div>
                           </td>

						   <td>
							  <div class="nk-toggle-switch" data-ts-color="green">
								<input class="kyc_status_change" data-id="{{$retailerUser->getKycDetail['id'] ? $retailerUser->getKycDetail['id'] : ''}}" id="ks{{$retailerUser->getKycDetail['id'] ? $retailerUser->getKycDetail['id'] : ''}}" type="checkbox" hidden="hidden" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Approve" data-off="Unapprove" {{ $retailerUser->getKycDetail['status'] ? 'checked' : '' }}>
								<label for="ks{{$retailerUser->getKycDetail['id'] ? $retailerUser->getKycDetail['id'] : ''}}" class="ts-helper"></label>
							 </div>
                            </td>
                           <td>
                               
                               <a  href="{{ route('admin.retailers.show', $retailerUser->id) }}"><button class="btn btn-success success-icon-notika btn-reco-mg btn-button-mg waves-effect"><i class="notika-icon notika-menus"></i></button></a>																
							   <a  href="{{ route('admin.retailers.edit', $retailerUser->id) }}"><button class="btn btn-info info-icon-notika btn-reco-mg btn-button-mg waves-effect"><i class="notika-icon notika-edit"></i></button></a>
                                <form action="{{ route('admin.retailers.destroy', $retailerUser->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                    <input type="hidden" name="_method" value="DELETE">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <button type="submit" class="btn btn-danger danger-icon-notika btn-reco-mg btn-button-mg waves-effect"> <i class="notika-icon notika-close"></i></button>
                                </form>
                                <a  href="{{ route('admin.setting.commission', $retailerUser->id) }}"><button class="btn btn-amber amber-icon-notika btn-reco-mg btn-button-mg waves-effect"><i class="notika-icon notika-star"></i></button></a>
						   </td>
                          </tr>
                         @endforeach
                        </tbody>
						<tfoot>
					     <tr>
						   <th width="10">	</th>  
                           <th>{{ trans('cruds.user.fields.id') }}</th>
                           <th> {{ trans('cruds.user.fields.name') }} </th> 
                           <th>{{ trans('cruds.user.fields.email') }}  </th>
                             <th> &nbsp; </th> 

						</tr>
						</tfoot>
					</table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	<script>
  $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('users_manage')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.users.mass_destroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  $.extend(true, $.fn.dataTable.defaults, {
    order: [[ 1, 'desc' ]],
    pageLength: 100,
  });
  $('.datatable-User:not(.ajaxTable)').DataTable({ buttons: dtButtons })
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
        $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust();
    });
	
	//for kyc status
		$('.kyc_status_change').change(function() {
		
        var status = $(this).prop('checked') == true ? 1 : 0; 
		
        var kyc_id = $(this).data('id'); 
		if(kyc_id){
		 $.ajax({
           type:"GET",
           url:"{{url('admin/kyc/changeStatus')}}?status="+status+"&kyc_id="+kyc_id,
           success:function(res){               
            if(res){
				//alert(res);
                console.log(res.success);
           
            }else{
               //alert("nnnn");
            }
           }
        });
		}else{
			alert('KYC is not uploaded of this user!');
		}
    });
	
});

</script>
@endsection

