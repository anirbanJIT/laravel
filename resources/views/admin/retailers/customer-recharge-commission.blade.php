@extends('layouts.admin')
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
@if(session('payoutMsg'))
<div class="container">
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="row mb-2">
        <div class="col-lg-12">
          <div class="alert alert-warning" role="alert">
            <h4>Here show payout status for following retailers : </h4>
            <table  class="table" >
              <thead>
                <tr>
                  <th>User Id</th>
                  <th>User Name</th>
                  <th>Meassage</th>
                </tr>
              </thead>
              @foreach(Session::get('payoutMsg') as $payMsg )
              <tr>
                @foreach($payMsg as $key => $val) 
                <td>{{$val}}</td>
                @endforeach
              </tr>
              @endforeach
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@elseif(Session::has('message'))
<div class="container">
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="row mb-2">
        <div class="col-lg-12">
        <!--  <div class="alert alert-warning" role="alert">-->
        <!--    <i class=" fa fa-check cool-green "></i>-->
        <!--    {{ nl2br(Session::get('message')) }}-->
        <!--  </div>-->
        <!--</div>-->
      </div>
    </div>
  </div>
</div>
@endif
<div class="breadcomb-area">
  <div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="breadcomb-list">
            <div class="row">
              <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                <div class="breadcomb-wp">
                  <div class="breadcomb-icon">
                    <i class="notika-icon notika-windows"></i>
                  </div>
                  <div class="breadcomb-ctn">
                    <h2>Customer Recharge Commission & Payout</h2>
                  </div>
                </div>
              </div>
              <!-- start filter-area -->
              <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                  <div class="form-element-list mg-t-0" style="padding: 0px;">
                    <div class="row">
                      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                          <div class="form-example-int form-example-st">
                              <div class="form-group">
                                <div class="nk-int-st">
                                    <select class="selectpicker select-filter" name="user_id" id="user_id" data-live-search="true">
                                        <option value="">Serach Name / Mobile number</option>
                                        @foreach($retailerUsers as $key => $retailerUser) 
                                          <option value="{{ $retailerUser->id ?? '' }}">{{ $retailerUser->name ?? '' }} ({{ $retailerUser->phone ?? '' }})  </option>
                                        @endforeach
                                    </select>
                                </div>
                              </div>
                          </div>
                      </div>
                      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <select class="selectpicker select-filter" name="status" id="status" data-live-search="true">
                            <option value="">Status</option>
                            <option value="1">Paid</option>
                            <option value="0">Unpaid</option>
                        </select>
                      </div>
                      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                          <div class="form-example-int form-example-st">
                              <div class="form-group">
                                <div class="nk-int-st">
                                  <input type="submit" class="btn btn-success notika-btn-success waves-effect" id="btnFiterSubmitSearch" value="Search" >
                                  <input type="button" class="btn btn-success notika-btn-success waves-effect" id="btnFiterSubmitReset" value="Reset" >
                                </div>
                              </div>
                          </div>
                      </div>
                    </div>
                  </div>
                </div>


                 


              <!-- end filter-area -->
            </div>
          </div>
        </div>
    </div>
  </div>
</div>




<div class="breadcomb-area">
  <div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="breadcomb-list">
            <div class="row">
              <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                <div class="breadcomb-wp">
                 
                  <div class="breadcomb-ctn">



                    <div class="form-element-area">
  <div class="container">
    <div class="row">
    <div class="col-md-6">
    <form action="{{ route('admin.fixcommissionupdate') }}" method="POST" enctype="multipart/form-data">
      <input type="hidden" name="id" value="{{ $data->id}}">
      @csrf
      @method('PUT')
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

          <p>Here <span class="bread-ntd">you can manage your Fix Commission</span></p>


          <div class="form-element-list">                       
            <div class="row">
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                <div class="form-group ic-cmp-int">
                  <div class="form-ic-cmp">
                    <i class="fa fa-pencil" aria-hidden="true"></i>
                  </div>
                  <div class="nk-int-st">
                    <input type="text"  id="commission" name="commission" class="form-control" value="{{ old('commission', isset($data) ? $data->commission : '') }}" placeholder="Fix Commission*" required>
                  </div>
                  @if($errors->has('commission'))
                  <em class="invalid-feedback">
                    {{ $errors->first('commission') }}
                  </em>
                  @endif

                </div>
              </div>
              
              <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4">

                <input type="submit" class="btn btn-success notika-btn-success waves-effect" value="{{ trans('global.save') }}" >
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>
    </div>
    <div class="col-md-6">
    <form action="{{ route('admin.fixrewardupdate') }}" method="POST" enctype="multipart/form-data">
      <input type="hidden" name="id" value="{{ $data1->id}}">
      @csrf
      @method('PUT')
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

          <p>Here <span class="bread-ntd">you can manage your Fix Reward</span></p>


          <div class="form-element-list">                       
            <div class="row">
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                <div class="form-group ic-cmp-int">
                  <div class="form-ic-cmp">
                    <i class="fa fa-pencil" aria-hidden="true"></i>
                  </div>
                  <div class="nk-int-st">
                    <input type="text"  id="commission" name="commission" class="form-control" value="{{ old('commission', isset($data) ? $data1->reward : '') }}" placeholder="Fix Reward*" required>
                  </div>
                  @if($errors->has('reward'))
                  <em class="invalid-feedback">
                    {{ $errors->first('reward') }}
                  </em>
                  @endif

                </div>
              </div>
              
              <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4">

                <input type="submit" class="btn btn-success notika-btn-success waves-effect" value="{{ trans('global.save') }}" >
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>
    </div>
  </div>
</div>

                      





                   </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

 


<!-- Data Table area Start-->
<form action="{{ route('admin.commission-payout') }}" method="POST" enctype="multipart/form-data" id="frm_commission">
  @csrf
  @method('PUT')
  <input type="hidden" name="payout_total_amount" value="yes">
  <div class="data-table-area" id="users-table">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="data-table-list">
            <!-- <div class="basic-tb-hd">
              <input type="submit" class="btn btn-success notika-btn-success waves-effect" id="btnFiterSubmitSearch" value="Pay Now" >
            </div> -->
            <div class="table-responsive">
              <table class="table table-striped datatable datatable-rechargeCommissionpayout" >
                <thead>
                  <tr>
                    <th><input type="checkbox" class="btn btn-success notika-btn-success waves-effect" id="check-all" value="check-all" ></th>
                    <th>Name</th>
                    <th>Paid Amt ({{env('CURRENCY_SYMBOL')}})</th>
                    <th>Unpaid Amt ({{env('CURRENCY_SYMBOL')}})</th>
                    <th>Total Amt ({{env('CURRENCY_SYMBOL')}})</th>
                    <th>Status</th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</form>
@endsection
@push('scripts')
<script type="text/javascript">

$(document).ready(function (){
    /* @author Naren Thaker @narenthaker.com 101220201500 */
    window._token = $('meta[name="csrf-token"]').attr('content');

    let excelButtonTrans = '{{ trans('global.datatables.export') }} {{ trans('global.datatables.excel') }}';

    $.extend(true, $.fn.dataTable.Buttons.defaults.dom.button, { className: 'btn' })

    $.extend(true, $.fn.dataTable.defaults, {
        responsive:!1,
        lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
        pageLength:10,
        processing: true,
        serverSide: true,
        buttons: [],
        dom: 'lBfrtip<"actions">',
    });

    var table = $('.datatable-rechargeCommissionpayout').DataTable({
      processing: true,
      serverSide: true,
      ajax: {
        headers: {'x-csrf-token': _token},
        url: "{{ url('admin/customer-recharge-commission-report') }}",
        type: 'GET',
        data: function (d) {
          d.user_id = $('#user_id').val();
          d.status = $('#status').val();
          d.to_date = $('#to_date').val();
          d.from_date = $('#from_date').val();
        }
      },
      columns: [
        { data: 'checkbox', name: 'checkbox', orderable:false, searchable:false },
        { data: 'user_id', name: 'user_id' },
        { data: 'paid', name: 'paid' },
        { data: 'unpaid', name: 'unpaid' },
        { data: 'total', name: 'total' },
        { data: 'status', name: 'status' }
      ],
      buttons: [
      {
          extend: 'excel',
          className: 'btn btn-success notika-btn-success waves-effect',
          text: excelButtonTrans,
          exportOptions: {
          columns: [ 1, 2, 3, 4, 5 ]
          },
          action: function(e, dt, node, config) {
            var dtButton= this;
            var currentPageLen = dt.page.len();
            var currentPage = dt.page.info().page; 
            dt.one( 'draw', function () {
                $.fn.DataTable.ext.buttons.excelHtml5.action.call(dtButton, e, dt, node, config);  
                setTimeout(function() {
                    dt.page.len(currentPageLen).draw();
                    dt.page(currentPage).draw('page');
                }), 500;
            });
            dt.page.len(-1).draw();
          }
        },
        {
          className: 'btn btn-success notika-btn-success waves-effect payNow',
          text: 'Pay Now',
          exportOptions: {
            columns: ':visible'
          },
          action: function(e, dt, node, config) {
            var allVals = [];  
            $(".pdr_checkbox:checked").each(function() {  
                allVals.push($(this).attr('data-id'));
            });             
            if(allVals.length <=0)  
            { 
                alert("Please select row.");  
            }  
            else {  
              var check = confirm("Are you sure you want to payout this user?");  
              if(check == true){  
                  $('#frm_commission').submit();

                  /*var join_selected_values = allVals.join(","); 
                  console.log(join_selected_values);
                  $.ajax({
                      headers: {'x-csrf-token': _token},
                      url: "{{ route('admin.commission-payout') }}",
                      method: 'PUT',
                      data: 'ids='+join_selected_values,
                      success: function (data) {
                        if (data['success']) { alert(data['success']); } 
                        else if (data['error']) { alert(data['error']); }
                        else { alert('Whoops Something went wrong!!'); }
                      },
                      error: function (data) { alert(data.responseText); }
                  });*/
              }
            }
            var dtButton= this;
            var currentPageLen = dt.page.len();
            var currentPage = dt.page.info().page; 
            dt.page.len(-1).draw();
          }
        }
      ],
    });
});

$('#btnFiterSubmitSearch').click(function(){
  $('.datatable-rechargeCommissionpayout').DataTable().draw(true);
});
$('#btnFiterSubmitReset').click(function(){
  $("#user_id").val('');
  $("#user_id").selectpicker("refresh");
  $('.datatable-rechargeCommissionpayout').DataTable().draw(true);
});

/*select multipal checkbox*/
$('#check-all').on('click', function() {
  if(this.checked){
    $('.pdr_checkbox').prop('checked', true);
  }else{
    $('.pdr_checkbox').prop('checked', false);
  }
});
/*end select multipal checkbox*/
</script>
<style>
.dataTables_filter
{
  display:none;
}
div.dt-buttons{
  float: right;
    display: inline-block;
    width: 200px !important;
}
</style>
@endpush