@extends('layouts.admin')
@section('content')
<style type="text/css">
.cursor-pointer{cursor: pointer;}
</style>
<div class="breadcomb-area">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="breadcomb-list">
          <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
              <div class="breadcomb-wp">
                <div class="breadcomb-icon">
                  <i class="notika-icon notika-windows"></i>
                </div>
                <div class="breadcomb-ctn">
                  <h2>Retailer List</h2>
                  <p>Show <span class="bread-ntd">all list</span></p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Data Table area Start-->
<div class="data-table-area">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="data-table-list">
          <div class="table-responsive">
            <table class="table table-striped datatable-RetailerList" id="datatable-RetailerList">
              <thead>
                <tr role="row" class="heading">
                  <th>Id</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Phone</th>
                  <th>Amount</th>
                  <th>Plan</th>
                </tr>
                <tr role="row" class="filterrow">
                  <th>Id</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Phone</th>
                  <th>Amount</th>
                  <th>Plan</th>
                </tr>
              </thead>
              <tbody>
                @foreach($retailers as $key => $retailer)
                <tr data-entry-id="{{ $retailer->id }}">
                  <td> {{ $retailer->id ?? '' }} </td>
                  <td class="cursor-pointer" onclick="window.location.href ='{{route("admin.buyer-list",$retailer->id)}}'">{{ $retailer->name ?? '' }}</td>
                  <td> {{ $retailer->email ?? '' }} </td>
                  <td> {{ $retailer->phone ?? '' }} </td>
                  <td> {{ $retailer->amount ?? '' }} </td>
                  <td> {{ $retailer->plan->name ?? ''}} </td>
                </tr>
                @endforeach
              </tbody>
              <tfoot>
                <tr>
                  <th>Id</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Phone</th>
                  <th>Amount</th>
                  <th>Plan</th>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@push('scripts')
<script type="text/javascript">
$(document).ready(function (){
    /* @author Naren Thaker @narenthaker.com 101220201500 */
    window._token = $('meta[name="csrf-token"]').attr('content');

    $.extend(true, $.fn.dataTable.Buttons.defaults.dom.button, { className: 'btn' })

    $.extend(true, $.fn.dataTable.defaults, {
        columnDefs: [],
        order: [[ 0, 'desc' ]],
        lengthMenu: [
            [ 10, 25, 50, 100, -1 ],
            [ '10', '25', '50', '100', 'All' ]
        ],
        buttons: [],
        orderCellsTop: true,
        dom: 'lBfrtip<"actions">',
        fixedHeader: true,
    });

    let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons);
    var table = $('.datatable-RetailerList:not(.ajaxTable)').DataTable({
        buttons: dtButtons,
        initComplete: function() {
            this.api().columns().every(function(d) {
                var column = this;
                var theadname = $('.datatable-RetailerList thead th').eq([d]).text();
                var select = $('<select class ="filter searchSelect"><option value="">'+theadname+': All</option></select>')
                             .appendTo($(".datatable-RetailerList thead tr:eq(1) th").eq(column.index()).empty() )
                             .tailselect({ search: true, deselect: true, width: 130, height: 200, placeholder: theadname});
                select.on("change", function(){
                    var val = $.fn.dataTable.util.escapeRegex(this.value());
                    column.search(val? ('^' + val + '$'): '', true, false).draw();
                });                
                column.data().unique().sort().each(function(d, j) {
                    select.options.add(d, d, '#');
                });
                select.query();

            });
        }
    });
    table.on('draw', function() {
        table.columns().indexes().each(function(idx) {
            var select = $(".datatable-RetailerList thead tr:eq(1) th").eq(idx).find('select').tailselect('.select');
            if (select.value() === null) {
                select.reload();                
                table.column(idx, {
                    search: 'applied'
                }).data().unique().sort().each(function(d, j) {
                    select.options.add(d, d);
                });

                select.query();
            }
        });
    });
});
</script>
@endpush
