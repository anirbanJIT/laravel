<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
      <meta name="description" content="POS - Bootstrap Admin Template">
      <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
      <meta name="author" content="Dreamguys - Bootstrap Admin Template">
      <meta name="robots" content="noindex, nofollow">
      <title>Paper Size List</title>
      
      <link rel="shortcut icon" type="image/x-icon" href="{{ asset('public/Admin/img/favicon.png')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/bootstrap.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/animate.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/dataTables.bootstrap4.min.css')}}">

      <!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.7.1/css/buttons.dataTables.min.css"> -->
      
       

      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/fontawesome.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/all.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/style.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/select2/css/select2.min.css')}}">
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <link rel="stylesheet" href="{{ asset('public/Admin/tabulator/bootstrap/tabulator_bootstrap4.css')}}">
      
   </head>
   <body>
      <!-- <div id="global-loader">
         <div class="whirly-loader"> </div>
      </div> -->
      <div class="main-wrapper">
        @extends('layouts.header')
     
        @extends('layouts.sidebar')
        
        <div class="page-wrapper">
            <div class="content container-fluid">
               <div class="page-header">
                  <div class="row">
                     <div class="col">
                        <h3 class="page-title">Paper Size List</h3>
                        <ul class="breadcrumb">
                           <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">{{ trans('global.dashboard') }}</a></li>
                           <li class="breadcrumb-item active">Paper Size List</li>
                        </ul>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-12">
                     <div class="card">
                        <div class="card-header row">
                            <div class="col-lg-6"><h4 class="card-title">Paper Size List</h4></div>
                            <div class="col-lg-6"><a href="{{ route('admin.paper-sizes.create')}}" class="btn btn-success" style="float: right;">+ {{ trans('global.add') }} {{ trans('cruds.paper_size.title_singular') }}</a></div>
                        </div>
                        <div class="card-body">
                           <div class="table-responsive">
                              <!-- <table class="table datanew" id="data-table-machine-numbers"> -->
                                 <button class="btn btn-outline-info waves-effect" id="download-csv">Download CSV</button> <p><br/></p>
                                 <div id="data-table-paper-size">
                                 </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
    <script src="{{ asset('public/Admin/js/jquery-3.6.0.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/feather.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.slimscroll.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/script.js')}}"></script>
      
     <script type="text/javascript" language="javascript" src='https://cdn.datatables.net/buttons/2.2.3/js/dataTables.buttons.min.js' defer></script>
      
      <script type="text/javascript" language="javascript" src='https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js' defer></script>
      
      <script type="text/javascript" language="javascript" src='https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js' defer></script>
      
      <script type="text/javascript" language="javascript" src='https://cdn.datatables.net/buttons/2.2.3/js/buttons.html5.min.js' defer></script> 
      
      <!-- <script type="text/javascript" src="{{ asset('public/Admin/js/3rdparty/tabulator.min.js') }}"></script> -->
      <script src="{{ asset('public/Admin/tabulator/tabulator.min.js') }}"></script>
      <script src="{{ asset('public/Admin/tabulator/jspdf.min.js') }}"></script>
      <script src="{{ asset('public/Admin/tabulator/xlsx.full.min.js') }}"></script>
      <script src="{{ asset('public/Admin/tabulator/jspdf.plugin.autotable.js') }}"></script>
      <script type="text/javascript">
      
      var tabledata = <?php echo json_encode($paperSizes); ?>;
      console.log(tabledata);
      function customFilter(data){
          return data.car && data.rating < 3;
      }

      function updateFilter(){

          var filter = $("#filter-field").val() == "function" ? customFilter : $("#filter-field").val();

          if($("#filter-field").val() == "function" ){
              $("#filter-type").prop("disabled", true);
              $("#filter-value").prop("disabled", true);
          }else{
              $("#filter-type").prop("disabled", false);
              $("#filter-value").prop("disabled", false);
          }

          table.setFilter(filter, $("#filter-type").val(), $("#filter-value").val());
      }

      //Update filters on value change
      $("#filter-field, #filter-type").change(updateFilter);
      $("#filter-value").keyup(updateFilter);

      var table = new Tabulator("#data-table-paper-size", {
          data:tabledata,           //load row data from array
          layout:"fitColumns",      //fit columns to width of table
          responsiveLayout:"collapse",  //hide columns that dont fit on the table
          tooltips:true,            //show tool tips on cells
          addRowPos:"top",          //when adding a new row, add it to the top of the table
          history:true,             //allow undo and redo actions on the table
          pagination:"local",       //paginate the data
          paginationSize:10,         //allow 7 rows per page of data
          movableColumns:true,      //allow column order to be changed
          resizableRows:true,       //allow row order to be changed
          initialSort:[             //set the initial sort order of the data
              {column:"id", dir:"desc"},
          ],
          columns: [
               // { data: "delete_chk", orderable:false, searchable:false},
                 {title:"Title", field:"title", editor:"label",headerFilter:"input",headerFilterPlaceholder:"Title"},
                {title:"Paper Height", field:"paper_height", editor:"label",headerFilter:"input",headerFilterPlaceholder:"Paper Height"},
                {title:"Paper Width", field:"paper_width", editor:"label",headerFilter:"input",headerFilterPlaceholder:"Paper Width"},
                {title:"Status", field:"id_status", 
					formatter:function(cell, formatterParams, onRendered){
						var arr = cell.getValue();
						var data = arr.split("_");
						if(data[1] == '1'){
							return '<div class="status-toggle d-flex justify-content-between align-items-center" onclick="$(\'#is_delete\').val(1)"><input type="checkbox" class="check status_change" data-id="'+data[0]+'" id="ts'+data[0]+'" data-on="Active"  checked ><label for="ts'+data[0]+'" class="checktoggle">checkbox</label></div>';
						} else {
							return '<div class="status-toggle d-flex justify-content-between align-items-center" onclick="$(\'#is_delete\').val(1)"><input type="checkbox" class="check status_change" data-id="'+data[0]+'" id="ts'+data[0]+'" data-off="InActive"><label for="ts'+data[0]+'" class="checktoggle">checkbox</label></div>';
						}            
					}
				},
               {title:"Action", field:"id", formatter:function(cell, formatterParams, onRendered)
               {
                  var edit_url = '{{ route("admin.paper-sizes.edit", ":id") }}';
                  edit_url = edit_url.replace(':id', cell.getValue());

                  var delete_url1 = '{{ route("admin.paper-sizes.delete", ":id") }}';
                  delete_url1 = delete_url1.replace(':id', cell.getValue());
                  var msg = "Are you sure you want to delete this Paper Size?";

                  var editlink = '<a  href="'+edit_url+'" class="btn btn-success text-white" title="Edit"><i class="fa fa-edit"></i></a><a  href="'+delete_url1+'" class="btn btn-danger text-white sa-warning" title="Remove" onclick="return confirm(\''+msg+'\');"><i class="fa fa-trash"></i></a>';
                  return editlink; 
               },
               }
            ],
      });
	  
	  $('.status_change').change(function() 
         {      
			
            var status = $(this).prop('checked') == true ? 1 : 0; 
            var model_id = $(this).data('id');  
					
             $.ajax({
                 type:"GET",
                 url:"{{url('admin/changeStatusPaperSize')}}?status="+status+"&paperSize_id="+model_id,
                 success:function(res){               
                  if(res){
					  $('#is_delete').val(0);
                      console.log(res.success);
                  }
                 }
              });
   
         });


      //trigger download of data.xlsx file
      document.getElementById("download-csv").addEventListener("click", function(){
          table.download("csv", "data.csv");
      });

      

</script>
   </body>
</html>