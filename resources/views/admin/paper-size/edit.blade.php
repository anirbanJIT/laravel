<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
      <meta name="description" content="POS - Bootstrap Admin Template">
      <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
      <meta name="author" content="Dreamguys - Bootstrap Admin Template">
      <meta name="robots" content="noindex, nofollow">
      <title>{{ trans('global.edit') }} {{ trans('cruds.paper_size.title_singular') }}</title>
      
      <link rel="shortcut icon" type="image/x-icon" href="{{ asset('public/Admin/img/favicon.png')}}">
      
      <link rel="stylesheet" href="{{ asset('public/Admin/css/bootstrap.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/select2/css/select2.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/animate.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/dataTables.bootstrap4.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/fontawesome.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/all.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/style.css')}}">
      <style>
		.error{
			color :red ! important;
		}
	  </style>
   </head>
   <body>
      <div id="global-loader">
         <div class="whirly-loader"> </div>
      </div>
      <div class="main-wrapper">
        @extends('layouts.header')
     
        @extends('layouts.sidebar')
        
        <div class="page-wrapper">
            <div class="content container-fluid">
               <div class="page-header">
                  <div class="row">
                     <div class="col">
                        <h3 class="page-title">{{ trans('global.edit') }} {{ trans('cruds.paper_size.title_singular') }}</h3>
                        <ul class="breadcrumb">
                           <li class="breadcrumb-item"><a href="{{route('admin.home')}}">{{ trans('global.dashboard') }}</a></li>
                           <li class="breadcrumb-item"><a href="{{route('admin.paper-sizes.index')}}">{{ trans('cruds.paper_size.title') }}</a></li>
                           <li class="breadcrumb-item active">{{ trans('global.edit') }} {{ trans('cruds.paper_size.title_singular') }}</li>
                        </ul>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-12">
                     <div class="card">
                        <!-- <div class="card-header row">
                            <div class="col-lg-6"><h4 class="card-title">{{ trans('global.create') }} {{ trans('cruds.permission.title_singular') }}</h4></div>
                            
                        </div> -->
                        <div class="card-body">
                           <form action="{{ route('admin.paper-sizes.update', [$paperSize->id]) }}" method="POST" id="frm">
                            {{ csrf_field() }} @method('PUT')
                              
                              <div class="form-group">
                                 <label>Title<span class="text-danger">*</span></label>
                                 <input type="text" id="title" name="title" value="{{ $paperSize->title }}"  class="form-control @error('title') is-invalid @enderror" placeholder="{{ trans('cruds.paper_size.fields.title') }}*" >
                                 @if($errors->has('title')) <em class="invalid-feedback"> {{ $errors->first('title') }} </em> @endif
                              </div>

                              <div class="form-group">
                                 <label>Width<span class="text-danger">*</span></label>
                                 <input type="text" id="paper_width" name="paper_width" value="{{ $paperSize->paper_width }}"  class="form-control @error('paper_width') is-invalid @enderror" placeholder="{{ trans('cruds.paper_size.fields.paper_width') }}*" >
                                 @if($errors->has('paper_width')) <em class="invalid-feedback"> {{ $errors->first('paper_width') }} </em> @endif
                              </div>

                              <div class="form-group">
                                 <label>Height<span class="text-danger">*</span></label>
                                 <input type="text" id="paper_height" name="paper_height" value="{{ $paperSize->paper_height }}"  class="form-control @error('paper_height') is-invalid @enderror" placeholder="{{ trans('cruds.paper_size.fields.paper_height') }}*" >
                                 @if($errors->has('paper_height')) <em class="invalid-feedback"> {{ $errors->first('paper_height') }} </em> @endif
                              </div>

                              <div class="">
                                 <button type="submit" class="btn btn-success">Save</button>
                              </div>
                           </form>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
    <script src="{{ asset('public/Admin/js/jquery-3.6.0.min.js')}}"></script>
    <script src="{{ asset('public/Admin/plugins/select2/js/select2.min.js')}}"></script>
    <script src="{{ asset('public/Admin/plugins/select2/js/custom-select.js')}}"></script>
    <script src="{{ asset('public/Admin/js/feather.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.slimscroll.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/script.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
    <script type="text/javascript">
       $(function(){
   $("#frm").validate({
       rules: {
         title: {
            required: true,
         },
         paper_width: {
            required: true,
         },
         paper_height: {
            required: true,
         },
     },
      messages: { 
           title: {
               required: "Please enter title"
           },
           paper_width: {
               required: "Please enter paper width"
           },
           paper_height: {
               required: "Please enter paper height"
           },
       },
   });
});
    </script>
   </body>
</html>