<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
      <meta name="description" content="POS - Bootstrap Admin Template">
      <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
      <meta name="author" content="Dreamguys - Bootstrap Admin Template">
      <meta name="robots" content="noindex, nofollow">
      <title>{{ trans('global.create') }} {{ trans('cruds.recharge-plan.title_singular') }}</title>
      
      <link rel="shortcut icon" type="image/x-icon" href="{{ asset('public/Admin/img/favicon.png')}}">
      
      <link rel="stylesheet" href="{{ asset('public/Admin/css/bootstrap.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/select2/css/select2.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/animate.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/dataTables.bootstrap4.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/fontawesome.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/all.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/style.css')}}">
      
   </head>
   <body>
      <div id="global-loader">
         <div class="whirly-loader"> </div>
      </div>
      <div class="main-wrapper">
        @extends('layouts.header')
     
        @extends('layouts.sidebar')
        
        <div class="page-wrapper">
            <div class="content container-fluid">
               <div class="page-header">
                  <div class="row">
                     <div class="col">
                        <h3 class="page-title">{{ trans('global.create') }} {{ trans('cruds.recharge-plan.title_singular') }}</h3>
                        <ul class="breadcrumb">
                           <li class="breadcrumb-item"><a href="{{route('admin.home')}}">{{ trans('global.dashboard') }}</a></li>
                           <li class="breadcrumb-item"><a href="{{ route('admin.recharge-plan.index') }}">{{ trans('cruds.recharge-plan.title') }}</a></li>
                           <li class="breadcrumb-item active">{{ trans('global.create') }} {{ trans('cruds.recharge-plan.title_singular') }}</li>
                        </ul>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-12">
                     <div class="card">
                        <!-- <div class="card-header row">
                            <div class="col-lg-6"><h4 class="card-title">{{ trans('global.create') }} {{ trans('cruds.permission.title_singular') }}</h4></div>
                            
                        </div> -->
                        <div class="card-body">
                           <form action="{{ route('admin.recharge-plan.store') }}" method="POST" id="frm_recharge_plan">
                            {{ csrf_field() }}
                           <div class="row">   
                              
                              <div class="form-group col-lg-6">
                                 <label>Country*</label>
                                <select class="form-control" name="country_id" id="country_id" required>
                                   <option value="">Select Country</option>
                                   @foreach($countries as $key => $country)
                                   <option  value="{{$key}}" {{old('country_id') == $key ? 'selected' : ''}}> {{$country}}</option>
                                   @endforeach
                               </select>
                                 @if($errors->has('country_id')) <em class="invalid-feedback"> {{ $errors->first('country_id') }} </em> @endif
                              </div>

                              <div class="form-group col-lg-6">
                                 <label>Current Plan *</label>
                                 <select class="form-control" name="recharge_plan_type" id="recharge_plan_type" required>
                                     @foreach($plans as $id => $name)
                                     <option value="{{ $id }}" {{ (isset($recharge_plan->recharge_plan_type) ? $recharge_plan->recharge_plan_type : old('recharge_plan_type')) == $id ? 'selected' : '' }}>{{ $name }}</option>
                                     @endforeach
                                 </select>
                                 
                                 @if ($errors->has('recharge_plan_type')) <em class="invalid-feedback"> {{ $errors->first('recharge_plan_type') }} </em> @endif
                              </div>

                              <div class="form-group col-lg-6">
                                 <label>Name*</label>
                                 <input type="text" id="recharge_plan_name" name="recharge_plan_name" value="{{ old('recharge_plan_name') }}" required class="form-control" placeholder="{{ trans('cruds.recharge-plan.fields.recharge_plan_name') }}*" >
                                 @if($errors->has('recharge_plan_name')) <em class="invalid-feedback"> {{ $errors->first('recharge_plan_name') }} </em> @endif
                              </div>

                              <div class="form-group col-lg-6">
                                 <label>Recharge Plan Credit*</label>
                                 <input type="text" id="recharge_plan_credit" name="recharge_plan_credit" value="{{ old('recharge_plan_credit') }}" required class="form-control" placeholder="{{ trans('cruds.recharge-plan.fields.recharge_plan_credit') }}*" >
                                 @if($errors->has('recharge_plan_credit')) <em class="invalid-feedback"> {{ $errors->first('recharge_plan_credit') }} </em> @endif
                              </div>

                              <div class="form-group col-lg-6">
                                 <label>Recharge Plan Amount *</label>
                                 <input type="text" id="recharge_plan_amount" name="recharge_plan_amount" value="{{ old('recharge_plan_amount') }}" required class="form-control" placeholder="{{ trans('cruds.recharge-plan.fields.recharge_plan_amount') }}*" >
                                 
                                 @if ($errors->has('recharge_plan_amount')) <em class="invalid-feedback"> {{ $errors->first('recharge_plan_amount') }} </em> @endif
                              </div>

                              <div class="form-group col-lg-6">
                                 <label>Recharge Plan Validity *</label>
                                 <input type="text" id="recharge_plan_validity" name="recharge_plan_validity" value="{{ old('recharge_plan_validity') }}" required class="form-control" placeholder="{{ trans('cruds.recharge-plan.fields.recharge_plan_validity') }}*" >
                                 
                                 @if ($errors->has('recharge_plan_validity')) <em class="invalid-feedback"> {{ $errors->first('recharge_plan_validity') }} </em> @endif
                              </div>
                               
                              <div class="form-group col-lg-6">
                                 <label>Recharge Plan Rewards</label>
                                <input type="text" id="recharge_plan_rewards" name="recharge_plan_rewards" value="{{ old('recharge_plan_rewards') }}" class="form-control" placeholder="{{ trans('cruds.recharge-plan.fields.recharge_plan_rewards') }}" >
                              </div>
                              
                              <div class="form-group col-lg-6">
                                 <label>Plan Role *</label>
                                 <select class="form-control" name="plan_roles" id="plan_roles" required>
                                     @foreach($roles as $id => $roles)
                                     @if($roles != 'Admin' && $roles != 'administrator')
                                     <option value="{{ $id }}" {{ $id == old('plan_roles') ? 'selected' : '' }}>{{ $roles }} </option>
                                     @endif
                                     @endforeach
                                 </select>
                                 
                                 @if ($errors->has('roles')) <em class="invalid-feedback"> {{ $errors->first('roles') }} </em> @endif
                              </div>

                              <div class="form-group col-lg-6">
                                 <label>Recharge Plan Per Day Cost *</label>
                                 <input type="text" id="recharge_plan_per_day_cost" name="recharge_plan_per_day_cost" value="{{ old('recharge_plan_per_day_cost') }}" required class="form-control" placeholder="Per Day Cost*" >
                                 
                                 @if ($errors->has('recharge_plan_per_day_cost')) <em class="invalid-feedback"> {{ $errors->first('recharge_plan_per_day_cost') }} </em> @endif
                              </div>

                              <div class="">
                                 <button type="submit" class="btn btn-success">Save</button>
                              </div>
                           </div>
                           </form>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
    <script src="{{ asset('public/Admin/js/jquery-3.6.0.min.js')}}"></script>
    <script src="{{ asset('public/Admin/plugins/select2/js/select2.min.js')}}"></script>
    <script src="{{ asset('public/Admin/plugins/select2/js/custom-select.js')}}"></script>
    <script src="{{ asset('public/Admin/js/feather.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.slimscroll.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/script.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js">
    </script>
    <script>
       $(function () {
        $("#frm_recharge_plan").validate({
            rules: {
                recharge_plan_type: {
                    required: true,
                },
                recharge_plan_name: {
                    required: true,
                },
                recharge_plan_credit: {
                    required: true,
                },
                recharge_plan_amount: {
                    required: true,
                },
                recharge_plan_per_day_cost: {
                    required: true,
                },
                recharge_plan_validity: {
                    required: true,
                },
                country_id: {
                    required: true,
                },
                currency: {
                    required: true,
                },
            },
            messages: {
                recharge_plan_type: {
                    required: "Please select plan type"
                },
                recharge_plan_name: {
                    required: "Please enter name"
                },
                recharge_plan_credit: {
                    required: "Please enter creadit"
                },
                recharge_plan_amount: {
                    required: "Please enter amount"
                },
                recharge_plan_per_day_cost: {
                    required: "Please enter per day cost"
                },
                recharge_plan_validity: {
                    required: "Please enter validity",
                },
                country_id: {
                    required: "Please select country"
                },
                currency: {
                    required: "Please select currency",
                },
            },
        });
    });
  </script>
   </body>
</html>