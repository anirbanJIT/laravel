<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
      <meta name="description" content="POS - Bootstrap Admin Template">
      <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
      <meta name="author" content="Dreamguys - Bootstrap Admin Template">
      <meta name="robots" content="noindex, nofollow">
      <title>Recharge Plan List</title>
      
      <link rel="shortcut icon" type="image/x-icon" href="{{ asset('public/Admin/img/favicon.png')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/bootstrap.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/animate.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/dataTables.bootstrap4.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/fontawesome.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/all.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/style.css')}}">
   </head>
   <body>
      <div id="global-loader">
         <div class="whirly-loader"> </div>
      </div>
      <div class="main-wrapper">
        @extends('layouts.header')
     
        @extends('layouts.sidebar')
        
        <div class="page-wrapper">
            <div class="content container-fluid">
               <div class="page-header">
                  <div class="row">
                     <div class="col">
                        <h3 class="page-title">Recharge Plan List</h3>
                        <ul class="breadcrumb">
                           <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">{{ trans('global.dashboard') }}</a></li>
                           <li class="breadcrumb-item active">Recharge Plan List</li>
                        </ul>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-12">
                     <div class="card">
                        <div class="card-header row">
                            <div class="col-lg-6"><h4 class="card-title">Recharge Plan List</h4></div>
                            <div class="col-lg-6"><a href="{{ route('admin.recharge-plan.create') }}" class="btn btn-success" style="float: right;">+ {{ trans('global.add') }} {{ trans('cruds.recharge-plan.title_singular') }}</a></div>
                        </div>
                        <div class="card-body">
                           <div class="table-responsive">
                              <table class="table  datanew ">
                                 <thead>
                                    <tr>
                                       <th width="10">
                                       </th>
                                       <th>
                                          Name
                                       </th>
                                       <th>
                                          Type
                                       </th>
                                       <th>
                                          Creadit
                                       </th>
                                       <th>
                                          Amount
                                       </th>
                                       <th>
                                          Validity
                                       </th>
                                       <th>
                                          Rewards
                                       </th>
                                       <th>
                                          Status
                                       </th>
                                       <th>
                                          &nbsp;
                                       </th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    @foreach($recharge_plans as $key => $recharge_plan)
                                    <tr data-entry-id="{{ $recharge_plan->id }}">
                                       <td>
                                       </td>
                                       <td>{{ $recharge_plan->recharge_plan_name ?? '' }}</td>
                                       <td>
                                          @if(!empty($recharge_plan->plan_type->name))
                                             {{ $recharge_plan->plan_type->name ?? '' }}
                                          @else
                                             {{ ucwords(str_replace('_',' ',$recharge_plan->recharge_plan_type)) }}
                                          @endif
                                       </td>
                                       <td>{{ $recharge_plan->recharge_plan_credit ?? '' }}</td>
                                       <td>{{ $recharge_plan->recharge_plan_amount ?? '' }}</td>
                                       <td>{{ $recharge_plan->recharge_plan_validity ?? '' }}</td>
                                       <td>{{ $recharge_plan->recharge_plan_rewards ?? '' }}</td>
                                       <td>
                                          <div class="status-toggle d-flex justify-content-between align-items-center">
                                          <input type="checkbox" class="check status_change" data-id="{{$recharge_plan->id}}" id="ts{{$recharge_plan->id}}" data-on="Active" data-off="InActive" {{ $recharge_plan->recharge_plan_status ? 'checked' : '' }}>
                                          <label for="ts{{$recharge_plan->id}}" class="checktoggle">checkbox</label>
                                          </div>
                                       </td>
                                       <td>
                                          <a  href="{{ route('admin.recharge-plan.edit', $recharge_plan->id) }}" class="btn btn-success text-white" title="Edit"><i class="fa fa-edit"></i></a>
                                          
                                          <a  href="{{ url('admin/recharge-plan/delete', $recharge_plan->id) }}" class="btn btn-danger text-white sa-warning" title="Remove" onclick="return confirm('{{ trans('global.areYouSure') }}');"><i class="fa fa-trash"></i></a>
                                       </td>
                                    </tr>
                                    @endforeach

                                    
                                    
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
    <script src="{{ asset('public/Admin/js/jquery-3.6.0.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/feather.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.slimscroll.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{ asset('public/Admin/plugins/apexchart/apexcharts.min.js')}}"></script>
    <script src="{{ asset('public/Admin/plugins/apexchart/chart-data.js')}}"></script>
    <script src="{{ asset('public/Admin/js/script.js')}}"></script>
    <script type="text/javascript">
        $('.status_change').change(function(e) {
            var status = $(this).prop('checked') == true ? 1 : 0;
            var recharge_plan_id = $(this).data('id');
            $.ajax({
                type: "GET",
                url: "{{url('admin/changeStatusRechargePlan')}}?status=" + status + "&id=" + recharge_plan_id,
                success: function(res) {
                    if (res) {
                     console.log(res.success);
                    } else {
                       console.log(res);
                    }
                }
            });

        });
    </script>
   </body>
</html>