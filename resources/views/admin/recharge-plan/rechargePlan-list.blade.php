<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
      <meta name="description" content="POS - Bootstrap Admin Template">
      <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
      <meta name="author" content="Dreamguys - Bootstrap Admin Template">
      <meta name="robots" content="noindex, nofollow">
      <title>Recharge Plan List</title>
      
      <link rel="shortcut icon" type="image/x-icon" href="{{ asset('public/Admin/img/favicon.png')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/bootstrap.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/animate.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/dataTables.bootstrap4.min.css')}}">

      <!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.7.1/css/buttons.dataTables.min.css"> -->
      
       

      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/fontawesome.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/all.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/style.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/select2/css/select2.min.css')}}">
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <link rel="stylesheet" href="{{ asset('public/Admin/tabulator/bootstrap/tabulator_bootstrap4.css')}}">
		
   </head>
   <body>
      <!-- <div id="global-loader">
         <div class="whirly-loader"> </div>
      </div> -->
      <div class="main-wrapper">
        @extends('layouts.header')
     
        @extends('layouts.sidebar')
        
        <div class="page-wrapper">
            <div class="content container-fluid">
               <div class="page-header">
                  <div class="row">
                     <div class="col">
                        <h3 class="page-title">Recharge Plan List</h3>
                        <ul class="breadcrumb">
                           <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">{{ trans('global.dashboard') }}</a></li>
                           <li class="breadcrumb-item active">Recharge Plan List</li>
                        </ul>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-12">
                     <div class="card">
                        <div class="card-header row">
                            <div class="col-lg-6"><h4 class="card-title">Recharge Plan List</h4></div>
                            <div class="col-lg-6"><a href="{{ route('admin.recharge-plan.create') }}" class="btn btn-success" style="float: right;">+ {{ trans('global.add') }} {{ trans('cruds.recharge-plan.title_singular') }}</a></div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
								<div id="data-table-rechargeplan-list" ></div>
							</div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
    <script src="{{ asset('public/Admin/js/jquery-3.6.0.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/feather.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.slimscroll.min.js')}}"></script>
  
    <script src="{{ asset('public/Admin/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/script.js')}}"></script>
    <script src="{{ asset('public/Admin/tabulator/tabulator.min.js') }}"></script>
    <script src="{{ asset('public/Admin/tabulator/jspdf.min.js') }}"></script>
    <script src="{{ asset('public/Admin/tabulator/xlsx.full.min.js') }}"></script>
    <script src="{{ asset('public/Admin/tabulator/jspdf.plugin.autotable.js') }}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script type="text/javascript">
		
		var tabledata = <?php echo json_encode($recharge_plans); ?>;
		
		var table = new Tabulator("#data-table-rechargeplan-list", {
			data:tabledata,           //load row data from array
			layout:"fitColumns",      //fit columns to width of table
			responsiveLayout:"collapse",  //hide columns that dont fit on the table
			tooltips:true,            //show tool tips on cells
			addRowPos:"top",          //when adding a new row, add it to the top of the table
			history:true,             //allow undo and redo actions on the table
			pagination:"local",       //paginate the data
			paginationSize:10,         //allow 7 rows per page of data
			movableColumns:true,      //allow column order to be changed
			resizableRows:true,       //allow row order to be changed
			columns: [
				{title:"ID", field:"id", editor:"label",headerFilter:"input",headerSort:false,				headerFilterPlaceholder:"Id",
			    },
				{title:"Name", field:"recharge_plan_name", editor:"label",headerFilter:"input",headerSort:false,			headerFilterPlaceholder:"Name"},
				{title:"Type", field:"plan_type.name", editor:"label",headerFilter:"input",headerSort:true,		headerFilterPlaceholder:"Type"},
				{title:"Creadit", field:"recharge_plan_credit", editor:"label",headerFilter:"input",headerSort:true,		headerFilterPlaceholder:"Creadit"},
				{title:"Amount", field:"recharge_plan_amount", editor:"label",headerFilter:"input",headerSort:true,		headerFilterPlaceholder:"Amount"},
				{title:"Validity", field:"recharge_plan_validity", editor:"label",headerFilter:"input",headerSort:true,		headerFilterPlaceholder:"Validity"},
				{title:"Rewards", field:"recharge_plan_rewards", editor:"label",headerFilter:"input",headerSort:true,		headerFilterPlaceholder:"Rewards"},
        	    {title:"Status", field:"id_status", headerSort:false,
					formatter:function(cell, formatterParams, onRendered){
						var arr = cell.getValue();
						var data = arr.split("_");
						if(data[1] == '1'){
							return '<div class="status-toggle d-flex justify-content-between align-items-center" onclick="$(\'#is_delete\').val(1)"><input type="checkbox" class="check status_change" data-id="'+data[0]+'" id="ts'+data[0]+'" data-on="Active"  checked ><label for="ts'+data[0]+'" class="checktoggle">checkbox</label></div>';
						} else {
							return '<div class="status-toggle d-flex justify-content-between align-items-center" onclick="$(\'#is_delete\').val(1)"><input type="checkbox" class="check status_change" data-id="'+data[0]+'" id="ts'+data[0]+'" data-off="InActive"><label for="ts'+data[0]+'" class="checktoggle">checkbox</label></div>';
						}            
					}
				},
				{title:"Action",width:170, field:"id", headerSort:false,
					formatter:function(cell, formatterParams, onRendered) { 
						var edit_url = '{{ route("admin.recharge-plan.edit",":id") }}';
						edit_url = edit_url.replace(':id', cell.getValue());
						
						var delete_url = '{{ url("admin/recharge-plan/delete/:id") }}';
						delete_url = delete_url.replace(':id', cell.getValue());

						/* var delete_url = '{{ route("admin.brand-type.destroy",":id") }}';
						delete_url = delete_url.replace(':id', cell.getValue());*/
						var msg = "Are you sure you want to delete this Plan?";
						var token = '{{ csrf_token() }}'; 
						/* <form action="'+delete_url+'" method="POST" onclick="$(\'#is_delete\').val(1)" onsubmit="return confirm(\''+msg+'\');" style="display: inline-block;"><input type="hidden" name="_method" value="DELETE"><input type="hidden" name="_token" value="'+token+'"><button type="submit" class="btn btn-danger danger-icon-notika btn-reco-mg btn-button-mg waves-effect"> <i class="fa fa-trash"></i></button></form> */
										  
						var link = '<a href="'+edit_url+'" class="btn btn-success text-white"><i class="fa fa-edit"></i></a><a  href="'+delete_url+'" style="margin-left: 5%;" class="btn btn-danger text-white sa-warning" title="Remove" onclick="return confirm(\''+msg+'\');"><i class="fa fa-trash"></i></a> ';
                  
						return link; 
					},
				},
			],
			
		});
		
		$('.status_change').change(function() 
         {      
			var status = $(this).prop('checked') == true ? 1 : 0;
            var recharge_plan_id = $(this).data('id');
            $.ajax({
                type: "GET",
                url: "{{url('admin/changeStatusRechargePlan')}}?status=" + status + "&id=" + recharge_plan_id,
                success: function(res) {
                    if (res) {
                     console.log(res.success);
                    } else {
                       console.log(res);
                    }
                }
            });
		});
		
	</script>
   </body>
</html>