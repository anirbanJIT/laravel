<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
      <meta name="description" content="POS - Bootstrap Admin Template">
      <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
      <meta name="author" content="Dreamguys - Bootstrap Admin Template">
      <meta name="robots" content="noindex, nofollow">
      <title>{{ trans('cruds.bulkcut-plan.title_singular') }} {{ trans('global.list') }}</title>
      
      <link rel="shortcut icon" type="image/x-icon" href="{{ asset('public/Admin/img/favicon.png')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/bootstrap.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/animate.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/dataTables.bootstrap4.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/fontawesome.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/all.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/style.css')}}">
   </head>
   <body>
      <div id="global-loader">
         <div class="whirly-loader"> </div>
      </div>
      <div class="main-wrapper">
        @extends('layouts.header')
     
        @extends('layouts.sidebar')
        
        <div class="page-wrapper">
            <div class="content container-fluid">
               <div class="page-header">
                  <div class="row">
                     <div class="col">
                        <h3 class="page-title">{{ trans('cruds.bulkcut-plan.title_singular') }} {{ trans('global.list') }}</h3>
                        <ul class="breadcrumb">
                           <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">{{ trans('global.dashboard') }}</a></li>
                           <li class="breadcrumb-item active">{{ trans('cruds.bulkcut-plan.title_singular') }} {{ trans('global.list') }}</li>
                        </ul>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-12">
                     <div class="card">
                        <div class="card-header row">
                            <div class="col-lg-6"><h4 class="card-title">{{ trans('cruds.bulkcut-plan.title_singular') }} {{ trans('global.list') }}</h4></div>
                           
                        </div>
                        <div class="card-body">
                           <div class="table-responsive">
                              <table class="table  datanew ">
                                 <thead>
                                    <tr>
                                       <th width="10">
                                       </th>
                                       <th>
                                          Name
                                       </th>
                                       <!-- <th>
                                          Bulk Balance
                                       </th>
                                       <th>
                                          Price (INR)
                                       </th> -->
                                       <th>
                                          Validity (Days)
                                       </th>
                                       <th>
                                          Duration (Hours)
                                       </th>
                                       <th>
                                          Limit
                                       </th>
                                       <!-- <th>
                                          Status
                                       </th> -->
                                       <th>
                                          &nbsp;
                                       </th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    @foreach($bulkcut_plans as $key => $bulkcut_plan)
                                    <tr data-entry-id="{{ $bulkcut_plan->id }}">
                                       <td>
                                       </td>
                                       <td>
                                          {{ $bulkcut_plan->name ?? '' }}
                                       </td>
                                       <!-- <td>
                                          {{ $bulkcut_plan->credit ?? '' }}
                                       </td>
                                       <td>
                                          {{ $bulkcut_plan->price ?? '' }}
                                       </td> -->                         
                                       <td>
                                          {{ $bulkcut_plan->validity ?? '' }}
                                       </td>
                                       <td>
                                          {{ $bulkcut_plan->duration ?? '' }}
                                       </td>
                                       <td>
                                          {{ $bulkcut_plan->cut_limit ?? '' }}
                                       </td>
                                       <!-- <td>
                                          <div class="status-toggle d-flex justify-content-between align-items-center">
                                          <input type="checkbox" class="check status_change" data-id="{{$bulkcut_plan->id}}" id="ts{{$bulkcut_plan->id}}" data-on="Active" data-off="InActive" {{ $bulkcut_plan->status ? 'checked' : '' }}>
                                          <label for="ts{{$bulkcut_plan->id}}" class="checktoggle">checkbox</label>
                                       </td> -->
                                       <td>
                                          <a  href="{{ route('admin.bulkcut-plan.edit', $bulkcut_plan->id) }}" class="btn btn-success text-white" title="Edit"><i class="fa fa-edit"></i></a> 

                                          <a  href="{{ url('admin/bulkcut/delete', $bulkcut_plan->id) }}"  class="btn btn-danger text-white sa-warning" title="Remove" onclick="return confirm('{{ trans('global.areYouSure') }}');"><i class="fa fa-trash"></i></a>
                                       </td>
                                    </tr>
                                    @endforeach
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
    <script src="{{ asset('public/Admin/js/jquery-3.6.0.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/feather.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.slimscroll.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{ asset('public/Admin/plugins/apexchart/apexcharts.min.js')}}"></script>
    <script src="{{ asset('public/Admin/plugins/apexchart/chart-data.js')}}"></script>
    <script src="{{ asset('public/Admin/js/script.js')}}"></script>
    <script type="text/javascript">
        $('.status_change').change(function() {
            var status = $(this).prop('checked') == true ? 1 : 0;
            var id = $(this).data('id');
            $.ajax({
                type: "GET",
                url: "{{url('admin/changeStatusBulkCutPlan')}}?status=" + status + "&id=" + id,
                success: function(res) {
                    if (res) {
                     console.log(res.success);
                    } else {
                       console.log(res);
                    }
                }
            });

        });
    </script>
   </body>
</html>