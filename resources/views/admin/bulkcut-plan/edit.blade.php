<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
      <meta name="description" content="POS - Bootstrap Admin Template">
      <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
      <meta name="author" content="Dreamguys - Bootstrap Admin Template">
      <meta name="robots" content="noindex, nofollow">
      <title>{{ trans('global.edit') }} {{ trans('cruds.bulkcut-plan.title_singular') }}</title>
      
      <link rel="shortcut icon" type="image/x-icon" href="{{ asset('public/Admin/img/favicon.png')}}">
      
      <link rel="stylesheet" href="{{ asset('public/Admin/css/bootstrap.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/select2/css/select2.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/animate.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/dataTables.bootstrap4.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/fontawesome.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/all.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/style.css')}}">
      
   </head>
   <body>
      <div id="global-loader">
         <div class="whirly-loader"> </div>
      </div>
      <div class="main-wrapper">
        @extends('layouts.header')
     
        @extends('layouts.sidebar')
        
        <div class="page-wrapper">
            <div class="content container-fluid">
               <div class="page-header">
                  <div class="row">
                     <div class="col">
                        <h3 class="page-title">{{ trans('global.edit') }} {{ trans('cruds.bulkcut-plan.title_singular') }}</h3>
                        <ul class="breadcrumb">
                           <li class="breadcrumb-item"><a href="{{route('admin.home')}}">{{ trans('global.dashboard') }}</a></li>
                           <li class="breadcrumb-item"><a href="{{ route('admin.bulkcut-plan.index') }}">{{ trans('cruds.bulkcut-plan.title') }}</a></li>
                           <li class="breadcrumb-item active">{{ trans('global.edit') }} {{ trans('cruds.bulkcut-plan.title_singular') }}</li>
                        </ul>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-12">
                     <div class="card">
                        <!-- <div class="card-header row">
                            <div class="col-lg-6"><h4 class="card-title">{{ trans('global.create') }} {{ trans('cruds.permission.title_singular') }}</h4></div>
                            
                        </div> -->
                        <div class="card-body">
                           <form action="{{ route('admin.bulkcut-plan.update', [$bulkcut_plan->id]) }}" method="POST" id="frm_recharge_plan">
                            {{ csrf_field() }}
                           <div class="row">   
                              
                              <div class="form-group col-lg-6">
                                 <label>Name*</label>
                                <input type="text" id="name" name="name" value="{{ $bulkcut_plan->name }}" required class="form-control" placeholder="{{ trans('cruds.bulkcut-plan.fields.name') }}*" >
                                 @if($errors->has('name')) <em class="invalid-feedback"> {{ $errors->first('name') }} </em> @endif
                              </div>

                              <div class="form-group col-lg-6">
                                 <label>{{ trans('cruds.bulkcut-plan.fields.validity') }}</label>
                                 <input type="text" id="validity" name="validity" value="{{ $bulkcut_plan->validity }}" required class="form-control" placeholder="{{ trans('cruds.bulkcut-plan.fields.validity') }}*" >
                                 
                                 @if ($errors->has('validity')) <em class="invalid-feedback"> {{ $errors->first('validity') }} </em> @endif
                              </div>

                              <div class="form-group col-lg-6">
                                 <label>{{ trans('cruds.bulkcut-plan.fields.duration') }}</label>
                                 <input type="number" id="duration" name="duration" value="{{ $bulkcut_plan->duration  }}" class="form-control" placeholder="{{ trans('cruds.bulkcut-plan.fields.duration') }}" min="0">
                                 @if($errors->has('duration')) <em class="invalid-feedback"> {{ $errors->first('duration') }} </em> @endif
                              </div>

                              <div class="form-group col-lg-6">
                                 <label>{{ trans('cruds.bulkcut-plan.fields.cut_limit') }}</label>
                                 <input type="number" id="cut_limit" name="cut_limit" value="{{ $bulkcut_plan->cut_limit }}" class="form-control" placeholder="{{ trans('cruds.bulkcut-plan.fields.cut_limit') }}" min="0">
                                 @if($errors->has('cut_limit')) <em class="invalid-feedback"> {{ $errors->first('cut_limit') }} </em> @endif
                              </div>

                              <div class="">
                                 <button type="submit" class="btn btn-success">Save</button>
                              </div>
                           </div>
                           </form>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
    <script src="{{ asset('public/Admin/js/jquery-3.6.0.min.js')}}"></script>
    <script src="{{ asset('public/Admin/plugins/select2/js/select2.min.js')}}"></script>
    <script src="{{ asset('public/Admin/plugins/select2/js/custom-select.js')}}"></script>
    <script src="{{ asset('public/Admin/js/feather.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.slimscroll.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/script.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js">
    </script>
    <script>
       $(function () {
        $(function(){
    $("#frm_recharge_plan").validate({
        rules: {
            name: {
                required: true,
            },
            // credit: {
            //     required: true,
            // },
            // price: {
            //     required: true,
            // },
            validity: {
                required: true,
            },
            duration: {
                required: true,
            },
            cut_limit: {
                required: true,
            },
      },
       messages: { 
            name: {
                required: "Please enter name"
            },
            // credit: {
            //     required: "Please enter bulk balance"
            // },
            // price: {
            //     required: "Please enter price"
            // },
            validity: {
                required: "Please enter validity",
            },
            duration: {
                required: "Please enter duration",
            },
            cut_limit: {
                required: "Please enter limit",
            }
        },
    });
});
  </script>
   </body>
</html>