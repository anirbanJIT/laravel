@extends('layouts.admin')
@section('content')
<div class="breadcomb-area">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="breadcomb-list">
          <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
              <div class="breadcomb-wp">
                <div class="breadcomb-icon">
                  <i class="notika-icon notika-windows"></i>
                </div>
                <div class="breadcomb-ctn">
                  <h2>Create Model Screen Image</h2>
                  <p>Here <span class="bread-ntd">you can create model screen</span></p>
                </div>
              </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-3">
              <div class="breadcomb-report">
                <a class="btn btn-success notika-btn-success waves-effect" href="{{ url()->previous() }}">
                {{ trans('global.back_to_list') }}
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="form-element-area">
  <div class="container">
    <form action="{{ route('admin.models-screen-image.store') }}" method="POST" enctype="multipart/form-data" >
      @csrf
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="form-element-list">
            <div class="basic-tb-hd">
              <!--<h2>{{ trans('global.create') }} {{ trans('cruds.user.title_singular') }}</h2>
                <p>Text Inputs with different sizes by height and column.</p>-->
            </div>
            <div class="row">
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="nk-int-mk sl-dp-mn mg-t-10">
                  <label>Brand Types*</label>
                </div>
                <div class="bootstrap-select">
                  <select class="selectpicker" name="brand_type_id" id="brand_type_id"  >
                    <option value="">Select Brand Type</option>
                    @foreach($brand_types as $id => $brand_type)
                    <option value="{{ $brand_type->id }}" {{ (in_array($brand_type->id, old('brand_type', [])) ) ? 'selected' : '' }}>{{ $brand_type->name }}</option>
                    @endforeach
                  </select>
                </div>
                @if($errors->has('brand_type_id'))
                <em class="invalid-feedback">
                {{ $errors->first('brand_type_id') }}
                </em>
                @endif
              </div>
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="nk-int-mk sl-dp-mn mg-t-10">
                  <label>Brands*</label>
                </div>
                <div class="bootstrap-select">
                  <select class="selectpicker" name="brand_id" id="brand_id"   >
                    <option value=" ">Select Brand </option>
                    @foreach($brands as $id => $brand)
                    <option value="{{ $brand->id }}" {{ (in_array($brand->id, old('brand', [])) ) ? 'selected' : '' }}>{{ $brand->name }}</option>
                    @endforeach
                  </select>
                </div>
                @if($errors->has('brand_id'))
                <em class="invalid-feedback">
                {{ $errors->first('brand_id') }}
                </em>
                @endif
              </div>
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="nk-int-mk sl-dp-mn mg-t-10">
                  <label>Models*</label>
                </div>
                <div class="bootstrap-select">
                  <select class="selectpicker" name="model_id" id="model_id"   >
                    <option value=" ">Select Model </option>
                    @foreach($brandModel as $id => $models)
                    <option value="{{ $models->id }}" {{ (in_array($models->id, old('models', [])) ) ? 'selected' : '' }}>{{ $models->name }}</option>
                    @endforeach
                  </select>
                </div>
                @if($errors->has('model_id'))
                <em class="invalid-feedback">
                {{ $errors->first('model_id') }}
                </em>
                @endif
              </div>
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-example-int mg-t-15">
                  <div class="form-group">
                    <div class="nk-int-st">
                      <input type="text" class="form-control input-sm" placeholder="Meta Title" value="{{ old('meta_title', isset($modelScreen) ? $modelScreen->meta_title : '') }}" id="meta_title" name="meta_title">
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-example-int mg-t-15">
                  <div class="form-group">
                    <div class="nk-int-st">
                      <textarea class="form-control" placeholder="Meta Keywords" id="meta_keywords" name="meta_keywords">{{ old('meta_keywords', isset($modelScreen) ? $modelScreen->meta_keywords : '') }}</textarea>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-example-int mg-t-15">
                  <div class="form-group">
                    <div class="nk-int-st textarea">
                      <textarea class="form-control" placeholder="Meta Description" id="meta_description" name="meta_description">{{ old('meta_description', isset($modelScreen) ? $modelScreen->meta_description : '') }}</textarea>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="nk-int-mk sl-dp-mn mg-t-10">
                </div>
                <div class="bootstrap-select">
                  <select class="selectpicker" name="status" id="status"  required >
                    <option>Select Status</option>
                    <option value="0">Inactive</option>
                    <option value="1" selected>Active</option>
                  </select>
                </div>
              </div>
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">&nbsp;</div>
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                Upload .blt file 
                <input type="file" accept=".blt" name="blt_file" class="form-control">
              </div>
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">&nbsp;</div>
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                Upload image file 
                <div class="input-group hdtuto control-group lst increment" >
                  <input type="file" name="filenames[]" multiple class="myfrm form-control" placeholder="Upload image file ">
                  <div class="input-group-btn"> 
                    <button class="btn btn-success btn-success1" type="button"><i class="fldemo glyphicon glyphicon-plus"></i>Add</button>
                  </div>
                </div>
                <div class="clone hide">
                  <div class="hdtuto control-group lst input-group" style="margin-top:10px">
                    <input type="file" name="filenames[]" multiple class="myfrm form-control">
                    <div class="input-group-btn"> 
                      <button class="btn btn-danger btn-danger1" type="button"><i class="fldemo glyphicon glyphicon-remove"></i> Remove</button>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">&nbsp;</div>
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <input type="submit" class="btn btn-success notika-btn-success waves-effect" value="{{ trans('global.save') }}" >
              </div>
            </div>
          </div>
        </div>
    </form>
    </div>  
  </div>
</div>

			<script type="text/javascript">	
$(document).ready(function(){
   $("#brand_type_id").change(function() {
    var brand_type = $(this).find(":selected").val();
    var request = $.ajax({
        type: 'GET',
        url: "{{url('admin/get-brand-type-list')}}?brand_type="+brand_type,
    });
    request.done(function(data){
        $("#brand_id").empty();
        if(data){
			$("#brand_id").append('<option value="">Select Brand</option>');
                $.each(data,function(key,value){
                 $("#brand_id").append('<option value="'+value['id']+'">'+value['name']+'</option>');
                });
            }
        $('#brand_id').selectpicker('refresh');
    });
	
});

$("#brand_id").change(function() {
    var brand = $(this).find(":selected").val();
    var request = $.ajax({
        type: 'GET',
        url: "{{url('admin/get-brand-list')}}?brand="+brand,
    });
	
    request.done(function(data){
        $("#model_id").empty();
        if(data){
			//alert(data);
			$("#model_id").append('<option value="">Select Model</option>');
                $.each(data,function(key,value){
                 $("#model_id").append('<option value="'+value['id']+'">'+value['name']+'</option>');
                });
            }
        $('#model_id').selectpicker('refresh');
    });
	
});




    $(document).ready(function() {

      $(".btn-success1").click(function(){ 

          var lsthmtl = $(".clone").html();

          $(".increment").after(lsthmtl);

      });

      $("body").on("click",".btn-danger1",function(){ 
	  

          $(this).parents(".hdtuto").remove();

      });

    });


 
   });
   
   
   
    
    
   </script>	
			@endsection
	