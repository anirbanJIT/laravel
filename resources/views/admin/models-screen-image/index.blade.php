<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
      <meta name="description" content="POS - Bootstrap Admin Template">
      <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
      <meta name="author" content="Dreamguys - Bootstrap Admin Template">
      <meta name="robots" content="noindex, nofollow">
      <title>Models List</title>
      
      <link rel="shortcut icon" type="image/x-icon" href="{{ asset('public/Admin/img/favicon.png')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/bootstrap.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/animate.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/dataTables.bootstrap4.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/fontawesome.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/all.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/style.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/select2/css/select2.min.css')}}">
   </head>
   <body>
      <!-- <div id="global-loader">
         <div class="whirly-loader"> </div>
      </div> -->
      <div class="main-wrapper">
        @extends('layouts.header')
     
        @extends('layouts.sidebar')
        
        <div class="page-wrapper">
            <div class="content container-fluid">
               <div class="page-header">
                  <div class="row">
                     <div class="col">
                        <h3 class="page-title">Model Screen Image List</h3>
                        <ul class="breadcrumb">
                           <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">{{ trans('global.dashboard') }}</a></li>
                           <li class="breadcrumb-item active">Model Screen Image List</li>
                        </ul>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-12">
                     <div class="card">
                        <div class="card-header row">
                            <div class="col-lg-6"><h4 class="card-title">Model Screen Image List</h4></div>
                            <div class="col-lg-6">
								<a href="{{ route('admin.models-screen-image.create') }}" class="btn btn-success" style="float: right;margin-left: 2%;">+ Add model Screen Image</a>
								<a id="openmodel" class="btn btn-success" style="float: right;">Rearrange</a>
							</div>
                        </div>
                        <div class="card-body">
                           <div class="table-responsive">
                              <table class="table  datanew ">
                                 <thead>
                                    <tr role="row" class="heading">
                                        <th width="10"></th>
                                        <th width="110"> Model Screen {{ trans('cruds.brand_type.fields.id') }} </th>
                                        <th width="200"> {{ trans('cruds.brand_type.title_singular') }} {{ trans('cruds.brands.fields.name') }} </th>
                                        <th width="200"> {{ trans('cruds.brands.title_singular') }} {{ trans('cruds.brands.fields.name') }} </th>                                        
                                        <th width="200"> {{ trans('cruds.models.title_singular') }} {{ trans('cruds.models.fields.name') }} </th>
                                        <th width="200"> {{ trans('cruds.sub_models.title_singular') }} {{ trans('cruds.models.fields.name') }} </th>
                                        <th width="200"> {{ trans('cruds.models.fields.name') }} </th>
                                        <th width="5%"> Total </th>
                                        <th width="130"> {{ trans('cruds.brand_type.fields.order') }} </th>
                                        <th width="15%"> {{ trans('cruds.brand_type.fields.date') }} </th>
                                        <th width="15%"> {{ trans('cruds.brand_type.fields.status') }} </th>
                                        <th width="12%"> {{ trans('cruds.brand_type.fields.actions') }} </th>
                                    </tr>

                                 </thead>
                                 <tbody>
                                     @foreach($brandModelScreen as $key => $modelScreen)
                                    <tr data-entry-id="{{ $modelScreen->id }}">
                                        <td>
                                        </td>
                                        <td>
                                            {{ $modelScreen->id ?? '' }}
                                        </td>
                                        <td class="word-break ">
                                            {{ $modelScreen->brandsTypes->name ?? '' }}
                                        </td>
                                        <td class="word-break ">
                                            {{ $modelScreen->brands->name ?? '' }}
                                        </td>
                                        <td class="word-break ">
                                            {{ $modelScreen->brandsModel->name ?? '' }}
                                        </td>
                                        <td class="word-break ">
                                            {{ $modelScreen->brandSubModel->name ?? '' }}
                                        </td>
                                        <td class="word-break ">
                                            @if(isset($modelScreen->name))
                                                @php
                                                $nameArray = json_decode($modelScreen->name,TRUE);
                                                $totalModelScreen = 0;
                                                if( $nameArray === NULL ){
                                                    $names = $data->name ?? null;
                                                }else{
                                                    $totalModelScreen = count(array_filter($nameArray));
                                                    $names =  implode(", ", $nameArray);
                                                    $names = trim($names, ", "); 
                                                }
                                                @endphp
                                                {{ $names }}

                                            @endif
                                        </td>
                                        <td class="word-break ">{{ $totalModelScreen }}</td>
                                        <td>
                                            {{ $modelScreen->order_number ?? $modelScreen->id }}
                                        </td>
                                        <td data-order="{{ \Carbon\Carbon::parse($modelScreen->created_at)->format('Y-m-d') ?? '' }}">
                                            {{ \Carbon\Carbon::parse($modelScreen->created_at)->format('d/m/Y') ?? '' }}
                                        </td>
                                         <td data-filter="{{ $modelScreen->status ?? '' }}" data-search="{{ $modelScreen->status ?? '' }}">
                                          <div class="status-toggle d-flex justify-content-between align-items-center">
                                             <input type="checkbox" class="check status_change" data-id="{{$modelScreen->id}}" id="ts{{$modelScreen->id}}" data-on="Active" data-off="InActive" {{ $modelScreen->status ? 'checked' : '' }}>
                                             <label for="ts{{$modelScreen->id}}" class="checktoggle">checkbox</label>
                                          </div>
                                        </td>
                                        <td>
                                          <a  href="{{ route('admin.models-screen-image.show', $modelScreen->id) }}" class="btn btn-primary text-white" title="View"><i class="fa fa-eye"></i></a>

                                          <a  href="{{ route('admin.models-screen-image.edit', $modelScreen->id) }}" class="btn btn-info text-white" title="Edit"><i class="fa fa-edit"></i></a>
                                          
                                          <a href="{{ url('admin/deleteModelScreen', $modelScreen->id) }}" class="btn btn-danger text-white" title="Delete" onclick="return confirm('{{ trans('global.areYouSure') }}');"><i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                   @endforeach
                                    
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
    <script src="{{ asset('public/Admin/js/jquery-3.6.0.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/feather.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.slimscroll.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{ asset('public/Admin/plugins/apexchart/apexcharts.min.js')}}"></script>
    <script src="{{ asset('public/Admin/plugins/apexchart/chart-data.js')}}"></script>
    <script src="{{ asset('public/Admin/js/script.js')}}"></script>
     <script src="{{ asset('public/Admin/plugins/select2/js/select2.min.js')}}"></script>
      <script src="{{ asset('public/Admin/plugins/select2/js/custom-select.js')}}"></script>
      <script type="text/javascript">
         $('.status_change').change(function() {     
        var status = $(this).prop('checked') == true ? 1 : 0; 
        var modelScreen_id = $(this).data('id'); 
         $.ajax({
           type:"GET",
           url:"{{url('admin/changeStatusModelScreen')}}?status="+status+"&modelScreen_id="+modelScreen_id,
           success:function(res){               
            if(res){
                console.log(res.success);
            }
           }
        });
    });

         // $('.sa-warning').on('click', function (e) 
         // {
         //    event.preventDefault();
         //    const url =  $(this).attr('href');
         //    swal({
         //       title: "Are you sure?",  
         //       text: "Once Deleted. You will also able to recover this again!",  
         //       type: "warning",  
         //       showCancelButton: true,  
         //       confirmButtonText: "Yes, delete it!",
         //    }).then(function(value) {
         //       if (value) {
         //           window.location.href = url;
         //       }
         //    });
         // });    
   </script>
   </body>
</html>