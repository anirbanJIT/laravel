<!DOCTYPE html>
<html lang="en">
	<head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
      <meta name="description" content="POS - Bootstrap Admin Template">
      <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
      <meta name="author" content="Dreamguys - Bootstrap Admin Template">
      <meta name="robots" content="noindex, nofollow">
      <title>Show Model Screen Image</title>
      
      <link rel="shortcut icon" type="image/x-icon" href="{{ asset('public/Admin/img/favicon.png')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/bootstrap.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/animate.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/dataTables.bootstrap4.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/fontawesome.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/all.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/style.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/select2/css/select2.min.css')}}">
	</head>
	<body>
		<div class="main-wrapper">
			@extends('layouts.header')
    		@extends('layouts.sidebar')
        
			<div class="page-wrapper">
				<div class="content container-fluid">
					<div class="page-header">
						<div class="row">
							<div class="col">
								<h3 class="page-title">Show Model Screen Image</h3>
								<ul class="breadcrumb">
									<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">{{ trans('global.dashboard') }}</a></li>
									<li class="breadcrumb-item">
									@if($brandModelScreenImage->sub_model_id)
										<a href="{{ route('admin.models-screen-image-list','id='.$brandModelScreenImage->sub_model_id) }}">Model Screen Images</a>
									@else
										<a href="{{ route('admin.brand-type.index') }}">Model Screen Images</a>
									@endif
									</li>
									<li class="breadcrumb-item active">Show Model Screen Image</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<div class="card">
								<div class="card-header row">
									<div class="col-lg-6"><h4 class="card-title">Show Model Screen Image</h4></div>
									<div class="col-lg-6">
									@if($brandModelScreenImage->sub_model_id)
										<a href="{{ route('admin.models-screen-image-list','id='.$brandModelScreenImage->sub_model_id) }}" class="btn btn-success" style="float: right;">{{ trans('global.back_to_list') }}</a>
									@else
										<a href="{{ route('admin.brand-type.index') }}" class="btn btn-success" style="float: right;">{{ trans('global.back_to_list') }}</a>
									@endif	
									</div>
								</div>
								<div class="card-body">
									<div class="table-responsive">
										<table class="table  datanew ">
											<tbody>
												<tr>
													<th>Category</th>
													<td>{{ $brandModelScreenImage->brandsTypes->name }}</td>
												</tr>
												<tr>
													<th>Brand</th>
													<td>{{ $brandModelScreenImage->brands->name }}</td>
												</tr>
												<tr>
													<th>Model</th>
													<td>{{ $brandModelScreenImage->brandsModel->name }}</td>
												</tr>
												<tr>
													<th>SubModel</th>
													<td>{{ $brandModelScreenImage->brandSubModel->name }}</td>
												</tr>
												<tr>
													<th> Name</th>
													<td>{{ $brandModelScreenImage->name }}</td>
												</tr>
												<tr>
													<th>Slug</th>
													<td>{{ $brandModelScreenImage->slug }}</td>
												</tr>
												<tr>
													<th>Status</th>
													<td>
														@if($brandModelScreenImage->status == 1)
															{{ 'Active' }}
														@else
															{{ 'Inactive' }}
														@endif
													</td>
												</tr>
												
												<tr>
													<th>Meta Title</th>
													<td>{{ $brandModelScreenImage->meta_title }}</td>
												</tr>
												<tr>
													<th>Meta Keywords</th>
													<td>{{ $brandModelScreenImage->meta_keywords }}</td>
												</tr>
												<tr>
													<th>Meta Description</th>
													<td>{{ $brandModelScreenImage->meta_description }}</td>
												</tr>
												<tr>
													<th>Image </th>
													<td>                                    
													@if(isset($brandModelScreenImage) && $brandModelScreenImage->image )
														<a href="{{ asset('Admin/images/models-screen-image/'.$brandModelScreenImage->image)}}" data-lightbox="lightbox-1"> 
															<img src="{{ asset('Admin/images/models-screen-image/'.$brandModelScreenImage->image) }}" style="width:200px;height:200px;">
														</a>
													@endif
													</td>
												</tr>
                                    		</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<script src="{{ asset('public/Admin/js/jquery-3.6.0.min.js')}}"></script>
		<script src="{{ asset('public/Admin/js/feather.min.js')}}"></script>
		<script src="{{ asset('public/Admin/js/jquery.slimscroll.min.js')}}"></script>
		<script src="{{ asset('public/Admin/js/jquery.dataTables.min.js')}}"></script>
		<script src="{{ asset('public/Admin/js/dataTables.bootstrap4.min.js')}}"></script>
		<script src="{{ asset('public/Admin/js/bootstrap.bundle.min.js')}}"></script>
		<script src="{{ asset('public/Admin/plugins/apexchart/apexcharts.min.js')}}"></script>
		<script src="{{ asset('public/Admin/plugins/apexchart/chart-data.js')}}"></script>
		<script src="{{ asset('public/Admin/js/script.js')}}"></script>
		<script src="{{ asset('public/Admin/plugins/select2/js/select2.min.js')}}"></script>
		<script src="{{ asset('public/Admin/plugins/select2/js/custom-select.js')}}"></script>
    </body>
</html>