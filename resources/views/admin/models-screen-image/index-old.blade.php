@extends('layouts.admin')
@section('content')

<div class="breadcomb-area">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="breadcomb-list">
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<div class="breadcomb-wp">
									<div class="breadcomb-icon">
										<i class="notika-icon notika-windows"></i>
									</div>
									<div class="breadcomb-ctn">
										<h2>Model Screen Image List</h2>
										<p>Show <span class="bread-ntd">all list</span></p>
									</div>
								</div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-3">
								<div class="breadcomb-report">
								
									 <a class="btn btn-success notika-btn-success waves-effect" href="{{ route('admin.models-screen-image.create') }}">
                Add model Screen Image
            </a>
									
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
 <!-- Data Table area Start-->
    <div class="data-table-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="data-table-list">
                        <!--<div class="basic-tb-hd">
                            <h2>Basic Example</h2>
                            <p>It's just that simple. Turn your simple table into a sophisticated data table and offer your users a nice experience and great features without any effort.</p>
                        </div>-->
                        <div class="table-responsive">
                            <table id="data-table-basic" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th width="10">

                        </th>
                        <th>
                            Category
                        </th>
                        <th>
                            Brand
                        </th>
						<th>
                            Model
                        </th>
                       
						
                        <th>
                            Status
                        </th>
                        <th>
                            &nbsp;
                        </th>
                                    </tr>
                                </thead>
                                <tbody>
								
                                    @foreach($brandModelScreen as $key => $modelScreen)
                        <tr data-entry-id="{{ $modelScreen->id }}">
                            <td>

                            </td>
                            <td>
                                {{ $modelScreen->brandsTypes->name ?? '' }}
                            </td>
                            <td>
                                {{ $modelScreen->brands->name ?? '' }}
                            </td>
							<td>
                                {{ $modelScreen->brandsModel->name ?? '' }}
                            </td>
                           
							
                            <td>
                             
							  <div class="nk-toggle-switch" data-ts-color="green">
                                               
                                                <input class="status_change" data-id="{{$modelScreen->id}}" id="ts{{$modelScreen->id}}" type="checkbox" hidden="hidden" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" data-off="InActive" {{ $modelScreen->status ? 'checked' : '' }}>
                                                <label for="ts{{$modelScreen->id}}" class="ts-helper"></label>
                                            </div>
                            </td>
                            <td>
                               
                               <a  href="{{ route('admin.models-screen-image.show', $modelScreen->id) }}"><button class="btn btn-success success-icon-notika btn-reco-mg btn-button-mg waves-effect" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="View"><i class="notika-icon notika-menus"></i></button></a>
								
								
								<a  href="{{ route('admin.models-screen-image.edit', $modelScreen->id) }}"><button class="btn btn-info info-icon-notika btn-reco-mg btn-button-mg waves-effect" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Edit"><i class="notika-icon notika-edit"></i></button></a>
								

                               <a href="{{ url('admin/deleteModelScreen', $modelScreen->id) }}" class="sa-warning" >
                                    <button class="btn btn-danger danger-icon-notika btn-reco-mg btn-button-mg waves-effect" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Delete"> <i class="notika-icon notika-close"></i>
									</button>
								</a>	
                               
								
                            </td>
                                    </tr>
                                   @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th width="10">

                        </th>
                        <th>
                            Brand Type
                        </th>
                        <th>
                            Brand
                        </th>
						<th>
                            Model
                        </th>
                       
						
                        <th>
                            Status
                        </th>
                        <th>
                            &nbsp;
                        </th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	
	<script>
    $(function () {
//	
		
	
	  $('.sa-warning').on('click', function (e) {
   event.preventDefault();
   const url =  $(this).attr('href');
  
   swal({
       title: "Are you sure?",  
text: "Once Deleted. You will also able to recover this again!",  
type: "warning",  
showCancelButton: true,  
confirmButtonText: "Yes, delete it!",
   }).then(function(value) {
       if (value) {
           window.location.href = url;
		  
       }
   });
});
	
	
	$('.status_change').change(function() {
		
        var status = $(this).prop('checked') == true ? 1 : 0; 
        var modelScreen_id = $(this).data('id'); 
		 $.ajax({
           type:"GET",
           url:"{{url('admin/changeStatusModelScreen')}}?status="+status+"&modelScreen_id="+modelScreen_id,
           success:function(res){               
            if(res){
				//alert(res);
                console.log(res.success);
           
            }else{
               //alert("nnnn");
            }
           }
        });
	
    });	
	});
	</script>
@endsection

