@extends('layouts.admin')

@section('content')





	<div class="breadcomb-area">

		<div class="container">

			<div class="row">

				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

					<div class="breadcomb-list">

						<div class="row">

							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">

								<div class="breadcomb-wp">

									<div class="breadcomb-icon">

										<i class="notika-icon notika-windows"></i>

									</div>

									<div class="breadcomb-ctn">

										<h2>Show Model Screen Image</h2>

										<p>Show <span class="bread-ntd">particular model screen image</span></p>

									</div>

								</div>

							</div>

							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-3">

								<div class="breadcomb-report">

								

								



			<a class="btn btn-success notika-btn-success waves-effect" href="{{ route('admin.models-screen-image.index') }}">

                {{ trans('global.back_to_list') }}

            </a>

									

								</div>

							</div>

						</div>

					</div>

				</div>

			</div>

		</div>

	</div>

	





   <div class="data-table-area">

        <div class="container">

            <div class="row">

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <div class="data-table-list">

                        <!--<div class="basic-tb-hd">

                            <h2>Basic Example</h2>

                            <p>It's just that simple. Turn your simple table into a sophisticated data table and offer your users a nice experience and great features without any effort.</p>

                        </div>-->

                        <div class="table-responsive">

                            <table id="data-table-basic" class="table table-striped">

                <tbody>

                    <tr>

                        <th width="20%">

                            Category

                        </th>

                        <td>

                            {{ $brandModel->brandsTypes->name ?? null }}

                        </td>

                    </tr>

					 <tr>

                        <th>

                            Brand

                        </th>

                        <td>

                            {{ $brandModel->brands->name ?? null }}

                        </td>

                    </tr>

                    <tr>

                        <th>

                            Model

                        </th>

                        <td>

                            {{ $brandModel->brandsModel->name ?? null }}

                        </td>

                    </tr>

                    <tr>

                        <th>

                            Sub Model

                        </th>

                        <td>

                            {{ $brandModel->brandSubModel->name ?? null }}

                        </td>

                    </tr>

					 <tr>

                        <th>

                            Status

                        </th>

                        <td>

                            {{ $brandModel->status ?? null }}

                        </td>

                    </tr><!-- 
                    <tr>

                        <th>

                            Name

                        </th>

                        <td>

                            {{ $brandModel->name ?? null }}

                        </td>

                    </tr> -->

					 <tr>

                        <th>

                            Meta Title

                        </th>

                        <td>

                            {{ $brandModel->meta_title }}

                        </td>

                    </tr>

					 <tr>

                        <th>

                            Meta Keywords

                        </th>

                        <td>

                            {{ $brandModel->meta_keywords }}

                        </td>

                    </tr>
                    <!-- <tr>
                        <th>
                            Blt file
                        </th>
                        <td>
                        @if(!empty($brandModel->blt_file))
                            @php $myArray = json_decode($brandModel->blt_file);  @endphp
                            @if( $myArray === NULL )
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <a href="{{ asset('images/models-screen-image/'.$brandModel->blt_file) }}" target="download">{{$brandModel->blt_file}} file</a> |
                                <a href="{{ url('admin/deleteModelScreenBltFiles/?id='.$brandModel->id.'&blt_file='.$brandModel->blt_file) }}" class="delete-blt"> Delete</a>
                            </div>
                            @else
                                @foreach($myArray as $key => $value)
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <a href="{{ asset('images/models-screen-image/'.$value) }}" target="download">{{$value}}</a> |
                                    <a href="{{ url('admin/deleteModelScreenBltFiles/?id='.$brandModel->id.'&blt_file='.$value) }}"  class="delete-blt"> Delete</a>
                                    <br/>
                                </div>
                                @endforeach
                            @endif
                        @endif 
                        </td>
                    </tr> -->

					<tr>

                        <th>

                            Images and Blt

							<form method="post" action="{{ url('admin/addMoreImage')}}" id="myForm" enctype="multipart/form-data">

							<input type="hidden" name="id" value="{{$brandModel->id}}">

							@csrf

							<div class="image-crp-img" style="margin-top:14px;">

                                           

                                           <!--  <div class="btn-group images-cropper-pro">

                                                <label title="Upload image file" for="inputImage" class="btn btn-primary img-cropper-cp" >

														<input type="file"  name="filenames" multiple id="inputImage" class="hide"> <i class="notika-icon notika-up-arrow"></i>

													</label>

                                               

                                            </div>
 -->
                                        </div></form>

                        </th>

                        <td>

	

						@if($brandModel->image)

						@php 
                            $nameArray = json_decode($brandModel->name, True);
                            $myArray = json_decode($brandModel->image, True); 
                            $bltArray = json_decode($brandModel->blt_file, True); 
                            $widthArray = json_decode($brandModel->width, True); 
                            $heightArray = json_decode($brandModel->height, True); 
                        @endphp

						@foreach($myArray as $key => $value)						

						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">

						<div class="animation-single-int">

						<div class="animation-img mg-b-15">
                            <a href="{{ asset('images/models-screen-image/'.$value) }}" data-lightbox="lightbox-{{$key}}"> 
                                <img src="{{ asset('images/models-screen-image/'.$value) }}" class="animate-eleven" style="width:200px;height:200px; margin: 5px 5px 5px 0;">
                            </a><br/>
                            @isset($nameArray[$key])
                                {{$nameArray[$key] ?? null}} <br/> 
                            @endisset
                            @isset($bltArray[$key])
							<a href="{{ asset('images/models-screen-image/'.$bltArray[$key] ?? null) }}" target="download">{{$bltArray[$key] ?? null}}</a>
                            @endisset
                            <br/>
                            {{ isset($widthArray[$key]) ? $widthArray[$key] : null }} x {{ isset($heightArray[$key]) ? $heightArray[$key] : null }}
							</div>
                            <div ><a href="{{ url('admin/deleteModelScreenImage/?id='.$brandModel->id.'&image='.$value) }}" class="sa-warning" >

                                    <button class="btn btn-danger danger-icon-notika btn-reco-mg btn-button-mg waves-effect"> <i class="notika-icon notika-close"></i></button>

								</a></div>	</div>
                            </div>

							@endforeach

						@endif

                        </td>

                    </tr>

                    

                </tbody>

            </table>

           

        </div>





    </div>

</div>

</div>

</div>

</div>

</div>

<script>

    $(function () {

//for image uplad button		

	$('#inputImage').change(function() {

  $('#myForm').submit();

});	

		

		

	

	  $('.sa-warning').on('click', function (e) {

   event.preventDefault();

   const url =  $(this).attr('href');

  

   swal({

       title: "Are you sure?",  

text: "Once Deleted. You will also able to recover this again!",  

type: "warning",  

showCancelButton: true,  

confirmButtonText: "Yes, delete it!",

   }).then(function(value) {

       if (value) {

           window.location.href = url;

		  

       }

   });

});
//blt deleted code
 $('.delete-blt').on('click', function (e) {

   event.preventDefault();

   const urls =  $(this).attr('href');
console.log(urls);
  

   swal({

       title: "Are you sure?",  

text: "Once Deleted. You will also able to recover this again!",  

type: "warning",  

showCancelButton: true,  

confirmButtonText: "Yes, delete it!",

   }).then(function(value) {

       if (value) {

           window.location.href = urls;

          

       }

   });

});
//blt deleted code end
	});



	</script>

@endsection