@extends('layouts.admin')
@section('content')
<div class="breadcomb-area">
   <div class="container">
      <div class="row">
         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="breadcomb-list">
               <div class="row">
                  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                     <div class="breadcomb-wp">
                        <div class="breadcomb-icon">
                           <i class="notika-icon notika-windows"></i>
                        </div>
                        <div class="breadcomb-ctn">
                           <h2>Create Model Screen Image</h2>
                           <p>Here <span class="bread-ntd">you can create model screen</span></p>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-3">
                     <div class="breadcomb-report">
                        <a class="btn btn-success notika-btn-success waves-effect" href="{{ url()->previous() }}">
                        {{ trans('global.back_to_list') }}
                        </a>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="form-element-area">
   <div class="container">
      <form action="{{ route('admin.models-screen-image.store') }}" method="POST" enctype="multipart/form-data" >
         @csrf
         <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
               <div class="form-element-list">
                  <div class="basic-tb-hd">
                     <!--<h2>{{ trans('global.create') }} {{ trans('cruds.user.title_singular') }}</h2>
                        <p>Text Inputs with different sizes by height and column.</p>-->
                  </div>
                  <div class="row">
                     <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="nk-int-mk sl-dp-mn mg-t-10">
                           <label>Category*</label>
                        </div>
                        <div class="bootstrap-select">
                           <select class="selectpicker" name="brand_type_id" id="brand_type_id"  >
                              <option value="">Select Category</option>
                              @foreach($brand_types as $id => $brand_type)
                              <option value="{{ $brand_type->id }}" {{ (in_array($brand_type->id, old('brand_type', [])) ) ? 'selected' : '' }}>{{ $brand_type->name }}</option>
                              @endforeach
                           </select>
                        </div>
                        @if($errors->has('brand_type_id'))
                        <em class="invalid-feedback">
                        {{ $errors->first('brand_type_id') }}
                        </em>
                        @endif
                     </div>
                     <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="nk-int-mk sl-dp-mn mg-t-10">
                           <label>Brands*</label>
                        </div>
                        <div class="bootstrap-select">
                           <select class="selectpicker" name="brand_id" id="brand_id"   >
                              <option value=" ">Select Brand </option>
                              @foreach($brands as $id => $brand)
                              <option value="{{ $brand->id }}" {{ (in_array($brand->id, old('brand', [])) ) ? 'selected' : '' }}>{{ $brand->name }}</option>
                              @endforeach
                           </select>
                        </div>
                        @if($errors->has('brand_id'))
                        <em class="invalid-feedback">
                        {{ $errors->first('brand_id') }}
                        </em>
                        @endif
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="nk-int-mk sl-dp-mn mg-t-10">
                           <label>Models*</label>
                        </div>
                        <div class="bootstrap-select">
                           <select class="selectpicker" name="model_id" id="model_id"   >
                              <option value=" ">Select Model </option>
                              @foreach($brandModel as $id => $models)
                              <option value="{{ $models->id }}" {{ (in_array($models->id, old('models', [])) ) ? 'selected' : '' }}>{{ $models->name }}</option>
                              @endforeach
                           </select>
                        </div>
                        @if($errors->has('model_id'))
                        <em class="invalid-feedback">
                        {{ $errors->first('model_id') }}
                        </em>
                        @endif
                     </div>
                     <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="nk-int-mk sl-dp-mn mg-t-10">
                           <label>Sub Models*</label>
                        </div>
                        <div class="bootstrap-select">
                           <select class="selectpicker" name="sub_model_id" id="sub_model_id"   >
                              <option value=" ">Select Sub Model </option>
                              @foreach($brandSubModel as $id => $submodels)
                              <option value="{{ $submodels->id }}" {{ (in_array($submodels->id, old('submodels', [])) ) ? 'selected' : '' }}>{{ $submodels->name }}</option>
                              @endforeach
                           </select>
                        </div>
                        @if($errors->has('sub_model_id'))
                        <em class="invalid-feedback">
                        {{ $errors->first('sub_model_id') }}
                        </em>
                        @endif
                     </div>
                  </div>
                  <!-- <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                       <div class="form-example-int mg-t-15">
                         <div class="form-group">
                           <div class="nk-int-st">
                             <input type="text" class="form-control input-sm" placeholder="Meta Title" value="{{ old('meta_title', isset($modelScreen) ? $modelScreen->meta_title : '') }}" id="meta_title" name="meta_title">
                           </div>
                         </div>
                       </div>
                     </div>
                     </div>
                     <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                       <div class="form-example-int mg-t-15">
                         <div class="form-group">
                           <div class="nk-int-st">
                             <textarea class="form-control" placeholder="Meta Keywords" id="meta_keywords" name="meta_keywords">{{ old('meta_keywords', isset($modelScreen) ? $modelScreen->meta_keywords : '') }}</textarea>
                           </div>
                         </div>
                       </div>
                     </div>
                     </div>
                     <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                       <div class="form-example-int mg-t-15">
                         <div class="form-group">
                           <div class="nk-int-st textarea">
                             <textarea class="form-control" placeholder="Meta Description" id="meta_description" name="meta_description">{{ old('meta_description', isset($modelScreen) ? $modelScreen->meta_description : '') }}</textarea>
                           </div>
                         </div>
                       </div>
                     </div>
                     </div> -->
                  <div class="row">
                     <!-- <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <div class="nk-int-mk sl-dp-mn mg-t-10">
                            <label>Name*</label>
                        </div>
                        <div class="form-group"> 
                            <div class="nk-int-st">
                                <input type="text" class="form-control" value="{{ old('name', isset($submodels) ? $submodels->name : '') }}" placeholder="Model Name" id="name" name="name">
                            </div>
                            @if($errors->has('name'))
                            <em class="invalid-feedback">
                                {{ $errors->first('name') }}
                            </em>
                            @endif
                        </div> 
                        </div> -->
                     <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <div class="nk-int-mk sl-dp-mn mg-t-10">
                           <label>Status*</label>
                        </div>
                        <div class="bootstrap-select">
                           <select class="selectpicker" name="status" id="status"  required >
                              <option>Select Status</option>
                              <option value="0" {{ (isset($submodels) && $submodels->status ? $submodels->status : old('status')) == 0 ? 'selected' : '' }} >Inactive</option>
                              <option value="1" {{ (isset($submodels) && $submodels->status ? $submodels->status : old('status')) == 1 ? 'selected' : 'selected' }}>Active</option>
                           </select>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mg-t-10">
                        <label class="col-lg-12 col-md-12 col-sm-12 col-xs-12">Upload .Blt and Image file </label>
                        <div class="input-group hd-blt control-group lst increment-blt" >
                           <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 mg-t-10" style="padding:0">
                              <label class="col-lg-12 col-md-12 col-sm-12 col-xs-12">Screen Name</label>
                              <input type="text" name="name[]" class="myfrm form-control" placeholder="Screen Name" required>
                           </div>
                           <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 mg-t-10" style="padding:0">
                              <label class="col-lg-12 col-md-12 col-sm-12 col-xs-12">Upload blt, plt file</label>
                              <input type="file" name="blt_file[]" class="myfrm form-control w-25" placeholder="Upload blt file" accept=".blt,.aes,.des,.plt" required title="Upload blt, aes, des file" placeholder="Upload blt, plt file">
                           </div>
                           <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 mg-t-10" style="padding:0">
                              <label class="col-lg-12 col-md-12 col-sm-12 col-xs-12">Upload image file</label>
                              <input type="file" name="filenames[]" class="myfrm form-control w-25" placeholder="Upload image file " accept="image/*" required title="Upload image file ">
                           </div>
                           <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12 mg-t-10" style="padding:0">
                              <label class="col-lg-12 col-md-12 col-sm-12 col-xs-12">Width</label>
                              <input type="text" name="width[]" class="myfrm form-control" placeholder="Width" required>
                           </div>
                           <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12 mg-t-10" style="padding:0">
                              <label class="col-lg-12 col-md-12 col-sm-12 col-xs-12">Height</label>
                              <input type="text" name="height[]" class="myfrm form-control" placeholder="Height" required>
                           </div>
                           <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12 mg-t-10" style="padding:0">
                              <label class="col-lg-12 col-md-12 col-sm-12 col-xs-12">&nbsp;</label>
                              <button class="btn btn-success btn-success-blt btn-block" type="button"><i class="fldemo glyphicon glyphicon-plus"></i>Add</button>
                           </div>
                        </div>
                        <div class="clone hide">
                           <div class="hd-blt control-group lst input-group" style="margin-top:10px">
                              <!-- <label class="col-lg-12 col-md-12 col-sm-12 col-xs-12">Upload .Blt and Image file </label> -->
                              <input type="text" name="name[]" class="myfrm form-control w-50" placeholder="Screen Name" style="width: 28%">
                              <input type="file" name="blt_file[]" class="myfrm form-control w-50" placeholder="Upload blt file " accept=".blt,.aes,.des,.plt" style="width: 28%" title="Upload blt, aes, des file">
                              <input type="file" name="filenames[]" class="myfrm form-control" placeholder="Upload image file " accept="image/*" style="width: 28%" title="Upload image file ">
                              <input type="text" name="width[]" class="myfrm form-control" placeholder="Width" style="width: 8%">
                              <input type="text" name="height[]" class="myfrm form-control" placeholder="Height" style="width: 8%">
                              <div class="input-group-btn"> 
                                 <button class="btn btn-danger btn-danger-blt" type="button" style="margin-top:0px;"><i class="fldemo glyphicon glyphicon-remove"></i>Remove</button>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">&nbsp;</div>
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <input type="submit" class="btn btn-success notika-btn-success waves-effect" value="{{ trans('global.save') }}" >
                     </div>
                  </div>
               </div>
            </div>
      </form>
      </div>  
   </div>
</div>
<script type="text/javascript"> 
   $(document).ready(function(){
      $("#brand_type_id").change(function() {
       var brand_type = $(this).find(":selected").val();
       var request = $.ajax({
           type: 'GET',
           url: "{{url('admin/get-brand-type-list')}}?brand_type="+brand_type,
       });
       request.done(function(data){
           $("#brand_id").empty();
           if(data){
        $("#brand_id").append('<option value="">Select Brand</option>');
                   $.each(data,function(key,value){
                    $("#brand_id").append('<option value="'+value['id']+'">'+value['name']+'</option>');
                   });
               }
           $('#brand_id').selectpicker('refresh');
       });
    
   });
   
   $("#brand_id").change(function() {
       var brand = $(this).find(":selected").val();
       var request = $.ajax({
           type: 'GET',
           url: "{{url('admin/get-brand-list')}}?brand="+brand,
       });
    
       request.done(function(data){
           $("#model_id").empty();
           if(data){
        //alert(data);
        $("#model_id").append('<option value="">Select Model</option>');
                   $.each(data,function(key,value){
                    $("#model_id").append('<option value="'+value['id']+'">'+value['name']+'</option>');
                   });
               }
           $('#model_id').selectpicker('refresh');
       });
    
   });
   
   
   $("#model_id").change(function() {
       var brand_models_id = $(this).find(":selected").val();
       var request = $.ajax({
           type: 'GET',
           url: "{{url('admin/get-sub-brand-type-list')}}?brand_models_id="+brand_models_id,
       });
     
       request.done(function(data){
           $("#sub_model_id").empty();
           if(data){
         //alert(data);
         $("#sub_model_id").append('<option value="">Select Model</option>');
                   $.each(data,function(key,value){
                    $("#sub_model_id").append('<option value="'+value['id']+'">'+value['name']+'</option>');
                   });
               }
           $('#sub_model_id').selectpicker('refresh');
       });
     
   });
   
   
       $(document).ready(function() {
   
         $(".btn-success1").click(function(){ 
   
             var lsthmtl = $(".clone").html();
   
             $(".increment").after(lsthmtl);
   
         });
   
         $("body").on("click",".btn-danger1",function(){ 
      
   
             $(this).parents(".hdtuto").remove();
   
         });
   
       });
   
   $(".btn-success-blt").click(function(){ 
   
         var lsthmtl = $(".clone").html();
   
         $(".increment-blt").after(lsthmtl);
   
     });
   
     $("body").on("click",".btn-danger-blt",function(){ 
     
   
         $(this).parents(".hd-blt").remove();
   
     })
    
      });
      
      
      
       
       
      
</script> 
@endsection