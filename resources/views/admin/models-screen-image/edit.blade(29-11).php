@extends('layouts.admin')
@section('content')

<div class="breadcomb-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="breadcomb-list">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="breadcomb-wp">
                                    <div class="breadcomb-icon">
                                        <i class="notika-icon notika-windows"></i>
                                    </div>
                                    <div class="breadcomb-ctn">
                                        <h2>Update Model Screen Image</h2>
                                        <p>Here <span class="bread-ntd">you can update model screen image</span></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-3">
                                <div class="breadcomb-report">
                                
                                     <a class="btn btn-success notika-btn-success waves-effect" href="{{ url()->previous() }}">
                {{ trans('global.back_to_list') }}
            </a>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
<div class="form-element-area">
  <div class="container">
  <form action="{{ route('admin.models-screen-image.update', [$brandModelScreen->id]) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
   <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="form-element-list">
                        <div class="basic-tb-hd">
                            <!--<h2>{{ trans('global.create') }} {{ trans('cruds.user.title_singular') }}</h2>
                            <p>Text Inputs with different sizes by height and column.</p>-->
                        </div>
                        
                        <div class="row">
                        
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="nk-int-mk sl-dp-mn mg-t-10">
                                    <label>Category*</label>
                                </div>
                                <div class="bootstrap-select">
                                    <select class="selectpicker" name="brand_type_id" id="brand_type_id"  >
                                    <option value="">Select Category</option>
                                        @foreach($brand_types as $id => $brand_type)
                                        
                                        <option value="{{ $brand_type->id }}" @if($brandModelScreen->brand_type_id==$brand_type->id) {{'selected'}} @else {{''}} @endif>{{ $brand_type->name }}</option>
                                        @endforeach
                                        </select>
                                        </div>
                                         @if($errors->has('brand_type_id'))
                                        <em class="invalid-feedback">
                                            {{ $errors->first('brand_type_id') }}
                                        </em>
                                        @endif
                                    </div>
                                    
                                    
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="nk-int-mk sl-dp-mn mg-t-10">
                                    <label>Brands*</label>
                                </div>
                                <div class="bootstrap-select">
                                    <select class="selectpicker" name="brand_id" id="brand_id"   >
                                    <option value=" ">Select Brand </option>
                                        @foreach($brands as $id => $brand)
                                        
                                        <option value="{{ $brand->id }}" @if($brandModelScreen->brand_id==$brand->id) {{'selected'}} @else {{''}} @endif >{{ $brand->name }}</option>
                                        @endforeach
                                        </select>
                                        </div>
                                         @if($errors->has('brand_id'))
                                        <em class="invalid-feedback">
                                            {{ $errors->first('brand_id') }}
                                        </em>
                                        @endif
                                    </div>
                                </div>
                        
                        <div class="row">    
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="nk-int-mk sl-dp-mn mg-t-10">
                                    <label>Models*</label>
                                </div>
                                <div class="bootstrap-select">
                                    <select class="selectpicker" name="model_id" id="model_id"   >
                                    <option value=" ">Select Models </option>
                                        @foreach($brandsModel as $id => $models)
                                        
                                        <option value="{{ $models->id }}" @if($brandModelScreen->model_id==$models->id) {{'selected'}} @else {{''}} @endif >{{ $models->name }}</option>
                                        @endforeach
                                        </select>
                                        </div>
                                         @if($errors->has('model_id'))
                                        <em class="invalid-feedback">
                                            {{ $errors->first('model_id') }}
                                        </em>
                                        @endif
                                    </div>
                        
                            
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="nk-int-mk sl-dp-mn mg-t-10">
                                    <label>Sub-Models*</label>
                                </div>
                                <div class="bootstrap-select">
                                    <select class="selectpicker" name="sub_model_id" id="sub_model_id"   >
                                        <option value=" ">Select Sub-Models </option>
                                        @foreach($brandSubModel as $id => $submodels)
                                        <option value="{{ $submodels->id }}" @if($brandModelScreen->sub_model_id==$submodels->id) {{'selected'}} @else {{''}} @endif >{{ $submodels->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                @if($errors->has('sub_model_id'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('sub_model_id') }}
                                </em>
                                @endif
                            </div>  
                        
                          </div>
                        
                       <!--  <div class="row">
                             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                               <div class="form-example-int mg-t-15">
                            <div class="form-group">
                               
                                <div class="nk-int-st">
                                    <input type="text" class="form-control input-sm" placeholder="Meta Title" value="{{ old('meta_title', isset($brandModelScreen) ? $brandModelScreen->meta_title : '') }}" id="meta_title" name="meta_title">
                                </div>
                            </div>
                        </div>
                            </div>
                             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                               <div class="form-example-int mg-t-15">
                            <div class="form-group">
                               
                                <div class="nk-int-st">
                                    <textarea class="form-control" placeholder="Meta Keywords" id="meta_keywords" name="meta_keywords">{{ old('meta_keywords', isset($brandModelScreen) ? $brandModelScreen->meta_keywords : '') }}</textarea>
                                </div>
                            </div>
                        </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                               <div class="form-example-int mg-t-15">
                            <div class="form-group">
                                <div class="nk-int-st textarea">
                                    <textarea class="form-control" placeholder="Meta Description" id="meta_description" name="meta_description">{{ old('meta_description', isset($brandModelScreen) ? $brandModelScreen->meta_description : '') }}</textarea>
                                </div>
                            </div>
                        </div>
                            </div> -->
                        <div class="row">    
                           <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                              <div class="nk-int-mk sl-dp-mn mg-t-10">
                                  <label>Old Name*</label>
                              </div>
                              <div class="form-group"> 
                                  <div class="nk-int-st">
                                    @php 
                                    $old_name = null;
                                    @endphp
                                    @if(isset($brandModelScreen->name))
                                        @php
                                            $nameArray = json_decode($brandModelScreen->name,TRUE);
                                            if( $nameArray === NULL ) {
                                                $old_name = $data->name ?? null;
                                            }else{
                                                $names = implode(", ",$nameArray);
                                                $old_name = trim($names ?? '', ", "); 
                                            }
                                        @endphp
                                    @endif
                                      <input type="text" class="form-control" value="{{ old('old_name') ?? $old_name }}" placeholder="Model Name" id="old_name" name="old_name">
                                  </div>
                                  @if($errors->has('old_name'))
                                  <em class="invalid-feedback">
                                      {{ $errors->first('old_name') }}
                                  </em>
                                  @endif
                              </div> 
                          </div>
                          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="nk-int-mk sl-dp-mn mg-t-10">
                                      <label>Status*</label>
                                </div>
                                <div class="bootstrap-select">
                                      <select class="selectpicker" name="status" id="status"  required >
                                          <option>Select Status</option>
                                    <option value="0" {{($brandModelScreen->status=='0') ? 'selected' : ''}}>Inactive</option>
                                    <option value="1" {{($brandModelScreen->status=='1') ? 'selected' : ''}}>Active</option>
                                    </select>
                                    </div>
                                    </div> 
                        </div>                        
                        <div class="row"> 
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="nk-int-mk sl-dp-mn mg-t-10">
                                    <label>Order number</label>
                                </div>
                                <div class="form-group"> 
                                    <div class="nk-int-st">
                                        <input type="number" name="order_number" id="order_number" class="form-control" placeholder="order number" value="{{ $brandModelScreen->order_number ?? $brandModelScreen->id }}" min="1">
                                    </div>
                                    @if($errors->has('order_number'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('order_number') }}
                                    </em>
                                    @endif
                                </div> 
                            </div>
                        </div> 
                            <!--- start blt file --> 
                            <div class="row">   
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">&nbsp;</div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    Upload .Blt and Image file
                                    <!-- <div class="input-group hd-blt control-group lst increment-blt">
                                        <input type="text" name="name[]" class="myfrm form-control w-50" value="" placeholder="Screen Name" style="width: 50%">
                                        <input type="file" name="blt_file[]" class="myfrm form-control" placeholder="Upload blt file " accept=".blt,.aes,.des" style="width: 25%">
                                        <input type="file" name="filenames[]" class="myfrm form-control" placeholder="Upload image file " accept="image/*" style="width: 25%">
                                        <div class="input-group-btn">
                                            <button class="btn btn-success btn-success-blt" type="button"><i class="fldemo glyphicon glyphicon-plus"></i>Add</button>
                                        </div>
                                    </div> -->
                                    <div class="input-group hd-blt control-group lst increment-blt" >
                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 mg-t-10" style="padding:0">
                                          <label class="col-lg-12 col-md-12 col-sm-12 col-xs-12">Screen Name</label>
                                          <input type="text" name="name[]" class="myfrm form-control" placeholder="Screen Name">
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 mg-t-10" style="padding:0">
                                          <label class="col-lg-12 col-md-12 col-sm-12 col-xs-12">Upload blt, plt file</label>
                                          <input type="file" name="blt_file[]" class="myfrm form-control w-25" placeholder="Upload blt file" accept=".blt,.aes,.des,.plt" title="Upload blt, aes, des file" placeholder="Upload blt, plt file">
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 mg-t-10" style="padding:0">
                                          <label class="col-lg-12 col-md-12 col-sm-12 col-xs-12">Upload image file</label>
                                          <input type="file" name="filenames[]" class="myfrm form-control w-25" placeholder="Upload image file " accept="image/*" title="Upload image file ">
                                        </div>
                                        <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12 mg-t-10" style="padding:0">
                                          <label class="col-lg-12 col-md-12 col-sm-12 col-xs-12">Width</label>
                                          <input type="text" name="width[]" class="myfrm form-control" placeholder="Width">
                                        </div>
                                        <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12 mg-t-10" style="padding:0">
                                          <label class="col-lg-12 col-md-12 col-sm-12 col-xs-12">Height</label>
                                          <input type="text" name="height[]" class="myfrm form-control" placeholder="Height">
                                        </div>
                                        <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12 mg-t-10" style="padding:0">
                                          <label class="col-lg-12 col-md-12 col-sm-12 col-xs-12">&nbsp;</label>
                                          <button class="btn btn-success btn-success-blt btn-block" type="button"><i class="fldemo glyphicon glyphicon-plus"></i>Add</button>
                                        </div>
                                    </div>
                                    <div class="clone hide">
                                        <div class="hd-blt control-group lst input-group" style="margin-top:10px">
                                            <label class="col-lg-12 col-md-12 col-sm-12 col-xs-12">Upload .Blt and Image file </label>                      
                                            <input type="text" name="name[]" class="myfrm form-control w-50" placeholder="Screen Name" style="width: 28%">
                                            <input type="file" name="blt_file[]" class="myfrm form-control" placeholder="Upload blt file " accept=".blt,.aes,.des" style="width: 28%" >
                                            <input type="file" name="filenames[]" class="myfrm form-control" placeholder="Upload image file " accept="image/*" style="width: 28%" >
                                            <input type="text" name="width[]" class="myfrm form-control" placeholder="Width" style="width: 8%">
                                            <input type="text" name="height[]" class="myfrm form-control" placeholder="Height" style="width: 8%">
                                            <div class="input-group-btn"> 
                                                <button class="btn btn-danger btn-danger-blt" type="button" style="margin-top:25px;"><i class="fldemo glyphicon glyphicon-remove"></i> Remove</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">&nbsp;</div>
                                <div class="clear"></div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    @if(!empty($brandModelScreen->blt_file))
                                        @php 
                                            $nameArray = json_decode($brandModelScreen->name, True); 
                                            $bltArray = json_decode($brandModelScreen->blt_file, True); 
                                            $imageArray = json_decode($brandModelScreen->image, True); 
                                            $widthArray = json_decode($brandModelScreen->width, True); 
                                            $heightArray = json_decode($brandModelScreen->height, True); 
                                            
                                        @endphp
                                        @forelse($imageArray as $key => $value) 
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                            @if(!empty($value))
                                                <a href="{{ asset('images/models-screen-image/'.$value) }}" data-lightbox="lightbox-{{$key}}"> 
                                                    <img src="{{ asset('images/models-screen-image/'.$value) }}" class="animate-eleven" style="width:60px;height: 60px; margin:0 auto; margin-bottom: 5px; background-color: #F5F5F5">
                                                </a>
                                                <br/>
                                            @endif  
                                            @isset($nameArray[$key])
                                                {{$nameArray[$key] ?? null}} <br/> 
                                            @endisset
                                            @isset($bltArray[$key])
                                                <a href="{{ asset('images/models-screen-image/'.$bltArray[$key] ?? null) }}" target="download">{{$bltArray[$key] ?? null}}</a><br/>
                                            @endisset
                                            <input type="text" name="old_width[]" class="form-control"  placeholder="Width" style="width: 40%" value="{{ isset($widthArray[$key]) ? $widthArray[$key] : null }}">
                                            <input type="text" name="old_height[]" class="form-control"  placeholder="Width" style="width: 40%" value="{{ isset($heightArray[$key]) ? $heightArray[$key] : null }}">
                                            <br/> 
                                        </div>
                                        @empty
                                        no data found
                                        @endforelse
                                    @endif                                    
                                </div>
                            </div>
                            <!--- end blt file -->

                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">&nbsp;</div>
                            <div class="clear"></div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> 
                                <input type="submit" class="btn btn-success notika-btn-success waves-effect" value="{{ trans('global.save') }}" >
                            </div>
                        </div>
                     
                      
                    </div>
                </div>
            </div>
</form>         
            </div>
            </div>
            <script type="text/javascript"> 
$(document).ready(function(){
   $("#brand_type_id").change(function() {
    var brand_type = $(this).find(":selected").val();
    var request = $.ajax({
        type: 'GET',
        url: "{{url('admin/get-brand-type-list')}}?brand_type="+brand_type,
    });
    request.done(function(data){
        $("#brand_id").empty();
        if(data){
            $("#brand_id").append('<option value="">Select Brand</option>');
                $.each(data,function(key,value){
                 $("#brand_id").append('<option value="'+value['id']+'">'+value['name']+'</option>');
                });
            }
        $('#brand_id').selectpicker('refresh');
    });
    
});

$("#brand_id").change(function() {
    var brand = $(this).find(":selected").val();
    var request = $.ajax({
        type: 'GET',
        url: "{{url('admin/get-brand-list')}}?brand="+brand,
    });
    
    request.done(function(data){
        $("#model_id").empty();
        if(data){
            //alert(data);
            $("#model_id").append('<option value="">Select Model</option>');
                $.each(data,function(key,value){
                 $("#model_id").append('<option value="'+value['id']+'">'+value['name']+'</option>');
                });
            }
        $('#model_id').selectpicker('refresh');
    });
    
});


$("#model_id").change(function() {
    var brand_models_id = $(this).find(":selected").val();
    var request = $.ajax({
        type: 'GET',
        url: "{{url('admin/get-sub-brand-type-list')}}?brand_models_id="+brand_models_id,
    });
  
    request.done(function(data){
        $("#sub_model_id").empty();
        if(data){
      //alert(data);
      $("#sub_model_id").append('<option value="">Select Model</option>');
                $.each(data,function(key,value){
                 $("#sub_model_id").append('<option value="'+value['id']+'">'+value['name']+'</option>');
                });
            }
        $('#sub_model_id').selectpicker('refresh');
    });
  
});
 
   });


$(document).ready(function() {

  $(".btn-success1").click(function(){ 

      var lsthmtl = $(".clone").html();

      $(".increment").after(lsthmtl);

  });

  $("body").on("click",".btn-danger1",function(){ 
  

      $(this).parents(".hdtuto").remove();

  });

  $(".btn-success-blt").click(function(){ 

      var lsthmtl = $(".clone").html();

      $(".increment-blt").after(lsthmtl);

  });

  $("body").on("click",".btn-danger-blt",function(){ 
  

      $(this).parents(".hd-blt").remove();

  })

});
   </script>    
            @endsection
    