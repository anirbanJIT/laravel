<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
      <meta name="description" content="POS - Bootstrap Admin Template">
      <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
      <meta name="author" content="Dreamguys - Bootstrap Admin Template">
      <meta name="robots" content="noindex, nofollow">
      <title>Update Model Screen Image</title>
      
      <link rel="shortcut icon" type="image/x-icon" href="{{ asset('public/Admin/img/favicon.png')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/bootstrap.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/animate.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/dataTables.bootstrap4.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/fontawesome.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/all.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/style.css')}}">
   </head>
   <body>
    <div id="global-loader">
        <div class="whirly-loader"> </div>
    </div>
    <div class="main-wrapper">
        @extends('layouts.header')
        @extends('layouts.sidebar')
        <div class="page-wrapper">
            <div class="content container-fluid">
                <div class="page-header">
					<div class="row">
						<div class="col">
							<h3 class="page-title">Update Model Screen Image</h3>
							<ul class="breadcrumb">
							   <li class="breadcrumb-item"><a href="{{route('admin.home')}}">{{ trans('global.dashboard') }}</a></li>
							   <li class="breadcrumb-item">
									@if($brandModelScreen->sub_model_id)
										<a href="{{ route('admin.models-screen-image-list','id='.$brandModelScreen->sub_model_id) }}">Model Screen Images</a>
									@else
										<a href="{{ route('admin.brand-type.index') }}">Model Screen Images</a>
									@endif
								</li>
							   <li class="breadcrumb-item active">Update Model Screen Image</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div class="card">
							<div class="card-body">
								<form action="{{ route('admin.models-screen-image.update', [$brandModelScreen->id]) }}" method="POST" enctype="multipart/form-data">
									{{ csrf_field() }}
									@method('PUT')
									<input type="hidden" name="get_sub_model_id" value="{{ $brandModelScreen->sub_model_id }}">
								    <div class="row">   
										<div class="form-group col-lg-6">
											<label>Category<span class="text-danger">*</span></label>
											<select class="form-control @error('brand_type_id') is-invalid @enderror" name="brand_type_id" id="brand_type_id"  >
												<option value="">Select Category</option>
												@foreach($brand_types as $id => $brand_type)
													<option value="{{ $brand_type->id }}" @if($brandModelScreen->brand_type_id==$brand_type->id) {{'selected'}} @else {{''}} @endif>{{ $brand_type->name }}</option>
												@endforeach
											</select>
											@if($errors->has('brand_type_id'))
												<em class="invalid-feedback">{{ $errors->first('brand_type_id') }}</em>
											@endif
										</div>
										<div class="form-group col-lg-6">
											<label>Brands<span class="text-danger">*</span></label>
											<select class="form-control @error('brand_id') is-invalid @enderror" name="brand_id" id="brand_id">
												<option value=" ">Select Brand </option>
												@foreach($brands as $id => $brand)
													<option value="{{ $brand->id }}" @if($brandModelScreen->brand_id==$brand->id) {{'selected'}} @else {{''}} @endif >{{ $brand->name }}</option>
												@endforeach
											</select>
											@if($errors->has('brand_id'))
												<em class="invalid-feedback">{{ $errors->first('brand_id') }}</em>
											@endif
										</div>
										<div class="form-group col-lg-4">
											<label>Models<span class="text-danger">*</span></label>
											<select class="form-control @error('model_id') is-invalid @enderror" name="model_id" id="model_id"   >
												<option value=" ">Select Model </option>
												@foreach($brandsModel as $id => $models)
													<option value="{{ $models->id }}" @if($brandModelScreen->model_id==$models->id) {{'selected'}} @else {{''}} @endif >{{ $models->name }}</option>
												@endforeach
											</select>
											@if($errors->has('model_id'))
												<em class="invalid-feedback">{{ $errors->first('model_id') }}</em>
											@endif
										</div>
										<div class="form-group col-lg-4">
											<label>Sub Models<span class="text-danger">*</span></label>
											<select class="form-control @error('sub_model_id') is-invalid @enderror" name="sub_model_id" id="sub_model_id"   >
												<option value=" ">Select Sub-Models </option>
												@foreach($brandSubModel as $id => $submodels)
													<option value="{{ $submodels->id }}" @if($brandModelScreen->sub_model_id==$submodels->id) {{'selected'}} @else {{''}} @endif >{{ $submodels->name }}</option>
												@endforeach
											</select>
											@if($errors->has('sub_model_id'))
												<em class="invalid-feedback">{{ $errors->first('sub_model_id') }}</em>
											@endif
										</div>
										<div class="form-group col-lg-4">
											<label>Status<span class="text-danger">*</span></label>
											<select class="form-control @error('status') is-invalid @enderror" name="status" id="status"  required >
												<option>Select Status</option>
												<option value="0" {{($brandModelScreen->status=='0') ? 'selected' : ''}}>Inactive</option>
												<option value="1" {{($brandModelScreen->status=='1') ? 'selected' : ''}}>Active</option>
											</select> 
											@if($errors->has('status'))
												<em class="invalid-feedback">{{ $errors->first('status') }}</em>
											@endif
										</div>
										<div class="form-group col-lg-6">
											<label>Old Name<span class="text-danger">*</span></label>
											@php 
												$old_name = null;
											@endphp
											@if(isset($brandImages))
												@php
													$nameArray = array_column($brandImages->toArray(),'screen_name');
													if( $nameArray === NULL ) {
														$old_name = $data->name ?? null;
													}else{
														$names = implode(", ",$nameArray);
														$old_name = trim($names ?? '', ", "); 
													}
												@endphp
											@endif
											<input type="text" class="form-control @error('old_name') is-invalid @enderror" value="{{ old('old_name') ?? $old_name }}" placeholder="Old Name" id="old_name" name="old_name">
											@if($errors->has('old_name'))
												<em class="invalid-feedback">{{ $errors->first('old_name') }}</em>
											@endif
										</div>
										<!-- <div class="form-group col-lg-6">
											<label>Order number</label>
											<input type="number" name="order_number" id="order_number" class="form-control @error('order_number') is-invalid @enderror" placeholder="order number" value="{{ $brandModelScreen->order_number ?? $brandModelScreen->id }}" min="1">
											@if($errors->has('order_number'))
												<em class="invalid-feedback">{{ $errors->first('order_number') }}</em>
											@endif
										</div> -->
									</div>

								    <div class="row hd-blt lst increment-blt">
										<div class="form-group col-lg-12">
											<h4>Upload .Blt and Image file</h4> <hr/>
										</div>
										<div class="form-group col-lg-3">
											<label>Screen Name<span class="text-danger">*</span></label>
											<input type="text" name="name[]" class="form-control @error('name') is-invalid @enderror" placeholder="Screen Name" required>
											@if($errors->has('name'))
												<em class="invalid-feedback">{{ $errors->first('name') }}</em>
											@endif
										</div>
										<div class="form-group col-lg-3">
											<label>Upload blt, plt file<span class="text-danger">*</span></label>
											<input type="file" accept=".blt,.aes,.des,.plt" class="form-control @error('blt_file') is-invalid @enderror" name="blt_file[]" id="blt_file">
											@if($errors->has('blt_file'))
												<em class="invalid-feedback">{{ $errors->first('blt_file') }}</em>
											@endif
										</div>
										<div class="form-group col-lg-3">
											<label>Upload image file<span class="text-danger">*</span></label>
											<input type="file" name="filenames[]" class="form-control @error('filenames') is-invalid @enderror" placeholder="Upload image file " accept="image/*" required title="Upload image file ">
											@if($errors->has('filenames'))
												<em class="invalid-feedback">{{ $errors->first('filenames') }}</em>
											@endif
										</div>
										<div class="form-group col-lg-1">
											<label>Width<span class="text-danger">*</span></label>
											<input type="text" name="width[]" class="form-control @error('width') is-invalid @enderror" placeholder="Width" required>
											@if($errors->has('width'))
												<em class="invalid-feedback">{{ $errors->first('width') }}</em>
											@endif
										</div>
										<div class="form-group col-lg-1">
											<label>Height<span class="text-danger">*</span></label>
											<input type="text" name="height[]" class="form-control @error('height') is-invalid @enderror" placeholder="Height" required>
											@if($errors->has('height'))
												<em class="invalid-feedback">{{ $errors->first('height') }}</em>
											@endif
										</div>
										<div class="form-group col-lg-1">
											<label><br></label>
											<a class="btn btn-primary btn-success-blt btn-block" id="add">+Add</a> 
										</div>
									</div>

									<div class="row clone hide hd-blt">
										<div class="form-group col-lg-3">
											<input type="text" name="name[]" class="form-control" placeholder="Screen Name" >
										</div>
										<div class="form-group col-lg-3">
											<input type="file" accept=".blt,.aes,.des,.plt" class="form-control" name="blt_file[]" id="blt_file">
										</div>
										<div class="form-group col-lg-3">
											<input type="file" name="filenames[]" class="form-control" placeholder="Upload image file " accept="image/*"  title="Upload image file ">
										</div>
										<div class="form-group col-lg-1">
											<input type="text" name="width[]" class="form-control" placeholder="Width" >
										</div>
										<div class="form-group col-lg-1">
											<input type="text" name="height[]" class="form-control" placeholder="Height" >
										</div>
										<div class="form-group col-lg-1">
											<a class="btn btn-danger btn-danger-blt btn-block" style="pedding:12%" id=""> <i class="fa fa-times"></i> Del</a> 
										</div>
									</div>
									<div>
										<button type="submit" class="btn btn-success">Save</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
            </div>
        </div>
    </div>
    <script src="{{ asset('public/Admin/js/jquery-3.6.0.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/feather.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.slimscroll.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{ asset('public/Admin/plugins/apexchart/apexcharts.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/script.js')}}"></script>
	
    <script type="text/javascript"> 
$(document).ready(function()
{
	
   $(".hide").hide();
   
   $("#add").click(function()
   { 
      var lsthmtl = $(".clone").html();
	  
      $(".increment-blt").after("<div class='row hd-blt lst increment-blt'> "+lsthmtl+"</div>");

   });

  $("body").on("click",".btn-danger-blt",function()
  { 
      //alert("hii");
      $(this).parents(".hd-blt").remove();
   })
 
});
   
   </script>
   
   </body>
</html>