<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
      <meta name="description" content="POS - Bootstrap Admin Template">
      <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
      <meta name="author" content="Dreamguys - Bootstrap Admin Template">
      <meta name="robots" content="noindex, nofollow">
      <title>Model Screen Image List</title>
      
      <link rel="shortcut icon" type="image/x-icon" href="{{ asset('public/Admin/img/favicon.png')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/bootstrap.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/animate.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/dataTables.bootstrap4.min.css')}}">

      <!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.7.1/css/buttons.dataTables.min.css"> -->
      
       

      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/fontawesome.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/all.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/style.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/select2/css/select2.min.css')}}">
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <link rel="stylesheet" href="{{ asset('public/Admin/tabulator/bootstrap/tabulator_bootstrap4.css')}}">
      
   </head>
   <body>
   
<div class="main-wrapper">
    @extends('layouts.header')
    @extends('layouts.sidebar')
    <div class="page-wrapper">
        <div class="content container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <h3 class="page-title">Model Screen Image List</h3>
                        <ul class="breadcrumb">
                           <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">{{ trans('global.dashboard') }}</a></li>
						   <li class="breadcrumb-item"><a href="{{ route('admin.brand-type.index') }}">Category List</a></li>
                           <li class="breadcrumb-item" ><a id="breadcrumb_brand_type_id" href="#">Brand List</a></li>
                           <li class="breadcrumb-item" ><a id="breadcrumb_brand_id" href="#">Models List</a></li>
						   <li class="breadcrumb-item" ><a id="breadcrumb_model_id" href="#">Submodels List</a></li>
                           <li class="breadcrumb-item active">Model Screen Image List</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header row">
                            <div class="col-lg-6">
								<h4 class="card-title">Model Screen Image List</h4>
							</div>
                            <div class="col-lg-6">
								<a href="{{ route('admin.models-screen-image.create','submodel_id='.$submodule_id) }}" class="btn btn-success" style="float: right;margin-left: 2%;">+ Add model Screen Image</a>
								<a id="openmodel" class="btn btn-success" style="float: right;">Rearrange</a>
							</div>
                        </div>
                        <div class="card-body">
							<button style="margin-bottom: 10px" class="btn btn-primary delete_all" data-url="{{ route('admin.models-screen-image.mass_destroy') }}">Delete All Selected</button> 
							<div class="table-responsive">
								<div class="text-right">
									<label style="float: right;">						
										<input id="filter-value" type="text" class="form-control form-control-sm" placeholder="Search..." aria-controls="DataTables_Table_0" style="float:right;margin-bottom:2%;">
									</label>
								</div>
                                <div id="data-table-models-screen-image-list"></div>
								<div class="modal fade" id="dragdrop_model"  role="dialog" >
									<div class="modal-dialog modal-dialog-centered" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title">Modal title</h5>
												<button type="button" class="close" data-bs-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>
											</div>
											<div class="modal-body">
												<table class="table table-bordered">
													<tr>
														<th>Id</th>
														<th>Name</th>
													</tr>
													<tbody class="row_position">
														@foreach($brandModelScreen as $row)
															<tr  id="{{ $row->id }}">
																<td>{{ $row->id }}</td>
																<td>{{ $row->name }}</td>
															</tr>
														@endforeach
													</tbody>
												</table>	
											</div>
											<div class="modal-footer">
												<button type="button" onclick="updateOrder()" class="btn btn-primary">Save changes</button>
												<button type="button" data-bs-dismiss="modal" class="btn btn-secondary">Close</button>
											</div>
										</div>
									</div>
								</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    <script src="{{ asset('public/Admin/js/jquery-3.6.0.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/feather.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.slimscroll.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/script.js')}}"></script>
      
    <script src="{{ asset('public/Admin/tabulator/tabulator.min.js') }}"></script>
    <script src="{{ asset('public/Admin/tabulator/jspdf.min.js') }}"></script>
    <script src="{{ asset('public/Admin/tabulator/xlsx.full.min.js') }}"></script>
    <script src="{{ asset('public/Admin/tabulator/jspdf.plugin.autotable.js') }}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script type="text/javascript">
		$("#openmodel").click(function(){
		    $("#dragdrop_model").modal('show');
		});
		$( ".row_position" ).sortable({
			delay: 150,
			stop: function() {
				var selectedData = new Array();
				$('.row_position>tr').each(function() {
					selectedData.push($(this).attr("id"));
				});
				
			}
		});
		function updateOrder() {
			var selectedData = new Array();
			$('.row_position>tr').each(function() {
				selectedData.push($(this).attr("id"));
			});
			
			var url = '{{ route("admin.modelsScreenImageOrderPosition") }}';
            	  
			$.ajax({
				url:url,
				type:'post',
				data:{position:selectedData},
				headers: {
						'X-CSRF-TOKEN': "{{csrf_token()}}"
					},
				success:function(){
					alert('your change successfully saved');
					window.location.reload();
				}
			});
		}
		var tabledata = <?php echo json_encode($brandModelScreen); ?>;
		var submodule_id = <?php echo $submodule_id; ?>;
		if(submodule_id != ""){
			var brand_type_url = '{{ route("admin.brands.show",":id") }}';
			brand_type_url = brand_type_url.replace(':id', tabledata[0].brand_type_id);
			$("#breadcrumb_brand_type_id").attr("href",brand_type_url);
			  
			var brand_url = '{{ route("admin.models.show",":id") }}';
			brand_url = brand_url.replace(':id', tabledata[0].brand_id);
			$("#breadcrumb_brand_id").attr("href",brand_url);
			  
			var model_url = '{{ route("admin.submodels.show",":id") }}';
			model_url = model_url.replace(':id', tabledata[0].model_id);
			$("#breadcrumb_model_id").attr("href",model_url);
		}
	  	var checkboxIcon = function(cell, formatterParams){ //plain text value
			return '<input class="sub_chk" type="checkbox" onclick="$(\'#is_delete\').val(1)" data-id="'+cell.getData().id+'">';
		};
		var table = new Tabulator("#data-table-models-screen-image-list", {
			data:tabledata,           //load row data from array
			layout:"fitColumns",      //fit columns to width of table
			responsiveLayout:"collapse",  //hide columns that dont fit on the table
			tooltips:true,            //show tool tips on cells
			addRowPos:"top",          //when adding a new row, add it to the top of the table
			history:true,             //allow undo and redo actions on the table
			pagination:"local",       //paginate the data
			paginationSize:10,         //allow 7 rows per page of data
			movableColumns:true,      //allow column order to be changed
			resizableRows:true,       //allow row order to be changed
			/* movableRows: true, //enable movable rows
     		rowMoved:function(row){
				$("#dragdrop_model").modal('show');
			}, */
			columns: [
				{title:'<input id="master" type="checkbox" onclick="$(\'#is_delete\').val(1)">', formatter:checkboxIcon, width:40, align:"center", cellClick:function(e, cell){cell.getRow().toggleSelect();},headerSort:false},
				/* {formatter:"rowSelection", titleFormatter:"rowSelection", hozAlign:"center", 
					headerSort:false, cellClick:function(e, cell){cell.getRow().toggleSelect();}
				}, */
				{title:"Model Screen Id", field:"id", editor:"label",headerFilter:"input",
					headerFilterPlaceholder:"Model Screen Id",headerSort:false,
				},
                {title:"Category Name", field:"brands_types.name", editor:"label",headerFilter:"input",		     headerFilterPlaceholder:"Category Name",headerSort:false,
				},
				{title:"Brand Name", field:"brands.name", editor:"label",headerFilter:"input",	headerFilterPlaceholder:"Brand Name",headerSort:false,
				},
				{title:"Model Name", field:"brands_model.name", editor:"label",headerFilter:"input",		headerFilterPlaceholder:"Model Name",headerSort:false,
				},
				{title:"Sub-Model Name", field:"brand_sub_model.name", editor:"label",headerFilter:"input",	headerFilterPlaceholder:"Sub-Model Name",headerSort:false,
				},
				{title:"Name", field:"name", editor:"label",headerFilter:"input",headerFilterPlaceholder:"Name",
					headerSort:false,formatter:function(cell, formatterParams, onRendered){
						return cell.getValue();
					}
				},
                {title:"Order", field:"position_order", editor:"label",headerFilter:"input",
					headerSort:true,headerFilterPlaceholder:"Order"
				},
				{title:"Status", field:"id_status", headerSort:false,
					formatter:function(cell, formatterParams, onRendered){
						var arr = cell.getValue();
						var data = arr.split("_");
						console.log(data[1]);
						if(data[1] == '1'){
							return '<div class="status-toggle d-flex justify-content-between align-items-center" ><input type="checkbox" class="check status_change" data-id="'+data[0]+'" id="ts'+data[0]+'" data-on="Active"  checked ><label for="ts'+data[0]+'" class="checktoggle">checkbox</label></div>';
						} else {
							return '<div class="status-toggle d-flex justify-content-between align-items-center" ><input type="checkbox" class="check status_change" data-id="'+data[0]+'" id="ts'+data[0]+'" data-off="InActive"><label for="ts'+data[0]+'" class="checktoggle">checkbox</label></div>';
						}            
					}
				},
				{title:"Action",width:170, field:"id", headerSort:false,
					formatter:function(cell, formatterParams, onRendered){ 
					    var show_url = '{{ route("admin.models-screen-image.show", ":id") }}';
						show_url = show_url.replace(':id', cell.getValue());
						
						var edit_url = '{{ route("admin.models-screen-image.edit", ":id") }}';
						edit_url = edit_url.replace(':id', cell.getValue());

						var delete_url1 = '{{ route("admin.models-screen-image.destroy",":id") }}';
						delete_url1 = delete_url1.replace(':id', cell.getValue());
						var msg = "Are you sure you want to delete this Model Screen Image?";
						var token = '{{ csrf_token() }}';

						var editlink = '<a  href="'+show_url+'" class="btn btn-primary  text-white" title="View"><i class="fa fa-eye"></i></a><a  href="'+edit_url+'" class="btn btn-success text-white" title="Edit"><i class="fa fa-edit"></i></a><form action="'+delete_url1+'" method="POST" onsubmit="return confirm(\''+msg+'\');" style="display: inline-block;"><input type="hidden" name="_method" value="DELETE"><input type="hidden" name="_token" value="'+token+'"><button type="submit" class="btn btn-danger danger-icon-notika btn-reco-mg btn-button-mg waves-effect"> <i class="fa fa-trash"></i></button></form>';
						return editlink; 
					},
				}
            ],
        });

$('.status_change').change(function() 
         {      
			var status = $(this).prop('checked') == true ? 1 : 0; 
            var model_id = $(this).data('id');  
					
             $.ajax({
                 type:"GET",
                 url:"{{url('admin/changeStatusModelScreen')}}?status="+status+"&modelScreen_id="+model_id,
                 success:function(res){               
                  if(res){
					  console.log(res.success);
                  }
                 }
              });
   
         });
		
		
		
		
		 $('#master').on('click', function(e) {  
			 if($(this).is(':checked',true))    
			 {  
				$(".sub_chk").prop('checked', true);    
			 } else {    
				$(".sub_chk").prop('checked',false);    
			 }    
        }); 
		 $('.delete_all').on('click', function(e) {  
			/* $("input[type='checkbox']").prop('checked', true);    */ 
			 
            var allVals = [];    
            $(".sub_chk:checked").each(function() {    
                allVals.push($(this).attr('data-id'));  
            });       
			
            if(allVals.length <=0)    
            {    
                alert("Please select Model Screen Image.");    
            }  else {    
  
                var check = confirm("Are you sure you want to delete this Model Screen Image?");    
                if(check == true){    
  
                    var join_selected_values = allVals.join(",");   
  
                    $.ajax({  
                        url: $(this).data('url'),  
                        type: 'DELETE',  
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},  
                        data: 'ids='+join_selected_values,  
                        success: function (data) {  
                            if (data['success']) {  
                                $("input[type='checkbox']:checked").each(function() {    
                                    $(this).parents("tr").remove();  
                                });  
                                alert(data['success']); 
								window.location.reload();
                            } else if (data['error']) {  
                                alert(data['error']);  
                            } else {  
                                alert('Whoops Something went wrong!!');  
                            }  
                        },  
                        error: function (data) {  
                            alert(data.responseText);  
                        }  
                    });  
  
                  $.each(allVals, function( index, value ) {  
                      $('table tr').filter("[data-row-id='" + value + "']").remove();  
                  });  
                }    
            }    
        }); 
      

</script>
   </body>
</html>