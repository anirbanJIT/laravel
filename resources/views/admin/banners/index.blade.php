<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
      <meta name="description" content="POS - Bootstrap Admin Template">
      <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
      <meta name="author" content="Dreamguys - Bootstrap Admin Template">
      <meta name="robots" content="noindex, nofollow">
      <title>Banner Details</title>
      
      <link rel="shortcut icon" type="image/x-icon" href="{{ asset('public/Admin/img/favicon.png')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/bootstrap.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/animate.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/dataTables.bootstrap4.min.css')}}">

      <!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.7.1/css/buttons.dataTables.min.css"> -->
      
       

      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/fontawesome.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/all.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/style.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/select2/css/select2.min.css')}}">
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <link rel="stylesheet" href="{{ asset('public/Admin/tabulator/bootstrap/tabulator_bootstrap4.css')}}">
      
   </head>
   <body>
      <!-- <div id="global-loader">
         <div class="whirly-loader"> </div>
      </div> -->
      <div class="main-wrapper">
        @extends('layouts.header')
     
        @extends('layouts.sidebar')
        
        <div class="page-wrapper">
            <div class="content container-fluid">
               <div class="page-header">
                  <div class="row">
                     <div class="col">
                        <h3 class="page-title">Banner List</h3>
                        <ul class="breadcrumb">
                           <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">{{ trans('global.dashboard') }}</a></li>
						   <li class="breadcrumb-item"><a href="{{ route('admin.banner.index') }}">Banner List</a></li>
                           <li class="breadcrumb-item active">Banner List Details</li>
                        </ul>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-12">
                     <div class="card">
                        <div class="card-header row">
                            <div class="col-lg-6"><h4 class="card-title">Banner List</h4></div>
                            <div class="col-lg-6">
								@if($location)
									<a href="{{ route('admin.createBanner',$location) }}" class="btn btn-success" style="float: right;">+ {{ trans('global.add') }} Banner</a>
								@else
									<a href="{{ route('admin.banner.index') }}">+ {{ trans('global.add') }} Banner</a>
								@endif
                                <a class="btn btn-success" id="openmodel" style="float: right;margin-right: 1%;">Rearrange</a>
							</div>
                        </div>
                        <div class="card-body">
			                      <div class="table-responsive">
                              <button class="btn btn-outline-info waves-effect" id="download-csv">Download CSV</button> <p><br/></p>
                              <div id="data-table-banner-details"></div>
                           </div>
                           <div id="data-table-banner"></div>
								<div class="modal fade" id="dragdrop_model"  role="dialog" >
									<div class="modal-dialog modal-dialog-centered" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title">Banner List</h5>
												<button type="button" class="close" data-bs-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>
											</div>
											<div class="modal-body">
												<table class="table table-bordered">
													<tr>
														<th>Id</th>
														<th>Title</th>
                                                        <th>Location</th>
                                                        <th>Image</th>
                                                        <th>Status</th>
													</tr>
													<tbody class="row_position">
													
													@foreach($banner_details as $row)
														<tr  id="{{ $row->title }}">
															<td>{{ $row->id }}</td>
                                                            <td>{{ $row->title }}</td>
															<td>{{ $row->banner_location }}</td>
                                                            <td>{{ $row->image }}</td>
                                                            <td>{{ $row->status }}</td>
														</tr>
													@endforeach
													</tbody>
												</table>	
											</div>
											<div class="modal-footer">
												<button type="button" onclick="updateOrder()" class="btn btn-primary">Save changes</button>
												<button type="button" data-bs-dismiss="modal" class="btn btn-secondary">Close</button>
											</div>
										</div>
									</div>
								</div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
    <script src="{{ asset('public/Admin/js/jquery-3.6.0.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/feather.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.slimscroll.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/script.js')}}"></script>
      
     <script type="text/javascript" language="javascript" src='https://cdn.datatables.net/buttons/2.2.3/js/dataTables.buttons.min.js' defer></script>
      
      <script type="text/javascript" language="javascript" src='https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js' defer></script>
      
      <script type="text/javascript" language="javascript" src='https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js' defer></script>
      
      <script type="text/javascript" language="javascript" src='https://cdn.datatables.net/buttons/2.2.3/js/buttons.html5.min.js' defer></script> 
      
      <!-- <script type="text/javascript" src="{{ asset('public/Admin/js/3rdparty/tabulator.min.js') }}"></script> -->
      <script src="{{ asset('public/Admin/tabulator/tabulator.min.js') }}"></script>
      <script src="{{ asset('public/Admin/tabulator/jspdf.min.js') }}"></script>
      <script src="{{ asset('public/Admin/tabulator/xlsx.full.min.js') }}"></script>
      <script src="{{ asset('public/Admin/tabulator/jspdf.plugin.autotable.js') }}"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
      <script type="text/javascript">

      $("#openmodel").click(function(){
		  $("#dragdrop_model").modal('show');
		});
		
		$( ".row_position" ).sortable({
			delay: 150,
			stop: function() {
				var selectedData = new Array();
				$('.row_position>tr').each(function() {
					selectedData.push($(this).attr("id"));
				});
				
			}
		});
		function updateOrder() {
			var selectedData = new Array();
			$('.row_position>tr').each(function() {
				selectedData.push($(this).attr("id"));
			});
			
			var edit_url = '{{ route("admin.bannerOrderPosition") }}';
            	  
			$.ajax({
				url:edit_url,
				type:'post',
				data:{position:selectedData},
				headers: {
						'X-CSRF-TOKEN': "{{csrf_token()}}"
					},
				success:function(){
					alert('your change successfully saved');
					window.location.reload();
				}
			});
		}
      
      var tabledata = <?php echo json_encode($banner_details); ?>;
      console.log(tabledata);
      function customFilter(data){
          return data.car && data.rating < 3;
      }

      function updateFilter(){

          var filter = $("#filter-field").val() == "function" ? customFilter : $("#filter-field").val();

          if($("#filter-field").val() == "function" ){
              $("#filter-type").prop("disabled", true);
              $("#filter-value").prop("disabled", true);
          }else{
              $("#filter-type").prop("disabled", false);
              $("#filter-value").prop("disabled", false);
          }

          table.setFilter(filter, $("#filter-type").val(), $("#filter-value").val());
      }

    //Update filters on value change
    $("#filter-field, #filter-type").change(updateFilter);
    $("#filter-value").keyup(updateFilter);
	
	var url = "{{URL::asset('public/images/banners/')}}"+'/';
	
    var table = new Tabulator("#data-table-banner-details", {
        data:tabledata,           //load row data from array
        layout:"fitColumns",      //fit columns to width of table
        responsiveLayout:"collapse",  //hide columns that dont fit on the table
        tooltips:true,            //show tool tips on cells
        addRowPos:"top",          //when adding a new row, add it to the top of the table
        history:true,             //allow undo and redo actions on the table
        pagination:"local",       //paginate the data
        paginationSize:10,         //allow 7 rows per page of data
        movableColumns:true,      //allow column order to be changed
        resizableRows:true,       //allow row order to be changed
        /* initialSort:[             //set the initial sort order of the data
            {column:"id", dir:"desc"},
        ], */
        columns: [
            {title:"Id", field:"id", editor:"label",headerFilter:"input",headerFilterPlaceholder:"Id"},
			{title:"Title", field:"title", editor:"label",headerFilter:"input",headerFilterPlaceholder:"Title"},
			{title:"Location", field:"banner_location", editor:"label",headerFilter:"input",						headerFilterPlaceholder:"Location",
				formatter:function(cell, formatterParams, onRendered){
					var location = cell.getValue();
					return location.replace("_"," ");
				}
			},
			{title:"Image", field:"image", formatter:"image", 
				formatterParams:{height:"50px",width:"50px",urlPrefix:url,urlSuffix:"",}
			},
		    {title:"Status", field:"id_status", 
                formatter:function(cell, formatterParams, onRendered){
					var arr = cell.getValue();
					var data = arr.split("_");
					//console.log(data[1]);
					if(data[1] == '1'){
						return '<div class="status-toggle d-flex justify-content-between align-items-center" onclick="$(\'#is_delete\').val(1)"><input type="checkbox" class="check status_change" data-id="'+data[0]+'" id="ts'+data[0]+'" data-on="Active"  checked ><label for="ts'+data[0]+'" class="checktoggle">checkbox</label></div>';
					} else {
						return '<div class="status-toggle d-flex justify-content-between align-items-center" onclick="$(\'#is_delete\').val(1)"><input type="checkbox" class="check status_change" data-id="'+data[0]+'" id="ts'+data[0]+'" data-off="InActive"><label for="ts'+data[0]+'" class="checktoggle">checkbox</label></div>';
					} 	         
				}
		    },
            {title:"Action",width:170, field:"id", 
			    formatter:function(cell, formatterParams, onRendered) { 
					var edit_url = '{{ route("admin.banner.edit",":id") }}';
					edit_url = edit_url.replace(':id', cell.getValue());

					var delete_url = '{{ route("admin.banner.destroy",":id") }}';
		            delete_url = delete_url.replace(':id', cell.getValue());
					var msg = "Are you sure you want to delete this Banner?";
					var token = '{{ csrf_token() }}';

					var editlink = '<a href="'+edit_url+'" class="btn btn-success text-white"><i class="fa fa-edit"></i></a> <form action="'+delete_url+'" method="POST" onsubmit="return confirm(\''+msg+'\');" style="display: inline-block;"><input type="hidden" name="_method" value="DELETE"><input type="hidden" name="_token" value="'+token+'"><button type="submit" class="btn btn-danger danger-icon-notika btn-reco-mg btn-button-mg waves-effect"> <i class="fa fa-trash"></i></button></form>';
                   
			        return editlink; 
			    },
		    },
        ],
			
    });
	
	$('.status_change').change(function() {
		var status = $(this).prop('checked') == true ? 1 : 0; 
		var model_id = $(this).data('id');
		
		$.ajax({
			type:"GET",
			url:"{{url('admin/changeStatusBanner')}}?status="+status+"&banner_id="+model_id,
			success:function(res){               
				if(res){
					//$('#is_delete').val(0);
					//alert(res);
					console.log(res.success);
				}else{
					//alert("nnnn");
				}
			}
		});
	});


    //trigger download of data.xlsx file
    document.getElementById("download-csv").addEventListener("click", function(){
        table.download("csv", "data.csv");
    });

</script>
   </body>
</html>