<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
      <meta name="description" content="POS - Bootstrap Admin Template">
      <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
      <meta name="author" content="Dreamguys - Bootstrap Admin Template">
      <meta name="robots" content="noindex, nofollow">
      <title>Banner List</title>
      
      <link rel="shortcut icon" type="image/x-icon" href="{{ asset('public/Admin/img/favicon.png')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/bootstrap.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/animate.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/dataTables.bootstrap4.min.css')}}">

      <!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.7.1/css/buttons.dataTables.min.css"> -->
      
       

      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/fontawesome.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/all.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/style.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/select2/css/select2.min.css')}}">
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <link rel="stylesheet" href="{{ asset('public/Admin/tabulator/bootstrap/tabulator_bootstrap4.css')}}">
      
   </head>
   <body>
      <!-- <div id="global-loader">
         <div class="whirly-loader"> </div>
      </div> -->
	  
      <div class="main-wrapper">
        @extends('layouts.header')
     
        @extends('layouts.sidebar')
        
        <div class="page-wrapper">
            <div class="content container-fluid">
               <div class="page-header">
                  <div class="row">
                     <div class="col">
                        <h3 class="page-title">Banner List</h3>
                        <ul class="breadcrumb">
                           <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">{{ trans('global.dashboard') }}</a></li>
                           <li class="breadcrumb-item active">Banner List</li>
                        </ul>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-12">
                     <div class="card">
                        <div class="card-header row">
                            <div class="col-lg-6"><h4 class="card-title">Banner List</h4></div>
                            <!--<div class="col-lg-6">
								<a class="btn btn-success logout " style="float: right;margin-left: 1%;" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('frm-logout').submit();">Login
								</a>
								<form id="frm-logout" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
								<a href="{{ route('admin.home') }}" class="btn btn-success" style="float: right;margin-left: 1%;">Home</a>
								<a class="btn btn-success" id="openmodel" style="float: right;margin-left: 1%;">Rearrange</a>
							</div>-->
                        </div>
                        <div class="card-body">
						
			                <div class="table-responsive">
								<button class="btn btn-outline-info waves-effect" id="download-csv">Download CSV</button> 
								<p><br></p>
								<div id="data-table-banner"></div>
								<div class="modal fade" id="dragdrop_model"  role="dialog" >
									<div class="modal-dialog modal-dialog-centered" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title">Modal title</h5>
												<button type="button" class="close" data-bs-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>
											</div>
											<div class="modal-body">
												<table class="table table-bordered">
													<tr>
														<th>Id</th>
														<th>Name</th>
													</tr>
													<tbody class="row_position">
													
													@foreach($banners as $row)
														<tr  id="{{ $row->banner_location }}">
															<td>{{ $row->id }}</td>
															<td>{{ $row->banner_location }}</td>
														</tr>
													@endforeach
													</tbody>
												</table>	
											</div>
											<div class="modal-footer">
												<button type="button" onclick="updateOrder()" class="btn btn-primary">Save changes</button>
												<button type="button" data-bs-dismiss="modal" class="btn btn-secondary">Close</button>
											</div>
										</div>
									</div>
								</div>
								
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
    <script src="{{ asset('public/Admin/js/jquery-3.6.0.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/feather.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.slimscroll.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/script.js')}}"></script>
    <script src="{{ asset('public/Admin/tabulator/tabulator.min.js') }}"></script>
    <script src="{{ asset('public/Admin/tabulator/jspdf.min.js') }}"></script>
    <script src="{{ asset('public/Admin/tabulator/xlsx.full.min.js') }}"></script>
    <script src="{{ asset('public/Admin/tabulator/jspdf.plugin.autotable.js') }}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
	<script type="text/javascript">
		$("#openmodel").click(function(){
		  $("#dragdrop_model").modal('show');
		});
		
		$( ".row_position" ).sortable({
			delay: 150,
			stop: function() {
				var selectedData = new Array();
				$('.row_position>tr').each(function() {
					selectedData.push($(this).attr("id"));
				});
				
			}
		});
		function updateOrder() {
			var selectedData = new Array();
			$('.row_position>tr').each(function() {
				selectedData.push($(this).attr("id"));
			});
			
			var edit_url = '{{ route("admin.bannerOrderPosition") }}';
            	  
			$.ajax({
				url:edit_url,
				type:'post',
				data:{position:selectedData},
				headers: {
						'X-CSRF-TOKEN': "{{csrf_token()}}"
					},
				success:function(){
					alert('your change successfully saved');
					window.location.reload();
				}
			});
		}
		
		var tabledata = <?php echo json_encode($banners); ?>;
		
		function customFilter(data){
          return data.car && data.rating < 3;
		}

		function updateFilter(){
			var filter = $("#filter-field").val() == "function" ? customFilter : $("#filter-field").val();
			if($("#filter-field").val() == "function" ){
              $("#filter-type").prop("disabled", true);
              $("#filter-value").prop("disabled", true);
			}else{
              $("#filter-type").prop("disabled", false);
              $("#filter-value").prop("disabled", false);
			}
			table.setFilter(filter, $("#filter-type").val(), $("#filter-value").val());
		}

		//Update filters on value change
		$("#filter-field, #filter-type").change(updateFilter);
		$("#filter-value").keyup(updateFilter);

		var table = new Tabulator("#data-table-banner", {
			data:tabledata,           //load row data from array
			layout:"fitColumns",      //fit columns to width of table
			responsiveLayout:"collapse",  //hide columns that dont fit on the table
			tooltips:true,            //show tool tips on cells
			addRowPos:"top",          //when adding a new row, add it to the top of the table
			history:true,             //allow undo and redo actions on the table
			pagination:"local",       //paginate the data
			paginationSize:10,         //allow 7 rows per page of data
			movableColumns:true,      //allow column order to be changed
			resizableRows:true,       //allow row order to be changed
			/* movableRows: true, //enable movable rows
     		rowMoved:function(row){
				$("#dragdrop_model").modal('show');
			}, */
			columns: [
				// { data: "delete_chk", orderable:false, searchable:false},
                {title:"Id", field:"id", editor:"label",headerFilter:"input",headerSort:false,headerFilterPlaceholder:"Id"},
				{title:"Location", field:"banner_location", editor:"label",headerFilter:"input",headerSort:false,headerFilterPlaceholder:"Location",
					formatter:function(cell, formatterParams, onRendered){
						var location = cell.getValue();
						return location.replace("_"," ");
					}
				},
            ],
			rowClick: function (e, row) {
				var link_url= '{{ route('admin.bannerDetails',":id") }}';
				link_url = link_url.replace(':id', row.getData().banner_location);
				window.location.href = link_url;
			}
      });


      //trigger download of data.csv file
      // document.getElementById("download-csv").addEventListener("click", function(){
      //     table.download("csv", "data.csv");
      // });

      //trigger download of data.xlsx file
      document.getElementById("download-csv").addEventListener("click", function(){
          table.download("csv", "data.csv");
      });

      //trigger download of data.pdf file
      // document.getElementById("download-pdf").addEventListener("click", function(){
      //     table.download("pdf", "data.pdf", {
      //         orientation:"portrait", //set page orientation to portrait
      //         title:"Example Report", //add title to report
      //     });
      // });

</script>
   </body>
</html>