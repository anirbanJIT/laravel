<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
      <meta name="description" content="POS - Bootstrap Admin Template">
      <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
      <meta name="author" content="Dreamguys - Bootstrap Admin Template">
      <meta name="robots" content="noindex, nofollow">
      <title>{{ trans('global.edit') }} {{ trans('cruds.banners.title_singular') }}</title>
      
      <link rel="shortcut icon" type="image/x-icon" href="{{ asset('public/Admin/img/favicon.png')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/bootstrap.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/animate.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/dataTables.bootstrap4.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/fontawesome.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/all.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/style.css')}}">
   </head>
   <body>
    <div id="global-loader">
        <div class="whirly-loader"> </div>
    </div>
    <div class="main-wrapper">
        @extends('layouts.header')
        @extends('layouts.sidebar')
        <div class="page-wrapper">
            <div class="content container-fluid">
                <div class="page-header">
					<div class="row">
						<div class="col">
							<h3 class="page-title">{{ trans('global.edit') }} {{ trans('cruds.banners.title_singular') }}</h3>
							<ul class="breadcrumb">
								<li class="breadcrumb-item"><a href="{{route('admin.home')}}">{{ trans('global.dashboard') }}</a></li>
								<li class="breadcrumb-item">
									@if($banners->banner_location)
										<a href="{{ route('admin.bannerDetails',$banners->banner_location) }}">Banner List Details</a>
									@else
										<a href="{{ route('admin.banner.index') }}">Banner List Details</a>
									@endif
								</li>
								<li class="breadcrumb-item active">{{ trans('global.edit') }} {{ trans('cruds.banners.title_singular') }}</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div class="card">
							<div class="card-body">
								<form action="{{ route('admin.banner.update', [$banners->id]) }}" method="POST" enctype="multipart/form-data" id="frm_banner">
									{{ csrf_field() }}
									@method('PUT')
									<div class="form-group">
										<label>Name<span class="text-danger">*</span></label>
										<input type="text" id="title" name="title" value="{{ $banners->title }}" class="form-control @error('title') is-invalid @enderror" placeholder="Name*" >
										@if($errors->has('title'))
											<em class="invalid-feedback text-danger">{{ $errors->first('title') }}</em>
										@endif
									</div>
									<div class="form-group">
										<label>Link<span class="text-danger">*</span></label>
										<input type="text" id="ex_link" name="ex_link" value="{{ $banners->ex_link }}" class="form-control @error('ex_link') is-invalid @enderror" placeholder="{{ trans('cruds.banners.fields.ex_link') }}" >
										@if($errors->has('ex_link'))
											<em class="invalid-feedback text-danger">{{ $errors->first('ex_link') }}</em>
										@endif
									</div>
									<div class="form-group">
										<label>Banner<span class="text-danger">*</span></label>
										<select class="form-control @error('banner_location') is-invalid @enderror" name="banner_location" id="banner_location">
											<option value="login_banner" {{ $banners->banner_location == 'login_banner' ? 'selected' : '' }}>Login Banner</option>
											<option value="home_banner" {{ $banners->banner_location == 'home_banner' ? 'selected' : '' }}>Home Banner</option>
										</select>
										@if($errors->has('banner_location'))
											<em class="invalid-feedback text-danger">{{ $errors->first('banner_location') }}</em>
										@endif
									</div>
									<div class="form-group">
										<label>Upload image</label>
										<input type="file" accept="image/*" name="image" id="image" class="form-control">
										<span id="preview">
											@if(!empty($banners->image))
												<img src="{{ asset('Admin/images/banners/'.$banners->image) }}" width="50" height="50">
											@endif
											<input type="hidden" name="imageUpdate" id = "imageUpdate" value="{{ $banners->image }}" >
										</span>
									</div>
									<div>
										<button type="submit" class="btn btn-success">Save</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
            </div>
        </div>
    </div>
    <script src="{{ asset('public/Admin/js/jquery-3.6.0.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/feather.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.slimscroll.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{ asset('public/Admin/plugins/apexchart/apexcharts.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/script.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
    <script>
      /* $(function(){
      $("#frm_banner").validate({
          rules: {
            title: {
               required: true,
            }
        },
         messages: { 
              title: {
                  required: "Please enter title"
              }
          },
      });
   }); */
		$('#image').change(function(){
			const file = this.files[0];
			if (file){
				let reader = new FileReader();
				reader.onload = function(event){
					$("#preview").html('<img id="imgPreview" hight="50px" width="50px" src="'+event.target.result+'" alt="pic" />');
				}
			reader.readAsDataURL(file);
			}
		});
      </script>
   </body>
</html>