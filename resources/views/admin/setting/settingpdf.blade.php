<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
      <meta name="description" content="POS - Bootstrap Admin Template">
      <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
      <meta name="author" content="Dreamguys - Bootstrap Admin Template">
      <meta name="robots" content="noindex, nofollow">
	  <meta name="csrf-token" content="{{ csrf_token() }}" />
      <title>Setting Pdf</title>
      
      <link rel="shortcut icon" type="image/x-icon" href="{{ asset('public/Admin/img/favicon.png')}}">
      
      <link rel="stylesheet" href="{{ asset('public/Admin/css/bootstrap.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/select2/css/select2.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/animate.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/dataTables.bootstrap4.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/fontawesome.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/all.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/style.css')}}">
      <style>
	  ul, ol, li {
    margin: 0;
    padding: 0;
    list-style: none;
}
.reorder_link {
    color: #3675B4;
    border: solid 2px #3675B4;
    border-radius: 3px;
    text-transform: uppercase;
    background: #fff;
    font-size: 18px;
    padding: 10px 20px;
    margin: 15px 15px 15px 0px;
    font-weight: bold;
    text-decoration: none;
    transition: all 0.35s;
    -moz-transition: all 0.35s;
    -webkit-transition: all 0.35s;
    -o-transition: all 0.35s;
    white-space: nowrap;
}
.reorder_link:hover {
    color: #fff;
    border: solid 2px #3675B4;
    background: #3675B4;
    box-shadow: none;
}
#reorder-helper{
    margin: 18px 10px;
    padding: 10px;
}
.light_box {
    background: #efefef;
    padding: 20px;
    margin: 15px 0;
    text-align: center;
    font-size: 1.2em;
}

/* image gallery */
.gallery{
    width:100%;
    float:left;
    margin-top:15px;
}
.gallery ul{
    margin:0;
    padding:0;
    list-style-type:none;
}
.gallery ul li{
    padding:7px;
    border:2px solid #ccc;
    float:left;
    margin:10px 7px;
    background:none;
    width:auto;
    height:auto;
}
.gallery img{
    width:200px;
	height:200px;
}

/* notice box */
.notice, .notice a{
    color: #fff !important;
}
.notice {
    z-index: 8888;
    padding: 10px;
    margin-top: 20px;
}
.notice a {
    font-weight: bold;
}
.notice_error {
    background: #E46360;
}
.notice_success {
    background: #657E3F;
}
	  </style>
   </head>
   <body>
      <div id="global-loader">
         <div class="whirly-loader"> </div>
      </div>
      <div class="main-wrapper">
        @extends('layouts.header')
     
        @extends('layouts.sidebar')
        
        <div class="page-wrapper">
            <div class="content container-fluid">
				<div class="page-header">
					<div class="row">
						<div class="col">
							<h3 class="page-title">Setting Pdf</h3>
							<ul class="breadcrumb">
								<li class="breadcrumb-item"><a href="{{route('admin.home')}}">{{ trans('global.dashboard') }}</a></li>
								<li class="breadcrumb-item active">Setting Pdf</li>
							</ul>
						</div>
					</div>
				</div>
                <div class="row">
					<div class="col-sm-12">
						<div class="card">
							<div class="card-body">
								<a href="{{URL::asset('Admin/settingpdf/'.$settingPDF[0]->settingpdf)}}" class="btn btn-primary" target="_blank">Download Setting pdf</a>
                                <br/><br/>
								<form action="{{ route('admin.setting.settingpdf-update') }}" method="POST" enctype="multipart/form-data">
                           			{{ csrf_field() }}
									<div class="form-group">
										<label>Setting Upload Multiple Images</label>
                                 		<input type="file"  id="settingpdf" name="settingpdf[]"  class="form-control" required multiple>
										@if($errors->has('settingpdf')) 
											<em class="invalid-feedback"> {{ $errors->first('settingpdf') }} </em> 
										@endif
									</div>
									<div class="">
										<button type="submit" class="btn btn-success">Save</button>
									</div>
									@if(session()->has('success'))
										<div class="alert alert-success alert-dismissible">
											<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
											<strong>Success!</strong> {{ session()->get('success') }}
										</div>
									@endif
									@if(session()->has('error'))
										<div class="alert alert-danger alert-dismissible">
											<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
											<strong>Validation!</strong> {{ session()->get('error') }}
										</div>
									@endif
								</form>
								
								<div class="mt-5 mb-2">
									<a href="javascript:void(0);" class="reorder_link" id="saveReorder">reorder photos</a>
									<div id="reorderHelper" class="light_box" style="display:none;">1. Drag photos to reorder.<br>2. Click 'Save Reordering' when finished.</div>
									<div class="gallery">
										<ul class="reorder_ul reorder-photos-list">
											@foreach($settingPDF as $row)
												<li id="image_li_{{ $row->id }}" class="ui-sortable-handle">
													<a href="javascript:void(0);" style="float:none;" class="image_link">
														<img src="{{ asset('Admin/settingpdf/'.$row->settingpdf) }}" alt="" width="100px" height="100px">
														
													</a>
													<button onclick="deleteImage({{ $row->id }})" class="btn btn-danger"><i class="fa fa-trash"></i></button>
												</li>
											@endforeach
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
            </div>
        </div>
    </div>
     <script src="{{ asset('public/Admin/js/jquery-3.6.0.min.js')}}"></script>
    <script src="{{ asset('public/Admin/plugins/select2/js/select2.min.js')}}"></script>
    <script src="{{ asset('public/Admin/plugins/select2/js/custom-select.js')}}"></script>
    <script src="{{ asset('public/Admin/js/feather.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.slimscroll.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/script.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
	
	
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script>
		$('.alert-success').show();
		setTimeout(function () {
			 $('.alert-success').hide();
			 $('.alert-danger').hide();
		}, 2000);
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});	
		
		function deleteImage(id){
			//alert(id);
			$.ajax({
				type: "POST",
				url: '{{ route("admin.setting.settingpdf-delete") }}',
				data: {id:id},
				success: function(data){
					
					if(data.success == true)
						window.location.reload();
				}
			});
		}
		
		$('.reorder_link').on('click',function(){
			
			$("ul.reorder-photos-list").sortable({ tolerance: 'pointer' });
			$('.reorder_link').html('save reordering');
			$('.reorder_link').attr("id","saveReorder");
			$('#reorderHelper').slideDown('slow');
			$('.image_link').attr("href","javascript:void(0);");
			$('.image_link').css("cursor","move");
        
			$("#saveReorder").click(function( e ){
				
				if( !$("#saveReorder i").length ){
					var url = "{{asset('images/refresh-animated.gif') }}";
					$(this).html('').prepend('<img src="'+url+'"/>');
					$("ul.reorder-photos-list").sortable('destroy');
					$("#reorderHelper").html("Reordering Photos - This could take a moment. Please don't navigate away from this page.").removeClass('light_box').addClass('notice notice_error');
					
					var h = [];
					$("ul.reorder-photos-list li").each(function() {
						h.push($(this).attr('id').substr(9));
					});
					
					$.ajax({
						type: "POST",
						url: '{{ route("admin.setting.settingpdf-order") }}',
						data: {ids: " " + h + ""},
						success: function(data){
							
							if(data.success == true)
								window.location.reload();
						}
					});	
					return false;
				}	
				e.preventDefault();
			});
		});


	</script>
   </body>
</html>