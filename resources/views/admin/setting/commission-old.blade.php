@extends('layouts.admin')
@section('content')
<div class="breadcomb-area">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="breadcomb-list">
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<div class="breadcomb-wp">
								<div class="breadcomb-icon">
									<i class="notika-icon notika-windows"></i>
								</div>
								<div class="breadcomb-ctn">
									<h2>Manage Commission Setting</h2>
									<p>Here <span class="bread-ntd">you can manage commission setting</span></p>
								</div>
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-3">
							<div class="breadcomb-report">
							<a class="btn btn-success notika-btn-success waves-effect" href="{{ url()->previous() }}"> {{ trans('global.back_to_list') }}</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
	
<div class="form-element-area">
  <div class="container">
  <form action="{{ route('admin.setting.commission-setting') }}" method="POST" enctype="multipart/form-data">
  <input type="hidden" name="id" value="{{ $data->id ?? ''}}">
  <input type="hidden" name="user_id" value="{{ $user_id ?? ''}}">
            @csrf
			@method('PUT')
   <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="form-element-list">
                        <div class="basic-tb-hd">
                        </div>
                       
                        <div class="row">
									

                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group ic-cmp-int">
                                    <div class="form-ic-cmp">
                                        <i class="notika-icon notika-dollar"></i>
                                    </div>
                                    <div class="nk-int-st">
										  <input type="text" id="commission" name="commission" class="form-control" value="{{ old('commission', $data->commission ?? '') }}" placeholder="Commission Percent*" required>
										  </div>
										@if($errors->has('commission'))
											<em class="invalid-feedback">
												{{ $errors->first('commission') }}
											</em>
										@endif
										
                                </div>
                            </div>
							{{-- <!--<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <div class="nk-int-mk sl-dp-mn mg-t-10">
                                    
                                </div>
                                <div class="bootstrap-select">
                                    <select class="selectpicker" name="commission_get_on" id="commission_get_on"  required >
									<option>Select Commission Get On</option>
									
									<option value="1" {{($data->commission_get_on=='1') ? 'selected' : ''}}>Own Recharge</option>
									<option value="2" {{($data->commission_get_on=='2') ? 'selected' : ''}}>Referral Recharge</option>
									<option value="3" {{($data->commission_get_on=='3') ? 'selected' : ''}}>Both</option>
									</select>
									</div>
							</div>-->--}}
									 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group ic-cmp-int">
                                    <div class="form-ic-cmp">
                                        <i class="notika-icon notika-phone"></i>
                                    </div>
                                    <div class="nk-int-st">
										  <input type="text"  name="minimum_recharge" class="form-control" value="{{ old('minimum_recharge', isset($data) ? $data->minimum_recharge : '') }}" placeholder="Minimum Recharge Amount*" required>
										  </div>
										@if($errors->has('minimum_recharge'))
											<em class="invalid-feedback">
												{{ $errors->first('minimum_recharge') }}
											</em>
										@endif
										
                                </div>
                            </div>
									
									</div>
									<div class="row">
                          
							 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="nk-int-mk sl-dp-mn mg-t-10">
                                    
                                </div>
								
                                <div class="bootstrap-select">
                                    <select class="selectpicker" name="recharge_month" id="recharge_month"  required >
									<option value="">Select Commission Duration</option>
									@if($data)
									@for($i=1;$i<=5;$i++)
										<option value="{{$i}}" {{($data->recharge_month==$i) ? 'selected' : ''}}>{{$i}}</option>
									@endfor
									@else
										@for($i=1;$i<=5;$i++)
										<option value="{{$i}}" >{{$i}}</option>
									@endfor
									@endif
									</select>
									</div>
									</div>
									 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="nk-int-mk sl-dp-mn mg-t-10">
                                    
                                </div>
                                <div class="bootstrap-select">
                                    <select class="selectpicker" name="status" id="status"  required >
									<option value="">Select Status</option>
									@if($data)
									<option value="0" {{($data->status=='0') ? 'selected' : ''}}>Inactive</option>
									<option value="1" {{($data->status=='1') ? 'selected' : ''}}>Active</option>
									@else
									<option value="0">Inactive</option>
									<option value="1">Active</option>
									@endif
									</select>
									</div>
									</div>
									<div class="clear">&nbsp;</div>
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                           
                            <input type="submit" class="btn btn-success notika-btn-success waves-effect" value="{{ trans('global.save') }}" >
                        </div>
                        </div>
                     
                      
                    </div>
                </div>
            </div>
</form>			
			</div>
            </div>
			@endsection