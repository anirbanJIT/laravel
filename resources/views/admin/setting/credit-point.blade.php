@extends('layouts.admin')
@section('content')
<div class="breadcomb-area">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="breadcomb-list">
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<div class="breadcomb-wp">
									<div class="breadcomb-icon">
										<i class="notika-icon notika-windows"></i>
									</div>
									<div class="breadcomb-ctn">
										<h2>Manage Credit Point Setting</h2>
										<p>Here <span class="bread-ntd">you can manage credit point setting</span></p>
									</div>
								</div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-3">
								<div class="breadcomb-report">
								
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
<div class="form-element-area">
  <div class="container">
  <form action="{{ route('admin.setting.credit-point-setting') }}" method="POST" enctype="multipart/form-data">
  <input type="hidden" name="id" value="{{ $data->id}}">
            @csrf
			@method('PUT')
   <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="form-element-list">
                        <div class="basic-tb-hd">
                            <!--<h2>{{ trans('global.create') }} {{ trans('cruds.user.title_singular') }}</h2>
                            <p>Text Inputs with different sizes by height and column.</p>-->
                        </div>
                       
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <div class="form-group ic-cmp-int">
                                    <div class="form-ic-cmp">
                                        <i class="notika-icon notika-credit-card"></i>
                                    </div>
                                    <div class="nk-int-st">
										  <input type="text" readonly id="credit" name="credit" class="form-control" value="{{ old('credit', isset($data) ? $data->credit : '') }}" placeholder="Credit*" required>
										  </div>
										@if($errors->has('credit'))
											<em class="invalid-feedback">
												{{ $errors->first('credit') }}
											</em>
										@endif
										
                                </div>
                            </div>
                           <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <div class="form-group ic-cmp-int">
                                    <div class="form-ic-cmp">
                                        <i class="notika-icon notika-dollar"></i>
                                    </div>
                                    <div class="nk-int-st">
										  <input type="text"  name="amount" class="form-control" value="{{ old('amount', isset($data) ? $data->amount : '') }}" placeholder="Amount*" required>
										  </div>
										@if($errors->has('amount'))
											<em class="invalid-feedback">
												{{ $errors->first('amount') }}
											</em>
										@endif
										
                                </div>
                            </div>
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                           
                            <input type="submit" class="btn btn-success notika-btn-success waves-effect" value="{{ trans('global.save') }}" >
                        </div>
                        </div>
                     
                      
                    </div>
                </div>
            </div>
</form>			
			</div>
            </div>
			@endsection