@extends('layouts.admin')
@section('content')
<div class="breadcomb-area">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="breadcomb-list">
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<div class="breadcomb-wp">
								<div class="breadcomb-icon">
									<i class="notika-icon notika-windows"></i>
								</div>
								<div class="breadcomb-ctn">
									<h2>Manage Commission Setting</h2>
									<p>Here <span class="bread-ntd">you can manage commission setting</span></p>
								</div>
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-3">
							<div class="breadcomb-report">
								<a class="btn btn-success notika-btn-success waves-effect" href="{{ url()->previous() }}"> {{ trans('global.back_to_list') }}</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="form-element-area">
	<div class="container">
		<form action="{{ route('admin.setting.commission-setting') }}" method="POST" enctype="multipart/form-data">
			<input type="hidden" name="user_id" value="{{ $user_id ?? ''}}">
			@csrf
			@method('PUT')
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="form-element-list">
						@foreach($data as $key => $plan_commission)
						@php
						$i = $key;
						$plan_commission = $plan_commission->getCommissionSettingDetail($plan_commission->commission_plan_id_from,$plan_commission->commission_plan_id_to,$user_id) ?? $plan_commission;
						@endphp
						<div class="row">
							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
								<div class="nk-int-mk sl-dp-mn mg-t-10">
                                    <label> {{ $plan_commission->planfrom->name ?? '' }} To {{ $plan_commission->planto->name ?? '' }} {{ trans('cruds.plan.fields.discount_value') }} *</label>
                                </div>
                                <div class="form-group">
									<div class="nk-int-st">
										<!-- <input type="text" name="commission_plan[{{$i}}][id]" value="{{ isset($plan_commission->id) ? $plan_commission->id : '' }}"> -->

										<input type="hidden" name="commission_plan[{{$i}}][user_id]" value="{{ isset($user_id) ? $user_id : '' }}">

										<input type="hidden" name="commission_plan[{{$i}}][commission_plan_id_from]" value="{{ isset($plan_commission->commission_plan_id_from) ? $plan_commission->commission_plan_id_from : '' }}">

										<input type="hidden" name="commission_plan[{{$i}}][commission_plan_id_to]" value="{{ isset($plan_commission->commission_plan_id_to) ? $plan_commission->commission_plan_id_to : '' }}">
										
										<input type="text" id="discount_value" name="commission_plan[{{$i}}][discount_value]" value="{{ $plan_commission->discount_value ?? $plan_commission->commission }}" class="form-control" placeholder="{{ trans('cruds.plan.fields.create_plan_to') }} {{ trans('cruds.plan.fields.discount_value') }}*" onkeyup="this.value = this.value.replace(/[^0-9\.]/g,'');" data-mask="99.99" required>
									</div>
									@if ($errors->has('commission_plan'.$i.'discount_value'))
									<span class="text-danger">{{ $errors->first('commission_plan.$i.discount_value') }}</span>
									@endif
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
								<div class="nk-int-mk sl-dp-mn mg-t-10">
                                    <label>{{ trans('cruds.plan.fields.discount_type') }}*</label>
                                </div>
								<div class="form-group">
									<div class="nk-int-st">
										@if(isset($plan_commission->discount_type))
										<select class="selectpicker" id="discount_type" name="commission_plan[{{$i}}][discount_type]" required>
										    <option value="" selected>{{ trans('cruds.plan.fields.discount_type') }}*</option>
										    <option value="2" {{ $plan_commission->discount_type == '0' ? 'selected' : '' }} >Percentage</option>
										    <option value="1" {{ $plan_commission->discount_type == '1' ? 'selected' : '' }} >Fixed</option>
										</select>
										@else
										<select class="selectpicker" id="discount_type" name="commission_plan[{{$i}}][discount_type]" required>
										    <option value="" selected>{{ trans('cruds.plan.fields.discount_type') }}*</option>
										    <option value="2" {{ $plan_commission->commission_type == '2' ? 'selected' : '' }} >Percentage</option>
										    <option value="1" {{ $plan_commission->commission_type == '1' ? 'selected' : '' }} >Fixed</option>
										</select>
										@endif
									</div>
									@if ($errors->has('commission_plan'.$i.'discount_type'))
									<span class="text-danger">{{ $errors->first('commission_plan'.$i.'discount_type') }}</span>
									@endif
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
								<div class="nk-int-mk sl-dp-mn mg-t-10">
                                    <label>{{ trans('cruds.plan.fields.minimum_recharge')}}*</label>
                                </div>
                                <div class="form-group">
									<div class="nk-int-st">
										<input type="text" id="minimum_recharge" name="commission_plan[{{$i}}][minimum_recharge]" value="{{ $plan_commission->minimum_recharge }}" class="form-control" placeholder="{{ trans('cruds.plan.fields.minimum_recharge')}}*" onkeyup="this.value = this.value.replace(/[^0-9\.]/g,'');" data-mask="99.99" required>
									</div>
									@if ($errors->has('commission_plan'.$i.'minimum_recharge'))
									<span class="text-danger">{{ $errors->first('commission_plan.$i.minimum_recharge') }}</span>
									@endif
								</div>
							</div>
						</div>	
						@endforeach						
						<div class="row mg-t-10">
							<div class="col-lg-8 col-md-7 col-sm-7 col-xs-12">
								<button input type="sumit" name="submit" class="btn btn-success notika-btn-success" id="submitBtn">Submit</button>
                            </div>
                        </div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
@endsection