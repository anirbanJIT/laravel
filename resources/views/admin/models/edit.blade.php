<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
      <meta name="description" content="POS - Bootstrap Admin Template">
      <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
      <meta name="author" content="Dreamguys - Bootstrap Admin Template">
      <meta name="robots" content="noindex, nofollow">
      <title>Update Model</title>
      
      <link rel="shortcut icon" type="image/x-icon" href="{{ asset('public/Admin/img/favicon.png')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/bootstrap.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/animate.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/dataTables.bootstrap4.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/fontawesome.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/all.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/style.css')}}">
   </head>
   <body>
    <div id="global-loader">
        <div class="whirly-loader"> </div>
    </div>
    <div class="main-wrapper">
        @extends('layouts.header')
        @extends('layouts.sidebar')
        <div class="page-wrapper">
            <div class="content container-fluid">
				<div class="page-header">
					<div class="row">
						<div class="col">
							<h3 class="page-title">Update Model</h3>
							<ul class="breadcrumb">
							    <li class="breadcrumb-item"><a href="{{route('admin.home')}}">{{ trans('global.dashboard') }}</a></li>
							    <li class="breadcrumb-item">
									@if($brandModel->brand_id)
										<a href="{{ route('admin.models.show',$brandModel->brand_id) }}">{{ trans('cruds.models.title') }}</a>
									@else
										<a href="{{ route('admin.brand-type.index') }}">{{ trans('cruds.models.title') }}</a>
									@endif
								</li>
								<li class="breadcrumb-item active">Update Model</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div class="card">
							<div class="card-body">
								<form action="{{ route('admin.models.update', [$brandModel->id]) }}" method="POST" enctype="multipart/form-data">
									{{ csrf_field() }}
									@method('PUT')
									<input type="hidden" name="get_brand_id" value="{{ $brandModel->brand_id }}">
									<div class="form-group">
										<label>Category<span class="text-danger">*</span></label>
										<select class="form-control @error('brand_type_id') is-invalid @enderror" name="brand_type_id" id="brand_type_id"  >
											<option value="">Select Category</option>
											@foreach($brand_types as $id => $brand_type)
												<option value="{{ $brand_type->id }}" {{ (isset($brandModel) && $brandModel->brand_type_id ? $brandModel->brand_type_id : old('brand_type_id')) == $brand_type->id ? 'selected' : '' }} >{{ $brand_type->name }}</option>
											@endforeach
										</select>
										@if($errors->has('brand_type_id'))
											<em class="invalid-feedback">{{ $errors->first('brand_type_id') }}</em>
										@endif
									</div>
									<div class="form-group">
										<label>Brands<span class="text-danger">*</span></label>
										<select class="form-control @error('brand_id') is-invalid @enderror" name="brand_id" id="brand_id"   >
											<option value="">Select Brand </option>
											@foreach($brands as $id => $brand)
												<option value="{{ $brand->id }}" {{ (isset($brandModel) && $brandModel->brand_id ? $brandModel->brand_id : old('brand_id')) == $brand->id ? 'selected' : '' }} >{{ $brand->name }}</option>
											@endforeach
										</select>
										@if($errors->has('brand_id'))
											<em class="invalid-feedback">{{ $errors->first('brand_id') }}</em>
										@endif
									</div>
									<div class="form-group">
										<label>Name<span class="text-danger">*</span></label>
										<input type="text" class="form-control @error('name') is-invalid @enderror" value="{{ old('name', isset($brandModel) ? $brandModel->name : '') }}" placeholder="Model Name" id="name" name="name">
										@if($errors->has('name'))
											<em class="invalid-feedback">{{ $errors->first('name') }}</em>
										@endif
									</div>
									<div class="form-group">
										<label>Status<span class="text-danger">*</span></label>
										<select class="form-control @error('status') is-invalid @enderror" name="status" id="status"  required >
											<option>Select Status</option>
											<option value="0" {{ (isset($brandModel) && $brandModel->status ? $brandModel->status : old('status')) == 0 ? 'selected' : '' }} >Inactive</option>
											<option value="1" {{ (isset($brandModel) && $brandModel->status ? $brandModel->status : old('status')) == 1 ? 'selected' : 'selected' }}>Active</option>
										</select>
										@if($errors->has('status'))
											<em class="invalid-feedback">{{ $errors->first('status') }}</em>
										@endif										
									</div>
									<div class="form-group">
										<label>Upload image</label>
										<input type="file" accept="image/*" class="form-control" name="fileUpload" id="inputImage">
										<span id="preview">
											@if(isset($brandModel) && $brandModel->image )
												<a href="{{ asset('Admin/images/models/'.$brandModel->image)}}" data-lightbox="lightbox-1"> 
													<img src="{{ asset('Admin/images/models/'.$brandModel->image)}}" alt="image-preview" width="50"/>
												</a>
											@endif
										</span>									 
									</div>
									<div class="form-group">
										<label>Meta Title</label>
										<input type="text" class="form-control" placeholder="Meta Title" id="metaTitle" name="metaTitle" value="{{$brandModel->meta_title}}">
										@if($errors->has('meta_title')) <em class="invalid-feedback"> {{ $errors->first('meta_title') }} </em> @endif
									</div>
									<div class="form-group">
										<label>Meta Keyword</label>
										<input type="text" class="form-control"  placeholder="Meta Keyword" id="metaKeyword" name="metaKeyword" value="{{$brandModel->meta_keywords}}">
										@if($errors->has('meta_keywords')) <em class="invalid-feedback"> {{ $errors->first('meta_keywords') }} </em> @endif
									</div>
									<div class="form-group">
										<label>Meta Description</label>
										<input type="text" class="form-control"  placeholder="Meta Keyword" id="metaDescription" name="metaDescription" value="{{$brandModel->meta_description}}">
										@if($errors->has('meta_description')) <em class="invalid-feedback"> {{ $errors->first('meta_description') }} </em> @endif
									</div>
									<div>
										<button type="submit" class="btn btn-success">Save</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
            </div>
        </div>
    </div>
    <script src="{{ asset('public/Admin/js/jquery-3.6.0.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/feather.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.slimscroll.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{ asset('public/Admin/plugins/apexchart/apexcharts.min.js')}}"></script>
    <script src="{{ asset('public/Admin/js/script.js')}}"></script>
	<script>
		
		$('#inputImage').change(function(){
			const file = this.files[0];
			
			console.log(file);
			if (file){
			let reader = new FileReader();
			reader.onload = function(event){
				console.log(event.target.result);
				$("#preview").html('<img id="imgPreview" hight="50px" width="50px" src="'+event.target.result+'" alt="pic" />');
				//$('#imgPreview').attr('src', );
			}
			reader.readAsDataURL(file);
			}
		});
		

	</script>
   </body>
</html>