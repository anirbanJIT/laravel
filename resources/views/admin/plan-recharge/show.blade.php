<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
		<meta name="description" content="POS - Bootstrap Admin Template">
		<meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
		<meta name="author" content="Dreamguys - Bootstrap Admin Template">
		<meta name="robots" content="noindex, nofollow">
		<title>{{ trans('global.show') }} Plan Recharge</title>
      
		<link rel="shortcut icon" type="image/x-icon" href="{{ asset('public/Admin/img/favicon.png')}}">
		<link rel="stylesheet" href="{{ asset('public/Admin/css/bootstrap.min.css')}}">
		<link rel="stylesheet" href="{{ asset('public/Admin/css/animate.css')}}">
		<link rel="stylesheet" href="{{ asset('public/Admin/css/dataTables.bootstrap4.min.css')}}">
		<link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/fontawesome.min.css')}}">
		<link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/all.min.css')}}">
		<link rel="stylesheet" href="{{ asset('public/Admin/css/style.css')}}">
	</head>
	<body>
		<div id="global-loader">
			<div class="whirly-loader"> </div>
		</div>
		<div class="main-wrapper">
			@extends('layouts.header')
			@extends('layouts.sidebar')
        	<div class="page-wrapper">
				<div class="content container-fluid">
					<div class="page-header">
						<div class="row">
							<div class="col">
								<h3 class="page-title">{{ trans('global.show') }} Plan Recharge</h3>
								<ul class="breadcrumb">
								   <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">{{ trans('global.dashboard') }}</a></li>
								   <li class="breadcrumb-item"><a href="{{ route('admin.planrecharge.index') }}">Plan Recharges</a></li>
								   <li class="breadcrumb-item active">{{ trans('global.show') }} Plan Recharge</li>
								</ul>
							</div>
						</div>
					</div>
				    <div class="row">
						<div class="col-sm-12">
							<div class="card">
								<div class="card-header row">
									<div class="col-lg-6"><h4 class="card-title">{{ trans('global.show') }} Plan Recharge</h4></div>
									<div class="col-lg-6"><a href="{{ url()->previous() }}" class="btn btn-success" style="float: right;">{{ trans('global.back_to_list') }}</a></div>
								</div>
								<div class="card-body">
									<div class="table-responsive">
										<table class="table table-border" style="width:100%;">
											<tbody>
												<tr>
												   <td style="width: 18%;">Id</th>
												   <td>{{ $planrecharge->id }}</th>
												</tr>
												<tr>
												   <td style="width: 18%;">Name</td>
												   <td>{{ $planrecharge->name }}</td>
												</tr>
												<tr>
												    <td>Categories</td>
												    <td>
														@foreach($category as $r) 
															{{ $r->name.", "}}
														@endforeach
												    </td>
												</tr>
												<tr>
												    <td>Amount</td>
												    <td>{{ isset($planrecharge->amount) ? $planrecharge->amount : "0" }}</td>
												</tr>
												<tr>
												    <td>Yearly Charges</td>
												    <td>{{ isset($planrecharge->yearly_charges) ? $planrecharge->yearly_charges : "-" }}</td>
												</tr>
												<tr>
													<td>Status</td>
													<td>
														@if($planrecharge->status == '1')
															{{ 'Enable' }}
														@else
															{{ 'Disable' }}
														@endif
													</td>
												</tr>
											</tbody>
										</table>

                                        <table class="table" style="width:100%;">
											<thead>
												<tr>
													<th style="width:18%;">Plotter</th>
													<th>Speed</th>
                                                    <th>Pressure</th>
												</tr>
											</thead>
											<tbody id="add_plotter">
												@php
													$i=0;
													$speed = json_decode($planrecharge->speed);
                                                    $j=0;
													$pressure = json_decode($planrecharge->pressure);
												@endphp
												@foreach($plotter as $r)
													<tr>
														<td>
															<input type='hidden' name='plotter_arr_ids[]' value='{{ $r["id"] }}'>
															<strong>{{ $r->name }}</strong>
														</td>
														<td>
															<input type='number' style="background:none; border:none;" class="form-control" name='plotter_speed_arr[]' min="0" max='{{ $r["speed"] }}' value='{{ $speed[$i++] }}'disabled>
														</td>
                                                        <td>
															<input type='number' style="background:none; border:none;" class="form-control" name='plotter_pressure_arr[]' min="0" max='{{ $r["pressure"] }}' value='{{ $pressure[$j++] }}' disabled>
														</td>
													</tr>
												@endforeach
											</tbody>
										</table>

									</div>
                                    
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
        <script>
		
			$("#plotter").change(function(){
				var speed = [];
				$("input[name='plotter_speed_arr[]']").each(function() {
					speed.push($(this).val());
				});
				console.log(speed);
                var pressure = [];
				$("input[name='plotter_pressure_arr[]']").each(function() {
					pressure.push($(this).val());
				});
				console.log(pressure);
				$("#add_plotter").html("");
				var i =0;
                var j =0;
				$("#plotter :selected").each(function() {
					$("#add_plotter").append("<tr><td><input type='hidden' name='plotter_arr_ids[]' value='"+this.value+"'><input type='text' name='plotter_arr[]' value='"+this.text+"' readonly disabled style='cursor: default;background-color: -internal-light-dark(rgba(239, 239, 239, 0.3), rgba(59, 59, 59, 0.3));color: -internal-light-dark(rgb(84, 84, 84), rgb(170, 170, 170));border-color: rgba(118, 118, 118, 0.3);'></td><td><input type='text' name='plotter_speed_arr[]' required value='"+(speed[i++] ?? '' )+"'></td><td><input type='text' name='plotter_pressure_arr[]' required value='"+(pressure[j++] ?? '' )+"'></td></tr>");
				});
				
				
			});
			$(document).ready(function() {
				$('#categories').select2();
				$('#plotter').select2();
			});
		</script>
		<script src="{{ asset('public/Admin/js/jquery-3.6.0.min.js')}}"></script>
		<script src="{{ asset('public/Admin/js/feather.min.js')}}"></script>
		<script src="{{ asset('public/Admin/js/jquery.slimscroll.min.js')}}"></script>
		<script src="{{ asset('public/Admin/js/jquery.dataTables.min.js')}}"></script>
		<script src="{{ asset('public/Admin/js/dataTables.bootstrap4.min.js')}}"></script>
		<script src="{{ asset('public/Admin/js/bootstrap.bundle.min.js')}}"></script>
		<script src="{{ asset('public/Admin/plugins/apexchart/apexcharts.min.js')}}"></script>
		<script src="{{ asset('public/Admin/plugins/apexchart/chart-data.js')}}"></script>
		<script src="{{ asset('public/Admin/js/script.js')}}"></script>
	</body>
</html>