<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
		<meta name="description" content="POS - Bootstrap Admin Template">
		<meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
		<meta name="author" content="Dreamguys - Bootstrap Admin Template">
		<meta name="robots" content="noindex, nofollow">
		<title>Create Plan Recharge</title>
      
		<link rel="shortcut icon" type="image/x-icon" href="{{ asset('public/Admin/img/favicon.png')}}">
		<link rel="stylesheet" href="{{ asset('public/Admin/css/bootstrap.min.css')}}">
		<link rel="stylesheet" href="{{ asset('public/Admin/plugins/select2/css/select2.min.css')}}">
		<link rel="stylesheet" href="{{ asset('public/Admin/css/animate.css')}}">
		<link rel="stylesheet" href="{{ asset('public/Admin/css/dataTables.bootstrap4.min.css')}}">
		<link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/fontawesome.min.css')}}">
		<link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/all.min.css')}}">
		<link rel="stylesheet" href="{{ asset('public/Admin/css/style.css')}}">
	</head>
	<body>
		<div id="global-loader">
			<div class="whirly-loader"> </div>
		</div>
		<div class="main-wrapper">
			@extends('layouts.header')
			@extends('layouts.sidebar')
        	<div class="page-wrapper">
				<div class="content container-fluid">
					<div class="page-header">
						<div class="row">
							<div class="col">
								<h3 class="page-title">Create Plan Recharge</h3>
								<ul class="breadcrumb">
								   <li class="breadcrumb-item"><a href="{{route('admin.home')}}">{{ trans('global.dashboard') }}</a></li>
								   <li class="breadcrumb-item"><a href="{{ route('admin.planrecharge.index') }}">Plan Recharges</a></li>
								   <li class="breadcrumb-item active">Create Plan Recharge</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<div class="card">
								<div class="card-body">
									<form action="{{ route('admin.planrecharge.store') }}" method="POST" enctype="multipart/form-data">
										{{ csrf_field() }}
										<div class="form-group">
											<label>Name<span class="text-danger">*</span></label>
											<input type="text" class="form-control @error('name') is-invalid @enderror" value="{{ old('name') }}" placeholder="Name*" id="name" name="name" required>
											@if($errors->has('name')) 
												<em class="invalid-feedback"> {{ $errors->first('name') }} </em> 
											@endif
										</div>
										<div class="form-group ">
											<label>Categories<span class="text-danger">*</span></label>
											<select class="form-control @error('categories') is-invalid @enderror" name="categories[]" id="categories" multiple  placeholder="Categories*" required>
												@foreach($brandTypes as $id => $brandType)
													<option value="{{ $brandType->id }}" {{ (in_array($id, old('categories', [])) || isset($plan) && $plan->PlanBrandTypes()->pluck('brand_type_id')->contains($brandType->id)) ? 'selected' : '' }}>{{ $brandType->name }} </option>
												@endforeach
											</select>
											@if ($errors->has('categories'))
												<span class="text-danger">{{ $errors->first('categories') }}</span>
											@endif
										</div>
										<div class="form-group">
											<label>Amount<span class="text-danger">*</span></label>
											<input type="text" class="form-control @error('amount') is-invalid @enderror" value="{{ old('amount') }}" placeholder="Amount*" id="amount" name="amount" required>
											@if($errors->has('amount')) 
												<em class="invalid-feedback"> {{ $errors->first('amount') }} </em> 
											@endif
										</div>
										<div class="form-group">
											<label>Yearly Charges<span class="text-danger">*</span></label>
											<input type="text" class="form-control @error('yearly_charges') is-invalid @enderror" value="{{ old('yearly_charges') }}" placeholder="Yearly Charges*" id="yearly_charges" name="yearly_charges" required>
											@if($errors->has('yearly_charges')) 
												<em class="invalid-feedback"> {{ $errors->first('yearly_charges') }} </em> 
											@endif
										</div>
										<div class="form-group">
											<label>Status<span class="text-danger">*</span></label>
											<select class="form-control @error('status') is-invalid @enderror" name="status">
												<option value="" >Select Status</option>
												<option value="1" {{ (old('status') == '1' )? 'selected': ''}}>Enable</option>
												<option value="0" {{ (old('status') == '0') ? 'selected': ''}}>Disable</option>
											</select>
											@if($errors->has('status')) 
												<em class="invalid-feedback"> {{ $errors->first('status') }} </em> 
											@endif
										</div>
										<div class="form-group ">
											<label>Plotters<span class="text-danger">*</span></label>
                                            @foreach($plotters as $id => $plotter)
												<table class="table">
														<tr><td style="width:18%; line-height:1;" ><strong>{{ $plotter->name }}</strong><input type='hidden' name='plotter_arr_ids[]' value="{{ $plotter->id }}"></td><td style="width:25%;">Max. Speed : {{ $plotter->speed }} <br> Max. Pressure : {{ $plotter->pressure }}</td><td style="width:25%;"><input type='number' class="form-control" name='plotter_speed_arr[]' min="0" max='{{ $plotter->speed }}' placeholder="Max. Speed" required></td><td style="width:25%;"><input type='number' class="form-control" name='plotter_pressure_arr[]' min="0" max='{{ $plotter->pressure }}' placeholder="Max. Pressure" required></td></tr>
													</table>
											@endforeach
										
											@if ($errors->has('plotter'))
												<span class="text-danger">{{ $errors->first('plotter') }}</span>
											@endif
										</div>
										
										<div class="mb-2">
											<button type="submit" class="btn btn-success">Save</button>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<script src="{{ asset('public/Admin/js/jquery-3.6.0.min.js')}}"></script>
		<script src="{{ asset('public/Admin/plugins/select2/js/select2.min.js')}}"></script>
		<script src="{{ asset('public/Admin/plugins/select2/js/custom-select.js')}}"></script>
		<script src="{{ asset('public/Admin/js/feather.min.js')}}"></script>
		<script src="{{ asset('public/Admin/js/jquery.slimscroll.min.js')}}"></script>
		<script src="{{ asset('public/Admin/js/bootstrap.bundle.min.js')}}"></script>
		<script src="{{ asset('public/Admin/js/script.js')}}"></script>
		
		<script>
			$("#plotter").change(function(){
				var speed = [];
				$("input[name='plotter_speed_arr[]']").each(function() {
					speed.push($(this).val());
				});
				console.log(speed);
				$("#add_plotter").html("");
				var i =0;
				$("#plotter :selected").each(function() {
					$("#add_plotter").append("<tr><td><input type='hidden' name='plotter_arr_ids[]' value='"+this.value+"'><input type='text' name='plotter_arr[]' value='"+this.text+"' style='cursor: default;background-color: -internal-light-dark(rgba(239, 239, 239, 0.3), rgba(59, 59, 59, 0.3));color: -internal-light-dark(rgb(84, 84, 84), rgb(170, 170, 170));border-color: rgba(118, 118, 118, 0.3);' readonly disabled></td><td><input type='text' name='plotter_speed_arr[]' required value='"+(speed[i++] ?? '' )+"'></td></tr>");
				});
				
				
			});
			$(document).ready(function() {
				$('#categories').select2();
				$('#plotter').select2();
			});
			
		</script>
	</body>
</html>