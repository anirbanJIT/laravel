<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
		<meta name="description" content="POS - Bootstrap Admin Template">
		<meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
		<meta name="author" content="Dreamguys - Bootstrap Admin Template">
		<meta name="robots" content="noindex, nofollow">
		<title>Edit Plan Recharge</title>
      
		<link rel="shortcut icon" type="image/x-icon" href="{{ asset('public/Admin/img/favicon.png')}}">
		<link rel="stylesheet" href="{{ asset('public/Admin/css/bootstrap.min.css')}}">
		<link rel="stylesheet" href="{{ asset('public/Admin/plugins/select2/css/select2.min.css')}}">
		<link rel="stylesheet" href="{{ asset('public/Admin/css/animate.css')}}">
		<link rel="stylesheet" href="{{ asset('public/Admin/css/dataTables.bootstrap4.min.css')}}">
		<link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/fontawesome.min.css')}}">
		<link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/all.min.css')}}">
		<link rel="stylesheet" href="{{ asset('public/Admin/css/style.css')}}">
	</head>
	<body>
		<div id="global-loader">
			<div class="whirly-loader"> </div>
		</div>
		<div class="main-wrapper">
			@extends('layouts.header')
			@extends('layouts.sidebar')
			<div class="page-wrapper">
				<div class="content container-fluid">
					<div class="page-header">
						<div class="row">
							<div class="col">
								<h3 class="page-title">Edit Plan Recharge</h3>
								<ul class="breadcrumb">
									<li class="breadcrumb-item"><a href="{{route('admin.home')}}">{{ trans('global.dashboard') }}</a></li>
									<li class="breadcrumb-item"><a href="{{ route('admin.planrecharge.index') }}">Plan Recharges</a></li>
									<li class="breadcrumb-item active">Edit Plan Recharge</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<div class="card">
								<div class="card-body">
									<form action="{{ route('admin.planrecharge.update', [$planrecharge->id]) }}" method="POST" enctype="multipart/form-data">
										{{ csrf_field() }} 
										@method('PUT')
										<div class="form-group">
											<label>Name<span class="text-danger">*</span></label>
											<input type="text" class="form-control @error('name') is-invalid @enderror" placeholder="Name*" id="name" name="name" value="{{$planrecharge->name}}">
											@if($errors->has('name')) 
												<em class="invalid-feedback"> {{ $errors->first('name') }} </em> 
											@endif
										</div>
										<div class="form-group ">
											<label>Categories<span class="text-danger">*</span></label>
											<select class="form-control @error('categories') is-invalid @enderror" name="categories[]" id="categories" multiple required>
												@foreach($brandTypes as $id => $brandType)
													<option value="{{ $brandType->id }}" {{ in_array($brandType->id,$category_ids) ? 'selected':'-' }}>{{ $brandType->name }} </option>
												@endforeach
											</select>
											@if ($errors->has('categories'))
												<span class="text-danger">{{ $errors->first('categories') }}</span>
											@endif
										</div>
										<div class="form-group">
											<label>Amount<span class="text-danger">*</span></label>
											<input type="text" class="form-control @error('amount') is-invalid @enderror" value="{{ $planrecharge->amount }}" placeholder="Amount*" id="amount" name="amount">
											@if($errors->has('amount')) 
												<em class="invalid-feedback"> {{ $errors->first('amount') }} </em> 
											@endif
										</div>
										<div class="form-group">
											<label>Yearly Charges<span class="text-danger">*</span></label>
											<input type="text" class="form-control @error('yearly_charges') is-invalid @enderror" value="{{ $planrecharge->yearly_charges }}" placeholder="Yearly Charges*" id="yearly_charges" name="yearly_charges">
											@if($errors->has('yearly_charges')) 
												<em class="invalid-feedback"> {{ $errors->first('yearly_charges') }} </em> 
											@endif
										</div>
										<div class="form-group">
											<label>Status<span class="text-danger">*</span></label>
											<select class="form-control @error('status') is-invalid @enderror" name="status">
												<option value="" >Select Status</option>
												<option value="1" {{ ($planrecharge->status == '1' )? 'selected': ''}}>Enable</option>
												<option value="0" {{ ($planrecharge->status == '0') ? 'selected': ''}}>Disable</option>
											</select>
											@if($errors->has('status')) 
												<em class="invalid-feedback"> {{ $errors->first('status') }} </em> 
											@endif
										</div>
										<div class="form-group ">
											<label>Plotters<span class="text-danger">*</span></label>
											
											@if ($errors->has('plotter'))
												<span class="text-danger">{{ $errors->first('plotter') }}</span>
											@endif
										</div>
										<table class="table">
											<thead>
												<tr>
													<th>Plotter</th>
                                                    <th>Maximum</th>
													<th>Speed</th>
                                                    <th>Pressure</th>
												</tr>
											</thead>
											<tbody id="add_plotter">
												@php
													$i=0;
													$speed = json_decode($planrecharge->speed);
                                                    $j=0;
													$pressure = json_decode($planrecharge->pressure);
												@endphp
												@foreach($plotter_name_id as $r)
													<tr>
														<td>
															<input type='hidden' name='plotter_arr_ids[]' value='{{ $r["id"] }}'>
															<strong>{{ $r["name"] }}</strong>
														</td>
                                                        <td>
															Max. Speed : {{ $r["speed"] }}<br>Max. Pressure : {{ $r["pressure"] }}
														</td>
														<td>
															<input type='number' class="form-control" name='plotter_speed_arr[]' min="0" max='{{ $r["speed"] }}' value='{{ $speed[$i++] }}' required>
														</td>
                                                        <td>
															<input type='number' class="form-control" name='plotter_pressure_arr[]' min="0" max='{{ $r["pressure"] }}' value='{{ $pressure[$j++] }}' required>
														</td>
													</tr>
												@endforeach
											</tbody>
										</table>
										<div style="margin-top: 18px;">
											<div style="float:left; margin-right:30px;"><a href="{{ url()->previous() }}" class="btn btn-danger" style="float: right;">{{ trans('global.back_to_list') }} </a></div> <button type="submit" class="btn btn-success">Save</button>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<script src="{{ asset('public/Admin/js/jquery-3.6.0.min.js')}}"></script>
		<script src="{{ asset('public/Admin/plugins/select2/js/select2.min.js')}}"></script>
		<script src="{{ asset('public/Admin/plugins/select2/js/custom-select.js')}}"></script>
		<script src="{{ asset('public/Admin/js/feather.min.js')}}"></script>
		<script src="{{ asset('public/Admin/js/jquery.slimscroll.min.js')}}"></script>
		<script src="{{ asset('public/Admin/js/jquery.dataTables.min.js')}}"></script>
		<script src="{{ asset('public/Admin/js/dataTables.bootstrap4.min.js')}}"></script>
		<script src="{{ asset('public/Admin/js/bootstrap.bundle.min.js')}}"></script>
		<script src="{{ asset('public/Admin/plugins/apexchart/apexcharts.min.js')}}"></script>
		
		<script src="{{ asset('public/Admin/js/script.js')}}"></script>
		<script>
		
			$("#plotter").change(function(){
				var speed = [];
				$("input[name='plotter_speed_arr[]']").each(function() {
					speed.push($(this).val());
				});
				console.log(speed);
                var pressure = [];
				$("input[name='plotter_pressure_arr[]']").each(function() {
					pressure.push($(this).val());
				});
				console.log(pressure);
				$("#add_plotter").html("");
				var i =0;
                var j =0;
				$("#plotter :selected").each(function() {
					$("#add_plotter").append("<tr><td><input type='hidden' name='plotter_arr_ids[]' value='"+this.value+"'><input type='text' name='plotter_arr[]' value='"+this.text+"' readonly disabled style='cursor: default;background-color: -internal-light-dark(rgba(239, 239, 239, 0.3), rgba(59, 59, 59, 0.3));color: -internal-light-dark(rgb(84, 84, 84), rgb(170, 170, 170));border-color: rgba(118, 118, 118, 0.3);'></td><td><input type='text' name='plotter_speed_arr[]' required value='"+(speed[i++] ?? '' )+"'></td><td><input type='text' name='plotter_pressure_arr[]' required value='"+(pressure[j++] ?? '' )+"'></td></tr>");
				});
				
				
			});
			$(document).ready(function() {
				$('#categories').select2();
				$('#plotter').select2();
			});
		</script>
	</body>
</html>