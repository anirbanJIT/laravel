@extends('layouts.admin')



@section('content')

 <!-- Start Status area -->

    <div class="notika-status-area">

        <div class="container">

            <div class="row">

			 @php $retailer = getCountRetailer() @endphp

			

                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                	<a href="{{route('admin.retailers-list')}}">
                    <div class="wb-traffic-inner notika-shadow sm-res-mg-t-30 tb-res-mg-t-30">

                        <div class="website-traffic-ctn">

                            <h2><i class="fa fa-users nk-selective-yellow" aria-hidden="true" style="font-size:23px;"></i> <span class="counter">{{$retailer}}</span></h2>

                            <p>Number Of Retailers</p>

                        </div>

                        

                    </div>
                </a>

                </div>

                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">

                    <div class="wb-traffic-inner notika-shadow sm-res-mg-t-30 tb-res-mg-t30">

                        <div class="website-traffic-ctn">

                            <h2><i class="fa fa-credit-card nk-selective-yellow" aria-hidden="true" style="font-size:23px;"></i> <span class="counter">{{ $totalCreditPoints}}</span></h2>

                           <!--  <p>Total Credit Points Sales</p> -->
                            <p>Total Wallet Balance</p>

                        </div>

                       

                    </div>

                </div>

                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">

                    <div class="wb-traffic-inner notika-shadow sm-res-mg-t-30 tb-res-mg-t-30 dk-res-mg-t-30">

                        <div class="website-traffic-ctn">

                            <h2> <i class="fa fa-money nk-selective-yellow" aria-hidden="true" style="font-size:23px;"></i> Rs <span class="counter">{{ $totalRechargeAmount }}</span></h2>

                            <p>Total Recharge Amount</p>

                        </div>

                        

                    </div>

                </div>

                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">

                    <div class="wb-traffic-inner notika-shadow sm-res-mg-t-30 tb-res-mg-t-30 dk-res-mg-t-30">

                        <div class="website-traffic-ctn">

                            <h2><i class="fa fa-suitcase nk-selective-yellow" aria-hidden="true" style="font-size:23px;"></i> Rs <span class="counter">{{ $totalRechargeAmountMonth }}</span></h2>

                            <p>Current Month Recharge Amount</p>

                        </div>

                      

                    </div>

                </div>

            </div>

        </div>

    </div>

    <!-- End Status area-->

    <!-- Start Sale Statistic area-->

    <div class="sale-statistic-area">

        <div class="container">

            <div class="row">

                <div class="col-lg-12 col-md-12 col-sm-7 col-xs-12">

                    <div class="sale-statistic-inner notika-shadow mg-tb-30">

                        <div class="curved-inner-pro">

                            <div class="curved-ctn">

                                <h2>Recharge Statistics</h2>

                                

                            </div>

                        </div>

                        <div id="chartContainer1" style="height: 300px; width: 100%;"></div>

                    </div>

                </div>

                

            </div>

        </div>

    </div>

    <!-- End Sale Statistic area-->

    <!-- Start Email Statistic area-->

    <div class="notika-email-post-area">

        <div class="container">

            <div class="row">

                

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">

                    <div id="chartContainer" style="height: 300px; width: 100%;"></div>



                </div>

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">

                    <div class="recent-items-wp notika-shadow sm-res-mg-t-30">

                        <div class="rc-it-ltd">

                            <div class="recent-items-ctn">

                                <div class="recent-items-title">

                                    <h2>Recent Recharges</h2>

                                </div>

                            </div>

                            <div class="recent-items-inn">

                                <table class="table table-inner table-vmiddle">

                                    <thead>

                                        <tr>                                        

                                            <th>Name</th>

											 <th>Email</th>

                                            <th style="width: 60px">Recharge Amount</th>

                                        </tr>

                                    </thead>

                                    <tbody>

									   @foreach($recentRecharges as $recentRecharge)

                                        <tr>                       

                                            <td>{{ $recentRecharge->retailerName }}</td>

											<td>{{ $recentRecharge->retailerEmail }}</td>

                                            <td class="f-500 c-cyan">RS {{ $recentRecharge->recharge_amount }}</td>

                                        </tr>

                                       @endforeach

                                    </tbody>

                                </table>

                            </div>

							

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

    <!-- End Email Statistic area-->

	<script type="text/javascript" src="{{asset('js/canvasjs.min.js')}}"></script>

	<script type="text/javascript">

  window.onload = function () {

	    var url = "{{url('admin/chart/getchatdata')}}"; 

		

		$.get(url, function(data, status){

		var attr = new Array();

		var attr1 = new Array();

		 debugger;

        var chardata=data.chart;

		var monthwiseRecharge=data.chart1;

		$.each(chardata, function(key,value) {

		 // alert(value.y);

		  // alert(value.label);

		 // debugger;

		   if(value.label == 1)

		   {

			var label = 'Active'   

		   }

		   else{

			 var label = 'Inactive' 

		   }

			   

		   var y=parseInt(value.y);

		   

		   

		  attr.push({ "y" : y,"label": label});

		}); 

		$.each(monthwiseRecharge, function(key,value) {

		    

		     var x1=value.months;

		     var y1=parseInt(value.y);



		  attr1.push({ y: y1, label: x1 });

		}); 

    chardata=attr;		

	var chart = new CanvasJS.Chart("chartContainer", {

	theme: "light2", // "light1", "light2", "dark1", "dark2"

	exportEnabled: true,

	animationEnabled: true,

	title: {

		text: "Retailers"

	},

	data: [{

		type: "pie",

		startAngle: 25,

		toolTipContent: "<b>{label}</b>: {y}%",

		showInLegend: "true",

		legendText: "{label}",

		indexLabelFontSize: 16,

		indexLabel: "{label} - {y}",

		dataPoints: chardata,

		  click: function(e){
		  	var d = e.dataPoint.label == 'Active' ? 1 : 0;
		  	var l = @json(route('admin.retailers-list'));
		  	window.open(l+'?status='+d,"_self");
		   },
        
	}]

});


chart.render();

    monthwiseRecharge=attr1;

	var chart = new CanvasJS.Chart("chartContainer1", {

		title:{

			text: " "              

		},

		data: [              

		{

			// Change type to "doughnut", "line", "splineArea", etc.

			type: "column",

			dataPoints: monthwiseRecharge

		}]

		

	});

	chart.render();

		});	



  }

  </script>

  

@endsection

