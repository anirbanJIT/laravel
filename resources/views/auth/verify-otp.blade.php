@extends('layouts.app')
@section('content')

 <div class="login-content">
        <!-- Login -->
		
		
        <div class="nk-block toggled " id="l-login">
		
		@if(\Session::has('message'))
                        <p class="alert alert-warning">
                            {{ \Session::get('message') }}
                        </p>
                    @endif
                    <form method="POST" action="{{ route('otp-verify-submit') }}">
                        {{ csrf_field() }}
						<input type="hidden" name="ui" value="{{request()->query('ui')}}">
                        <h1><a href="#" style="color:white;font-size: 24px;"><!--<img src="{{ asset('img/logo/logo.png') }}" alt="" />-->{{env('APP_NAME','')}}</a></h1>

            <div class="nk-form mg-t-30">
			
			<div class="cmp-tb-hd">
                            <h2>Verify your mobile number</h2>
                            <!--<p>Basic example for input groups with floating labels </p>-->
                        </div>
						 <div class="clear">&nbsp;</div>
			 <div class="input-group">
                    <span class="input-group-addon nk-ic-st-pro"><i class="notika-icon notika-phone"></i></span>
                    <div class="nk-int-st">
                        <input name="otp_number" type="text" class="form-control{{ $errors->has('otp_number') ? ' is-invalid' : '' }}"   placeholder="Enter OTP number" value="">
                            @if($errors->has('otp_number'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('otp_number') }}
                                </div>
                            @endif
                    </div>
                </div>
			
               <div class="clear">&nbsp;</div>
				<div class="col-6">
                                <button type="submit" class="btn btn-success px-4">
                                    Submit
                                </button>
                            </div>
               <!-- <a href="#l-register" data-ma-action="nk-login-switch" data-ma-block="#l-register" class="btn btn-login btn-success btn-float"><i class="notika-icon notika-right-arrow right-arrow-ant"></i></a>-->
            </div>

           </form>
        </div>
</div>


@endsection