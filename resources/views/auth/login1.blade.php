<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
      <meta name="description" content="POS - Bootstrap Admin Template">
      <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
      <meta name="author" content="Dreamguys - Bootstrap Admin Template">
      <meta name="robots" content="noindex, nofollow">
      <title>Dreams Pos admin template</title>
      <link rel="shortcut icon" type="image/x-icon" href="{{ asset('public/Admin/img/favicon.png')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/bootstrap.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/animate.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/dataTables.bootstrap4.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/fontawesome.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/all.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/style.css')}}">
   </head>
   <body>
      <div id="global-loader">
         <div class="whirly-loader"> </div>
      </div>
  <div class="main-wrapper">
     
     @extends('layouts.header')
     
     


 <div class="login-content">
        <!-- Login -->
        <div class="nk-block toggled" id="l-login">
		
		@if(\Session::has('message'))
                        <p class="alert alert-info">
                            {{ \Session::get('message') }}
                        </p>
                    @endif
                    <form method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}
                        <h1><a href="#" style="color:white;font-size: 24px;"><!--<img src="{{ asset('img/logo/logo.png') }}" alt="" />-->{{env('APP_NAME','')}}</a></h1>

            <div class="nk-form">
                <div class="input-group">
                    <span class="input-group-addon nk-ic-st-pro"><i class="notika-icon notika-support"></i></span>
                    <div class="nk-int-st">
                        <input name="email" type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" required  placeholder="Email" value="{{ old('email', null) }}">
                            @if($errors->has('email'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('email') }}
                                </div>
                            @endif
                    </div>
                </div>
                <div class="input-group mg-t-15">
                    <span class="input-group-addon nk-ic-st-pro"><i class="fa fa-lock"></i></span>
                    <div class="nk-int-st">
                        <input name="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" required placeholder="Password">
                            @if($errors->has('password'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('password') }}
                                </div>
                            @endif
                    </div>
                </div>
                <div class="fm-checkbox">
                    <label>
<input class="i-checks" name="remember" type="checkbox" id="remember" style="vertical-align: middle;" /><i></i>

					Keep me signed in</label>
                </div>
				<div class="col-6">
                                <button type="submit" class="btn btn-success px-4">
                                    Login
                                </button>
                            </div>
               <!-- <a href="#l-register" data-ma-action="nk-login-switch" data-ma-block="#l-register" class="btn btn-login btn-success btn-float"><i class="notika-icon notika-right-arrow right-arrow-ant"></i></a>-->
            </div>

            <div class="nk-navigation nk-lg-ic">
                <a href="{{ route('register')}}" data-ma-block="#l-register"><i class="notika-icon notika-plus-symbol"></i> <span>Register</span></a>
                <a href="{{ route('password.request') }}"  data-ma-block="#l-forget-password"><i>?</i> <span>Forgot Password</span></a>
            </div></form>
        </div>
</div>


</div>
 
 <script src="{{ asset('public/Admin/js/jquery-3.6.0.min.js')}}"></script>
      <script src="{{ asset('public/Admin/js/feather.min.js')}}"></script>
      <script src="{{ asset('public/Admin/js/jquery.slimscroll.min.js')}}"></script>
      <script src="{{ asset('public/Admin/js/jquery.dataTables.min.js')}}"></script>
      <script src="{{ asset('public/Admin/js/dataTables.bootstrap4.min.js')}}"></script>
      <script src="{{ asset('public/Admin/js/bootstrap.bundle.min.js')}}"></script>
      <script src="{{ asset('public/Admin/plugins/apexchart/apexcharts.min.js')}}"></script>
      <script src="{{ asset('public/Admin/plugins/apexchart/chart-data.js')}}"></script>
      <script src="{{ asset('public/Admin/js/script.js')}}"></script>
   </body>
</html>