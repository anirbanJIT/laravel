@extends('layouts.app')
@section('content')

 <div class="login-content">
        <!-- Login -->
		
		
        <div class="nk-block toggled " id="l-login">
		
		@if(\Session::has('message'))
                        <p class="alert alert-warning">
                            {{ \Session::get('message') }}
                        </p>
                    @endif
                    <form method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}
                        <h1><a href="#" style="color:white;font-size: 24px;"><!--<img src="{{ asset('img/logo/logo.png') }}" alt="" />-->{{env('APP_NAME','')}}</a></h1>

            <div class="nk-form mg-t-30">
			
			<div class="cmp-tb-hd">
                            <h2>Create a new account</h2>
                            <!--<p>Basic example for input groups with floating labels </p>-->
                        </div>
						
			 <div class="input-group">
                    <span class="input-group-addon nk-ic-st-pro"><i class="notika-icon notika-next"></i></span>
                    <div class="nk-int-st">
                        <input name="referred_by" type="text" class="form-control{{ $errors->has('referred_by') ? ' is-invalid' : '' }}"   placeholder="Referral Id" value="{{ old('referred_by', null) }}">
                            @if($errors->has('referred_by'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('referred_by') }}
                                </div>
                            @endif
                    </div>
                </div>
			 <div class="input-group">
                    <span class="input-group-addon nk-ic-st-pro"><i class="notika-icon notika-support"></i></span>
                    <div class="nk-int-st">
                        <input name="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"   placeholder="Name" value="{{ old('name', null) }}">
                            @if($errors->has('name'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('name') }}
                                </div>
                            @endif
                    </div>
                </div>
                <div class="input-group">
                    <span class="input-group-addon nk-ic-st-pro"><i class="notika-icon notika-mail"></i></span>
                    <div class="nk-int-st">
                        <input name="email" type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"   placeholder="Email" value="{{ old('email', null) }}">
                            @if($errors->has('email'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('email') }}
                                </div>
                            @endif
                    </div>
                </div>
				 <div class="input-group">
                    <span class="input-group-addon nk-ic-st-pro"><i class="notika-icon notika-phone"></i></span>
                    <div class="nk-int-st">
                        <input name="phone" type="text" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}"   placeholder="Mobile Number (With country code)" value="{{ old('phone', null) }}">
                            @if($errors->has('phone'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('phone') }}
                                </div>
                            @endif
                    </div>
                </div>
                <div class="input-group">
                    <span class="input-group-addon nk-ic-st-pro"><i class="notika-icon notika-edit"></i></span>
                    <div class="nk-int-st">
                        <input name="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"  placeholder="Password">
                            @if($errors->has('password'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('password') }}
                                </div>
                            @endif
                    </div>
                </div>
				 <div class="input-group">
                    <span class="input-group-addon nk-ic-st-pro"><i class="notika-icon notika-edit"></i></span>
                    <div class="nk-int-st">
                        <input name="password_confirmation" type="password" class="form-control{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}"  placeholder="Password Confirmation">
                            @if($errors->has('password_confirmation'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('password_confirmation') }}
                                </div>
                            @endif
                    </div>
                </div>
               <div class="clear">&nbsp;</div>
				<div class="col-6">
                                <button type="submit" class="btn btn-success px-4">
                                    Submit
                                </button>
                            </div>
               <!-- <a href="#l-register" data-ma-action="nk-login-switch" data-ma-block="#l-register" class="btn btn-login btn-success btn-float">
			   <i class="notika-icon notika-right-arrow right-arrow-ant"></i></a>-->
            </div>
 <div class="nk-navigation nk-lg-ic">
                <a href="{{ route('login')}}" data-ma-block="#l-login"><i class="notika-icon notika-plus-symbol"></i> <span>Login</span></a>
                <a href="{{ route('password.request') }}"  data-ma-block="#l-forget-password"><i>?</i> <span>Forgot Password</span></a>
            </div>
           </form>
        </div>
</div>


@endsection