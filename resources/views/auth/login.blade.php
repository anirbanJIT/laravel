<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
      <meta name="description" content="">
      <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, invoice, html5, responsive, Projects">
      <meta name="author" content="">
      <meta name="robots" content="noindex, nofollow">
      <title>Login - Modern Mart</title>
      <link rel="shortcut icon" class="rounded-circle" type="image/x-icon" href="{{ asset('public/Admin/img/favicon.png')}}">

      <link rel="stylesheet" href="{{ asset('public/Admin/css/bootstrap.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/fontawesome.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/plugins/fontawesome/css/all.min.css')}}">
      <link rel="stylesheet" href="{{ asset('public/Admin/css/style.css')}}">
   </head>
   <body class="account-page">
      <div class="main-wrapper">
         <div class="account-content">
            <div class="login-wrapper">
               <div class="login-content">
                  <div class="login-userset">
                     
                     <div class="login-logo">
                        <img src="{{ asset('public/Admin/img/ic_app.png')}}" alt="img" class="rounded-circle">
                       
                     </div>
 <h2 style="font-weight: 700;font-size: 26px;">Modern Mart</h2>
			
                     <div class="login-userheading">
                        <h3>Sign In</h3>
                        <h4>Please login to your account</h4>
                     </div>
                     @if(\Session::has('message'))
                        <p class="alert alert-warning">
                            {{ \Session::get('message') }}
                        </p>
                    @endif
                     <form method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}
                     <div class="form-login">
                        <label>Email</label>
                        <div class="form-addons">
                           <input type="text" placeholder="Enter your email address" name="email">
                           <img src="{{ asset('public/Admin/img/icons/mail.svg')}}" alt="img">
                        </div>
                     </div>
                     <div class="form-login">
                        <label>Password</label>
                        <div class="pass-group">
                           <input type="password" class="pass-input" placeholder="Enter your password" name="password">
                           <span class="fas toggle-password fa-eye-slash"></span>
                        </div>
                     </div>
                     <div class="form-login">
                        <div class="alreadyuser">
                           <!-- <h4><a href="" class="hover-a">Forgot Password?</a></h4> -->
                        </div>
                     </div>
                     <div class="form-login">
                        <button type="submit" class="btn btn-login"> Sign In </button>
                     </div>
                     </form>
                     <div class="signinform text-center">
                        <!-- <h4>Don’t have an account? <a href="{{ route('register')}}" class="hover-a">Sign Up</a></h4> -->
                     </div>
                     
                  </div>
               </div>
               <div class="login-img">
                  <img src="{{ asset('public/Admin/img/1024.png')}}" alt="img" style="width: 98%;height: auto;">
               </div>
            </div>
         </div>
      </div>
      <script src="{{ asset('public/Admin/js/jquery-3.6.0.min.js')}}"></script>
      <script src="{{ asset('public/Admin/js/feather.min.js')}}"></script>
      <script src="{{ asset('public/Admin/js/bootstrap.bundle.min.js')}}"></script>
      <script src="{{ asset('public/Admin/js/script.js')}}"></script>
   </body>
</html>