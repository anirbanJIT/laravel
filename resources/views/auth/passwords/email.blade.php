@extends('layouts.app')

@section('content')



 <div class="login-content">

        <!-- Login -->

        <div class="nk-block toggled" id="l-login">

		

		@if(\Session::has('message'))

                        <p class="alert alert-info">

                            {{ \Session::get('message') }}

                        </p>

                    @endif

                   <form method="POST" action="{{ route('password.email') }}">

                        {{ csrf_field() }}

                        <h1 style="color: #fff;">{{ env('APP_NAME', ' ') }}</h1>



            <div class="nk-form">

                <div class="input-group">

                    <span class="input-group-addon nk-ic-st-pro"><i class="notika-icon notika-support"></i></span>

                    <div class="nk-int-st">

                       <input type="email" name="email" class="form-control" required="autofocus" placeholder="Email">

                                @if($errors->has('email'))

                                    <em class="invalid-feedback">

                                        {{ $errors->first('email') }}

                                    </em>

                                @endif

                    </div>

                </div>

              

                <div class="fm-checkbox">

                    <label>

<input class="i-checks" name="remember" type="checkbox" id="remember" style="vertical-align: middle;" /><i></i>



					Keep me signed in</label>

                </div>

				<div class="col-6">

                                <button type="submit" class="btn btn-success px-4">

                                    Reset Password

                                </button>

                            </div>

               <!-- <a href="#l-register" data-ma-action="nk-login-switch" data-ma-block="#l-register" class="btn btn-login btn-success btn-float"><i class="notika-icon notika-right-arrow right-arrow-ant"></i></a>-->

            </div>



           </form>

        </div>

</div>



