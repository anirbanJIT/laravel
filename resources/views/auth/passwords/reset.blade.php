@extends('layouts.app')

@section('content')



 <div class="login-content">

        <!-- Login -->

        <div class="nk-block toggled" id="l-login">

		

		@if(\Session::has('message'))

                        <p class="alert alert-info">

                            {{ \Session::get('message') }}

                        </p>

                    @endif

                   <form method="POST" action="{{ route('password.request') }}">

                        {{ csrf_field() }}

                        <h1 style="color: #fff;">{{ env('APP_NAME', ' ') }}</h1>



            <div class="nk-form">

                <div class="input-group">

                    <span class="input-group-addon nk-ic-st-pro"><i class="notika-icon notika-support"></i></span>

                    <div class="nk-int-st">

                       <input type="email" name="email" class="form-control" required placeholder="Email">

                                @if($errors->has('email'))

                                    <em class="invalid-feedback">

                                        {{ $errors->first('email') }}

                                    </em>

                                @endif

                    </div>

                </div>

                <div class="input-group mg-t-15">

                    <span class="input-group-addon nk-ic-st-pro"><i class="fa fa-lock"></i></span>

                    <div class="nk-int-st">

                         <input type="password" name="password" class="form-control" required placeholder="Password">

                                @if($errors->has('password'))

                                    <em class="invalid-feedback">

                                        {{ $errors->first('password') }}

                                    </em>

                                @endif

                    </div>

                </div>

				 <div class="input-group mg-t-15">

                    <span class="input-group-addon nk-ic-st-pro"><i class="fa fa-lock"></i></span>

                    <div class="nk-int-st">

                          <input type="password" name="password_confirmation" class="form-control" required placeholder="Password confirmation">

                                @if($errors->has('password_confirmation'))

                                    <em class="invalid-feedback">

                                        {{ $errors->first('password_confirmation') }}

                                    </em>

                                @endif

                    </div>

                </div>

                <div class="fm-checkbox">

                    <label>

<input class="i-checks" name="remember" type="checkbox" id="remember" style="vertical-align: middle;" /><i></i>



					Keep me signed in</label>

                </div>

				<div class="col-6">

                                <button type="submit" class="btn btn-success px-4">

                                     Reset Password

                                </button>

                            </div>

              

            </div>



          </form>

        </div>

</div>





@endsection