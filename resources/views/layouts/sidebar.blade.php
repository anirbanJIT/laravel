@php $user = Auth::user() @endphp
<div class="sidebar" id="sidebar">
    <div class="sidebar-inner slimscroll">
        <div id="sidebar-menu" class="sidebar-menu">
            <ul>
                <li>
                    <a href="{{ route('admin.home') }}"><img src="{{ asset('public/Admin/img/icons/dashboard.svg') }}"
                            alt="img"><span> {{ trans('global.dashboard') }}</span> </a>
                </li>
                @if (Gate::check('users_manage'))
                    <li><a href="{{ route('admin.users.index') }}"><img
                                src="{{ asset('public/Admin/img/icons/users1.svg') }}" alt="img"> <span>User
                                Management</span></a></li>
                @endif

                @if (
                    $user->can('brandstype_manage') ||
                        $user->can('brands_manage') ||
                        $user->can('brand_models') ||
                        $user->can('brand_sub_models') ||
                        $user->can('brand_models_screen_image') ||
                        $user->can('bulkupload_manage'))
                    <li class="submenu">
                        <a href="javascript:void(0);"><i data-feather="layers"></i> <span> Brands</span> <span
                                class="menu-arrow"></span></a>
                        <ul>
                            @can('brandstype_manage')
                                <li><a href="{{ route('admin.brand-type.index') }}">Categories</a></li>
                            @endcan

                            @can('brand_models_screen_image')
                                <li><a href="{{ route('admin.models-screen-image.create') }}">Upload Model screen</a></li>
                            @endcan
                            @can('bulkupload_manage')
                                <li><a href="{{ route('admin.bulkupload.create') }}">Bulk Upload</a></li>
                            @endcan
                        </ul>
                    </li>
                @endif

                @if ($user->can('brands_manage') || $user->can('brand_models') || $user->can('brand_sub_models'))
                    <!--            <li class="submenu">
    <a href="javascript:void(0);"><i data-feather="layers"></i> <span> Cagtegory</span> <span class="menu-arrow"></span></a>
                <ul>
     @can('brands_manage')
    <li><a href="{{ route('admin.brands.index') }}">Brands</a></li>
@endcan
     @can('brand_models')
    <li><a href="{{ route('admin.models.index') }}">Models</a></li>
@endcan
     @can('brand_sub_models')
    <li><a href="{{ route('admin.submodels.index') }}">Sub Models</a></li>
@endcan
    </ul>
   </li> -->
                @endif

                @if (
                    $user->can('users_manage') ||
                        $user->can('retailer_manage') ||
                        $user->can('manage_members_tree') ||
                        $user->can('manage_monthly_recharge') ||
                        $user->can('manage_customer_recharge_commission') ||
                        $user->can('manage_recharge_commission_payout') ||
                        $user->can('manage_credit_wallet_transaction') ||
                        $user->can('buyer-manage') ||
                        $user->can('manage_credit_wallet') ||
                        $user->can('recharge_plan_manage') ||
                        $user->can('bulk_cut_plan_manage') ||
                        $user->can('profile_change_request') ||
                        $user->can('imei_change_request'))
                    <li class="submenu">
                        <a href="javascript:void(0);"><img src="{{ asset('public/Admin/img/icons/users1.svg') }}"
                                alt="img"><span>Retailer Management</span> <span class="menu-arrow"></span></a>
                        <ul>
                            @can('retailer_manage')
                                <li><a href="{{ route('admin.retailers-list') }}">Retailers</a></li>
                            @endcan
                            @can('manage_members_tree')
                                <li><a href="{{ route('admin.members-tree') }}">Members Tree</a></li>
                            @endcan
                            @can('manage_monthly_recharge')
                                <!-- <li><a href="{{ route('admin.users-monthly-recharge') }}">Monthly Recharge</a></li> -->
                            @endcan
                            @can('manage_recharge_commission_payout')
                                <!-- <li><a href="{{ route('admin.customer-recharge-commission') }}">Customer Recharge Commission & Payout</a></li> -->
                            @endcan
                            @can('manage_credit_wallet_transaction')
                                <li><a href="{{ route('admin.credit-wallet-transaction') }}">Wallet Transaction</a></li>
                            @endcan
                            @can('buyer-manage')
                                <li><a href="{{ route('admin.buyer-list') }}">Buyer List</a></li>
                            @endcan
                            @can('manage_credit_wallet')
                                <li><a href="{{ route('admin.retailer-credit-balance') }}">Balances</a></li>
                            @endcan
                            @can('plan_manage')
                                <li><a href="{{ route('admin.plans.index') }}">Plans</a></li>
                            @endcan
                            @can('recharge_plan_manage')
                                <li><a href="{{ route('admin.recharge-plan.index') }}">Recharge Plan</a></li>
                            @endcan
                            @can('bulk_cut_plan_manage')
                                <li><a href="{{ route('admin.bulkcut-plan.index') }}">Bulkcut Plan</a></li>
                            @endcan
                            @can('profile_change_request')
                                <li><a href="{{ route('admin.retailer-profile-change-request') }}">Profile Change
                                        Requests</a></li>
                            @endcan
                            @can('imei_change_request')
                                <li><a href="{{ route('admin.imei-change-request') }}">IMEI Change Requests</a></li>
                            @endcan
                            @can('retailer_manage')
                                <!-- <li><a href="{{ route('admin.staffs-list') }}">Staffs</a></li> -->
                            @endcan
                        </ul>
                    </li>
                @endif
                @if ($user->can('video_category_manage') || $user->can('video_manage'))
                    <!-- <li class="submenu">
               <a href="javascript:void(0);"><i class="fa fa-video"></i> <span> Video</span> <span class="menu-arrow"></span></a>
               <ul>
                  @can('video_category_manage')
    <li><a href="{{ route('admin.video-category.index') }}">Video Category</a></li>
@endcan
                  @can('video_manage')
    <li><a href="{{ route('admin.video.index') }}">Video</a></li>
@endcan
               </ul>
            </li> -->
                @endif
                <li class="submenu">
                    <a href="javascript:void(0);"><img src="{{ asset('public/Admin/img/icons/settings.svg') }}"
                            alt="img"><span> Settings</span> <span class="menu-arrow"></span></a>
                    <ul>
                        @can('video_category_manage')
                            <li><a href="{{ route('admin.video-category.index') }}">Video</a></li>
                        @endcan
                        @can('banner_manage')
                            <li><a href="{{ route('admin.banner.index') }}">Banner</a></li>
                        @endcan
                        @can('currency_manage')
                            <!-- <li><a href="{{ route('admin.setting.currency') }}">Currency</a></li> -->
                        @endcan
                        @can('notification_manage')
                            <li><a href="{{ route('admin.generalnotification.index') }}">Notification</a></li>
                        @endcan
                        @can('complaint_feedback')
                            <li><a href="{{ route('admin.complaint-feedback.index') }}">Complaint</a></li>
                            <li><a href="{{ route('admin.feedback-list') }}">Feedback</a></li>
                        @endcan
                        @can('papersize_manage')
                            <li><a href="{{ route('admin.paper-sizes.index') }}">Paper Size Manage</a></li>
                            <li><a href="{{ route('admin.setting.privacypolicy', '') }}">Privacy Policy</a></li>
                            <li><a href="{{ route('admin.setting.setting_pdf', '') }}">Setting Pdf</a></li>
                            <li><a href="{{ route('admin.user-reactions.index') }}">Likes & Dislikes</a></li>
                            <li><a href="{{ route('admin.serial-numbers.index') }}">Serial Numbers</a></li>
                            <li><a href="{{ route('admin.machine-numbers.index') }}">Machine Numbers</a></li>
                            <li><a href="{{ route('admin.setting.website') }}">Website</a></li>
                            <li><a href="{{ route('admin.setting.otpprice') }}">OTP Price</a></li>
                        @endcan
                        <li><a href="{{ route('auth.change_password') }}">Change Password</a></li>
                    </ul>
                </li>
                <li><a href="{{ route('admin.plotter.index') }}"><img
                            src="{{ asset('public/Admin/img/icons/Plotter.png') }}" alt="img">
                        <span>Plotter</span></a></li>
                <li><a href="{{ route('admin.planrecharge.index') }}"><img
                            src="{{ asset('public/Admin/img/icons/Plan.png') }}" alt="img"> <span>Plans</span></a>
                </li>
                <li>
                    <a href="{{ route('admin.licenseKeyList') }}">
                        <img src="{{ asset('public/Admin/img/icons/Plan.png') }}" alt="img" />
                        <span>License Key</span>
                    </a>
                </li>
            </ul>


        </div>
    </div>
</div>
