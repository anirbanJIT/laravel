<div class="header">
            <div class="header-left active">
               <a href="" class="logo">
                  <!-- <h3><b>Modern Mart</b></h3> -->
               <img src="{{ asset('public/Admin/img/Logo_5.png') }}" class="rounded-circle" alt="Logo" style="height: 40px;width: auto;">
               </a>
               <a href="" class="logo-small">
               <img src="{{ asset('public/Admin/img/Logo_5.png') }}" class="rounded-circle" alt="Logo" style="height: 40px;width: auto;">
               </a>
           
            </div>
            <a id="mobile_btn" class="mobile_btn" href="#sidebar">
            <span class="bar-icon">
            <span></span>
            <span></span>
            <span></span>
            </span>
            </a>
    		<a id="toggle_btn" href="javascript:void(0);"><i class="fa fa-bars" style="margin-top:16px;"></i>
               </a>
            @php $getAuthReferral = getAuthReferral() @endphp

            <?php if(auth()->user()->type == 0){ $a = 'Admin'; } else { $a = 'Retailer'; } ?>
            <ul class="nav user-menu">
               <!-- <li class="nav-item dropdown">
                  <a href="javascript:void(0);" class="nav-link"> <h4>Your Referral Id: {{ $getAuthReferral->referral_id }}</h4>
                  </a>
               </li> -->
               <li class="nav-item dropdown has-arrow main-drop">
                  <a href="javascript:void(0);" class="dropdown-toggle nav-link userset" data-bs-toggle="dropdown">
                  <span class="user-img"><img src="{{ asset('public/Admin/img/profiles/user.png') }}" alt="">
                  <span class="status online"></span></span>
                  </a>
                  <div class="dropdown-menu menu-drop-user">
                     <div class="profilename">
                        <div class="profileset">
                           <span class="user-img"><img src="{{ asset('public/Admin/img/profiles/user.png') }}" alt="">
                           <span class="status online"></span></span>
                           <div class="profilesets">
                              <h6>{{ auth()->user()->name}}</h6>
                              <h5>{{ $a }}</h5>
                           </div>
                        </div>
                        <a class="dropdown-item logout pb-0" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('frm-logout').submit();">
                                <i class="icon-key"></i>Logout
                            </a>
                            <form id="frm-logout" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
                        
                     </div>
                  </div>
               </li>
            </ul>
            <div class="dropdown mobile-user-menu">
               <a href="javascript:void(0);" class="nav-link dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
               <div class="dropdown-menu dropdown-menu-right">
                  <a class="dropdown-item" href="{{ route('logout') }}">Logout</a>
               </div>
            </div>
         </div>