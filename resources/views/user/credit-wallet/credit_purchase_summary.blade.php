@extends('layouts.admin')
@section('content')
<div class="breadcomb-area">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="breadcomb-list">
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<div class="breadcomb-wp">
									<div class="breadcomb-icon">
										<i class="notika-icon notika-windows"></i>
									</div>
									<div class="breadcomb-ctn">
										<h2>Purchase Credit Point</h2>
										<p>Here <span class="bread-ntd">you can purchase credit point</span></p>
									</div>
								</div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-3">
								<div class="breadcomb-report">
								
									 <button class="btn btn-success notika-btn-success waves-effect" >
									 
                Your Credit Point :{{ $credit->amount}}
            </button>
									
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
<div class="form-element-area">
  <div class="container">
  <form action="{{ route('user.credit-purchase') }}" method="POST" enctype="multipart/form-data">
            @csrf
   <div class="row">
   
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="data-table-list">
                        <div class="basic-tb-hd">
                            <h2>Credit point conversion ( {{$credit_setting->credit." CREDIT POINT = ".env('CURRENCY_SYMBOL')." ".$credit_setting->amount}} )</h2>
                           <!-- <p>It's just that simple. Turn your simple table into a sophisticated data table and offer your users a nice experience and great features without any effort.</p>-->
                        </div>
                        <div class="table-responsive">
                            <table id="data-table-basic" class="table table-sc-ex">
                <tbody>
				
				<tr>
                       
                    </tr>
                    <tr>
                        <th>
                            Purchase Credit Points
                        </th>
                        <td>
                            {{ $request->credit }}
                        </td>
                    </tr>
					
					 <tr>
                        <th>
                            You Pay 
                        </th>
                        <td>
                            {{ env('CURRENCY_SYMBOL')." ".$request->credit*$credit_setting->amount }}
                        </td>
                    </tr>
					 
                    
                </tbody>
            </table>
           
        </div>


    </div>
</div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			
				 <input type="hidden" name="name" value="{{auth()->user()->name}}">
				  <input type="hidden" name="email" value="{{auth()->user()->email}}"> 
				  <input type="hidden" name="phone" value="{{auth()->user()->phone}}"> 
				  <input type="hidden" name="credit_amt" value="{{$request->credit}}">
				  <input type="hidden" name="singal_credit_pur_amt" value="{{$credit_setting->amount}}">
				   <input type="hidden" name="total_credit_pur_amt" value="{{$request->credit*$credit_setting->amount}}">
				 @csrf
                    <div class="form-element-list">
                        <div class="basic-tb-hd">
                            <!--<h2>{{ trans('global.create') }} {{ trans('cruds.user.title_singular') }}</h2>
                            <p>Text Inputs with different sizes by height and column.</p>-->
                        </div>
						
                        <div class="row">
						
                           <div class="clear">&nbsp;</div>
							
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                           
                            <input type="submit" class="btn btn-success notika-btn-success waves-effect" value="Pay Now" >
                        </div>
                        </div>
                     
                      
                    </div>
					
                </div>
            </div>
</form>			
			</div>
            </div>
			
			@endsection
	