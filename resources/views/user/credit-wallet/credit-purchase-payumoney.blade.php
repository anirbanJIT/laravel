
@php
// Test Detail
// Card Type = visa
//card name = Test
//expiry date = 5/20
//cvv = 123
//card number=4012001037141112, 5123456789012346
//$payment_detail = payuPaymentDetail();
    $MERCHANT_KEY = env('MERCHANT_KEY'); // add your id
    $SALT = env('SALT'); // add your id

    $PAYU_BASE_URL = env('PAYU_BASE_URL');
    //$PAYU_BASE_URL = "https://secure.payu.in";
    $action = '';
    $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
    $posted = array();
    $posted = array(
        'key' => $MERCHANT_KEY,
        'txnid' => $txnid,
		'udf1' => Session::get('credit_invoice'),
        'amount' => $request->total_credit_pur_amt,
        'firstname' => $request->name,
        'email' => $request->email,
        'productinfo' => 'Credit Purchase',
        'surl' => 'user/credit-order-response',
        'furl' => 'user/credit-order-cancel',
        'service_provider' => 'payu_paisa',
    );

    if(empty($posted['txnid'])) {
        $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
    } 
    else 
    {
        $txnid = $posted['txnid'];
    }


    
    $hash = '';
    $hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
    
    if(empty($posted['hash']) && sizeof($posted) > 0) {
        $hashVarsSeq = explode('|', $hashSequence);
        $hash_string = '';	
        foreach($hashVarsSeq as $hash_var) {
            $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
            $hash_string .= '|';
        }
        $hash_string .= $SALT;

        $hash = strtolower(hash('sha512', $hash_string));
        $action = $PAYU_BASE_URL . '/_payment';
    } 
    elseif(!empty($posted['hash'])) 
    {
        $hash = $posted['hash'];
        $action = $PAYU_BASE_URL . '/_payment';
    }

@endphp
<html>
  <head>
  <script>
  
  $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
    var hash = '{{$hash}}';
    function submitPayuForm() {
      if(hash == '') {
        return;
      }
      var payuForm = document.forms.payuForm;
      payuForm.submit();
    }
  </script>
  </head>
  <body onload="submitPayuForm()">
    Processing.....
        <form action="{{$action}}" method="post" name="payuForm"><br />
		 <input type="hidden" name="_token" value="{{csrf_token()}}"/><br />
            <input type="hidden" name="key" value="{{$MERCHANT_KEY}}" /><br />
            <input type="hidden" name="hash" value="{{$hash}}"/><br />
            <input type="hidden" name="txnid" value="{{$txnid}}" /><br />
			<input type="hidden" name="udf1" value="{{Session::get('credit_invoice')}}" /><br />
            <input type="hidden" name="amount" value="{{$request->total_credit_pur_amt}}" /><br />
            <input type="hidden" name="firstname" id="firstname" value="{{$request->name}}" /><br />
            <input type="hidden" name="email" id="email" value="{{$request->email}}" /><br />
            <input type="hidden" name="productinfo" value="Credit Purchase"><br />
            <input type="hidden" name="surl" value="{{url('user/credit-order-response')}}" /><br />
            <input type="hidden" name="furl" value="{{url('user/credit-order-cancel')}}" /><br />
            <input type="hidden" name="service_provider" value="payu_paisa"  /><br />
            <?php
            if(!$hash) { ?>
                <input type="submit" value="Submit" />
            <?php } ?>
        </form>
  </body>
</html>