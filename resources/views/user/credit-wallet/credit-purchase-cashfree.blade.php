@php 

  $secretKey = env('CASHFREE_SECRET_KEY');
  $postData = array( 
  "appId" => env('CASHFREE_APP_ID'), 
  "orderId" => Session::get('credit_invoice'), 
  "orderAmount" => $request->total_credit_pur_amt, 
  "orderCurrency" => env('CURRENCY_SYMBOL'), 
  "orderNote" => 'Purchase Credits Point', 
  "customerName" => $request->name, 
  "customerPhone" => $request->phone, 
  "customerEmail" => $request->email,
  "returnUrl" => url('user/credit-order-response'), 
  "notifyUrl" => url('user/credit-order-response'),
);
ksort($postData);
$signatureData = "";
foreach ($postData as $key => $value){
    $signatureData .= $key.$value;
}
$signature = hash_hmac('sha256', $signatureData, $secretKey,true);
$signature = base64_encode($signature);
$url = env('CASHFREE_URL');
//if ($mode == "PROD") {
 // $url = "https://www.cashfree.com/checkout/post/submit";
//} else {
  //$url = "https://test.cashfree.com/billpay/checkout/post/submit";
//}

@endphp
<!DOCTYPE html>
<html>
<head>
  <head>
  <script>
  
  $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
    function submitPayuForm() {
      var payuForm = document.forms.payuForm;
      payuForm.submit();
    }
  </script>
  </head>
</head>
<body onload="submitPayuForm()">



  <form action="<?php echo $url; ?>" name="payuForm" method="post">
      <p>Please wait.......</p>
      <input type="hidden" name="signature" value='{{$signature}}'/>
      <input type="hidden" name="orderNote" value='{{"Purchase Credits Point"}}'/>
      <input type="hidden" name="orderCurrency" value="{{env('CURRENCY_SYMBOL')}}"/>
      <input type="hidden" name="customerName" value='{{$request->name}}'/>
      <input type="hidden" name="customerEmail" value='{{$request->email}}'/>
      <input type="hidden" name="customerPhone" value='{{$request->phone}}'/>
      <input type="hidden" name="orderAmount" value='{{$request->total_credit_pur_amt}}'/>
      <input type ="hidden" name="notifyUrl" value='{{url('user/credit-order-response')}}'/>
      <input type ="hidden" name="returnUrl" value='{{url('user/credit-order-response')}}'/>
      <input type="hidden" name="appId" value="{{env('CASHFREE_APP_ID')}}"/>
      <input type="hidden" name="orderId" value="{{Session::get('credit_invoice')}}"/>
  </form>
</body>
</html>








