@extends('layouts.admin')
@section('content')

<div class="breadcomb-area">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="breadcomb-list">
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<div class="breadcomb-wp">
									<div class="breadcomb-icon">
										<i class="notika-icon notika-windows"></i>
									</div>
									<div class="breadcomb-ctn">
										<h2>Credit Wallet Transaction</h2>
										<p>Show <span class="bread-ntd">all list</span></p>
									</div>
								</div>
							</div>
								<div class="col-lg-6 col-md-6 col-sm-6 col-xs-3">
								<div class="breadcomb-report">
								
									 <button class="btn btn-success notika-btn-success waves-effect" >
									 
                Your Credit Point :{{ $credit->amount}}
            </button>
									
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
 <!-- Data Table area Start-->
    <div class="data-table-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="data-table-list">
                        <!--<div class="basic-tb-hd">
                            <h2>Basic Example</h2>
                            <p>It's just that simple. Turn your simple table into a sophisticated data table and offer your users a nice experience and great features without any effort.</p>
                        </div>-->
                        <div class="table-responsive">
                            <table id="data-table-basic" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th width="10">

                        </th>
                        <th>
                            Invoice No.
                        </th>
                        <th>
                            Receiver
                        </th>
                        <th>
                            Sender
                        </th>
						 <th>
                            Credit
                        </th>
                        <th>
                            Debit
                        </th>
                        <th>
                            Balance
                        </th>
						<th>
                            Transaction Date
                        </th>
                                    </tr>
                                </thead>
                                <tbody>
								
                                    @foreach($creditTransaction as $key => $credit)
                        <tr data-entry-id="{{ $credit->id }}">
                            <td>

                            </td>
                            <td>
                                {{ $credit->invoice_no ?? '' }}
                            </td>
                            <td>
                                {{ $credit->receiver->receiver_name ?? '' }}
                            </td>
                            <td>
                                {{ $credit->sender->sender_name ?? '' }}
                            </td>
							<td>
                                {{ $credit->credit_amt ?? '' }}
                            </td>
							<td>
                                {{ $credit->debit_amt ?? '' }}
                            </td>
							<td>
                                {{ $credit->balance_amt ?? '' }}
                            </td>
							<td>
                                {{ date("d-m-Y",strtotime($credit->created_at)) ?? '' }}
                            </td>
							
                                    </tr>
                                   @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                         <th width="10">

                        </th>
                        <th>
                            Invoice No.
                        </th>
                        <th>
                            Receiver
                        </th>
                        <th>
                            Sender
                        </th>
						 <th>
                            Credit
                        </th>
                        <th>
                            Debit
                        </th>
                        <th>
                            Balance
                        </th>
						<th>
                            Transaction Date
                        </th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	
	<script>
    $(function () {
	
	  $('.sa-warning').on('click', function (e) {
   event.preventDefault();
   const url =  $(this).attr('href');
  
   swal({
       title: "Are you sure?",  
text: "Once Deleted. You will also able to recover this again!",  
type: "warning",  
showCancelButton: true,  
confirmButtonText: "Yes, delete it!",
   }).then(function(value) {
       if (value) {
           window.location.href = url;
		  
       }
   });
});
	
	
	$('.status_change').change(function() {
		
        var status = $(this).prop('checked') == true ? 1 : 0; 
        var model_id = $(this).data('id'); 
		 $.ajax({
           type:"GET",
           url:"{{url('admin/changeStatus')}}?status="+status+"&model_id="+model_id,
           success:function(res){               
            if(res){
				//alert(res);
                console.log(res.success);
           
            }else{
               //alert("nnnn");
            }
           }
        });
	
    });	
	});
	</script>
@endsection

