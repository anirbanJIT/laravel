@extends('layouts.admin')
@section('content')

<div class="breadcomb-area">
		<div class="container">
			<div class="row">
					<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="breadcomb-list">
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<div class="breadcomb-wp">
									<div class="breadcomb-icon">
										<i class="notika-icon notika-windows"></i>
									</div>
									<div class="breadcomb-ctn">
										<h2>My Reffered Member</h2>
										<p>Show <span class="bread-ntd">all list</span></p>
									</div>
								</div>
							</div>
								<div class="col-lg-6 col-md-6 col-sm-6 col-xs-3">
								<div class="breadcomb-report">
								
									 
									
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			</div>
		</div>
	</div>
	
 <!-- Data Table area Start-->
    <div class="data-table-area">
        <div class="container">
            <div class="row">
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
			<div class="recent-post-wrapper notika-shadow sm-res-mg-t-30 tb-res-ds-n dk-res-ds">
                        <div class="recent-post-ctn">
                            <div class="recent-post-title">
                                <h2>My Direct Members</h2>
                            </div>
                        </div>
                        <div class="recent-post-items">
						  @foreach($child->getChildMember as $childMember)
                            <div class="recent-post-signle rct-pt-mg-wp">
                                <a href="#">
                                    <div class="recent-post-flex" style="margin-top: 5px;">
                                        <div class="recent-post-img">
                                            <img src="{{asset('img/post/2.jpg')}}" alt="">
                                        </div>
                                        <div class="recent-post-it-ctn">
                                            <h2>{{$childMember->name}}</h2>
                                          <p>{{ date("d-m-Y H:i:s",strtotime($childMember->created_at)) ?? '' }}</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            @endforeach
                          
                            <div class="recent-post-signle">
                          
                           
                            <!--<div class="recent-post-signle">
                                <a href="#">
                                    <div class="recent-post-flex rc-ps-vw">
                                        <div class="recent-post-line rct-pt-mg">
                                            <p>View All</p>
                                        </div>
                                    </div>
                                </a>
                            </div>-->
                        </div>
                    </div>
			</div>			</div>
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                    <div class="data-table-list">
                        <!--<div class="basic-tb-hd">
                            <h2>Basic Example</h2>
                            <p>It's just that simple. Turn your simple table into a sophisticated data table and offer your users a nice experience and great features without any effort.</p>
                        </div>-->
                        <div class="table-responsive">
                            <table id="data-table-basic" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th width="10">

                        </th>
                        <th>
                            Name
                        </th>
                        <th>
                            Email
                        </th>
                        <th>
                            Contact No.
                        </th>
						 <th>
                            Status
                        </th>
                        <th>
                            Joining Data
                        </th>
                        
                                    </tr>
                                </thead>
                                <tbody>
								
                                   @foreach($child->getChildMember as $childMember)
                        <tr data-entry-id="{{ $childMember->id }}">
                            <td>

                            </td>
                            <td>
                                {{ $childMember->name ?? '' }}
                            </td>
                            <td>
                                {{ $childMember->email ?? '' }}
                            </td>
                            <td>
                                {{ $childMember->phone ?? '' }}
                            </td>
							<td>
                                <div class="nk-toggle-switch" data-ts-color="green">
                                               
                                                <input class="status_change" data-id="{{$childMember->id}}" id="ts{{$childMember->id}}" type="checkbox" hidden="hidden" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" data-off="InActive" {{ $childMember->status ? 'checked' : '' }}>
                                                <label for="ts{{$childMember->id}}" class="ts-helper"></label>
                                            </div>
                            </td>
							
							<td>
                                {{ date("d-m-Y H:i:s",strtotime($childMember->created_at)) ?? '' }}
                            </td>
							
                                    </tr>
                                   @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                          <th width="10">

                        </th>
                        <th>
                            Name
                        </th>
                        <th>
                            Email
                        </th>
                        <th>
                            Contact No.
                        </th>
						 <th>
                            Status
                        </th>
                        <th>
                            Joining Data
                        </th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	
	<script>
    $(function () {
	
	  $('.sa-warning').on('click', function (e) {
   event.preventDefault();
   const url =  $(this).attr('href');
  
   swal({
       title: "Are you sure?",  
text: "Once Deleted. You will also able to recover this again!",  
type: "warning",  
showCancelButton: true,  
confirmButtonText: "Yes, delete it!",
   }).then(function(value) {
       if (value) {
           window.location.href = url;
		  
       }
   });
});
	
	
	$('.status_change').change(function() {
		
        var status = $(this).prop('checked') == true ? 1 : 0; 
        var model_id = $(this).data('id'); 
		 $.ajax({
           type:"GET",
           url:"{{url('admin/changeStatus')}}?status="+status+"&model_id="+model_id,
           success:function(res){               
            if(res){
				//alert(res);
                console.log(res.success);
           
            }else{
               //alert("nnnn");
            }
           }
        });
	
    });	
	});
	</script>
@endsection

