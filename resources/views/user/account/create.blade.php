@extends('layouts.admin')
@section('content')
<div class="breadcomb-area">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="breadcomb-list">
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<div class="breadcomb-wp">
									<div class="breadcomb-icon">
										<i class="notika-icon notika-windows"></i>
									</div>
									<div class="breadcomb-ctn">
										<h2>{{ trans('global.create') }} {{ trans('cruds.user.title_singular') }}</h2>
										<p>Here <span class="bread-ntd">you can create user</span></p>
									</div>
								</div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-3">
								<div class="breadcomb-report">
								
									 <a class="btn btn-success notika-btn-success waves-effect" href="{{ url()->previous() }}">
                {{ trans('global.back_to_list') }}
            </a>
									
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
    {{-- changeDateFormate(date('Y-m-d'),'m/d/Y')  --}}
	@php $kycCats = getKycCat() @endphp
	{{-- dd($kycCats) --}}
<div class="tabs-info-area">
 <div class="container">
   <div class="row">
     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	  <div class="widget-tabs-int">
			<div class="tab-hd">
				<h2>Add User Details</h2>
				<p>Add quick, dynamic tab functionality to transition through panes of local content, even via dropdown menus.</p>
			</div>
	   <div class="widget-tabs-list">
			<ul class="nav nav-tabs">
			    <li class="active"><a data-toggle="tab" href="#general">General</a></li>
				<li><a data-toggle="tab" href="#home">Contact Details</a></li>
				<li><a data-toggle="tab" href="#menu1">Bank Details</a></li>
				<li><a data-toggle="tab" href="#menu2">KYC Details</a></li>
			</ul>
		<div class="tab-content tab-custom-st">
		<div id="general" class="tab-pane fade in active">
		<div class="tab-ctn">
		   <form action="{{ route('admin.users.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="form-element-list">
                        <div class="basic-tb-hd">
                            <!--<h2>{{ trans('global.create') }} {{ trans('cruds.user.title_singular') }}</h2>
                            <p>Text Inputs with different sizes by height and column.</p>-->
                        </div>
                       
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group ic-cmp-int">
                                    <div class="form-ic-cmp">
                                        <i class="notika-icon notika-support"></i>
                                    </div>
                                    <div class="nk-int-st">
                                        <input type="text" id="name" name="name" value="{{ old('name', isset($user) ? $user->name : '') }}" required class="form-control" placeholder="{{ trans('cruds.user.fields.name') }}*" >
										
                                    </div>
								@if($errors->has('name'))
									<em class="invalid-feedback">
										{{ $errors->first('name') }}
									</em>
								@endif
								<p class="helper-block">
									{{ trans('cruds.user.fields.name_helper') }}
								</p>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group ic-cmp-int">
                                    <div class="form-ic-cmp">
                                        <i class="notika-icon notika-mail"></i>
                                    </div>
                                    <div class="nk-int-st">
                                        <input type="text" id="email" name="email" class="form-control" value="{{ old('email', isset($user) ? $user->email : '') }}" required placeholder="{{ trans('cruds.user.fields.email') }}*">
                                   
									</div>
								 @if($errors->has('email'))
									<em class="invalid-feedback">
										{{ $errors->first('email') }}
									</em>
								@endif
								<p class="helper-block">
									{{ trans('cruds.user.fields.email_helper') }}
								</p>
                                </div>
                            </div>
							
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group ic-cmp-int">
                                    <div class="form-ic-cmp">
                                        <i class="notika-icon notika-edit"></i>
                                    </div>
                                    <div class="nk-int-st">
                                        <input type="password" id="password" name="password" class="form-control" required placeholder="{{ trans('cruds.user.fields.password') }}">
									
                                    </div>
								@if($errors->has('password'))
									<em class="invalid-feedback">
										{{ $errors->first('password') }}
									</em>
								@endif
								<p class="helper-block">
									{{ trans('cruds.user.fields.password_helper') }}
								</p>
                                </div>
                            </div>
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="nk-int-mk sl-dp-mn mg-t-10">
                                    <h2>{{ trans('cruds.user.fields.roles') }}*</h2>
                                </div>
                                <div class="bootstrap-select">
                                    <select class="selectpicker" name="roles[]" id="roles" multiple required>
									
											@foreach($roles as $id => $roles)
										<option value="{{ $id }}" {{ (in_array($id, old('roles', [])) || isset($user) && $user->roles->contains($id)) ? 'selected' : '' }}>{{ $roles }}</option>
									@endforeach
								   </select>
							    </div>
								@if($errors->has('roles'))
									<em class="invalid-feedback">
										{{ $errors->first('roles') }}
									</em>
								@endif
								<p class="helper-block">
									{{ trans('cruds.user.fields.roles_helper') }}
								</p>
								
                            </div>
							
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                           
                            <input type="submit" class="btn btn-success notika-btn-success waves-effect" value="{{ trans('global.save') }}" >
                        </div>
                        </div>
                     
                      
                    </div>
                </div>
            </div>
        </form>			
        </div>
		</div>
			
		
		<div id="home" class="tab-pane fade">
		<div class="tab-ctn">
         <form action="{{ route('admin.users.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
                         <div class="row">
                           <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <div class="nk-int-st">
                                        <input type="text" class="form-control" name="address" id="address" placeholder="Address">
                                    </div>
                                </div>
                            </div>
                          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                     <div class="form-group">
                                    <select class="selectpicker" name="brandType">
											<option value="">Select Country</option>
											
											<option value="">fdgfdg</option>
											<option value="">fdgfdg</option>
											<option value="">fdgfdg</option>
											<option value="">fdgfdg</option>
											<option value="">fdgfdg</option>
											
										</select>
								  
										<span class="text-danger"></span>
									</div>
                              
                            </div>
							</div>
						<div class="row">
                         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <select class="selectpicker" name="brandType">
											<option value="">Select State</option>
											
											<option value="">fdgfdg</option>
											<option value="">fdgfdg</option>
											<option value="">fdgfdg</option>
											<option value="">fdgfdg</option>
											<option value="">fdgfdg</option>
											
										</select>
								  
										<span class="text-danger"></span>
									
                                </div>
                            </div>
                          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                 <div class="form-group">
                                    <select class="selectpicker" name="brandType">
											<option value="">Select City</option>
											
											<option value="">fdgfdg</option>
											<option value="">fdgfdg</option>
											<option value="">fdgfdg</option>
											<option value="">fdgfdg</option>
											<option value="">fdgfdg</option>
											
										</select>
								  
										<span class="text-danger"></span>
									
                                </div>
                            </div>
							 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <div class="nk-int-st">
                                        <input type="text" class="form-control" name="pinCode" id="pinCode" placeholder="Pine Code">
                                    </div>
                                </div>
                            </div>
							
                        </div>	
						<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                           
                            <input type="submit" class="btn btn-success notika-btn-success waves-effect" value="{{ trans('global.save') }}" >
                        </div>
						</div>
            </form> 						
		</div>
		</div>
			<div id="menu1" class="tab-pane fade">
				<div class="tab-ctn">
			<form action="{{ route('admin.users.store') }}" method="POST" enctype="multipart/form-data">
              @csrf
				  <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <div class="nk-int-st">
                                        <input type="text" class="form-control" name="bankName" id="bankName" placeholder="Bank Name">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <div class="nk-int-st">
                                        <input type="text" class="form-control" name="accountNo" id="accountNo" placeholder="Account Number">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <div class="nk-int-st">
                                        <input type="text" class="form-control" name="branchName" id="branchName" placeholder="Branch Name">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <div class="nk-int-st">
                                        <input type="text" class="form-control" name="accountHolderName" id="accountHolderName" placeholder="Account Holder Name">
                                    </div>
                                </div>
                            </div>
							 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <div class="nk-int-st">
                                        <input type="text" class="form-control" name="ifscCode" id="ifscCode" placeholder="IFSC Code">
                                    </div>
                                </div>
                            </div>
							 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <div class="nk-int-st">
                                        <input type="text" class="form-control" name="accountType" id="accountType" placeholder="Account Type">
                                    </div>
                                </div>
                            </div>
                        </div>	
                     <div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                           
                            <input type="submit" class="btn btn-success notika-btn-success waves-effect" value="{{ trans('global.save') }}" >
                        </div>
						</div>	
                 </form>						
				</div>
			</div>
			<div id="menu2" class="tab-pane fade">
				<div class="tab-ctn">
				  <div class="row">
				            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="bootstrap-select fm-cmp-mg">
                                    <select class="selectpicker" name="brandType">
											<option value="">Select KYC Type</option>
											<option value="">fdgfdg</option>
											<option value="">fdgfdg</option>
											<option value="">fdgfdg</option>
											<option value="">fdgfdg</option>
											<option value="">fdgfdg</option>
											
										</select>
								  
										<span class="text-danger"></span>
									
                                </div>
                               </div>                           
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <div class="nk-int-st">
                                        <input type="text" class="form-control" name="kycNo" id="kycNo" placeholder="KYC Number">
                                    </div>
                                </div>
                            </div>
							
					</div>
					<div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <div class="nk-int-st">
                                        <input type="text" class="form-control" name="customerName" id="customerName" placeholder="Customer Name">
                                    </div>
                                </div>
                            </div>
							 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
								     <label>Relation</label>
                                     <div class="fm-checkbox">
                                    <label><input type="radio" name="relation"> <i></i>Father </label>
									 <label><input type="radio" name="relation"> <i></i>Mother </label>
									  <label><input type="radio" name="relation"> <i></i>Husband </label>
									   <label><input type="radio" name="relation"> <i></i>Wife </label>
                                </div>
                                </div>
                            </div>
					</div>
					<div class="row">		
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <div class="nk-int-st">
                                        <input type="text" class="form-control" name="phone" id="phone" placeholder="Phone Number">
                                    </div>
                                </div>
                            </div>
							 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <div class="nk-int-st">
                                        <input type="text" class="form-control" name="email" id="email" placeholder="Email">
                                    </div>
                                </div>
                            </div>
					</div>
					<div class="row">		
							 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <div class="nk-int-st">
                                        <input type="text" class="form-control" name="address" id="address" placeholder="Address">
                                    </div>
                                </div>
                            </div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <div class="nk-int-st">
                                        <input type="text" class="form-control" name="pinCode" id="pinCode" placeholder="Pin Code">
                                    </div>
                                </div>
                            </div>
                     </div>	
				</div>
			</div>
	   </div>
	 </div>
	</div>
    </div>
  </div>                        
 </div>
</div>
			@endsection