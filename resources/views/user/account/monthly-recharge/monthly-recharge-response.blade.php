@extends('layouts.admin')
@section('content')
<div class="breadcomb-area">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="breadcomb-list">
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<div class="breadcomb-wp">
									<div class="breadcomb-icon">
										<i class="notika-icon notika-windows"></i>
									</div>
									<div class="breadcomb-ctn">
										<h2>Monthly Recharge Account</h2>
										<p>Here <span class="bread-ntd">you can monthly recharge account</span></p>
									</div>
								</div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-3">
								<div class="breadcomb-report">
								
									
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
<div class="form-element-area">
  <div class="container">
  
            @csrf
   <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="form-element-list">
                        <div class="basic-tb-hd">
                            <h2>Thank You. Your Payment has been done successfully.</h2>
                            <!--<p>Text Inputs with different sizes by height and column.</p>-->
                        </div>
						
                       
                      
                    </div>
                </div>
            </div>
			</div>
            </div>
			
			@endsection
	