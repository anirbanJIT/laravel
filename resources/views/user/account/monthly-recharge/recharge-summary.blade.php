@extends('layouts.admin')
@section('content')
<div class="breadcomb-area">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="breadcomb-list">
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<div class="breadcomb-wp">
									<div class="breadcomb-icon">
										<i class="notika-icon notika-windows"></i>
									</div>
									<div class="breadcomb-ctn">
										<h2>Recharge Account</h2>
										<p>Here <span class="bread-ntd">you can recharge/activate your account</span></p>
									</div>
								</div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-3">
								<div class="breadcomb-report">
								
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
		@php 
		$date = strtotime("+$request->recharge_duration months", strtotime($expire_date));
		$effectiveDate = date('d M Y', $date); 
		$expire_date = date('Y-m-d H:i:s', $date); 
		@endphp
<div class="form-element-area">
  <div class="container">
  <form action="{{ route('user.recharge') }}" method="post" enctype="multipart/form-data">
   <input type="hidden" name="name" value="{{auth()->user()->name}}">
				  <input type="hidden" name="email" value="{{auth()->user()->email}}"> 
				  <input type="hidden" name="phone" value="{{auth()->user()->phone}}"> 
				  <input type="hidden" name="recharge_duration" value="{{$request->recharge_duration}}">
				  <input type="hidden" name="recharge_amount" value="{{$request->recharge_amount}}">
				  <input type="hidden" name="recharge_amount_after_conversion" value="{{$request->recharge_amount*$creditConversionAmount}}">
				  <input type="hidden" name="expire_date" value="{{$expire_date}}">
            @csrf
   <div class="row">
  
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="data-table-list">
                        <div class="basic-tb-hd">
                            <h2>Your account is active for {{ $request->recharge_duration}} month and expired on {{$effectiveDate}}</h2>
                           <!-- <p>It's just that simple. Turn your simple table into a sophisticated data table and offer your users a nice experience and great features without any effort.</p>-->
                        </div>
                        <div class="table-responsive">
                            <table id="data-table-basic" class="table table-sc-ex">
                <tbody>
				
				<tr>
                       
                    </tr>
                    <tr>
                        <th>
                            You have recharged for :
                        </th>
                        <td>
                            {{ $request->recharge_duration." Month" }}
                        </td>
                    </tr>
					
					 <tr>
                        <th>
                            You Pay : 
                        </th>
                        <td>
						{{$request->recharge_amount." Credits (".env('CURRENCY_SYMBOL')." ".$request->recharge_amount*$creditConversionAmount.")" }}
                        </td>
                    </tr>
					 
                    
                </tbody>
            </table>
          
                    <div class="form-element-list">
                        <div class="basic-tb-hd">
                        <h2>Select Payment method</h2>
                        </div>
						<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="radio">
						<label><input type="radio" name="payment_method" value="credit_wallet" checked>Credit Wallet</label>
						</div>
						<div class="radio">
						<label><input type="radio" name="payment_method" value="cash_free">CashFree</label>
						</div>
						<div class="clear">&nbsp;</div>
							
                            <input type="submit" class="btn btn-success notika-btn-success waves-effect" value="Pay Now" >
						</div>					
						
                       
                     
                      
                    </div>
					
                </div>
           
        </div>


    </div>
</div>
                
</form>			
			</div>
            </div>
			
			@endsection
	