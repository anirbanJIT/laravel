@extends('layouts.admin')
@section('content')
<div class="breadcomb-area">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="breadcomb-list">
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<div class="breadcomb-wp">
									<div class="breadcomb-icon">
										<i class="notika-icon notika-windows"></i>
									</div>
									<div class="breadcomb-ctn">
										<h2>Recharge Account </h2>
										<p>Here <span class="bread-ntd">you can recharge/activate your account</span></p>
									</div>
								</div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-3">
								<div class="breadcomb-report">
								
									
									
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
<div class="form-element-area">
  <div class="container">
  <form action="{{ route('user.recharge-summary') }}" method="POST" enctype="multipart/form-data">
 
            @csrf
   <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="form-element-list">
                        <div class="basic-tb-hd">
                            <!--<h2>{{ trans('global.create') }} {{ trans('cruds.user.title_singular') }}</h2>
                            <p>Text Inputs with different sizes by height and column.</p>-->
                        </div>
						
                        <div class="row">
						
                           <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="nk-int-mk sl-dp-mn mg-t-10">
                                    <h2>Select Month</h2>
                                </div>
								
                                <div class="bootstrap-select">
                                    <select class="selectpicker" name="recharge_duration" id="recharge_duration"  required >
									<option value="1" selected>1</option>
									
									@for($i=2;$i<=12;$i++)
										<option value="{{$i}}" >{{$i}}</option>
									@endfor
									
									</select>
									</div>
									</div>
									 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
									  <div class="nk-int-mk sl-dp-mn mg-t-10">
                                    <h2>Recharge Amount</h2>
                                </div>
                               <div class="form-example-int mg-t-15">
                            <div class="form-group">
                               
                                <div class="nk-int-st">
                                    <input type="text" class="form-control input-sm" readonly value="{{$data->minimum_recharge}}" placeholder="Recharge Amount" id="recharge_amount" name="recharge_amount">
                                </div>
								
                            </div>
                        </div>
                            </div>
							
                           <div class="clear">&nbsp;</div>
							
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                           
                            <input type="submit" class="btn btn-success notika-btn-success waves-effect" value="{{ trans('global.submit') }}" >
                        </div>
                        </div>
                     
                      
                    </div>
                </div>
            </div>
</form>			
			</div>
            </div>
	<script>
	$("#recharge_duration").change(function(){
	var duration = $(this).find(":selected").val();	
	var amount = duration * {{$data->minimum_recharge}};
		$("#recharge_amount").val(amount);
		
		
	});
</script>	
			@endsection
	