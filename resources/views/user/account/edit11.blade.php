@extends('layouts.admin')
<style type="text/css">
     .error{
		 color:red;
	 }
   </style>
@section('content')


<div class="breadcomb-area">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="breadcomb-list">
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<div class="breadcomb-wp">
									<div class="breadcomb-icon">
										<i class="notika-icon notika-windows"></i>
									</div>
									<div class="breadcomb-ctn">
										<h2>Welcome {{$user->name}}</h2>
										<p>Here <span class="bread-ntd">you can update Your Profile</span></p>
									</div>
								</div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-3">
								<div class="breadcomb-report">
								
									 <a class="btn btn-success notika-btn-success waves-effect" href="{{ url()->previous() }}">
										{{ trans('global.back_to_list') }}
									</a>
									
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<div class="form-element-area">
  
			<div class="container">

   <div class="row">
     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	  <div class="widget-tabs-int">
			
	   <div class="widget-tabs-list" id="example-basic">
			<ul class="nav nav-tabs">
			    <li class="active"><a data-toggle="tab" href="#general">General</a></li>
				<li><a data-toggle="tab" href="#home">Contact Details</a></li>
				<li><a data-toggle="tab" href="#menu1">Bank Details</a></li>
				<li><a data-toggle="tab" href="#menu2" class="kyc">KYC Details</a></li>
			</ul>
		<div class="tab-content tab-custom-st">
		<div id="general" class="tab-pane fade in active">
		<div class="tab-ctn">
		 <form action="{{ route('user.general-information.update', [$user->id]) }}" method="POST" enctype="multipart/form-data">
            @csrf
		@method('POST')  
          <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="form-element-list">
                        <div class="basic-tb-hd">
                             <!--<h2>{{ trans('global.edit') }} {{ trans('cruds.user.title_singular') }}</h2>
                           <p>Text Inputs with different sizes by height and column.</p>-->
                        </div>
                       
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group ic-cmp-int">
                                    <div class="form-ic-cmp">
                                        <i class="notika-icon notika-support"></i>
                                    </div>
                                    <div class="nk-int-st">
                                        <input type="text" id="name" name="name" value="{{ old('name', isset($user) ? $user->name : '') }}" required class="form-control" placeholder="{{ trans('cruds.user.fields.name') }}*" >
										
                                    </div>
									 @if($errors->has('name'))
								       <em class="invalid-feedback">
									{{ $errors->first('name') }}
								</em>
								@endif
								<p class="helper-block">
									{{ trans('cruds.user.fields.name_helper') }}
								</p>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group ic-cmp-int">
                                    <div class="form-ic-cmp">
                                        <i class="notika-icon notika-mail"></i>
                                    </div>
                                    <div class="nk-int-st">
                                        <input type="text" id="email" name="email" class="form-control" value="{{ old('email', isset($user) ? $user->email : '') }}" required placeholder="{{ trans('cruds.user.fields.email') }}*" disabled>
                                   
									</div>
									 @if($errors->has('email'))
								<em class="invalid-feedback">
									{{ $errors->first('email') }}
								</em>
								@endif
								<p class="helper-block">
									{{ trans('cruds.user.fields.email_helper') }}
								</p>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group ic-cmp-int">
								    
                                    <div class="form-ic-cmp">
									 
                                        <i class="notika-icon notika-support"></i>
                                    </div>
                                    <div class="nk-int-st">
                                        <input type="text" id="phone" name="phone" value="{{ old('phone', isset($user) ? $user->phone : '') }}" required class="form-control" placeholder="Phone *" >
										
                                    </div>
									 @if($errors->has('phone'))
								       <em class="invalid-feedback">
									{{ $errors->first('phone') }}
									</em>
									@endif
								
                                </div>
                            </div>
							
							
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                           
						     <input type="submit" class="btn btn-success notika-btn-success waves-effect" value="{{ trans('global.save') }}" > 
                        </div>
                        </div>
                     
                      
                    </div>
                </div>
            </div>
          </form>
 			
        </div>
		</div>
		  
		<div id="home" class="tab-pane fade">
		<div class="tab-ctn">
         <form action="{{ route('user.contact-detail.update', [$user->id]) }}" method="POST" id="contact_form" enctype="multipart/form-data">
            @csrf
                         <div class="row">
						 <input type="hidden" name="user_id" value="{{$user->id}}">
                           <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
								     <label>Address</label>
                                    <div class="nk-int-st">
                                        <input type="text" class="form-control" name="address" id="address" placeholder="Address"  value="{{$user->getContactDetail ? $user->getContactDetail->address : ''}}" required>
                                    </div>
								 @if ($errors->has('address'))
								  <span class="text-danger">{{ $errors->first('address') }}</span>
								 @endif
                                </div>
                            </div>
						
                             <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                     <div class="form-group">
									  <label>Country</label>
                                    <select class="selectpicker" name="country" id="country" required>
										<option value="">Select Country</option>
										
										@foreach($countries as $key => $country)
										@if(!empty($user->getContactDetail->country))
									    <option  value="{{$key}}" {{$user->getContactDetail->country == $key  ? 'selected' : ''}}> {{$country}}</option>
									    @else
										 <option  value="{{$key}}"> {{$country}}</option>
                                         @endif									 
									    @endforeach
								    </select>
									 @if ($errors->has('country'))
								    <span class="text-danger">{{ $errors->first('country') }}</span>
								     @endif
									</div>
                              
                            </div>
							</div>
						<div class="row">
                         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
								     <label>State</label>
                                    <select class="selectpicker" name="state" id="state" required>
									</select>

									 @if ($errors->has('state'))
								    <span class="text-danger">{{ $errors->first('state') }}</span>
								     @endif
									
                                </div>
                            </div>
                          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
								     <label>City</label>
                                    <div class="nk-int-st">
                                        <input type="text" class="form-control" name="city" id="city" placeholder="City" value="{{$user->getContactDetail ? $user->getContactDetail->city : ''}}" required>
                                    </div>
									 @if ($errors->has('city'))
								    <span class="text-danger">{{ $errors->first('city') }}</span>
								     @endif
                                </div>
                            </div>
							 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
								    <label>Pin Code</label>
                                    <div class="nk-int-st">
                                        <input type="text" class="form-control" name="pin_code" id="pin_code" placeholder="Pin Code"  value="{{$user->getContactDetail ? $user->getContactDetail->pin_code : ''}}" required>
                                    </div>
									 @if ($errors->has('pin_code'))
								    <span class="text-danger">{{ $errors->first('pin_code') }}</span>
								     @endif
                                </div>
                            </div>
							
                        </div>	
						<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                           
                            <input type="submit" class="btn btn-success notika-btn-success waves-effect" value="{{ trans('global.save') }}" >
                        </div>
						</div>
            </form> 						
		</div>
		</div>
			<div id="menu1" class="tab-pane fade">
				<div class="tab-ctn">
				
			<form action="{{ route('user.bank-detail.update', [$user->id]) }}" method="POST" id="bank_form" enctype="multipart/form-data">
              @csrf
				  <div class="row">
				             <input type="hidden" name="user_id" value="{{$user->id}}"> 
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
								    <label>Bank Name</label>
                                    <div class="nk-int-st">
                                        <input type="text" class="form-control" name="bank_name" id="bank_name" placeholder="Bank Name" value="{{$user->getBankDetail ? $user->getBankDetail->bank_name : ''}}" required>
                                    </div>
									 @if ($errors->has('bank_name'))
										<span class="text-danger">{{ $errors->first('bank_name') }}</span>
									    @endif
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
								 <label>Account Number</label>
                                    <div class="nk-int-st">
                                        <input type="text" class="form-control" name="account_no" id="account_no" value="{{$user->getBankDetail ? $user->getBankDetail->account_no : ''}}" placeholder="Account Number" required>
                                    </div>
									 @if ($errors->has('account_no'))
										<span class="text-danger">{{ $errors->first('account_no') }}</span>
									    @endif
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
								 <label>Branch Name</label>
                                    <div class="nk-int-st">
                                        <input type="text" class="form-control" name="branch_name" id="branch_name" value="{{$user->getBankDetail ? $user->getBankDetail->branch_name : ''}}" placeholder="Branch Name" required>
                                    </div>
									 @if ($errors->has('branch_name'))
										<span class="text-danger">{{ $errors->first('branch_name') }}</span>
									    @endif
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
								     <label>Account Holder Name</label>
                                    <div class="nk-int-st">
                                        <input type="text" class="form-control" name="account_name" id="account_name" value="{{$user->getBankDetail ? $user->getBankDetail->account_name : ''}}" placeholder="Account Holder Name" required>
                                    </div>
									 @if ($errors->has('account_name'))
										<span class="text-danger">{{ $errors->first('account_name') }}</span>
									    @endif
                                </div>
                            </div>
							 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
								     <label>IFSC Code</label>
                                    <div class="nk-int-st">
                                        <input type="text" class="form-control ifsc_code" name="ifsc_code" id="ifsc_code" maxlength="11" minlength="11" size="12" value="{{$user->getBankDetail ? $user->getBankDetail->ifsc_code : ''}}" placeholder="IFSC Code" required>
                                    </div>
									 @if ($errors->has('ifsc_code'))
										<span class="text-danger">{{ $errors->first('ifsc_code') }}</span>
									    @endif
                                </div>
                            </div>
							 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							     <div class="form-group">
								     <label>Account Type</label>
                                    <select class="selectpicker" name="account_type" id="account_type" required>  
									<option value="">Select Account Type </option>
									@if(!empty($user->getBankDetail->account_type))
										
									<option value="Current" {{$user->getBankDetail->account_type == 'Current'  ? 'selected' : ''}}> Current </option>
									<option value="Saving" {{$user->getBankDetail->account_type == 'Saving'  ? 'selected' : ''}}>Saving</option>
									@else
										<option value="Current"> Current </option>
									    <option value="Saving">Saving</option>
									@endif
									</select>

									 @if ($errors->has('account_type'))
								    <span class="text-danger">{{ $errors->first('account_type') }}</span>
								     @endif
									
                                </div>
								{{--     <div class="form-group">
								     <label>Account Type</label>
									  
                                    <div class="nk-int-st">
                                        <input type="text" class="form-control" name="account_type" id="account_type" value="{{$user->getBankDetail ? $user->getBankDetail->account_type : ''}}" placeholder="Account Type">
                                    </div>
									 @if ($errors->has('account_type'))
										<span class="text-danger">{{ $errors->first('account_type') }}</span>
									    @endif
                                </div>  --}}
                            </div>
                        </div>	
                     <div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                           
                            <input type="submit" class="btn btn-success notika-btn-success waves-effect" value="{{ trans('global.save') }}" >
                        </div>
						</div>	
                 </form>						
				</div>
			</div>
			<div id="menu2" class="tab-pane fade">
			<form action="{{ route('user.kyc-detail.update', [$user->id]) }}" method="POST" id="kyc_detail" enctype="multipart/form-data">
              @csrf
			 
				<div class="tab-ctn">
				  <div class="row">
				            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<input type="hidden" name="user_id" value="{{$user->id}}">
                                <div class="bootstrap-select fm-cmp-mg">
								     <label>KYC Type</label>
                                    <select class="selectpicker" name="kyc_id" id="kyc_id">
											<option value="">Select KYC Type</option>
											@foreach($kycCategory as $kycCat)
											
									     @if(!empty($user->getKycDetail->kyc_id))
									    <option  value="{{$kycCat->id}}" {{$user->getKycDetail->kyc_id == $kycCat->id  ? 'selected' : ''}}> {{ $kycCat->name }}</option>
									    @else
										 <option  value="{{ $kycCat->id }}"> {{ $kycCat->name }}</option>
                                         @endif			
											@endforeach
										</select>
								        @if ($errors->has('kyc_id'))
										<span class="text-danger">{{ $errors->first('kyc_id') }}</span>
									    @endif
										
									
                                </div>
                               </div>                           
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
								      <label>KYC Number</label>
                                    <div class="nk-int-st">
                                        <input type="text" class="form-control" name="kyc_number" id="kyc_number" placeholder="KYC Number" value="{{$user->getKycDetail ? $user->getKycDetail->kyc_number : ''}}">
                                    </div>
									 @if ($errors->has('kyc_number'))
										<span class="text-danger">{{ $errors->first('kyc_number') }}</span>
									    @endif
                                </div>
                            </div>
							
					</div>
					<div class="row">
					          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group nk-datapk-ctm form-elet-mg" id="data_2">
                                     <label>KYC Expiry Date</label>
                                    <div class="input-group date nk-int-st">
                                        <span class="input-group-addon"></span>
                                        <input type="text" class="form-control datepicker" name="kyc_expiry_date" id="kyc_expiry_date" placeholder="KYC Expiry Date" value="{{$user->getKycDetail ? $user->getKycDetail->kyc_expiry_date : ''}}">
                                    </div>
									 @if ($errors->has('kyc_expiry_date'))
										<span class="text-danger">{{ $errors->first('kyc_expiry_date') }}</span>
									    @endif
                                </div>
                              </div> 
							  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group nk-datapk-ctm form-elet-mg" id="data_2">
                                    <label>DOB</label>
                                    <div class="input-group date nk-int-st">
                                        <span class="input-group-addon"></span>
                                        <input type="text" class="form-control datepicker" name="dob" id="dob" placeholder="Date of Birth" value="{{$user->getKycDetail ? $user->getKycDetail->dob : ''}}">
                                    </div>
									 @if ($errors->has('dob'))
										<span class="text-danger">{{ $errors->first('dob') }}</span>
									    @endif
                                </div>
                              </div> 
							  {{-- <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <div class="form-group">
								   
                                     <div class="fm-checkbox">
									   <label>Relation: </label>
									  @if(!empty($user->getKycDetail->relation)) 
                                    <label><input type="radio" name="relation" value="Father" {{ ($user->getKycDetail->relation=="Father")? "checked" : "" }}> <i></i>Father </label>
									<label><input type="radio" name="relation" value="Mother" {{ ($user->getKycDetail->relation=="Mother")? "checked" : "" }}> <i></i>Mother </label>
									<label><input type="radio" name="relation" value="Husband" {{ ($user->getKycDetail->relation=="Husband")? "checked" : "" }}> <i></i>Husband </label>
									<label><input type="radio" name="relation" value="Wife" {{ ($user->getKycDetail->relation=="Wife")? "checked" : "" }}> <i></i>Wife </label>
									@else
									<label><input type="radio" name="relation" value="Father" > <i></i>Father </label>
									<label><input type="radio" name="relation" value="Mother"> <i></i>Mother </label>
									<label><input type="radio" name="relation" value="Husband"> <i></i>Husband </label>
									<label><input type="radio" name="relation" value="Wife"> <i></i>Wife </label>	
									@endif
									 @if ($errors->has('relation'))
										<span class="text-danger">{{ $errors->first('relation') }}</span>
									    @endif
                                </div>
                                </div>
							  </div>  --}}
                           
							
					</div>
					<div class="row">	
                              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
								     <label>Customer Name</label>
									 @if(!empty($user->getKycDetail->customer_name))
                                    <div class="nk-int-st">
                                        <input type="text" class="form-control" name="customer_name" id="customer_name" placeholder="Customer Name" required value="{{$user->getKycDetail->customer_name ? $user->getKycDetail->customer_name : ''}}" required>
                                    </div>
									  
									 @else
                                     <div class="nk-int-st">
                                        <input type="text" id="customer_name" name="customer_name" value="{{ old('name', isset($user) ? $user->name : '') }}" required class="form-control" placeholder="{{ trans('cruds.user.fields.name') }}*" >
										
                                    </div>	
                                    @endif
                                     @if ($errors->has('customer_name'))
										<span class="text-danger">{{ $errors->first('customer_name') }}</span>
									    @endif									
										
										
                                </div>
                            </div>					
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
								      <label>Phone Number</label>
									 @if(!empty($user->getKycDetail->phone)) 
                                    <div class="nk-int-st">
                                        <input type="text" class="form-control" name="phone" id="phone" placeholder="Phone Number" value="{{$user->getKycDetail->phone ? $user->getKycDetail->phone : ''}}" required>
                                    </div>
									@else
									 <div class="nk-int-st">
                                        <input type="text" class="form-control" name="phone" id="phone" placeholder="Phone Number" value="{{$user->phone ? $user->phone : ''}}" required>
                                    </div>	
									@endif
									 @if ($errors->has('phone'))
										<span class="text-danger">{{ $errors->first('phone') }}</span>
									    @endif
                                </div>
                            </div>
							
					</div>
					  <div class="row">	
                              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
								  <label>Email</label>
								    @if(!empty($user->getKycDetail->emailKyc))
                                    <div class="nk-int-st">
                                        <input type="text" class="form-control" name="emailKyc" id="emailKyc" placeholder="Email" value="{{$user->getKycDetail->emailKyc ? $user->getKycDetail->emailKyc : ''}}" required>
                                    </div>
                                    @else
                                    <div class="nk-int-st">
                                        <input type="text" class="form-control" name="emailKyc" id="emailKyc" placeholder="Email" value="{{$user->email ? $user->email : ''}}" required>
                                    </div>
									@endif    									
									 @if ($errors->has('emailKyc'))
										<span class="text-danger">{{ $errors->first('emailKyc') }}</span>
									    @endif
                                </div>
                            </div>	
                            							
							 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
								   <label>Address</label>
								  
								   
                                    <div class="nk-int-st">
									   @if(!empty($user->getKycDetail->address))  
                                        <input type="text" class="form-control" name="address" id="address" placeholder="Address" value="{{$user->getKycDetail->address ? $user->getKycDetail->address : ''}}">                                    
									
									  @elseif(!empty($user->getContactDetail->address))
                                        <input type="text" class="form-control" name="address" id="address" placeholder="Address" value="{{$user->getContactDetail->address ? $user->getContactDetail->address : ''}}">
                                      </div>
                                      @else
									  <input type="text" class="form-control" name="address" id="address" placeholder="Address">	
									@endif
									    @if ($errors->has('address'))
										<span class="text-danger">{{ $errors->first('address') }}</span>
									    @endif
                                </div>
                            </div>	
                     </div>	

						<div class="row">
						  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
							         <label>Country</label>
                                    <select class="selectpicker" name="country_id" id="country_id">
										<option value="">Select Country</option>
										
										@foreach($countries as $key => $country)
										@if(!empty($user->getKycDetail->country_id))
									    <option  value="{{$key}}" {{$user->getKycDetail->country_id == $key  ? 'selected' : ''}}> {{$country}}</option>
									    @elseif(!empty($user->getContactDetail->country))
                                         <option  value="{{$key}}" {{$user->getContactDetail->country == $key  ? 'selected' : ''}}> {{$country}}</option>										
									    @else
										 <option  value="{{$key}}"> {{$country}}</option>
                                         @endif									 
									    @endforeach
								    </select>
									     @if ($errors->has('country_id'))
										<span class="text-danger">{{ $errors->first('country_id') }}</span>
									    @endif
						     </div>
                            </div>
                         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
								  <label>State</label>
                                    <select class="selectpicker" name="state_id" id="state_id">
									</select>

										 @if ($errors->has('state_id'))
										<span class="text-danger">{{ $errors->first('state_id') }}</span>
									    @endif
									
                                </div>
                            </div>
							</div>
					 <div class="row">		
                          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
								    <label>City</label>
									<div class="nk-int-st">
								    @if(!empty($user->getKycDetail))
                               
                                        <input type="text" class="form-control" name="city" id="city" placeholder="City" value="{{$user->getKycDetail ? $user->getKycDetail->city : '' }}">
                                   
									@elseif(!empty($user->getContactDetail))
									<input type="text" class="form-control" name="city" id="city" placeholder="City" value="{{$user->getContactDetail ? $user->getContactDetail->city : '' }}">
									 @else
									 <input type="text" class="form-control" name="city" id="city" placeholder="City">	
									@endif
                                </div>
                            </div>
							</div>
							 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
								     <label>Pin Code</label>
									  <div class="nk-int-st">
									 @if(!empty($user->getKycDetail))
                                   
                                        <input type="text" class="form-control" name="pin_code" id="pin_code" placeholder="Pine Code" value="{{$user->getKycDetail ? $user->getKycDetail->pin_code : ''}}">
                                    
									@elseif(!empty($user->getContactDetail))
									
                                        <input type="text" class="form-control" name="pin_code" id="pin_code" placeholder="Pine Code" value="{{$user->getContactDetail ? $user->getContactDetail->pin_code : ''}}">	
                                     @else  
									   <input type="text" class="form-control pin_codekyc" name="pin_code" id="pin_code" placeholder="Pine Code">
								   @endif

									 @if ($errors->has('pin_code'))
										<span class="text-danger">{{ $errors->first('pin_code') }}</span>
									    @endif
                                </div>
                            </div>
							</div>
							</div>
						  <div class="row">
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							 <div class="form-group ic-cmp-int">
							      <div class="form-ic-cmp">
                                        <i class="notika-icon notika-file"></i>
                                    </div>
							     <div class="nk-int-st">		
								<input type="file" accept="image/*" name="attachment"> Upload image
							   </div>
                              </div> 
                            </div>
                           <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						     @if(!empty($user->getKycDetail->attachment))
								<img src="{{ asset('images/kyc/'.$user->getKycDetail->attachment)}}" width="100">
							    @else
								{{ $user->getKycDetail->attachment ?? '' }}	
							    @endif
						     
							 <input type="hidden" name="attachmnetUpdate" value="{{$user->getKycDetail ? $user->getKycDetail->attachment : ''}}">
						   </div>
                          </div>
					  <div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <input type="submit" class="btn btn-success notika-btn-success waves-effect" value="{{ trans('global.save') }}" >
                       </div>
					</div>
				</div>
				</form>
			</div>
			
	   </div>
	 </div>
	</div>
    </div>
  </div>                        
 </div>
            </div>
			@endsection
			
@push('scripts')

<script type="text/javascript">
    $(document).ready(function() {
     // for kyc detail on load
      var countryId = $('#country_id :selected').val();
	  var country = $('#country :selected').val();
	    
      if(countryId){
        $.ajax({
           type:"GET",
           url:"{{url('user/get-state-list')}}?country_id="+countryId,
           success:function(res){               
            if(res){
                $("#state_id").empty();
                $("#state_id").append('<option value="">Select</option>');
				var s_id = ({{$user->getKycDetail ? $user->getKycDetail->state_id : 'null'}} || {{$user->getContactDetail ? $user->getContactDetail->state : 'null'}} );
				
				if(s_id){
					 $.each(res,function(key,value){
					var newClass = (s_id == key) ? "selected" : " ";
					//alert(newClass);
                    $("#state_id").append('<option value="'+key+'" '+newClass+'>'+value+'</option>');
                });
               $('#state_id').selectpicker('refresh');
				}
				
				
               
            }else{
               $("#state_id").empty();
            }
           }
        });
    }else{
        $("#state_id").empty(); 
    } 
//For contact Detail
    if(country){
        $.ajax({
           type:"GET",
           url:"{{url('user/get-state-list')}}?country_id="+country,
           success:function(res){               
            if(res){
                $("#state_id").empty();
                $("#state_id").append('<option value="">Select</option>');
				var s_id = ({{$user->getContactDetail ? $user->getContactDetail->state : 'null'}} );
				
				if(s_id){
					 $.each(res,function(key,value){
					var newClass = (s_id == key) ? "selected" : " ";
					//alert(newClass);
                    $("#state").append('<option value="'+key+'" '+newClass+'>'+value+'</option>');
                });
               $('#state').selectpicker('refresh');
				}
				
				
               
            }else{
               $("#state").empty();
            }
           }
        });
    }else{
        $("#state").empty();
		
       
    } 	
    });
	//On change 
    $('#country').change(function(){
    var countryID = $(this).val();   
	
    if(countryID){
        $.ajax({
           type:"GET",
           url:"{{url('user/get-state-list')}}?country_id="+countryID,
           success:function(res){               
            if(res){
                $("#state").empty();
                $("#state").append('<option>Select</option>');
                $.each(res,function(key,value){
                    $("#state").append('<option value="'+key+'">'+value+'</option>');
                });
               $('#state').selectpicker('refresh');
            }else{
               $("#state").empty();
            }
           }
        });
    }else{
        $("#state").empty();
       
    }      
   }); 
   
    $('#country_id').change(function(){
    var countryID = $(this).val();   
	
    if(countryID){
        $.ajax({
           type:"GET",
           url:"{{url('user/get-state-list')}}?country_id="+countryID,
           success:function(res){               
            if(res){
                $("#state_id").empty();
                $("#state_id").append('<option>Select</option>');
                $.each(res,function(key,value){
                    $("#state_id").append('<option value="'+key+'">'+value+'</option>');
                });
               $('#state_id').selectpicker('refresh');
            }else{
               $("#state_id").empty();
            }
           }
        });
    }else{
        $("#state_id").empty();
      
    }      
   }); 
   
   $('.datepicker').datepicker({
    format: 'yyyy-mm-dd',
	autoclose: true,
    
});
  
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
<script>
      $(function(){
		 
		//IFSC Code validation
		$(".ifsc_code").change(function () {      
		var inputvalues = $(this).val(); 
  	
		  var reg = "^[A-Za-z]{4}0[A-Z0-9a-z]{6}$";    
						if (inputvalues.match(reg)) {    
							return true;    
						}    
						else {    
							 $(".ifsc_code").val("");    
							alert("You entered invalid IFSC code");    
							//document.getElementById("txtifsc").focus();    
							return false;    
						}    
		});      
		
	  });

    if ($("#contact_form").length > 0) {
        $("#contact_form").validate({
 
            rules: {
                address: {
                    required: true,
                    maxlength: 150
                },
 
                country: {
                    required: true,
                },
 
                state: {
                    required: true,                   
                },
				city: {
                    required: true,
                },
				pin_code: {
                    required: true,
					maxlength:6,
					rangelength: [6,6]
                },
            },
            messages: {
 
                address: {
                    required: "Please enter address",
                },
                country: {
                    required: "Please select country",
                },
                state: {
                    required: "Please select state",
                },
				city: {
					required: "Please enter city",
				},
				pin_code: {
					required: "Please enter Pin Code",
					maxlength: "The Pin Code should be 6 digits",
					rangelength: "The Pin Code should be 6 digits",
				},
 
            },
        })
    } 
	  //Bank detail for validation
	if ($("#bank_form").length > 0) {
        $("#bank_form").validate({
 
            rules: {
                bank_name: {
                    required: true,
                },
 
                account_no: {
                    required: true,
                },
 
                branch_name: {
                    required: true,                   
                },
				account_name: {
                    required: true,
                },
				ifsc_code: {
                    required: true,
					
					maxlength:11,
					
                },
				
            },
            messages: {
 
                bank_name: {
                    required: "Please Enter Bank Name",
                },
                account_no: {
                    required: "Please Enter Account Number",
                },
                branch_name: {
                    required: "Please Enter Branch Name",
                },
				account_name: {
					required: "Please Enter Account Holder Name",
				},
				ifsc_code: {
					required: "Please Enter IFSC Code",
					minlength: "The IFSC Code should be 11 digits",
					maxlength: "The IFSC Code not more than 12 digits",
					
				},
				
				
 
            },
        })
    } 
	//KYC detail validation
	if ($("#kyc_detail").length > 0) {
        $("#kyc_detail").validate({
 
            rules: {
                kyc_id: {
                    required: true,
                },
 
                kyc_number: {
                    required: true,
                },
 
                dob: {
                    required: true,                   
                },
				customer_name: {
                    required: true,
                },
				phone: {
                    required: true,
					minlength:10,
					maxlength:14,
                },
				emailKyc: {
                    required: true,
					email: true,
                },
				address: {
                    required: true,
					
                },
				country_id: {
                    required: true,
					
                },
				state_id: {
                    required: true,
					
                },
				pin_code: {
                    required: true,
					maxlength:6,
					rangelength: [6,6],
					
					
                },
				
            },
            messages: {
 
                kyc_id: {
                    required: "Please Select KYC Category",
                },
                kyc_number: {
                    required: "Please Enter KYC Number",
                },
                dob: {
                    required: "Please Enter DOB",
                },
				customer_name: {
					required: "Please Enter Customer Name",
				},
				phone: {
					required: "Please Enter Phone Number",
					minlength: "The phone number should be 10 digits",
					maxlength: "The phone number not more tahn 14 digits",
				},
               emailKyc: {
					required: "Please Enter Email",
					email: "The Email should be valid",
					
				},
				address: {
					required: "Please Enter Customer Name",
				},
				country_id: {
					required: "Please Select Country",
				},
				state_id: {
					required: "Please Select State",
				},
				pin_code: {
					required: "Please Enter Pin Code",
					rangelength: "The Pin Code should be 6 digits",
					maxlength: "The Pin Code not more than 6 digits",
				},
            },
        })
    } 
 </script>

@endpush			