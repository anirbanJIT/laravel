@extends('layouts.admin')
@section('content')


	<div class="breadcomb-area">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="breadcomb-list">
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<div class="breadcomb-wp">
									<div class="breadcomb-icon">
										<i class="notika-icon notika-windows"></i>
									</div>
									<div class="breadcomb-ctn">
										<h2>Show Model Screen Image</h2>
										<p>Show <span class="bread-ntd">particular model screen image</span></p>
									</div>
								</div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-3">								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	


   <div class="data-table-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="data-table-list">
                        <!--<div class="basic-tb-hd">
                            <h2>Basic Example</h2>
                            <p>It's just that simple. Turn your simple table into a sophisticated data table and offer your users a nice experience and great features without any effort.</p>
                        </div>-->
                        <div class="table-responsive">
                            <table id="data-table-basic" class="table table-striped">
                <tbody>
                    <tr>
                        <th width="20%">
                            Brand Category
                        </th>
                        <td>
                            {{ $brandModel->brandsTypes->name }}
                        </td>
                    </tr>
					 <tr>
                        <th>
                            Brand Name
                        </th>
                        <td>
                            {{ $brandModel->brands->name }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Model
                        </th>
                        <td>
                            {{ $brandModel->brandsModel->name }}
                        </td>
                    </tr>

					<tr>
                        <th>
                            Images 
							
                        </th>
                        <td>
						
						@if($brandModel->image)
						@php $myArray = json_decode($brandModel->image);  @endphp
						@foreach($myArray as $key => $value)
						
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
						<div class="animation-single-int">
						<div class="animation-img mg-b-15">
                            <img src="{{ asset('images/models-screen-image/'.$value) }}" class="animate-eleven" style="width:200px;height:200px;">
							
							</div></div></div>
							@endforeach
						@endif
                        </td>
                    </tr>
                    
                </tbody>
            </table>
           
        </div>


    </div>
</div>
</div>
</div>
</div>
</div>
<script>
    $(function () {
//for image uplad button		
	$('#inputImage').change(function() {
  $('#myForm').submit();
});	
		
		
	
	  $('.sa-warning').on('click', function (e) {
   event.preventDefault();
   const url =  $(this).attr('href');
  
   swal({
       title: "Are you sure?",  
text: "Once Deleted. You will also able to recover this again!",  
type: "warning",  
showCancelButton: true,  
confirmButtonText: "Yes, delete it!",
   }).then(function(value) {
       if (value) {
           window.location.href = url;
		  
       }
   });
});
	});
	</script>
@endsection