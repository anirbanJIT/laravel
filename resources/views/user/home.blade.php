@extends('layouts.admin')

@section('content')
 <!-- Start Status area -->
    <div class="notika-status-area">
        <div class="container">
            <div class="row">
			 
			 
			    @php $creditPoints = getCreditPoints() @endphp
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                    <div class="wb-traffic-inner notika-shadow sm-res-mg-t-30 tb-res-mg-t-30">
                        <div class="website-traffic-ctn">
                            <h2><i class="fa fa-credit-card" aria-hidden="true" style="color:#00c292; font-size:23px;"></i> <span class="counter">{{$creditPoints->amount}}</span></h2>
                            <p>Credit Points</p>
                        </div>

                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                    <div class="wb-traffic-inner notika-shadow sm-res-mg-t-30 tb-res-mg-t-30">
                        <div class="website-traffic-ctn">
                            <h2><i class="fa fa-money" aria-hidden="true" style="color:#00c292; font-size:23px;"></i> Rs <span class="counter">{{ $totalRechargeAmount }}</span></h2>
                            <p>Total Recharge Amount</p>
                        </div>
                       
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                    <div class="wb-traffic-inner notika-shadow sm-res-mg-t-30 tb-res-mg-t-30 dk-res-mg-t-30">
                        <div class="website-traffic-ctn">
                            <h2><i class="fa fa-rupee" aria-hidden="true" style="color:#00c292; font-size:23px;"></i> <span class="counter">{{ $commission }}</span></h2>
                            <p>Total Commission</p>
                        </div>
                       
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                    <div class="wb-traffic-inner notika-shadow sm-res-mg-t-30 tb-res-mg-t-30 dk-res-mg-t-30">
                        <div class="website-traffic-ctn">
						     
						    @if($userStatus->status == 1)
                            <h2><i class="fa fa-check" aria-hidden="true" style="color:#00c292; font-size:23px;"></i> Active</h2>
						    @else
							<h2><i class="fa fa-close" aria-hidden="true" style="color:#00c292; font-size:23px;"></i> Inactive</h2>
							@endif
                            <p>Status</p>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Status area-->
    <!-- Start Sale Statistic area-->
    <div class="sale-statistic-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-7 col-sm-6 col-xs-12">
                    <div class="sale-statistic-inner notika-shadow mg-tb-30">
                        <div class="curved-inner-pro">
                            <div class="curved-ctn">
                                <h2>Commission Statistics</h2>
                               
                            </div>
                        </div>
                         <div id="chartContainer" style="height: 300px; width: 100%;"></div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-5 col-sm-6 col-xs-12">
                    <div class="recent-items-wp notika-shadow mg-tb-30">
                        <div class="rc-it-ltd">
                            <div class="recent-items-ctn">
                                <div class="recent-items-title">
                                    <h2>My Reffered Members</h2>
                                </div>
                            </div>
                            <div class="recent-items-inn">
                                <table class="table table-inner table-vmiddle">
                                    <thead>
                                        <tr>                                        
                                            <th>Name</th>
											 <th>Email</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
									   @foreach($child->getChildMember as $childMember)
                                        <tr>                       
                                            <td>{{$childMember->name}}</td>
											<td>{{$childMember->email}}</td>
                                           
                                        </tr>
                                       @endforeach
                                    </tbody>
                                </table>
                            </div>
							
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Sale Statistic area-->
    @push('scripts')
	<script type="text/javascript" src="{{asset('js/canvasjs.min.js')}}"></script>
	<script type="text/javascript">
  window.onload = function () {
	    var url = "{{url('user/chart/getchart')}}"; 
		
		$.get(url, function(data, status){
		
		var attr = new Array();
		
        var chartData=data.chart;

		$.each(chartData, function(key,value) {
		    
		     var x1=value.months;
		     var y1=parseInt(value.y);

		  attr.push({ y: y1, label: x1 });
		}); 
    chardata=attr;		

	var chart = new CanvasJS.Chart("chartContainer", {
		title:{
			text: " "              
		},
		data: [              
		{
			// Change type to "doughnut", "line", "splineArea", etc.
			type: "column",
			dataPoints: chardata
		}]
		
	});
	chart.render();
		});	

  }
  </script>
	
	@endpush


@endsection
