@extends('layouts.admin')
@section('content')

<div class="breadcomb-area">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="breadcomb-list">
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<div class="breadcomb-wp">
									<div class="breadcomb-icon">
										<i class="notika-icon notika-windows"></i>
									</div>
									<div class="breadcomb-ctn">
										<h2>Models List</h2>
										<p>Show <span class="bread-ntd">all list</span></p>
									</div>
								</div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-3">
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
 <!-- Data Table area Start-->
    <div class="data-table-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="data-table-list">
                        <!--<div class="basic-tb-hd">
                            <h2>Basic Example</h2>
                            <p>It's just that simple. Turn your simple table into a sophisticated data table and offer your users a nice experience and great features without any effort.</p>
                        </div>-->
                        <div class="table-responsive">
                            <table id="data-table-basic" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th width="10"></th>
                                        <th> Brand Category  </th>
										 <th>Brand Name</th> 
										  <th>Model</th>
										 <th>Image</th>
                                    </tr>                                  
                                </thead>
                                <tbody>
								
                                    @foreach($brandModel as $key => $model)
                        <tr data-entry-id="{{ $model->id }}">
                            <td>

                            </td>
                            <td>  {{ $model->brandsTypes->name ?? '' }} </td>
                            <td> {{ $model->brands->name ?? '' }} </td>
                            <td>{{ $model->name ?? '' }} </td>
							<td>@if($model->image)
                                <image src="{{URL::asset('images/models/'.$model->image)}}" style="width:70px;height:70px;">
							@endif
							 </td>
                          
                              
                                    </tr>
                                   @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th width="10">

                        </th>
                        <th>
                            Brand Type
                        </th>
                        <th>
                            Brand
                        </th>
                        <th>
                            Name
                        </th>
						<th>
                            Image
                        </th>
                       
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	
	<script>
    $(function () {
	
	  $('.sa-warning').on('click', function (e) {
   event.preventDefault();
   const url =  $(this).attr('href');
  
   swal({
       title: "Are you sure?",  
text: "Once Deleted. You will also able to recover this again!",  
type: "warning",  
showCancelButton: true,  
confirmButtonText: "Yes, delete it!",
   }).then(function(value) {
       if (value) {
           window.location.href = url;
		  
       }
   });
});
	
	
	$('.status_change').change(function() {
		
        var status = $(this).prop('checked') == true ? 1 : 0; 
        var model_id = $(this).data('id'); 
		 $.ajax({
           type:"GET",
           url:"{{url('admin/changeStatus')}}?status="+status+"&model_id="+model_id,
           success:function(res){               
            if(res){
				//alert(res);
                console.log(res.success);
           
            }else{
               //alert("nnnn");
            }
           }
        });
	
    });	
	});
	</script>
@endsection

