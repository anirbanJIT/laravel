@extends('layouts.admin')
@section('content')


	<div class="breadcomb-area">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="breadcomb-list">
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<div class="breadcomb-wp">
									<div class="breadcomb-icon">
										<i class="notika-icon notika-windows"></i>
									</div>
									<div class="breadcomb-ctn">
										<h2>Show Model</h2>
										<p>Show <span class="bread-ntd">particular model</span></p>
									</div>
								</div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-3">
								<div class="breadcomb-report">
								
									 <a class="btn btn-success notika-btn-success waves-effect" href="{{ url()->previous() }}">
                {{ trans('global.back_to_list') }}
            </a>
									
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	


   <div class="data-table-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="data-table-list">
                        <!--<div class="basic-tb-hd">
                            <h2>Basic Example</h2>
                            <p>It's just that simple. Turn your simple table into a sophisticated data table and offer your users a nice experience and great features without any effort.</p>
                        </div>-->
                        <div class="table-responsive">
                            <table id="data-table-basic" class="table table-striped">
                <tbody>
                    <tr>
                        <th>
                            Brand Type
                        </th>
                        <td>
                            {{ $brandModel->brandsTypes->name }}
                        </td>
                    </tr>
					 <tr>
                        <th>
                            Brand
                        </th>
                        <td>
                            {{ $brandModel->brands->name }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Name
                        </th>
                        <td>
                            {{ $brandModel->name }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Slug
                        </th>
                        <td>
                            {{ $brandModel->slug }}
                        </td>
                    </tr>
					 <tr>
                        <th>
                            Status
                        </th>
                        <td>
                            {{ $brandModel->status }}
                        </td>
                    </tr>
					 <tr>
                        <th>
                            Meta Title
                        </th>
                        <td>
                            {{ $brandModel->meta_title }}
                        </td>
                    </tr>
					 <tr>
                        <th>
                            Meta Keywords
                        </th>
                        <td>
                            {{ $brandModel->meta_keywords }}
                        </td>
                    </tr>
					 <tr>
                        <th>
                            Meta Description
                        </th>
                        <td>
                            {{ $brandModel->meta_description }}
                        </td>
                    </tr>
					
					<tr>
                        <th>
                            Image
                        </th>
                        <td>@if($brandModel->image)
                            <img src="{{ asset('images/models/'.$brandModel->image) }}" style="width:200px;height:200px;">
						@endif
                        </td>
                    </tr>
                    
                </tbody>
            </table>
           
        </div>


    </div>
</div>
</div>
</div>
</div>
</div>
@endsection