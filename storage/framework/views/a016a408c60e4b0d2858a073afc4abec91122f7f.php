<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
      <meta name="description" content="POS - Bootstrap Admin Template">
      <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, invoice, html5, responsive, Projects">
      <meta name="author" content="Dreamguys - Bootstrap Admin Template">
      <meta name="robots" content="noindex, nofollow">
      <title>Register - Pos admin template</title>
      <link rel="shortcut icon" type="image/x-icon" href="<?php echo e(asset('public/Admin/img/favicon.png')); ?>">
      <link rel="stylesheet" href="<?php echo e(asset('public/Admin/css/bootstrap.min.css')); ?>">
      <link rel="stylesheet" href="<?php echo e(asset('public/Admin/plugins/fontawesome/css/fontawesome.min.css')); ?>">
      <link rel="stylesheet" href="<?php echo e(asset('public/Admin/plugins/fontawesome/css/all.min.css')); ?>">
      <link rel="stylesheet" href="<?php echo e(asset('public/Admin/css/style.css')); ?>">
   </head>
   <body class="account-page">
      <div class="main-wrapper">
         <div class="account-content">
            <div class="login-wrapper">
                
               <div class="login-content">
                  <?php if(\Session::has('message')): ?>
                        <p class="alert alert-warning">
                            <?php echo e(\Session::get('message')); ?>

                        </p>
                    <?php endif; ?>
                  <div class="login-userset">
                    <form method="POST" action="<?php echo e(route('register')); ?>">
                        <?php echo e(csrf_field()); ?>

                     <div class="login-logo">
                       <h3><b>Modern Mart Admin Panel</b></h3>
                     </div>
                     <div class="login-userheading">
                        <h3>Create a new account</h3>
                     </div>
                     <div class="form-login">
                        <label>Referral Id</label>
                        <div class="form-addons">
                           <input type="text" placeholder="Enter Referral Id" name="referred_by" >
                           <img src="<?php echo e(asset('public/Admin/img/icons/users1.svg')); ?>" alt="img">
                        </div>
                     </div>
                     <div class="form-login">
                        <label>Full Name</label>
                        <div class="form-addons">
                           <input type="text" placeholder="Enter your full name" name="name">
                           <img src="<?php echo e(asset('public/Admin/img/icons/users1.svg')); ?>" alt="img">
                        </div>
                     </div>
                     <div class="form-login">
                        <label>Email</label>
                        <div class="form-addons">
                           <input type="text" placeholder="Enter your email address" name="email">
                           <img src="<?php echo e(asset('public/Admin/img/icons/mail.svg')); ?>" alt="img">
                        </div>
                     </div>
                     <div class="form-login">
                        <label>Mobile Number (With country code)</label>
                        <div class="form-addons">
                           <input type="text" placeholder="Enter your full name" name="phone">
                           <img src="<?php echo e(asset('public/Admin/img/icons/users1.svg')); ?>" alt="img">
                        </div>
                     </div>
                     <div class="form-login">
                        <label>Password</label>
                        <div class="pass-group">
                           <input type="password" class="pass-input" placeholder="Enter your password" name="password">
                           <span class="fas toggle-password fa-eye-slash"></span>
                        </div>
                     </div>
                     <div class="form-login">
                        <label>Password</label>
                        <div class="pass-group">
                           <input type="password" class="pass-input" placeholder="Enter your password" name="password_confirmation">
                           <span class="fas toggle-password fa-eye-slash"></span>
                        </div>
                     </div>
                     <div class="form-login">
                        <button type="submit" class="btn btn-login"> Sign Up </button>
                     </div>
                     <div class="signinform text-center">
                        <h4>Already a user? <a href="<?php echo e(route('login')); ?>" class="hover-a">Sign In</a></h4>
                     </div>
                     </form>
                  </div>
                
               </div>
               <div class="login-img">
                  <img src="<?php echo e(asset('public/Admin/img/login.jpg')); ?>" alt="img">
               </div>
            </div>
         </div>
      </div>
       <script src="<?php echo e(asset('public/Admin/js/jquery-3.6.0.min.js')); ?>"></script>
      <script src="<?php echo e(asset('public/Admin/js/feather.min.js')); ?>"></script>
      <script src="<?php echo e(asset('public/Admin/js/bootstrap.bundle.min.js')); ?>"></script>
      <script src="<?php echo e(asset('public/Admin/js/script.js')); ?>"></script>
   </body>
</html><?php /**PATH C:\laragon\www\Laravel\resources\views/auth/register.blade.php ENDPATH**/ ?>