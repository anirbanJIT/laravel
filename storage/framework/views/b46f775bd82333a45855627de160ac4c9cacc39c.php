<?php $user = Auth::user() ?>
<div class="sidebar" id="sidebar">
    <div class="sidebar-inner slimscroll">
        <div id="sidebar-menu" class="sidebar-menu">
            <ul>
                <li>
                    <a href="<?php echo e(route('admin.home')); ?>"><img src="<?php echo e(asset('public/Admin/img/icons/dashboard.svg')); ?>"
                            alt="img"><span> <?php echo e(trans('global.dashboard')); ?></span> </a>
                </li>
                <?php if(Gate::check('users_manage')): ?>
                    <li><a href="<?php echo e(route('admin.users.index')); ?>"><img
                                src="<?php echo e(asset('public/Admin/img/icons/users1.svg')); ?>" alt="img"> <span>User
                                Management</span></a></li>
                <?php endif; ?>

                <?php if(
                    $user->can('brandstype_manage') ||
                        $user->can('brands_manage') ||
                        $user->can('brand_models') ||
                        $user->can('brand_sub_models') ||
                        $user->can('brand_models_screen_image') ||
                        $user->can('bulkupload_manage')): ?>
                    <li class="submenu">
                        <a href="javascript:void(0);"><i data-feather="layers"></i> <span> Brands</span> <span
                                class="menu-arrow"></span></a>
                        <ul>
                            <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('brandstype_manage')): ?>
                                <li><a href="<?php echo e(route('admin.brand-type.index')); ?>">Categories</a></li>
                            <?php endif; ?>

                            <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('brand_models_screen_image')): ?>
                                <li><a href="<?php echo e(route('admin.models-screen-image.create')); ?>">Upload Model screen</a></li>
                            <?php endif; ?>
                            <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('bulkupload_manage')): ?>
                                <li><a href="<?php echo e(route('admin.bulkupload.create')); ?>">Bulk Upload</a></li>
                            <?php endif; ?>
                        </ul>
                    </li>
                <?php endif; ?>

                <?php if($user->can('brands_manage') || $user->can('brand_models') || $user->can('brand_sub_models')): ?>
                    <!--            <li class="submenu">
    <a href="javascript:void(0);"><i data-feather="layers"></i> <span> Cagtegory</span> <span class="menu-arrow"></span></a>
                <ul>
     <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('brands_manage')): ?>
    <li><a href="<?php echo e(route('admin.brands.index')); ?>">Brands</a></li>
<?php endif; ?>
     <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('brand_models')): ?>
    <li><a href="<?php echo e(route('admin.models.index')); ?>">Models</a></li>
<?php endif; ?>
     <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('brand_sub_models')): ?>
    <li><a href="<?php echo e(route('admin.submodels.index')); ?>">Sub Models</a></li>
<?php endif; ?>
    </ul>
   </li> -->
                <?php endif; ?>

                <?php if(
                    $user->can('users_manage') ||
                        $user->can('retailer_manage') ||
                        $user->can('manage_members_tree') ||
                        $user->can('manage_monthly_recharge') ||
                        $user->can('manage_customer_recharge_commission') ||
                        $user->can('manage_recharge_commission_payout') ||
                        $user->can('manage_credit_wallet_transaction') ||
                        $user->can('buyer-manage') ||
                        $user->can('manage_credit_wallet') ||
                        $user->can('recharge_plan_manage') ||
                        $user->can('bulk_cut_plan_manage') ||
                        $user->can('profile_change_request') ||
                        $user->can('imei_change_request')): ?>
                    <li class="submenu">
                        <a href="javascript:void(0);"><img src="<?php echo e(asset('public/Admin/img/icons/users1.svg')); ?>"
                                alt="img"><span>Retailer Management</span> <span class="menu-arrow"></span></a>
                        <ul>
                            <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('retailer_manage')): ?>
                                <li><a href="<?php echo e(route('admin.retailers-list')); ?>">Retailers</a></li>
                            <?php endif; ?>
                            <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('manage_members_tree')): ?>
                                <li><a href="<?php echo e(route('admin.members-tree')); ?>">Members Tree</a></li>
                            <?php endif; ?>
                            <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('manage_monthly_recharge')): ?>
                                <!-- <li><a href="<?php echo e(route('admin.users-monthly-recharge')); ?>">Monthly Recharge</a></li> -->
                            <?php endif; ?>
                            <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('manage_recharge_commission_payout')): ?>
                                <!-- <li><a href="<?php echo e(route('admin.customer-recharge-commission')); ?>">Customer Recharge Commission & Payout</a></li> -->
                            <?php endif; ?>
                            <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('manage_credit_wallet_transaction')): ?>
                                <li><a href="<?php echo e(route('admin.credit-wallet-transaction')); ?>">Wallet Transaction</a></li>
                            <?php endif; ?>
                            <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('buyer-manage')): ?>
                                <li><a href="<?php echo e(route('admin.buyer-list')); ?>">Buyer List</a></li>
                            <?php endif; ?>
                            <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('manage_credit_wallet')): ?>
                                <li><a href="<?php echo e(route('admin.retailer-credit-balance')); ?>">Balances</a></li>
                            <?php endif; ?>
                            <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('plan_manage')): ?>
                                <li><a href="<?php echo e(route('admin.plans.index')); ?>">Plans</a></li>
                            <?php endif; ?>
                            <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('recharge_plan_manage')): ?>
                                <li><a href="<?php echo e(route('admin.recharge-plan.index')); ?>">Recharge Plan</a></li>
                            <?php endif; ?>
                            <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('bulk_cut_plan_manage')): ?>
                                <li><a href="<?php echo e(route('admin.bulkcut-plan.index')); ?>">Bulkcut Plan</a></li>
                            <?php endif; ?>
                            <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('profile_change_request')): ?>
                                <li><a href="<?php echo e(route('admin.retailer-profile-change-request')); ?>">Profile Change
                                        Requests</a></li>
                            <?php endif; ?>
                            <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('imei_change_request')): ?>
                                <li><a href="<?php echo e(route('admin.imei-change-request')); ?>">IMEI Change Requests</a></li>
                            <?php endif; ?>
                            <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('retailer_manage')): ?>
                                <!-- <li><a href="<?php echo e(route('admin.staffs-list')); ?>">Staffs</a></li> -->
                            <?php endif; ?>
                        </ul>
                    </li>
                <?php endif; ?>
                <?php if($user->can('video_category_manage') || $user->can('video_manage')): ?>
                    <!-- <li class="submenu">
               <a href="javascript:void(0);"><i class="fa fa-video"></i> <span> Video</span> <span class="menu-arrow"></span></a>
               <ul>
                  <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('video_category_manage')): ?>
    <li><a href="<?php echo e(route('admin.video-category.index')); ?>">Video Category</a></li>
<?php endif; ?>
                  <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('video_manage')): ?>
    <li><a href="<?php echo e(route('admin.video.index')); ?>">Video</a></li>
<?php endif; ?>
               </ul>
            </li> -->
                <?php endif; ?>
                <li class="submenu">
                    <a href="javascript:void(0);"><img src="<?php echo e(asset('public/Admin/img/icons/settings.svg')); ?>"
                            alt="img"><span> Settings</span> <span class="menu-arrow"></span></a>
                    <ul>
                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('video_category_manage')): ?>
                            <li><a href="<?php echo e(route('admin.video-category.index')); ?>">Video</a></li>
                        <?php endif; ?>
                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('banner_manage')): ?>
                            <li><a href="<?php echo e(route('admin.banner.index')); ?>">Banner</a></li>
                        <?php endif; ?>
                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('currency_manage')): ?>
                            <!-- <li><a href="<?php echo e(route('admin.setting.currency')); ?>">Currency</a></li> -->
                        <?php endif; ?>
                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('notification_manage')): ?>
                            <li><a href="<?php echo e(route('admin.generalnotification.index')); ?>">Notification</a></li>
                        <?php endif; ?>
                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('complaint_feedback')): ?>
                            <li><a href="<?php echo e(route('admin.complaint-feedback.index')); ?>">Complaint</a></li>
                            <li><a href="<?php echo e(route('admin.feedback-list')); ?>">Feedback</a></li>
                        <?php endif; ?>
                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('papersize_manage')): ?>
                            <li><a href="<?php echo e(route('admin.paper-sizes.index')); ?>">Paper Size Manage</a></li>
                            <li><a href="<?php echo e(route('admin.setting.privacypolicy', '')); ?>">Privacy Policy</a></li>
                            <li><a href="<?php echo e(route('admin.setting.setting_pdf', '')); ?>">Setting Pdf</a></li>
                            <li><a href="<?php echo e(route('admin.user-reactions.index')); ?>">Likes & Dislikes</a></li>
                            <li><a href="<?php echo e(route('admin.serial-numbers.index')); ?>">Serial Numbers</a></li>
                            <li><a href="<?php echo e(route('admin.machine-numbers.index')); ?>">Machine Numbers</a></li>
                            <li><a href="<?php echo e(route('admin.setting.website')); ?>">Website</a></li>
                            <li><a href="<?php echo e(route('admin.setting.otpprice')); ?>">OTP Price</a></li>
                        <?php endif; ?>
                        <li><a href="<?php echo e(route('auth.change_password')); ?>">Change Password</a></li>
                    </ul>
                </li>
                <li><a href="<?php echo e(route('admin.plotter.index')); ?>"><img
                            src="<?php echo e(asset('public/Admin/img/icons/Plotter.png')); ?>" alt="img">
                        <span>Plotter</span></a></li>
                <li><a href="<?php echo e(route('admin.planrecharge.index')); ?>"><img
                            src="<?php echo e(asset('public/Admin/img/icons/Plan.png')); ?>" alt="img"> <span>Plans</span></a>
                </li>
                <li>
                    <a href="<?php echo e(route('admin.licenseKeyList')); ?>">
                        <img src="<?php echo e(asset('public/Admin/img/icons/Plan.png')); ?>" alt="img" />
                        <span>License Key</span>
                    </a>
                </li>
            </ul>


        </div>
    </div>
</div>
<?php /**PATH C:\laragon\www\Laravel\resources\views/layouts/sidebar.blade.php ENDPATH**/ ?>