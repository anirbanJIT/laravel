<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <meta name="description" content="POS - Bootstrap Admin Template">
    <meta name="keywords"
        content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
    <meta name="author" content="Dreamguys - Bootstrap Admin Template">
    <meta name="robots" content="noindex, nofollow">
    <title><?php echo e(trans('global.create')); ?> Serial Numbers</title>

    <link rel="shortcut icon" type="image/x-icon" href="<?php echo e(asset('public/Admin/img/favicon.png')); ?>">

    <link rel="stylesheet" href="<?php echo e(asset('public/Admin/css/bootstrap.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('public/Admin/plugins/select2/css/select2.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('public/Admin/css/animate.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('public/Admin/css/dataTables.bootstrap4.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('public/Admin/plugins/fontawesome/css/fontawesome.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('public/Admin/plugins/fontawesome/css/all.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('public/Admin/css/style.css')); ?>">

</head>

<body>
    <div id="global-loader">
        <div class="whirly-loader"> </div>
    </div>
    <div class="main-wrapper">
        

        

        <div class="page-wrapper">
            <div class="content container-fluid">
                <div class="page-header">
                    <div class="row">
                        <div class="col">
                            <h3 class="page-title"><?php echo e(trans('global.create')); ?> Serial Numbers</h3>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a
                                        href="<?php echo e(route('admin.home')); ?>"><?php echo e(trans('global.dashboard')); ?></a></li>
                                <li class="breadcrumb-item">
                                    <a href="<?php echo e(route('admin.licenseKeyList')); ?>">
                                        License Key List
                                    </a>
                                </li>
                                <li class="breadcrumb-item active">Create License Key</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">

                            <div class="card-body">

                                <div class="row">
                                    <div class="col-md-6">
                                        <?php if(count($errors) > 0): ?>
                                            <div class="alert alert-danger">
                                                <ul>
                                                    <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <li><?php echo e($error); ?></li>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </ul>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                </div>

                                <form action="<?php echo e(route('admin.licenseKeyStore')); ?>" method="POST">
                                    <?php echo e(csrf_field()); ?>

                                    <div class="row tr_clone_first tr_clone">
                                        <div class="form-group col-md-6">
                                            <label>Quantity</label>
                                            <input type="number" name="quantity" id="quantity" class="form-control"
                                                placeholder="Quantity" required="true" min="1" />
                                        </div>

                                    </div>
                                    <div>
                                        <button type="submit" class="btn btn-success">Create</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="<?php echo e(asset('public/Admin/js/jquery-3.6.0.min.js')); ?>"></script>
    <script src="<?php echo e(asset('public/Admin/plugins/select2/js/select2.min.js')); ?>"></script>
    <script src="<?php echo e(asset('public/Admin/plugins/select2/js/custom-select.js')); ?>"></script>
    <script src="<?php echo e(asset('public/Admin/js/feather.min.js')); ?>"></script>
    <script src="<?php echo e(asset('public/Admin/js/jquery.slimscroll.min.js')); ?>"></script>
    <script src="<?php echo e(asset('public/Admin/js/bootstrap.bundle.min.js')); ?>"></script>
    <script src="<?php echo e(asset('public/Admin/js/script.js')); ?>"></script>
    <script></script>
</body>

</html>

<?php echo $__env->make('layouts.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php echo $__env->make('layouts.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\laragon\www\Laravel\resources\views/admin/licensekey/createkey.blade.php ENDPATH**/ ?>