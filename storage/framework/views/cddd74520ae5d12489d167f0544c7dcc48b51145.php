<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
      <meta name="description" content="POS - Bootstrap Admin Template">
      <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
      <meta name="author" content="Dreamguys - Bootstrap Admin Template">
      <meta name="robots" content="noindex, nofollow">
      <title><?php echo e(trans('cruds.imei_change_request.title_singular')); ?> <?php echo e(trans('global.list')); ?></title>
      
      <link rel="shortcut icon" type="image/x-icon" href="<?php echo e(asset('public/Admin/img/favicon.png')); ?>">
      <link rel="stylesheet" href="<?php echo e(asset('public/Admin/css/bootstrap.min.css')); ?>">
      <link rel="stylesheet" href="<?php echo e(asset('public/Admin/css/animate.css')); ?>">
      <link rel="stylesheet" href="<?php echo e(asset('public/Admin/css/dataTables.bootstrap4.min.css')); ?>">

      <!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.7.1/css/buttons.dataTables.min.css"> -->
      
       

      <link rel="stylesheet" href="<?php echo e(asset('public/Admin/plugins/fontawesome/css/fontawesome.min.css')); ?>">
      <link rel="stylesheet" href="<?php echo e(asset('public/Admin/plugins/fontawesome/css/all.min.css')); ?>">
      <link rel="stylesheet" href="<?php echo e(asset('public/Admin/css/style.css')); ?>">
      <link rel="stylesheet" href="<?php echo e(asset('public/Admin/plugins/select2/css/select2.min.css')); ?>">
      <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
      <link rel="stylesheet" href="<?php echo e(asset('public/Admin/tabulator/bootstrap/tabulator_bootstrap4.css')); ?>">
      
   </head>
   <body>
      <!-- <div id="global-loader">
         <div class="whirly-loader"> </div>
      </div> -->
      <div class="main-wrapper">
        
     
        
        
        <div class="page-wrapper">
            <div class="content container-fluid">
               <div class="page-header">
                  <div class="row">
                     <div class="col">
                        <h3 class="page-title"><?php echo e(trans('cruds.imei_change_request.title_singular')); ?> <?php echo e(trans('global.list')); ?></h3>
                        <ul class="breadcrumb">
                           <li class="breadcrumb-item"><a href="<?php echo e(route('admin.home')); ?>"><?php echo e(trans('global.dashboard')); ?></a></li>
                           <li class="breadcrumb-item active"><?php echo e(trans('cruds.imei_change_request.title_singular')); ?> <?php echo e(trans('global.list')); ?></li>
                        </ul>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-12">
                     <div class="card">
                        <div class="card-header row">
                            <div class="col-lg-6"><h4 class="card-title"><?php echo e(trans('cruds.imei_change_request.title_singular')); ?> <?php echo e(trans('global.list')); ?></h4></div>
                            
                        </div>
                        <div class="card-body">
                           <div class="table-responsive">
                              <!-- <table class="table datanew" id="data-table-retailers"> -->
                                 <!-- <button class="btn btn-outline-info waves-effect" id="download-csv">Download CSV</button> <p><br/></p> -->
                                 <div id="data-table-retailers">
                                 </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
    <script src="<?php echo e(asset('public/Admin/js/jquery-3.6.0.min.js')); ?>"></script>
    <script src="<?php echo e(asset('public/Admin/js/feather.min.js')); ?>"></script>
    <script src="<?php echo e(asset('public/Admin/js/jquery.slimscroll.min.js')); ?>"></script>
    <script src="<?php echo e(asset('public/Admin/js/jquery.dataTables.min.js')); ?>"></script>
    <script src="<?php echo e(asset('public/Admin/js/dataTables.bootstrap4.min.js')); ?>"></script>
    <script src="<?php echo e(asset('public/Admin/js/bootstrap.bundle.min.js')); ?>"></script>
    <script src="<?php echo e(asset('public/Admin/js/script.js')); ?>"></script>
      
     <script type="text/javascript" language="javascript" src='https://cdn.datatables.net/buttons/2.2.3/js/dataTables.buttons.min.js' defer></script>
      
      <script type="text/javascript" language="javascript" src='https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js' defer></script>
      
      <script type="text/javascript" language="javascript" src='https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js' defer></script>
      
      <script type="text/javascript" language="javascript" src='https://cdn.datatables.net/buttons/2.2.3/js/buttons.html5.min.js' defer></script> 
      
      <!-- <script type="text/javascript" src="<?php echo e(asset('public/Admin/js/3rdparty/tabulator.min.js')); ?>"></script> -->
      <script src="<?php echo e(asset('public/Admin/tabulator/tabulator.min.js')); ?>"></script>
      <script src="<?php echo e(asset('public/Admin/tabulator/jspdf.min.js')); ?>"></script>
      <script src="<?php echo e(asset('public/Admin/tabulator/xlsx.full.min.js')); ?>"></script>
      <script src="<?php echo e(asset('public/Admin/tabulator/jspdf.plugin.autotable.js')); ?>"></script>
      <script type="text/javascript">
      
      var tabledata = <?php echo json_encode($retailerUsers); ?>;
     // console.log(tabledata);
      function customFilter(data){
          return data.car && data.rating < 3;
      }

      function updateFilter(){

          var filter = $("#filter-field").val() == "function" ? customFilter : $("#filter-field").val();

          if($("#filter-field").val() == "function" ){
              $("#filter-type").prop("disabled", true);
              $("#filter-value").prop("disabled", true);
          }else{
              $("#filter-type").prop("disabled", false);
              $("#filter-value").prop("disabled", false);
          }

          table.setFilter(filter, $("#filter-type").val(), $("#filter-value").val());
      }

      //Update filters on value change
      $("#filter-field, #filter-type").change(updateFilter);
      $("#filter-value").keyup(updateFilter);

      var table = new Tabulator("#data-table-retailers", {
          data:tabledata,           //load row data from array
          layout:"fitColumns",      //fit columns to width of table
          responsiveLayout:"collapse",  //hide columns that dont fit on the table
          tooltips:true,            //show tool tips on cells
          addRowPos:"top",          //when adding a new row, add it to the top of the table
          history:true,             //allow undo and redo actions on the table
          pagination:"local",       //paginate the data
          paginationSize:10,         //allow 7 rows per page of data
          movableColumns:true,      //allow column order to be changed
          resizableRows:true,       //allow row order to be changed
          initialSort:[             //set the initial sort order of the data
              {column:"id", dir:"desc"},
          ],
          columns: [
               // { data: "delete_chk", orderable:false, searchable:false},
                {title:"Id", field:"id", editor:"label",headerFilter:"input",headerFilterPlaceholder:"Id"},
                {title:"Name", field:"name", editor:"label",headerFilter:"input",headerFilterPlaceholder:"Name"},
                {title:"Email", field:"email", editor:"label",headerFilter:"input",headerFilterPlaceholder:"Email"},
                {title:"Phone", field:"phone", editor:"label",headerFilter:"input",headerFilterPlaceholder:"Phone"},
                {title:"IMEI", field:"device_id", editor:"label",headerFilter:"input",headerFilterPlaceholder:"IMEI"},
                {title:"Date", field:"created_at", orderable:false, searchable:false,headerFilterPlaceholder:"Date"},
                {title:"Status", field:"status", formatter:function(cell, formatterParams, onRendered){
                  if(cell.getValue() == 1){
                      return "Active";
                  } else {
                      return "Deactive"
                  }            
               }},
        ],
      });

      //trigger download of data.csv file
      // document.getElementById("download-csv").addEventListener("click", function(){
      //     table.download("csv", "data.csv");
      // });

      //trigger download of data.xlsx file
      document.getElementById("download-csv").addEventListener("click", function(){
          table.download("csv", "data.csv");
      });

      //trigger download of data.pdf file
      // document.getElementById("download-pdf").addEventListener("click", function(){
      //     table.download("pdf", "data.pdf", {
      //         orientation:"portrait", //set page orientation to portrait
      //         title:"Example Report", //add title to report
      //     });
      // });

</script>
   </body>
</html>
<?php echo $__env->make('layouts.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php echo $__env->make('layouts.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\laragon\www\Laravel\resources\views/admin/retailers/retailer-imei-change-request-list.blade.php ENDPATH**/ ?>